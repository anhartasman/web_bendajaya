<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;
class LogInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function doLogin()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'username'    => 'required', // make sure the username field is not empty
        'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('/admin/login')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        // create our user data for the authentication
        $userdata = array(
            'username'  => Input::get('username'),
            'password'  => Input::get('password')
        );
echo "username :".$userdata['username'];
echo "password :".$userdata['password'];


    $isian=DB::table('tb_akun_admin')
    ->where('username', '=', $userdata['username'])
    ->where('password', '=', $userdata['password'])
    ->first();
    if(count($isian)==1){

    \Cookie::queue('keybaru', $userdata['username']); 

    $infomember = new \stdClass();
    $infomember->user_pangkat=$isian->level;
    $infomember->user_id=$isian->id;
    $infomember->user_username=$isian->username;
    $infomember->user_mmid=$isian->mmid;
    $infomember->user_nama=$isian->nama;

    \Cookie::queue('infomember', $infomember);


        $tb_infomember=DB::table('tb_infomember')
        ->where('mmid', '=', $isian->mmid)
        ->first();
         $infomember->user_idtemplate=$tb_infomember->idstyle_template;
        //Cookie::make('keybaru', $userdata['username']);
    //return $isian;
    return Redirect::to('/admin');
  }else{
      return Redirect::to('/admin/login');
  }


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
