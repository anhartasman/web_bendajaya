<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use App\Http\Requests;

use Validator;
use Session;
use Cookie;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Vsmoraes\Pdf\Pdf;
use DB;
use View;

class web_admin extends Controller
{
    private $pdf;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function pageTes(){
        $sandi="udin123";
        $salt="1000004".rand(1,999);
        $password = hash('sha512', $sandi . $salt);
        echo "sandi : ".$sandi."<br>";
        echo "salt : ".$salt."<br>";
        echo "hasil : ".$password."<br>";
        /* A uniqid, like: 4b3403665fea6 */
        printf("uniqid(): %s\r\n", uniqid());
     }
     public function pagePengaturanTransaksi(){
       $tb_infomember=DB::table('tb_infomember')
       ->where('mmid', '=', getUserInfo("user_mmid"))
       ->first();
       $tb_diskon=DB::table('tb_diskon')
       ->where('mmid', '=', getUserInfo("user_mmid"))
       ->first();
     return View::make('hal_admin.setting_transaksi',
     array(
       'tb_infomember'=>$tb_infomember
      ,'tb_diskon'=>$tb_diskon
     ));

     }
     public function savePengaturanDiskon(){
        $isian=array();
        $QZ=preg_replace('/\D/', '',Input::get('QZ'));
        $QG=preg_replace('/\D/', '',Input::get('QG'));
        $GA=preg_replace('/\D/', '',Input::get('GA'));
        $KD=preg_replace('/\D/', '',Input::get('KD'));
        $JT=preg_replace('/\D/', '',Input::get('JT'));
        $SJ=preg_replace('/\D/', '',Input::get('SJ'));
        $MV=preg_replace('/\D/', '',Input::get('MV'));
        $IL=preg_replace('/\D/', '',Input::get('IL'));
        $KAI=preg_replace('/\D/', '',Input::get('KAI'));
        $HTL=preg_replace('/\D/', '',Input::get('HTL'));
        if($QZ<=10000){$isian['QZ']=$QZ;}
        if($QG<=10000){$isian['QG']=$QG;}
        if($GA<=10000){$isian['GA']=$GA;}
        if($KD<=10000){$isian['KD']=$KD;}
        if($JT<=10000){$isian['JT']=$JT;}
        if($SJ<=10000){$isian['SJ']=$SJ;}
        if($MV<=10000){$isian['MV']=$MV;}
        if($IL<=10000){$isian['IL']=$IL;}
        if($KAI<=5000){$isian['KAI']=$KAI;}
        if($HTL<=10000){$isian['HTL']=$HTL;}

              DB::table('tb_diskon')
            ->where('mmid', '=', getUserInfo("user_mmid"))
              ->update($isian);

                return Redirect::to('/admin/transaksi/setting');
     }
     public function savePengaturanTransaksi(){
       $rules = array(
          'trx_batasbayar'    => 'required|numeric|max:10'
       );
        $validator = Validator::make(Input::all(), $rules);
         if ($validator->fails()) {
           return Redirect::to('/admin/transaksi/setting')
                 ->withErrors($validator);
         }

              DB::table('tb_infomember')
            ->where('mmid', '=', getUserInfo("user_mmid"))
              ->update(array(
                   'trx_batasbayar' => Input::get('trx_batasbayar')
              ));

              $tb_infomember=DB::table('tb_infomember')
              ->where('mmid', '=', getUserInfo("user_mmid"))
              ->first();

                return Redirect::to('/admin/transaksi/setting');
     }


     public function savePengaturanAkun(){

       $rules = array(
          'idakun'    => 'required',
          'name' => 'required',
          'username' => 'required',
          'email' => 'required',
          'phone' => 'required'
       );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);


         if ($validator->fails()) {
           return Redirect::to('/admin/akun/pengaturan')
                 ->withErrors($validator);
         }
$gagal=0;
         $idakun=Input::get('idakun');
         $name=Input::get('name');
         $phone=Input::get('phone');
         $sandibaru=Input::get('newpassword');
         $sandi=Input::get('password');
         $sandi2=Input::get('confirmpassword');

         $bahanupdate=array(
              'name' => Input::get('name')
              ,'phone' => Input::get('phone')
         );

if($sandibaru!=null){
  if($sandibaru==Input::get('confirmnewpassword')){
if($sandi==$sandi2){
    $cari=DB::table('tb_user')
    ->where('username', '=', Input::get('username'))
    ->first();
    if($cari==null){
      $gagal=1;
    }else{
    $salt=$cari->salt;

    $hasilhashlama = hash('sha512',$sandi . $salt);
        $carilama=DB::table('tb_user')
        ->where('password',$hasilhashlama)
        ->first();
        if($carilama==null){
          $gagal=1;
        }else{
    $hasilhash = hash('sha512',$sandibaru . $salt);

    $bahanupdate['password']=$hasilhash;
  }
    }
  }else{
    $gagal=1;
  }
}else{
  $gagal=1;
}
}
if(Input::file('filefoto')!=null){

  $destinationPath = 'uploads/images'; // upload path
  $extension = Input::file('filefoto')->getClientOriginalExtension(); // getting image extension
  //$fileName = rand(11111,99999).'.'.$extension; // renameing image
  $fileName = "fotolawyer".$idakun.'.'.$extension;
      $bahanupdate['filefoto']=$fileName;
      Input::file('filefoto')->move($destinationPath, $fileName); // uploading file to given path

}
if($gagal==1){
  return Redirect::to('/admin/akun/pengaturan')
      ->withErrors(['Ada yang salah']);
}else{

       DB::table('tb_user')
       ->where('id', $idakun)
       ->update($bahanupdate);
}

      return Redirect::to('/admin/akun/pengaturan');
     }
     public function pagePengaturanAkun(){
       $isian=DB::table('tb_user')
       ->where('id', '=', getUserInfo("user_id"))
       ->first();

                 $id=$isian->id;
                 $username=$isian->username;
                 $name=$isian->name;
                 $level=$isian->level;

     return View::make('hal_admin.tabel_akun_setting', array('id'=>$id,'username'=>$username,'name' =>$name,'level'=>$level));

     }
     public function eraseAkun(){

       $rules = array(
          'idakun'    => 'required'
       );
       $validator = Validator::make(Input::all(), $rules);
       $idakun=Input::get('idakun');
       $level=Input::get('nomlevel');


       if ($validator->fails() || ($level>getUserInfo("user_pangkat"))) {
         return Redirect::to('/admin/akun/level/delete/'.$idakun)
               ->withErrors($validator);
       }
          DB::table('tb_admin')->where('id', '=', $idakun)->delete();

        return Redirect::to('/admin/akun/list');
     }
     public function deleteAkun($id){
       $isian=DB::table('tb_admin')
       ->where('id', '=', $id)
       ->where('level', '<=', getUserInfo("user_pangkat"))
       ->first();

                 $username=$isian->username;
                 $name=$isian->name;
                 $level=$isian->level;
                 $password=$isian->password;

     return View::make('hal_admin.tabel_akun_delete', array('id'=>$id,'username'=>$username,'name' =>$name,'level'=>$level));

     }
     public function crud_u(){
       $halaman=Request::segment(2)."/".Request::segment(3);
       $urlbalik="";
       switch($halaman){
         case "users/manage": // proses update user
     $rules = array(
        'idakun'    => 'required'
        ,'username'    => 'required'
        ,'name' => 'required'
        ,'email' => 'required'
        ,'phone' => 'required'
        ,'address' => 'required'
     );
     $messages = array(
           'idakun.required'=>'Data tidak lengkap'
           ,'username.required'=>'Username harus diisi'
           ,'name.required'=>'Nama harus diisi'
           ,'email.required'=>'Email harus diisi'
           ,'phone.required'=>'Phone harus diisi'
           ,'address.required'=>'Address harus diisi'
     );
     $idakun=Input::get('idakun');
     $level=Input::get('level');
     $address=Input::get('address');
     $tb_user=DB::table('tb_user')
     ->where('id', $idakun)
     ->select(['level'])
     ->first();

     $urlbalik=url('/admin/users/manage/role')."/".$tb_user->level;
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);

       if ($validator->fails() || ($level>getUserInfo("user_pangkat"))) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $userdata = array();
         foreach($rules as $key=>$value) {
           if($key!="idakun"){
            $userdata[$key] = Input::get($key);
           }
         }


          DB::table('tb_rekening')
          ->where('idu', $idakun)
          ->update(array(
            'namabank'=>$namabank
            ,'nomrek'=>$nomrek
            ,'atasnama'=>$atasnama
          ));

          DB::table('tb_rekening')
          ->where('idu', $idakun)
          ->update(array(
            'namabank'=>$namabank
            ,'nomrek'=>$nomrek
            ,'atasnama'=>$atasnama
          ));

         $username=Input::get('username');
         DB::table('tb_user')
         ->where('id', $idakun)
         ->update($userdata);

         $tbluser=DB::table('tb_user')
         ->where('id', $idakun)
         ->first();

         DB::table('tb_wallet')
         ->where('idu', $tbluser->id)
         ->update(array(
           'detail'=>"Belong to $tbluser->name"
         ));

         $resetsandi=Input::get('resetsandi');
         if($resetsandi==1){
           $sandi=$username."123";
           $salt="garammanis".rand(1,999);
           $hasilhash = hash('sha512', $sandi. $salt);
                DB::table('tb_user')
                ->where('id', $idakun)
                ->update(array(
                     'salt' => $salt,
                     'password' => $hasilhash
                ));
         }

         $lock=Input::get('lock');
         if($lock==1){
                DB::table('tb_user')
                ->where('id', $idakun)
                ->update(array(
                     'status' => 0
                ));
         }else{
           DB::table('tb_user')
           ->where('id', $idakun)
           ->update(array(
                'status' => 1
           ));
         }

         }

     break;
     case "lawyers/list": // proses update lawyer
     $rules = array(
         'name'    => 'required'
         ,'email' => 'required'
         ,'username' => 'required'
         ,'phone' => 'required'
         ,'hargapermenit'    => 'required'
         ,'persenfee' => 'required'
         ,'id'    => 'required'
     );
     $messages = array(
           'id.required'=>'Data tidak lengkap'
           ,'name.required'=>'Nama lawyer harus diisi'
           ,'email.required'=>'Email harus diisi'
           ,'username.required'=>'Username harus diisi'
           ,'phone.required'=>'Phone harus diisi'
           ,'persenfee.required'=>'Persenan harus diisi'
           ,'hargapermenit.required'=>'Harga per menit harus ditentukan'
     );

     // run the validation rules on the inputs from the form
     $validator = Validator::make(Input::all(), $rules,$messages);

    $idfoto= Input::get('id');

     if ($validator->fails()) {
       return Redirect::to('/admin/lawyers/list/edit/'.$idfoto)
             ->withErrors($valixdator);
     } else {
       $nomid=$idfoto;
       $kategori=Input::get('bidang');

    $dafmap=DB::table('tb_jem_lawyer')
    ->where('idl', '=', $idfoto)
    ->get();

    foreach ($dafmap as $map) {
      if (! in_array($map->idj, $kategori)){

        echo "gak ada ".$map->id;
        DB::table('tb_jem_lawyer')
        ->where('id', '=', $map->id)
        ->where('idl', '=', $idfoto)
        ->delete();

      }
    }
    foreach($kategori as $kat){
    echo $kat;
    if(($kat+0)!=0){

    $dafmap=DB::table('tb_jem_lawyer')
    ->where('idl', '=', $idfoto)
    ->where('idj', '=', $kat)
    ->first();

    if($dafmap==null){

    DB::table('tb_jem_lawyer')->insert(
    array(
           'idj' => $kat,
           'idl' => $idfoto
     )
     );

    }
    }

    echo "<br>";
    }



$tampilkanposisi=0;
if(Input::get('posisilawyer')!=null && Input::get('posisifirm')!=null){
  $tampilkanposisi=2;
}else if(Input::get('posisilawyer')!=null){
  $tampilkanposisi=1;
}else if(Input::get('posisifirm')!=null){
  $tampilkanposisi=3;
}
        DB::table('tb_lawyer')
       ->where('id', $idfoto)
       ->update(array(
               'permenit' => ambilangka(Input::get('hargapermenit'))
              ,'persenfee' => Input::get('persenfee')
              ,'firmlatitude' => Input::get('firmlatitude')
              ,'firmlongitude' => Input::get('firmlongitude')
              ,'tampilkanposisi'=>$tampilkanposisi
         ));
         DB::table('tb_user')
        ->where('idr', $idfoto)
        ->update(array(
                'name' => Input::get('name')
                ,'username' => Input::get('username')
                ,'email' => Input::get('email')
                ,'phone' => Input::get('phone')
          ));

         if(Input::file('foto')!=null){
           $destinationPath = 'uploads/images'; // upload path
           $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
           //$fileName = rand(11111,99999).'.'.$extension; // renameing image
           $fileName = "fotolawyer".$nomid.'.'.$extension;
           Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
           // sending back with message

           DB::table('tb_user')
           ->where('idr', $idfoto)
           ->update(array(
                  'filefoto' => $fileName,
                )
            );

       }

     }

return Redirect::to('/admin/lawyers/list/edit/'.$idfoto);
     break;
     case "callcenter/dokumentasi" : //proses update dokumentasi
     $rules = array(
        'idd'    => 'required'
        ,'name' => 'required'
        ,'masalah' => 'required'
        ,'idj' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
           ,'name.required'=>'Nama harus diisi'
           ,'masalah.required'=>'Kendala harus diisi'
           ,'idj.required'=>'Bidang hukum harus diisi'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/callcenter/dokumentasi');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $idj=Input::get('idj');
         $userdata = array();

         $tb_klien=DB::table('tb_klien')
                       ->where('idu',Input::get('name'))
                       ->select(['id'])
                       ->first();

         $userdata['idk'] = $tb_klien->id;
         $userdata['masalah'] = Input::get('masalah');
         $userdata['idj'] = Input::get('idj');
         if($idj!=0){
         $userdata['status'] = 1;
         }else{
         $userdata['status'] = 0;
         }

         DB::table('tb_dokumentasi')
         ->where('id', $idd)
         ->update($userdata);
     }
     break;
     case "callcenter/orderaktif" : //proses update order aktif
     $rules = array(
        'idd'    => 'required'
        ,'status' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
           ,'status.required'=>'Status harus dipilih'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/callcenter/orderaktif');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);

         $userdata = array();
         $status = Input::get('status');
         $idj = Input::get('idj');
         if($idj!=null){
           if($idj==0){
             if($status!=1){
                  $urlbalik=url('/admin/callcenter/orderaktif/edit/'.$idd);
               return Redirect::to($urlbalik)
                     ->withErrors("Dokumen yang dialihkan tidak bisa diubah statusnya");
             }
           }else{

                         $dafmap=DB::table('tb_jem_cs')
                         ->where('idcs', '=', getUserInfo("user_roleid"))
                         ->where('idj', '=', $idj)
                         ->get();
                         if(count($dafmap)==0 && $status!=1){
                              $urlbalik=url('/admin/callcenter/orderaktif/edit/'.$idd);
                           return Redirect::to($urlbalik)
                                 ->withErrors("Bidang tersebut bukan untuk Anda kerjakan");
                         }
           }
           $userdata["idj"]=$idj;
         }
         $userdata["status"]=$status;


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {

         DB::table('tb_dokumentasi')
         ->where('id', $idd)
         ->update($userdata);
       }
     break;
     case "keuangan/pengajuantopup": //proses update top up
     $rules = array(
        'idt'    => 'required'
        ,'status' => 'required'
     );
     $messages = array(
           'idt.required'=>'Data tidak lengkap'
           ,'status.required'=>'Status harus dipilih'
     );
     $idt=Input::get('idt');

     $urlbalik=url('/admin/keuangan/pengajuantopup');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);

         $userdata = array();
         $status = Input::get('status');
         $admin_comment = Input::get('admin_comment');

         $userdata["status"]=$status;
         $userdata["admin_comment"]=$admin_comment;

       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {

         $tabeltopup=DB::table('tb_topup')
         ->where('id', '=', $idt)
         ->first();
         $jumlahtopup=$tabeltopup->topup_amount;


         $tb_user=DB::table('tb_user')
         ->where('id', '=', $tabeltopup->idu)
         ->select(['id','token','idsaldo'])
         ->first();

         DB::table('tb_topup')
         ->where('id', $idt)
         ->update($userdata);
         if($status==3){

           $tb_wallet=DB::table('tb_wallet')
           ->where('id', '=', $tb_user->idsaldo)
           ->first();

           $saldolama=$tb_wallet->ecash;
           $saldobaru=$saldolama+$jumlahtopup;


           DB::table('tb_wallet')
           ->where('id', $tb_user->idsaldo)
           ->update(array(
             "ecash"=>$saldobaru
           ));

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Saldo Anda sudah bertambah'
           );

           kirimFCM($tb_user->token,$msg);
         }else if($status==2){

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Top up Anda ditolak'
           );

           kirimFCM($tb_user->token,$msg);
         }

       }
     break;
     case "keuangan/transferfee": //proses update transferfee
     $rules = array(
        'idw'    => 'required'
        ,'status' => 'required'
     );
     $messages = array(
           'idw.required'=>'Data tidak lengkap'
           ,'status.required'=>'Status harus dipilih'
     );
     $idw=Input::get('idw');

     $urlbalik=url('/admin/keuangan/transferfee');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);

         $userdata = array();
         $status = Input::get('status');
         $admin_comment = Input::get('admin_comment');

         $userdata["status"]=$status;
         $userdata["admin_comment"]=$admin_comment;

         $tabelwithdraw=DB::table('tb_withdraw')
         ->where('id', '=', $idw)
         ->first();
         $jumlahwithdraw=$tabelwithdraw->nilai;

         $tb_lawyer=DB::table('tb_lawyer')
         ->where('id', '=', $tabelwithdraw->idl)
         ->select(['id','saldo','token'])
         ->first();
         $saldolama=$tb_lawyer->saldo;
         $saldobaru=$saldolama-$jumlahwithdraw;



       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       }else if (($saldolama<$jumlahwithdraw)) {
         return Redirect::to($urlbalik)
               ->withErrors("Saldo tidak cukup");
       } else {

         DB::table('tb_withdraw')
         ->where('id', $idw)
         ->update($userdata);
         if($status==3){
           $tamp=strtotime($tabelwithdraw->tanggalminta);
           $periode_m=date('m',$tamp);
           $periode_y=date('Y',$tamp);
           $tb_mutasi=DB::table('tb_mutasi')
                ->whereRaw('YEAR(periode) = ?', [$periode_y])
                ->whereRaw('MONTH(periode) = ?', [$periode_m])
                ->first();
           if($tb_mutasi!=null){
             $withdrawlama=$tb_mutasi->withdraw;
             $withdrawbaru=$withdrawlama+$jumlahwithdraw;
             DB::table('tb_mutasi')
            ->where('id', $tb_mutasi->id)
            ->update(array(
             'withdraw'=>$withdrawbaru
                ));
           }else{
             DB::table('tb_mutasi')->insert(
                   array(
                               'idl' => $tb_lawyer->id
                               ,'periode' => $tabelwithdraw->tanggalminta
                               ,'fee'=>0
                               ,'share'=>0
                               ,'tl'=>0
                               ,'withdraw'=>$jumlahwithdraw
                               ,'saldo'=>$saldolama
                         )
                         );
           }
           DB::table('tb_lawyer')
           ->where('id', $tb_lawyer->id)
           ->update(array(
             "saldo"=>$saldobaru
           ));


                      $msg = array
                      (
                      	'idurusan'=>4
                        ,'pesan'=>'Uang sudah ditransfer'
                      );

                      kirimFCM($tb_lawyer->token,$msg);

         }else if($status==1){

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Withdraw sedang diproses'
           );

           kirimFCM($tb_lawyer->token,$msg);
         }else if($status==2){

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Withdraw ditolak'
           );

           kirimFCM($tb_lawyer->token,$msg);
         }

       }
     break;
     case "keuangan/transferfeeklien": //proses update transferfee
     $rules = array(
        'idw'    => 'required'
        ,'status' => 'required'
     );
     $messages = array(
           'idw.required'=>'Data tidak lengkap'
           ,'status.required'=>'Status harus dipilih'
     );
     $idw=Input::get('idw');

     $urlbalik=url('/admin/keuangan/transferfeeklien');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);

         $userdata = array();
         $status = Input::get('status');
         $admin_comment = Input::get('admin_comment');

         $userdata["status"]=$status;
         $userdata["admin_comment"]=$admin_comment;

         $tabelwithdraw=DB::table('tb_withdrawkomisi')
         ->where('id', '=', $idw)
         ->first();
         $jumlahwithdraw=$tabelwithdraw->nilai;

         $tb_klien=DB::table('tb_klien')
         ->where('id', '=', $tabelwithdraw->idk)
         ->select(['id','komisi','token'])
         ->first();
         $komisilama=$tb_klien->komisi;
         $komisibaru=$komisilama-$jumlahwithdraw;



       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       }else if (($komisilama<$jumlahwithdraw)) {
         return Redirect::to($urlbalik)
               ->withErrors("Saldo tidak cukup");
       } else {

         DB::table('tb_withdrawkomisi')
         ->where('id', $idw)
         ->update($userdata);
         if($status==3){
           $tamp=strtotime($tabelwithdraw->tanggalminta);
           $periode_m=date('m',$tamp);
           $periode_y=date('Y',$tamp);
           $tb_mutasi=DB::table('tb_mutasiklien')
                ->whereRaw('YEAR(periode) = ?', [$periode_y])
                ->whereRaw('MONTH(periode) = ?', [$periode_m])
                ->first();
           if($tb_mutasi!=null){
             $withdrawlama=$tb_mutasi->withdraw;
             $withdrawbaru=$withdrawlama+$jumlahwithdraw;
             DB::table('tb_mutasiklien')
            ->where('id', $tb_mutasi->id)
            ->update(array(
             'withdraw'=>$withdrawbaru
                ));
           }else{
             DB::table('tb_mutasiklien')->insert(
                   array(
                               'idk' => $tb_klien->id
                               ,'periode' => $tabelwithdraw->tanggalminta
                               ,'withdraw'=>$jumlahwithdraw
                               ,'totalkomisi'=>$komisilama
                         )
                         );
           }
           DB::table('tb_klien')
           ->where('id', $tb_klien->id)
           ->update(array(
             "komisi"=>$komisibaru
           ));


                      $msg = array
                      (
                      	'idurusan'=>4
                        ,'pesan'=>'Uang sudah ditransfer'
                      );

                      kirimFCM($tb_klien->token,$msg);

         }else if($status==1){

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Withdraw sedang diproses'
           );

           kirimFCM($tb_klien->token,$msg);
         }else if($status==2){

           $msg = array
           (
           	'idurusan'=>4
             ,'pesan'=>'Withdraw ditolak'
           );

           kirimFCM($tb_klien->token,$msg);
         }

       }
     break;
     case "kontenapps/berita" : //proses update berita
     $rules = array(
        'idd'    => 'required'
        ,'judul' => 'required'
        ,'isi' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
           ,'judul.required'=>'Judul harus diisi'
           ,'isi.required'=>'Berita harus diisi'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/kontenapps/berita/edit/'.$idd);
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $userdata = array();

         $userdata['judul'] = Input::get('judul');
         $userdata['isi'] = Input::get('isi');

         DB::table('tb_berita')
         ->where('id', $idd)
         ->update($userdata);
     }
     break;
     case "product_category/list" : //proses update kategori produk
     $rules = array(
        'idd'    => 'required'
        ,'kategori' => 'required'
        ,'info' => 'required'
        ,'color' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
           ,'kategori.required'=>'Kategori harus diisi'
          ,'info.required'=>'Keterangan belum diisi'
         ,'color.required'=>'Warna belum diisi'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/product_category/list/edit/'.$idd);
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $userdata = array();

         $userdata['category'] = Input::get('kategori');
         $userdata['info'] = Input::get('info');
         $userdata['color'] = Input::get('color');
         $userdata['last_update'] = date("Y-m-d H:i:s");

         DB::table('tb_product_cat')
         ->where('id', $idd)
         ->update($userdata);

        if(Input::file('foto')!=null){

          $destinationPath = 'uploads/images'; // upload path
          $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
          //$fileName = rand(11111,99999).'.'.$extension; // renameing image
          $fileName = "ikonproduk".$idd.'.'.$extension;
          Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
          // sending back with message

          DB::table('tb_product_cat')
          ->where('id', $idd)
          ->update(array(
                 'icon' => $fileName,
               )
           );

          }

     }
     break;
     case "product/list" : //proses update produk
     $rules = array(
        'idd'    => 'required'
        ,'kategori' => 'required'
        ,'name' => 'required'
        ,'price' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
          ,'info.required'=>'Keterangan belum diisi'
         ,'name.required'=>'Nama belum diisi'
        ,'price.required'=>'Harga belum diisi'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/product/list/edit/'.$idd);
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $nomid=$idd;
         $kategori=Input::get('kategori');

      $dafcat=DB::table('tb_jem_product_cat')
      ->where('idp', '=', $nomid)
      ->get();

      foreach ($dafcat as $map) {
        if (! in_array($map->idpc, $kategori)){

          echo "gak ada ".$map->id;
          DB::table('tb_jem_product_cat')
          ->where('id', '=', $map->id)
          ->where('idp', '=', $nomid)
          ->delete();

        }
      }
      foreach($kategori as $kat){
      echo $kat;
      if(($kat+0)!=0){

      $dafmap=DB::table('tb_jem_product_cat')
      ->where('idp', '=', $nomid)
      ->where('idpc', '=', $kat)
      ->first();

      if($dafmap==null){

      DB::table('tb_jem_product_cat')->insert(
      array(
             'idpc' => $kat,
             'idp' => $nomid
       )
       );

      }
      }

      echo "<br>";
      }

         $userdata = array();

         $userdata['info'] = Input::get('info');
         $userdata['price'] = Input::get('price');
         $userdata['name'] = Input::get('name');
         $userdata['last_update'] = date("Y-m-d H:i:s");

         DB::table('tb_product')
         ->where('id', $idd)
         ->update($userdata);

        if(Input::file('foto')!=null){

          $destinationPath = 'uploads/images'; // upload path
          $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
          //$fileName = rand(11111,99999).'.'.$extension; // renameing image
          $fileName = "fotoutamaproduk".$idd.'.'.$extension;
          Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
          // sending back with message

          DB::table('tb_product')
          ->where('id', $idd)
          ->update(array(
                 'mainpict' => $fileName,
               )
           );

          }

     }
     break;
     case "transport_service/list" : //proses update jasa transportasi
     $rules = array(
        'idd'    => 'required'
        ,'name' => 'required'
        ,'detail' => 'required'
     );
     $messages = array(
           'idd.required'=>'Data tidak lengkap'
           ,'name.required'=>'Nama jasa harus diisi'
          ,'detail.required'=>'Detail jasa belum diisi'
     );
     $idd=Input::get('idd');

     $urlbalik=url('/admin/transport_service/list/edit/'.$idd);
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {
         $userdata = array();

         $userdata['name'] = Input::get('name');
         $userdata['detail'] = Input::get('detail');
         $userdata['last_update'] = date("Y-m-d H:i:s");

         DB::table('tb_transport_service')
         ->where('id', $idd)
         ->update($userdata);

     }
     break;
     //akhir proses update
   }


     return Redirect::to($urlbalik);

     }


     public function crud_d(){
       $halaman=Request::segment(2)."/".Request::segment(3);
       $urlbalik="";
       switch($halaman){
         case "users/manage": // proses delete user
         $urlbalik=url('/admin/users/manage');
     $rules = array(
        'idakun'    => 'required'
     );
     $messages = array(
           'idakun.required'=>'Data tidak lengkap'
     );
     $idakun=Input::get('idakun');
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules);


       if ($validator->fails()) {
         return Redirect::to($urlbalik)
               ->withErrors($validator);
       } else {


         }
         $tb_user=DB::table('tb_user')
                   ->where('id',$idakun)
                   ->select(['level'])
                   ->first();

     $bisahapus=1;

         $tbjem=DB::table('tb_transaksi')
         ->where('idb', '=', $idakun)
         ->orWhere('ids','=', $idakun)
         ->get();
         if(count($tbjem)>0){
           $bisahapus=0;
         }

         $tbjem=DB::table('tb_topup')
         ->where('idu', '=', $idakun)
         ->get();
         if(count($tbjem)>0){
           $bisahapus=0;
         }

         $tbjem=DB::table('tb_product')
         ->where('idu', '=', $idakun)
         ->get();
         if(count($tbjem)>0){
           $bisahapus=0;
         }

    if($bisahapus==1){
     DB::table('tb_user')
     ->where('id', $idakun)
     ->delete();
   }else{

     DB::table('tb_user')
     ->where('id', $idakun)
     ->update(array(
       'status'=>0
     ));

     DB::table('tb_product')
     ->where('idu', $idakun)
     ->update(array(
       'aktif'=>0
     ));

   }


     break;
     case "lawyers/list" : //proses delete lawyer
     $idfoto= Input::get('id');

         $isian=DB::table('tb_user')
         ->where('idr', '=', $idfoto)
         ->first();

            $namafilegambar=$isian->filefoto;
            if($namafilegambar!=""){
            $alamatfile=public_path('uploads/images/'.$namafilegambar);

    echo $alamatfile;
    unlink($alamatfile);
    }

    DB::table('tb_lawyer')->where('id', '=', $idfoto)->delete();
    DB::table('tb_jem_lawyer')->where('idl', '=', $idfoto)->delete();

    return Redirect::to('/admin/lawyers/list');
     break;
     case "kontenapps/berita" : //proses delete berita
     $id= Input::get('id');
    DB::table('tb_berita')->where('id', '=', $id)->delete();

    return Redirect::to('/admin/kontenapps/berita');
     break;
     case "product_category/list" : //proses delete kategori produk
     $id= Input::get('id');
     $bisahapus=0;

     $tbkategori=DB::table('tb_product_cat')
     ->where('id', '=', $id)
     ->first();

     $namafilegambar=$tbkategori->icon;

    $tbjem=DB::table('tb_jem_product_cat')
    ->where('idpc', '=', $id)
    ->get();
    if(count($tbjem)==0){
    $bisahapus=1;
    if($namafilegambar!=""){
    $alamatfile=public_path('uploads/images/'.$namafilegambar);

    echo $alamatfile;
    unlink($alamatfile);
    }


    DB::table('tb_product_cat')->where('id', '=', $id)->delete();
    }
    return Redirect::to('/admin/product_category/list');
     break;
     case "product/list" : //proses delete produk
     $id= Input::get('id');

     $tbproduk=DB::table('tb_product')
     ->where('id', '=', $id)
     ->first();

     $namafilegambar=$tbproduk->mainpict;
     if($namafilegambar!=""){
     $alamatfile=public_path('uploads/images/'.$namafilegambar);

     echo $alamatfile;
     unlink($alamatfile);
     }

    DB::table('tb_product')->where('id', '=', $id)->delete();
    DB::table('tb_jem_product_cat')->where('idp', '=', $id)->delete();

    return Redirect::to('/admin/product/list');
     break;
     case "transport_service/list" : //proses delete jasa transportasi
     $id= Input::get('id');

     $tabelservice=DB::table('tb_transport_service')
     ->where('id', '=', $id)
     ->delete();

    return Redirect::to('/admin/transport_service/list');
     break;
   }


     return Redirect::to($urlbalik);
     // akhir proses delete
     }



     public function editAkun($id){
       $isian=DB::table('tb_admin')
       ->where('id', '=', $id)
       ->first();

                 $username=$isian->username;
                 $name=$isian->name;
                 $level=$isian->level;

     return View::make('hal_admin.tabel_akun_edit', array('id'=>$id,'username'=>$username,'name' =>$name,'level'=>$level));

     }
     public function addAkun()
     {
     // validate the info, create rules for the inputs
     $rules = array(
        'username'    => 'required',
        'name' => 'required',
        'sandi' => 'required|confirmed'
     );
     $messages = array(
           'username.required'=>'Username harus diisi',
           'name.required'=>'Nama harus diisi',
           'sandi.required'=>'Password harus diisi',
           'sandi.confirmed'=>'Password harus sama',
     );
     $sandi=Input::get('sandi');
     $level=Input::get('level');

     // run the validation rules on the inputs from the form
     $validator = Validator::make(Input::all(), $rules,$messages);



     if ($validator->fails() || ($level>getUserInfo("user_pangkat"))) {
      return Redirect::to('/admin/akun/tambah')
            ->withErrors($validator)
            ->withInput();
     }  else {
       $salt="garammanis".rand(1,999);
       $hasilhash = hash('sha512', $sandi. $salt);
       $idterakhir = DB::table('tb_admin')->insertGetId(
       array(
                  'username' => Input::get('username'),
                  'name' => Input::get('name'),
                  'salt' => $salt,
                  'password' => $hasilhash,
                  'level' =>5,
                  'last_login' =>''
            )
       );
              return Redirect::to('/admin/akun/list');

     }
     }

     public function tambahAkun(){
           return view('hal_admin.tabel_akun_add');
     }


     public function allAkun(){
       $dafakun=DB::table('tb_admin')
       ->where('id','!=', getUserInfo("user_id"))
       ->get();
       return View::make('hal_admin.tabel_akun', array('dafakun'=>$dafakun));
     }


     public function pageDepan()
     {
        return View::make('hal_admin.index');
     }

     public function allLawyer(){
       $daflawyer=DB::table('tb_lawyer')
       ->get();
       return View::make('hal_admin.tabel_lawyer', array('daflawyer'=>$daflawyer));
     }


     public function allKlien(){
       $dafklien=DB::table('tb_klien')
       ->get();
       return View::make('hal_admin.tabel_klien', array('dafklien'=>$dafklien));
     }

     public function tambahKlien(){
           return view('hal_admin.tabel_klien_add');
     }

     public function addKlien(){
       // validate the info, create rules for the inputs
       $rules = array(
           'name'    => 'required',
           'email' => 'required',
           'foto' => 'required'
       );
       $messages = array(
             'name.required'=>'Nama  harus diisi',
             'email.required'=>'Email harus diisi',
             'foto.required'=>'Foto harus diisi',
       );
       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules,$messages);


       if ($validator->fails()) {
         return Redirect::to('/admin/klien/tambah')
               ->withErrors($validator);
       }

     $idterakhir = DB::table('tb_klien')->insertGetId(
         array(
                     'name' => Input::get('name'),
                     'online' => 0,
                     'email' => Input::get('email'),
                     'username' => Input::get('username'),
                     'password' => Input::get('password'),
                     'filefoto'  => ""
               )
               );


               $destinationPath = 'uploads/images'; // upload path
               $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
               //$fileName = rand(11111,99999).'.'.$extension; // renameing image
               $fileName = "fotoklien".$idterakhir.'.'.$extension;
               Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
               // sending back with message
                    DB::table('tb_klien')
               ->where('id', $idterakhir)
               ->update(array(
                           'filefoto' => $fileName
                     ));


                 return Redirect::to('/admin/klien/edit/'.$idterakhir);

     }

     public function editKlien($id){
       $isian=DB::table('tb_klien')
       ->where('id', '=', $id)
       ->first();
                $name=$isian->name;
                $email=$isian->email;
                $filefoto=$isian->filefoto;
                $username=$isian->username;
                $password=$isian->password;


     return View::make('hal_admin.tabel_klien_edit', array('id'=>$id
     ,'name'=>$name
     ,'email' =>$email
     ,'filefoto'=>$filefoto
     ,'username'=>$username
     ,'password'=>$password));

     }

     public function saveKlien(){
       // validate the info, create rules for the inputs
       $rules = array(
           'name'    => 'required',
           'email' => 'required',
           'id'    => 'required'
       );
       $messages = array(
             'id.required'=>'Data tidak lengkap',
             'name.required'=>'Nama lawyer harus diisi',
             'email.required'=>'Email harus diisi',
       );

       // run the validation rules on the inputs from the form
       $validator = Validator::make(Input::all(), $rules,$messages);

      $idfoto= Input::get('id');

       if ($validator->fails()) {
         return Redirect::to('/admin/klien/edit/'.$idfoto)
               ->withErrors($validator);
       } else {
         $nomid=$idfoto;

         }

if(Input::file('foto')!=null){


           $destinationPath = 'uploads/images'; // upload path
           $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
           //$fileName = rand(11111,99999).'.'.$extension; // renameing image
           $fileName = "fotoklien".$nomid.'.'.$extension;
           Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
           // sending back with message


   DB::table('tb_klien')
->where('id', $idfoto)
->update(array(
          'name' => Input::get('name'),
          'email' => Input::get('email'),
          'filefoto' => $fileName,
          'update_time' => date('Y-m-d H:i:s')
    ));
}else{

 DB::table('tb_klien')
->where('id', $idfoto)
->update(array(
  'name' => Input::get('name'),
  'email' => Input::get('email'),
  'username' => Input::get('username'),
  'password' => Input::get('password'),
          'update_time' => date('Y-m-d H:i:s')
    ));
}


  return Redirect::to('/admin/klien/edit/'.$idfoto);

     }

     public function hapusKlien($id){
       $isian=DB::table('tb_klien')
       ->where('id', '=', $id)
       ->first();
                $name=$isian->name;
                $email=$isian->email;
                $filefoto=$isian->filefoto;
                $username=$isian->username;
                $password=$isian->password;

     return View::make('hal_admin.tabel_klien_delete', array('id'=>$id
     ,'name'=>$name
     ,'email' =>$email
     ,'filefoto'=>$filefoto
     ,'username'=>$username
     ,'password'=>$password ));

     }
     public function eraseKlien(){

             $idfoto= Input::get('id');

                 $isian=DB::table('tb_klien')
                 ->where('id', '=', $idfoto)
                 ->first();

                    $namafilegambar=$isian->filefoto;
                    if($namafilegambar!=""){
                    $alamatfile=public_path('uploads/images/'.$namafilegambar);

         echo $alamatfile;
        unlink($alamatfile);
      }
          DB::table('tb_klien')->where('id', '=', $idfoto)->delete();

        return Redirect::to('/admin/klien/list');
     }

     public function allInbox(){
       $dafinbox=DB::table('tb_pesan')
       ->orderBy('id', 'DESC')
       ->get();
       return View::make('hal_admin.mail_inbox', array('dafinbox'=>$dafinbox));
     }


     public function readInbox($id){
       $isipesan=DB::table('tb_pesan')
       ->where('id', '=', $id)
       ->first();
         $filepesan=DB::table('tb_filepesan')
         ->where('idp', '=', $id)
         ->get();

     return View::make('hal_admin.mail_read', array('id'=>$id
     ,'isipesan'=>$isipesan
     ,'filepesan' =>$filepesan));

     }
     function hal_laporan($par_a=null,$par_b=null,$par_c=null){
       $halaman=Request::segment(2)."/".Request::segment(3);
       $con_thn=date('Y');
       $con_bln=date('m');
       $arview=array();
       $elfil=array();
       $needjsv = array();
       $needjsf = array();
       $tipetabel=1;
       $urledit="";
       $urldelete="";
       $urlview="";
       switch($halaman){
         case "laporan/logpanggilan": // tabel log laporan
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;

         $namatabel="Log panggilan";
         $namamenudaricont="Log telepon";
         $dafmenudaricont=array('#'=>'Laporan','null'=>'Log panggilan');
         $dafcol=array('No','Tanggal','Klien','Lama','Biaya');
         $years = DB::table('tb_call_history')
         ->where('idl',getUserInfo("user_roleid"))
         ->where('seconds','!=',0)
         ->distinct()
         ->select([DB::raw('YEAR(start) as tahun')])
         ->orderBy('start','desc')
         ->get();

         //$elemen = new \stdClass();

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                     ,'onchange'=>'ambillistbulan();'
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                            $elfil[]=$elemen;


          $elemen=array(
            'type'=>'select'
            ,'label'=>'Bulan'
            ,'attr'=>array(
            'name'=>'pilbln'
            ,'id'=>'pilbln'
            )
            ,'list'=>array()
            ,'catch'=>null
            ,'onchange'=>'ambildatatabel();'
          );
          if($blnurl!=null){
                 $listbul=array();
            $dafbul=DB::table('tb_call_history')
            ->where('idl',getUserInfo("user_roleid"))
            ->where('seconds','!=',0)
                 ->distinct()
                 ->select([DB::raw('MONTH(start) as bulan')])
                 ->whereRaw('YEAR(start) = ?', [$thnurl])
                 ->get();
                 foreach ($dafbul as $bul) {
                 $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                 }
            $elemen['list']=$listbul;
            $elemen['catch']=$blnurl;
          }

          $elfil[]=$elemen;


                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2s";
                 $isijsf->name="ambillistbulan";
                 $isijsf->url="\"".url('/')."/admin/laporan/jsonlogpanggilan_listbulan/"."\"+isipilthn";
                 $isijsf->var="listbulan";
                 $isijsf->target="pilbln";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabel";
                 $isijsf->url="\"".url('/')."/admin/laporan/logpanggilan/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;
                 if($blnurl==null){
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();ambillistbulan();";
                 $needjsf[]=$isijsf;
                 }else{
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                 $needjsf[]=$isijsf;
                 }
         break;
         case "kontenapps/berita": // tabel berita
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;
         $tipetabel=2;
         $urledit=url('/admin/kontenapps/berita/edit');
         $urldelete=url('/admin/kontenapps/berita/delete');

         $namatabel="Arsip Berita";
         $namamenudaricont="Berita";
         $dafmenudaricont=array('#'=>'Konten apps','null'=>'Berita');
         $dafcol=array('No','Tanggal','Judul');
         $years = DB::table('tb_berita')
         ->select([DB::raw('YEAR(create_at) as tahun')])
         ->orderBy('create_at','desc')
         ->distinct()
         ->get();

         //$elemen = new \stdClass();

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                     ,'onchange'=>'ambillistbulan();'
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                            $elfil[]=$elemen;


          $elemen=array(
            'type'=>'select'
            ,'label'=>'Bulan'
            ,'attr'=>array(
            'name'=>'pilbln'
            ,'id'=>'pilbln'
            )
            ,'list'=>array()
            ,'catch'=>null
            ,'onchange'=>'ambildatatabel();'
          );
          if($blnurl!=null){
                 $listbul=array();
            $dafbul=DB::table('tb_berita')
                 ->select([DB::raw('MONTH(create_at) as bulan')])
                 ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                  ->distinct()
                 ->get();
                 foreach ($dafbul as $bul) {
                 $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                 }
            $elemen['list']=$listbul;
            $elemen['catch']=$blnurl;
          }

          $elfil[]=$elemen;


                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2s";
                 $isijsf->name="ambillistbulan";
                 $isijsf->url="\"".url('/')."/admin/kontenapps/berita/listbulan/"."\"+isipilthn";
                 $isijsf->var="listbulan";
                 $isijsf->target="pilbln";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabel";
                 $isijsf->url="\"".url('/')."/admin/kontenapps/berita/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;
                 if($blnurl==null){
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();ambillistbulan();ambildatatabel();";
                 $needjsf[]=$isijsf;
                 }else{
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                 $needjsf[]=$isijsf;
                 }
         break;
         case "laporan/fee": // tabel laporan fee
         $thn=$con_thn;
         $thnurl=$par_a;

         $namatabel="Laporan fee";
         $namamenudaricont="Laporan fee";
         $dafmenudaricont=array('#'=>'Laporan','null'=>'Laporan fee');
         $dafcol=array('No','Periode','Fee','Share','Tanya Lawyer','Withdraw','Saldo');
         $years = DB::table('tb_mutasi')
         ->where('idl',getUserInfo("user_roleid"))
         ->distinct()
         ->select([DB::raw('YEAR(periode) as tahun')])
         ->orderBy('periode','desc')
         ->get();

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                     ,'onchange'=>'ambildatatabelmutasi();'
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                            $elfil[]=$elemen;


                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabelmutasi";
                 $isijsf->url="\"".url('/')."/admin/laporan/fee/carijson/"."\"+isipilthn";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="ambildatatabelmutasi();";
                 $needjsf[]=$isijsf;


         break;
         case "laporan/transferfee": // tabel  transfer fee
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;

         $namatabel="Log withdraw";
         $namamenudaricont="Log withdraw";
         $dafmenudaricont=array('#'=>'Laporan','null'=>'Tranfer fee');
         $dafcol=array('No','Tanggal minta','Nilai','Status');
         $years = DB::table('tb_withdraw')
         ->where('idl',getUserInfo("user_roleid"))
         ->distinct()
         ->select([DB::raw('YEAR(tanggalminta) as tahun')])
         ->orderBy('tanggalminta','desc')
         ->get();

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                     ,'onchange'=>'ambillistbulan();'
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                            $elfil[]=$elemen;


          $elemen=array(
            'type'=>'select'
            ,'label'=>'Bulan'
            ,'attr'=>array(
            'name'=>'pilbln'
            ,'id'=>'pilbln'
            )
            ,'list'=>array()
            ,'catch'=>null
            ,'onchange'=>'ambildatatabel();'
          );
          if($blnurl!=null){
                 $listbul=array();
                 $dafbul=DB::table('tb_withdraw')
                 ->where('idl',getUserInfo("user_roleid"))
                      ->distinct()
                      ->select([DB::raw('MONTH(tanggalminta) as bulan')])
                      ->whereRaw('YEAR(tanggalminta) = ?', [$thnurl])
                      ->get();
                 foreach ($dafbul as $bul) {
                 $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                 }
            $elemen['list']=$listbul;
            $elemen['catch']=$blnurl;
          }

          $elfil[]=$elemen;


                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2s";
                 $isijsf->name="ambillistbulan";
                 $isijsf->url="\"".url('/')."/admin/laporan/jsontransferfee_listbulan/"."\"+isipilthn";
                 $isijsf->var="listbulan";
                 $isijsf->target="pilbln";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabel";
                 $isijsf->url="\"".url('/')."/admin/laporan/transferfee/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;
                 if($blnurl==null){
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();ambillistbulan();";
                 $needjsf[]=$isijsf;
                 }else{
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                 $needjsf[]=$isijsf;
                 }
         break;
         case "laporan/transferfeeklien": // tabel  transfer fee klien
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;

         $namatabel="Log withdraw";
         $namamenudaricont="Log withdraw";
         $dafmenudaricont=array('#'=>'Laporan','null'=>'Tranfer fee');
         $dafcol=array('No','Tanggal minta','Nilai','Status');
         $years = DB::table('tb_withdrawkomisi')
         ->where('idk',getUserInfo("user_roleid"))
         ->distinct()
         ->select([DB::raw('YEAR(tanggalminta) as tahun')])
         ->orderBy('tanggalminta','desc')
         ->get();

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                     ,'onchange'=>'ambillistbulan();'
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                            $elfil[]=$elemen;


          $elemen=array(
            'type'=>'select'
            ,'label'=>'Bulan'
            ,'attr'=>array(
            'name'=>'pilbln'
            ,'id'=>'pilbln'
            )
            ,'list'=>array()
            ,'catch'=>null
            ,'onchange'=>'ambildatatabel();'
          );
          if($blnurl!=null){
                 $listbul=array();
                 $dafbul=DB::table('tb_withdrawkomisi')
                 ->where('idk',getUserInfo("user_roleid"))
                      ->distinct()
                      ->select([DB::raw('MONTH(tanggalminta) as bulan')])
                      ->whereRaw('YEAR(tanggalminta) = ?', [$thnurl])
                      ->get();
                 foreach ($dafbul as $bul) {
                 $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                 }
            $elemen['list']=$listbul;
            $elemen['catch']=$blnurl;
          }

          $elfil[]=$elemen;


                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2s";
                 $isijsf->name="ambillistbulan";
                 $isijsf->url="\"".url('/')."/admin/laporan/jsontransferfeeklien_listbulan/"."\"+isipilthn";
                 $isijsf->var="listbulan";
                 $isijsf->target="pilbln";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabel";
                 $isijsf->url="\"".url('/')."/admin/laporan/transferfeeklien/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;
                 if($blnurl==null){
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();ambillistbulan();";
                 $needjsf[]=$isijsf;
                 }else{
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                 $needjsf[]=$isijsf;
                 }
         break;
         case "users/manage": //tabel user
         $tipetabel=2;
         $urledit=url('/admin/users/manage/edit');
         $urldelete=url('/admin/users/manage/delete');
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;

         $namatabel="Manage User";
         $namamenudaricont="Manage User";
         $dafmenudaricont=array('null'=>'Users');
         $dafcol=array('No','Nama','Role','Username','Email');


                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabeluser";
                 $isijsf->url="\"".url('/')."/admin/users/manage/carijson\"";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;

                 if($par_a==null){
                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="ambildatatabeluser();";
                 $needjsf[]=$isijsf;
               }else{
               $isijsf = new \stdClass();
               $isijsf->type="main";
               $isijsf->content="pilihselectpilrole();ambildatatabeluserbyrole();";
               $needjsf[]=$isijsf;
               }

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabeluserbyrole";
                 $isijsf->url="\"".url('/')."/admin/users/manage/carijson/\"+isipilrole";
                 $isijsf->var="jsonisitabelbyrole";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;


                 $role[0]="-All-";
                 $role[1]="Buyer";
                 $role[3]="Seller";
                 $role[4]="Admin Operation";
                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Role'
                   ,'attr'=>array(
                   'name'=>'pilrole'
                   ,'id'=>'pilrole'
                   )
                   ,'list'=>$role
                   ,'catch'=>$par_a
                   ,'onchange'=>'ambildatatabeluserbyrole();'
                 );


                 $elfil[]=$elemen;
         break;

         case "lawyers/list": //tabel lawyer
         $tipetabel=2;
         $urledit=url('/admin/lawyers/list/edit');
         $urldelete=url('/admin/lawyers/list/delete');
         $thn=$con_thn;
         $blnurl=$par_a;
         $thnurl=$par_b;

         $namatabel="Daftar Lawyer";
         $namamenudaricont="Manage Lawyer";
         $dafmenudaricont=array('null'=>'Lawyers');
         $dafcol=array('No','Nama','Bidang','Permenit','Email');

                 $isijsv = new \stdClass();
                 $isijsv->name="hasilpiltahun";
                 $needjsv[]=$isijsv;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabellawyer";
                 $isijsf->url="\"".url('/')."/admin/lawyers/list/carijson\"";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="ambildatatabellawyer();";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambillistlawyerbybidang";
                 $isijsf->url="\"".url('/')."/admin/lawyers/list/carijson/\"+isipilbidang";
                 $isijsf->var="jsonlistlawyerbybidang";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;

                 $tb_jenishukum=DB::table('tb_jenishukum')
                      ->select(['id','jenis'])
                      ->get();

                      $isilist=array();
                      $isilist[0]="-all-";
                      foreach($tb_jenishukum as $tbj){
                        $isilist[$tbj->id]=$tbj->jenis;
                      }

                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Bidang hukum'
                   ,'attr'=>array(
                   'name'=>'pilbidang'
                   ,'id'=>'pilbidang'
                   )
                   ,'list'=>$isilist
                   ,'catch'=>null
                   ,'onchange'=>'ambillistlawyerbybidang();'
                 );


                 $elfil[]=$elemen;
         break;
         case "lawyers/billingfee" : // tabel billing fee
         $thn=$con_thn;
         $thnurl=$par_a;
         $idlurl=$par_b;

         $namatabel="Laporan fee";
         $namamenudaricont="Laporan fee";
         $dafmenudaricont=array('#'=>'Laporan','null'=>'Laporan fee');
         $dafcol=array('No','Periode','Lawyer','Fee','Share','Tanya Lawyer','Withdraw','Saldo');
         if($par_b==null){
         $years = DB::table('tb_mutasi')
         ->select([DB::raw('YEAR(periode) as tahun')])
         ->groupBy(DB::raw('YEAR(periode)'))
         ->orderBy('periode','desc')
         ->get();
         }else{
         $years = DB::table('tb_mutasi')
         ->where('idl',$idlurl)
         ->select([DB::raw('YEAR(periode) as tahun')])
         ->groupBy(DB::raw('YEAR(periode)'))
         ->orderBy('periode','desc')
         ->get();
         }

         $arel=array();
         $arel[0]="pilih";
         foreach ($years as $year) {
         $arel[$year->tahun]=$year->tahun;

          }


                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Tahun'
                     ,'attr'=>array(
                     'name'=>'pilthn'
                     ,'id'=>'pilthn'
                     )
                     ,'catch'=>date('Y')
                     ,'list'=>$arel
                   );
                   if($thnurl!=null){
                     $elemen['catch']=$thnurl;
                   }
                   $elfil[]=$elemen;

                 $lawyers = DB::table('tb_lawyer')
                 ->join('tb_user','tb_lawyer.idu','=','tb_user.id')
                 ->select(['tb_lawyer.id as id','tb_user.name as name'])
                 ->get();

                 $arel=array();
                 $arel[0]="-all-";
                 foreach ($lawyers as $l) {
                 $arel[$l->id]=$l->name;
                 }


                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Lawyer'
                   ,'attr'=>array(
                   'name'=>'pillawyer'
                   ,'id'=>'pillawyer'
                   )
                   ,'catch'=>null
                   ,'list'=>$arel
                   ,'onchange'=>'ambildatatabelmutasi();'
                 );

                 if($idlurl!=null){
                   $elemen['catch']=$idlurl;
                 }

                $elfil[]=$elemen;

                 $isijsf = new \stdClass();
                 $isijsf->type="ajax2t";
                 $isijsf->name="ambildatatabelmutasi";
                 $isijsf->url="\"".url('/')."/admin/lawyers/billingfee/carijson/"."\"+isipilthn+\"/\"+isipillawyer";
                 $isijsf->var="jsonisitabel";
                 $isijsf->target="sample_1";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="pilihselectpilthn();pilihselectpillawyer();";
                 $needjsf[]=$isijsf;

                 $isijsf = new \stdClass();
                 $isijsf->type="main";
                 $isijsf->content="ambildatatabelmutasi();";
                 $needjsf[]=$isijsf;
        break;
        case "lawyers/logtelepon": // tabel log telpon lawyers
        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $idlurl=$par_c;

        $namatabel="Log panggilan";
        $namamenudaricont="Log telepon";
        $dafmenudaricont=array('#'=>'Laporan','null'=>'Log panggilan');
        $dafcol=array('No','Tanggal','Lawyer','Klien','Lama','Biaya');

        if($par_c!=null){
        $years = DB::table('tb_call_history')
        ->where('idl',$idlurl)
        ->where('seconds','!=',0)
        ->distinct()
        ->select([DB::raw('YEAR(start) as tahun')])
        ->orderBy('start','desc')
        ->get();
        }else{
        $years = DB::table('tb_call_history')
        ->where('seconds','!=',0)
        ->distinct()
        ->select([DB::raw('YEAR(start) as tahun')])
        ->orderBy('start','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $dafbul=DB::table('tb_call_history')
                ->where('seconds','!=',0)
                     ->select([DB::raw('MONTH(start) as bulan')])
                     ->whereRaw('YEAR(start) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;

         $lawyers = DB::table('tb_lawyer')
         ->join('tb_user','tb_lawyer.idu','=','tb_user.id')
         ->select(['tb_lawyer.id as id','tb_user.name as name'])
         ->get();

         $arel=array();
         $arel[0]="-all-";
         foreach ($lawyers as $l) {
         $arel[$l->id]=$l->name;

          }
         $elemen=array(
           'type'=>'select'
           ,'label'=>'Lawyer'
           ,'attr'=>array(
           'name'=>'pillawyer'
           ,'id'=>'pillawyer'
           )
           ,'catch'=>null
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($idlurl!=null){
           $elemen['catch']=$idlurl;
         }

                  $elfil[]=$elemen;

                $isijsv = new \stdClass();
                $isijsv->name="hasilpiltahun";
                $needjsv[]=$isijsv;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/lawyers/jsonlogtelepon/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/lawyers/logtelepon/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipillawyer";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();";
                $needjsf[]=$isijsf;

                if($blnurl!=null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilbln();pilihselectpillawyer();ambildatatabel();";
                $needjsf[]=$isijsf;


                }else{
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }
        break;
        case "lawyers/rating": // tabel rating lawyers
        $thn=$con_thn;
        $thnurl=$par_a;
        $idlurl=$par_b;

        $namatabel="Tabel kinerja lawyer";
        $namamenudaricont="Laporan Kinerja";
        $dafmenudaricont=array('#'=>'Laporan','null'=>'Laporan kinerja');
        $dafcol=array('No','Periode','Lawyer','Seconds','Fee','Kinerja');
        if($par_b==null){
        $years = DB::table('tb_call_history')
        ->distinct()
        ->select([DB::raw('YEAR(start) as tahun')])
        ->orderBy('start','desc')
        ->get();
        }else{
        $years = DB::table('tb_call_history')
        ->where('idl',$idlurl)
        ->distinct()
        ->select([DB::raw('YEAR(start) as tahun')])
        ->orderBy('start','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;

                           $lawyers = DB::table('tb_lawyer')
                           ->join('tb_user','tb_lawyer.idu','=','tb_user.id')
                           ->select(['tb_lawyer.id as id','tb_user.name as name'])
                           ->get();

                           $arel=array();
                           $arel[0]="-all-";
                           foreach ($lawyers as $l) {
                           $arel[$l->id]=$l->name;

                            }


                                     $elemen=array(
                                       'type'=>'select'
                                       ,'label'=>'Lawyer'
                                       ,'attr'=>array(
                                       'name'=>'pillawyer'
                                       ,'id'=>'pillawyer'
                                       )
                                       ,'catch'=>null
                                       ,'list'=>$arel
                                       ,'onchange'=>'ambildatatabelrating();'
                                     );

                                     if($idlurl!=null){
                                       $elemen['catch']=$idlurl;
                                     }

                                              $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabelrating";
                $isijsf->url="\"".url('/')."/admin/lawyers/rating/carijson/"."\"+isipilthn+\"/\"+isipillawyer";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpillawyer();";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambildatatabelrating();";
                $needjsf[]=$isijsf;
        break;
        case "callcenter/dokumentasi": // tabel dokumentasi
        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=3;
        $urledit=url('/admin/callcenter/dokumentasi/edit');

        if(session('pangkatuser')!=3){
          $tipetabel=5;
          $urlview=url('/admin/callcenter/dokumentasi/view');

        }

        $namatabel="Dokumentasi";
        $namamenudaricont="Dokumentasi";
        $dafmenudaricont=array('#'=>'Dokumentasi','null'=>'Tabel dokumentasi');
        $dafcol=array('No','Tanggal','Status','Atas name','Masalah','Hukum');

        if($statusurl!=null){
        $years = DB::table('tb_dokumentasi')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{
        $years = DB::table('tb_dokumentasi')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_dokumentasi')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;


         $arel=array();
         $arel[-1]="-all-";
         $arel[0]="Pending";
         $arel[1]="Waiting";
         $arel[2]="On progress";
         $arel[3]="Failed";
         $arel[4]="Success";

         $elemen=array(
           'type'=>'select'
           ,'label'=>'Status'
           ,'attr'=>array(
           'name'=>'pilstatus'
           ,'id'=>'pilstatus'
           )
           ,'catch'=>0
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($statusurl!=null){
           $elemen['catch']=$statusurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/callcenter/jsondokumentasi/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/callcenter/dokumentasi/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipilstatus";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpilstatus();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }
        break;
        case "callcenter/orderaktif": // tabel order aktif
        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=4;
        $urledit=url('/admin/callcenter/orderaktif/edit');

        $namatabel="Dokumentasi";
        $namamenudaricont="Dokumentasi";
        $dafmenudaricont=array('#'=>'Order aktif','null'=>'Tabel order');
        $dafcol=array('No','Tanggal','Status','Atas name','Masalah','Hukum');

        if($statusurl!=null){
        $years = DB::table('tb_dokumentasi')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{

        $years = DB::table('tb_dokumentasi')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_dokumentasi')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;


         $arel=array();
         $arel[-1]="-all-";
         $arel[1]="Waiting";
         $arel[2]="On progress";

         $elemen=array(
           'type'=>'select'
           ,'label'=>'Status'
           ,'attr'=>array(
           'name'=>'pilstatus'
           ,'id'=>'pilstatus'
           )
           ,'catch'=>1
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($statusurl!=null){
           $elemen['catch']=$statusurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/callcenter/jsondokumentasi/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/callcenter/orderaktif/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipilstatus";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpilstatus();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }
        break;
        case "callcenter/arsiporder": // tabel arsip order
        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=5;
        $urlview=url('/admin/callcenter/arsiporder/view');

        $namatabel="Dokumentasi";
        $namamenudaricont="Dokumentasi";
        $dafmenudaricont=array('#'=>'Arsip order','null'=>'Tabel arsip order');
        $dafcol=array('No','Tanggal','Status','Atas name','Masalah','Hukum');

        if($statusurl!=null){
        $years = DB::table('tb_dokumentasi')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{
        $years = DB::table('tb_dokumentasi')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_dokumentasi')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;


         $arel=array();
         $arel[-1]="-all-";
         $arel[3]="Failed";
         $arel[4]="Success";

         $elemen=array(
           'type'=>'select'
           ,'label'=>'Status'
           ,'attr'=>array(
           'name'=>'pilstatus'
           ,'id'=>'pilstatus'
           )
           ,'catch'=>0
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($statusurl!=null){
           $elemen['catch']=$statusurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/callcenter/jsondokumentasi/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/callcenter/arsiporder/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipilstatus";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpilstatus();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }
        break;
        case "keuangan/transferfee" : // tabel transfer fee
        $tipetabel=4;
        $urledit=url('/admin/keuangan/transferfee/edit');
        $namatabel="Permintaan withdraw";
        $namamenudaricont="Permintaan withdraw";
        $dafmenudaricont=array('#'=>'Laporan','null'=>'Tranfer fee');
        $dafcol=array('No','Tanggal minta','Lawyer','Nilai','Status','Pesan');



                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/transferfee/carijson\"";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambildatatabel();";
                $needjsf[]=$isijsf;

        break;
		case "keuangan/transferfeeklien" : // tabel transfer fee klien
        $tipetabel=4;
        $urledit=url('/admin/keuangan/transferfeeklien/edit');
        $namatabel="Permintaan withdraw";
        $namamenudaricont="Permintaan withdraw";
        $dafmenudaricont=array('#'=>'Laporan','null'=>'Tranfer fee');
        $dafcol=array('No','Tanggal minta','Klien','Nilai','Status','Pesan');



                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/transferfeeklien/carijson\"";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambildatatabel();";
                $needjsf[]=$isijsf;

        break;
        case "keuangan/pengajuantopup": // tabel pengajuan topup

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=4;
        $urledit=url('/admin/keuangan/pengajuantopup/edit');
        if(session('pangkatuser')!=4){
          $tipetabel=5;
          $urlview=url('/admin/keuangan/pengajuantopup/view');

        }

        $namatabel="Tabel top up";
        $namamenudaricont="Pengajuan top up";
        $dafmenudaricont=array('#'=>'Tabel top up','null'=>'Pengajuan topup');
        $dafcol=array('No','Tanggal','Nama','Jumlah top up','Saldo sebelumnya','Status');

        if($statusurl!=null){
        $years = DB::table('tb_topup')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{
        $years = DB::table('tb_topup')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_topup')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;


         $arel=array();
         $arel[-1]="-all-";
         $arel[0]="Belum konfirm";
         $arel[1]="Sudah konfirm";
         $arel[2]="Ditolak";

         $elemen=array(
           'type'=>'select'
           ,'label'=>'Status'
           ,'attr'=>array(
           'name'=>'pilstatus'
           ,'id'=>'pilstatus'
           )
           ,'catch'=>1
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($statusurl!=null){
           $elemen['catch']=$statusurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/keuangan/jsonpengajuantopup/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/pengajuantopup/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipilstatus";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpilstatus();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }

        break;
        case "keuangan/arsiptopup" : //tabel arsip top up

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=5;
        $urlview=url('/admin/keuangan/arsiptopup/view');

        $namatabel="Tabel arsip top up";
        $namamenudaricont="Arsip top up";
        $dafmenudaricont=array('#'=>'Top up','null'=>'Arsip');
        $dafcol=array('No','Tanggal','Nama','Jumlah top up','Saldo sebelumnya');

        if($statusurl!=null){
        $years = DB::table('tb_topup')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{
        $years = DB::table('tb_topup')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_topup')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                     ->where('status',3)
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;

/**
         $arel=array();
         $arel[-1]="-all-";
         $arel[0]="Belum konfirm";
         $arel[1]="Sudah konfirm";
         $arel[2]="Ditolak";

         $elemen=array(
           'type'=>'select'
           ,'label'=>'Status'
           ,'attr'=>array(
           'name'=>'pilstatus'
           ,'id'=>'pilstatus'
           )
           ,'catch'=>1
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($statusurl!=null){
           $elemen['catch']=$statusurl;
         }

                  $elfil[]=$elemen;
**/
                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/keuangan/jsonarsiptopup/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/arsiptopup/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }

        break;
        case "keuangan/transaksijualbeli" : //tabel transaksi jual beli

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=5;
        $urlview=url('/admin/keuangan/transaksijualbeli/view');

        $namatabel="Tabel transaksi";
        $namamenudaricont="Arsip transaksi";
        $dafmenudaricont=array('#'=>'Keuangan','null'=>'Transaksi jual beli');
        $dafcol=array('No','Tanggal','User Pembeli','User Penjual','Biaya','Pajak','Ongkir','Keuntungan','Status');

        if($statusurl!=null){
        $years = DB::table('tb_transaksi')
        ->where('status',$statusurl)
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }else{
        $years = DB::table('tb_transaksi')
        ->distinct()
        ->select([DB::raw('YEAR(create_at) as tahun')])
        ->orderBy('create_at','desc')
        ->get();
        }

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_transaksi')
                     ->select([DB::raw('MONTH(create_at) as bulan')])
                     ->whereRaw('YEAR(create_at) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/keuangan/transaksijualbeli/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/transaksijualbeli/carijson/"."\"+isipilthn+\"/\"+isipilbln";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }

        break;
        case "keuangan/saldodeposit": //tabel saldo deposit

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $statusurl=$par_c;
        $tipetabel=1;
        $urlview=url('/admin/keuangan/arsiptopup/view');

        $namatabel="Tabel saldo deposit";
        $namamenudaricont="Saldo deposit";
        $dafmenudaricont=array('#'=>'Saldo deposit','null'=>'Tabel');
        $dafcol=array('No','Nama','Jumlah saldo');


                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/saldodeposit/carijson\"";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambildatatabel();";
                $needjsf[]=$isijsf;

        break;
        case "keuangan/arsiptransferfee" : //tabel arsip transfer fee

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $idlurl=$par_c;
        $tipetabel=5;
        $urlview=url('/admin/keuangan/arsiptransferfee/view');

        $namatabel="Tabel arsip withdraw";
        $namamenudaricont="Arsip withdraw";
        $dafmenudaricont=array('#'=>'Withdraw','null'=>'Arsip');
        $dafcol=array('No','Tanggal','Nama','Jumlah withdraw');

        $years = DB::table('tb_withdraw')
        ->distinct()
        ->select([DB::raw('YEAR(tanggalminta) as tahun')])
        ->orderBy('tanggalminta','desc')
        ->get();

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_withdraw')
                     ->select([DB::raw('MONTH(tanggalminta) as bulan')])
                     ->whereRaw('YEAR(tanggalminta) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;

         $lawyers = DB::table('tb_lawyer')
         ->join('tb_user','tb_lawyer.idu','=','tb_user.id')
         ->select(['tb_lawyer.id as id','tb_user.name as name'])
         ->get();

         $arel=array();
         $arel[0]="-all-";
         foreach ($lawyers as $l) {
         $arel[$l->id]=$l->name;

          }
         $elemen=array(
           'type'=>'select'
           ,'label'=>'Lawyer'
           ,'attr'=>array(
           'name'=>'pillawyer'
           ,'id'=>'pillawyer'
           )
           ,'catch'=>null
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($idlurl!=null){
           $elemen['catch']=$idlurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/keuangan/jsonarsiptransferfee/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/arsiptransferfee/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipillawyer";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpillawyer();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }

        break;
        case "keuangan/arsiptransferfeeklien" : //tabel arsip transfer fee klien

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $idlurl=$par_c;
        $tipetabel=5;
        $urlview=url('/admin/keuangan/arsiptransferfeeklien/view');

        $namatabel="Tabel arsip withdraw";
        $namamenudaricont="Arsip withdraw";
        $dafmenudaricont=array('#'=>'Withdraw','null'=>'Arsip');
        $dafcol=array('No','Tanggal','Nama','Jumlah withdraw');

        $years = DB::table('tb_withdrawkomisi')
        ->distinct()
        ->select([DB::raw('YEAR(tanggalminta) as tahun')])
        ->orderBy('tanggalminta','desc')
        ->get();

        $arel=array();
        $arel[0]="pilih";
        foreach ($years as $year) {
        $arel[$year->tahun]=$year->tahun;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Tahun'
                    ,'attr'=>array(
                    'name'=>'pilthn'
                    ,'id'=>'pilthn'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistbulan();'
                  );
                  if($thnurl!=null){
                    $elemen['catch']=$thnurl;
                  }
                           $elfil[]=$elemen;


         $elemen=array(
           'type'=>'select'
           ,'label'=>'Bulan'
           ,'attr'=>array(
           'name'=>'pilbln'
           ,'id'=>'pilbln'
           )
           ,'list'=>array()
           ,'catch'=>null
           ,'onchange'=>'ambildatatabel();'
         );
         if($blnurl!=null){
                $listbul=array();
                $listbul[0]="-all-";
                $dafbul=DB::table('tb_withdrawkomisi')
                     ->select([DB::raw('MONTH(tanggalminta) as bulan')])
                     ->whereRaw('YEAR(tanggalminta) = ?', [$thnurl])
                      ->distinct()
                     ->get();
                foreach ($dafbul as $bul) {
                $listbul[$bul->bulan]=bulanIndo($bul->bulan);
                }
           $elemen['list']=$listbul;
           $elemen['catch']=$blnurl;
         }

         $elfil[]=$elemen;

         $kliens = DB::table('tb_klien')
         ->join('tb_user','tb_klien.idu','=','tb_user.id')
         ->select(['tb_klien.id as id','tb_user.name as name'])
         ->get();

         $arel=array();
         $arel[0]="-all-";
         foreach ($kliens as $l) {
         $arel[$l->id]=$l->name;

          }
         $elemen=array(
           'type'=>'select'
           ,'label'=>'Klien'
           ,'attr'=>array(
           'name'=>'pilklien'
           ,'id'=>'pilklien'
           )
           ,'catch'=>null
           ,'list'=>$arel
           ,'onchange'=>'ambildatatabel();'
         );

         if($idlurl!=null){
           $elemen['catch']=$idlurl;
         }

                  $elfil[]=$elemen;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2s";
                $isijsf->name="ambillistbulan";
                $isijsf->url="\"".url('/')."/admin/keuangan/jsonarsiptransferfeeklien/listbulan/"."\"+isipilthn";
                $isijsf->var="listbulan";
                $isijsf->target="pilbln";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/keuangan/arsiptransferfeeklien/carijson/"."\"+isipilthn+\"/\"+isipilbln+\"/\"+isipilklien";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilthn();pilihselectpilbln();pilihselectpilklien();ambildatatabel();";
                $needjsf[]=$isijsf;

                if($blnurl==null){
                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambillistbulan();";
                $needjsf[]=$isijsf;
                }

        break;
        case "product_category/list" : //tabel kategori produk

        $thn=$con_thn;
        $thnurl=$par_a;
        $blnurl=$par_b;
        $idlurl=$par_c;
        $tipetabel=6;
        $urledit=url('/admin/product_category/list/edit');
        $urldelete=url('/admin/product_category/list/delete');
        $urlview=url('/admin/product_category/list/view');

        $namatabel="Tabel kategori produk";
        $namamenudaricont="Kategori produk";
        $dafmenudaricont=array('#'=>'Produk','null'=>'Kategori');
        $dafcol=array('No','Kategori','Keterangan',"Warna");


                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambildatatabel";
                $isijsf->url="\"".url('/')."/admin/product_category/list/carijson"."\"";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="ambildatatabel();";
                $needjsf[]=$isijsf;


        break;
        case "product/list": // tabel produk
        $thn=$con_thn;
        $paramcat=$par_a;
        $tipetabel=6;
        $urledit=url('/admin/product/list/edit');
        $urldelete=url('/admin/product/list/delete');
        $urlview=url('/admin/product/list/view');

        $namatabel="Produk";
        $namamenudaricont="Tabel Produk";
        $dafmenudaricont=array('#'=>'Toko online','null'=>'Produk');
        $dafcol=array('No','Nama Produk','Kategori','Harga');
        $categories=[];
        $produk = DB::table('tb_product')
        ->where('ids', '=', getUserInfo("user_id"))
        ->orderBy('id','asc')
        ->get();

        //$elemen = new \stdClass();

        foreach ($produk as $p) {

        $jemkategori = DB::table('tb_jem_product_cat')
        ->where('idp', '=', $p->id)
        ->get();

        foreach ($jemkategori as $jk) {
          $kategori = DB::table('tb_product_cat')
          ->where('id', '=', $jk->idpc)
          ->first();

          $categories[]=$kategori;
        }

         }

        $arel=array();
        $arel[0]="pilih";
        foreach ($categories as $cat) {
        $arel[$cat->id]=$cat->category;

         }


                  $elemen=array(
                    'type'=>'select'
                    ,'label'=>'Kategori'
                    ,'attr'=>array(
                    'name'=>'pilcat'
                    ,'id'=>'pilcat'
                    )
                    ,'catch'=>date('Y')
                    ,'list'=>$arel
                    ,'onchange'=>'ambillistproduk();'
                  );
                  if($paramcat!=null){
                    $elemen['catch']=$paramcat;
                  }
                           $elfil[]=$elemen;

                $isijsv = new \stdClass();
                $isijsv->name="hasilpilkategori";
                $needjsv[]=$isijsv;

                $isijsf = new \stdClass();
                $isijsf->type="ajax2t";
                $isijsf->name="ambillistproduk";
                $isijsf->url="\"".url('/')."/admin/product/list/carijson/"."\"+isipilcat";
                $isijsf->var="jsonisitabel";
                $isijsf->target="sample_1";
                $needjsf[]=$isijsf;

                $isijsf = new \stdClass();
                $isijsf->type="main";
                $isijsf->content="pilihselectpilcat();ambillistproduk();";
                $needjsf[]=$isijsf;

        break;
        case "transport_service/list": // tabel jasa transportasi
        $thn=$con_thn;
        $paramcat=$par_a;
        $tipetabel=2;
        $urledit=url('/admin/transport_service/list/edit');
        $urldelete=url('/admin/transport_service/list/delete');

        $namatabel="Transport Provider";
        $namamenudaricont="Transport Provider";
        $dafmenudaricont=array('#'=>'Toko online','null'=>'Transport Provider');
        $dafcol=array('No','Name','Detail');

        $isijsf = new \stdClass();
        $isijsf->type="ajax2t";
        $isijsf->name="gettransportlist";
        $isijsf->url="\"".url('/')."/admin/transport_service/list/carijson"."\"";
        $isijsf->var="jsonisitabel";
        $isijsf->target="sample_1";
        $needjsf[]=$isijsf;

        $isijsf = new \stdClass();
        $isijsf->type="main";
        $isijsf->content="gettransportlist();";
        $needjsf[]=$isijsf;

        break;
        case "laporan/complain": // tabel complain
        $thn=$con_thn;
        $paramcat=$par_a;
        $tipetabel=1;
        $urledit=url('/admin/transport_service/list/edit');
        $urldelete=url('/admin/transport_service/list/delete');

        $namatabel="Laporan Keluhan";
        $namamenudaricont="Laporan Keluhan";
        $dafmenudaricont=array('#'=>'Laporan','null'=>'Laporan Keluhan');
        $dafcol=array('No','Tanggal','Nama','Judul','Pesan');

        $isijsf = new \stdClass();
        $isijsf->type="ajax2t";
        $isijsf->name="getcomplainlist";
        $isijsf->url="\"".url('/')."/admin/laporan/complain/carijson"."\"";
        $isijsf->var="jsonisitabel";
        $isijsf->target="sample_1";
        $needjsf[]=$isijsf;

        $isijsf = new \stdClass();
        $isijsf->type="main";
        $isijsf->content="getcomplainlist();";
        $needjsf[]=$isijsf;

        break;

        // akhir tabel

       }

       foreach ($needjsf as $isijsf){

if(isset($isijsf->var)){
       $isijsv = new \stdClass();
       $isijsv->name=$isijsf->var;
       $needjsv[]=$isijsv;
}
       }

       $arview=array('needjsv'=>$needjsv
       ,'needjsf'=>$needjsf
       ,'dafcol'=>$dafcol
       ,'namatabel'=>$namatabel
       ,'namamenudaricont'=>$namamenudaricont
       ,'dafmenudaricont'=>$dafmenudaricont
       ,'halamandaricont'=>$halaman
       ,'elfil'=>$elfil
       ,'tipetabel'=>$tipetabel
       ,'urledit'=>$urledit
       ,'urldelete'=>$urldelete
       ,'urlview'=>$urlview);


      return View::make('hal_admin.hal_laporan',$arview);

     }
     public function hal_add(){

       $elfil=array();
       $needjsv = array();
       $needjsf = array();
       $butuhdialog=0;

         $halaman=Request::segment(2)."/".Request::segment(3);
         $form_action="";
         switch($halaman){
           case "users/manage": //form add user

           $namatabel="Add User";
           $namamenudaricont="Add User";
           $dafmenudaricont=array(url('/admin/users/manage')=>'Users','null'=>'Add');

                   $isijsf = new \stdClass();
                   $isijsf->type="ajax2t";
                   $isijsf->name="ambildatatabeluser";
                   $isijsf->url="\"".url('/')."/admin/users/manage/carijson\"";
                   $isijsf->var="jsonisitabel";
                   $isijsf->target="sample_1";
                   $needjsf[]=$isijsf;
                   $isijsf = new \stdClass();
                   $isijsf->type="main";
                   $isijsf->content="ambildatatabeluser();";
                   $needjsf[]=$isijsf;

                   //$role[1]="Buyer";
                  // $role[2]="Driver";
                   $role[3]="Seller";
                   $role[4]="Admin Operation";

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Role'
                     ,'attr'=>array(
                     'name'=>'role'
                     ,'id'=>'role'
                     ,'class'=>'form-control'
                     )
                     ,'list'=>$role
                     ,'catch'=>null
                     ,'onchange'=>'ambildatatabel();'
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Address'
                     ,'attr'=>array(
                     'name'=>'address'
                     ,'id'=>'address'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/users/manage/add_confirm');
           break;
           case "lawyers/list": //form add lawyer

           $namatabel="Tambah Lawyer";
           $namamenudaricont="Add Lawyer";
           $dafmenudaricont=array(url('/admin/lawyers/list')=>'Lawyers','null'=>'Add');



                   $tb_jenishukum=DB::table('tb_jenishukum')
                        ->select(['id','jenis'])
                        ->get();

                        $isilist=array();
                        foreach($tb_jenishukum as $tbj){
                          $isilist[$tbj->id]=$tbj->jenis;
                        }

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'bidang[]'
                     ,'id'=>'bidang'
                     ,'class'=>'form-control'
                     ,'multiple'=>'multiple'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>null
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Username is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Phone is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Permenit'
                     ,'attr'=>array(
                     'name'=>'hargapermenit'
                     ,'id'=>'hargapermenit'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Harga permenit is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Persenan (%)'
                     ,'attr'=>array(
                     'name'=>'persenfee'
                     ,'id'=>'persenfee'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Persenan is required')
                      )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Foto is required')
                      )
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/lawyers/list/add_confirm');
           break;
           case "callcenter/dokumentasi": //form add dokumentasi

           $namatabel="Tulis Dokumentasi";
           $namamenudaricont="Tulis Dokumentasi";
           $dafmenudaricont=array(url('/admin/callcenter/dokumentasi')=>'Dokumentasi','null'=>'Add');



                   $tb_listuser=DB::table('tb_user')
                        ->where('level',1)
                        ->select(['id','name'])
                        ->get();

                        $isilist=array();
                        foreach($tb_listuser as $tbu){
                          $isilist[$tbu->id]=$tbu->name;
                        }

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>null
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Kendala'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Kendala is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;
                   $tb_jenishukum=DB::table('tb_jenishukum')
                        ->get();

                        $isilist=array();
                        $isilist[0]="Belum ditentukan";
                        foreach($tb_jenishukum as $tbj){
                          $isilist[$tbj->id]=$tbj->jenis;
                        }
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'idj'
                     ,'id'=>'idj'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>0
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/callcenter/dokumentasi/add_confirm');
           break;
           case "kontenapps/berita": //form add berita

           $namatabel="Tulis Berita";
           $namamenudaricont="Tulis Berita";
           $dafmenudaricont=array(url('/admin/kontenapps/berita')=>'Berita','null'=>'Add');


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Judul'
                     ,'attr'=>array(
                     'name'=>'judul'
                     ,'id'=>'judul'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Judul is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Isi'
                     ,'attr'=>array(
                     'name'=>'isi'
                     ,'id'=>'isi'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isi berita is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/kontenapps/berita/add_confirm');
           break;
           case "product_category/list": //form add kategori produk

           $namatabel="Tambah Kategori Produk";
           $namamenudaricont="Tambah Kategori Produk";
           $dafmenudaricont=array(url('/admin/product_category/list')=>'Tabel Kategori','null'=>'Add');


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Kategori'
                     ,'attr'=>array(
                     'name'=>'kategori'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan Kategori Produk')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Keterangan'
                     ,'attr'=>array(
                     'name'=>'info'
                     ,'id'=>'info'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan info kategori')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Warna'
                     ,'attr'=>array(
                     'name'=>'color'
                     ,'id'=>'color'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan color kategori')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Ikon Kategori'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Ikon harus dipilih')
                      )
                   );
                   $elfil[]=$elemen;


                   $form_action=url('/admin/product_category/list/add_confirm');
           break;
           case "product/list": //form add produk

           $namatabel="Tambah Produk";
           $namamenudaricont="Tambah Produk";
           $dafmenudaricont=array(url('/admin/product/list')=>'Tabel Produk','null'=>'Add');

           $tb_cat=DB::table('tb_product_cat')
                ->select(['id','category'])
                ->get();

                $isilist=array();
                foreach($tb_cat as $cat){
                  $isilist[$cat->id]=$cat->category;
                }

           $elemen=array(
             'type'=>'select'
             ,'label'=>'Kategori'
             ,'attr'=>array(
             'name'=>'kategori[]'
             ,'id'=>'kategori'
             ,'class'=>'form-control'
             ,'multiple'=>'multiple'
             )
             ,'selek2'=>''
             ,'list'=>$isilist
             ,'catch'=>null
           );
           $elfil[]=$elemen;


            $elemen=array(
              'type'=>'text'
              ,'label'=>'Nama Produk'
              ,'attr'=>array(
              'name'=>'name'
              ,'id'=>'name'
              ,'class'=>'form-control'
              ,'required'=>''
              )
              ,'validators'=>array(
                'notEmpty'=>array('message'=>'Isikan Nama Produk')
              )
            );
            $elfil[]=$elemen;

            $elemen=array(
              'type'=>'text'
              ,'label'=>'Harga Produk'
              ,'attr'=>array(
              'name'=>'price'
              ,'id'=>'price'
              ,'class'=>'form-control'
              ,'required'=>''
              )
              ,'validators'=>array(
                'notEmpty'=>array('message'=>'Isikan Harga Produk')
              )
            );
            $elfil[]=$elemen;

            $elemen=array(
              'type'=>'unik_fotoprofil'
              ,'label'=>'Foto Produk'
              ,'attr'=>array(
              'name'=>'foto'
              ,'id'=>'foto'
              ,'class'=>'form-control'
              ,'required'=>''
              )
              ,'validators'=>array(
                'notEmpty'=>array('message'=>'Foto harus dipilih')
               )
            );
            $elfil[]=$elemen;

            $elemen=array(
              'type'=>'textarea'
              ,'label'=>'Keterangan Produk'
              ,'attr'=>array(
              'name'=>'info'
              ,'id'=>'info'
              ,'class'=>'form-control'
              ,'required'=>''
              )
              ,'validators'=>array(
                'notEmpty'=>array('message'=>'Isikan Keterangan Produk')
              )
            );
            $elfil[]=$elemen;


                   $form_action=url('/admin/product/list/add_confirm');
           break;
           case "transport_service/list": //form add jasa transportasi

           $namatabel="Add Transport Service";
           $namamenudaricont="Add Transport Service";
           $dafmenudaricont=array(url('/admin/transport_service/list')=>'Transport Service','null'=>'Add');


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Name'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan nama jasa transportasi')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Detail'
                     ,'attr'=>array(
                     'name'=>'detail'
                     ,'id'=>'detail'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan keterangan tentang jasa ini')
                     )
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/transport_service/list/add_confirm');
           break;

           // akhir form add

         }

         foreach ($needjsf as $isijsf){

  if(isset($isijsf->var)){
         $isijsv = new \stdClass();
         $isijsv->name=$isijsf->var;
         $needjsv[]=$isijsv;
  }
         }
         $arview=array('needjsv'=>$needjsv
         ,'needjsf'=>$needjsf
         ,'namatabel'=>$namatabel
         ,'namamenudaricont'=>$namamenudaricont
         ,'dafmenudaricont'=>$dafmenudaricont
         ,'halamandaricont'=>$halaman
         ,'form_action'=>$form_action
         ,'butuhdialog'=>$butuhdialog
         ,'elfil'=>$elfil);
      return View::make('hal_admin.hal_form',$arview);
     }

     public function hal_edit($par_a=null){

       $elfil=array();
       $needjsv = array();
       $needjsf = array();
       $butuhdialog=0;
       $tipeform=3;

         $halaman=Request::segment(2)."/".Request::segment(3);
         $form_action="";
         switch($halaman){
           case "users/manage": //form edit user
           $idu=$par_a;
           $tabeluser=DB::table('tb_user')
           ->where('id', '=', $idu)
           ->first();

           $namatabel="Edit User";
           $namamenudaricont="Edit User";
           $dafmenudaricont=array(url('/admin/users/manage')=>'Users','null'=>'Edit');

                   $isijsf = new \stdClass();
                   $isijsf->type="ajax2t";
                   $isijsf->name="ambildatatabeluser";
                   $isijsf->url="\"".url('/')."/admin/users/manage/carijson\"";
                   $isijsf->var="jsonisitabel";
                   $isijsf->target="sample_1";
                   $needjsf[]=$isijsf;
                   $isijsf = new \stdClass();
                   $isijsf->type="main";
                   $isijsf->content="ambildatatabeluser();";
                   $needjsf[]=$isijsf;

                   $role[1]="Klien";
                   $role[2]="Lawyer";
                   $role[3]="Customer Service";
                   $role[4]="Finance";
                   $role[5]="Direksi";
                   $role[6]="Admin Operation";

                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idakun'
                     ,'id'=>'idakun'
                     ,'value'=>$tabeluser->id
                     )
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Role'
                     ,'attr'=>array(
                     'name'=>'role'
                     ,'id'=>'role'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>idrole($tabeluser->level)
                     )
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Name'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->name
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->email
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->username
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->phone
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Address'
                     ,'attr'=>array(
                     'name'=>'address'
                     ,'id'=>'address'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->address
                     )
                   );
                   $elfil[]=$elemen;

                   if($tabeluser->level==1 || $tabeluser->level==2){
                   $tb_rekening=DB::table('tb_rekening')
                   ->where('idu', '=', $idu)
                   ->first();

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Bank'
                     ,'attr'=>array(
                     'name'=>'namabank'
                     ,'id'=>'namabank'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_rekening->namabank
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Rekening'
                     ,'attr'=>array(
                     'name'=>'nomrek'
                     ,'id'=>'nomrek'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_rekening->nomrek
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Atas name'
                     ,'attr'=>array(
                     'name'=>'atasnama'
                     ,'id'=>'atasnama'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_rekening->atasnama
                     )
                   );
                   $elfil[]=$elemen;
                   }

                   $elemen=array(
                     'type'=>'checkbox'
                     ,'label'=>'Reset password?'
                     ,'attr'=>array(
                     'name'=>'resetsandi'
                     ,'id'=>'resetsandi'
                     ,'value'=>1
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'checkbox'
                     ,'label'=>'Lock?'
                     ,'attr'=>array(
                     'name'=>'lock'
                     ,'id'=>'lock'
                     ,'value'=>1
                     )
                   );
                   if($tabeluser->status!=1){
                     $elemen["attr"]["checked"]="";
                   }
                   $elfil[]=$elemen;

                   $form_action=url('/admin/users/manage/save_update');
           break;
           case "lawyers/list": //form edit lawyer
           $id=$par_a;
           $namatabel="Edit Lawyer";
           $namamenudaricont="Edit Lawyer";
           $dafmenudaricont=array(url('/admin/lawyers/list')=>'Lawyers','null'=>'Edit');

        $form_action=url('/admin/lawyers/list/save_update');
           $isian=DB::table('tb_lawyer')
           ->where('id', '=', $id)
           ->first();

           $tb_user=DB::table('tb_user')
           ->where('id', '=', $isian->idu)
           ->first();

           $name=$tb_user->name;
           $email=$tb_user->email;
           $filefoto=$tb_user->filefoto;
           $username=$tb_user->username;
           $password=$tb_user->password;
           $phone=$tb_user->phone;
           $persenfee=$isian->persenfee;
           $hargapermenit=$isian->permenit;
           $firmlatitude=$isian->firmlatitude;
           $firmlongitude=$isian->firmlongitude;
           $tampilkanposisi=$isian->tampilkanposisi;


           $dafmapjen=DB::table('tb_jem_lawyer')
           ->where('idl', '=', $id)
           ->select(['id','idj'])
           ->get();

           $kategori = new \stdClass();
           $i=0;
           $listval=array();
           foreach ($dafmapjen as $map) {
           $i+=1;
                 $cat=DB::table('tb_jenishukum')
                 ->where('id', '=', $map->idj)
                 ->first();
                 $isikat = new \stdClass();
                 $isikat->id=$cat->id;
                 $isikat->jenis=$cat->jenis;
                 $kategori->$i=$isikat;
                 $listval[]=$cat->id;

           }

           $tb_jenishukum=DB::table('tb_jenishukum')
              ->select(['id','jenis'])
              ->get();

              $isilist=array();
              foreach($tb_jenishukum as $tbj){
                $isilist[$tbj->id]=$tbj->jenis;
              }

                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$id
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'bidang[]'
                     ,'id'=>'bidang'
                     ,'class'=>'form-control'
                     ,'value'=>'1'
                     ,'multiple'=>'multiple'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$listval
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$name
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$email
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$username
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Username is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$phone
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Phone is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Permenit'
                     ,'attr'=>array(
                     'name'=>'hargapermenit'
                     ,'id'=>'hargapermenit'
                     ,'required'=>''
                     ,'class'=>'form-control rupiahjs'
                     ,'value'=>rupiah($hargapermenit)
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Harga permenit is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Persenan (%)'
                     ,'attr'=>array(
                     'name'=>'persenfee'
                     ,'id'=>'persenfee'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$persenfee
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Persenan is required')
                      )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     )
                     ,'src'=>$filefoto
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Firm latitude'
                     ,'attr'=>array(
                     'name'=>'firmlatitude'
                     ,'id'=>'firmlatitude'
                     ,'class'=>'form-control'
                     ,'value'=>$firmlatitude
                     )
                   );

                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Firm longitude'
                     ,'attr'=>array(
                     'name'=>'firmlongitude'
                     ,'id'=>'firmlongitude'
                     ,'class'=>'form-control'
                     ,'value'=>$firmlongitude
                     )
                   );

                   $elfil[]=$elemen;

                   $elemena=array(
                     'type'=>'checkbox'
                     ,'label'=>'Tampilkan posisi lawyer'
                     ,'attr'=>array(
                     'name'=>'posisilawyer'
                     ,'id'=>'posisilawyer'
                     ,'value'=>1
                     )
                   );

                   $elemenb=array(
                     'type'=>'checkbox'
                     ,'label'=>'Tampilkan posisi firm'
                     ,'attr'=>array(
                     'name'=>'posisifirm'
                     ,'id'=>'posisifirm'
                     ,'value'=>1
                     )
                   );
                   if($tampilkanposisi==1){
                     $elemena["attr"]["checked"]="";
                   }else if($tampilkanposisi==2){
                     $elemena["attr"]["checked"]="";
                     $elemenb["attr"]["checked"]="";
                   }else if($tampilkanposisi==3){
                     $elemenb["attr"]["checked"]="";
                   }
                   $elfil[]=$elemena;
                   $elfil[]=$elemenb;

           break;
           case "callcenter/dokumentasi": //form edit dokumentasi
           $idd=$par_a;
           $tabeldokumen=DB::table('tb_dokumentasi')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Edit Dokumentasi";
           $namamenudaricont="Edit Dokumentasi";
           $dafmenudaricont=array(url('/admin/callcenter/dokumentasi')=>'Dokumentasi','null'=>'Edit');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $tb_listuser=DB::table('tb_user')
                        ->where('level',1)
                        ->select(['id','name'])
                        ->get();

                        $isilist=array();
                        foreach($tb_listuser as $tbu){
                          $isilist[$tbu->id]=$tbu->name;
                        }

                        $tb_klien=DB::table('tb_klien')
                                      ->where('id',$tabeldokumen->idk)
                                      ->select(['idu'])
                                      ->first();

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Klien'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$tb_klien->idu
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Kendala'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeldokumen->masalah
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Kendala is required and cannot be empty')
                     )
                   );

                   $elfil[]=$elemen;

                   $tb_jenishukum=DB::table('tb_jenishukum')
                        ->get();

                        $isilist=array();
                        $isilist[0]="Belum ditentukan";
                        foreach($tb_jenishukum as $tbj){
                          $isilist[$tbj->id]=$tbj->jenis;
                        }
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'idj'
                     ,'id'=>'idj'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$tabeldokumen->idj
                   );


                   $form_action=url('/admin/callcenter/dokumentasi/save_update');
           break;
           case "callcenter/orderaktif": //form edit order aktif
           $idd=$par_a;
           $butuhdialog=1;


           $tabeldokumen=DB::table('tb_dokumentasi')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Update order aktif";
           $namamenudaricont="Update order aktif";
           $dafmenudaricont=array(url('/admin/callcenter/orderaktif')=>'Order aktif','null'=>'Update');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Tanggal'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabeldokumen->tanggal
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                        $tb_user=DB::table('tb_user')
                             ->where('idr',$tabeldokumen->idk)
                             ->select(['id','name'])
                             ->first();

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Klien'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_user->name
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Kendala'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeldokumen->masalah
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Kendala is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   if($tabeldokumen->status==1){
                   $tb_jenishukum=DB::table('tb_jenishukum')
                        ->get();

                        $isilist=array();
                        $isilist[0]="Belum ditentukan";
                        foreach($tb_jenishukum as $tbj){
                          $isilist[$tbj->id]=$tbj->jenis;
                        }
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'idj'
                     ,'id'=>'idj'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$tabeldokumen->idj
                   );
                   $elfil[]=$elemen;
                 }else{
                   $tb_jenishukum=DB::table('tb_jenishukum')
                       ->where('id',$tabeldokumen->idj)
                       ->select(['jenis'])
                       ->first();

              $elemen=array(
                'type'=>'text'
                ,'label'=>'Bidang hukum'
                ,'attr'=>array(
                'name'=>'nama22'
                ,'id'=>'nama22'
                ,'class'=>'form-control'
                ,'value'=>$tb_jenishukum->jenis
                ,'disabled'=>''
                )
              );
              $elfil[]=$elemen;
                 }

                        $arstatus=array();
                        $arstatus[1]="Waiting";
                        $arstatus[2]="On progress";
                        $arstatus[3]="Failed";
                        $arstatus[4]="Success";
                        if($tabeldokumen->status==2){
                        $arstatus=array();
                        $arstatus[3]="Failed";
                        $arstatus[4]="Success";
                        }

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Progress'
                     ,'attr'=>array(
                     'name'=>'status'
                     ,'id'=>'status'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$arstatus
                     ,'catch'=>$tabeldokumen->status
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/callcenter/orderaktif/save_update');
           break;
           case "keuangan/pengajuantopup" : //form edit topup
           $idt=$par_a;
           $butuhdialog=1;

           $tabeltopup=DB::table('tb_topup')
           ->where('id', '=', $idt)
           ->first();

           if($tabeltopup->status>=2){
             $tipeform=5;
           }

           $namatabel="Validasi top up";
           $namamenudaricont="Validasi top up";
           $dafmenudaricont=array(url('/admin/keuangan/pengajuantopup')=>'Pengajuan top up','null'=>'Validasi');

           if($tabeltopup->status<2){
                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idt'
                     ,'id'=>'idt'
                     ,'value'=>$idt
                     )
                   );
                   $elfil[]=$elemen;
          }

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Tanggal'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabeltopup->create_at
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                        $tb_user=DB::table('tb_user')
                             ->where('id',$tabeltopup->idu)
                             ->select(['id','name'])
                             ->first();

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Klien'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_user->name
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                    $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Jumlah'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>rupiah($tabeltopup->topup_amount)
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                   if($tabeltopup->buktitransfer!=null){
                   $elemen=array(
                    'type'=>'img'
                    ,'label'=>'Bukti transfer'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    )
                    ,'src'=>$tabeltopup->buktitransfer
                  );
                  $elfil[]=$elemen;
                  }

                   if($tabeltopup->user_comment!=""){
                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan klien'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$tabeltopup->user_comment
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                        $arstatus=array();
                        $arstatus[2]="Ditolak";
                        $arstatus[3]="Diterima";

                    if($tabeltopup->status<2){
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'status'
                     ,'id'=>'status'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$arstatus
                     ,'catch'=>$tabeltopup->status
                   );
                   $elfil[]=$elemen;
                 }else{
                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$arstatus[$tabeltopup->status]
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan dari finance'
                     ,'attr'=>array(
                     'name'=>'admin_comment'
                     ,'id'=>'admin_comment'
                     ,'class'=>'form-control'
                     )
                   );

                  if($tabeltopup->status>=2){
                    $elemen['attr']['disabled']="";
                    $elemen['attr']['value']=$tabeltopup->admin_comment;
                  }
                   $elfil[]=$elemen;

                   $form_action=url('/admin/keuangan/pengajuantopup/save_update');
           break;
           case "keuangan/transferfee" : //form edit transfer fee lawyer
           $idw=$par_a;
           $butuhdialog=1;

           $tabelwithdraw=DB::table('tb_withdraw')
           ->where('id', '=', $idw)
           ->first();

           if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
             $tipeform=5;
           }

           $namatabel="Validasi withdraw";
           $namamenudaricont="Validasi withdraw";
           $dafmenudaricont=array(url('/admin/keuangan/transferfee')=>'Pengajuan withdraw','null'=>'Validasi');

           if($tabelwithdraw->status<2){
                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idw'
                     ,'id'=>'idw'
                     ,'value'=>$idw
                     )
                   );
                   $elfil[]=$elemen;
          }

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Tanggal'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelwithdraw->tanggalminta
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $tb_user=DB::table('tb_user')
                        ->where('idr',$tabelwithdraw->idl)
                        ->select(['id','name'])
                        ->first();

                     $tb_lawyer=DB::table('tb_lawyer')
                         ->where('id',$tabelwithdraw->idl)
                         ->select(['saldo'])
                         ->first();


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Lawyer'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_user->name
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                    $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Saldo'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>rupiah($tb_lawyer->saldo)
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                    $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Jumlah withdraw'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>rupiah($tabelwithdraw->nilai)
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $tb_rekening=DB::table('tb_rekening')
                   ->where('idu', '=', $tb_user->id)
                   ->first();

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Bank'
                     ,'attr'=>array(
                     'name'=>'namabank'
                     ,'id'=>'namabank'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->namabank
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Rekening'
                     ,'attr'=>array(
                     'name'=>'nomrek'
                     ,'id'=>'nomrek'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->nomrek
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Atas name'
                     ,'attr'=>array(
                     'name'=>'atasnama'
                     ,'id'=>'atasnama'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->atasnama
                     )
                   );
                   $elfil[]=$elemen;

                   if($tabelwithdraw->komenlawyer!=""){
                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan lawyer'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelwithdraw->komenlawyer
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                        $arstatus=array();
                        $arstatus[1]="Diproses";
                        $arstatus[2]="Ditolak";
                        $arstatus[3]="Selesai";

                    if($tabelwithdraw->status==0 || $tabelwithdraw->status==1){
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'status'
                     ,'id'=>'status'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$arstatus
                     ,'catch'=>$tabelwithdraw->status
                   );
                   $elfil[]=$elemen;
                 }else{
                 $arstatus[-1]="Dibatalkan";
                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$arstatus[$tabelwithdraw->status]
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan dari finance'
                     ,'attr'=>array(
                     'name'=>'admin_comment'
                     ,'id'=>'admin_comment'
                     ,'class'=>'form-control'
                     )
                   );
                   if($tabelwithdraw->status==1){
                     $elemen['attr']['value']=$tabelwithdraw->admin_comment;
                   }else if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
                    $elemen['attr']['disabled']="";
                    $elemen['attr']['value']=$tabelwithdraw->admin_comment;
                   }
                   $elfil[]=$elemen;

                   $form_action=url('/admin/keuangan/transferfee/save_update');
           break;
           case "keuangan/transferfeeklien" : //form edit transfer fee klien
           $idw=$par_a;
           $butuhdialog=1;

           $tabelwithdraw=DB::table('tb_withdrawkomisi')
           ->where('id', '=', $idw)
           ->first();

           if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
             $tipeform=5;
           }

           $namatabel="Validasi withdraw";
           $namamenudaricont="Validasi withdraw";
           $dafmenudaricont=array(url('/admin/keuangan/transferfee')=>'Pengajuan withdraw','null'=>'Validasi');

           if($tabelwithdraw->status<2){
                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idw'
                     ,'id'=>'idw'
                     ,'value'=>$idw
                     )
                   );
                   $elfil[]=$elemen;
          }

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Tanggal'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelwithdraw->tanggalminta
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $tb_user=DB::table('tb_user')
                        ->where('idr',$tabelwithdraw->idk)
                        ->select(['id','name'])
                        ->first();

                     $tb_klien=DB::table('tb_klien')
                         ->where('id',$tabelwithdraw->idk)
                         ->select(['komisi'])
                         ->first();


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Klien'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_user->name
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                    $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Komisi'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>rupiah($tb_klien->komisi)
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                    $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Jumlah withdraw'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>rupiah($tabelwithdraw->nilai)
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $tb_rekening=DB::table('tb_rekening')
                   ->where('idu', '=', $tb_user->id)
                   ->first();

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Bank'
                     ,'attr'=>array(
                     'name'=>'namabank'
                     ,'id'=>'namabank'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->namabank
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Rekening'
                     ,'attr'=>array(
                     'name'=>'nomrek'
                     ,'id'=>'nomrek'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->nomrek
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Atas name'
                     ,'attr'=>array(
                     'name'=>'atasnama'
                     ,'id'=>'atasnama'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tb_rekening->atasnama
                     )
                   );
                   $elfil[]=$elemen;

                   if($tabelwithdraw->user_comment!=""){
                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan klien'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelwithdraw->user_comment
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                        $arstatus=array();
                        $arstatus[1]="Diproses";
                        $arstatus[2]="Ditolak";
                        $arstatus[3]="Selesai";

                    if($tabelwithdraw->status==0 || $tabelwithdraw->status==1){
                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'status'
                     ,'id'=>'status'
                     ,'class'=>'form-control'
                     )
                     ,'selek2'=>''
                     ,'list'=>$arstatus
                     ,'catch'=>$tabelwithdraw->status
                   );
                   $elfil[]=$elemen;
                 }else{
                 $arstatus[-1]="Dibatalkan";
                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Status'
                     ,'attr'=>array(
                     'name'=>'masalah'
                     ,'id'=>'masalah'
                     ,'class'=>'form-control'
                     ,'value'=>$arstatus[$tabelwithdraw->status]
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;
                 }

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Pesan dari finance'
                     ,'attr'=>array(
                     'name'=>'admin_comment'
                     ,'id'=>'admin_comment'
                     ,'class'=>'form-control'
                     )
                   );
                   if($tabelwithdraw->status==1){
                     $elemen['attr']['value']=$tabelwithdraw->admin_comment;
                   }else if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
                    $elemen['attr']['disabled']="";
                    $elemen['attr']['value']=$tabelwithdraw->admin_comment;
                   }
                   $elfil[]=$elemen;

                   $form_action=url('/admin/keuangan/transferfeeklien/save_update');
           break;
           case "kontenapps/berita": //form edit berita
           $idd=$par_a;
           $tabelberita=DB::table('tb_berita')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Edit Berita";
           $namamenudaricont="Edit Berita";
           $dafmenudaricont=array(url('/admin/kontenapps/berita')=>'Berita','null'=>'Edit');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Judul'
                     ,'attr'=>array(
                     'name'=>'judul'
                     ,'id'=>'judul'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelberita->judul
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Judul is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Isi'
                     ,'attr'=>array(
                     'name'=>'isi'
                     ,'id'=>'isi'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabelberita->isi
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isi berita is required and cannot be empty')
                     )
                   );

                   $elfil[]=$elemen;


                   $form_action=url('/admin/kontenapps/berita/save_update');
           break;
           case "product_category/list": //form edit kategori produk
           $idd=$par_a;
           $tabelkategori=DB::table('tb_product_cat')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Edit Kategori Produk";
           $namamenudaricont="Edit Kategori Produk";
           $dafmenudaricont=array(url('/admin/product_category/list')=>'Kategori Produk','null'=>'Edit');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Kategori'
                     ,'attr'=>array(
                     'name'=>'kategori'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelkategori->category
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Kategori belum dimasukkan')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Keterangan'
                     ,'attr'=>array(
                     'name'=>'info'
                     ,'id'=>'info'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelkategori->info
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan info kategori')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Warna'
                     ,'attr'=>array(
                     'name'=>'color'
                     ,'id'=>'color'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelkategori->color
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan color kategori')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     )
                     ,'src'=>$tabelkategori->icon
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/product_category/list/save_update');
           break;
           case "product/list": //form edit produk
           $idd=$par_a;
           $tabelproduk=DB::table('tb_product')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Edit Produk";
           $namamenudaricont="Edit Produk";
           $dafmenudaricont=array(url('/admin/product/list')=>'Tabel Produk','null'=>'Edit');

           $dafmapjen=DB::table('tb_jem_product_cat')
           ->where('idp', '=', $idd)
           ->select(['id','idpc'])
           ->get();

           $kategori = new \stdClass();
           $i=0;
           $listval=array();
           foreach ($dafmapjen as $map) {
           $i+=1;
                 $cat=DB::table('tb_product_cat')
                 ->where('id', '=', $map->idpc)
                 ->first();
                 $isikat = new \stdClass();
                 $isikat->id=$cat->id;
                 $isikat->name=$cat->category;
                 $kategori->$i=$isikat;
                 $listval[]=$cat->id;

           }

           $tb_jenishukum=DB::table('tb_product_cat')
              ->select(['id','category'])
              ->get();

              $isilist=array();
              foreach($tb_jenishukum as $tbj){
                $isilist[$tbj->id]=$tbj->category;
              }

                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Kategori'
                     ,'attr'=>array(
                     'name'=>'kategori[]'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'value'=>'1'
                     ,'multiple'=>'multiple'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$listval
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelproduk->name
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan name produk')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Harga'
                     ,'attr'=>array(
                     'name'=>'price'
                     ,'id'=>'price'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelproduk->price
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan price produk')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Keterangan'
                     ,'attr'=>array(
                     'name'=>'info'
                     ,'id'=>'info'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelproduk->info
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan info produk')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto Produk'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     )
                     ,'src'=>$tabelproduk->mainpict
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/product/list/save_update');
           break;
           case "transport_service/list": //form edit jasa transportasi
           $idd=$par_a;
           $tabelservice=DB::table('tb_transport_service')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Edit Transport Service";
           $namamenudaricont="Edit Transport Service";
           $dafmenudaricont=array(url('/admin/transport_service/list')=>'Transport Service','null'=>'Edit');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idd'
                     ,'id'=>'idd'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Name'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelservice->name
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isian nama transport service belum dimasukkan')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Detail'
                     ,'attr'=>array(
                     'name'=>'detail'
                     ,'id'=>'detail'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelservice->detail
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan keterangan')
                     )
                   );
                   $elfil[]=$elemen;


                   $form_action=url('/admin/transport_service/list/save_update');
           break;

           //akhir form edit

         }

         foreach ($needjsf as $isijsf){

  if(isset($isijsf->var)){
         $isijsv = new \stdClass();
         $isijsv->name=$isijsf->var;
         $needjsv[]=$isijsv;
  }
         }
         $arview=array('needjsv'=>$needjsv
         ,'needjsf'=>$needjsf
         ,'namatabel'=>$namatabel
         ,'namamenudaricont'=>$namamenudaricont
         ,'dafmenudaricont'=>$dafmenudaricont
         ,'halamandaricont'=>$halaman
         ,'form_action'=>$form_action
         ,'elfil'=>$elfil
         ,'butuhdialog'=>$butuhdialog
          ,'tipeform'=>$tipeform);
      return View::make('hal_admin.hal_form',$arview);
     }
     public function hal_view($par_a=null){
       $elfil=array();
       $needjsv = array();
       $needjsf = array();
       $butuhdialog=0;
       $tipeform=5;
       $halaman=Request::segment(2)."/".Request::segment(3);
       switch($halaman){
         case "callcenter/arsiporder": //form view arsip order
         $idd=$par_a;
         $butuhdialog=1;


         $tabeldokumen=DB::table('tb_dokumentasi')
         ->where('id', '=', $idd)
         ->first();

         $namatabel="Detail order";
         $namamenudaricont="Detail order";
         $dafmenudaricont=array(url('/admin/callcenter/arsiporder')=>'Arsip order','null'=>'Detail');


                 $elemen=array(
                   'type'=>'hidden'
                   ,'attr'=>array(
                   'name'=>'idd'
                   ,'id'=>'idd'
                   ,'value'=>$idd
                   )
                 );
                 $elfil[]=$elemen;

                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Tanggal'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tabeldokumen->tanggal
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                      $tb_user=DB::table('tb_user')
                           ->where('idr',$tabeldokumen->idk)
                           ->select(['id','name'])
                           ->first();

                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Klien'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tb_user->name
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 $elemen=array(
                   'type'=>'richtext'
                   ,'label'=>'Kendala'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'required'=>''
                   ,'class'=>'form-control'
                   ,'value'=>$tabeldokumen->masalah
                   ,'disabled'=>''
                   )
                   ,'validators'=>array(
                     'notEmpty'=>array('message'=>'Kendala is required and cannot be empty')
                   )
                 );
                 $elfil[]=$elemen;

                 if($tabeldokumen->status==1){
                 $tb_jenishukum=DB::table('tb_jenishukum')
                      ->get();

                      $isilist=array();
                      $isilist[0]="Belum ditentukan";
                      foreach($tb_jenishukum as $tbj){
                        $isilist[$tbj->id]=$tbj->jenis;
                      }
                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Bidang hukum'
                   ,'attr'=>array(
                   'name'=>'idj'
                   ,'id'=>'idj'
                   ,'class'=>'form-control'
                   )
                   ,'selek2'=>''
                   ,'list'=>$isilist
                   ,'catch'=>$tabeldokumen->idj
                 );
                 $elfil[]=$elemen;
               }else{
                 $tb_jenishukum=DB::table('tb_jenishukum')
                     ->where('id',$tabeldokumen->idj)
                     ->select(['jenis'])
                     ->first();

            $elemen=array(
              'type'=>'text'
              ,'label'=>'Bidang hukum'
              ,'attr'=>array(
              'name'=>'nama22'
              ,'id'=>'nama22'
              ,'class'=>'form-control'
              ,'value'=>$tb_jenishukum->jenis
              ,'disabled'=>''
              )
            );
            $elfil[]=$elemen;
               }

                      $arstatus=array();
                      $arstatus[1]="Waiting";
                      $arstatus[2]="On progress";
                      $arstatus[3]="Failed";
                      $arstatus[4]="Success";
                      if($tabeldokumen->status==2){
                      $arstatus=array();
                      $arstatus[3]="Failed";
                      $arstatus[4]="Success";
                      }


                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Progress'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$arstatus[$tabeldokumen->status]
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 $form_action=url('/admin/callcenter/orderaktif/save_update');
         break;
         case "keuangan/arsiptopup": //form view arsip top up

         $idt=$par_a;
         $butuhdialog=1;

         $tabeltopup=DB::table('tb_topup')
         ->where('id', '=', $idt)
         ->first();


         $namatabel="Detail top up";
         $namamenudaricont="Detail top up";
         $dafmenudaricont=array(url('/admin/keuangan/arsiptopup')=>'Arsip top up','null'=>'Detail');

         if($tabeltopup->status<2){
                 $elemen=array(
                   'type'=>'hidden'
                   ,'attr'=>array(
                   'name'=>'idt'
                   ,'id'=>'idt'
                   ,'value'=>$idt
                   )
                 );
                 $elfil[]=$elemen;
        }

                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Tanggal'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tabeltopup->create_at
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                      $tb_user=DB::table('tb_user')
                           ->where('idr',$tabeltopup->idu)
                           ->select(['id','name'])
                           ->first();

                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Klien'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tb_user->name
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                  $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Jumlah'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>rupiah($tabeltopup->topup_amount)
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 $tb_metode_topup=DB::table('tb_metode_topup')
                 ->where('id', '=', $tabeltopup->idmt)
                 ->first();

                 $elemen=array(
                  'type'=>'text'
                  ,'label'=>'Metode bayar'
                  ,'attr'=>array(
                  'name'=>'nama22'
                  ,'id'=>'nama22'
                  ,'class'=>'form-control'
                  ,'value'=>$tb_metode_topup->metode." (".$tb_metode_topup->nomakun.")"
                  ,'disabled'=>''
                  )
                );
                $elfil[]=$elemen;

                 if($tabeltopup->buktitransfer!=null){
                 $elemen=array(
                  'type'=>'img'
                  ,'label'=>'Bukti transfer'
                  ,'attr'=>array(
                  'name'=>'nama22'
                  ,'id'=>'nama22'
                  ,'class'=>'form-control'
                  )
                  ,'src'=>$tabeltopup->buktitransfer
                );
                $elfil[]=$elemen;
                }

                 if($tabeltopup->user_comment!=""){
                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan klien'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$tabeltopup->user_comment
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                      $arstatus=array();
                      $arstatus[2]="Ditolak";
                      $arstatus[3]="Diterima";

                  if($tabeltopup->status<2){
                  $arstatus[0]="Belum konfirm";
                  $arstatus[1]="Sudah konfirm";
                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'status'
                   ,'id'=>'status'
                   ,'class'=>'form-control'
                   )
                   ,'selek2'=>''
                   ,'list'=>$arstatus
                   ,'catch'=>$tabeltopup->status
                 );
                 $elfil[]=$elemen;
               }else{
                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$arstatus[$tabeltopup->status]
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan dari finance'
                   ,'attr'=>array(
                   'name'=>'admin_comment'
                   ,'id'=>'admin_comment'
                   ,'class'=>'form-control'
                   )
                 );

                if($tabeltopup->status>=2){
                  $elemen['attr']['disabled']="";
                  $elemen['attr']['value']=$tabeltopup->admin_comment;
                }
                 $elfil[]=$elemen;

                 $form_action=url('/admin/keuangan/pengajuantopup/save_update');


         break;
         case "keuangan/arsiptransferfee" : //form view arsip transfer fee
         $idw=$par_a;
         $butuhdialog=1;

         $tabelwithdraw=DB::table('tb_withdraw')
         ->where('id', '=', $idw)
         ->first();

         if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
           $tipeform=5;
         }

         $namatabel="Detail withdraw";
         $namamenudaricont="Detail withdraw";
         $dafmenudaricont=array(url('/admin/keuangan/arsiptransferfee')=>'Arsip withdraw','null'=>'Detail');


                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Tanggal'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tabelwithdraw->tanggalminta
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 $tb_user=DB::table('tb_user')
                      ->where('idr',$tabelwithdraw->idl)
                      ->select(['id','name'])
                      ->first();

                   $tb_lawyer=DB::table('tb_lawyer')
                       ->where('id',$tabelwithdraw->idl)
                       ->select(['saldo'])
                       ->first();


                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Lawyer'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tb_user->name
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                  $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Saldo'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>rupiah($tb_lawyer->saldo)
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                  $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Jumlah withdraw'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>rupiah($tabelwithdraw->nilai)
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 if($tabelwithdraw->komenlawyer!=""){
                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan lawyer'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$tabelwithdraw->komenlawyer
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                      $arstatus=array();
                      $arstatus[1]="Diproses";
                      $arstatus[2]="Ditolak";
                      $arstatus[3]="Selesai";

                  if($tabelwithdraw->status==0 || $tabelwithdraw->status==1){
                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'status'
                   ,'id'=>'status'
                   ,'class'=>'form-control'
                   )
                   ,'selek2'=>''
                   ,'list'=>$arstatus
                   ,'catch'=>$tabelwithdraw->status
                 );
                 $elfil[]=$elemen;
               }else{
               $arstatus[-1]="Dibatalkan";
                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$arstatus[$tabelwithdraw->status]
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan dari finance'
                   ,'attr'=>array(
                   'name'=>'admin_comment'
                   ,'id'=>'admin_comment'
                   ,'class'=>'form-control'
                   )
                 );
                  $elemen['attr']['disabled']="";
                  $elemen['attr']['value']=$tabelwithdraw->admin_comment;

                 $elfil[]=$elemen;

                 $form_action=url('/admin/keuangan/transferfee/save_update');
         break;
         case "keuangan/arsiptransferfeeklien" : //form view arsip transfer fee klien
         $idw=$par_a;
         $butuhdialog=1;

         $tabelwithdraw=DB::table('tb_withdrawkomisi')
         ->where('id', '=', $idw)
         ->first();

         if($tabelwithdraw->status==-1 || $tabelwithdraw->status==2 || $tabelwithdraw->status==3){
           $tipeform=5;
         }

         $namatabel="Detail withdraw";
         $namamenudaricont="Detail withdraw";
         $dafmenudaricont=array(url('/admin/keuangan/arsiptransferfeeklien')=>'Arsip withdraw','null'=>'Detail');


                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Tanggal'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tabelwithdraw->tanggalminta
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 $tb_user=DB::table('tb_user')
                      ->where('idr',$tabelwithdraw->idk)
                      ->select(['id','name'])
                      ->first();

                   $tb_klien=DB::table('tb_klien')
                       ->where('id',$tabelwithdraw->idk)
                       ->select(['komisi'])
                       ->first();


                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Klien'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>$tb_user->name
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                  $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Komisi'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>rupiah($tb_klien->komisi)
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                  $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Jumlah withdraw'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   ,'value'=>rupiah($tabelwithdraw->nilai)
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;

                 if($tabelwithdraw->user_comment!=""){
                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan klien'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$tabelwithdraw->user_comment
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                      $arstatus=array();
                      $arstatus[1]="Diproses";
                      $arstatus[2]="Ditolak";
                      $arstatus[3]="Selesai";

                  if($tabelwithdraw->status==0 || $tabelwithdraw->status==1){
                 $elemen=array(
                   'type'=>'select'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'status'
                   ,'id'=>'status'
                   ,'class'=>'form-control'
                   )
                   ,'selek2'=>''
                   ,'list'=>$arstatus
                   ,'catch'=>$tabelwithdraw->status
                 );
                 $elfil[]=$elemen;
               }else{
               $arstatus[-1]="Dibatalkan";
                 $elemen=array(
                   'type'=>'text'
                   ,'label'=>'Status'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$arstatus[$tabelwithdraw->status]
                   ,'disabled'=>''
                   )
                 );
                 $elfil[]=$elemen;
               }

                 $elemen=array(
                   'type'=>'textarea'
                   ,'label'=>'Pesan dari finance'
                   ,'attr'=>array(
                   'name'=>'admin_comment'
                   ,'id'=>'admin_comment'
                   ,'class'=>'form-control'
                   )
                 );
                  $elemen['attr']['disabled']="";
                  $elemen['attr']['value']=$tabelwithdraw->admin_comment;

                 $elfil[]=$elemen;

                 $form_action=url('/admin/keuangan/transferfee/save_update');
         break;
         case "callcenter/dokumentasi": //form view dokumentasi
         $idd=$par_a;
         $tabeldokumen=DB::table('tb_dokumentasi')
         ->where('id', '=', $idd)
         ->first();

         $namatabel="View Dokumentasi";
         $namamenudaricont="View Dokumentasi";
         $dafmenudaricont=array(url('/admin/callcenter/dokumentasi')=>'Dokumentasi','null'=>'View');


                 $tb_listuser=DB::table('tb_user')
                      ->where('level',1)
                      ->select(['id','name'])
                      ->get();

                      $isilist=array();
                      foreach($tb_listuser as $tbu){
                        $isilist[$tbu->id]=$tbu->name;
                      }

                      $tb_klien=DB::table('tb_klien')
                                    ->where('id',$tabeldokumen->idk)
                                    ->select(['idu'])
                                    ->first();

                   $tb_listuser=DB::table('tb_user')
                        ->where('level',1)
                        ->where('idr',$tabeldokumen->idk)
                        ->select(['name'])
                        ->first();
                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Klien'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     ,'value'=>$tb_listuser->name
                     ,'disabled'=>''
                     )

                   );
                   $elfil[]=$elemen;

                 $elemen=array(
                   'type'=>'richtext'
                   ,'label'=>'Kendala'
                   ,'attr'=>array(
                   'name'=>'masalah'
                   ,'id'=>'masalah'
                   ,'class'=>'form-control'
                   ,'value'=>$tabeldokumen->masalah
                   ,'disabled'=>''
                   )

                 );

                 $elfil[]=$elemen;

                $elemen=array(
                  'type'=>'text'
                  ,'label'=>'Bidang hukum'
                  ,'attr'=>array(
                  'name'=>'idj'
                  ,'id'=>'idj'
                  ,'class'=>'form-control'
                  )
                );

                  $tb_jenishukumaa=DB::table('tb_jenishukum')
                      ->where('id',$tabeldokumen->idj)
                      ->first();

                  if($tb_jenishukumaa==null){
                    $elemen['attr']['disabled']='';
                    $elemen['attr']['value']='Belum ditentukan';
                  }else{
                    $elemen['attr']['value']=$tb_jenishukumaa->jenis;
                  }

                    $elfil[]=$elemen;
                    $form_action="";
          break;
          case "keuangan/pengajuantopup" : //form view topup
          $idt=$par_a;
          $butuhdialog=1;

          $tabeltopup=DB::table('tb_topup')
          ->where('id', '=', $idt)
          ->first();

          if($tabeltopup->status>=2){
            $tipeform=5;
          }

          $namatabel="Detail top up";
          $namamenudaricont="Detail top up";
          $dafmenudaricont=array(url('/admin/keuangan/pengajuantopup')=>'Pengajuan top up','null'=>'Detail');

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Tanggal'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabeltopup->create_at
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                       $tb_user=DB::table('tb_user')
                            ->where('idr',$tabeltopup->idu)
                            ->select(['id','name'])
                            ->first();

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Klien'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_user->name
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                   $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Jumlah'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>rupiah($tabeltopup->topup_amount)
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;
                  if($tabeltopup->status!=0 && $tabeltopup->status!=-1){

                     $tb_metode_topup=DB::table('tb_metode_topup')
                     ->where('id', '=', $tabeltopup->idmt)
                     ->first();

                     $elemen=array(
                      'type'=>'text'
                      ,'label'=>'Metode bayar'
                      ,'attr'=>array(
                      'name'=>'nama22'
                      ,'id'=>'nama22'
                      ,'class'=>'form-control'
                      ,'value'=>$tb_metode_topup->metode." (".$tb_metode_topup->nomakun.")"
                      ,'disabled'=>''
                      )
                    );
                    $elfil[]=$elemen;

                  }
                  if($tabeltopup->buktitransfer!=null){
                  $elemen=array(
                   'type'=>'img'
                   ,'label'=>'Bukti transfer'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   )
                   ,'src'=>$tabeltopup->buktitransfer
                 );
                 $elfil[]=$elemen;
                 }

                  if($tabeltopup->user_comment!=""){
                  $elemen=array(
                    'type'=>'textarea'
                    ,'label'=>'Pesan klien'
                    ,'attr'=>array(
                    'name'=>'masalah'
                    ,'id'=>'masalah'
                    ,'class'=>'form-control'
                    ,'value'=>$tabeltopup->user_comment
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;
                }

                       $arstatus=array();
                       $arstatus[-1]="Pebayaran dibatalkan";;
                       $arstatus[0]="Belum konfirm pembayaran";
                       $arstatus[1]="Sudah konfirm pembayaran";
                       $arstatus[2]="Ditolak";
                       $arstatus[3]="Diterima";

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Status'
                    ,'attr'=>array(
                    'name'=>'masalah'
                    ,'id'=>'masalah'
                    ,'class'=>'form-control'
                    ,'value'=>$arstatus[$tabeltopup->status]
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;


                 if($tabeltopup->admin_comment!=""){
                  $elemen=array(
                    'type'=>'textarea'
                    ,'label'=>'Pesan dari finance'
                    ,'attr'=>array(
                    'name'=>'admin_comment'
                    ,'id'=>'admin_comment'
                    ,'class'=>'form-control'
                    ,'disabled'=>''
                    ,'value'=>$tabeltopup->admin_comment
                    )
                  );
                  $elfil[]=$elemen;
                }


                  $form_action=url('/admin/keuangan/pengajuantopup/save_update');
          break;
          case "product_category/list": //form view kategori

          $idt=$par_a;
          $butuhdialog=1;

          $tabelkategori=DB::table('tb_product_cat')
          ->where('id', '=', $idt)
          ->first();


          $namatabel="Detail kategori";
          $namamenudaricont="Detail kategori";
          $dafmenudaricont=array(url('/admin/product_category/list')=>'Kategori produk','null'=>'Detail');


                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Kategori'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelkategori->category
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'textarea'
                    ,'label'=>'Keterangan'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelkategori->info
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Warna'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelkategori->color
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  if($tabelkategori->icon!=null){
                  $elemen=array(
                   'type'=>'img'
                   ,'label'=>'Ikon Kategori'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   )
                   ,'src'=>$tabelkategori->icon
                 );
                 $elfil[]=$elemen;
                 }

          $form_action=url('/admin/keuangan/pengajuantopup/save_update');

          break;
          case "product/list": //form view produk

          $idt=$par_a;
          $butuhdialog=1;

          $tabelproduk=DB::table('tb_product')
          ->where('id', '=', $idt)
          ->first();


          $namatabel="Detail produk";
          $namamenudaricont="Detail produk";
          $dafmenudaricont=array(url('/admin/product/list')=>'Tabel produk','null'=>'Detail');


                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Nama'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelproduk->name
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Harga'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelproduk->price
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'textarea'
                    ,'label'=>'Keterangan'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tabelproduk->info
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  if($tabelproduk->mainpict!=null){
                  $elemen=array(
                   'type'=>'img'
                   ,'label'=>'Foto Produk'
                   ,'attr'=>array(
                   'name'=>'nama22'
                   ,'id'=>'nama22'
                   ,'class'=>'form-control'
                   )
                   ,'src'=>$tabelproduk->mainpict
                 );
                 $elfil[]=$elemen;
                 }

          $form_action=url('/admin/keuangan/pengajuantopup/save_update');

          break;
          case "keuangan/transaksijualbeli": //form view transaksi jual beli

          $idt=$par_a;
          $butuhdialog=1;

          $arstatus=array();
          $arstatus[0]="Belum diantar";
          $arstatus[1]="Sedang diantar";
          $arstatus[2]="Sudah diterima";
          $arstatus[3]="Dicancel";

          $tb_transaksi=DB::table('tb_transaksi')
          ->where('id', '=', $idt)
          ->first();


          $namatabel="Detail transaksi";
          $namamenudaricont="Detail transaksi";
          $dafmenudaricont=array(url('admin/keuangan/transaksijualbeli')=>'Tabel transaksi','null'=>'Detail');

          $tb_user_buyer=DB::table('tb_user')
                        ->where('id',$tb_transaksi->idb)
                        ->select(['name'])
                        ->first();
          $tb_user_seller=DB::table('tb_user')
                        ->where('id',$tb_transaksi->ids)
                        ->select(['name'])
                        ->first();
          $tb_address=DB::table('tb_address')
                        ->where('id',$tb_transaksi->ida)
                        ->first();
          $tb_recipient=DB::table('tb_recipient')
                        ->where('id',$tb_transaksi->idr)
                        ->first();

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Tanggal'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_transaksi->create_at
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Biaya'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>rupiah($tb_transaksi->bill)
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Pajak'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>rupiah($tb_transaksi->tax)
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Ongkir'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>rupiah($tb_transaksi->ongkir)
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Keuntungan'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    ,'value'=>rupiah($tb_transaksi->tax+$tb_transaksi->ongkir)
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Status'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$arstatus[$tb_transaksi->status]
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'User pembeli'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_user_buyer->name
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'User penjual'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_user_seller->name
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Nama penerima'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_recipient->name
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'text'
                    ,'label'=>'Nomor telepon penerima'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_recipient->phone
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;

                  $elemen=array(
                    'type'=>'textarea'
                    ,'label'=>'Alamat penerima'
                    ,'attr'=>array(
                    'name'=>'info'
                    ,'id'=>'info'
                    ,'class'=>'form-control'
                    ,'value'=>$tb_address->address
                    ,'disabled'=>''
                    )
                  );
                  $elfil[]=$elemen;


          $form_action=url('/admin/keuangan/pengajuantopup/save_update');

          break;

          // akhir form view
       }
       foreach ($needjsf as $isijsf){

if(isset($isijsf->var)){
       $isijsv = new \stdClass();
       $isijsv->name=$isijsf->var;
       $needjsv[]=$isijsv;
}
       }
       $arview=array('needjsv'=>$needjsv
       ,'needjsf'=>$needjsf
       ,'namatabel'=>$namatabel
       ,'namamenudaricont'=>$namamenudaricont
       ,'dafmenudaricont'=>$dafmenudaricont
       ,'halamandaricont'=>$halaman
       ,'form_action'=>$form_action
       ,'elfil'=>$elfil
       ,'butuhdialog'=>$butuhdialog
        ,'tipeform'=>$tipeform);
    return View::make('hal_admin.hal_form',$arview);
     }
     public function hal_delete($par_a=null){

       $elfil=array();
       $needjsv = array();
       $needjsf = array();
       $tipeform=4;
       $butuhdialog=0;

         $halaman=Request::segment(2)."/".Request::segment(3);
         $form_action="";
         switch($halaman){
           case "users/manage": //form delete user
           $idu=$par_a;
           $tabeluser=DB::table('tb_user')
           ->where('id', '=', $idu)
           ->first();

           $namatabel="Add User";
           $namamenudaricont="Add User";
           $dafmenudaricont=array(url('/admin/users/manage')=>'Users','null'=>'Edit');

                   $isijsf = new \stdClass();
                   $isijsf->type="ajax2t";
                   $isijsf->name="ambildatatabeluser";
                   $isijsf->url="\"".url('/')."/admin/users/manage/carijson\"";
                   $isijsf->var="jsonisitabel";
                   $isijsf->target="sample_1";
                   $needjsf[]=$isijsf;
                   $isijsf = new \stdClass();
                   $isijsf->type="main";
                   $isijsf->content="ambildatatabeluser();";
                   $needjsf[]=$isijsf;

                   $role[1]="Klien";
                   $role[2]="Lawyer";
                   $role[3]="Customer Service";
                   $role[4]="Finance";
                   $role[5]="Direksi";
                   $role[6]="Admin Operation";

                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'idakun'
                     ,'id'=>'idakun'
                     ,'value'=>$tabeluser->id
                     )
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Role'
                     ,'attr'=>array(
                     'name'=>'role'
                     ,'id'=>'role'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>idrole($tabeluser->level)
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->name
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->email
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->username
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->phone
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Address'
                     ,'attr'=>array(
                     'name'=>'address'
                     ,'id'=>'address'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabeluser->address
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $form_action=url('/admin/users/manage/delete_data');
           break;
           case "lawyers/list": //form delete lawyer
           $id=$par_a;
           $namatabel="Hapus Lawyer";
           $namamenudaricont="Hapus Lawyer";
           $dafmenudaricont=array(url('/admin/lawyers/list')=>'Lawyers','null'=>'Edit');

        $form_action=url('/admin/lawyers/list/delete_data');
           $isian=DB::table('tb_lawyer')
           ->where('id', '=', $id)
           ->first();

           $tb_user=DB::table('tb_user')
           ->where('id', '=', $isian->idu)
           ->first();
                    $name=$tb_user->name;
                    $email=$tb_user->email;
                    $filefoto=$tb_user->filefoto;
                    $username=$tb_user->username;
                    $password=$tb_user->password;
                    $phone=$tb_user->phone;
                    $persenfee=$isian->persenfee;
                    $hargapermenit=$isian->permenit;
                    $firmlatitude=$isian->firmlatitude;
                    $firmlongitude=$isian->firmlongitude;
                    $tampilkanposisi=$isian->tampilkanposisi;


                           $dafmapjen=DB::table('tb_jem_lawyer')
                           ->where('idl', '=', $id)
                           ->select(['id','idj'])
                           ->get();

                           $kategori = new \stdClass();
                           $i=0;
                           $listval=array();
                            foreach ($dafmapjen as $map) {
                              $i+=1;
                                    $cat=DB::table('tb_jenishukum')
                                    ->where('id', '=', $map->idj)
                                    ->first();
                                    $isikat = new \stdClass();
                                    $isikat->id=$cat->id;
                                    $isikat->jenis=$cat->jenis;
                                    $kategori->$i=$isikat;
                                    $listval[]=$cat->id;

                            }

                            $tb_jenishukum=DB::table('tb_jenishukum')
                                 ->select(['id','jenis'])
                                 ->get();

                                 $isilist=array();
                                 foreach($tb_jenishukum as $tbj){
                                   $isilist[$tbj->id]=$tbj->jenis;
                                 }

                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$id
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Bidang hukum'
                     ,'attr'=>array(
                     'name'=>'bidang[]'
                     ,'id'=>'bidang'
                     ,'class'=>'form-control'
                     ,'value'=>'1'
                     ,'multiple'=>'multiple'
                     ,'disabled'=>''
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$listval
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$name
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Name is required and cannot be empty')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Email'
                     ,'attr'=>array(
                     'name'=>'email'
                     ,'id'=>'email'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$email
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'The email address is required')
                       ,'regexp'=>array('regexp'=>"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",'message'=>'Please enter valid email format')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Username'
                     ,'attr'=>array(
                     'name'=>'username'
                     ,'id'=>'username'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$username
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Username is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Phone'
                     ,'attr'=>array(
                     'name'=>'phone'
                     ,'id'=>'phone'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$phone
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Phone is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Permenit'
                     ,'attr'=>array(
                     'name'=>'hargapermenit'
                     ,'id'=>'hargapermenit'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$hargapermenit
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Harga permenit is required')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Persenan (%)'
                     ,'attr'=>array(
                     'name'=>'persenfee'
                     ,'id'=>'persenfee'
                     ,'required'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$persenfee
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Persenan is required')
                      )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     )
                     ,'src'=>$filefoto
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Firm latitude'
                     ,'attr'=>array(
                     'name'=>'firmlatitude'
                     ,'id'=>'firmlatitude'
                     ,'class'=>'form-control'
                     ,'value'=>$firmlatitude
                     ,'disabled'=>''
                     )
                   );

                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Firm longitude'
                     ,'attr'=>array(
                     'name'=>'firmlongitude'
                     ,'id'=>'firmlongitude'
                     ,'class'=>'form-control'
                     ,'value'=>$firmlongitude
                     ,'disabled'=>''
                     )
                   );

                   $elfil[]=$elemen;



           break;
           case "kontenapps/berita": //form delete berita
           $idd=$par_a;
           $tabelberita=DB::table('tb_berita')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Hapus Berita";
           $namamenudaricont="Hapus Berita";
           $dafmenudaricont=array(url('/admin/kontenapps/berita')=>'Berita','null'=>'Hapus');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Judul'
                     ,'attr'=>array(
                     'name'=>'judul'
                     ,'id'=>'judul'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tabelberita->judul
                     )
                   );
                   $elfil[]=$elemen;


                   $elemen=array(
                     'type'=>'richtext'
                     ,'label'=>'Isi'
                     ,'attr'=>array(
                     'name'=>'isi'
                     ,'id'=>'isi'
                     ,'disabled'=>''
                     ,'class'=>'form-control'
                     ,'value'=>$tabelberita->isi
                     )
                   );

                   $elfil[]=$elemen;


                   $form_action=url('/admin/kontenapps/berita/delete_data');
           break;
           case "product_category/list": //form delete kategori produk
           $idd=$par_a;
           $tabelkategori=DB::table('tb_product_cat')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Hapus Kategori";
           $namamenudaricont="Hapus Kategori";
           $dafmenudaricont=array(url('/admin/product_category/list')=>'Kategori','null'=>'Hapus');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Kategori'
                     ,'attr'=>array(
                     'name'=>'kategori'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tabelkategori->category
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Keterangan'
                     ,'attr'=>array(
                     'name'=>'kategori'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tabelkategori->info
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Warna'
                     ,'attr'=>array(
                     'name'=>'kategori'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     ,'value'=>$tabelkategori->color
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'unik_fotoprofil'
                     ,'label'=>'Foto'
                     ,'attr'=>array(
                     'name'=>'foto'
                     ,'id'=>'foto'
                     ,'class'=>'form-control'
                     ,'disabled'=>''
                     )
                     ,'src'=>$tabelkategori->icon
                   );
                   $elfil[]=$elemen;


                   $form_action=url('/admin/product_category/list/delete_data');
           break;
           case "product/list": //form delete produk
           $idd=$par_a;
           $tabelproduk=DB::table('tb_product')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Hapus Produk";
           $namamenudaricont="Hapus Produk";
           $dafmenudaricont=array(url('/admin/product/list')=>'Tabel Produk','null'=>'Hapus');

           $dafmapjen=DB::table('tb_jem_product_cat')
           ->where('idp', '=', $idd)
           ->select(['id','idpc'])
           ->get();

           $kategori = new \stdClass();
           $i=0;
           $listval=array();
           foreach ($dafmapjen as $map) {
           $i+=1;
                 $cat=DB::table('tb_product_cat')
                 ->where('id', '=', $map->idpc)
                 ->first();
                 $isikat = new \stdClass();
                 $isikat->id=$cat->id;
                 $isikat->name=$cat->category;
                 $kategori->$i=$isikat;
                 $listval[]=$cat->id;

           }

           $tb_jenishukum=DB::table('tb_product_cat')
              ->select(['id','category'])
              ->get();

              $isilist=array();
              foreach($tb_jenishukum as $tbj){
                $isilist[$tbj->id]=$tbj->category;
              }


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'select'
                     ,'label'=>'Kategori'
                     ,'attr'=>array(
                     'name'=>'kategori[]'
                     ,'id'=>'kategori'
                     ,'class'=>'form-control'
                     ,'value'=>'1'
                     ,'multiple'=>'multiple'
                     )
                     ,'selek2'=>''
                     ,'list'=>$isilist
                     ,'catch'=>$listval
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Nama'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelproduk->name
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Harga'
                     ,'attr'=>array(
                     'name'=>'nama22'
                     ,'id'=>'nama22'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelproduk->price
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Keterangan'
                     ,'attr'=>array(
                     'name'=>'info'
                     ,'id'=>'info'
                     ,'class'=>'form-control'
                     ,'value'=>$tabelproduk->info
                     ,'disabled'=>''
                     )
                   );
                   $elfil[]=$elemen;

                   if($tabelproduk->mainpict!=null){
                   $elemen=array(
                    'type'=>'img'
                    ,'label'=>'Foto Produk'
                    ,'attr'=>array(
                    'name'=>'nama22'
                    ,'id'=>'nama22'
                    ,'class'=>'form-control'
                    )
                    ,'src'=>$tabelproduk->mainpict
                  );
                  $elfil[]=$elemen;
                  }


                   $form_action=url('/admin/product/list/delete_data');
           break;
           case "transport_service/list": //form delete jasa transportasi
           $idd=$par_a;
           $tabelservice=DB::table('tb_transport_service')
           ->where('id', '=', $idd)
           ->first();

           $namatabel="Delete Transport Service";
           $namamenudaricont="Delete Transport Service";
           $dafmenudaricont=array(url('/admin/transport_service/list')=>'Transport Service','null'=>'Delete');


                   $elemen=array(
                     'type'=>'hidden'
                     ,'attr'=>array(
                     'name'=>'id'
                     ,'id'=>'id'
                     ,'value'=>$idd
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'text'
                     ,'label'=>'Name'
                     ,'attr'=>array(
                     'name'=>'name'
                     ,'id'=>'name'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelservice->name
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isian nama transport service belum dimasukkan')
                     )
                   );
                   $elfil[]=$elemen;

                   $elemen=array(
                     'type'=>'textarea'
                     ,'label'=>'Detail'
                     ,'attr'=>array(
                     'name'=>'detail'
                     ,'id'=>'detail'
                     ,'class'=>'form-control'
                     ,'required'=>''
                     ,'value'=>$tabelservice->detail
                     ,'disabled'=>''
                     )
                     ,'validators'=>array(
                       'notEmpty'=>array('message'=>'Isikan keterangan')
                     )
                   );
                   $elfil[]=$elemen;


                   $form_action=url('/admin/transport_service/list/delete_data');
           break;

           // akhir form delete
         }

         foreach ($needjsf as $isijsf){

     if(isset($isijsf->var)){
         $isijsv = new \stdClass();
         $isijsv->name=$isijsf->var;
         $needjsv[]=$isijsv;
     }
         }
         $arview=array('needjsv'=>$needjsv
         ,'needjsf'=>$needjsf
         ,'namatabel'=>$namatabel
         ,'namamenudaricont'=>$namamenudaricont
         ,'dafmenudaricont'=>$dafmenudaricont
         ,'halamandaricont'=>$halaman
         ,'form_action'=>$form_action
         ,'elfil'=>$elfil
          ,'tipeform'=>$tipeform
        ,'butuhdialog'=>$butuhdialog);
      return View::make('hal_admin.hal_form',$arview);
     }

     public function crud_c(){
       $halaman=Request::segment(2)."/".Request::segment(3);
       $urlbalik="";
       switch($halaman){
         case "users/manage": // proses insert user
         $urlbalik=url('/admin/users/manage/add');
         $rules = array(
             'role' => ''
             ,'name' => 'required'
            ,'email' => 'required|email'
            ,'username' => 'required'
            ,'phone' => 'required'
            ,'address' => 'required'
         );
         $messsages = array(
               'name.required'=>'Nama empty',
               'email.required'=>'Email empty',
               'email.email'=>'Insert email formatted',
               'username.required'=>'Username empty',
               'phone.required'=>'Phone empty',
               'address.required'=>'Address empty',
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);


         if ($validator->fails()) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {
           $userdata = array();
           foreach($rules as $key=>$value) {
            $userdata[$key] = Input::get($key);
           }
           $role=Input::get('role');
           $username=Input::get('username');
           $name=Input::get('name');
           $email=Input::get('email');
           $phone=Input::get('phone');
           $address=Input::get('address');

           $sandi=$username."123";
           $salt="garammanis".rand(1,999);
           $hasilhash = hash('sha512', $sandi. $salt);
           $idterakhir = DB::table('tb_user')->insertGetId(
           array(
                      'username' => $username,
                      'name' => $name,
                      'email' => $email,
                      'phone' => $phone,
                      'salt' => $salt,
                      'password' => $hasilhash,
                      'level' =>$role,
                      'address' =>$address
                )
           );

           $arin=array();
           $arin['idu']=$idterakhir;

           $idwallet = DB::table('tb_wallet')->insertGetId(
           array(
                      'ecash' => 0
                      ,'detail' => "Belong to $name"
                )
           );

           DB::table('tb_user')
           ->where('id', $idterakhir)
           ->update(array(
             'idsaldo' => $idwallet
           ));

           DB::table('tb_rekening')
           ->insert(array(
                  'idu' =>$idterakhir
            ));

             return Redirect::to('/admin/users/manage/edit/'.$idterakhir)
                 ->with(['Berhasil menambah data']);

               //return Redirect::to('/admin/users/manage')
                   //->with(['Berhasil menambah data']);
           echo  var_dump($userdata);
         }

         break;
         case "lawyers/list": //proses insert lawyer
         $rules = array(
             'name'    => 'required'
             ,'email' => 'required'
             ,'hargapermenit' => 'required'
             ,'phone' => 'required'
             ,'username' => 'required'
             ,'persenfee' => 'required'
             ,'foto' => 'required'
         );
         $messages = array(
               'name.required'=>'Nama  harus diisi'
               ,'email.required'=>'Email harus diisi'
               ,'hargapermenit.required'=>'Harga per menit harus diisi'
               ,'persenfee.required'=>'Persenan harus diisi'
               ,'phone.required'=>'Phone harus diisi'
               ,'username.required'=>'Username harus diisi'
               ,'foto.required'=>'Foto harus diupload'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messages);


         if ($validator->fails()) {
           return Redirect::to('/admin/lawyers/list/add')
                 ->withErrors($validator);
         }else{
           //return "siap proses";
           $username=Input::get('username');
           $sandi=$username."123";
           $salt="garammanis".rand(1,999);
           $hasilhash = hash('sha512', $sandi. $salt);

           $idu = DB::table('tb_user')->insertGetId(
               array(
                           'name' => Input::get('name'),
                           'email' => Input::get('email'),
                           'phone' => Input::get('phone'),
                           'username' => Input::get('username'),
                           'salt' => $salt,
                           'password' => $hasilhash,
                           'level'  => "2",
                           'filefoto'  => ""
                     )
                     );

           $idterakhir = DB::table('tb_lawyer')->insertGetId(
               array(
                           'idu' => $idu,
                           'online' => 0,
                           'permenit' => Input::get('hargapermenit'),
                           'persenfee' => Input::get('persenfee')
                     )
                     );

                     DB::table('tb_rekening')
                     ->insert(array(
                            'idu' =>$idu
                      ));

                     $destinationPath = 'uploads/images'; // upload path
                     $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
                     //$fileName = rand(11111,99999).'.'.$extension; // renameing image
                     $fileName = "fotolawyer".$idterakhir.'.'.$extension;
                     Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
                     // sending back with message
                          DB::table('tb_user')
                     ->where('id', $idu)
                     ->update(array(
                                 'filefoto' => $fileName
                           ));

                           $bidanghukum=Input::get('bidang');
                           foreach($bidanghukum as $kat){
                            if(($kat+0)!=0){
                             DB::table('tb_jem_lawyer')->insert(
                                   array(
                                               'idl' => $idterakhir,
                                               'idj' => $kat
                                         )
                                         );
                             }
                             }

                             return Redirect::to('/admin/lawyers/list')
                                 ->with(['Berhasil menambah data']);
         }


         break;
         case "callcenter/dokumentasi": // proses insert dokumentasi
         $urlbalik=url('/admin/callcenter/dokumentasi/add');
         $rules = array(
             'role' => ''
             ,'name' => 'required'
            ,'masalah' => 'required'
            ,'idj' => 'required'
         );
         $messsages = array(
               'name.required'=>'Nama empty'
               ,'masalah.required'=>'Kendala empty'
               ,'idj.required'=>'Bidang hukum empty'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);


         if ($validator->fails()) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {

           $tb_klien=DB::table('tb_klien')
                         ->where('idu',Input::get('name'))
                         ->select(['id'])
                         ->first();

           $arin=array(
                      'masalah' => Input::get('masalah')
                      ,'idk' => $tb_klien->id
                      ,'tanggal'=>date("Y-m-d H:i:s")
                );
                $idj= Input::get('idj');
                if($idj!=0){
                  $arin['status']=1;
                  $arin['idj']=$idj;
                }
           DB::table('tb_dokumentasi')->insert($arin);

               return Redirect::to('/admin/callcenter/dokumentasi')
                   ->with(['Berhasil menambah data']);
         }

         break;
         case "kontenapps/berita": // proses insert berita
         $urlbalik=url('/admin/kontenapps/berita/add');
         $rules = array(
             'role' => ''
             ,'judul' => 'required'
            ,'isi' => 'required'
         );
         $messsages = array(
               'judul.required'=>'Judul empty'
               ,'isi.required'=>'Isi empty'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);


         if ($validator->fails()) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {
           $arin=array(
                      'judul' => Input::get('judul')
                      ,'isi' => Input::get('isi')
                      ,'tanggal'=>date("Y-m-d H:i:s")
                );

           DB::table('tb_berita')->insert($arin);

               return Redirect::to('/admin/kontenapps/berita')
                   ->with(['Berhasil menambah data']);
         }

         break;
         case "product_category/list": // proses insert kategori produk
         $urlbalik=url('/admin/product_category/list/add');
         $rules = array(
             'role' => ''
             ,'kategori' => 'required'
             ,'info' => 'required'
             ,'color' => 'required'
             ,'foto' => 'required'
         );
         $messsages = array(
               'kategori.required'=>'Kategori belum diisi'
              ,'info.required'=>'Keterangan belum diisi'
             ,'color.required'=>'Warna belum diisi'
            ,'foto.required'=>'Ikon belum dipilih'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);
         $color=Input::get('color');
         $colorbenar=0;

         if (ctype_xdigit($color)) {
         $colorbenar=1;
             }
         if ($validator->fails() && $colorbenar==0) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {
           $arin=array(
                      'category' => Input::get('kategori')
                      ,'info' => Input::get('info')
                      ,'color' => Input::get('color')
                      ,'icon' => ""
                      ,'create_at' => date("Y-m-d H:i:s")
                      ,'last_update' => date("Y-m-d H:i:s")
                );

           $idk=DB::table('tb_product_cat')->insertGetId($arin);


           $destinationPath = 'uploads/images'; // upload path
           $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
           //$fileName = rand(11111,99999).'.'.$extension; // renameing image
           $fileName = "ikonproduk".$idk.'.'.$extension;
           Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
           // sending back with message
               DB::table('tb_product_cat')
           ->where('id', $idk)
           ->update(array(
                      'icon' => $fileName
                ));

               return Redirect::to('/admin/product_category/list')
                   ->with(['Berhasil menambah data']);
         }

         break;
         case "product/list": // proses insert produk
         $urlbalik=url('/admin/product/list/add');
         $rules = array(
             'role' => ''
             ,'name' => 'required'
             ,'price' => 'required'
             ,'foto' => 'required'
             ,'info' => 'required'
         );
         $messsages = array(
               'name.required'=>'Nama Produk belum diisi'
              ,'price.required'=>'Harga Produk belum diisi'
              ,'foto.required'=>'Foto harus diupload'
             ,'info.required'=>'Keterangan Produk belum diisi'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);


         if ($validator->fails()) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {
           $arin=array(
                      'name' => Input::get('name')
                      ,'price' => Input::get('price')
                      ,'mainpict'  => ""
                      ,'info' => Input::get('info')
                      ,'ids' => getUserInfo('user_id')
                      ,'create_at' => date("Y-m-d H:i:s")
                      ,'last_update' => date("Y-m-d H:i:s")
                );

          $idp =  DB::table('tb_product')->insertGetId($arin);


          $destinationPath = 'uploads/images'; // upload path
          $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
          //$fileName = rand(11111,99999).'.'.$extension; // renameing image
          $fileName = "fotoutamaproduk".$idp.'.'.$extension;
          Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
          // sending back with message
              DB::table('tb_product')
          ->where('id', $idp)
          ->update(array(
                     'mainpict' => $fileName
               ));

          $kategori=Input::get('kategori');
          foreach($kategori as $kat){
           if(($kat+0)!=0){
            DB::table('tb_jem_product_cat')->insert(
                  array(
                              'idp' => $idp,
                              'idpc' => $kat
                        )
                        );
            }
           }


               return Redirect::to('/admin/product/list')
                   ->with(['Berhasil menambah data']);
         }

         break;
         case "transport_service/list": // proses insert jasa transportasi
         $urlbalik=url('/admin/transport_service/list/add');
         $rules = array(
             'name' => 'required'
             ,'detail' => 'required'
         );
         $messsages = array(
               'name.required'=>'Isian nama belum diisi'
              ,'detail.required'=>'Isian detail belum diisi'
         );
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules,$messsages);


         if ($validator->fails()) {
             return Redirect::to($urlbalik)
                 ->withErrors($validator);
         } else {
           $arin=array(
                      'name' => Input::get('name')
                      ,'detail' => Input::get('detail')
                      ,'create_at' => date("Y-m-d H:i:s")
                      ,'last_update' => date("Y-m-d H:i:s")
                );

           $idt=DB::table('tb_transport_service')->insertGetId($arin);

               return Redirect::to('/admin/transport_service/list')
                   ->with(['Berhasil menambah data']);
         }

         break;

         //akhir proses insert
       }
     }


     public function jsonlistbulan($thn){
       $halaman=Request::segment(2)."/".Request::segment(3);

       $listbul=array();
       $listbul[0]="Pilih";

       switch($halaman){
         case "laporan/jsonlogpanggilan_listbulan": // cari json list bulan log panggilan
         $dafbul=DB::table('tb_call_history')
         ->where('idl',getUserInfo("user_roleid"))
         ->where('seconds','!=',0)
              ->distinct()
              ->select([DB::raw('MONTH(start) as bulan')])
              ->whereRaw('YEAR(start) = ?', [$thn])
              ->get();

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "laporan/jsontransferfee_listbulan": // cari json list transfer fee
         $dafbul=DB::table('tb_withdraw')
         ->where('idl',getUserInfo("user_roleid"))
              ->distinct()
              ->select([DB::raw('MONTH(tanggalminta) as bulan')])
              ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
              ->get();

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "laporan/jsontransferfeeklien_listbulan": // cari json list transfer fee klien
         $dafbul=DB::table('tb_withdrawkomisi')
         ->where('idl',getUserInfo("user_roleid"))
              ->distinct()
              ->select([DB::raw('MONTH(tanggalminta) as bulan')])
              ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
              ->get();

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "lawyers/jsonlogtelepon" : // cari json list bulan log telepon
         $dafbul=DB::table('tb_call_history')
         ->where('seconds','!=',0)
              ->select([DB::raw('MONTH(start) as bulan')])
              ->whereRaw('YEAR(start) = ?', [$thn])
               ->distinct()
              ->get();

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "callcenter/jsondokumentasi" :  // cari json list bulan dokumentasi
         $dafbul=DB::table('tb_dokumentasi')
              ->select([DB::raw('MONTH(create_at) as bulan')])
              ->whereRaw('YEAR(create_at) = ?', [$thn])
              ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "kontenapps/berita" :  // cari json list bulan berita
         $dafbul=DB::table('tb_berita')
              ->select([DB::raw('MONTH(create_at) as bulan')])
              ->whereRaw('YEAR(create_at) = ?', [$thn])
              ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "keuangan/jsonpengajuantopup" :  // cari json list bulan pengajuann topup
         $dafbul=DB::table('tb_topup')
              ->select([DB::raw('MONTH(create_at) as bulan')])
              ->whereRaw('YEAR(create_at) = ?', [$thn])
              ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "keuangan/jsonarsiptopup" :  // cari json list bulan arsip topup
         $dafbul=DB::table('tb_topup')
              ->select([DB::raw('MONTH(create_at) as bulan')])
              ->whereRaw('YEAR(create_at) = ?', [$thn])
              ->where('status',3)
              ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "keuangan/jsonarsiptransferfee" :  // cari json list bulan arsip transfer fee
         $dafbul=DB::table('tb_withdraw')
              ->select([DB::raw('MONTH(tanggalminta) as bulan')])
              ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
               ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "keuangan/jsonarsiptransferfeeklien" :  // cari json list bulan arsip transfer fee klien
         $dafbul=DB::table('tb_withdrawkomisi')
              ->select([DB::raw('MONTH(tanggalminta) as bulan')])
              ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
               ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
         case "keuangan/transaksijualbeli" :  // cari json list bulan transaksi jualan
         $dafbul=DB::table('tb_transaksi')
              ->select([DB::raw('MONTH(create_at) as bulan')])
              ->whereRaw('YEAR(create_at) = ?', [$thn])
               ->distinct()
              ->get();
        $listbul[0]="-all-";

              foreach ($dafbul as $bul) {
              $listbul[$bul->bulan]=bulanIndo($bul->bulan);
              }
         break;
       }

       return $listbul;
     }


     public function carijson($par_a=null,$par_b=null,$par_c=null){

       $bahanreturn="[,,]";
       $bahanreturnbaru = array();
       $halaman=Request::segment(2)."/".Request::segment(3);
       switch($halaman){
         case "laporan/logpanggilan": //cari json log panggilan
         $thn=$par_a;
         $bln=$par_b;
         $artikels=DB::table('tb_call_history')
                    ->where('idl',getUserInfo("user_roleid"))
                    ->where('seconds','!=',0)
                    ->whereRaw('YEAR(start) = ?', [$thn])
                    ->whereRaw('MONTH(start) = ?', [$bln])
                    ->get();

           if($bln==0){
           $artikels=DB::table('tb_call_history')
                     ->where('idl',getUserInfo("user_roleid"))
                     ->where('seconds','!=',0)
                     ->whereRaw('YEAR(start) = ?', [$thn])
                     ->get();
           }
           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_user=DB::table('tb_user')
                    ->where('idr',$art->idk)
                    ->select(['name'])
                    ->first();

          $totalbiaya=rupiah($art->bill);
          $tot+=$art->bill;
          $bahanreturnbaru[] =array($nom
          ,$art->start
          ,$tb_user->name
          ,$art->seconds
          ,$totalbiaya);

          }

                    $tb_lawyer=DB::table('tb_lawyer')
                              ->where('idu',getUserInfo('user_roleid'))
                              ->select(['persenfee'])
                              ->first();
                    $tot4lawyer=($tot*$tb_lawyer->persenfee)/100;
                    $tot4tl=$tot-$tot4lawyer;
          $listnya["isifooter"]="Total pendapatan : ".rupiah($tot).", untuk Anda : ".rupiah($tot4lawyer)." untuk TanyaLawyer : ".rupiah($tot4tl);
          $listnya["list"]=$bahanreturnbaru;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "laporan/transferfee": //cari json  transfer fee
         $thn=$par_a;
         $bln=$par_b;
         $artikels=DB::table('tb_withdraw')
                    ->where('idl',getUserInfo("user_roleid"))
                    ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
                    ->whereRaw('MONTH(tanggalminta) = ?', [$bln])
                    ->get();

           if($bln==0){
           $artikels=DB::table('tb_withdraw')
                     ->where('idl',getUserInfo("user_roleid"))
                     ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
                     ->get();
           }

           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,rupiah($art->nilai)
          ,$art->status);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "laporan/transferfeeklien": //cari json  transfer fee klien
         $thn=$par_a;
         $bln=$par_b;
         $artikels=DB::table('tb_withdrawkomisi')
                    ->where('idl',getUserInfo("user_roleid"))
                    ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
                    ->whereRaw('MONTH(tanggalminta) = ?', [$bln])
                    ->get();

           if($bln==0){
           $artikels=DB::table('tb_withdrawkomisi')
                     ->where('idl',getUserInfo("user_roleid"))
                     ->whereRaw('YEAR(tanggalminta) = ?', [$thn])
                     ->get();
           }

           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,rupiah($art->nilai)
          ,$art->status);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "laporan/fee": // cari json laporan fee
         $thn=$par_a;
         $artikels=DB::table('tb_mutasi')
                    ->where('idl',getUserInfo("user_roleid"))
                    ->whereRaw('YEAR(periode) = ?', [$thn])
                    ->get();
           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $periode=bulanIndo(date('m',strtotime($art->periode)));
          $fee=rupiah($art->fee);
          $share=rupiah($art->share);
          $tl=rupiah($art->tl);
          $withdraw=rupiah($art->withdraw);
          $saldo=rupiah($art->saldo);

          $bahanreturnbaru[] =array($nom
          ,$periode
          ,$fee
          ,$share
          ,$tl
          ,$withdraw
          ,$saldo);

          }

                    $tb_lawyer=DB::table('tb_lawyer')
                              ->where('idu',getUserInfo('user_roleid'))
                              ->select(['persenfee'])
                              ->first();
                    $tot4lawyer=($tot*$tb_lawyer->persenfee)/100;
                    $tot4tl=$tot-$tot4lawyer;
          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "users/manage": //cari json daftar user
         $listidtabel=array();
         if($par_a!=null && $par_a!=0){
         $tb_user=DB::table('tb_user')
                    ->where('level',$par_a)
                    ->where('id','!=',getUserInfo('user_id'))
                    ->where('status',1)
                    ->get();
         }else{
         $tb_user=DB::table('tb_user')
                    ->where('id','!=',getUserInfo('user_id'))
                    ->where('status',1)
                    ->get();
                  }
           $nom=0;
           $jum=count($tb_user);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($tb_user as $tbu){
          $nom+=1;
          $listidtabel[]=$tbu->id;

          $bahanreturnbaru[] =array($nom
          ,$tbu->name
          ,idrole($tbu->level)
          ,$tbu->username
          ,$tbu->email);

          }


          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "lawyers/list": //cari json daftar lawyer
         $listidtabel=array();
         $listlawyer=array();

if($par_a==null || $par_a==0){
         $tb_lawyer=DB::table('tb_lawyer')
                      ->select(['id','idu','permenit'])
                     ->get();
}else{
  $tb_lawyer = DB::table('tb_jem_lawyer')
      ->join('tb_lawyer', 'tb_jem_lawyer.idl', '=', 'tb_lawyer.id')
      ->where('idj',$par_a)
      ->select(['tb_lawyer.id','tb_lawyer.idu','tb_lawyer.permenit'])
      ->get();
}
       $nom=0;
       $jum=count($tb_lawyer);
       if($jum>0){
        foreach($tb_lawyer as $tbl){
        $nom+=1;
        $listidtabel[]=$tbl->id;
          $idl=$tbl->id;
            $dafbid="";
          $tb_jem=DB::table('tb_jem_lawyer')
                      ->where('idl',$idl)
                      ->get();

                      foreach($tb_jem as $tbj){
                        $tb_jen=DB::table('tb_jenishukum')
                                    ->where('id',$tbj->idj)
                                    ->first();
                                    $dafbid.=$tb_jen->jenis.",";
                      }
                      $tb_user=DB::table('tb_user')
                                  ->where('id',$tbl->idu)
                                  ->first();
                      $bahanreturnbaru[] =array($nom
                      ,$tb_user->name
                      ,$dafbid
                      ,rupiah($tbl->permenit)
                      ,$tb_user->email);

         }


          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "lawyers/billingfee": // cari json billing fee
         $thn=$par_a;
         if($par_b!=null && $par_b!=0){
         $artikels=DB::table('tb_mutasi')
                    ->where('idl',$par_b)
                    ->whereRaw('YEAR(periode) = ?', [$thn])
                    ->get();
         }else{
         $artikels=DB::table('tb_mutasi')
                    ->whereRaw('YEAR(periode) = ?', [$thn])
                    ->get();
         }
           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $totshare=0;
          $totfee=0;
          $tot4tl=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_lawyer=DB::table('tb_lawyer')
                     ->where('id', $art->idl)
                     ->select(['id','idu','persenfee'])
                     ->first();

          $tb_user=DB::table('tb_user')
                    ->where('id', $tb_lawyer->idu)
                    ->select(['name'])
                    ->first();

          $periode=bulanIndo(date('m',strtotime($art->periode)));
          $fee=rupiah($art->fee);
          $totfee+=$art->fee;
          $share=rupiah($art->share);
          $totshare+=$art->share;
          $tl=rupiah($art->tl);
          $tot4tl+=$art->tl;
          $withdraw=rupiah($art->withdraw);
          $saldo=rupiah($art->saldo);

          $bahanreturnbaru[] =array($nom
          ,$periode
          ,$tb_user->name
          ,$fee
          ,$share
          ,$tl
          ,$withdraw
          ,$saldo);

          }


            $listnya["isifooter"]="Total pendapatan $par_a : ".rupiah($totfee).", untuk lawyer : ".rupiah($totshare)." untuk TanyaLawyer : ".rupiah($tot4tl);

          $listnya["list"]=$bahanreturnbaru;
        }else{
          $listnya["isifooter"]="";
        }
        $bahanreturn=json_encode($listnya);

         break;
         case "lawyers/logtelepon" : // cari json log telpon lawyers
         $thn=$par_a;
         $bln=$par_b;
         $idl=$par_c;
         $artikels=DB::table('tb_call_history')
                    ->where('idl',$idl)
                    ->where('seconds','!=',0)
                    ->whereRaw('YEAR(start) = ?', [$thn])
                    ->whereRaw('MONTH(start) = ?', [$bln])
                    ->get();

           if($idl==0){
           $artikels=DB::table('tb_call_history')
                      ->where('seconds','!=',0)
                      ->whereRaw('YEAR(start) = ?', [$thn])
                      ->whereRaw('MONTH(start) = ?', [$bln])
                      ->get();
           }
           $nom=0;
           $jum=count($artikels);
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_user=DB::table('tb_user')
                    ->where('idr',$art->idk)
                    ->select(['name'])
                    ->first();

          $tb_userlawyer=DB::table('tb_user')
                    ->where('idr',$art->idl)
                    ->select(['name'])
                    ->first();

          $totalbiaya=rupiah($art->bill);
          $tot+=$art->bill;
          $bahanreturnbaru[] =array($nom
          ,$art->start
          ,$tb_userlawyer->name
          ,$tb_user->name
          ,$art->seconds
          ,$totalbiaya);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $bahanreturn=json_encode($listnya);
          }
         break;
         case "lawyers/rating": // cari json rating lawyers
         $thn=$par_a;
         if($par_b!=null && $par_b!=0){
         $dafbulan=DB::table('tb_call_history')
                    ->select([DB::raw('month(start) as bulan')])
                    ->where('idl',$par_b)
                    ->where('seconds','!=',0)
                    ->whereRaw('YEAR(start) = ?', [$thn])
                    ->groupBy(DB::raw('month(start)'))
                    ->get();
         }else{
         $dafbulan=DB::table('tb_call_history')
                    ->select([DB::raw('month(start) as bulan')])
                    ->where('seconds','!=',0)
                    ->whereRaw('YEAR(start) = ?', [$thn])
                    ->groupBy(DB::raw('month(start)'))
                    ->get();
         }
           $nom=0;
           $jum=count($dafbulan);
           if($jum>0){
          $urlutama=url('/');
          $totshare=0;
          $totfee=0;
          $tot4tl=0;

          foreach($dafbulan as $isdafbul){


if($par_b!=null && $par_b!=0){
          $artikels=DB::table('tb_call_history')
                     ->select(['idl','idk','start'
                     ,DB::raw('sum(seconds) as seconds')
                     ,DB::raw('sum(bill) as bill')
                     ,DB::raw('avg(rating) as rating')
                     ])
                     ->whereRaw('MONTH(start) = ?', [$isdafbul->bulan])
                     ->whereRaw('YEAR(start) = ?', [$thn])
                     ->where('seconds','!=',0)
                     ->where('idl',$par_b)
                     ->groupBy('idl')
                     ->get();
}else{
          $artikels=DB::table('tb_call_history')
                     ->select(['idl','idk','start'
                     ,DB::raw('sum(seconds) as seconds')
                     ,DB::raw('sum(bill) as bill')
                     ,DB::raw('avg(rating) as rating')
                     ])
                     ->whereRaw('MONTH(start) = ?', [$isdafbul->bulan])
                     ->whereRaw('YEAR(start) = ?', [$thn])
                     ->where('seconds','!=',0)
                     ->groupBy('idl')
                     ->get();
}
                     foreach($artikels as $art){
                     $nom+=1;
          $tb_lawyer=DB::table('tb_lawyer')
                     ->where('id', $art->idl)
                     ->select(['id','idu','persenfee'])
                     ->first();

          $tb_user=DB::table('tb_user')
                    ->where('id', $tb_lawyer->idu)
                    ->select(['name'])
                    ->first();

          $periode=bulanIndo(date('m',strtotime($art->start)));
          $seconds=$art->seconds;

          $bahanreturnbaru[] =array($nom
          ,$periode
          ,$tb_user->name
          ,$seconds
          ,rupiah($art->bill)
          ,ceil($art->rating));

          }
                    }


            $listnya["isifooter"]="";

          $listnya["list"]=$bahanreturnbaru;
        }else{
          $listnya["isifooter"]="";
        }
        $bahanreturn=json_encode($listnya);
         break;
         case "callcenter/dokumentasi" : // cari json dokumentasi
         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels=DB::table('tb_dokumentasi')
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }

           if($status!=-1){
           $artikels->where('status', [$status]);
           }

           $artikels=$artikels
                     ->select(['id'
                     ,'idk'
                     ,'idj'
                     ,'status'
                     ,'tanggal'
                     ,DB::raw('SUBSTR(masalah,1,80) as masalah')])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Pending";
           $arstatus[1]="Waiting";
           $arstatus[2]="On progress";
           $arstatus[3]="Failed";
           $arstatus[4]="Success";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;
          $jenishukum="Belum ditentukan";

          if($art->idj!=0){
          $tb_jenishukum=DB::table('tb_jenishukum')
                        ->where('id',$art->idj)
                        ->first();
          $jenishukum=$tb_jenishukum->jenis;
          }

          $tb_user=DB::table('tb_user')
                        ->where('idr',$art->idk)
                        ->select(['name'])
                        ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$arstatus[$art->status]
          ,$tb_user->name
          ,$art->masalah."..."
          ,$jenishukum);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }
         break;
         case "callcenter/orderaktif" : // cari json order aktif
         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels = DB::table('tb_jem_cs')
         ->join('tb_dokumentasi','tb_dokumentasi.idj','=','tb_jem_cs.idj')
         ->where('tb_jem_cs.idcs',getUserInfo("user_roleid"));

         $artikels->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }

           if($status==-1){
           }else if($status ==1 || $status==2){
           $artikels->where('status', [$status]);
           }else{
             return "error";
           }



           $artikels=$artikels
                     ->select(['tb_dokumentasi.id'
                     ,'tb_dokumentasi.idk'
                     ,'tb_dokumentasi.idj'
                     ,'tb_dokumentasi.status'
                     ,'tb_dokumentasi.tanggal'
                     ,DB::raw('SUBSTR(tb_dokumentasi.masalah,1,80) as masalah')])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Pending";
           $arstatus[1]="Waiting";
           $arstatus[2]="On progress";
           $arstatus[3]="Failed";
           $arstatus[4]="Success";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;
          $jenishukum="Belum ditentukan";

          if($art->idj!=0){
          $tb_jenishukum=DB::table('tb_jenishukum')
                        ->where('id',$art->idj)
                        ->first();
          $jenishukum=$tb_jenishukum->jenis;
          }

          $tb_user=DB::table('tb_user')
                        ->where('idr',$art->idk)
                        ->select(['name'])
                        ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$arstatus[$art->status]
          ,$tb_user->name
          ,$art->masalah."..."
          ,$jenishukum);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }
         break;
         case "callcenter/arsiporder" : // cari json arsip order
         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels=DB::table('tb_dokumentasi')
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }

           if($status!=-1 && $status!=0 && $status!=1 && $status!=2){
           $artikels->where('status', [$status]);
         }else{
           $artikels->where( function ( $query )
               {
                 $query->where('status','=', 3)
                          ->orWhere('status','=', 4);
               });

         }

           $artikels=$artikels
                     ->select(['id'
                     ,'idk'
                     ,'idj'
                     ,'status'
                     ,'tanggal'
                     ,DB::raw('SUBSTR(masalah,1,80) as masalah')])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Pending";
           $arstatus[1]="Waiting";
           $arstatus[2]="On progress";
           $arstatus[3]="Failed";
           $arstatus[4]="Success";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;
          $jenishukum="Belum ditentukan";

          if($art->idj!=0){
          $tb_jenishukum=DB::table('tb_jenishukum')
                        ->where('id',$art->idj)
                        ->first();
          $jenishukum=$tb_jenishukum->jenis;
          }

          $tb_user=DB::table('tb_user')
                        ->where('idr',$art->idk)
                        ->select(['name'])
                        ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$arstatus[$art->status]
          ,$tb_user->name
          ,$art->masalah."..."
          ,$jenishukum);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }
         break;
         case "keuangan/pengajuantopup" : // cari json pengajuan top up

         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels=DB::table('tb_topup')
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }

           if($status!=-1){
           $artikels->where('status', [$status]);
           $artikels->where('status','!=', 3);
         }

           $artikels=$artikels
                     ->select(['id'
                     ,'idu'
                     ,'topup_amount'
                     ,'status'
                     ,'last_amount'
                     ,'create_at'])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Belum konfirm";
           $arstatus[1]="Sudah konfirm";
           $arstatus[2]="Ditolak";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_user=DB::table('tb_user')
                        ->where('id',$art->idu)
                        ->select(['name'])
                        ->first();

          $listidtabel[]=$art->id;
          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$tb_user->name
          ,rupiah($art->topup_amount)
          ,rupiah($art->last_amount)
          ,$arstatus[$art->status]);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/arsiptopup": // cari json arsip top up

         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels=DB::table('tb_topup')
                    ->where('status', 3)
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }


           $artikels=$artikels
                     ->select(['id'
                     ,'idu'
                     ,'topup_amount'
                     ,'status'
                     ,'last_amount'
                     ,'create_at'])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Belum konfirm";
           $arstatus[1]="Sudah konfirm";
           $arstatus[2]="Ditolak";
           $arstatus[3]="Diterima";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_user=DB::table('tb_user')
                        ->where('id',$art->idu)
                        ->select(['name'])
                        ->first();


          $listidtabel[]=$art->id;
          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$tb_user->name
          ,rupiah($art->topup_amount)
          ,rupiah($art->last_amount)
          ,$arstatus[$art->status]);


          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/saldodeposit": // cari json saldo deposit

         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;


            $tb_user=DB::table('tb_user')
                          ->select(['id','name','idsaldo'])
                          ->get();

           $nom=0;
           $jum=count($tb_user);
           $listidtabel=array();
           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($tb_user as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $tb_wallet=DB::table('tb_wallet')
                        ->where('id',$art->idsaldo)
                        ->select(['ecash'])
                        ->first();
          $uang=0;

          if($tb_wallet!=null){
            $uang=$tb_wallet->ecash;
          }

          $bahanreturnbaru[] =array($nom
          ,$art->name
          ,rupiah($uang));

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/transferfee": //cari json transferfee pengajuan withdraw

         $arstatus=array();
         $arstatus[-1]="Dibatalkan";
         $arstatus[0]="Menunggu";
         $arstatus[1]="Diproses";
         $arstatus[2]="Ditolak";
         $arstatus[3]="Selesai";

         $artikels=DB::table('tb_withdraw')
                    ->where('status',-1)
                    ->orWhere('status',0)
                    ->orWhere('status',1)
                    ->orWhere('status',2)
                    ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $tb_user=DB::table('tb_user')
          ->where('idr',$art->idl)
          ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,$tb_user->name
          ,rupiah($art->nilai)
          ,$arstatus[$art->status]
          ,$art->komenlawyer);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/transferfeeklien": //cari json transferfee klien pengajuan withdraw

         $arstatus=array();
         $arstatus[-1]="Dibatalkan";
         $arstatus[0]="Menunggu";
         $arstatus[1]="Diproses";
         $arstatus[2]="Ditolak";
         $arstatus[3]="Selesai";

         $artikels=DB::table('tb_withdrawkomisi')
                    ->where('status',-1)
                    ->orWhere('status',0)
                    ->orWhere('status',1)
                    ->orWhere('status',2)
                    ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $tb_user=DB::table('tb_user')
          ->where('idr',$art->idk)
          ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,$tb_user->name
          ,rupiah($art->nilai)
          ,$arstatus[$art->status]
          ,$art->user_comment);

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/arsiptransferfee": //cari json arsip transferfee pengajuan withdraw
         $thn=$par_a;
         $bln=$par_b;
         $idl=$par_c;
         $arstatus=array();
         $arstatus[-1]="Dibatalkan";
         $arstatus[0]="Menunggu";
         $arstatus[1]="Diproses";
         $arstatus[2]="Ditolak";
         $arstatus[3]="Selesai";

         $artikels=DB::table('tb_withdraw')
         ->where('status',3)
         ->whereRaw('YEAR(tanggalminta) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(tanggalminta) = ?', [$bln]);
         }
         if($idl!=0){
         $artikels->where('idl', [$idl]);
         }

         $artikels=$artikels->get();
           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $tb_user=DB::table('tb_user')
          ->where('idr',$art->idl)
          ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,$tb_user->name
          ,rupiah($art->nilai)  );

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/arsiptransferfeeklien": //cari json arsip transferfee pengajuan withdraw
         $thn=$par_a;
         $bln=$par_b;
         $idk=$par_c;
         $arstatus=array();
         $arstatus[-1]="Dibatalkan";
         $arstatus[0]="Menunggu";
         $arstatus[1]="Diproses";
         $arstatus[2]="Ditolak";
         $arstatus[3]="Selesai";

         $artikels=DB::table('tb_withdrawkomisi')
         ->where('status',3)
         ->whereRaw('YEAR(tanggalminta) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(tanggalminta) = ?', [$bln]);
         }
         if($idk!=0){
         $artikels->where('idk', [$idk]);
         }

         $artikels=$artikels->get();
           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $tb_user=DB::table('tb_user')
          ->where('idr',$art->idk)
          ->first();

          $bahanreturnbaru[] =array($nom
          ,$art->create_atminta
          ,$tb_user->name
          ,rupiah($art->nilai)  );

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "kontenapps/berita" : // cari json berita

         $thn=$par_a;
         $bln=$par_b;
         $artikels=DB::table('tb_berita')
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }

           $artikels=$artikels
                     ->select(['id'
                     ,'tanggal'
                     ,'judul' ])
                     ->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();


           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;
          $listidtabel[]=$art->id;

          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$art->judul
           );

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "product_category/list": //cari json daftar kategori
         $listidtabel=array();
         $listkategori=array();

         $tb_kategori=DB::table('tb_product_cat')
                      ->select(['id'
                      ,'category'
                      ,'info'
                      ,'color' ])
                     ->get();
       $nom=0;
       $jum=count($tb_kategori);
       if($jum>0){
        foreach($tb_kategori as $tbl){
        $nom+=1;
        $listidtabel[]=$tbl->id;
          $idl=$tbl->id;

                      $bahanreturnbaru[] =array($nom
                      ,$tbl->category
                      ,$tbl->info
                      ,$tbl->color
                    );

         }


          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "product/list" : // cari json list produk

         $paramcat=$par_a;
         $products=DB::table('tb_product')
         ->where('ids', '=', getUserInfo("user_id"))->get();

           $nom=0;
           $jum=count($products);
           $listidtabel=array();


           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($products as $prod){
          $nom+=1;
          $listkat="";
          $jemkategori = DB::table('tb_jem_product_cat')
          ->where('idp', '=', $prod->id)
          ->get();

          $bisamuncul=0;
          if($paramcat>0){
           foreach ($jemkategori as $jk) {
             $kategori = DB::table('tb_product_cat')
             ->where('id', '=', $jk->idpc)
             ->first();

               if($kategori->id==$paramcat){
                 $bisamuncul=1;
               }

             $listkat.=$kategori->category.", ";
           }
         }else{
          foreach ($jemkategori as $jk) {
            $kategori = DB::table('tb_product_cat')
            ->where('id', '=', $jk->idpc)
            ->first();
            $listkat.=$kategori->category.", ";
          }
            $bisamuncul=1;

         }

           if($bisamuncul==1){
           $listidtabel[]=$prod->id;
          $bahanreturnbaru[] =array($nom
          ,$prod->name
          ,$listkat
          ,$prod->price
           );
         }

          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "transport_service/list": //cari json jasa transportasi
         $listidtabel=array();
         $listkategori=array();

         $tb_transport_service=DB::table('tb_transport_service')
                     ->get();
       $nom=0;
       $jum=count($tb_transport_service);
       if($jum>0){
        foreach($tb_transport_service as $tbl){
        $nom+=1;
        $listidtabel[]=$tbl->id;
          $idl=$tbl->id;

                      $bahanreturnbaru[] =array($nom
                      ,$tbl->name
                      ,$tbl->detail
                    );

         }


          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "keuangan/transaksijualbeli": // cari json transaksi jual beli

         $thn=$par_a;
         $bln=$par_b;
         $status=$par_c;
         $artikels=DB::table('tb_transaksi')
                    ->whereRaw('YEAR(create_at) = ?', [$thn]);
         if($bln!=0){
         $artikels->whereRaw('MONTH(create_at) = ?', [$bln]);
          }


           $artikels=$artikels->get();

           $nom=0;
           $jum=count($artikels);
           $listidtabel=array();

           $arstatus=array();
           $arstatus[0]="Belum diantar";
           $arstatus[1]="Sedang diantar";
           $arstatus[2]="Sudah diterima";
           $arstatus[3]="Dicancel";

           if($jum>0){
          $urlutama=url('/');
          $tot=0;
          foreach($artikels as $art){
          $nom+=1;

          $tb_user_buyer=DB::table('tb_user')
                        ->where('id',$art->idb)
                        ->select(['name'])
                        ->first();
          $tb_user_seller=DB::table('tb_user')
                        ->where('id',$art->ids)
                        ->select(['name'])
                        ->first();


          $listidtabel[]=$art->id;
          $bahanreturnbaru[] =array($nom
          ,$art->create_at
          ,$tb_user_buyer->name
          ,$tb_user_seller->name
          ,rupiah($art->bill)
          ,rupiah($art->tax)
          ,rupiah($art->ongkir)
          ,rupiah($art->tax+$art->ongkir)
          ,$arstatus[$art->status]);


          }

          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;
         case "laporan/complain": //cari json complain
         $listidtabel=array();
         $listkategori=array();

         $tb_complain=DB::table('tb_complain')
                     ->get();
       $nom=0;
       $jum=count($tb_complain);
       if($jum>0){
        foreach($tb_complain as $tbl){
        $nom+=1;
        $listidtabel[]=$tbl->id;
          $idl=$tbl->id;

          $tb_user=DB::table('tb_user')
                        ->where('id',$tbl->idu)
                        ->select(['name'])
                        ->first();

                      $bahanreturnbaru[] =array($nom
                      ,$tbl->create_at
                      ,$tb_user->name
                      ,$tbl->message_title
                      ,$tbl->message_body
                    );

         }


          $listnya["isifooter"]="";
          $listnya["list"]=$bahanreturnbaru;
          $listnya["listidtabel"]=$listidtabel;
          $bahanreturn=json_encode($listnya);
          }

         break;

         //akhir cari json tabel


       }


       return $bahanreturn;

     }


     public function __construct(Pdf $pdf){
            $this->pdf = $pdf;
//Session::put('key', 'value');
//Session::get('key')tb_infomember
 /**
      return view()->share([
      'testimonials'=> $testimonials
      ,'services'=> $services
      ,'team'=> $team
      ,'faqs'=> $faqs
      ,'policies'=> $policies
      ,'kontaks'=> $kontaks
      ,'kategorisgaleri'=> $kategorisgaleri
      ,'user_pangkat'=> $user_pangkat
      ,'user_id'=> $user_id
      ,'dafmastertemplate'=> $dafmastertemplate
      ,'daftemplate'=> $daftemplate
      ,'notifairline'=> $notifairline
      ,'notiftrain'=> $notiftrain
      ,'notifhotel'=> $notifhotel
      ]);
      **/

      $idlogin=session('coklogin');
      $tb_user=DB::table('tb_user')
      ->where('id',$idlogin)
      ->first();

           return view()->share([
           'tb_user'=> $tb_user
           ]);

     	//return view()->share(['datakontak'=>$datakontak,'kategorisblog'=>$kategorisblog]);
     }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
