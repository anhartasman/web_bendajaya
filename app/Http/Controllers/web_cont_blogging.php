<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_blogging extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idartikel)
     {
         // validate the info, create rules for the inputs
         $rules = array(
             'judul'    => 'required', // make sure the username field is not empty
             'isi' => 'required' // make sure the username field is not empty

         );
         $isian=DB::table('tb_webcont_blog')
         ->where('id', '=', $idartikel)
         ->first();
            $tanggal=$isian->tanggal;
            $alamatredir='/admin/blogging/edit/'.date('Y',strtotime($tanggal)).'/'.date('m',strtotime($tanggal)).'/'.$idartikel;
         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);

         // if the validator fails, redirect back to the form
         if ($validator->fails()) {
           return Redirect::to($alamatredir)
                 ->withErrors($validator) // send back all errors to the login form
                 ->withInput(Input::except('judul')); // send back the input (not the password) so that we can repopulate the form
         } else {

           }


     DB::table('tb_webcont_blog')
->where('id', $idartikel)
->update(array(
            'judul' => Input::get('judul'),
            'isi' => Input::get('isi')
      ));


    return Redirect::to($alamatredir);

}

public function doErase($idartikel)
{


   DB::table('tb_webcont_blog')->where('id', '=', $idartikel)->delete();

 return Redirect::to('/admin/blogging/tambah');

}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'judul'    => 'required', // make sure the username field is not empty
        'isi' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/blogging/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('judul')); // send back the input (not the password) so that we can repopulate the form
    } else {


$idterakhir = DB::table('tb_webcont_blog')->insertGetId(
      array(
                  'tanggal' => date('Y-m-d h:i:s'),
                  'judul' => Input::get('judul'),
                  'isi' => Input::get('isi')
            )
            );
              return Redirect::to('/admin/blogging/view/'.date('Y').'/'.date('n').'/'.$idterakhir);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
