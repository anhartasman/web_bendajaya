<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_galeri extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idfoto)
     {

}

public function doErase($idfoto)
{


}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'nama'    => 'required', // make sure the username field is not empty
        'keterangan' => 'required', // make sure the username field is not empty
        'kategori' => 'required',
        'foto' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/elemenweb/galeri/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('nama')); // send back the input (not the password) so that we can repopulate the form
    } else {


$idterakhir = DB::table('tb_webcont_galeri')->insertGetId(
      array(
                  'namafoto' => Input::get('nama'),
                  'keterangan' => Input::get('keterangan'),
                  'kategori' => Input::get('kategori'),
                  'gambar'  => ""
            )
            );


            $destinationPath = 'uploads/images'; // upload path
            $extension = Input::file('foto')->getClientOriginalExtension(); // getting image extension
            //$fileName = rand(11111,99999).'.'.$extension; // renameing image
            $fileName = "isigaleri".$idterakhir.'.'.$extension;
            Input::file('foto')->move($destinationPath, $fileName); // uploading file to given path
            // sending back with message


                 DB::table('tb_webcont_galeri')
            ->where('id', $idterakhir)
            ->update(array(
                        'gambar' => $fileName
                  ));


              return Redirect::to('/admin/elemenweb/galeri/view/'.$idterakhir);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
