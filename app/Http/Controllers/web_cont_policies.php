<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_policies extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idpolicy)
     {
         // validate the info, create rules for the inputs
         $rules = array(
             'judul'    => 'required', // make sure the username field is not empty
             'isi' => 'required' // make sure the username field is not empty

         );

         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);

         // if the validator fails, redirect back to the form
         if ($validator->fails()) {
           return Redirect::to('/admin/infoweb/policies/edit/'.$idpolicy)
                 ->withErrors($validator) // send back all errors to the login form
                 ->withInput(Input::except('judul')); // send back the input (not the password) so that we can repopulate the form
         } else {


           }


     DB::table('tb_webcont_policies')
->where('id', $idpolicy)
->update(array(
            'judul' => Input::get('judul'),
            'isi' => Input::get('isi')
      ));


    return Redirect::to('/admin/infoweb/policies/view/'.$idpolicy);

}

public function doErase($idpolicy)
{

   DB::table('tb_webcont_policies')->where('id', '=', $idpolicy)->delete();

 return Redirect::to('/admin/infoweb/policies/tambah');

}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'judul'    => 'required', // make sure the username field is not empty
        'isi' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/infoweb/policies/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('judul')); // send back the input (not the password) so that we can repopulate the form
    } else {



$idterakhir = DB::table('tb_webcont_policies')->insertGetId(
      array(
                  'judul' => Input::get('judul'),
                  'isi' => Input::get('isi')
            )
            );
              return Redirect::to('/admin/infoweb/policies/view/'.$idterakhir);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
