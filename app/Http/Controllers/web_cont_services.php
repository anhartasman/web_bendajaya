<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_services extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idservis)
     {
         // validate the info, create rules for the inputs
         $rules = array(
             'namaservis'    => 'required', // make sure the username field is not empty
             'keteranganservis' => 'required' // make sure the username field is not empty

         );

         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);

         // if the validator fails, redirect back to the form
         if ($validator->fails()) {
           return Redirect::to('/admin/infoweb/services/edit/'.$idservis)
                 ->withErrors($validator) // send back all errors to the login form
                 ->withInput(Input::except('namaservis')); // send back the input (not the password) so that we can repopulate the form
         } else {
           $nomid=$idservis;

             // create our user data for the authentication
             $userdata = array(
                 'namaservis'  => Input::get('namaservis'),
                 'keteranganservis'  => Input::get('keteranganservis'),
                 'logoservis'  => Input::get('logoservis')
             );
           }
     echo "namaservis : ".$userdata['namaservis']."<br>";
     echo "keteranganservis : ".$userdata['keteranganservis'];

if(Input::file('logoservis')!=null){


             $destinationPath = 'uploads/images'; // upload path
             $extension = Input::file('logoservis')->getClientOriginalExtension(); // getting image extension
             //$fileName = rand(11111,99999).'.'.$extension; // renameing image
             $fileName = "gambarservis".$nomid.'.'.$extension;
             Input::file('logoservis')->move($destinationPath, $fileName); // uploading file to given path
             // sending back with message


     DB::table('tb_webcont_services')
->where('id', $idservis)
->update(array(
            'namaservis' => Input::get('namaservis'),
            'keteranganservis' => Input::get('keteranganservis'),
            'gambar' => $fileName
      ));
}else{
     DB::table('tb_webcont_services')
->where('id', $idservis)
->update(array(
            'namaservis' => Input::get('namaservis'),
            'keteranganservis' => Input::get('keteranganservis')
      ));
}


    return Redirect::to('/admin/infoweb/services/edit/'.$idservis);

}

public function doErase($idservis)
{
          $isian=DB::table('tb_webcont_services')
          ->where('id', '=', $idservis)
          ->first();

             $namafilegambar=$isian->gambar;
             $alamatfile=public_path('uploads/images/'.$namafilegambar);

  echo $alamatfile;
 unlink($alamatfile);

      DB::table('tb_webcont_services')->where('id', '=', $idservis)->delete();

return Redirect::to('/admin/infoweb/services/tambah');

}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'namaservis'    => 'required', // make sure the username field is not empty
        'keteranganservis' => 'required', // make sure the username field is not empty
        'logoservis' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/infoweb/services/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('namaservis')); // send back the input (not the password) so that we can repopulate the form
    } else {

        // create our user data for the authentication
        $userdata = array(
            'namaservis'  => Input::get('namaservis'),
            'keteranganservis'  => Input::get('keteranganservis'),
            'logoservis'  => Input::get('logoservis')
        );
echo "namaservis : ".$userdata['namaservis']."<br>";
echo "keteranganservis : ".$userdata['keteranganservis'];


$idterakhir=DB::table('tb_webcont_services')->insertGetId(
      array(
                  'namaservis' => Input::get('namaservis'),
                  'keteranganservis' => Input::get('keteranganservis'),
                  'gambar'  => ""
            )
            );

                  $destinationPath = 'uploads/images'; // upload path
                  $extension = Input::file('logoservis')->getClientOriginalExtension(); // getting image extension
                  //$fileName = rand(11111,99999).'.'.$extension; // renameing image
                  $fileName = "gambarservis".$idterakhir.'.'.$extension;
                  Input::file('logoservis')->move($destinationPath, $fileName); // uploading file to given path
                  // sending back with message


                                   DB::table('tb_webcont_services')
                              ->where('id', $idterakhir)
                              ->update(array(
                                          'gambar' => $fileName
                                    ));


              return Redirect::to('/admin/infoweb/services/view/'.$idterakhir);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
