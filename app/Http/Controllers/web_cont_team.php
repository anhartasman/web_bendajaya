<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_team extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idteam)
     {
         // validate the info, create rules for the inputs
         $rules = array(
             'namaanggota'    => 'required', // make sure the username field is not empty
             'jabatananggota' => 'required' // make sure the username field is not empty

         );

         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);

         // if the validator fails, redirect back to the form
         if ($validator->fails()) {
           return Redirect::to('/admin/infoweb/team/edit/'.$idteam)
                 ->withErrors($validator) // send back all errors to the login form
                 ->withInput(Input::except('namaanggota')); // send back the input (not the password) so that we can repopulate the form
         } else {
           $nomid=$idteam;

           }

if(Input::file('fotoanggota')!=null){


             $destinationPath = 'uploads/images'; // upload path
             $extension = Input::file('fotoanggota')->getClientOriginalExtension(); // getting image extension
             //$fileName = rand(11111,99999).'.'.$extension; // renameing image
             $fileName = "fototeam".$nomid.'.'.$extension;
             Input::file('fotoanggota')->move($destinationPath, $fileName); // uploading file to given path
             // sending back with message


     DB::table('tb_webcont_team')
->where('id', $idteam)
->update(array(
            'namaanggota' => Input::get('namaanggota'),
            'jabatananggota' => Input::get('jabatananggota'),
            'gambar' => $fileName
      ));
}else{
     DB::table('tb_webcont_team')
->where('id', $idteam)
->update(array(
            'namaanggota' => Input::get('namaanggota'),
            'jabatananggota' => Input::get('jabatananggota')
      ));
}


    return Redirect::to('/admin/infoweb/team/view/'.$idteam);

}

public function doErase($idteam)
{

          $isian=DB::table('tb_webcont_team')
          ->where('id', '=', $idteam)
          ->first();

             $namafilegambar=$isian->gambar;
             $alamatfile=public_path('uploads/images/'.$namafilegambar);

  echo $alamatfile;
 unlink($alamatfile);
   DB::table('tb_webcont_team')->where('id', '=', $idteam)->delete();

 return Redirect::to('/admin/infoweb/team/tambah');

}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'namaanggota'    => 'required', // make sure the username field is not empty
        'jabatananggota' => 'required', // make sure the username field is not empty
        'fotoanggota' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/infoweb/team/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('namaanggota')); // send back the input (not the password) so that we can repopulate the form
    } else {


        // create our user data for the authentication
        $userdata = array(
            'namaanggota'  => Input::get('namaanggota'),
            'jabatananggota'  => Input::get('jabatananggota'),
            'fotoanggota'  => Input::get('fotoanggota')
        );
echo "namaanggota : ".$userdata['namaanggota']."<br>";
echo "jabatananggota : ".$userdata['jabatananggota'];




$idterakhir = DB::table('tb_webcont_team')->insertGetId(
      array(
                  'namaanggota' => Input::get('namaanggota'),
                  'jabatananggota' => Input::get('jabatananggota'),
                  'gambar'  => ""
            )
            );


            $destinationPath = 'uploads/images'; // upload path
            $extension = Input::file('fotoanggota')->getClientOriginalExtension(); // getting image extension
            //$fileName = rand(11111,99999).'.'.$extension; // renameing image
            $fileName = "fototeam".$idterakhir.'.'.$extension;
            Input::file('fotoanggota')->move($destinationPath, $fileName); // uploading file to given path
            // sending back with message


                 DB::table('tb_webcont_team')
            ->where('id', $idterakhir)
            ->update(array(
                        'gambar' => $fileName
                  ));


              return Redirect::to('/admin/infoweb/team/view/'.$idterakhir);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
