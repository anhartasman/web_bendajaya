<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use DB;

class web_cont_testimonials extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
     {

         $this->middleware('tesAdminLogin', ['only' => ['doAdd','doSave','doErase']]);//this will applies only show,update methods of your controller

     }
          public function doSave($idtestimonial)
     {
         // validate the info, create rules for the inputs
         $rules = array(
             'sumbertesti'    => 'required', // make sure the username field is not empty
             'isitesti' => 'required' // make sure the username field is not empty

         );

         // run the validation rules on the inputs from the form
         $validator = Validator::make(Input::all(), $rules);

         // if the validator fails, redirect back to the form
         if ($validator->fails()) {
           return Redirect::to('/admin/infoweb/testimonials/edit/'.$idtestimonial)
                 ->withErrors($validator) // send back all errors to the login form
                 ->withInput(Input::except('namaservis')); // send back the input (not the password) so that we can repopulate the form
         } else {


             // create our user data for the authentication
             $userdata = array(
                 'sumbertesti'  => Input::get('sumbertesti'),
                 'isitesti'  => Input::get('isitesti')
             );
           }
     echo "sumbertesti : ".$userdata['sumbertesti']."<br>";
     echo "isitesti : ".$userdata['isitesti'];

     DB::table('tb_webcont_testimonials')
->where('id', $idtestimonial)
->update(array(
            'sumbertesti' => Input::get('sumbertesti'),
            'isitesti' => Input::get('isitesti')
      ));


    return Redirect::to('/admin/infoweb/testimonials/edit/'.$idtestimonial);

}

public function doErase($idtestimonial)
{
      DB::table('tb_webcont_testimonials')->where('id', '=', $idtestimonial)->delete();

return Redirect::to('/admin/infoweb/testimonials/tambah');

}
     public function doAdd()
{
    // validate the info, create rules for the inputs
    $rules = array(
        'sumbertesti'    => 'required', // make sure the username field is not empty
        'isitesti' => 'required'
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
      return Redirect::to('/admin/infoweb/testimonials/tambah')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('sumbertesti')); // send back the input (not the password) so that we can repopulate the form
    } else {

        $nomid=0;
        if(count(DB::table('tb_webcont_testimonials')->orderBy('id', 'desc')->get())>0){
        $nomid=DB::table('tb_webcont_testimonials')->orderBy('id', 'desc')->first()->id;
        }
        $nomid+=1;

        // create our user data for the authentication
        $userdata = array(
            'sumbertesti'  => Input::get('sumbertesti'),
            'isitesti'  => Input::get('isitesti')
        );
echo "sumbertesti : ".$userdata['sumbertesti']."<br>";
echo "isitesti : ".$userdata['isitesti'];



DB::table('tb_webcont_testimonials')->insert(
      array(
                  'sumbertesti' => Input::get('sumbertesti'),
                  'isitesti' => Input::get('isitesti')
            )
            );

              return Redirect::to('/admin/infoweb/testimonials/view/'.$nomid);


    }
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
