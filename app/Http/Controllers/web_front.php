<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

use League\Glide\Server;
use League\Glide\ServerFactory;
use Validator;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Mail;
use DB;
use View;
use DateTime;

class web_front extends Controller
{
  public function pageTesSalt($sandi){
     $salt="garammanis".rand(1,999);
     $password = hash('sha512', $sandi . $salt);
     echo "sandi : ".$sandi."<br>";
     echo "salt : ".$salt."<br>";
     echo "hasil : ".$password."<br>";
     /* A uniqid, like: 4b3403665fea6 */
     printf("uniqid(): %s\r\n", uniqid());
  }
       public function pageLogin()
       {
         return view('hal_admin.login');
       }

       public function doLogIn(Request $request)
       {

              $rules = array(
                  'username' => 'required', // make sure the username field is not empty
                  'password' => 'required|min:3', // make sure the username field is not empty
                  'captcha' => 'required|captcha' // password can only be alphanumeric and has to be greater than 3 characters
              );
              $messsages = array(
                    'username.required'=>'Username empty',
                    'password.required'=>'Password empty',
                    'captcha.required'=>'Captcha empty',
                    'captcha.captcha'=>'Captcha wrong',
              );
              // run the validation rules on the inputs from the form
              $validator = Validator::make(Input::all(), $rules,$messsages);

              // if the validator fails, redirect back to the form
              if ($validator->fails()) {
                  return Redirect::to('/admin/login')
                      ->withErrors($validator); // send back the input (not the password) so that we can repopulate the form
              } else {

                  // create our user data for the authentication
                  $userdata = array(
                      'username'  => Input::get('username'),
                      'password'  => Input::get('password')
                  );
          echo "username :".$userdata['username'];
          echo "password :".$userdata['password'];

          $datapertama=DB::table('tb_user')
          ->where('username', '=', $userdata['username'])
          ->first();
          $arsalah=array('sdsdsd','adadad');

          if (!$datapertama) {
              return Redirect::to('/admin/login')
                  ->withErrors("username salah"); // send back the input (not the password) so that we can repopulate the form
          }

          $hasilhash = hash('sha512', $userdata['password'] . $datapertama->salt);
              $isian=DB::table('tb_user')
              ->where('username', '=', $userdata['username'])
              ->where('password', '=', $hasilhash)
              ->first();
              if(count($isian)==1){

              //\Cookie::queue('coklogin', $isian->id);

              session(['coklogin'=>$isian->id]);

              $infomember = new \stdClass();
              $infomember->user_pangkat=$isian->level;
              $infomember->user_id=$isian->id;
              $infomember->user_username=$isian->username;
              $infomember->user_nama=$isian->name;

/**
                if($isian->level==1){
                  $tabelklien=DB::table('tb_klien')
                  ->where('idu',$isian->id)
                  ->select(['id'])
                  ->first();
                  $infomember->user_roleid=$tabelklien->id;
                }else if($isian->level==2){
                    $tabellawyer=DB::table('tb_lawyer')
                    ->where('idu',$isian->id)
                    ->select(['id'])
                    ->first();
                    $infomember->user_roleid=$tabellawyer->id;
                }else if($isian->level==3){
                    $tabelcs=DB::table('tb_cs')
                    ->where('idu',$isian->id)
                    ->select(['id'])
                    ->first();
                    $infomember->user_roleid=$tabelcs->id;
                }
**/

                session(['pangkatuser'=>$isian->level]);
                //\Cookie::queue('infomember', $infomember);
                session(['infomember'=>$infomember]);
                  //Cookie::make('keybaru', $userdata['username']);
              //return $isian;
              return Redirect::to('/admin');
            }else{
                return Redirect::to('/admin/login')
                    ->withErrors("Password salah");
            }
            }


       }
       public function servisAPI()
       {
         $json = file_get_contents('php://input');
         $obj = json_decode($json);
         $modul=$obj->module;
         $action=$obj->action;
         $dataret=array();
         $dataret["module"]=$modul;
         $dataret["action"]=$action;
         $dataret["error_code"]="000";
         $dataret["error_msg"]="";
         $sesivalid=1;
         $get_date_time=date("Y-m-d H:i:s");
         $get_date=date("Y-m-d");

         switch ($modul){
           case "global":
              switch($action){
                case "login":
                if((! isset($obj->username)) || (! isset($obj->password))){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $role="klien";
                  $cari=DB::table('tb_user')
                  ->where('username', $obj->username)
                  ->first();
                  $bisalogin=0;
                  if($cari!=null){
                  $salt=$cari->salt;

                  $hashdariuser = hash('sha512',$obj->password . $salt);
                  $tabel=DB::table('tb_user')
                  ->where('username',$obj->username)
                  ->where('password', $hashdariuser)
                  ->first();
                    $dataret["jumtabel"]=count($tabel);


                  if(count($tabel)==1){
                  $bisalogin=1;
                  }
                  }

                    if($bisalogin==0){
                      $dataret["error_code"]="001";
                      $dataret["error_msg"]="Login failed";
                    }else{
                      switch($tabel->level){
                        case 1:
                        $role="buyer";
                        break;
                        case 2:
                        $role="Driver";
                        break;
                        case 3:
                        $role="Seller";
                        break;
                        case 4:
                        $role="Administrator";
                        break;
                      }

                      $sesid=0;

                    $bahansessid = hash('sha512', $obj->username . $obj->password.$sesid);

                    DB::table('tb_user')
                   ->where('id', $tabel->id)
                   ->update(array(
                     'sessid' => $bahansessid
                   ));

                   $tblwallet=DB::table('tb_wallet')
                   ->where('id', $tabel->idsaldo)
                   ->first();

                    $bahandata=array();
                    $bahandata['id']=$tabel->id;
                    $bahandata['sessid']=$bahansessid;
                    $bahandata['username']=$obj->username;
                    $bahandata['name']=$tabel->name;
                    $bahandata['email']=$tabel->email;
                    $bahandata['rolehost']=$role;
                    $bahandata['phone']=$tabel->phone;
                    $bahandata['address']=$tabel->address;
                    $bahandata['ecash']=$tblwallet->ecash;
                    $dataret["respon_data"]=$bahandata;

                     }
                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "register":
                if((! isset($obj->username)) || (! isset($obj->name)) || (! isset($obj->email)) || (! isset($obj->password))){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{

                  $cari=DB::table('tb_user')
                  ->where('username', $obj->username)
                  ->where('password', $obj->password)
                  ->first();
                  $salt="garammanis".rand(1,999);
                  $hashdariuser = hash('sha512',$obj->password . $salt);


                    if($cari!=null){
                      $dataret["error_code"]="001";
                      $dataret["error_msg"]="Username sudah ada";
                    }else if($cari==null){

                    $idw=DB::table('tb_wallet')
                    ->insertGetId(array(
                           'ecash'=>0
                           ,'detail' =>"Belong to $obj->name"
                     ));

                     $arin=array(
                                'name' => $obj->name
                                ,'email' => $obj->email
                                ,'username' => $obj->username
                                ,'password' => $hashdariuser
                                ,'salt' => $salt
                                ,'idsaldo' => $idw
                                ,'level' => 1
                                ,'create_at' => date("Y-m-d H:i:s")
                                ,'last_update' => date("Y-m-d H:i:s")
                          );

                      $iduser = DB::table('tb_user')->insertGetId($arin);

                      $idtr=DB::table('tb_rekening')
                      ->insertGetId(array(
                             'idu'=>$iduser
                       ));

                       DB::table('tb_user')
                       ->where('id', $iduser)
                       ->update(array(
                         'idr' => $idtr
                       ));

                    $dataret["respon_data"]="REGISTER BERHASIL";

                     }
                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "update_account":
                if((! isset($obj->sessid)) || (! isset($obj->phone)) || (! isset($obj->username)) || (! isset($obj->name)) || (! isset($obj->email)) || (! isset($obj->password)) || (! isset($obj->passwordupdate)) || (! isset($obj->confirmpasswordupdate)) || (! isset($obj->address))){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();

                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $salt=$tabelsessid->salt;
                    $hashpasswordlama = hash('sha512',$obj->password . $salt);
                    $hashpasswordupdate = hash('sha512',$obj->passwordupdate . $salt);
                    $hashconfirmpasswordupdate = hash('sha512',$obj->confirmpasswordupdate . $salt);

                   $arupdate=array(
                              'name' => $obj->name
                              ,'email' => $obj->email
                              ,'username' => $obj->username
                              ,'phone' => $obj->phone
                              ,'address' => $obj->address
                              ,'last_update' => date("Y-m-d H:i:s")
                        );
                   if(strlen($obj->password)>0){
                     $tabelcari=DB::table('tb_user')
                     ->where('password',$hashpasswordlama)
                     ->where('sessid',$obj->sessid)
                     ->first();

                     if($tabelcari==null){
                       $dataret["error_code"]="001";
                       $dataret["error_msg"]="Password tidak valid";
                     }else{

                       if(strlen($obj->passwordupdate)>0){
                          if($obj->passwordupdate==$obj->confirmpasswordupdate){
                            $arupdate["password"]=$hashpasswordupdate;
                          }
                       }
                     }
                   }

                  $tbladdress=DB::table('tb_address')
                  ->where('idu',$tabelsessid->id)
                  ->where('address',$tabelsessid->address)
                  ->where('aktif',1)
                  ->first();
                  if($tbladdress!=null){
                    $arupdateaddress=array(
                      'address' => $obj->address
                    );
                    DB::table('tb_address')
                    ->where('id', $tbladdress->id)
                    ->update($arupdateaddress);

                  }else{
                    $arisiaddress=array(
                      'idu' => $tabelsessid->id
                      ,'label' => "Rumah ".$tabelsessid->name
                      ,'address' => $obj->address
                      ,'aktif' => 1
                    );
                    DB::table('tb_address')->insertGetId($arisiaddress);
                  }

                  if($dataret["error_code"]=="000"){
                  DB::table('tb_user')
                  ->where('id', $tabelsessid->id)
                  ->update($arupdate);
                 }
                       $bahandata['info']="";
                        $dataret["respon_data"]=$bahandata;
                  }

                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "logout":
                if((! isset($obj->sessid))){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();

                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $namatabel="klien";
                    if($tabelsessid->level==2){
                      $namatabel="lawyer";
                    }
                    DB::table('tb_user')
                   ->where('id', $tabelsessid->id)
                   ->update(array(
                     'sessid' => ''
                       ));
                    DB::table('tb_'.$namatabel)
                   ->where('idu', $tabelsessid->id)
                   ->update(array(
                     'token' => 0
                       ));

                       $bahandata['info']="Token berhasil dihapus";
                        $dataret["respon_data"]=$bahandata;
                  }

                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "getAccount":
                if( (! isset($obj->sessid)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $tb_rekening=DB::table('tb_rekening')
                    ->where('idu',$tabelsessid->id)
                    ->first();

                    $tblwallet=DB::table('tb_wallet')
                    ->where('id', $tabelsessid->idsaldo)
                    ->first();

                    $bahandata['id']=$tabelsessid->id;
                    $bahandata['sessid']=$tabelsessid->sessid;
                    $bahandata['username']=$tabelsessid->username;
                    $bahandata['name']=$tabelsessid->name;
                    $bahandata['email']=$tabelsessid->email;
                    $bahandata['rolehost']=idrole($tabelsessid->level);
                    $bahandata['phone']=$tabelsessid->phone;
                    $bahandata['address']=$tabelsessid->address;
                    $bahandata['ecash']=$tblwallet->ecash;
                    $bahandata["namabank"]=$tb_rekening->namabank;
                    $bahandata["nomrek"]=$tb_rekening->nomrek;
                    $bahandata["atasnama"]=$tb_rekening->atasnama;
                    $dataret["respon_data"]=$bahandata;
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                case "getAddress":
                if( (! isset($obj->sessid)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $tbladdress=DB::table('tb_address')
                    ->where('idu',$tabelsessid->id)
                    ->where('aktif',1)
                    ->get();

                    $dataret["respon_data"]=$tbladdress;

                  }

                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "addAddress":
                if( (! isset($obj->sessid)) || (! isset($obj->address_title)) ||  (! isset($obj->address)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $datain = array(
                        'label'    => $obj->address_title
                        ,'address'    => $obj->address
                        ,'idu'    => $tabelsessid->id
                        ,'aktif'    => 1
                        ,'create_at' => date("Y-m-d H:i:s")
                        ,'last_update' => date("Y-m-d H:i:s")
                    );

                   $cektbladdress=DB::table('tb_address')
                   ->where('idu',$tabelsessid->id)
                   ->where('address',$obj->address)
                   ->get();
                   if(count($cektbladdress)==0){
                     $ida = DB::table('tb_address')->insertGetId($datain);
                   }else{
                     DB::table('tb_address')
                     ->where('id', $cektbladdress[0]->id)
                     ->update($datain);
                   }
                    $dataret["respon_data"]="Berhasil nambah data";


                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "editAddress":
                if( (! isset($obj->sessid)) || (! isset($obj->address_id)) || (! isset($obj->address_title)) ||  (! isset($obj->address)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $datain = array(
                        'label'    => $obj->address_title
                        ,'address'    => $obj->address
                        ,'last_update' => date("Y-m-d H:i:s")
                    );

                     DB::table('tb_address')
                     ->where('id', $obj->address_id)
                     ->update($datain);

                    $dataret["respon_data"]="Berhasil edit data";


                  }
                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "deleteAddress":
                if( (! isset($obj->sessid)) || (! isset($obj->address_id)) || (! isset($obj->address_title)) ||  (! isset($obj->address)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $terpakai=DB::table('tb_transaksi')
                    ->where('ida', $obj->address_id)
                    ->get();
                    if(count($terpakai)>0){
                     $datain = array(
                         'aktif'    => 0
                         ,'last_update' => date("Y-m-d H:i:s")
                     );
                     DB::table('tb_address')
                     ->where('id', $obj->address_id)
                     ->update($datain);
                    }else{
                     DB::table('tb_address')
                     ->where('id', $obj->address_id)
                     ->delete();

                     }
                    $dataret["respon_data"]="Berhasil hapus data";


                  }
                }

                $hasil=json_encode($dataret);
                echo $hasil;
                break;
                case "get_system_config":
                    $dafconfig=array();

                    $tblconfig=DB::table('tb_system_config')
                    ->get();

                    foreach($tblconfig as $tc){
                      $dafconfig[$tc->label]=$tc->value;
                    }

                    $dataret["respon_data"]=$dafconfig;

                $hasil=json_encode($dataret);

                echo $hasil;
                break;
                case "get_transport_services":

                    $tb_transport_service=DB::table('tb_transport_service')
                    ->get();

                    $dataret["respon_data"]=$tb_transport_service;

                $hasil=json_encode($dataret);

                echo $hasil;
                break;
                case "setprofil":
                if( (! isset($obj->sessid)) || (! isset($obj->email)) || (! isset($obj->namabank)) || (! isset($obj->nomorrekening)) || (! isset($obj->atasnama)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  $tabelsessid=DB::table('tb_user')
                  ->where('level',1)
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $bahandata["email"]=$tabelsessid->email;

                    DB::table('tb_user')
                    ->where('id',$tabelsessid->id)
                    ->update(array(
                           'email' => $obj->email
                     ));
                    DB::table('tb_rekening')
                    ->where('idu',$tabelsessid->id)
                    ->update(array(
                           'namabank' => $obj->namabank
                            ,'nomrek' => $obj->nomorrekening
                            ,'atasnama' => $obj->atasnama
                     ));

                    $bahandata["email"]=$obj->email;
                    $bahandata["namabank"]=$obj->namabank;
                    $bahandata["nomrek"]=$obj->nomorrekening;
                    $bahandata["atasnama"]=$obj->atasnama;
                    $dataret["respon_data"]=$bahandata;
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "get_product_categories":
                //if( (! isset($obj->sessid)) ){


                    $tabelkategori=DB::table('tb_product_cat')
                    ->get();

                    $dataret["respon_data"]=$tabelkategori;

                $hasil=json_encode($dataret);
                $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "get_products":
                if( (! isset($obj->category_id)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{
                  /**
                  $tabelsessid=DB::table('tb_user')
                  ->where('level',1)
                  ->where('sessid',$obj->sessid)
                  ->first();
                  **/
                    $tabelsessid=DB::table('tb_user')
                    ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $daftarproduk = array();

                    $tbljem=DB::table('tb_jem_product_cat')
                    ->where('idpc',$obj->category_id)
                    ->get();

                    foreach($tbljem as $key) {
                      $tabelproduk=DB::table('tb_product')
                      ->where('id',$key->idp)
                      ->first();
                      $daftarkategori = array();

                        $tbljemproduk=DB::table('tb_jem_product_cat')
                        ->where('idp',$tabelproduk->id)
                        ->get();

                        foreach($tbljemproduk as $jemproduk) {
                          $tabelkategori=DB::table('tb_product_cat')
                          ->where('id',$jemproduk->idpc)
                          ->first();
                          $daftarkategori[]=$tabelkategori;
                        }
                        $tabelproduk->categories=$daftarkategori;

                      $daftarproduk[]=$tabelproduk;
                    }

                    $dataret["respon_data"]=$daftarproduk;
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "get_product_detail":
                if( (! isset($obj->product_id)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{

                    $tabelsessid=DB::table('tb_user')
                    ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $tabelproduk=DB::table('tb_product')
                    ->where('id',$obj->product_id)
                    ->first();
                    $daftarkategori = array();

                    $tbljem=DB::table('tb_jem_product_cat')
                    ->where('idp',$obj->product_id)
                    ->get();

                    foreach($tbljem as $key) {
                      $tabelkategori=DB::table('tb_product_cat')
                      ->where('id',$key->idpc)
                      ->first();
                      $daftarkategori[]=$tabelkategori;
                    }
                    $tabelproduk->categories=$daftarkategori;

                    $tb_seller=DB::table('tb_user')
                    ->select(['name'])
                    ->where('id',$tabelproduk->ids)
                    ->first();

                    $tabelproduk->seller_name=$tb_seller->name;
                    $dataret["respon_data"]=$tabelproduk;
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "get_profile":
                if( (! isset($obj->profile_id)) || (! isset($obj->sessid)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();

                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                    $tblprofil=DB::table('tb_user')
                    ->where('id',$obj->profile_id)
                    ->select(['id'])
                    ->first();

                    $dataret["respon_data"]=func_get_profil($tblprofil->id);
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
                case "submit_complain":
                if( (! isset($obj->sessid)) || (! isset($obj->message_title)) || (! isset($obj->message_body)) ){
                $dataret["error_code"]="001";
                $dataret["error_msg"].="Parameter tidak lengkap.";
                }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();

                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                      $arin=array(
                                 'idu' => $tabelsessid->id
                                 ,'message_title' => $obj->message_title
                                 ,'message_body' => $obj->message_body
                                 ,'create_at' => date("Y-m-d H:i:s")
                                 ,'last_update' => date("Y-m-d H:i:s")
                           );

                    $idc=DB::table('tb_complain')->insertGetId($arin);

                    $dataret["respon_data"]="BERHASIL SUBMIT COMPLAIN";
                  }
                }

                $hasil=json_encode($dataret);
                $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
                $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);


                echo $hasil;
                break;
              }
           break;
           //akhir case global
           case "transaction":
           switch($action){
             case "tesasal":

             $taxnum=func_get_system("tax");
             echo $taxnum;
             break;
              case "checkout":
/**
              {"action":"checkout","module":"transaction","product_order":{"address_id":"1","buyer":"Udin","comment":"asdasd","created_at":1533032385770,"date_ship":1533032374358,"last_update":1533032385770,"phone":"08754646944","serial":"69bbde1f","shipping":"Ojek Bendajaya","status":"WAITING","tax":10.0,"total_fees":15500.0},"product_order_detail":[{"amount":1,"created_at":1533032385771,"last_update":1533032385771,"price_item":5000.0,"product_id":5,"product_name":"Jagung"}]}
    --> END POST (467-byte body)
**/
              if(
                (! isset($obj->sessid))
              || (! isset($obj->product_order))
              || (! isset($obj->product_order_detail))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

            $tabelsessid=DB::table('tb_user')
            ->where('sessid',$obj->sessid)
            ->first();
            if($tabelsessid==null){
              $dataret["error_code"]="001";
              $dataret["error_msg"]="Invalid session";
            }else{
              $dafseller=array();
              $daftrxseller=array();
              $keranjang=$obj->product_order_detail;
              $keterangantransaksi=$obj->product_order;
              $bahandata["pesan"]="BERHASIL INPUT TRANSAKSI";

              foreach($keranjang as $k){
                $tbpk=DB::table('tb_product')
                ->where('id',$k->product_id)
                ->first();
                if (! in_array($tbpk->ids, $dafseller)){
                  $dafseller[]=$tbpk->ids;
                  $objtrxseller= new \stdClass();
                  $objtrxseller->ids=$tbpk->ids;
                  $objtrxseller->keranjang=array();
                  $objtrxseller->keranjang[]=$k;
                  $objtrxseller->dafid=array();
                  $objtrxseller->dafid[]=$k->product_id;
                  $daftrxseller[]=$objtrxseller;
                }else{
                  for($aaa=0;$aaa<count($daftrxseller);$aaa++){
                    if($daftrxseller[$aaa]->ids==$tbpk->ids){
                      if (! in_array($tbpk->id, $daftrxseller[$aaa]->dafid)){
                        $daftrxseller[$aaa]->keranjang[]=$k;
                        $daftrxseller[$aaa]->dafid[]=$tbpk->id;
                      }
                    }
                  }
                }
              }

              $datarecipien = array(
                  'name'    => $keterangantransaksi->buyer
                  ,'phone'    => $keterangantransaksi->phone
              );

                $tbrecipient=DB::table('tb_recipient')
                ->where('name',$keterangantransaksi->buyer)
                ->where('phone',$keterangantransaksi->phone)
                ->get();
                $idr=0;

                if(count($tbrecipient)>0){
                  $idr=$tbrecipient[0]->id;
                }else{
                  $idr = DB::table('tb_recipient')->insertGetId($datarecipien);
                }

                $nogrouptrx=0;
                $dptnogrouptrx=0;
                $jumsama=0;
                while($dptnogrouptrx==0){
                  $nogrouptrx=$tabelsessid->id.rand(10,100000);
                  if($jumsama>0){
                    $nogrouptrx.=$jumsama;
                  }
                  $tbtrksi=DB::table('tb_transaksi')
                  ->where('nogroup', $nogrouptrx)
                  ->get();
                  if(count($tbtrksi)==0){
                    $dptnogrouptrx=1;
                  }else{
                    $jumsama+=1;
                  }
                }

                $taxnum=func_get_system("tax");

                foreach($daftrxseller as $daf){
                  $totalprice=0;
                  foreach($daf->keranjang as $k){

                    $tbp=DB::table('tb_product')
                    ->select(['id'
                              ,'name'
                              ,'price'
                              ])
                    ->where('id', $k->product_id)
                    ->first();
/**
                    $hargaawal=$tbp->price;
                    $pajaknya=($hargaawal*$taxnum)/100;
                    $hargabaru=$hargaawal+$pajaknya;
                    $tbp->price=$hargabaru;
**/
                    $totalone=($tbp->price)*($k->amount);
                    $totalprice+=$totalone;
                  }

                  $tax=($totalprice*$taxnum)/100;
                  $ongkir=func_check_ongkir($obj->sessid,$keterangantransaksi->address_id,$keterangantransaksi->transport_service_id);

                  $datatrx = array(
                      'idb'    => $tabelsessid->id
                      ,'ids'   => $daf->ids
                      ,'nogroup'   => $nogrouptrx
                      ,'idts'    => $keterangantransaksi->transport_service_id
                      ,'ida'    => $keterangantransaksi->address_id
                      ,'idr'    => $idr
                      ,'bill'    => $totalprice
                      ,'ongkir' => $ongkir
                      ,'tax' => $tax
                      ,'comment'    => $keterangantransaksi->comment
                      ,'create_at'    => $get_date_time
                      ,'last_update'    => $get_date_time
                      ,'status' => 0
                  );

                  $tb_wallet=DB::table('tb_wallet')
                  ->where('id',$tabelsessid->idsaldo)
                  ->first();
                  $saldolama=$tb_wallet->ecash;
                  $saldobaru=$saldolama-$totalprice-$tax-$ongkir;

                  if($saldobaru<0){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"].="Saldo tidak cukup.";

                  }else{

                  $idtrx = DB::table('tb_transaksi')->insertGetId($datatrx);

                  DB::table('tb_wallet')
                  ->where('id',$tabelsessid->idsaldo)
                  ->update(array(
                       'ecash' => $saldobaru
                  ));


                  foreach($daf->keranjang as $k){

                  $tbp=DB::table('tb_product')
                  ->select(['id'
                            ,'name'
                            ,'price'
                            ])
                  ->where('id', $k->product_id)
                  ->first();

                  $datain = array(
                      'product_id'    => $k->product_id
                      ,'product_name'    => $tbp->name
                      ,'price_item'    => $tbp->price
                      ,'amount'    => $k->amount
                      ,'idtrx'    => $idtrx
                      ,'status' => 0
                      ,'create_at'    => $get_date_time
                      ,'last_update'    => $get_date_time
                  );

                  $idk = DB::table('tb_jem_transaksi_produk')->insertGetId($datain);
                  $dptnotrx=0;
                  $jumsama=0;
                  while($dptnotrx==0){
                    $notrx=$idtrx.$tabelsessid->id.rand(10,100000);
                    if($jumsama>0){
                      $notrx.=$jumsama;
                    }
                    $tbtrksi=DB::table('tb_transaksi')
                    ->where('notrx', $notrx)
                    ->get();
                    if(count($tbtrksi)==0){
                      $dptnotrx=1;
                    }else{
                      $jumsama+=1;
                    }
                  }

                  DB::table('tb_transaksi')
                  ->where('id', $idtrx)
                  ->update(array(
                   'notrx' => $notrx
                  ));

                  $bahandata["idtrx"]=$idtrx;
                  $bahandata["notrx"]=$notrx;
                  }
                }

                  //akhir foreach $daftrxseller
                }



              $dataret["respon_data"]=$bahandata;
             }
              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "check_ongkir":
              if( (! isset($obj->sessid)) || (! isset($obj->destination))  || (! isset($obj->transport_service_id))   || (! isset($obj->aridp)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $dafseller=array();

                foreach($obj->aridp as $idp){
                  $tbpk=DB::table('tb_product')
                  ->where('id',$idp)
                  ->first();
                  if (! in_array($tbpk->ids, $dafseller)){
                    $dafseller[]=$tbpk->ids;
                  }
                }

              $ongkirsatuan=func_check_ongkir($obj->sessid,$obj->destination,$obj->transport_service_id);
              $bahandata["ongkir"]=($ongkirsatuan*count($dafseller));
              $dataret["respon_data"]=$bahandata;
              }

             $hasil=json_encode($dataret);

             echo $hasil;
              break;
              case "get_my_shop_transactions":
              if( (! isset($obj->sessid)) || (! isset($obj->status)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tb_transaksi=DB::table('tb_transaksi')
                  ->where('ids',$tabelsessid->id)
                  ->where('status',$obj->status)
                  ->get();

                  for($i=0; $i<count($tb_transaksi); $i++){
                      $tb_transaksi[$i]->orders=func_get_order_by_idtrx($tb_transaksi[$i]->id);
                      $tb_transaksi[$i]->buyer_profile=func_get_profil($tb_transaksi[$i]->idb);
                      $tb_transaksi[$i]->seller_profile=func_get_profil($tb_transaksi[$i]->ids);

                  }

                  $dataret["respon_data"]=$tb_transaksi;
                }
              }

             $hasil=json_encode($dataret);
             $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

             echo $hasil;
              break;
              case "get_my_transactions":
              if( (! isset($obj->sessid)) || (! isset($obj->status)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tb_transaksi=DB::table('tb_transaksi')
                  ->where('idb',$tabelsessid->id)
                  ->where('status',$obj->status)
                  ->get();

                  for($i=0; $i<count($tb_transaksi); $i++){
                      $tb_transaksi[$i]->orders=func_get_order_by_idtrx($tb_transaksi[$i]->id);
                      $tb_transaksi[$i]->buyer_profile=func_get_profil($tb_transaksi[$i]->idb);
                      $tb_transaksi[$i]->seller_profile=func_get_profil($tb_transaksi[$i]->ids);

                  }

                  $dataret["respon_data"]=$tb_transaksi;
                }
              }

             $hasil=json_encode($dataret);
             $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

             echo $hasil;
              break;
              case "get_my_orders":
              if( (! isset($obj->sessid)) || (! isset($obj->transaction_id)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                  $tb_order=func_get_order_by_idtrx($obj->transaction_id);

                  $dataret["respon_data"]=$tb_order;
                }
              }

             $hasil=json_encode($dataret);
             $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

             echo $hasil;
              break;
              case "get_trx_detail":
              if( (! isset($obj->sessid)) || (! isset($obj->transaction_id)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                  $tb_transaksi=DB::table('tb_transaksi')
                  ->where('id',$obj->transaction_id)
                  ->first();
                  $tb_transaksi->orders=func_get_order_by_idtrx($obj->transaction_id);
                  $tb_transaksi->buyer_profile=func_get_profil($tb_transaksi->idb);
                  $tb_transaksi->seller_profile=func_get_profil($tb_transaksi->ids);
                  $dataret["respon_data"]=$tb_transaksi;
                }
              }

             $hasil=json_encode($dataret);
             $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

             echo $hasil;
              break;
              case "confirm_transaction":
              if( (! isset($obj->sessid)) || (! isset($obj->transaction_id))  || (! isset($obj->status)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tb_transaksi=DB::table('tb_transaksi')
                  ->where('id',$obj->transaction_id)
                  ->first();

                  $tb_user=DB::table('tb_user')
                  ->where('id',$tb_transaksi->idb)
                  ->first();

                  $bahanupdate["status"]=$obj->status;
                  $bahanupdate["last_update"]=$get_date_time;

                  if($tb_transaksi->ids==$tabelsessid->id){

                    $bahanupdate["seller_comment"]=$obj->seller_comment;
                    if($obj->status==3){

                      $tb_wallet=DB::table('tb_wallet')
                      ->where('id',$tb_user->idsaldo)
                      ->first();
                      $saldolama=$tb_wallet->ecash;
                      $saldobaru=$saldolama+$tb_transaksi->bill+$tb_transaksi->tax+$tb_transaksi->ongkir;

                      DB::table('tb_wallet')
                      ->where('id',$tb_user->idsaldo)
                      ->update(array(
                             'ecash' => $saldobaru
                       ));

                    }
                  }else if($tb_transaksi->idb==$tabelsessid->id){

                    $bahanupdate["comment"]=$obj->buyer_comment;

                  }

                  DB::table('tb_transaksi')
                  ->where('id',$obj->transaction_id)
                  ->update($bahanupdate);

                  $dataret["respon_data"]="Sukses merubah data transaksi";
                }
              }

             $hasil=json_encode($dataret);
             $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("icon\":\"","icon\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

             echo $hasil;
              break;
            }
           break;
           //akhir case transaction
           case "product":
           switch($action){
              case "add_product":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->product_name))
              || (! isset($obj->product_price))
              || (! isset($obj->product_description))
              || (! isset($obj->product_categories))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{

                      $arin=array(
                                 'ids' => $tabelsessid->id
                                 ,'name' => $obj->product_name
                                 ,'price' => $obj->product_price
                                 ,'mainpict' => ""
                                 ,'info' => $obj->product_description
                                 ,'create_at' => date("Y-m-d H:i:s")
                                 ,'last_update' => date("Y-m-d H:i:s")
                                 ,"aktif"=>1
                           );

                    $idp=DB::table('tb_product')->insertGetId($arin);

                   foreach($obj->product_categories as $prodcat) {
                    $tblcatproduk=DB::table('tb_product_cat')
                    ->where('id',$prodcat->id)
                    ->get();
                    if(count($tblcatproduk)>0){

                        $arincat=array(
                               'idpc' => $prodcat->id
                               ,'idp' => $idp
                         );
                     $idj=DB::table('tb_jem_product_cat')->insertGetId($arincat);
                    }
                  }
                  $bahanret["product_id"]=$idp;
                    $dataret["respon_data"]=$bahanret;
                    $dataret["error_msg"]="";
                  }

              }
              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "edit_product":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->product_id))
              || (! isset($obj->product_name))
              || (! isset($obj->product_price))
              || (! isset($obj->product_description))
              || (! isset($obj->product_categories))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
              $idd=$obj->product_id;
              $arin=array(
                         'name' => $obj->product_name
                         ,'price' => $obj->product_price
                         ,'info' => $obj->product_description
                         ,'last_update' => date("Y-m-d H:i:s")
              );
                $nomid=$idd;
                $kategori=array();

              foreach ($obj->product_categories as $cat) {
                $kategori[]=$cat->id;
              }

             $dafcat=DB::table('tb_jem_product_cat')
             ->where('idp', '=', $nomid)
             ->get();

             foreach ($dafcat as $map) {
               if (! in_array($map->idpc, $kategori)){

                 DB::table('tb_jem_product_cat')
                 ->where('id', '=', $map->id)
                 ->where('idp', '=', $nomid)
                 ->delete();

               }
             }
             foreach($kategori as $kat){
             if(($kat+0)!=0){

             $dafmap=DB::table('tb_jem_product_cat')
             ->where('idp', '=', $nomid)
             ->where('idpc', '=', $kat)
             ->first();

             if($dafmap==null){

             DB::table('tb_jem_product_cat')->insert(
             array(
                    'idpc' => $kat,
                    'idp' => $nomid
              )
              );

             }
             }

             }

                DB::table('tb_product')
                ->where('id', $idd)
                ->update($arin);

              }
              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "delete_product":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->product_id))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
              $idd=$obj->product_id;

              $tbproduk=DB::table('tb_product')
              ->where('id', '=', $idd)
              ->first();

              $namafilegambar=$tbproduk->mainpict;
              if($namafilegambar!=""){
              $alamatfile=public_path('uploads/images/'.$namafilegambar);

              unlink($alamatfile);
              }

                $terpakai=DB::table('tb_jem_transaksi_produk')
                ->where('product_id', $idd)
                ->get();

                if(count($terpakai)>0){
                 $datain = array(
                     'aktif'    => 0
                     ,'last_update' => date("Y-m-d H:i:s")
                 );
                 DB::table('tb_product')
                 ->where('id', $idd)
                 ->update($datain);
                }else{
                 DB::table('tb_product')
                 ->where('id', $idd)
                 ->delete();

                 }
             DB::table('tb_jem_product_cat')->where('idp', '=', $idd)->delete();


              }
              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "my_products":
              if(
                (! isset($obj->sessid))
              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $daftarproduk = array();

                    $tabelproduk=DB::table('tb_product')
                    ->where('ids',$tabelsessid->id)
                    ->where('aktif',1)
                    ->get();
                    foreach($tabelproduk as $prod) {
                      $produk=func_get_product($prod->id);

                      $daftarproduk[]=$produk;
                    }

                    $dataret["respon_data"]=$daftarproduk;
                  }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
              echo $hasil;
              break;
              case "search_product":
              if(
                (! isset($obj->sessid)) || (! isset($obj->query))
              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $daftarproduk = array();

                    $tabelproduk=DB::table('tb_product')
                    ->where('aktif',1)
                    ->where('name','like',"%{$obj->query}%")
                    ->get();
                    foreach($tabelproduk as $prod) {
                      $produk=func_get_product($prod->id);

                      $daftarproduk[]=$produk;
                    }

                    $dataret["respon_data"]=$daftarproduk;
                  }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
              echo $hasil;
              break;
              case "search_seller_by_category":
              if(
                (! isset($obj->sessid)) || (! isset($obj->category_id))
              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                  $dafidseller = array();
                  $dafseller = array();
                  $dafcat=DB::table('tb_jem_product_cat')
                  ->where('idpc', '=', $obj->category_id)
                  ->get();

                    foreach($dafcat as $cat) {
                      $tabelproduk=DB::table('tb_product')
                      ->select(['ids'])
                      ->where('id',$cat->idp)
                      ->first();
                      if (! in_array($tabelproduk->ids, $dafidseller)){
                        $dafidseller[]=$tabelproduk->ids;
                        $profilpenjual=func_get_profil($tabelproduk->ids);
                        $dafseller[]=$profilpenjual;
                      }

                    }

                    $dataret["respon_data"]=$dafseller;
                  }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("mainpict\":\"","mainpict\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
              echo $hasil;
              break;

            }
           break;
           // akhir case product
           case "wallet":

             DB::table('tb_topup')
             ->where('expired_date', '<','NOW()')
             ->where('status',0)
             ->update(array(
               'status' => 3
               ,'admin_comment' => 'Request tidak valid dan sudah kadaluarsa'
                 ));
           switch($action){
              case "request_topup":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->bank_name))
              || (! isset($obj->account_name))
              || (! isset($obj->account_number))
              || (! isset($obj->topup_amount))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $bank_id=0;
                      $terpakai=DB::table('tb_bank')
                      ->where('bank_name', $obj->bank_name)
                      ->where('account_name', $obj->account_name)
                      ->where('account_number', $obj->account_number)
                      ->get();
                      if(count($terpakai)>0){
                        $bank_id=$terpakai[0]->id;
                      }else{
                      $arinbank=array(
                                 'bank_name' => $obj->bank_name
                                 ,'account_name' => $obj->account_name
                                 ,'account_number' => $obj->account_number
                                 ,'create_at' => $get_date_time
                                 ,'last_update' => $get_date_time
                           );
                      $bank_id = DB::table('tb_bank')->insertGetId($arinbank);

                      }

                      $tb_wallet=DB::table('tb_wallet')
                      ->where('id',$tabelsessid->idsaldo)
                      ->first();
                      $exp_date = strtotime("+6 hour", strtotime($get_date_time));
                      $exp_date=date("Y-m-d H:i:s", $exp_date);
                      $arin=array(
                                 'idu' => $tabelsessid->id
                                 ,'bank_id' => $bank_id
                                 ,'topup_amount' => $obj->topup_amount
                                 ,'last_amount' => $tb_wallet->ecash
                                 ,'create_at' => $get_date_time
                                 ,'last_update' => $get_date_time
                                 ,'expired_date' => $exp_date
                                 ,'status' => 0
                           );

                    $idtp=DB::table('tb_topup')->insertGetId($arin);

                    $detailtopup=func_get_topup($idtp);
                    $dataret["respon_data"]=$detailtopup;
                    $dataret["error_msg"]="";
                  }

              }
              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "get_topup_detail":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->topup_id))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $detailtopup=func_get_topup($obj->topup_id);
                    $dataret["respon_data"]=$detailtopup;
                    $dataret["error_msg"]="";
                  }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("buktitransfer\":\"","buktitransfer\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

              echo $hasil;
              break;
              case "confirm_topup":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->topup_id))
              || (! isset($obj->user_comment))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    DB::table('tb_topup')
                    ->where('id', $obj->topup_id)
                    ->update(array(
                     'user_comment' => $obj->user_comment
                    ,'last_update' => $get_date_time
                    ,'status' => 1
                    ));

                      $tb_topup=DB::table('tb_topup')
                      ->where('id', $obj->topup_id)
                      ->first();
                      $tb_bank=DB::table('tb_bank')
                      ->where('id', $tb_topup->bank_id)
                      ->first();

                    $detailtopup=array(
                                'id' => $tb_topup->id
                               ,'bank_name' => $tb_bank->bank_name
                               ,'account_name' => $tb_bank->account_name
                               ,'account_number' => $tb_bank->account_number
                               ,'topup_amount' => $tb_topup->topup_amount
                               ,'create_at' => $tb_topup->create_at
                               ,'last_update' => $tb_topup->last_update
                               ,'buktitransfer' => $tb_topup->buktitransfer
                               ,'status' => $tb_topup->status
                               ,'user_comment' => $tb_topup->user_comment
                               ,'expired_date' => $tb_topup->expired_date
                         );
                    $dataret["respon_data"]=$detailtopup;
                    $dataret["error_msg"]="";
                  }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("buktitransfer\":\"","buktitransfer\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

              echo $hasil;
              break;
              case "get_my_topups":
              if(
                (! isset($obj->sessid))
              || (! isset($obj->status))

              ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                  $tabelsessid=DB::table('tb_user')
                  ->where('sessid',$obj->sessid)
                  ->first();
                  if($tabelsessid==null){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Invalid session";
                  }else{
                    $daftopup=array();
                    if($obj->status<4){
                      $tb_topup=DB::table('tb_topup')
                      ->where('idu',$tabelsessid->id)
                      ->where('status',$obj->status)
                      ->get();
                    }else if($obj->status==4){
                      $tb_topup=DB::table('tb_topup')
                      ->where('idu',$tabelsessid->id)
                      ->where(function($query) {
                          $query->where('status', 2)
                                ->orWhere('status', 3);
                        })
                      ->get();
                    }
                      foreach($tb_topup as $tt){
                        $detailtopup=func_get_topup($tt->id);
                        $daftopup[]=$detailtopup;
                      }
                      $dataret["respon_data"]=$daftopup;

                }

              }
              $hasil=json_encode($dataret);
              $hasil=str_replace("buktitransfer\":\"","buktitransfer\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

              echo $hasil;
              break;

            }
           break;


           case "klien":
           switch($action){
              case "register":
              if((! isset($obj->datauser_nama)) || (! isset($obj->datauser_email))  || (! isset($obj->datauser_username))  || (! isset($obj->datauser_password))  || (! isset($obj->datauser_idref))  ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                                  $tabel=DB::table('tb_user')
                                  ->where('username',$obj->datauser_username)
                                  ->first();
                                  if($tabel!=null){
                                    $dataret["error_code"]="001";
                                    $dataret["error_msg"]="Username already used";
                                  }else{
                                    $filefoto="fotodefault.png";
                                    if(isset($obj->fotoprofil)){
                                      $filefoto=$obj->fotoprofil;
                                    }

                                      $salt="garammanis".rand(1,999);
                                      $hasilhash = hash('sha512', $obj->datauser_password. $salt);
                                    $datauser = array(
                                        'name'    => $obj->datauser_nama
                                        ,'email'    => $obj->datauser_email
                                        ,'username'    => $obj->datauser_username
                                        ,'password'    => $hasilhash
                                        ,'salt'    => $salt
                                        ,'filefoto'    => $filefoto
                                        ,'level'    => 1
                                    );

                                    $idk = DB::table('tb_user')->insertGetId($datauser);

                                    $tabelklien=DB::table('tb_klien')
                                    ->where('idu',$obj->datauser_idref)
                                    ->select(['id'])
                                    ->first();

                                    $datadiriuser = array(
                                        'idu'    => $idk
                                        ,'idref'    => $tabelklien->id
                                    );

                                    $iddu=DB::table('tb_klien')->insertGetId($datadiriuser);
                                    DB::table('tb_user')
                                    ->where('id', $idk)
                                    ->update(array(
                                           'idr' => $iddu
                                     ));

                                     DB::table('tb_rekening')
                                     ->insert(array(
                                            'role'=>1
                                            ,'idr' => $iddu
                                            ,'idu' =>$idk
                                      ));

                                   $bahandata["info"]="Success";
                                   $bahandata["butuhvalidasi"]=1;

                                   $dataret["respon_data"]=$bahandata;

                                  }
              }

                              $hasil=json_encode($dataret);
                              echo $hasil;
              break;
              case "savephonenumber":
              if((! isset($obj->phonenumber)) || (! isset($obj->username))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabel=DB::table('tb_user')
                ->where('username',$obj->username)
                ->first();
                if($tabel==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Username not found";
                }else{
                  $idk=$tabel->id;
                  DB::table('tb_user')
                 ->where('id', $idk)
                 ->update(array(
                   'phone' => $obj->phonenumber
                  ));

                  $bahandata["info"]="Success";

                  $dataret["respon_data"]=$bahandata;
                }

              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "bayarlawyer":
              if((! isset($obj->sessid)) || (! isset($obj->idl)) || (! isset($obj->detikpotong))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $waktubayar=date('Y-m-d');
                $timestamp=strtotime($waktubayar);
                //$periodebayar=date('m',$timestamp);

                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                    $tabellawyer=DB::table('tb_lawyer')
                    ->where('id',$obj->idl)
                    ->first();
                    $saldolawyer=$tabellawyer->saldo;
                    $tabelklien=DB::table('tb_klien')
                    ->where('idu',$tabelsessid->id)
                    ->first();

                    $tabelklienupline=DB::table('tb_klien')
                    ->where('id',$tabelklien->idref)
                    ->first();

                    $idk=$tabelklien->id;
                    $hrgpermenit=$tabellawyer->permenit;
                    $hrgperdetik=ceil($hrgpermenit/60);
                    $totalhrg=$hrgperdetik*$obj->detikpotong;
                    $saldoawal=$tabelklien->saldo;
                    $saldoakhir=$saldoawal-$totalhrg;
                    $persenfee=$tabellawyer->persenfee;
                    $untuklawyer=($totalhrg*$persenfee)/100;
                    $untukupline=($totalhrg*5)/100;

                    if($saldoakhir>=0){
                      $saldolawyer+=$untuklawyer;
                    DB::table('tb_klien')
                    ->where('id', $tabelklien->id)
                    ->update(array(
                     'saldo' => $saldoakhir
                       ));
                     DB::table('tb_lawyer')
                    ->where('id', $tabellawyer->id)
                    ->update(array(
                      'saldo' => $saldolawyer
                        ));

                        if($tabelklienupline!=null){
                          $komisiawal=$tabelklienupline->komisi;
                          $komisiakhir=$komisiawal+$untukupline;
                          DB::table('tb_klien')
                          ->where('id', $tabelklienupline->id)
                          ->update(array(
                           'komisi' => $komisiakhir
                             ));

                             $tb_mutasi=DB::table('tb_mutasiklien')
                             ->where('idk', $tabelklienupline->id)
                             ->whereRaw('YEAR(periode) = ?', [date('Y',$timestamp)])
                             ->whereRaw('MONTH(periode) = ?', [date('m',$timestamp)])
                             ->first();
                             if($tb_mutasi!=null){
                               $komisilama=$tb_mutasi->komisi;
                               $komisilamaakhir=$komisilama+$untukupline;
                               $totalkomisilama=$tb_mutasi->totalkomisi;
                               $totalkomisilamaakhir=$totalkomisilama+$untukupline;


                               DB::table('tb_mutasiklien')
                              ->where('id', $tb_mutasi->id)
                              ->update(array(
                                'komisi' => $komisilamaakhir
                               ,'totalkomisi'=>$totalkomisilamaakhir
                                  ));
                             }else{
                               DB::table('tb_mutasiklien')->insert(
                                     array(
                                                 'idk' => $tabelklienupline->id
                                                 ,'periode' => $waktubayar
                                                 ,'komisi'=>$untukupline
                                                 ,'totalkomisi'=>$untukupline
                                           )
                                           );
                               }
                        }

                       $history=DB::table('tb_call_history')
                       ->where('idk', $idk)
                       ->orderBy('id', 'DESC')
                       ->first();
                       if(count($history)>0){
                       $idh=$history->id;
                         $seconds=$history->seconds;
                         $bill=$history->bill;

                         $secondsakhir=$seconds+$obj->detikpotong;
                         $billakhir=$bill+$totalhrg;
                         DB::table('tb_call_history')
                        ->where('id', $idh)
                        ->update(array(
                          'seconds' => $secondsakhir
                         ,'bill' => $billakhir
                            ));
                       }

                       $tb_mutasi=DB::table('tb_mutasi')
                       ->where('idl', $tabellawyer->id)
                       ->whereRaw('YEAR(periode) = ?', [date('Y',$timestamp)])
                       ->whereRaw('MONTH(periode) = ?', [date('m',$timestamp)])
                       ->first();
                       if($tb_mutasi!=null){
                         $feelama=$tb_mutasi->fee;
                         $sharelama=$tb_mutasi->share;
                         $tllama=$tb_mutasi->tl;
                         $saldolama=$tb_mutasi->saldo;

                         $pecahtotalhrg=($totalhrg*$persenfee)/100;
                         $feebaru=$feelama+$totalhrg;
                         $sharebaru=$sharelama+$pecahtotalhrg;
                         $tlbaru=$tllama+($totalhrg-$pecahtotalhrg);
                         $saldobaru=$saldolama+$pecahtotalhrg;

                         DB::table('tb_mutasi')
                        ->where('id', $tb_mutasi->id)
                        ->update(array(
                          'periode' => $waktubayar
                         ,'fee' => $feebaru
                         ,'share'=>$sharebaru
                         ,'tl'=>$tlbaru
                         ,'saldo'=>$saldobaru
                            ));
                       }else{
                         $fee=$totalhrg;
                         $share=($fee*$persenfee)/100;
                         $tl=$feebaru-$sharebaru;
                         $saldo=$tabellawyer->saldo;
                         DB::table('tb_mutasi')->insert(
                               array(
                                           'idl' => $tabellawyer->id
                                           ,'periode' => $waktubayar
                                           ,'fee'=>$fee
                                           ,'share'=>$share
                                           ,'tl'=>$tl
                                           ,'saldo'=>$saldo
                                     )
                                     );
                         }


                       $arhasil=array();
                       $arhasil["msg"]="Saldo terpotong";
                       $arhasil["saldoawal"]=$saldoawal;
                       $arhasil["dipotong"]="$totalhrg";
                       $arhasil["sisasaldo"]="$saldoakhir";

                       $dataret["respon_data"]=$arhasil;
                     }else{
                       $dataret["error_code"]="001";
                       $dataret["error_msg"]="Saldo tidak cukup";
                     }

                }

              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "message_list":
              if((! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                    $tabelklien=DB::table('tb_klien')
                    ->where('idu',$tabelsessid->id)
                    ->first();
                    $idk=$tabelklien->id;

                  $listpesan=DB::table('tb_pesan')
                  ->where('idk', $idk)
                  ->select(['id','judul',DB::raw("SUBSTR(pesan, 1, 20) as potonganpesan"),'tanggal','tanggalupdate','status'])
                  ->orderBy('id', 'DESC');

                  if(isset($obj->periode)){

                    $timestamp=strtotime($obj->periode);

                    $listpesan->whereRaw('YEAR(tanggal) = ?', [date('Y',$timestamp)])
                    ->whereRaw('MONTH(tanggal) = ?', [date('m',$timestamp)]);
                  }else{
                    $listpesan->whereRaw('tanggal >= now()-interval 3 month');
                  }
                  $listpesan=$listpesan->get();

                  $dataret["respon_data"]=$listpesan;



                }

              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "message_detail":
              if((! isset($obj->sessid))  || (! isset($obj->idm))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{

                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                    $tabelklien=DB::table('tb_klien')
                    ->where('idu',$tabelsessid->id)
                    ->first();
                    $idk=$tabelklien->id;

                  $tabelpesan=DB::table('tb_pesan')
                  ->where('id', $obj->idm)
                  ->select(['id','judul','pesan','tanggal','tanggalupdate','status'])
                  ->first();

                $tabelfilepesan=DB::table('tb_filepesan')
                ->where('idp', $obj->idm)
                ->select(['id','name','ukuran'])
                ->get();


                $bahandata=array();
                $bahandata['judul']=$tabelpesan->judul;
                $bahandata['pesan']=$tabelpesan->pesan;
                $bahandata['tanggal']=$tabelpesan->tanggal;
                $bahandata['daftarfile']=$tabelfilepesan;
                $dataret["respon_data"]=$bahandata;



                }

              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "get_saldo":
              if((! isset($obj->sessid))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();

                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tabelklien=DB::table('tb_klien')
                  ->where('idu',$tabelsessid->id)
                  ->select(['saldo'])
                  ->first();
                  $dataret["respon_data"]=intval($tabelklien->saldo);
                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "get_komisi":
              if((! isset($obj->sessid))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();

                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tabelklien=DB::table('tb_klien')
                  ->where('idu',$tabelsessid->id)
                  ->select(['komisi'])
                  ->first();
                  $dataret["respon_data"]=intval($tabelklien->komisi);
                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "set_token":
              if((! isset($obj->sessid)) || (! isset($obj->token))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  DB::table('tb_klien')
                 ->where('idu', $tabelsessid->id)
                 ->update(array(
                   'token' => $obj->token
                     ));

                     $bahandata['token']=$obj->token;
                      $dataret["respon_data"]=$bahandata;
                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "kirimgambar":
              if((! isset($obj->sessid)) || (! isset($obj->alamatfile))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  /**
                  DB::table('tb_klien')
                 ->where('id', $tabelsessid->idt)
                 ->update(array(
                   'token' => $obj->token
                     ));
                     **/

                     $bahandata['alamatfile']=$obj->alamatfile;
                      $dataret["respon_data"]=$bahandata;
                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "opencall":
              if((! isset($obj->sessid)) || (! isset($obj->idl))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tabellawyer=DB::table('tb_lawyer')
                  ->where('id',$obj->idl)
                  ->first();
                  $tabelklien=DB::table('tb_klien')
                  ->where('idu',$tabelsessid->id)
                  ->first();
                  $idk=$tabelklien->id;

                  $history=DB::table('tb_call_history')
                  ->where('idk', $idk)
                  ->orderBy('id', 'DESC')
                  ->first();
                  $tanggal=date("Y-m-d H:i:s");
                  if(count($history)>0){
                  $idh=$history->id;
                    if($history->seconds==0){

                      DB::table('tb_call_history')
                     ->where('id', $idh)
                     ->update(array(
                       'idl' => $obj->idl
                       ,'start'=>$tanggal
                         ));
                    }else{
                      $idh = DB::table('tb_call_history')->insertGetId(
                      array(
                             'idk' => $idk
                             ,'idl'=>$obj->idl
                             ,'start'=>$tanggal
                             ,'seconds'=>0
                       )
                       );
                    }
                  }else{
                    $idh = DB::table('tb_call_history')->insertGetId(
                    array(
                           'idk' => $idk
                           ,'idl'=>$obj->idl
                           ,'start'=>$tanggal
                           ,'seconds'=>0
                     )
                     );
                  }

                  $bahandata['tokenlawyer']=$tabellawyer->token;
                   $dataret["respon_data"]=$bahandata;

                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "lawyer_list":
              if((! isset($obj->sessid))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();

                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                    $tabellawyer=DB::table('tb_lawyer')
                    ->select(['id','idu','permenit','tampilkanposisi'])
                    ->get();

                    //$tabellawyerbaru = new \stdClass();
                    $tabellawyerbaru = array();
                    foreach ($tabellawyer as $tbll) {
                            $temptbll=$tbll;

                             $tb_user=DB::table('tb_user')
                             ->where('id', '=', $tbll->idu)
                             ->first();
                             $temptbll->name=$tb_user->name;
                             $temptbll->filefoto=$tb_user->filefoto;

                             $dafmapjen=DB::table('tb_jem_lawyer')
                             ->where('idl', '=', $tbll->id)
                             ->get();
                             $kategori = array();
                             $i=0;
                             $cart = array();
                              foreach ($dafmapjen as $map) {
                                $i+=1;
                                      $cat=DB::table('tb_jenishukum')
                                      ->where('id', '=', $map->idj)
                                      ->first();
                                      $isikat = new \stdClass();
                                      $isikat->id=$cat->id;
                                      $isikat->jenis=$cat->jenis;
                                      $kategori[] = $isikat;

                              }
                              $temptbll->kategori=$kategori;
                              $tabellawyerbaru[] = $temptbll;


                    }

                     $bahandata['rows']=count($tabellawyer);
                     $bahandata['lawyer_list']=$tabellawyerbaru;
                     $dataret["respon_data"]=$bahandata;
                }

              }

              $hasil=json_encode($dataret);
              $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
              echo $hasil;
              break;
              case "lawyer_positions":
              if((! isset($obj->sessid))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tabellawyer=DB::table('tb_lawyer')
                  ->select(['id','latitude','longitude','firmlatitude','firmlongitude','tampilkanposisi'])
                  ->get();

                      $dataret["respon_data"]=$tabellawyer;
                }
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "get_topup_form":
              if( (! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                  $tb_klien=DB::table('tb_klien')
                 ->where('idu', $tabelsessid->id)
                 ->select(['id','saldo'])
                 ->first();
                 $bahandata['saldo']=$tb_klien->saldo;

                 $tb_topup=DB::table('tb_topup')
                 ->where('idk', $tb_klien->id)
                 ->where('status','!=',3)
                 ->first();

                 if($tb_topup!=null){
                    $status=$tb_topup->status;
                    if($status==-1){
                     $bahandata['step']=1; // isi jumlah topup
                    }else if($status==0){
                     $bahandata['step']=2; // menyuruh konfirmasi
                     $bahandata['jumlah']=$tb_topup->jumlah;
                     $bahandata['tanggal']=$tb_topup->tanggal;
                     $bahandata['idt']=$tb_topup->id;
                     $tb_metode_topup=DB::table('tb_metode_topup')
                     ->get();
                     $bahandata['listmetodetopup']=$tb_metode_topup;
                    }else if($status==1){
                      $bahandata['step']=3; // menunggu validasi
                      $bahandata['jumlah']=$tb_topup->jumlah;
                      $bahandata['tanggal']=$tb_topup->tanggal;
                      $bahandata['komenklien']=$tb_topup->komenklien;
                      $bahandata['buktitransfer']=$tb_topup->buktitransfer;
                      $tb_metode_topup=DB::table('tb_metode_topup')
                      ->where('id', $tb_topup->idmt)
                      ->first();
                      $bahandata['metodetopup']=$tb_metode_topup;
                    }else if($status==2){
                     $bahandata['step']=4; // validasi gagal, disertai alasannya dan meminta topup ulang
                     $bahandata['jumlah']=$tb_topup->jumlah;
                     $bahandata['tanggal']=$tb_topup->tanggal;
                     $bahandata['komenklien']=$tb_topup->komenklien;
                     $bahandata['komenpusat']=$tb_topup->komenpusat;
                     $bahandata['buktitransfer']=$tb_topup->buktitransfer;
                     $tb_metode_topup=DB::table('tb_metode_topup')
                     ->where('id', $tb_topup->idmt)
                     ->first();
                     $bahandata['metodetopup']=$tb_metode_topup;
                    }
                 }else{
                  $bahandata['step']=1;
                 }

                      $dataret["respon_data"]=$bahandata;
                }
              }

              $hasil=json_encode($dataret);
              $hasil=str_replace("buktitransfer\":\"","buktitransfer\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);

              echo $hasil;
              break;
              case "process_topup_form":
              if( (! isset($obj->sessid)) || (! isset($obj->step)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();

                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $step=$obj->step;
                  $tanggal=date("Y-m-d H:i:s");

                  $tb_klien=DB::table('tb_klien')
                  ->where('idu', $tabelsessid->id)
                  ->select(['id','saldo'])
                  ->first();
                  switch($step){
                    case 1: //membuat pengauan topup

                    if( (! isset($obj->jumlah)) ){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"].="Parameter tidak lengkap.";
                    }else{
                      $jumlahtopup=$obj->jumlah;
                      $tb_topup=DB::table('tb_topup')
                      ->where('idk', $tb_klien->id)
                      ->where( function ( $query )
                          {
                            $query->where('status',-1)
                                     ->orWhere('status','=', 2);
                          })
                      ->first();
                      if($tb_topup==null){

                          $cektopup=DB::table('tb_topup')
                          ->where('idk', $tb_klien->id)
                          ->where('status',0)
                          ->first();
                          if($cektopup==null){
                            DB::table('tb_topup')->insert(
                            array(
                                   'idk' => $tb_klien->id
                                   ,'jumlah'=>$jumlahtopup
                                   ,'saldosebelumnya'=>$tb_klien->saldo
                                   ,'tanggal'=>$tanggal
                                   ,'status'=>0
                             )
                             );
                           }else{
                           $dataret["error_code"]="001";
                           $dataret["error_msg"].="Pengajuan sudah pernah dibuat";
                           }
                      }else{
                       DB::table('tb_topup')
                       ->where('id', $tb_topup->id)
                       ->update(array(
                         'jumlah'=>$jumlahtopup
                         ,'tanggal'=>$tanggal
                         ,'status'=>0
                         ,'saldosebelumnya'=>$tb_klien->saldo
                        ));
                      }

                      $tb_metode_topup=DB::table('tb_metode_topup')
                      ->get();
                      $bahandata['listmetodetopup']=$tb_metode_topup;
                      $bahandata['info']="pengajuan topup berhasil dibuat. Silahkan transfer sesuai dengan jumlah topup";
                    }

                    break;
                    case 2: //mengkonfirmasi pembayaran

                    if( (! isset($obj->idt)) ||   (! isset($obj->idmt)) ||  (! isset($obj->komenklien)) ){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"].="Parameter tidak lengkap.";
                    }else{

                      $idmt=$obj->idmt;
                      $komenklien=$obj->komenklien;

                      $tb_metode_topup=DB::table('tb_metode_topup')
                      ->where('id', $idmt)
                      ->select(['id'])
                      ->first();
                      if($tb_metode_topup==null){
                        $dataret["error_code"]="001";
                        $dataret["error_msg"].="Metode pembayaran tidak diketahui";
                      }else{
                        DB::table('tb_topup')
                        ->where('id', $obj->idt)
                        ->where('status','!=',3)
                        ->update(array(
                          'idmt'=>$idmt
                          ,'komenklien'=>$komenklien
                          ,'status'=>1
                            ));

                            $bahandata['info']="Konfirmasi berhasil. Saldo akan ditambah setelah divalidasi oleh pusat";
                      }

                    }

                    break;

                  }
                }
              }

              if($dataret["error_code"]!="001"){
                $dataret["respon_data"]=$bahandata;
              }

              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "cancel_topup":
              if( (! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();
                 if($tabelsessid==null){
                   $dataret["error_code"]="001";
                   $dataret["error_msg"]="Invalid session";
                 }else{

                   DB::table('tb_topup')
                   ->where('idk', $tabelsessid->idr)
                   ->where( function ( $query )
                       {
                         $query->where('status',0)
                                  ->orWhere('status','=', 1)
                                  ->orWhere('status','=', 2);
                       })
                   ->update(array(
                     'status'=>-1
                    ));

                 }

                 }


              if($dataret["error_code"]!="001"){
                $bahandata['info']="Pengajuan topup berhasil dibatalkan";
                $dataret["respon_data"]=$bahandata;
              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "log_topup":
              if((! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                    $idk=$tabelsessid->idr;
                    $history=DB::table('tb_topup')
                    ->where('idk', $idk)
                    ->where('status',3)
                    ->orderBy('id', 'DESC');


                    if(isset($obj->periode)){

                      $timestamp=strtotime($obj->periode);

                      $history->whereRaw('YEAR(tanggal) = ?', [date('Y',$timestamp)])
                      ->whereRaw('MONTH(tanggal) = ?', [date('m',$timestamp)]);
                    }else{
                      $history->whereRaw('tanggal >= now()-interval 3 month');
                    }
                    $history=$history->get();
                    $tabelhistorybaru=$history;


                 $isitabel=json_encode($tabelhistorybaru);
                 $dataret["respon_data"]=$tabelhistorybaru;

                }
              }

              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "get_withdrawkomisi_form":
              if( (! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tb_klien=DB::table('tb_klien')
                  ->where('idu', $tabelsessid->id)
                  ->select(['id','komisi','idu'])
                  ->first();
                  $bahandata['komisi']=$tb_klien->komisi;

                  $tb_withdraw=DB::table('tb_withdrawkomisi')
                  ->where('idk', $tb_klien->id)
                  ->where('status','!=',3)
                  ->first();

                  if($tb_withdraw!=null){
                     $status=$tb_withdraw->status;
                     if($status==-1){
                      $bahandata['step']=1; // isi jumlah withdraw
                     }else if($status==0){
                      $bahandata['step']=2; // menunggu validasi
                      $bahandata['jumlah']=$tb_withdraw->nilai;
                      $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                      $bahandata['komenklien']=$tb_withdraw->komenklien;
                     }else if($status==1){
                       $bahandata['step']=3; // withdraw diproses
                       $bahandata['jumlah']=$tb_withdraw->nilai;
                       $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                       $bahandata['komenklien']=$tb_withdraw->komenklien;
                       $bahandata['komenpusat']=$tb_withdraw->komenpusat;
                     }else if($status==2){
                       $bahandata['step']=4; // withdraw gagal, disertai alasannya dan meminta withdraw ulang
                       $bahandata['jumlah']=$tb_withdraw->nilai;
                       $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                       $bahandata['komenklien']=$tb_withdraw->komenklien;
                       $bahandata['komenpusat']=$tb_withdraw->komenpusat;
                     }

                  }else{
                   $bahandata['step']=1;
                  }
                  $dataret["respon_data"]=$bahandata;
                }
              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "withdrawkomisi":
              if((! isset($obj->sessid)) || (! isset($obj->jumlah)) || (! isset($obj->komenklien))){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();

                 if($tabelsessid==null){
                   $dataret["error_code"]="001";
                   $dataret["error_msg"]="Invalid session";
                 }else{

                   $tanggal=date("Y-m-d H:i:s");
                   $tb_klien=DB::table('tb_klien')
                   ->where('idu', $tabelsessid->id)
                   ->select(['id','komisi','idu'])
                   ->first();

                   $komisiklien=$tb_klien->komisi;
                   $jumlah=$obj->jumlah;
                   $komenklien=$obj->komenklien;

                   if($jumlah>$komisiklien){
                     $dataret["error_code"]="001";
                     $dataret["error_msg"]="Saldo tidak cukup";
                   }else{

                           $tb_withdraw=DB::table('tb_withdrawkomisi')
                           ->where('idk', $tabelsessid->idr)
                           ->where( function ( $query )
                               {
                                 $query->where('status',-1)
                                          ->orWhere('status', 2);
                               })
                           ->first();

                           if($tb_withdraw==null){

                               $cekwithdraw=DB::table('tb_withdrawkomisi')
                               ->where('idk', $tabelsessid->idr)
                               ->where('status',0)
                               ->first();
                               if($cekwithdraw==null){
                                   DB::table('tb_withdrawkomisi')->insert(
                                   array(
                                          'idk' => $tabelsessid->idr
                                          ,'nilai'=>$jumlah
                                          ,'tanggalminta'=>$tanggal
                                          ,'komenklien'=>$komenklien
                                          ,'status'=>0
                                    )
                                    );
                               }else{
                               $dataret["error_code"]="001";
                               $dataret["error_msg"].="Pengajuan sudah pernah dibuat";
                               }

                           }else{
                            DB::table('tb_withdrawkomisi')
                            ->where('id', $tb_withdraw->id)
                            ->update(array(
                              'nilai'=>$jumlah
                              ,'tanggalminta'=>$tanggal
                              ,'komenklien'=>$komenklien
                              ,'status'=>0
                             ));
                           }

                 }

                 }

              }

              if($dataret["error_code"]!="001"){
                $bahandata['info']="pengajuan withdraw berhasil dibuat. Silahkan menunggu validasi dari pusat";
                $dataret["respon_data"]=$bahandata;
              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "cancel_withdrawkomisi":
              if( (! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();
                 if($tabelsessid==null){
                   $dataret["error_code"]="001";
                   $dataret["error_msg"]="Invalid session";
                 }else{

                   DB::table('tb_withdrawkomisi')
                   ->where('idk', $tabelsessid->idr)
                   ->where( function ( $query )
                       {
                         $query->where('status',0)
                                  ->orWhere('status','=', 2);
                       })
                   ->update(array(
                     'status'=>-1
                    ));

                 }

                 }


              if($dataret["error_code"]!="001"){
                $bahandata['info']="Pengajuan withdraw komisi berhasil dibatalkan";
                $dataret["respon_data"]=$bahandata;
              }
              $hasil=json_encode($dataret);
              echo $hasil;
              break;
              case "log_withdrawklien":
              if((! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                    $idk=$tabelsessid->idr;
                    $history=DB::table('tb_withdrawkomisi')
                    ->where('idk', $idk)
                    ->where('status',3)
                    ->orderBy('id', 'DESC');


                    if(isset($obj->periode)){

                      $timestamp=strtotime($obj->periode);

                      $history->whereRaw('YEAR(tanggalminta) = ?', [date('Y',$timestamp)])
                      ->whereRaw('MONTH(tanggalminta) = ?', [date('m',$timestamp)]);
                    }else{
                      $history->whereRaw('tanggalminta >= now()-interval 3 month');
                    }
                    $history=$history->get();
                    $tabelhistorybaru=$history;


                 $isitabel=json_encode($tabelhistorybaru);
                 $dataret["respon_data"]=$tabelhistorybaru;

                }
              }

              $hasil=json_encode($dataret);

              echo $hasil;
              break;
              case "komisi":
              if((! isset($obj->sessid)) ){
              $dataret["error_code"]="001";
              $dataret["error_msg"].="Parameter tidak lengkap.";
              }else{
                $tabelsessid=DB::table('tb_user')
                ->where('level',1)
                ->where('sessid',$obj->sessid)
                ->first();


                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $idk=$tabelsessid->idr;
                   $tanggal=date("Y-m-d");
                   $tamp=strtotime($tanggal);
                   $periode_y=date('Y',$tamp);
                    $idl=$tabelsessid->idr;
                    $history=DB::table('tb_mutasiklien')
                    ->where('idk', $idk)
                    ->whereRaw('YEAR(periode) = ?', [$periode_y])
                    ->orderBy('id', 'DESC')
                    ->select(['id','idk',DB::raw('MONTH(periode) as periode'),'komisi','totalkomisi','withdraw'])
                    ->get();
                    $tabelhistorybaru=$history;

                 $isitabel=json_encode($tabelhistorybaru);
                 $dataret["respon_data"]=$tabelhistorybaru;

                }
              }

              $hasil=json_encode($dataret);

              echo $hasil;
              break;


           }

           break;
           //akhir case klien
           case "lawyer":
           switch($action){
             case "retrieve_list":
             if((! isset($obj->sessid)) || (! isset($obj->bidanghukum))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('sessid',$obj->sessid)
               ->first();
             if($tabelsessid==null){
               $dataret["error_code"]="001";
               $dataret["error_msg"]="Invalid session";
             }else{
               $tabel = DB::table('tb_jem_lawyer')
                   ->join('tb_lawyer', 'tb_jem_lawyer.idl', '=', 'tb_lawyer.id')
                   ->where('idj',$obj->bidanghukum)
                   //->select(['tb_lawyer.id','tb_lawyer.name','tb_lawyer.online','tb_lawyer.permenit','tb_lawyer.filefoto','tb_lawyer.token'])
                   ->select(['tb_lawyer.id','tb_lawyer.online'])
                   ->get();

             $isitabel=json_encode($tabel);
             $dataret["respon_data"]=$tabel;
            }
            }
            $hasil=json_encode($dataret);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             echo $hasil;
             break;
             case "get_saldo":
             if((! isset($obj->sessid))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $tabellawyer=DB::table('tb_lawyer')
                 ->where('idu',$tabelsessid->id)
                 ->select(['saldo'])
                 ->first();
                 $dataret["respon_data"]=intval($tabellawyer->saldo);
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "profilklien":
             if((! isset($obj->sessid)) || (! isset($obj->idk))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $tabelklien=DB::table('tb_klien')
                 ->where('id',$obj->idk)
                 ->select(['id','idu','saldo'])
                 ->first();
                  $tb_user=DB::table('tb_user')
                  ->where('id', '=', $tabelklien->idu)
                  ->first();
                    $tabelklienbaru = $tabelklien;
                    $tabelklienbaru->name=$tb_user->name;
                    $tabelklienbaru->email=$tb_user->email;
                    $tabelklienbaru->filefoto=$tb_user->filefoto;
                    $tabelklienbaru->username=$tb_user->username;
                 $dataret["respon_data"]=$tabelklienbaru;
               }
             }

             $hasil=json_encode($dataret);
             $hasil=str_replace("filefoto\":\"","filefoto\":\"".url('/')."/gambarupload/w/200/h/200/",$hasil);
             echo $hasil;
             break;
             case "set_online":
             if((! isset($obj->sessid)) || (! isset($obj->online)) || (! isset($obj->onlinesejak))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $kolomnya = array(
                     'online'    => $obj->online
                 );
                 if($obj->online==1){
                   $kolomnya['onlinesejak']=$obj->onlinesejak;
                 }

                 DB::table('tb_lawyer')
                ->where('idu', $tabelsessid->id)
                ->update($kolomnya);

                    $bahandata['online']=$obj->online;
                     $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "is_online":
             if((! isset($obj->sessid))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();


               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{

                $tabellawyer=DB::table('tb_lawyer')
                ->where('idu',$tabelsessid->id)
                ->first();
                $bahandata['online']=$tabellawyer->online;
                $bahandata['onlinesejak']=$tabellawyer->onlinesejak;
                $bahandata['name']=$tabelsessid->name;
                 $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "get_stat":
             if((! isset($obj->sessid))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();


               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{

                $tabellawyer=DB::table('tb_lawyer')
                ->where('idu',$tabelsessid->id)
                ->first();
                $bahandata['online']=$tabellawyer->online;
                $bahandata['onlinesejak']=$tabellawyer->onlinesejak;
                $bahandata['tampilkanposisi']=$tabellawyer->tampilkanposisi;
                $bahandata['name']=$tabelsessid->name;
                 $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "set_statsharepos":
             if((! isset($obj->sessid)) || (! isset($obj->tampilkanposisi)) ){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $kolomnya = array(
                     'tampilkanposisi'    => $obj->tampilkanposisi
                 );

                 switch ($obj->tampilkanposisi){
                   case 0:
                   $kolomnya['latitude']=0;
                   $kolomnya['longitude']=0;
                   $kolomnya['firmlatitude']=0;
                   $kolomnya['firmlongitude']=0;
                   break;
                   case 1:
                   $kolomnya['firmlatitude']=0;
                   $kolomnya['firmlongitude']=0;
                   break;
                   case 3:
                   $kolomnya['firmlatitude']=0;
                   $kolomnya['firmlongitude']=0;
                   break;
                 }

                 DB::table('tb_lawyer')
                ->where('idu', $tabelsessid->id)
                ->update($kolomnya);

                    $bahandata['info']="Saved";
                     $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "set_token":
             if((! isset($obj->sessid)) || (! isset($obj->token))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 DB::table('tb_lawyer')
                ->where('idu', $tabelsessid->id)
                ->update(array(
                  'token' => $obj->token
                    ));

                    $bahandata['token']=$obj->token;
                     $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "save_position":
             if((! isset($obj->sessid)) || (! isset($obj->latitude)) || (! isset($obj->longitude))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();
               $tabellawyer=DB::table('tb_lawyer')
               ->where('idu', $tabelsessid->id)
               ->first();

               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 if(($tabellawyer->tampilkanposisi==1) || ($tabellawyer->tampilkanposisi==2)){
                       DB::table('tb_lawyer')
                      ->where('idu', $tabelsessid->id)
                      ->update(array(
                        'latitude' => $obj->latitude
                        ,'longitude' => $obj->longitude
                          ));
                  }

                    $bahandata["info"]="Position saved";
                    $dataret["respon_data"]=$bahandata;
               }
             }

             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "get_withdraw_form":
             if( (! isset($obj->sessid)) ){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();


               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $tb_lawyer=DB::table('tb_lawyer')
                 ->where('idu', $tabelsessid->id)
                 ->select(['id','saldo','idu'])
                 ->first();
                 $bahandata['saldo']=$tb_lawyer->saldo;

                 $tb_withdraw=DB::table('tb_withdraw')
                 ->where('idl', $tb_lawyer->id)
                 ->where('status','!=',3)
                 ->first();

                 if($tb_withdraw!=null){
                    $status=$tb_withdraw->status;
                    if($status==-1){
                     $bahandata['step']=1; // isi jumlah withdraw
                    }else if($status==0){
                     $bahandata['step']=2; // menunggu validasi
                     $bahandata['jumlah']=$tb_withdraw->nilai;
                     $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                     $bahandata['komenlawyer']=$tb_withdraw->komenlawyer;
                    }else if($status==1){
                      $bahandata['step']=3; // withdraw diproses
                      $bahandata['jumlah']=$tb_withdraw->nilai;
                      $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                      $bahandata['komenlawyer']=$tb_withdraw->komenlawyer;
                      $bahandata['komenpusat']=$tb_withdraw->komenpusat;
                    }else if($status==2){
                      $bahandata['step']=4; // withdraw gagal, disertai alasannya dan meminta withdraw ulang
                      $bahandata['jumlah']=$tb_withdraw->nilai;
                      $bahandata['tanggalminta']=$tb_withdraw->tanggalminta;
                      $bahandata['komenlawyer']=$tb_withdraw->komenlawyer;
                      $bahandata['komenpusat']=$tb_withdraw->komenpusat;
                    }

                 }else{
                  $bahandata['step']=1;
                 }
                 $dataret["respon_data"]=$bahandata;
               }
             }
             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "withdraw":
             if((! isset($obj->sessid)) || (! isset($obj->jumlah)) || (! isset($obj->komenlawyer))){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{
                  $tanggal=date("Y-m-d H:i:s");
                  $tb_lawyer=DB::table('tb_lawyer')
                  ->where('idu', $tabelsessid->id)
                  ->select(['id','saldo','idu'])
                  ->first();

                  $saldolawyer=$tb_lawyer->saldo;
                  $jumlah=$obj->jumlah;
                  $komenlawyer=$obj->komenlawyer;

                  if($jumlah>$saldolawyer){
                    $dataret["error_code"]="001";
                    $dataret["error_msg"]="Saldo tidak cukup";
                  }else{

                          $tb_withdraw=DB::table('tb_withdraw')
                          ->where('idl', $tabelsessid->idr)
                          ->where( function ( $query )
                              {
                                $query->where('status',-1)
                                         ->orWhere('status', 2);
                              })
                          ->first();


                          if($tb_withdraw==null){
                              $cekwithdraw=DB::table('tb_withdraw')
                              ->where('idl', $tabelsessid->idr)
                              ->where('status',0)
                              ->first();
                              if($cekwithdraw==null){
                                  DB::table('tb_withdraw')->insert(
                                  array(
                                         'idl' => $tabelsessid->idr
                                         ,'nilai'=>$jumlah
                                         ,'tanggalminta'=>$tanggal
                                         ,'komenlawyer'=>$komenlawyer
                                         ,'status'=>0
                                   )
                                   );
                              }else{
                              $dataret["error_code"]="001";
                              $dataret["error_msg"].="Pengajuan sudah pernah dibuat";
                              }
                          }else{
                           DB::table('tb_withdraw')
                           ->where('id', $tb_withdraw->id)
                           ->update(array(
                             'nilai'=>$jumlah
                             ,'tanggalminta'=>$tanggal
                             ,'komenlawyer'=>$komenlawyer
                             ,'status'=>0
                            ));
                          }

                }

                }

             }

             if($dataret["error_code"]!="001"){
               $bahandata['info']="pengajuan withdraw berhasil dibuat. Silahkan menunggu validasi dari pusat";
               $dataret["respon_data"]=$bahandata;
             }
             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "cancel_withdraw":
             if( (! isset($obj->sessid)) ){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();
                if($tabelsessid==null){
                  $dataret["error_code"]="001";
                  $dataret["error_msg"]="Invalid session";
                }else{

                  DB::table('tb_withdraw')
                  ->where('idl', $tabelsessid->idr)
                  ->where( function ( $query )
                      {
                        $query->where('status',0)
                                 ->orWhere('status','=', 2);
                      })
                  ->update(array(
                    'status'=>-1
                   ));

                }

                }


             if($dataret["error_code"]!="001"){
               $bahandata['info']="Pengajuan withdraw berhasil dibatalkan";
               $dataret["respon_data"]=$bahandata;
             }
             $hasil=json_encode($dataret);
             echo $hasil;
             break;
             case "log_withdraw":
             if((! isset($obj->sessid)) ){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();


               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{

                   $idl=$tabelsessid->idr;
                   $history=DB::table('tb_withdraw')
                   ->where('idl', $idl)
                   ->where('status',3)
                   ->orderBy('id', 'DESC');


                   if(isset($obj->periode)){

                     $timestamp=strtotime($obj->periode);

                     $history->whereRaw('YEAR(tanggalminta) = ?', [date('Y',$timestamp)])
                     ->whereRaw('MONTH(tanggalminta) = ?', [date('m',$timestamp)]);
                   }else{
                     $history->whereRaw('tanggalminta >= now()-interval 3 month');
                   }
                   $history=$history->get();
                   $tabelhistorybaru=$history;


                $isitabel=json_encode($tabelhistorybaru);
                $dataret["respon_data"]=$tabelhistorybaru;

               }
             }

             $hasil=json_encode($dataret);

             echo $hasil;
             break;
             case "billingfee":
             if((! isset($obj->sessid)) ){
             $dataret["error_code"]="001";
             $dataret["error_msg"].="Parameter tidak lengkap.";
             }else{
               $tabelsessid=DB::table('tb_user')
               ->where('level',2)
               ->where('sessid',$obj->sessid)
               ->first();


               if($tabelsessid==null){
                 $dataret["error_code"]="001";
                 $dataret["error_msg"]="Invalid session";
               }else{
                 $idl=$tabelsessid->idr;
                  $tanggal=date("Y-m-d");
                  $tamp=strtotime($tanggal);
                  $periode_y=date('Y',$tamp);
                   $idl=$tabelsessid->idr;
                   $history=DB::table('tb_mutasi')
                   ->where('idl', $idl)
                   ->whereRaw('YEAR(periode) = ?', [$periode_y])
                   ->orderBy('id', 'DESC')
                   ->select(['id','idl',DB::raw('MONTH(periode) as periode'),'fee','share','tl','withdraw','saldo'])
                   ->get();
                   $tabelhistorybaru=$history;


                $isitabel=json_encode($tabelhistorybaru);
                $dataret["respon_data"]=$tabelhistorybaru;

               }
             }

             $hasil=json_encode($dataret);

             echo $hasil;
             break;
           }
           //akhir case lawyer

           break;
         }


       }

       public function kirimbuktitransfertopup(){
         $idt=Input::get('topup_id');
         $destinationPath = 'uploads/images';
         if(Input::file('gambarbukti')!=null){
           $extension = Input::file('gambarbukti')->getClientOriginalExtension(); // getting image extension
           $namafileasli=Input::file('gambarbukti')->getClientOriginalName();
           //$fileName = rand(11111,99999).'.'.$extension; // renameing image
           $fileName = "buktitransfertopup_".$idt.'.'.$extension;
           Input::file('gambarbukti')->move($destinationPath, $fileName);

           DB::table('tb_topup')
           ->where('id', $idt)
           ->update(array(
             'buktitransfer'=>$fileName
            ,'last_update' => date("Y-m-d H:i:s")
            ));

            return 1;
         }else{
           return 0;
         }
       }

              public function kirimGambar()
              {
                $judulpesan=Input::get('judulpesan');
                $idk=Input::get('idk');
                $isipesan=Input::get('isipesan');
                $jumlahfile=Input::get('jumlahfile');
                $destinationPath = 'uploads/images'; // upload path
                $tanggal=date("Y-m-d H:i:s");
/**
                for($i=0;$i<$jumlahfile;$i++){
                      if(Input::file('uploaded_file'.$i)!=null){
                $extension = Input::file('uploaded_file'.$i)->getClientOriginalExtension(); // getting image extension
                $namafileasli=Input::file('uploaded_file'.$i)->getClientOriginalName();
                //$fileName = rand(11111,99999).'.'.$extension; // renameing image
                $fileName = $namafileasli;
                Input::file('uploaded_file'.$i)->move($destinationPath, $fileName);

                                  }
                }

                **/
                $idp = DB::table('tb_pesan')->insertGetId(
                array(
                       'idk' => $idk,
                       'judul'=>$judulpesan,
                       'pesan'=>$isipesan,
                       'tanggal'=>$tanggal
                 )
                 );

                for($i=0;$i<$jumlahfile;$i++){
            //    $extension = Input::file('uploaded_file');
                  if(Input::file('uploaded_file'.$i)!=null){
                  $ukuranfile=Input::get('ukuranfile'.$i);
                    $extension = Input::file('uploaded_file'.$i)->getClientOriginalExtension(); // getting image extension
                    $namafileasli=Input::file('uploaded_file'.$i)->getClientOriginalName();
                    //$fileName = rand(11111,99999).'.'.$extension; // renameing image
                    $fileName = $idp.'_'.$namafileasli;
                    Input::file('uploaded_file'.$i)->move($destinationPath, $fileName);

                                     DB::table('tb_filepesan')->insert(
                                    array(
                                           'idp' => $idp,
                                           'name'=>$fileName,
                                           'ukuran'=>$ukuranfile
                                     )
                                     );

                  }
                }
                return "selesai";
              }

              public function kirimFotoProduk(){
                $sessid=Input::get('sessid');
                $idp=Input::get('idp');
                $destinationPath = 'uploads/images';
                if(Input::file('fotoproduk')!=null){

                    $tabelsessid=DB::table('tb_user')
                    ->where('sessid',$sessid)
                    ->select(['id','idr','level'])
                    ->first();

                  if($tabelsessid!=null){
                    $extension = Input::file('fotoproduk')->getClientOriginalExtension(); // getting image extension
                    $namafileasli=Input::file('fotoproduk')->getClientOriginalName();

                    $fileName = "fotoutamaproduk".$idp.'.'.$extension;
                    Input::file('fotoproduk')->move($destinationPath, $fileName);

                    DB::table('tb_product')
                    ->where('id', $idp)
                    ->update(array(
                      'mainpict'=>$fileName
                     ));
                   return 1;
                  }else{

                   return 0;
                  }

                }else{
                  return 0;
                }
              }
              public function kirimFotoProfil(){
                $sessid=Input::get('sessid');
                $destinationPath = 'uploads/images';
                if(Input::file('fotoprofil')!=null){

                    $tabelsessid=DB::table('tb_user')
                    ->where('sessid',$sessid)
                    ->select(['id','idr','level'])
                    ->first();

                  if($tabelsessid!=null){
                    $extension = Input::file('fotoprofil')->getClientOriginalExtension(); // getting image extension
                    $namafileasli=Input::file('fotoprofil')->getClientOriginalName();
                    $namafoto="fotoklien";
                    if($tabelsessid->level==2){
                      $namafoto="fotolawyer";
                    }
                    $fileName = $namafoto.$tabelsessid->idr.'.'.$extension;
                    Input::file('fotoprofil')->move($destinationPath, $fileName);

                    DB::table('tb_user')
                    ->where('id', $tabelsessid->id)
                    ->update(array(
                      'filefoto'=>$fileName
                     ));
                   return 1;
                  }else{

                   return 0;
                  }

                }else{
                  return 0;
                }
              }

                     public function halVoip()
                     {
                       return view('hal_visitor.halvoip');
                     }

                     public function halVoipSpy()
                     {
                       return view('hal_visitor.halvoipspy');
                     }

                     public function halAPI($aksestoken){
                       $app_secret="e370c1f222681be24a536a4d09881744";
                       $appsecret_proof= hash_hmac('sha256', $aksestoken, $app_secret);

                       $url="https://graph.accountkit.com/v1.1/me/?access_token=".$aksestoken."&appsecret_proof=".$appsecret_proof;
                       $ch = curl_init($url);
                       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                       curl_setopt($ch, CURLOPT_HTTPHEADER,
                               array( 'Content-Type: application/json','Accept: application/json')
                       );
                       $result = curl_exec($ch);
                       return $result;
                     }


}
