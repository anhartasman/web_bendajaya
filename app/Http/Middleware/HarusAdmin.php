<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Illuminate\Support\Facades\Auth;

class HarusAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


          if($request->session()->has('coklogin')==false){
           return redirect('admin/login');
          }
          $pangkatuser=getUserInfo('user_pangkat');
          $roles=array_slice(func_get_args(),2);
          foreach ($roles as $role){
            if($role==$pangkatuser){
                       return $next($request);
            }
          }

           return redirect('admin/login');




    }
}
