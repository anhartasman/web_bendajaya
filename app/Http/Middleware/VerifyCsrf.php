<?php

namespace App\Http\Middleware;

// use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
class VerifyCsrf extends ExceptionHandler
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $routes = [
        'api/v1/apis/get_menus_header',
        'tesservis',
    ];

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    *
    * @throws \Illuminate\Session\TokenMismatchException
    */
    public function handle($request, \Closure $next)
    {
        if ($this->isReading($request)
           || $this->excludedRoutes($request)
           || $this->tokensMatch($request))
        {
           return $this->addCookieToResponse($request, $next($request));
        }

        throw new \TokenMismatchException;
    }

   /**
    * This will return a bool value based on route checking.
    * @param  Request $request
    * @return boolean
    */
    protected function excludedRoutes($request)
    {
        foreach($this->routes as $route)
            if ($request->is($route))
                return true;

            return false;
    }
}
