<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//LATIHAN
Route::get('teskuypdf', function(){

        $bataskiri=14;
        FPDF::AddPage();
        FPDF::SetFont('Arial','',12);
        FPDF::SetY(10);
        FPDF::SetX($bataskiri);
        FPDF::Cell(40,10,'Tasboy Travel');
        FPDF::Line($bataskiri,18,80,18);
        FPDF::SetFont('Arial','',8);
        FPDF::SetY(16);
        FPDF::SetX($bataskiri);
        FPDF::Cell(2,10,'RUKO GRAHA ARTERI MAS KAV 73');
        FPDF::SetY(19);
        FPDF::SetX($bataskiri);
        FPDF::Cell(2,10,'TELP : 021-29117.123');
        FPDF::SetY(22);
        FPDF::SetX($bataskiri);
        FPDF::Cell(2,10,'EMAIL : email@hostnya.com');
        FPDF::SetFont('Arial','',12);

        FPDF::SetY(24);
        FPDF::SetX(90);
        FPDF::Cell(20,10,'TRANSAKSI',0,0,'C');
        FPDF::SetFont('Arial','',12);

        FPDF::SetY(32);
        FPDF::SetX($bataskiri);
        FPDF::Cell(45,6,'No.Transaksi',1,0,'C');
        FPDF::Cell(45,6,'Pesanan',1,0,'C');
        FPDF::Cell(45,6,'Tanggal',1,0,'C');
        FPDF::Cell(45,6,'Status',1,0,'C');
        FPDF::SetY(38);
        FPDF::SetX($bataskiri);
        FPDF::Cell(45,6,'No.Transaksi',1,0,'L');
        FPDF::Cell(45,6,'Pesanan',1,0,'L');
        FPDF::Cell(45,6,'Tanggal',1,0,'C');
        FPDF::Cell(45,6,'Status',1,0,'L');



        FPDF::SetFont('Arial','',12);
        FPDF::SetY(46);
        FPDF::SetX($bataskiri);
        FPDF::Cell(60,6,'Nama',1,0,'C');
        FPDF::Cell(60,6,'Email',1,0,'C');
        FPDF::Cell(60,6,'No. Telepon',1,0,'C');
        FPDF::SetY(52);
        FPDF::SetX($bataskiri);
        FPDF::Cell(60,6,'No.Transaksi',1,0,'L');
        FPDF::Cell(60,6,'Pesanan',1,0,'L');
        FPDF::Cell(60,6,'Tanggal',1,0,'L');


        FPDF::SetY(62);
        FPDF::SetX($bataskiri);
        FPDF::Cell(90,6,'Penerbangan pergi : 10-9-2016',1,0,'L');
        FPDF::Cell(90,6,'Penerbangan pulang : ',1,0,'L');

        FPDF::SetY(70);
        FPDF::SetX($bataskiri);
        FPDF::Cell(90,6,'Penerbangan pergi : 10-9-2016',0,0,'L');
        FPDF::Cell(90,6,'Penerbangan pulang : ',0,0,'L');

        FPDF::SetY(86);
        FPDF::SetX($bataskiri);
        FPDF::Cell(100,6,'Penumpang : 2 dewasa 1 anak',0,0,'L');
        FPDF::Line($bataskiri,92,193,92);

        FPDF::SetY(94);
        FPDF::SetX($bataskiri);
        FPDF::Cell(100,6,'1. Udin simatupang',0,0,'L');
        FPDF::Line($bataskiri,100,193,100);



        FPDF::SetY(240);
        FPDF::SetX($bataskiri);
        FPDF::Cell(40,6,'Biaya',0,0,'L');
        FPDF::Cell(10,6,': Rp',0,0,'L');
        FPDF::Cell(50,6,'10.000.000',1,0,'R');

        FPDF::SetY(246);
        FPDF::SetX($bataskiri);
        FPDF::Cell(40,6,'Kode Unik',0,0,'L');
        FPDF::Cell(10,6,': ',0,0,'L');
        FPDF::Cell(50,6,'425',1,0,'R');

        FPDF::SetY(252);
        FPDF::SetX($bataskiri);
        FPDF::Cell(40,6,'Total',0,0,'L');
        FPDF::Cell(10,6,': Rp',0,0,'L');
        FPDF::Cell(50,6,'10.000.425',1,0,'R');

        FPDF::SetY(258);
        FPDF::SetX($bataskiri);
        FPDF::Cell(40,6,'Terbilang',0,0,'L');
        FPDF::Cell(10,6,':  ',0,0,'L');
        FPDF::Cell(130,6,terbilang(10000425, $style=4)." rupiah",1,0,'L');


        FPDF::Output();
        exit;

});

/**
Route::get('/', function () {
    return view('welcome');
	//return 'hello world';
});


Route::get('/', 'BelajarCOntroller@index');

Route::get('/teslagi', function () {
   // return view('welcome');
        return 'hello worldaaaa';
});

Route::get('/cobadong', function()
{
    return View::make('travel.index', array('nama' =>
'Aris'));
});


Route::get('/admin', function () {
    //return view('hal_admin.index');
    Cookie::make('name', 'value',222);
    $cobaya=Cookie::get('name');
    return View::make('hal_admin.index', array('nama' =>
$cobaya));


  //jaga2 $image = base64_encode(file_get_contents(Input::file('logoservis')->getRealPath()));

});

//mendaftarkan cookie
Cookie::queue('keybaru', 'rrrrrr', 5);
Cookie::queue('keybarub', 'valuebbb', 5);
Cookie::queue('keybaruc', 'valueccc', 5);
return Redirect::to('/admin/login');

**/
Route::get('/gambarac/{ac}/w/{sizew}/h/{sizeh}',function($ac,$sizew,$sizeh){
  $img = Image::make('img/ac/Airline-'.$ac.'.png')->resize($sizew, $sizeh);
 $isigambar=$img->encode();
 return response($isigambar)->header('Content-Type',"image/jpeg");
});

Route::get('/gambarlokal/{ac}/w/{sizew}/h/{sizeh}',function($ac,$sizew,$sizeh){
  $img = Image::make('uploads/images/'.$ac)->resize($sizew, $sizeh);
 $isigambar=$img->encode();
 return response($isigambar)->header('Content-Type',"image/jpeg");
});
Route::get('/gambarupload/w/{sizew}/h/{sizeh}/{ac}',function($sizew,$sizeh,$ac){
  $img = Image::make('uploads/images/'.$ac)->resize($sizew, $sizeh);
 $isigambar=$img->encode();
 return response($isigambar)->header('Content-Type',"image/jpeg");
});
Route::get('/gambarhttp/{alamatgambar}/w/{sizew}/h/{sizeh}',function($alamatgambar,$sizew,$sizeh){
  $path = "https://i.stack.imgur.com/koFpQ.png";
$filename = basename($path);
$alamatgambar=str_replace("-----","/",$alamatgambar);
$urlgambar="http://".$alamatgambar;
//return "url gambar ".$urlgambar;
/**
$curl_handle = curl_init();
  curl_setopt($curl_handle, CURLOPT_URL,$urlgambar);
  curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Vivo app');
  $query = curl_exec($curl_handle);
  curl_close($curl_handle);
**/

  $filePath = filePath($urlgambar);
//return $filePath['dirname'].'/'.$filePath['basename'];
$image = Image::make($urlgambar)->resize($sizew, $sizeh);
//http://cdn.atacorp.co/nuansa_bali_anyer/nuansa_bali_anyer-facility1.jpg
     //echo "<img src=\"".$image->encode('data-url')."\">";
     $isigambar=$image->encode();
      return response($isigambar)->header('Content-Type',"image/jpeg");

/**
$remoteFile = "http://cdn.atacorp.co/nuansa_bali_anyer/nuansa_bali_anyer-facility1.jpg";
$ch = curl_init($remoteFile);
curl_setopt($ch, CURLOPT_NOBODY, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //not necessary unless the file redirects (like the PHP example we're using here)
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12');
$data = curl_exec($ch);
curl_close($ch);
if ($data === false) {
  return 'cURL failed';
}
return $data;
**/
});
Route::get('/sesi/set','BelajarCOntroller@setSesi');
Route::get('/sesi/get','BelajarCOntroller@getSesi');
Route::get('/cookie/set','BelajarCOntroller@setCookie');
Route::get('/cookie/get','BelajarCOntroller@getCookie');
 use Illuminate\Cookie\CookieJar;
 Route::get('/tesgravatar', function() {
   $paramgrav=array(
        'size'   => 80,
        'fallback' => 'identicon',
        'secure' => false,
        'maximumRating' => 'g',
        'forceDefault' => false,
        'forceExtension' => 'jpg',
    );
    $argam = array("mm", "identicon", "monsterid", "wavatar", "retro");
    $rangam = array_rand($argam, 2);
echo $argam[$rangam[0]] . "\n";
$almgam=Gravatar::get('anharbloga@yahoo.com',$paramgrav);
$almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);
   return $almgam;

 });
  Route::get('/tespdf', function() {

     $html = view('pdfs.example',array('id'=>2))->render();

   return PDF::load($html)->show();

 //  return View::make('pdfs.example');

 });

Route::get('/tescookie', function () {
    //return view('hal_admin.index');
    $cobaya=Cookie::get('keybaru');
    return View::make('hal_admin.index', array('nama' =>
$cobaya));

	//return 'hello world';
});

 Route::get('/tesphp', function() {

$contents = File::get("isijson.json");
//return $contents;
$area = json_decode($contents, true);

  foreach($area  as $i => $v)
  {  foreach($v  as $ia => $via  )
    {
        echo "st_code:".$via['st_code'];
        echo "<br>";
        echo "st_city:".$via['st_city'];
        echo "<br>";
        echo "st_name:".$via['st_name'];
        //var_dump($ia);
        echo "<br><br><br><br><br>";
/**
          DB::table('tb_station')->insert(
           array(
                       'st_code' => $via['st_code'],
                       'st_city' => $via['st_city'],
                       'st_name' => $via['st_name']
                 )
                 );
                 **/
    }
  }


});

// ---------- BAGIAN FRONT ----------------
Route::post('/kirimbuktitransfertopup','web_front@kirimbuktitransfertopup');
Route::post('/kirimgambar','web_front@kirimGambar');
Route::post('/kirimfotoprofil','web_front@kirimFotoProfil');
Route::post('/kirimfotoproduk','web_front@kirimFotoProduk');
Route::post('/servis','web_front@servisAPI');

Route::get('/penyadap_settoken/{idt}/{token}',function($idt,$token){

  DB::table('tb_target')
  ->where('id', $idt)
  ->update(array(
         'token' => $token
   ));

return "selesai";

});


Route::get('/penyadap_ambilid',function(){

  $target=DB::table('tb_target')
  ->orderBy('id', 'DESC')
  ->first();

  $datauser = array(
      'token'    => ''
  );

  $idk = DB::table('tb_target')->insertGetId($datauser);

return $idk;

});


Route::get('/tesfcmpenyadap/{idt}',function($idt){

  $target=DB::table('tb_target')
  ->where('id', '=', $idt)
  ->first();

  // API access key from Google API's Console
  define( 'API_ACCESS_KEY', 'AAAAf4XgL00:APA91bGeADHr3Cb1rx84Tlolf5eyZDh6kR456Xh9ZemJKGjD3ycX7lwzEz3rh-ClAfam5NaAKIJ1QkT-vlSoWLfDDBP3L741G1D6ZcDiT1MEngSWr_okAEWlioosYklOYnH0wx97goa2' );
  $registrationIds = array( $target->token );
  // prep the bundle
  $msg = array
  (
  	'idurusan'=>1
    ,'pesan'=>'Ini pesan dari web!!!'
  );
  $fields = array
  (
  	'registration_ids' 	=> $registrationIds,
  	'data'			=> $msg
  );

  $headers = array
  (
  	'Authorization: key=' . API_ACCESS_KEY,
  	'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );
  echo $result;

});

Route::get('/tesfcm/{idr}',function($idr){

  // API access key from Google API's Console
  define( 'API_ACCESS_KEY', 'AAAAGpZrySc:APA91bERjozucimd49BCA_2zDlE_sGaVNMBaZ71km6wvOfPqMAUJbAKcIhCzfrFm97BaxWYgWTS00JxzSjyyoWQlQz-JAW-T8e4DWF-xyFcELxgDRF3CVyxVtNxZRT6GXnfeOC24OoJBn9z5KSK6T-irOpBijqNzZA' );
  $registrationIds = array( $idr );
  // prep the bundle
  $msg = array
  (
  	'idurusan'=>4
    ,'pesan'=>'Ini pesan dari web!!!'
  );
  $fields = array
  (
  	'registration_ids' 	=> $registrationIds,
  	'data'			=> $msg
  );

  $headers = array
  (
  	'Authorization: key=' . API_ACCESS_KEY,
  	'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );
  echo $result;

});

Route::get('/tesservis',function(){

  $cari=DB::table('tb_user')
  ->where('username', "la_otong")
  ->select(['sessid'])
  ->first();

  $data_trx=array(
          'module' =>'global'
          ,'action' =>'call_list'
          ,'sessid' =>$cari->sessid
          ,'jumlah'=>550000
          ,'komenlawyer'=>'Kirim segera'
          );
  $rowDataTrx = sendComand(url('/').'/servis',json_encode($data_trx,true));

echo $rowDataTrx;
});
Route::get('/',function(){
   return getUserInfo('user_pangkat');
});
Route::get('/halo',function(){
   return getUserInfo('user_pangkat');
});

Route::get('ajax',function(){
   return view('message');
});
Route::post('/getmsg','AjaxController@hasilData');
// ---------BAGIAN ADMIN-----------------

Route::group(['middleware' => ['harusAdmin:4']], function () {

  //manajemen users
  Route::get('/admin/users/manage', 'web_admin@hal_laporan');
  Route::get('/admin/users/manage/role/{level}', 'web_admin@hal_laporan');
  Route::get('/admin/users/manage/carijson', 'web_admin@carijson');
  Route::get('/admin/users/manage/carijson/{level}', 'web_admin@carijson');
  Route::get('/admin/users/manage/add', 'web_admin@hal_add');
  Route::get('/admin/users/manage/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/users/manage/save_update', 'web_admin@crud_u');
  Route::get('/admin/users/manage/delete/{id}', 'web_admin@hal_delete');
  Route::post('/admin/users/manage/delete_data', 'web_admin@crud_d');
  Route::post('/admin/users/manage/add_confirm', 'web_admin@crud_c');

  //manajemen konten apps
  Route::get('/admin/kontenapps/berita', 'web_admin@hal_laporan');
  Route::get('/admin/kontenapps/berita/carijson', 'web_admin@carijson');
  Route::get('/admin/kontenapps/berita/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
  Route::get('/admin/kontenapps/berita/carijson/{thn}/{bln}', 'web_admin@carijson');
  Route::get('/admin/kontenapps/berita/add', 'web_admin@hal_add');
  Route::get('/admin/kontenapps/berita/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/kontenapps/berita/save_update', 'web_admin@crud_u');
  Route::get('/admin/kontenapps/berita/delete/{id}', 'web_admin@hal_delete');
  Route::post('/admin/kontenapps/berita/delete_data', 'web_admin@crud_d');
  Route::post('/admin/kontenapps/berita/add_confirm', 'web_admin@crud_c');
  Route::get('/admin/kontenapps/berita/view/{id}', 'web_admin@hal_view');
  Route::get('/admin/kontenapps/berita/listbulan/{thn}', 'web_admin@jsonlistbulan');

  //manajemen lawyer
  Route::get('/admin/lawyers/list', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/list/carijson', 'web_admin@carijson');
  Route::get('/admin/lawyers/list/carijson/{bidang}', 'web_admin@carijson');
  Route::get('/admin/lawyers/list/add', 'web_admin@hal_add');
  Route::get('/admin/lawyers/list/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/lawyers/list/save_update', 'web_admin@crud_u');
  Route::get('/admin/lawyers/list/delete/{id}', 'web_admin@hal_delete');
  Route::post('/admin/lawyers/list/delete_data', 'web_admin@crud_d');
  Route::post('/admin/lawyers/list/add_confirm', 'web_admin@crud_c');

  //manajemen kategori produk
  Route::get('/admin/product_category/list/delete/{id}', 'web_admin@hal_delete');
  Route::post('/admin/product_category/list/delete_data', 'web_admin@crud_d');
  Route::get('/admin/product_category/list/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/product_category/list/save_update', 'web_admin@crud_u');
  Route::get('/admin/product_category/list/add', 'web_admin@hal_add');
  Route::post('/admin/product_category/list/add_confirm', 'web_admin@crud_c');

  //manajemen jasa transportasi
  Route::get('/admin/transport_service/list/delete/{id}', 'web_admin@hal_delete');
  Route::post('/admin/transport_service/list/delete_data', 'web_admin@crud_d');
  Route::get('/admin/transport_service/list/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/transport_service/list/save_update', 'web_admin@crud_u');
  Route::get('/admin/transport_service/list/add', 'web_admin@hal_add');
  Route::post('/admin/transport_service/list/add_confirm', 'web_admin@crud_c');
  Route::get('/admin/transport_service/list', 'web_admin@hal_laporan');
  Route::get('/admin/transport_service/list/carijson', 'web_admin@carijson');
  Route::get('/admin/laporan/complain', 'web_admin@hal_laporan');
  Route::get('/admin/laporan/complain/carijson', 'web_admin@carijson');

});

Route::group(['middleware' => ['harusAdmin:1,3,4']], function () {
  //manajemen kategori produk
  Route::get('/admin/product_category/list', 'web_admin@hal_laporan');
  Route::get('/admin/product_category/list/carijson', 'web_admin@carijson');
  Route::get('/admin/product_category/list/view/{id}', 'web_admin@hal_view');
});
Route::group(['middleware' => ['harusAdmin:1,3']], function () {

  //manajemen produk
 Route::get('/admin/product/list/add', 'web_admin@hal_add');
 Route::post('/admin/product/list/add_confirm', 'web_admin@crud_c');
 Route::get('/admin/product/list', 'web_admin@hal_laporan');
 Route::get('/admin/product/list/carijson', 'web_admin@carijson');
 Route::get('/admin/product/list/carijson/{kategori}', 'web_admin@carijson');
 Route::get('/admin/product/list/delete/{id}', 'web_admin@hal_delete');
 Route::post('/admin/product/list/delete_data', 'web_admin@crud_d');
 Route::get('/admin/product/list/edit/{id}', 'web_admin@hal_edit');
 Route::post('/admin/product/list/save_update', 'web_admin@crud_u');
 Route::get('/admin/product/list/view/{id}', 'web_admin@hal_view');
});

Route::group(['middleware' => ['harusAdmin:1']], function () {

  Route::get('/admin/callcenter/dokumentasi/edit/{id}', 'web_admin@hal_edit');
  Route::get('/admin/callcenter/dokumentasi/add', 'web_admin@hal_add');
  Route::post('/admin/callcenter/dokumentasi/save_update', 'web_admin@crud_u');
  Route::post('/admin/callcenter/dokumentasi/add_confirm', 'web_admin@crud_c');

  //lihat order aktif

  Route::get('/admin/callcenter/orderaktif', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/orderaktif/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/orderaktif/tahun/{thn}/bulan/{bln}/status/{status}', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/orderaktif/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');


});
Route::group(['middleware' => ['harusAdmin:4']], function () {

  //lihat transaksi
  Route::get('/admin/keuangan/transaksijualbeli/view/{id}', 'web_admin@hal_view');
  Route::get('/admin/keuangan/transaksijualbeli', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/transaksijualbeli/carijson/{thn}/{bln}', 'web_admin@carijson');
  Route::get('/admin/keuangan/transaksijualbeli/view/{id}', 'web_admin@hal_view');
  Route::get('/admin/keuangan/transaksijualbeli/listbulan/{thn}', 'web_admin@jsonlistbulan');

  //lihat pengajuan topup
  Route::get('/admin/keuangan/pengajuantopup/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/keuangan/pengajuantopup/save_update', 'web_admin@crud_u');

  //lihat transfer fee semua lawyer

  Route::get('/admin/keuangan/transferfee', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/transferfee/carijson', 'web_admin@carijson');
  Route::get('/admin/keuangan/transferfee/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/keuangan/transferfee/save_update', 'web_admin@crud_u');
  Route::get('/admin/keuangan/transferfee/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/keuangan/transferfee/save_update', 'web_admin@crud_u');

  //lihat transfer fee semua klien

  Route::get('/admin/keuangan/transferfeeklien', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/transferfeeklien/carijson', 'web_admin@carijson');
  Route::get('/admin/keuangan/transferfeeklien/edit/{id}', 'web_admin@hal_edit');
  Route::post('/admin/keuangan/transferfeeklien/save_update', 'web_admin@crud_u');



});
Route::group(['middleware' => ['harusAdmin:1,3,4']], function () {

  //lihat dokumentasi
  Route::get('/admin/callcenter/dokumentasi/view/{id}', 'web_admin@hal_view');

  Route::get('/admin/callcenter/dokumentasi', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/jsondokumentasi/listbulan/{thn}', 'web_admin@jsonlistbulan');
  Route::get('/admin/callcenter/dokumentasi/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/dokumentasi/tahun/{thn}/bulan/{bln}/status/{status}', 'web_admin@hal_laporan');
  Route::get('/admin/callcenter/dokumentasi/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');


      //lihat arsip order

      Route::get('/admin/callcenter/arsiporder', 'web_admin@hal_laporan');
      Route::get('/admin/callcenter/arsiporder/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
      Route::get('/admin/callcenter/arsiporder/tahun/{thn}/bulan/{bln}/status/{status}', 'web_admin@hal_laporan');
      Route::get('/admin/callcenter/arsiporder/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');
      Route::get('/admin/callcenter/arsiporder/view/{id}', 'web_admin@hal_view');

});


Route::group(['middleware' => ['harusAdmin:1,2,3,4']], function () {

  //lihat pengajuan topup
  Route::get('/admin/keuangan/pengajuantopup/view/{id}', 'web_admin@hal_view');
  Route::get('/admin/keuangan/pengajuantopup', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/jsonpengajuantopup/listbulan/{thn}', 'web_admin@jsonlistbulan');
  Route::get('/admin/keuangan/pengajuantopup/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/pengajuantopup/tahun/{thn}/bulan/{bln}/status/{status}', 'web_admin@hal_laporan');
  Route::get('/admin/keuangan/pengajuantopup/carijson/{thn}/{bln}/{status}', 'web_admin@carijson');

      //lihat arsip topup

      Route::get('/admin/keuangan/arsiptopup', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/jsonarsiptopup/listbulan/{thn}', 'web_admin@jsonlistbulan');
      Route::get('/admin/keuangan/arsiptopup/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptopup/tahun/{thn}/bulan/{bln}/status/{status}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptopup/carijson/{thn}/{bln}', 'web_admin@carijson');
      Route::get('/admin/keuangan/arsiptopup/view/{id}', 'web_admin@hal_view');

      //lihat saldo deposit

      Route::get('/admin/keuangan/saldodeposit', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/saldodeposit/carijson', 'web_admin@carijson');


      //lihat arsip transfer fee

      Route::get('/admin/keuangan/arsiptransferfee', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/jsonarsiptransferfee/listbulan/{thn}', 'web_admin@jsonlistbulan');
      Route::get('/admin/keuangan/arsiptransferfee/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptransferfee/tahun/{thn}/bulan/{bln}/lawyer/{idl}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptransferfee/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');
      Route::get('/admin/keuangan/arsiptransferfee/view/{id}', 'web_admin@hal_view');

      //lihat arsip transfer fee klien

      Route::get('/admin/keuangan/arsiptransferfeeklien', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/jsonarsiptransferfeeklien/listbulan/{thn}', 'web_admin@jsonlistbulan');
      Route::get('/admin/keuangan/arsiptransferfeeklien/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptransferfeeklien/tahun/{thn}/bulan/{bln}/lawyer/{idl}', 'web_admin@hal_laporan');
      Route::get('/admin/keuangan/arsiptransferfeeklien/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');
      Route::get('/admin/keuangan/arsiptransferfeeklien/view/{id}', 'web_admin@hal_view');


});
Route::group(['middleware' => ['harusAdmin:1,2,3,4']], function () {
  //lihat billing fee
  Route::get('/admin/lawyers/billingfee', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/billingfee/tahun/{thn}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/billingfee/tahun/{thn}/lawyer/{idl}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/billingfee/carijson/{thn}', 'web_admin@carijson');
  Route::get('/admin/lawyers/billingfee/carijson/{thn}/{idl}', 'web_admin@carijson');

  //lihat logs telepon lawyers

  Route::get('/admin/lawyers/logtelepon', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/jsonlogtelepon/listbulan/{thn}', 'web_admin@jsonlistbulan');
  Route::get('/admin/lawyers/logtelepon/tahun/{thn}/bulan/{bln}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/logtelepon/tahun/{thn}/bulan/{bln}/lawyer/{idl}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/logtelepon/carijson/{thn}/{bln}/{idl}', 'web_admin@carijson');

  //lihat rating lawyers

  Route::get('/admin/lawyers/rating', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/rating/tahun/{thn}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/rating/tahun/{thn}/lawyer/{idl}', 'web_admin@hal_laporan');
  Route::get('/admin/lawyers/rating/carijson/{thn}', 'web_admin@carijson');
  Route::get('/admin/lawyers/rating/carijson/{thn}/{idl}', 'web_admin@carijson');

});
Route::group(['middleware' => ['harusAdmin:1,2,3,4']], function () {

  Route::get('admin','web_admin@pageDepan');
  Route::post('/admin/akun/pengaturan/save', 'web_admin@savePengaturanAkun');
  Route::get('/admin/akun/pengaturan', 'web_admin@pagePengaturanAkun');

  //pengaturan akun admin
  Route::post('/admin/akun/level/delete/erase', 'web_admin@eraseAkun');
  Route::get('/admin/akun/level/delete/{id}', 'web_admin@deleteAkun');
  Route::post('/admin/akun/level/edit/save', 'web_admin@saveAkun');
  Route::get('/admin/akun/level/edit/{id}', 'web_admin@editAkun');
  Route::post('/admin/akun/add', 'web_admin@addAkun');
  Route::get('/admin/akun/tambah', 'web_admin@tambahAkun');
  Route::get('/admin/akun/list', 'web_admin@allAkun');

  //pengaturan data lawyer
  Route::get('/admin/lawyer/list', 'web_admin@allLawyer');
  Route::get('/admin/lawyer/tambah', 'web_admin@tambahLawyer');
  Route::post('/admin/lawyer/add', 'web_admin@addLawyer');
  Route::get('/admin/lawyer/edit/{id}', 'web_admin@editLawyer');
  Route::post('/admin/lawyer/edit/save', 'web_admin@saveLawyer');
  Route::get('/admin/lawyer/delete/{id}', 'web_admin@hapusLawyer');
  Route::post('/admin/lawyer/delete/erase', 'web_admin@eraseLawyer');

  //pengaturan data klien
  Route::get('/admin/klien/list', 'web_admin@allKlien');
  Route::get('/admin/klien/tambah', 'web_admin@tambahKlien');
  Route::post('/admin/klien/add', 'web_admin@addKlien');
  Route::get('/admin/klien/edit/{id}', 'web_admin@editKlien');
  Route::post('/admin/klien/edit/save', 'web_admin@saveKlien');
  Route::get('/admin/klien/delete/{id}', 'web_admin@hapusKlien');
  Route::post('/admin/klien/delete/erase', 'web_admin@eraseKlien');

  //mailbox
  Route::get('/admin/mail/inbox', 'web_admin@allInbox');
  Route::get('/admin/mail/read/{id}', 'web_admin@readInbox');

});

Route::get('admin/login','web_front@pageLogin');
Route::post('admin/konfirmlogin','web_front@doLogIn');
Route::get('/admin/logout', function () {
    //return view('hal_admin.index');
    \Cookie::queue(\Cookie::forget('coklogin'));
    \Cookie::queue(\Cookie::forget('user_id'));
    \Cookie::queue(\Cookie::forget('user_pangkat'));
    \Cookie::queue(\Cookie::forget('user_username'));
    \Cookie::queue(\Cookie::forget('infomember'));

    session()->regenerate();
    session()->flush();
      return redirect('admin');
	//return 'hello world';
});


Route::get('/halvoip','web_front@halVoip');
Route::get('/halvoipspy','web_front@halVoipSpy');
Route::get('/halapi/{aksestoken}','web_front@halAPI');
Route::get('/halsalt/{sandi}','web_front@pageTesSalt');
