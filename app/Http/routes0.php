<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//LATIHAN
/**
Route::get('/', function () {
    return view('welcome');
	//return 'hello world';
});


Route::get('/', 'BelajarCOntroller@index');

Route::get('/teslagi', function () {
   // return view('welcome');
        return 'hello worldaaaa';
});

Route::get('/cobadong', function()
{
    return View::make('travel.index', array('nama' =>
'Aris'));
});


Route::get('/admin', function () {
    //return view('hal_admin.index');
    Cookie::make('name', 'value',222);
    $cobaya=Cookie::get('name');
    return View::make('hal_admin.index', array('nama' =>
$cobaya));


  //jaga2 $image = base64_encode(file_get_contents(Input::file('logoservis')->getRealPath()));

});

//mendaftarkan cookie
Cookie::queue('keybaru', 'rrrrrr', 5);
Cookie::queue('keybarub', 'valuebbb', 5);
Cookie::queue('keybaruc', 'valueccc', 5);
return Redirect::to('/admin/login');

**/

Route::get('/cookie/set','BelajarCOntroller@setCookie');
Route::get('/cookie/get','BelajarCOntroller@getCookie');
 use Illuminate\Cookie\CookieJar;
 Route::get('/tesgravatar', function() {
   $paramgrav=array(
        'size'   => 80,
        'fallback' => 'identicon',
        'secure' => false,
        'maximumRating' => 'g',
        'forceDefault' => false,
        'forceExtension' => 'jpg',
    );
    $argam = array("mm", "identicon", "monsterid", "wavatar", "retro");
    $rangam = array_rand($argam, 2);
echo $argam[$rangam[0]] . "\n";
$almgam=Gravatar::get('anharbloga@yahoo.com',$paramgrav);
$almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);
   return $almgam;

 });
  Route::get('/tespdf', function() {

     $html = view('pdfs.example',array('id'=>2))->render();

   return PDF::load($html)->show();

 //  return View::make('pdfs.example');

 });

Route::get('/tescookie', function () {
    //return view('hal_admin.index');
    $cobaya=Cookie::get('keybaru');
    return View::make('hal_admin.index', array('nama' =>
$cobaya));

	//return 'hello world';
});

//   Route::post('/', 'LogInController@doLogIn');
/**
Route::post('/admin/konfirmlogin', function () {

    return View::make('hal_admin.index', array('nama' =>""));

	//return 'hello world';
});
**/
// ---------- BAGIAN FRONT ----------------

// submit komentar artikel di blog
Route::post('/submitblogcomment', 'web_front@submitblogcomment');
// halaman tes admin dan front
Route::get('/tesadmin', 'web_admin@pageTes');
// halaman error
Route::get('/hal_error', 'web_front@pageError');
// menu Penerbangan

Route::get('/onlysj', 'web_front@onlySJ');
Route::get('/onlyga', 'web_front@onlyGA');
Route::get('/selek', 'web_front@pageSelek');
Route::get('/flight', 'web_front@pageFlight');
Route::get('/flight/otomatiscari/{org}/{des}/{flight}/{tglberangkat}/{tglpulang}/{jumadt}/{jumchd}/{juminf}/{kotasal}/{kottujuan}', 'web_front@pageFlightCari');
Route::get('/flight/flight_isiforma', 'web_front@pageFlightFroma');
//Route::post('/flight/kirimdatadiri', 'web_front@pageFlightBook');
Route::get('/flight/thankyou', 'web_front@pageThankyou');
Route::post('/flight/konfirmasi/kirimbukti/{notrx}', 'web_front@kirimBuktiPembayaranAirline');
Route::get('/flight/konfirmasi/{notrx}', 'web_front@pageKonfirmPembayaranAirline');
Route::get('/flight/konfirmasi', 'web_front@pageFormTagihanAirline');
Route::post('/flight/kirimdatadiri', 'web_front@createTagihanPesawat');
Route::post('/flight/flight_isiform', 'web_front@pageFlightForm');
Route::get('/tesflight', 'web_front@pageSamping');

Route::get('/jadwalPesawat/org/{org}/des/{des}/flight/{flight}/tglberangkat/{tglberangkat}/tglpulang/{tglpulang}/jumadt/{jumadt}/jumchd/{jumchd}/juminf/{juminf}/ac/{ac}', 'web_front@jadwalPesawat');

Route::get('/ambiljadwal', 'web_front@pageJadwal');
Route::get('/ambilbandara', 'web_front@pageBandara');

// menu About
Route::get('/', 'web_front@pageHome');
Route::get('/about', 'web_front@pageAbout');
Route::get('/about/about', 'web_front@pageAbout');
Route::get('/about/galeri', 'web_front@pageGaleri');
Route::get('/about/galeri/view/{idfoto}', 'web_front@pageGaleriView');
Route::get('/about/faq', 'web_front@pageFAQ');
Route::get('/about/policies', 'web_front@pagePolicy');
Route::get('/about/blog', 'web_front@pageBlog');
Route::get('/about/blog/{thn}/{bln}/{judul}', 'web_front@pageBlogView');
Route::get('/about/blog/kategori/{namakategori}', 'web_front@pageBlogKategori');
Route::post('/about/simpanbukutamu', 'web_front@simpanBukuTamu');
Route::get('/about/kontak', 'web_front@pageKontak');
Route::get('/about/tesmaster', 'web_front@pageMaster');

Route::get('ajax',function(){
   return view('message');
});
Route::post('/getmsg','AjaxController@hasilData');


// ---------BAGIAN ADMIN-----------------

// pengaturan akun
Route::post('/admin/akun/pengaturan/save', ['uses' => 'web_admin@savePengaturanAkun','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/akun/pengaturan', ['uses' => 'web_admin@pagePengaturanAkun','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/akun/level/delete/erase', ['uses' => 'web_admin@eraseAkun','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/akun/level/delete/{id}', ['uses' => 'web_admin@deleteAkun','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/akun/level/edit/save', ['uses' => 'web_admin@saveAkun','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/akun/level/edit/{id}', ['uses' => 'web_admin@editAkun','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/akun/add', 'web_admin@addAkun');
Route::get('/admin/akun/tambah', ['uses' => 'web_admin@tambahAkun','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/akun/level/{lvl}', ['uses' => 'web_admin@allAkun','middleware' => 'tesAdminLogin:1']);

// pengaturan transaksi
Route::post('/admin/transaksi/setting/save', ['uses' => 'web_admin@savePengaturanTransaksi','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/transaksi/setting', ['uses' => 'web_admin@pagePengaturanTransaksi','middleware' => 'tesAdminLogin:2']);

Route::post('/admin/transaksi/airline/pilihkeputusan/{id}', ['uses' => 'web_admin@keputusanTagihanAirline','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/transaksi/airline/setbukti/tran/{idt}/id/{id}', ['uses' => 'web_admin@toggleBuktiTagihanAirline','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/transaksi/airline/view/{id}', ['uses' => 'web_admin@viewTagihanAirline','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/transaksi/airline/viewpdf/{id}', ['uses' => 'web_admin@viewTagihanAirline','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/transaksi/airline/tran/{status}', ['uses' => 'web_admin@transaksiAirline','middleware' => 'tesAdminLogin:2']);

//pengaturan style filter
Route::get('/admin/elemenweb/templatefilter/detail/{id}', ['uses' => 'web_admin@pageDetailTemplateFilter','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter', ['uses' => 'web_admin@pageTemplateFilter','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/template/select/{id}', ['uses' => 'web_admin@selectStyleFilterTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/template/toggle/{id}', ['uses' => 'web_admin@toggleStyleFilterTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/color/select/{id}', ['uses' => 'web_admin@selectStyleFilterColor','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/color/toggle/{id}', ['uses' => 'web_admin@toggleStyleFilterColor','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/layout/select/{id}', ['uses' => 'web_admin@selectStyleFilterLayout','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/layout/toggle/{id}', ['uses' => 'web_admin@toggleStyleFilterLayout','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/element/select/{id}', ['uses' => 'web_admin@selectStyleFilterElement','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/templatefilter/element/toggle/{id}', ['uses' => 'web_admin@toggleStyleFilterElement','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/color/select/{id}', ['uses' => 'web_admin@selectColorTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/color/toggle/{id}', ['uses' => 'web_admin@toggleColorTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/color', ['uses' => 'web_admin@pageStyleFilterColor','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/layout/select/{id}', ['uses' => 'web_admin@selectLayoutTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/layout/toggle/{id}', ['uses' => 'web_admin@toggleLayoutTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/layout', ['uses' => 'web_admin@pageStyleFilterLayout','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/element/select/{id}', ['uses' => 'web_admin@selectElementTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/element/toggle/{id}', ['uses' => 'web_admin@toggleElementTemplate','middleware' => 'tesAdminLogin:3']);
Route::get('/admin/elemenweb/stylefilter/element', ['uses' => 'web_admin@pageStyleFilterElement','middleware' => 'tesAdminLogin:3']);


//pengaturan style
Route::get('/admin/elemenweb/style/template', ['uses' => 'web_admin@pageStyleTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/template/select/{id}', ['uses' => 'web_admin@selectStyleTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/template/toggle/{id}', ['uses' => 'web_admin@toggleStyleTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/color/select/{id}', ['uses' => 'web_admin@selectColorTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/color/toggle/{id}', ['uses' => 'web_admin@toggleColorTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/color', ['uses' => 'web_admin@pageStyleColor','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/layout/select/{id}', ['uses' => 'web_admin@selectLayoutTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/layout/toggle/{id}', ['uses' => 'web_admin@toggleLayoutTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/layout', ['uses' => 'web_admin@pageStyleLayout','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/element/select/{id}', ['uses' => 'web_admin@selectElementTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/element/toggle/{id}', ['uses' => 'web_admin@toggleElementTemplate','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/style/element', ['uses' => 'web_admin@pageStyleElement','middleware' => 'tesAdminLogin:2']);

// pengaturan logo

Route::post('/admin/elemenweb/logoweb/save', ['uses' => 'web_admin@saveLogoWeb','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/logoweb', ['uses' => 'web_admin@editLogoWeb','middleware' => 'tesAdminLogin:2']);


// pengaturan buku tamu

Route::get('/admin/bukutamu/toggle/{id}', ['uses' => 'web_admin@toggleBukuTamu','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/bukutamu', ['uses' => 'web_admin@allBukuTamu','middleware' => 'tesAdminLogin:1']);

// Pengaturan blog
Route::post('/admin/blogging/erase/{idartikel}', ['uses' => 'web_admin@eraseArtikel','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/blogging/delete/{tahun}/{bulan}/{id}', ['uses' => 'web_admin@formhapusArtikel','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/blogging/save/{idartikel}', ['uses' => 'web_admin@saveArtikel','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/blogging/edit/{tahun}/{bulan}/{id}', ['uses' => 'web_admin@editArtikel','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/blogging/add', ['uses' => 'web_admin@addArtikel','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/blogging/tambah', ['uses' => 'web_admin@tambahArtikel','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/blogging/view/{tahun}/{bulan}/{id}', ['uses' => 'web_admin@viewArtikel','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/blogging/', ['uses' => 'web_admin@allArtikel','middleware' => 'tesAdminLogin:1']);


// pengaturan slider
Route::post('/admin/elemenweb/slider/erase/{idfoto}', ['uses' => 'web_admin@eraseSlider','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/slider/delete/{id}', ['uses' => 'web_admin@formhapusSlider','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/elemenweb/slider/save/{idfoto}', ['uses' => 'web_admin@saveSlider','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/slider/edit/{id}', ['uses' => 'web_admin@editSlider','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/elemenweb/slider/add', ['uses' => 'web_admin@addSlider','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/slider/tambah', ['uses' => 'web_admin@tambahSlider','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/slider/view/{id}', ['uses' => 'web_admin@viewSlider','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/slider/', ['uses' => 'web_admin@allSlider','middleware' => 'tesAdminLogin:2']);

// pengaturan galeri
Route::post('/admin/elemenweb/galeri/erase/{idgaleri}', ['uses' => 'web_admin@eraseGaleri','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/delete/{id}', ['uses' => 'web_admin@formhapusGaleri','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/elemenweb/galeri/save/{idgaleri}', ['uses' => 'web_admin@saveGaleri','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/edit/{id}', ['uses' => 'web_admin@editGaleri','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/elemenweb/galeri/add', ['uses' => 'web_admin@addGaleri','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/tambah', ['uses' => 'web_admin@tambahGaleri','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/view/{id}', ['uses' => 'web_admin@viewGaleri','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/{kategori}', ['uses' => 'web_admin@viewGaleriByKategori','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/elemenweb/galeri/', ['uses' => 'web_admin@viewGaleriByAll','middleware' => 'tesAdminLogin:2']);

// pengaturan kontak
Route::post('/admin/infoweb/kontak/save', ['uses' => 'web_admin@saveKontak','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/kontak', ['uses' => 'web_admin@pageEditKontak','middleware' => 'tesAdminLogin:2']);

// pengaturan rekening
Route::post('/admin/infoweb/rekening/erase/{idrekening}', ['uses' => 'web_admin@eraseRekening','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/rekening/delete/{id}', ['uses' => 'web_admin@deleteRekening','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/rekening/save/{idrekening}', ['uses' => 'web_admin@saveRekening','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/rekening/edit/{id}', ['uses' => 'web_admin@editRekening','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/rekening/add', ['uses' => 'web_admin@addRekening','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/rekening/tambah', ['uses' => 'web_admin@tambahRekening','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/rekening', ['uses' => 'web_admin@allRekening','middleware' => 'tesAdminLogin:2']);

// pengaturan policies
Route::post('/admin/infoweb/policies/erase/{idpolicy}', ['uses' => 'web_admin@erasePolicy','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/policies/delete/{id}', ['uses' => 'web_admin@formhapusPolicy','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/policies/save/{idpolicy}', ['uses' => 'web_admin@savePolicy','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/policies/edit/{id}', ['uses' => 'web_admin@editPolicy','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/policies/add', ['uses' => 'web_admin@addPolicy','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/policies/tambah', ['uses' => 'web_admin@tambahPolicy','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/policies/view/{id}', ['uses' => 'web_admin@viewPolicy','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/policies', ['uses' => 'web_admin@listPolicy','middleware' => 'tesAdminLogin:2']);


// pengaturan faq
Route::post('/admin/infoweb/faq/erase/{idfaq}', ['uses' => 'web_admin@eraseFAQ','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/faq/delete/{id}', ['uses' => 'web_admin@formhapusFAQ','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/faq/save/{idfaq}', ['uses' => 'web_admin@saveFAQ','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/faq/edit/{id}', ['uses' => 'web_admin@editFAQ','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/faq/add', ['uses' => 'web_admin@addFAQ','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/faq/tambah', ['uses' => 'web_admin@tambahFAQ','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/faq/view/{id}', ['uses' => 'web_admin@viewFAQ','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/faq', ['uses' => 'web_admin@listFAQ','middleware' => 'tesAdminLogin:2']);

// pengaturan team
Route::post('/admin/infoweb/team/erase/{idteam}', ['uses' => 'web_admin@eraseTeam','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/team/delete/{id}', ['uses' => 'web_admin@formhapusTeam','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/team/save/{idteam}', ['uses' => 'web_admin@saveTeam','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/team/edit/{id}', ['uses' => 'web_admin@editTeam','middleware' => 'tesAdminLogin:2']);
Route::post('/admin/infoweb/team/add', ['uses' => 'web_admin@addTeam','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/team/tambah', ['uses' => 'web_admin@tambahTeam','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/team/view/{id}', ['uses' => 'web_admin@viewTeam','middleware' => 'tesAdminLogin:2']);
Route::get('/admin/infoweb/team', ['uses' => 'web_admin@listTeam','middleware' => 'tesAdminLogin:2']);

// pengaturan testimonial
Route::post('/admin/infoweb/testimonials/erase/{idtestimonial}', ['uses' => 'web_admin@eraseTestimonial','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/testimonials/delete/{id}', ['uses' => 'web_admin@formhapusTestimonial','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/infoweb/testimonials/save/{idtestimonial}', ['uses' => 'web_admin@saveTestimonial','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/testimonials/edit/{id}', ['uses' => 'web_admin@editTestimonial','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/infoweb/testimonials/add', ['uses' => 'web_admin@addTestimonial','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/testimonials/tambah', ['uses' => 'web_admin@tambahTestimonial','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/testimonials/view/{id}', ['uses' => 'web_admin@viewTestimonial','middleware' => 'tesAdminLogin:1']);

// pengaturan about
Route::post('/admin/infoweb/about/save', ['uses' => 'web_admin@saveAbout','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/about', ['uses' => 'web_admin@pageEditAbout','middleware' => 'tesAdminLogin:1']);

// pengaturan servis
Route::post('/admin/infoweb/services/erase/{idservis}', ['uses' => 'web_admin@condeleteService','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/services/delete/{id}', ['uses' => 'web_admin@deleteService','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/infoweb/services/save/{idservis}', ['uses' => 'web_admin@saveService','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/services/edit/{id}', ['uses' => 'web_admin@editService','middleware' => 'tesAdminLogin:1']);
Route::post('/admin/infoweb/services/add', 'web_admin@addService');
Route::get('/admin/infoweb/services/tambah', ['uses' => 'web_admin@tambahService','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/services/view/{id}', ['uses' => 'web_admin@viewService','middleware' => 'tesAdminLogin:1']);
Route::get('/admin/infoweb/services', ['uses' => 'web_admin@listService','middleware' => 'tesAdminLogin:1']);

Route::get('/admin/logout', function () {
    //return view('hal_admin.index');
    \Cookie::queue(\Cookie::forget('keybaru'));
    \Cookie::queue(\Cookie::forget('user_id'));
    \Cookie::queue(\Cookie::forget('user_pangkat'));
    \Cookie::queue(\Cookie::forget('user_username'));
    \Cookie::queue(\Cookie::forget('user_mmid'));
    \Cookie::queue(\Cookie::forget('infomember'));
      return redirect('admin');
	//return 'hello world';
});

Route::get('/cobadung', 'web_front@pageHome');
//Route::post('/admin/konfirmlogin', 'LogInController@doLogIn');
Route::post('/admin/konfirmlogin', 'web_front@doLogIn');

Route::get('/admin/login', 'web_front@pageLogin');
/**
Route::get('/admin/login', function () {
  $domain_base=url('\\');
  $domain_base=str_replace("\\","",$domain_base);
  $tb_infomember=DB::table('tb_infomember')->where('domain',$domain_base)->first();

    return View::make('hal_admin.login', array('settingan_member' =>$tb_infomember));

	//return 'hello world';
});
**/


//Route::get('/admin', ['middleware' =>'web_admin@pageAdmin']);
Route::get('admin', ['uses' => 'web_admin@pageAdmin','middleware' =>'tesAdminLogin:1']);

Route::get('/admin/penulisan', function () {

    $cobaya=Cookie::get('keybaru');
    if(empty($cobaya)){
      return redirect('admin/login');
    }else{
    return View::make('hal_admin.index', array('nama' =>
$cobaya));
    }

});

Route::get('/siswalist', 'BelajarCOntroller@index');

Route::get('/db', function(){

    //$isian=DB::table('akun_admin')->get();
    $isian=DB::table('akun_admin')->first();
    return $isian->username;
});
