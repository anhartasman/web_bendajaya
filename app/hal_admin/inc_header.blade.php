 <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"> <img src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png" width="200px" height="40px" alt="User Image"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>

                <?php

                  if(isset($rubahseen) && isset($notifairlineFromView)){
                  if($rubahseen==1){
                    $notifairline=$notifairlineFromView;
                  }
                  }

              $jumnotif=0;
              $jumnotif+=count($notifairline); ?>
              @if($jumnotif>0)
              <span class="label label-warning">{{$jumnotif}}</span>
              @endif
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{$jumnotif}} notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                    @foreach($notifairline as $notif)

                  <li>
                    <a href="{{ url('/') }}/admin/transaksi/airline/view/{{$notif->id}}">
                      @if($notif->status==0)
                      <i class="fa fa-warning text-yellow"></i>
                      Transaksi baru {{$notif->notrx}}
                      @elseif($notif->status==1)
                      <i class="fa fa-warning text-yellow"></i>
                      Transaksi belum dikonfirm {{$notif->notrx}}
                      @endif
                    </a>
                  </li>

                  @endforeach
                </ul>
              </li>

            </ul>
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png" class="user-image" alt="User Image">
              <span class="hidden-xs">{{getUserInfo("user_nama")}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png" class="img-circle" alt="User Image">
                <p>
                  {{getUserInfo("user_nama")}} - {{getUserInfo("user_username")}}

                </p>
              </li>
              <!-- Menu Body
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                -->
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('/') }}/admin/akun/pengaturan" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/') }}/admin/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
