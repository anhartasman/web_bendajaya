
<section class="sidebar">
 <!-- Sidebar user panel -->
 <div class="user-panel">
   <div class="pull-left image">
     <img src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png" class="img-circle" alt="User Image">
   </div>
   <div class="pull-left info">
     <p>{{getUserInfo("user_nama")}}</p>
     <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
   </div>
 </div>
 <!-- search form -->

 <!-- /.search form -->
 <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu">
   <li class="header">MAIN NAVIGATION</li>
   <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>



   <li class="{{ Request::segment(2) === 'infoweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Informasi Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">

       <li><a href="{{ url('/') }}/admin/infoweb/about"><i class="fa fa-circle-o"></i> About</a></li>
       <li class="{{ Request::segment(3) === 'services' ? 'active' : null }}">
         <a href="#"><i class="fa fa-circle-o"></i> Services
           <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
           </span>
         </a>
         <ul class="treeview-menu">

<?php

foreach ($services as $servis) {
    $segment5 = Request::segment(5);

?>
           <li class="<?php if($servis->id==$segment5){print("active");} ?>">
             <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $servis->namaservis;?>
               <span class="pull-right-container">
                 <i class="fa fa-angle-left pull-right"></i>
               </span>
             </a>
             <ul class="treeview-menu">
               <li><a href="{{ url('/') }}/admin/infoweb/services/view/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
               <li><a href="{{ url('/') }}/admin/infoweb/services/edit/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
               <li><a href="{{ url('/') }}/admin/infoweb/services/delete/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
             </ul>
           </li>
<?php
}
 ?>

   <li><a href="{{ url('/') }}/admin/infoweb/services/tambah"><i class="fa fa-circle-o"></i> Tambah Servis</a></li>
         </ul>
       </li>

             <li class="{{ Request::segment(3) === 'testimonials' ? 'active' : null }}">
               <a href="#"><i class="fa fa-circle-o"></i> Testimonials
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">

       <?php


       foreach ($testimonials as $testimonial) {
          $segment5 = Request::segment(5);

       ?>
                 <li class="<?php if($testimonial->id==$segment5){print("active");} ?>">
                   <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $testimonial->sumbertesti;?>
                     <span class="pull-right-container">
                       <i class="fa fa-angle-left pull-right"></i>
                     </span>
                   </a>
                   <ul class="treeview-menu">
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/view/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/edit/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/delete/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                   </ul>
                 </li>
       <?php
       }
       ?>

         <li><a href="{{ url('/') }}/admin/infoweb/testimonials/tambah"><i class="fa fa-circle-o"></i> Tambah Testimonial</a></li>
               </ul>
             </li>


                          <li class="{{ Request::segment(3) === 'team' ? 'active' : null }}">
                            <a href="#"><i class="fa fa-circle-o"></i> Team
                              <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                            <ul class="treeview-menu">

                    <?php

                    foreach ($team as $anggota) {
                       $segment5 = Request::segment(5);

                    ?>
                              <li class="<?php if($anggota->id==$segment5){print("active");} ?>">
                                <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $anggota->namaanggota;?>
                                  <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                  </span>
                                </a>
                                <ul class="treeview-menu">
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/view/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/edit/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/delete/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                </ul>
                              </li>
                    <?php
                    }
                    ?>

                      <li><a href="{{ url('/') }}/admin/infoweb/team/tambah"><i class="fa fa-circle-o"></i> Tambah Team</a></li>
                            </ul>
                          </li>


                                 <li class="{{ Request::segment(3) === 'faq' ? 'active' : null }}">
                                   <a href="#"><i class="fa fa-circle-o"></i> FAQ
                                     <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                     </span>
                                   </a>
                                   <ul class="treeview-menu">

                          <?php

                          foreach ($faqs as $faq) {
                              $segment5 = Request::segment(5);

                          ?>
                                     <li class="<?php if($faq->id==$segment5){print("active");} ?>">
                                       <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $faq->pertanyaan;?>
                                         <span class="pull-right-container">
                                           <i class="fa fa-angle-left pull-right"></i>
                                         </span>
                                       </a>
                                       <ul class="treeview-menu">
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/view/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/edit/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/delete/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                       </ul>
                                     </li>
                          <?php
                          }
                           ?>

                             <li><a href="{{ url('/') }}/admin/infoweb/faq/tambah"><i class="fa fa-circle-o"></i> Tambah FAQ</a></li>
                                   </ul>
                                 </li>



                                        <li class="{{ Request::segment(3) === 'policies' ? 'active' : null }}">
                                          <a href="#"><i class="fa fa-circle-o"></i> Policies
                                            <span class="pull-right-container">
                                              <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                          </a>
                                          <ul class="treeview-menu">

                                 <?php

                                 foreach ($policies as $policy) {
                                     $segment5 = Request::segment(5);

                                 ?>
                                            <li class="<?php if($policy->id==$segment5){print("active");} ?>">
                                              <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $policy->judul;?>
                                                <span class="pull-right-container">
                                                  <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                              </a>
                                              <ul class="treeview-menu">
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/view/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/edit/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/delete/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                              </ul>
                                            </li>
                                 <?php
                                 }
                                  ?>

                                    <li><a href="{{ url('/') }}/admin/infoweb/policies/tambah"><i class="fa fa-circle-o"></i> Tambah Policy</a></li>
                                          </ul>
                                        </li>




                <li><a href="{{ url('/') }}/admin/infoweb/kontak"><i class="fa fa-circle-o"></i> Kontak</a></li>



                <li class="{{ Request::segment(3) === 'rekening' ? 'active' : null }}">
                  <a href="#"><i class="fa fa-circle-o"></i> Rekening
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                          </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}/admin/infoweb/rekening/tambah"><i class="fa fa-bank"></i> <span>Tambah Rekening</span></a></li>
                    <li><a href="{{ url('/') }}/admin/infoweb/rekening"><i class="fa fa-bank"></i> <span>Pengaturan</span></a></li>


                        </ul>
                </li>


     </ul>
   </li>


   <li class="{{ Request::segment(2) === 'elemenweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Elemen Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">
      <li><a href="{{ url('/') }}/admin/elemenweb/logoweb"><i class="fa fa-circle-o"></i>Logo Web</a></li>

        @if(getUserInfo("user_pangkat")==3)
        <li><a href="{{ url('/') }}/admin/elemenweb/templatefilter"><i class="fa fa-circle-o"></i>Template Filter</a></li>

        @endif

                <li class="{{ Request::segment(3) === 'style' ? 'active' : null }}">
                  <a href="#"><i class="fa fa-circle-o"></i> Tampilan
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}/admin/elemenweb/style/template"><i class="fa fa-circle-o"></i>Template</a></li>
                      <li><a href="{{ url('/') }}/admin/elemenweb/style/color"><i class="fa fa-circle-o"></i>Color</a></li>
                        <li><a href="{{ url('/') }}/admin/elemenweb/style/layout"><i class="fa fa-circle-o"></i>Layout</a></li>
                          <li><a href="{{ url('/') }}/admin/elemenweb/style/element"><i class="fa fa-circle-o"></i>Element</a></li>

                  </ul>
                </li>
       <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
         <a href="#"><i class="fa fa-picture-o"></i> Galeri
           <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
           </span>
         </a>
         <ul class="treeview-menu">

                        <li><a href="{{ url('/') }}/admin/elemenweb/galeri/tambah"><i class="fa fa-circle-o"></i>Upload Foto</a></li>

                               <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
                                 <a href="#"><i class="fa fa-circle-o"></i> Album
                                   <span class="pull-right-container">
                                     <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                                 </a>
                                 <ul class="treeview-menu">

                        <?php

                        foreach ($kategorisgaleri as $kategori) {
                            $segment5 = Request::segment(5);

                        ?>
                                                   <li><a href="{{ url('/') }}/admin/elemenweb/galeri/<?php  echo $kategori->id;?>"><i class="fa fa-circle-o"></i><?php  echo $kategori->category;?></a></li>
                        <?php
                        }
                         ?> <li><a href="{{ url('/') }}/admin/elemenweb/galeri"><i class="fa fa-circle-o"></i>All</a></li>
        </ul>
                               </li>
   </ul>
       </li>

         <li class="{{ Request::segment(3) === 'slider' ? 'active' : null }}">
           <a href="#"><i class="fa fa-picture-o"></i> Slider Banner
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span>
           </a>
           <ul class="treeview-menu">

                          <li><a href="{{ url('/') }}/admin/elemenweb/slider/tambah"><i class="fa fa-circle-o"></i>Upload Banner</a></li>
                          <li><a href="{{ url('/') }}/admin/elemenweb/slider"><i class="fa fa-circle-o"></i>Album</a></li>
     </ul>
         </li>



     </ul>
   </li>



      <li class="{{ Request::segment(2) === 'blogging' ? 'active' : null }}  treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Penulisan Blog</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">

          <li><a href="{{ url('/') }}/admin/blogging/tambah"><i class="fa fa-circle-o"></i>Tulis Baru</a></li>


   <?php


//->select(array('judul','id'))
//$services = DB::table('tb_webcont_blog')->groupBy(DB::raw('YEAR(tanggal)'))->where( DB::raw('MONTH(tanggal)'), '=', date('n') )->get();
//$services = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('YEAR(tanggal) as tahun')])->groupBy(DB::raw('YEAR(tanggal)'))->orderBy('tanggal','desc')->get();
$services = DB::table('tb_webcont_blog')->select([DB::raw('YEAR(tanggal) as tahun'),DB::raw('MONTH(tanggal) as bulan')])->groupBy(DB::raw('YEAR(tanggal)'))->orderBy('tanggal','desc')->get();

    $segment5 = Request::segment(5);
    $segment4 = Request::segment(4);
   //$services= DB::table('tb_webcont_services')->get();
   foreach ($services as $servis) {

   ?>

             <li class="<?php if($servis->tahun==$segment4){print("active");} ?>">
               <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $servis->tahun;?>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <?php
                 //$servicesmonths->where()->get()
                 $servicesmonths = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('MONTH(tanggal) as bulan')]);
                $servicehasilget=$servicesmonths->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->groupBy(DB::raw('MONTH(tanggal)'))->get();
                    //$services= DB::table('tb_webcont_services')->get();
                    $bulansebelumnya=0;
                    $awal=0;
                    foreach ($servicehasilget as $servicesmonth) {
                      $monthNum  =$servicesmonth->bulan;
                      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                      $monthName = $dateObj->format('F'); // March
                      if($monthNum!=$bulansebelumnya){
                       ?>
              <li class="<?php if($servicesmonth->bulan==$segment5){print("active");} ?>">
                <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $monthName." ".$servicesmonth->bulan;?>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <?php
                  $servicemonthlget = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('MONTH(tanggal) as bulan')])->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->where(DB::raw('MONTH(tanggal)'), '=',$servicesmonth->bulan)->orderBy('tanggal','desc')->get();

                //  $servicemonthlget=$servicesmonths->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->where(DB::raw('MONTH(tanggal)'), '=',$servicesmonth->bulan)->orderBy('tanggal','desc')->get();

                  foreach ($servicemonthlget as $namar) {
                   ?>
                   <li><a href="{{ url('/') }}/admin/blogging/view/<?php  echo $servis->tahun;?>/<?php  echo $namar->bulan;?>/<?php  echo $namar->id;?>"><i class="fa fa-circle-o"></i> <?php  echo $namar->judul;?></a></li>

                     <?php
                      }
                      ?>

                 <?php
                 }
                 ?>
                 <?php
                 if($monthNum!=$bulansebelumnya){
                  ?>
               </ul>
             </li>

                              <?php
                              $bulansebelumnya =$monthNum;
                              }
                              ?>
                 <?php

               }
                  ?>

                          </ul>
                        </li>

   <?php
   }
    ?>



    </ul>
    </li>
    <li class="{{ Request::segment(2) === 'bukutamu' ? 'active' : null }}"><a href="{{ url('/') }}/admin/bukutamu"><i class="fa fa-book"></i> <span>Buku Tamu</span></a></li>

    <li class="{{ Request::segment(2) === 'transaksi' ? 'active' : null }}  treeview">
      <a href="#">
        <i class="fa fa-reorder"></i> <span>Transaksi</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">

        <li class="{{ Request::segment(3) === 'airline' ? 'active' : null }}">
          <a href="#"><i class="fa fa-plane"></i> Airline
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/0"><i class="fa fa-circle-o"></i>Belum dibayar</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/1"><i class="fa fa-circle-o"></i>Menunggu konfirmasi</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/2"><i class="fa fa-circle-o"></i>Diterima</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/3"><i class="fa fa-circle-o"></i>Diterima dengan syarat</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/4"><i class="fa fa-circle-o"></i>Ditolak</a></li>

    </ul>
        </li>


      </ul>
    </li>
    <li class="{{ Request::segment(2) === 'akun' ? 'active' : null }}  treeview">
      <a href="#">
        <i class="fa fa-user"></i> <span>Akun</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">

        <li><a href="{{ url('/') }}/admin/akun/pengaturan"><i class="fa fa-circle-o"></i>Pengaturan</a></li>

        <li class="{{ Request::segment(3) === 'level' ? 'active' : null }}">
          <a href="#"><i class="fa fa-user"></i> Tabel Akun
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            @if(getUserInfo("user_pangkat")==3)
            <li><a href="{{ url('/') }}/admin/akun/level/3"><i class="fa fa-circle-o"></i>Super Admin</a></li>
            @endif
            @if(getUserInfo("user_pangkat")>1)
            <li><a href="{{ url('/') }}/admin/akun/level/2"><i class="fa fa-circle-o"></i>Admin</a></li>
            @endif
            <li><a href="{{ url('/') }}/admin/akun/level/1"><i class="fa fa-circle-o"></i>Operator</a></li>
         </ul>
        </li>
        <li><a href="{{ url('/') }}/admin/akun/tambah"><i class="fa fa-circle-o"></i>Tambah</a></li>



      </ul>
    </li>

 </ul>
</section>
