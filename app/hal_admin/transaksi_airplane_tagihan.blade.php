<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/flat/blue.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') }}"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <!-- bagian header -->
@include('hal_admin.inc_header');
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
     <!-- bagian sidebar-->
    @include('hal_admin.inc_sidebar');
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>{{$notrx}} </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->

    <section class="invoice">
      <!-- title row -->
      @if($errors->has())
         @foreach ($errors->all() as $error)
            <h1>{{ $error }}</h1>
        @endforeach
      @endif
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> AdminLTE, Inc.
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Departure
          <address>
            <strong>{{$org}} - {{$des}}</strong><br>
            <strong>Kode Booking : {{$PNRDep}}</strong><br>
            <?php
            $dafdep = json_decode($daftranpergi, true);
            ?>

 					 @foreach($dafdep['jalurpergi'] as $trans)
             {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
             @endforeach

          </address>
        </div>
        <!-- /.col -->
        @if($flight=="R")
        <div class="col-sm-4 invoice-col">
          Return
          <address>
            <strong>{{$des}} - {{$org}} ({{$PNRRet}})</strong><br>
            <strong>Kode Booking : {{$PNRRet}}</strong><br>
            <?php
            $dafret = json_decode($daftranpulang, true);
            ?>

 					 @foreach($dafret['jalurpulang'] as $trans)
             {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
             @endforeach
          </address>
        </div>
        @endif
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #{{$notrx}}</b><br>
          <b>Order ID:</b> {{$id}}<br>
          <b>Payment Due:</b> {{$tgl_bill_exp}}<br>
          <b>Nama:</b> {{$cpname}}<br>
          <b>Telepon:</b> {{$cptlp}}<br>
          <b>Email:</b> {{$cpmail}}<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Tabel penumpang dewasa-->
      <?php $nomadt=0; ?>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Adt</th>
              <th>Nama</th>
              <th>Handphone</th>
            </tr>
            </thead>
            <tbody>
                @foreach($dafpendewasa as $pen)
                <?php $nomadt+=1; ?>
              <tr>
                <td>{{$nomadt}}</td>
                <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                <td>{{$pen->hp}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>

@if($jumchd>0)
            <!-- Tabel penumpang anak-->
            <?php $nomchd=0; ?>
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Chd</th>
                    <th>Nama</th>
                    <th>Lahir</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($dafpenanak as $pen)
                      <?php $nomchd+=1; ?>
                    <tr>
                      <td>{{$nomchd}}</td>
                      <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                      <td>{{$pen->birth}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
@endif

@if($juminf>0)
            <!-- Tabel penumpang bayi-->
            <?php $nominf=0; ?>
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Inf</th>
                    <th>Nama</th>
                    <th>Lahir</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($dafpenbayi as $pen)
                      <?php $nominf+=1; ?>
                    <tr>
                      <td>{{$nominf}}</td>
                      <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                      <td>{{$pen->birth}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
@endif
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Bukti Pembayaran</p>

          @if(count($dafbuk)>0)
          <?php $nobuk=0; ?>
         @foreach($dafbuk as $bukti)
         <?php $nobuk+=1;?>
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           Tanggal : {{$bukti->tanggalbayar}} <br>
           Jumlah : {{rupiah($bukti->jumlahbayar)}} <br>
           Rekening Asal : {{$bukti->rekasal_bank}} - {{$bukti->rekasal_norek}} A/N {{$bukti->rekasal_napem}} <br>
           Rekening Tujuan : {{$bukti->rektuju_bank}} - {{$bukti->rektuju_norek}} A/N {{$bukti->rektuju_napem}} <br>

           <img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="" style="width:200px;height:200px;">
           <br>
           @if($status==1 || $status==3)
           Terima? <input type="checkbox" name="pilihan" onclick="ceklist({{$bukti->id}})" value="{{$bukti->id}}" <?php  if($bukti->status==1){print("checked");}?>></td>
           @elseif($bukti->status==1)
           Sah
           @endif
          </p>

          @endforeach

          @if($status==1 || $status==3)
          <form role="form" method="post" action="../pilihkeputusan/{{$id}}"  enctype = "multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
            Proses <select name="keputusan">
            <option value="4">Tolak</option>
            <option value="2">Terima</option>
            <option value="3">Terima dengan syarat</option>
            </select>
            </div>
                  <div class="form-group">
                  <img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">
                  <input type="text" id="captcha" name="captcha">
                  </div>
          <div class="form-group">
            Catatan<br>
            <textarea name="catatan" rows="4" cols="50">{{$catatan}}</textarea>
          </div>
        <div class="form-group ">
          <button type="submit"style="width:50%;" class="btn btn-block btn-primary">Proses</button>
        </div>
      </form>
        @endif
                              @endif


        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Batas Pembayaran : {{$tgl_bill_exp}}</p>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Biaya:</th>
                <td>{{$biayanormal}}</td>
              </tr>
              <tr>
                <th>Kode Unik</th>
                <td>{{$kodeunik}}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>{{$biaya}}</td>
              </tr>
            </table>
          </div>
@if($status==2)
            <p class="lead">Tanggal Terima : {{$tgl_bill_acc}}</p>

@endif
        </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- bagian footer -->
 @include('hal_admin.inc_footer');

  <!-- Control Sidebar -->

   @include('hal_admin.inc_control_sidebar');
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{ URL::asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ URL::asset('plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ URL::asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ URL::asset('plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ URL::asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ URL::asset('plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ URL::asset('dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset('dist/js/demo.js') }}"></script>
<script>
  var ajaxku;
  function ceklist(idnya){
    ajaxku = buatajax();
    var url="{{ url('/') }}/admin/transaksi/airline/setbukti/tran/{{$id}}/id/"+idnya;
    //url=url+"?q="+nip;
    //url=url+"&sid="+Math.random();
    ajaxku.onreadystatechange=stateChanged;
    ajaxku.open("GET",url,true);
    ajaxku.send(null);
  }
  function buatajax(){
    if (window.XMLHttpRequest){
      return new XMLHttpRequest();
    }
    if (window.ActiveXObject){
       return new ActiveXObject("Microsoft.XMLHTTP");
     }
     return null;
   }
   function stateChanged(){
     var data;
      if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>0){
          //document.getElementById("hasilkirim").html = data;

         }else{
          // document.getElementById("hasilkirim").html = "";
                //   $('#hasilkirim').html("");
         }
       }
  }
</script>
</body>
</html>
