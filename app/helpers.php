<?php
// My common functions
function func_get_topup($idt){
 $tb_topup=DB::table('tb_topup')
 ->where('id', $idt)
 ->first();
 $tb_bank=DB::table('tb_bank')
 ->where('id', $tb_topup->bank_id)
 ->first();

$detailtopup=array(
           'id' => $tb_topup->id
          ,'bank_name' => $tb_bank->bank_name
          ,'account_name' => $tb_bank->account_name
          ,'account_number' => $tb_bank->account_number
          ,'topup_amount' => $tb_topup->topup_amount
          ,'create_at' => $tb_topup->create_at
          ,'last_update' => $tb_topup->last_update
          ,'buktitransfer' => $tb_topup->buktitransfer
          ,'status' => $tb_topup->status
          ,'user_comment' => $tb_topup->user_comment
          ,'admin_comment' => $tb_topup->admin_comment
          ,'expired_date' => $tb_topup->expired_date
    );

    return $detailtopup;
}
function func_get_product($idp){

  $tabelproduk=DB::table('tb_product')
  ->where('id',$idp)
  ->first();

  $daftarkategori = array();

    $tbljemproduk=DB::table('tb_jem_product_cat')
    ->where('idp',$tabelproduk->id)
    ->get();

    foreach($tbljemproduk as $jemproduk) {
      $tabelkategori=DB::table('tb_product_cat')
      ->where('id',$jemproduk->idpc)
      ->first();
      $daftarkategori[]=$tabelkategori;
    }
    $tabelproduk->categories=$daftarkategori;
/**
    $taxnum=func_get_system("tax");
    $hargaawal=$tabelproduk->price;
    $pajaknya=($hargaawal*$taxnum)/100;
    $hargabaru=$hargaawal+$pajaknya;
    $tabelproduk->price=$hargabaru;
    **/
    return $tabelproduk;
}
function func_get_order_by_idtrx($idtrx){

  $tb_order=DB::table('tb_jem_transaksi_produk')
  ->where('idtrx',$idtrx)
  ->get();

  for($i=0; $i<count($tb_order); $i++){
      $tb_order[$i]->product_detail=func_get_product($tb_order[$i]->product_id);
  }
    return $tb_order;
}
function func_get_profil($id){

  $tb_user=DB::table('tb_user')
  ->select(['id'
            ,'level'
            ,'name'
            ,'email'
            ,'phone'
            ,'filefoto'
            ,'username'
            ,'status'
            ,'address'
            ])
  ->where('id',$id)
  ->first();

    $daftarproduk = array();
    $usercategories=array();
    $tblproduk=DB::table('tb_product')
    ->where('ids',$id)
    ->get();

    foreach($tblproduk as $tbp) {
      $daftarkategori = array();

        $tbljemproduk=DB::table('tb_jem_product_cat')
        ->where('idp',$tbp->id)
        ->get();

        foreach($tbljemproduk as $jemproduk) {
          $tabelkategori=DB::table('tb_product_cat')
          ->where('id',$jemproduk->idpc)
          ->first();
          $daftarkategori[]=$tabelkategori;

          if (! in_array($tabelkategori->category, $usercategories)){
            $usercategories[]=$tabelkategori->category;
          }

        }
        $tbp->categories=$daftarkategori;

      $daftarproduk[]=$tbp;
    }
    $tb_user->products=$daftarproduk;
    $tb_user->categories=$usercategories;

    return $tb_user;
}

function func_check_ongkir($sessid,$destination,$tsid){
    return func_get_system("ongkir");
}

function func_get_system($cari){
  $tblconfig=DB::table('tb_system_config')
  ->get();
  $arcari=array();
  foreach($tblconfig as $tblcon) {
        $arcari[$tblcon->label]=$tblcon->value;
  }
  return $arcari[$cari];
}
function listkomabidanghukumlawyer($arid){
  $tb_jem_lawyer=DB::table('tb_jem_lawyer')
  ->where('idl', $arid)
  ->get();
  $dafbid="";
  foreach($tb_jem_lawyer as $jem){

      $tb_jenishukum=DB::table('tb_jenishukum')
      ->where('id', $jem->idj)
      ->first();
      $dafbid.=",".$tb_jenishukum->jenis;
  }

  return $dafbid;
}
function filePath($filePath)
{
$fileParts = pathinfo($filePath);

if(!isset($fileParts['filename']))
{$fileParts['filename'] = substr($fileParts['basename'], 0, strrpos($fileParts['basename'], '.'));}

return $fileParts;
}
function mintagambar($gbr,$sizew,$sizeh){
    $path = "https://i.stack.imgur.com/koFpQ.png";
  $urlgambar=$gbr;
  //return "url gambar ".$urlgambar;
  $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,$urlgambar);
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Vivo app');
    $query = curl_exec($curl_handle);
    curl_close($curl_handle);

    $filePath = filePath($urlgambar);
  //return $filePath['dirname'].'/'.$filePath['basename'];
  $image = Image::make("$urlgambar")->resize($sizew, $sizeh);

       //echo "<img src=\"".$image->encode('data-url')."\">";
       $isigambar=$image->encode();
        return response($isigambar)->header('Content-Type',"image/jpeg");

}
function mintagambarhttp($gbr,$sizew,$sizeh){
    $path = "https://i.stack.imgur.com/koFpQ.png";
  $urlgambar="http://".$gbr;
  //return "url gambar ".$urlgambar;
  $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL,$urlgambar);
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Vivo app');
    $query = curl_exec($curl_handle);
    curl_close($curl_handle);

    $filePath = filePath($urlgambar);
  //return $filePath['dirname'].'/'.$filePath['basename'];
  $image = Image::make("$urlgambar")->resize($sizew, $sizeh);

       //echo "<img src=\"".$image->encode('data-url')."\">";
       $isigambar=$image->encode();
        return response($isigambar)->header('Content-Type',"image/jpeg");

}
function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
    "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x <12) {
        $temp = " ". $angka[$x];
    } else if ($x <20) {
        $temp = kekata($x - 10). " belas";
    } else if ($x <100) {
        $temp = kekata($x/10)." puluh". kekata($x % 10);
    } else if ($x <200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x <1000) {
        $temp = kekata($x/100) . " ratus" . kekata($x % 100);
    } else if ($x <2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x <1000000) {
        $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
    } else if ($x <1000000000) {
        $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
    } else if ($x <1000000000000) {
        $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
    } else if ($x <1000000000000000) {
        $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
    }
        return $temp;
}


function terbilang($x, $style=4) {
    if($x<0) {
        $hasil = "minus ". trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}

function daerahhotel_detailByCode($kode){
  $tb_diskon=DB::table('tb_daerah')
  ->where('code', $kode)
  ->first();
  $arnya["city"]=$tb_diskon->city;
  $arnya["country"]=$tb_diskon->country;

  return ($arnya);
}
function station_detailByCode($kode){
  $tb_diskon=DB::table('tb_station')
  ->where('st_code', $kode)
  ->first();
  $arnya["st_city"]=$tb_diskon->st_city;
  $arnya["st_name"]=$tb_diskon->st_name;

  return ($arnya);
}
function airport_namakota($kode){
  $tb_diskon=DB::table('tb_airport')
  ->where('kode', $kode)
  ->first();
  return ($tb_diskon->nama);
}
function hasildiskon($mmid,$produk,$jumlah){
  $tb_diskon=DB::table('tb_diskon')
  ->where('mmid', $mmid)
  ->first();
  return ($jumlah-$tb_diskon->$produk);
}
function helpblog_jumlahkomen($arid){
  $tb_komen=DB::table('tb_webcont_blog_comment')
  ->where('article_id', $arid)
  ->get();
  return count($tb_komen);
}
function helpblog_tag($arid){
  $tb_map=DB::table('tb_blogcategory_map')
  ->where('article_id', $arid)
  ->join('tb_blogcategory', 'tb_blogcategory_map.category_id', '=', 'tb_blogcategory.id')
  ->get();
  return $tb_map;
}
function helpgaleri_tag($arid){
  $tb_map=DB::table('tb_galericategory_map')
  ->where('image_id', $arid)
  ->join('tb_galericategory', 'tb_galericategory_map.category_id', '=', 'tb_galericategory.id')
  ->get();
  return $tb_map;
}
function maskapai($ac){
  $maskapai["QZ"]="AirAsia";
  $maskapai["QG"]="Citilink";
  $maskapai["GA"]="Garuda Indonesia";
  $maskapai["KD"]="Kalstar Aviation";
  $maskapai["JT"]="Lion Air";
  $maskapai["SJ"]="Sriwijaya Air";
  $maskapai["MV"]="Trans Nusa";
  $maskapai["IL"]="Trigana Air";

  return $maskapai[$ac];
}
function lamajalan($awal,$akhir,$kalimat){
  $from_time = strtotime($awal);
    $to_time = strtotime($akhir);
  $minuteride=round(abs($to_time - $from_time) / 60,2);
  return convertToHoursMins($minuteride,$kalimat);
}
function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
function lamahari($date1,$date2){
  $startTimeStamp = strtotime($date1);
$endTimeStamp = strtotime($date2);

$timeDiff = abs($endTimeStamp - $startTimeStamp);

$numberDays = $timeDiff/86400;  // 86400 seconds in one day

// and you might want to convert to integer
$numberDays = intval($numberDays);

return $numberDays;
}
function statustravel($s){
  $ar["O"]="sekali jalan";
  $ar["R"]="pulang pergi";
  return $ar[$s];
}
function statustransaksi($nom){
  $dafstat=array("Belum dibayar"
  ,"Menunggu konfirmasi dari admin"
  ,"Diterima"
  ,"Diterima dengan syarat"
  ,"Ditolak");
  return $dafstat[$nom];
}
function namatransaksi($hrf){
  $namatran["AIR"]="Tiket Pesawat";
  $namatran["KAI"]="Tiket Kereta";
  $namatran["HTL"]="Kamar Hotel";
  return $namatran[$hrf];
}
function getUserInfo($tolong){

  $infomember=session('infomember');
  if(empty($infomember)){
   return redirect('admin/login');
  }
  return $infomember->$tolong;
}


function angkaceil($uang)
{
 $ratusan = substr($uang, -3);
 $kurang=1000-$ratusan;
 $akhir = $uang + ($kurang);
 if($kurang==1000){
   return $uang;
 }else{
   return $akhir;
 }
}
function rupiahceil($uang)
{
 $ratusan = substr($uang, -3);
 $kurang=1000-$ratusan;
 $akhir = $uang + ($kurang);
 if($kurang==1000){
   return rupiah($uang);
 }else{
   return rupiah($akhir);
 }
}
function idrole($isi){
  $role[0]="-All-";
  $role[1]="Buyer";//Customer Service
  $role[2]="Driver";//Finance
  $role[3]="Seller";//Direksi
  $role[4]="Admin Operation";

  return $role[$isi];

}
function bulanIndo($intisi){
  $BulanIndo = array("","Januari", "Februari", "Maret",
             "April", "Mei", "Juni",
             "Juli", "Agustus", "September",
             "Oktober", "November", "Desember");
  return $BulanIndo[intval($intisi)];
}
function DateToIndo($isi) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
/**
  	$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");

		$tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
		$tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring

		//$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
**/

$date=date_create($isi);
$result =date_format($date,"d-m-Y H:i");

    return($result);
}

function DateToIndoTgl($isi) {
$date=date_create($isi);
$result =date_format($date,"d-m-Y");

    return($result);
}
function somethingOrOther()
{
    return (mt_rand(1,2) == 1) ? 'something' : 'other';
}
function rupiah($isi)
{
    return "Rp ".number_format($isi,0,".",".");
}
function ambilangka($tulisan){
  preg_match_all('!\d+!',$tulisan,$hasilkonver);
  $angka=null;
  foreach($hasilkonver as $h){
    foreach($h as $ha){
      $angka.=$ha;
    }
  }
  return $angka;
}
function rupiahNoRp($isi)
{
    return number_format($isi,0,".",".");
}
function rupiahrp($isi)
{
    return "Rp. ".number_format($isi,0,".",".");
}
function sendComand($url,$json)
{
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                array( 'Content-Type: application/json','Accept: application/json')
        );
        $result = curl_exec($ch);
        return $result;
}
function html_cut($text, $max_length)
{
    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i};
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }

    while ($tags)
        $result .= "</".array_pop($tags).">";

    return $result;
}
function datalawyerById($idl){
  $tb_lawyer=DB::table('tb_lawyer')
  ->where('id', $idl)
  ->first();

  return $tb_lawyer;
}
function dataklienById($idk){
  $tb_klien=DB::table('tb_klien')
  ->where('id', $idk)
  ->first();

  return $tb_klien;
}
function mulaiWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function akhirWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}
function kirimFCM($token,$arisi){
  // API access key from Google API's Console
  /**
  define( 'API_ACCESS_KEY', 'AAAAGpZrySc:APA91bERjozucimd49BCA_2zDlE_sGaVNMBaZ71km6wvOfPqMAUJbAKcIhCzfrFm97BaxWYgWTS00JxzSjyyoWQlQz-JAW-T8e4DWF-xyFcELxgDRF3CVyxVtNxZRT6GXnfeOC24OoJBn9z5KSK6T-irOpBijqNzZA' );
  $registrationIds = array( $token );
  // prep the bundle
  $msg = $arisi;
  $fields = array
  (
  	'registration_ids' 	=> $registrationIds,
  	'data'			=> $msg
  );

  $headers = array
  (
  	'Authorization: key=' . API_ACCESS_KEY,
  	'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt( $ch,CURLOPT_POST, true );
  curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );
  echo $result;
  **/
}
?>
