<?php 

namespace App\models;
class Siswa extends Eloquent {
  protected $table = 'akun_admin';
  // jika dikosongkan tabelnya harus Siswas (pke s)
  protected $primaryKey = 'username';
  // jika tdkpake maka primary key harus dgn nama id
  public $timestamps = false;
  // biar gk selalu mengupdate kolom created_at update_at
  }  ?>
