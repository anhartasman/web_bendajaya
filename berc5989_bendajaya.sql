-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 06 Agu 2018 pada 00.31
-- Versi server: 10.1.33-MariaDB
-- Versi PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berc5989_bendajaya`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_address`
--

CREATE TABLE `tb_address` (
  `id` int(11) NOT NULL,
  `idu` int(11) NOT NULL,
  `label` text NOT NULL,
  `address` text NOT NULL,
  `aktif` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_address`
--

INSERT INTO `tb_address` (`id`, `idu`, `label`, `address`, `aktif`, `create_at`, `last_update`) VALUES
(1, 18, 'Buaran', 'Jln Buaran 3 Blok ME Nom 13', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 18, 'Bekasi', 'Bintara', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 17, 'bintara', 'cgbffg', 1, '2018-08-05 11:04:00', '2018-08-05 11:04:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  `salt` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `nama`, `username`, `password`, `salt`, `level`, `last_login`, `update_time`) VALUES
(1, 'Ujang', 'ujang', '1c87e047981901fcd2ed04f528fe62cf188dbf3525074bcea4ca0f318c78304ff6ab1a8d6463c807181948fa3b492b797b52f07bc5aa98af80333d850ab3902c', 'garammanis', 5, NULL, NULL),
(2, 'Udin', 'udin', '9237a2f7e6c70c987620ebefd1f695b5b8eb37654729ca7f8dd438b8b9e94819f561ce5af1caf1b77d195719d3d40008af364d6e30cea36d1ab7aad34c6382bf', 'garammanis827', 5, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bank`
--

CREATE TABLE `tb_bank` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `account_name` varchar(20) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_bank`
--

INSERT INTO `tb_bank` (`id`, `bank_name`, `account_name`, `account_number`, `create_at`, `last_update`) VALUES
(1, 'bca', '555736', 'anhar', '2018-08-03 13:56:15', '2018-08-03 13:56:15'),
(2, 'bca', 'anjsr', '566466', '2018-08-03 14:01:10', '2018-08-03 14:01:10'),
(3, 'bni', 'udin', '5656', '2018-08-03 14:33:27', '2018-08-03 14:33:27'),
(4, ' vgg', 'gjhh', 'hgghh', '2018-08-03 14:35:42', '2018-08-03 14:35:42'),
(5, 'nvg', 'fgghy', '4555', '2018-08-03 14:37:34', '2018-08-03 14:37:34'),
(6, 'bcv', 'ghj', '4565', '2018-08-03 14:42:03', '2018-08-03 14:42:03'),
(7, 'fhh', 'fgh', '56656', '2018-08-03 14:45:26', '2018-08-03 14:45:26'),
(8, 'hhgc', 'vhu', 'ghhhh', '2018-08-03 14:47:49', '2018-08-03 14:47:49'),
(9, 'vhh', 'fkjhh', 'dtgg', '2018-08-03 14:50:28', '2018-08-03 14:50:28'),
(10, 'chh', 'ggy', '444', '2018-08-03 14:51:53', '2018-08-03 14:51:53'),
(11, 'vvvf', 'bvhuu', 'vhjjj', '2018-08-03 14:54:07', '2018-08-03 14:54:07'),
(12, 'ghg', 'gbvc', 'vhgg', '2018-08-03 15:00:30', '2018-08-03 15:00:30'),
(13, 'vhh', 'bnvh', 'gvvg', '2018-08-03 16:36:56', '2018-08-03 16:36:56'),
(14, 'fggg', 'vvv', 'cggg', '2018-08-03 16:42:24', '2018-08-03 16:42:24'),
(15, 'fvg', 'vvgg', 'cvg', '2018-08-03 16:46:22', '2018-08-03 16:46:22'),
(16, 'bnbg', 'bfff', 'vgdhh', '2018-08-03 17:00:51', '2018-08-03 17:00:51'),
(17, 'fhgg', 'afvb', 'ggcc', '2018-08-03 17:10:16', '2018-08-03 17:10:16'),
(18, 'cbff', 'vbvf', 'cfrd', '2018-08-03 17:15:05', '2018-08-03 17:15:05'),
(19, 'fgc', 'fcc', 'c', '2018-08-03 17:20:37', '2018-08-03 17:20:37'),
(20, 'vnjhh', 'vgh', 'fnhff', '2018-08-03 17:30:47', '2018-08-03 17:30:47'),
(21, 'cggg', 'cbg', 'fgg', '2018-08-03 17:43:48', '2018-08-03 17:43:48'),
(22, 'vvxc', ' vdf', 'vvcc', '2018-08-03 17:46:27', '2018-08-03 17:46:27'),
(23, 'tcvcf', 'cdd', 'vbvvccc', '2018-08-03 17:53:41', '2018-08-03 17:53:41'),
(24, 'vlgbg', 'gbcf', 'vhfhhj', '2018-08-03 17:56:09', '2018-08-03 17:56:09'),
(25, 'cfhfrt', 'gcfv', 'ggggf', '2018-08-03 17:59:32', '2018-08-03 17:59:32'),
(26, 'gfgvdgg', 'vgdhhh', 'hnrgg', '2018-08-03 18:02:47', '2018-08-03 18:02:47'),
(27, 'bbvvnj', ' bjhff', 'v vzx', '2018-08-03 18:07:51', '2018-08-03 18:07:51'),
(28, 'vncd', ' vcvnjh', 'cbngg', '2018-08-03 18:35:34', '2018-08-03 18:35:34'),
(29, 'xbhg', 'cbjkkkn', 'gbbjj', '2018-08-03 19:46:18', '2018-08-03 19:46:18'),
(30, 'vvvhbbh', 'g bbhh', 'vvyy', '2018-08-03 19:48:06', '2018-08-03 19:48:06'),
(31, 'ffrfgg', ' cggg', 'ggfghf', '2018-08-03 19:49:32', '2018-08-03 19:49:32'),
(32, 'bhghg', 'vvgfd', 'vdggg', '2018-08-03 19:51:44', '2018-08-03 19:51:44'),
(33, 'fgvfyy', 'gguu', 'ggyuu', '2018-08-03 19:54:09', '2018-08-03 19:54:09'),
(34, 'vghhghu', 'gvvg', 'xfghvv', '2018-08-03 19:59:01', '2018-08-03 19:59:01'),
(35, 'bca', 'bcgjxh', '64654688', '2018-08-05 15:30:45', '2018-08-05 15:30:45'),
(36, 'vjhf', 'vvgg', 'vhhy', '2018-08-05 20:00:26', '2018-08-05 20:00:26'),
(37, 'fgf', 'cvff', 'fvg', '2018-08-05 20:23:30', '2018-08-05 20:23:30'),
(38, 'vg', 'vvvvz', 'vzvzvz', '2018-08-05 20:31:53', '2018-08-05 20:31:53'),
(39, 'Cimb Niaga', 'juhardi', '123456789', '2018-08-06 00:11:32', '2018-08-06 00:11:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `isi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_berita`
--

INSERT INTO `tb_berita` (`id`, `tanggal`, `judul`, `isi`) VALUES
(1, '2017-05-26 09:45:58', 'bonus referral', 'dapatkan bonus lima persen dari referral yang Anda ajak bergabung di tanya lawyer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_call_history`
--

CREATE TABLE `tb_call_history` (
  `id` int(11) NOT NULL,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `seconds` int(11) NOT NULL,
  `bill` int(11) NOT NULL,
  `rating` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_call_history`
--

INSERT INTO `tb_call_history` (`id`, `idk`, `idl`, `start`, `seconds`, `bill`, `rating`) VALUES
(1, 2, 5, '2017-02-23 19:33:54', 15, 1125, 1),
(2, 2, 5, '2017-02-24 15:01:06', 15, 1125, 1),
(3, 2, 5, '2017-02-24 15:02:49', 10, 750, 3),
(4, 2, 5, '2017-02-24 18:22:57', 10, 750, 4),
(5, 2, 4, '2017-02-25 20:41:41', 10, 1010, 2),
(6, 2, 5, '2017-03-01 15:59:15', 5, 375, 4),
(7, 2, 5, '2017-03-01 16:00:38', 15, 1125, 2),
(8, 2, 5, '2017-03-01 18:10:35', 5, 375, 1),
(9, 2, 5, '2017-03-02 06:52:45', 5, 375, 4),
(10, 2, 5, '2017-03-02 09:45:36', 5, 375, 1),
(11, 2, 5, '2017-03-02 09:51:55', 5, 375, 1),
(12, 2, 5, '2017-03-02 10:59:40', 40, 3000, 4),
(13, 2, 5, '2017-03-02 11:00:53', 10, 750, 1),
(14, 2, 5, '2017-03-02 11:02:23', 15, 1125, 1),
(15, 2, 5, '2017-03-02 11:17:57', 5, 375, 5),
(16, 2, 5, '2017-03-02 11:43:17', 10, 750, 5),
(17, 2, 5, '2017-03-02 11:48:14', 5, 375, 5),
(18, 2, 5, '2017-03-02 15:58:35', 5, 375, 4),
(19, 2, 5, '2017-03-02 16:01:49', 5, 375, 4),
(20, 2, 5, '2017-03-03 04:06:21', 10, 750, 3),
(21, 2, 5, '2017-03-03 04:07:21', 5, 375, 4),
(22, 2, 5, '2017-03-07 09:12:21', 10, 750, 2),
(23, 2, 5, '2017-03-07 09:15:40', 5, 375, 3),
(24, 2, 5, '2017-03-07 09:30:37', 10, 750, 5),
(25, 2, 5, '2017-03-10 13:58:11', 15, 1125, 1),
(26, 2, 5, '2017-03-10 14:06:36', 15, 1125, 5),
(27, 2, 5, '2017-03-14 10:01:17', 5, 375, 5),
(28, 1, 5, '2018-05-02 04:34:19', 385, 28875, 2),
(29, 2, 5, '2017-04-05 05:59:31', 0, 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_cs`
--

CREATE TABLE `tb_cs` (
  `id` int(11) NOT NULL,
  `idu` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_cs`
--

INSERT INTO `tb_cs` (`id`, `idu`) VALUES
(1, 15),
(3, 16);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_dokumentasi`
--

CREATE TABLE `tb_dokumentasi` (
  `id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `idk` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT '0',
  `masalah` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_dokumentasi`
--

INSERT INTO `tb_dokumentasi` (`id`, `tanggal`, `status`, `idk`, `idj`, `masalah`) VALUES
(1, '2017-04-27 18:07:57', 0, 1, 0, 'istri saya dan saya berkulit putih tetapi istri saya melahirkan seorang anak berkulit hitam, bagaimana cara memastikan dia tidak selingkuh dan bagaimana hukumenafkahi anak ini apabila dia merupakan hasil perselingkuhan'),
(2, '2017-04-27 18:09:47', 1, 2, 3, 'Kemarin mantan pegawai saya datang dan menagih gaji yang pernah dia tidak terima, bagaimanakah hukumnya apakah saya wajib untuk membayar gaji dia atau tidak usah?'),
(3, '2017-04-27 21:56:12', 1, 1, 4, '<p>pada suatu hari di galaksi yang jauh di luar sana seorang jedi bertemu dengan para clones yang sedang kelelahan<br></p>'),
(4, '2017-04-27 21:57:37', 0, 2, 0, '<p>Saya mendapatkan hadiah mobil dari kantor, apakah mobil ini sebaiknya langsung saya terima atau dilacak terlebih dahulu asal usulnya?<br></p>'),
(5, '2017-04-27 22:11:49', 4, 2, 2, '<p>ini seharusnya diisi dengan kendala bla bla bla hohohohoho<br></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_filepesan`
--

CREATE TABLE `tb_filepesan` (
  `id` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ukuran` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_filepesan`
--

INSERT INTO `tb_filepesan` (`id`, `idp`, `nama`, `ukuran`) VALUES
(1, 3, '3_Slip Gaji Anhar Tasman Des 16.pdf.pdf', '0'),
(2, 4, '4_manual-faq.txt.txt', '0'),
(3, 5, '5_en_story.txt.txt', '0'),
(4, 6, '6_en.txt.txt', '0'),
(5, 7, '7_en.txt.txt', '0'),
(6, 7, '7_IMG_20161002_111303.jpg.jpg', '0'),
(7, 8, '8_en.txt.txt', '0'),
(8, 8, '8_IMG_20161002_111303.jpg.jpg', '0'),
(9, 10, '10_bbm_tone.wav.wav', '0'),
(10, 11, '11_IMG_20170220_102506.jpg.jpg', '0'),
(11, 12, '12_IMG_20170220_102506.jpg.jpg', '0'),
(12, 12, '12_facebook_ringtone_pop.m4a.m4a', '0'),
(13, 13, '13_IMG-20170217-WA0005.jpeg.jpeg', '0'),
(14, 14, '14_IMG-20170217-WA0005.jpeg.jpeg', '0'),
(15, 14, '14_IMG_20170130_174820.jpg.jpg', '0'),
(16, 15, '15_IMG_20170220_043758.jpg.jpg', '0'),
(17, 15, '15_IMG_20170220_102506.jpg.jpg', '0'),
(18, 16, '16_IMG_20170220_102506.jpg.jpg', '0'),
(19, 16, '16_IMG_20170220_043758.jpg.jpg', '0'),
(20, 17, '17_IMG_20170220_102506.jpg.jpg', '0'),
(21, 18, '18_IMG_20170220_043758.jpg.jpg', '0'),
(22, 18, '18_1487590161949.jpg.jpg', '0'),
(23, 19, '19_IMG_20170215_160920_386.jpg.jpg', '0'),
(24, 19, '19_13x19cm_nulisbukutemplate.doc.doc.doc', '0'),
(25, 20, '20_Wait-for-it.mp4.mp4', '0'),
(26, 21, '21_VID_85290914_110147_282.mp4.mp4', '0'),
(27, 21, '21_Its-a-trap-to-grow-up.jpg.jpg', '0'),
(28, 23, '23_IMG-20170220-WA0011.jpeg', ''),
(29, 24, '24_IMG-20170220-WA0011.jpeg', ''),
(30, 25, '25_IMG-20170220-WA0011.jpeg', ''),
(31, 26, '26_IMG-20170220-WA0011.jpeg', ''),
(32, 27, '27_IMG-20170220-WA0011.jpeg', '736 Kb'),
(33, 28, '28_IMG-20170220-WA0011.jpeg', '736 Kb'),
(34, 28, '28_en_story.txt', '89 Kb'),
(35, 28, '28_Slip Gaji Anhar Tasman Des 16.pdf', '39 Kb'),
(36, 29, '29_Masya Allah dan Subhanallah.jpg', '53 Kb'),
(37, 30, '30_Masya Allah dan Subhanallah.jpg', '53 Kb'),
(38, 31, '31_1487725661534.jpg', '36 Kb'),
(39, 32, '32_1487744087377.jpg', '57 Kb'),
(40, 33, '33_1487744964351.jpg', '64 Kb'),
(41, 34, '34_1487745389276.jpg', '65 Kb'),
(42, 35, '35_1487745878132.jpg', '53 Kb'),
(43, 36, '36_IMG-20170222-WA0003.jpg', '81 Kb'),
(44, 37, '37_1487747776265.jpg', '59 Kb'),
(45, 38, '38_1487748035968.jpg', '61 Kb'),
(46, 38, '38_1487748047170.jpg', '49 Kb'),
(47, 39, '39_1487750500119.jpg', '32 Kb'),
(48, 40, '40_1487751030411.jpg', '48 Kb'),
(49, 41, '41_1488258449285.jpg', '36 Kb'),
(50, 43, '43_1489590158065.jpg', '53 Kb');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_gambarproduk`
--

CREATE TABLE `tb_gambarproduk` (
  `id` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_infosistem`
--

CREATE TABLE `tb_infosistem` (
  `kolom` varchar(20) NOT NULL,
  `isi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_infosistem`
--

INSERT INTO `tb_infosistem` (`kolom`, `isi`) VALUES
('avidhape', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jem_cs`
--

CREATE TABLE `tb_jem_cs` (
  `id` int(11) NOT NULL,
  `idcs` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jem_cs`
--

INSERT INTO `tb_jem_cs` (`id`, `idcs`, `idj`) VALUES
(1, 1, 1),
(3, 1, 2),
(4, 3, 1),
(5, 3, 3),
(6, 1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jem_lawyer`
--

CREATE TABLE `tb_jem_lawyer` (
  `id` int(11) NOT NULL,
  `idl` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jem_lawyer`
--

INSERT INTO `tb_jem_lawyer` (`id`, `idl`, `idj`) VALUES
(3, 4, 2),
(4, 4, 4),
(5, 5, 3),
(6, 5, 1),
(7, 4, 1),
(8, 8, 1),
(9, 8, 3),
(10, 8, 4),
(11, 7, 1),
(12, 7, 3),
(13, 9, 1),
(14, 9, 2),
(15, 10, 1),
(16, 10, 4),
(17, 11, 2),
(18, 11, 3),
(19, 11, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jem_product_cat`
--

CREATE TABLE `tb_jem_product_cat` (
  `id` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `idpc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jem_product_cat`
--

INSERT INTO `tb_jem_product_cat` (`id`, `idp`, `idpc`) VALUES
(1, 1, 1),
(10, 5, 7),
(13, 6, 6),
(12, 6, 2),
(11, 5, 1),
(9, 4, 7),
(15, 8, 6),
(32, 18, 7),
(17, 10, 3),
(31, 18, 6),
(19, 12, 2),
(20, 12, 6),
(21, 13, 1),
(22, 13, 6),
(23, 14, 6),
(24, 14, 7),
(25, 15, 7),
(26, 16, 1),
(27, 17, 2),
(28, 17, 7),
(29, 1, 3),
(30, 1, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jem_transaksi_produk`
--

CREATE TABLE `tb_jem_transaksi_produk` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `price_item` double NOT NULL,
  `amount` int(11) NOT NULL,
  `idtrx` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jem_transaksi_produk`
--

INSERT INTO `tb_jem_transaksi_produk` (`id`, `product_id`, `product_name`, `price_item`, `amount`, `idtrx`, `status`, `create_at`, `last_update`) VALUES
(1, 5, 'Jagung', 5000, 1, '1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 5, 'Jagung', 5000, 1, '2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 5, 'Jagung', 5000, 1, '3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 5, 'Jagung', 5000, 1, '4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'Bayam', 15000, 1, '5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 5, 'Jagung', 5000, 1, '6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 'Bayam', 13000, 1, '7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 'Bayam', 13000, 1, '8', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 16, ' sk', 986868, 1, '9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 1, 'Bayam', 13000, 1, '9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 'Bayam', 13000, 1, '10', 0, '2018-08-05 11:52:25', '2018-08-05 11:52:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenishukum`
--

CREATE TABLE `tb_jenishukum` (
  `id` int(11) NOT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `materil` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenishukum`
--

INSERT INTO `tb_jenishukum` (`id`, `jenis`, `materil`) VALUES
(1, 'Pidana', 1),
(2, 'Perdata', 1),
(3, 'Investasi', 1),
(4, 'Perusahaan', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_klien`
--

CREATE TABLE `tb_klien` (
  `id` int(11) NOT NULL,
  `idu` int(11) NOT NULL,
  `online` int(1) DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  `saldo` int(11) DEFAULT '0',
  `komisi` int(11) DEFAULT '0',
  `token` text,
  `idch` int(11) DEFAULT NULL,
  `idref` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_klien`
--

INSERT INTO `tb_klien` (`id`, `idu`, `online`, `update_time`, `saldo`, `komisi`, `token`, `idch`, `idref`) VALUES
(1, 1, 0, NULL, 4734275, NULL, 'eeu36V4-ezM:APA91bFnSd5w_EjcMthYGFDPvfnG4A9GU67xcXIt9Aszir-Gfida0h0o26QgJ6PVwZAoNwS9ihzPWH_WU86IFFLdL-_Nug9rsIGyqt0cI4hPyLLIIRpiSNVCdRn7ktC9-Ocjq9gMAMyO', NULL, 0),
(2, 2, 0, NULL, 7344970, 445000, 'ej2M7um8Cgk:APA91bGQuXNyuxmrdsr75YXiEmM4MGAB-bL5jGlvvNkvKkf8A1ksjLwfVHhCE_NktOu9j_6J_08HDi0yrOy-hfYOt3ORMGQhnQFv-EdAmFQtz9b7jGPkEQpXS8v3jnNASPChTsesYDbd', NULL, 0),
(5, 21, 0, NULL, 0, NULL, NULL, NULL, 2),
(6, 22, 0, NULL, 0, 0, 'cG4Sh5qDN4E:APA91bE9Vsz93sBHDPgXVsWyMHvO54oVCqn9cyXdt9KJX3_ykRlTtKvT507m_YbvthC0avAQ9z1DXokc_7ayLQbMtkcxfZCyLWMwupw-2n0c0VdHvo5NuasCevuquHhScJ9bFCcqeSey', NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_lawyer`
--

CREATE TABLE `tb_lawyer` (
  `id` int(11) NOT NULL,
  `idu` int(11) NOT NULL,
  `online` int(1) DEFAULT '0',
  `onlinesejak` datetime DEFAULT NULL,
  `permenit` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `token` text,
  `latitude` varchar(15) DEFAULT '0',
  `longitude` varchar(15) DEFAULT '0',
  `firmlatitude` varchar(15) DEFAULT '0',
  `firmlongitude` varchar(15) DEFAULT '0',
  `tampilkanposisi` int(1) DEFAULT '0',
  `persenfee` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_lawyer`
--

INSERT INTO `tb_lawyer` (`id`, `idu`, `online`, `onlinesejak`, `permenit`, `update_time`, `saldo`, `token`, `latitude`, `longitude`, `firmlatitude`, `firmlongitude`, `tampilkanposisi`, `persenfee`) VALUES
(4, 3, 1, NULL, 6010, '2017-03-31 21:31:30', 1200000, '0', '-6.18398556', '106.7676037', '0', '0', 3, 60),
(5, 4, 1, '2017-04-03 12:47:47', 4500, '2016-12-22 17:41:21', 1317325, 'eKrN_2b9uSc:APA91bF2Ts0dTkHmeyhNHWYvDLLlAPH7zgfLlEHa4w3kdmh0gJZhdtESLEZnvSqV5xjBOS99LgqY6BQxZp3nDDJjiPYrs2XJlLQYVYZuzWGubIwXQieTcJ-nbBs3tudGd2adwLfXiUne', '0', '0', '0', '0', 3, 60),
(7, 5, 1, NULL, 5500, '2016-12-23 09:30:14', 1500000, NULL, '0', '0', '0', '0', 0, 60),
(8, 6, 0, NULL, 7800, NULL, 1500000, NULL, '0', '0', '0', '0', 0, 60),
(9, 7, 1, NULL, 6500, NULL, 1500000, NULL, '0', '0', '0', '0', 0, 60),
(10, 8, 0, NULL, 6500, NULL, 1500000, NULL, '0', '0', '0', '0', 0, 60),
(11, 12, 0, NULL, 4500, NULL, 1500000, NULL, '0', '0', '0', '0', 0, 60),
(12, 13, 0, NULL, 5000, NULL, 1500000, NULL, '0', '0', '0', '0', 0, 50);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_metode_topup`
--

CREATE TABLE `tb_metode_topup` (
  `id` int(11) NOT NULL,
  `metode` varchar(20) DEFAULT NULL,
  `nomakun` varchar(30) DEFAULT NULL,
  `namaakun` varchar(30) DEFAULT NULL,
  `petunjuk` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_metode_topup`
--

INSERT INTO `tb_metode_topup` (`id`, `metode`, `nomakun`, `namaakun`, `petunjuk`) VALUES
(1, 'Transfer BCA', '644338877', 'TLCorp', 'Pergi ke ATM, lalu transfer ke rekening ini');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mutasi`
--

CREATE TABLE `tb_mutasi` (
  `id` int(11) NOT NULL,
  `idl` int(11) DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `fee` int(11) DEFAULT NULL,
  `share` int(11) DEFAULT NULL,
  `tl` int(11) DEFAULT NULL,
  `withdraw` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_mutasi`
--

INSERT INTO `tb_mutasi` (`id`, `idl`, `periode`, `fee`, `share`, `tl`, `withdraw`, `saldo`) VALUES
(1, 5, '2017-04-20', 500000, 300000, 200000, 100000, 3000000),
(3, 5, '2017-05-04', 0, 0, 0, 1050000, 400000),
(4, 5, '2018-05-04', 25125, 14850, 9900, NULL, 1317100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_mutasiklien`
--

CREATE TABLE `tb_mutasiklien` (
  `id` int(11) NOT NULL,
  `idk` int(11) DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `komisi` int(11) DEFAULT NULL,
  `totalkomisi` int(11) DEFAULT NULL,
  `withdraw` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_mutasiklien`
--

INSERT INTO `tb_mutasiklien` (`id`, `idk`, `periode`, `komisi`, `totalkomisi`, `withdraw`) VALUES
(1, 2, '2017-05-18', 800000, 800000, 355000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `tanggalupdate` datetime NOT NULL,
  `main` int(1) NOT NULL,
  `idbm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pesan`
--

INSERT INTO `tb_pesan` (`id`, `status`, `idk`, `idl`, `judul`, `pesan`, `tanggal`, `tanggalupdate`, `main`, `idbm`) VALUES
(1, 0, 2, 5, 'zf', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, 0, 2, 5, '', 'tes tes tes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(3, 0, 2, 5, '', 'tes tes tesxt Dy xgxgzgzg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(4, 0, 2, 5, '', 'ycycyd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(5, 0, 2, 5, '', 'xgxtxtzr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(6, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(7, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(8, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(9, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(10, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(11, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(12, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(13, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(14, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(15, 0, 2, 5, '', 'CJ dhhh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(16, 0, 2, 5, '', 'sxjHH hhchahgdycy', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(17, 0, 2, 5, '', 'hHahaha', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(18, 0, 2, 5, '', 'hahahahs', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(19, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(20, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(21, 0, 2, 5, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(22, 0, 2, 5, '', 'gsgvxh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(23, 0, 2, 5, '', 'ffc', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(24, 0, 2, 5, '', 'ffc', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(25, 0, 2, 5, '', 'SG sgsg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(26, 0, 2, 5, '', 'SG sgsg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(27, 0, 2, 5, 'fhxhhxh', 'SG sgsg', '2017-02-21 20:21:28', '0000-00-00 00:00:00', 0, 0),
(28, 0, 2, 5, 'fhxhhxh', 'SG sgsg', '2017-02-21 20:22:14', '0000-00-00 00:00:00', 0, 0),
(29, 0, 1, 4, '', '', '2017-02-22 07:33:26', '0000-00-00 00:00:00', 0, 0),
(30, 0, 1, 4, 'tes pesan', 'apakah bisa masuk?', '2017-02-22 07:35:40', '0000-00-00 00:00:00', 0, 0),
(31, 0, 1, 4, 'saya ditilang', 'polisinya minta 100ribu? dikasih ga?', '2017-02-22 08:03:22', '0000-00-00 00:00:00', 0, 0),
(32, 0, 1, 5, 'tes siang', 'lagi coba lagi', '2017-02-22 13:14:53', '0000-00-00 00:00:00', 0, 0),
(33, 0, 1, 4, 'tes meeting', 'tes lagi meeting', '2017-02-22 13:29:30', '0000-00-00 00:00:00', 0, 0),
(34, 0, 1, 4, 'tes air', 'kirim air lagi', '2017-02-22 13:36:36', '0000-00-00 00:00:00', 0, 0),
(35, 0, 1, 4, 'tes makan', 'makan nasi enak', '2017-02-22 13:44:42', '0000-00-00 00:00:00', 0, 0),
(36, 0, 2, 5, 'CC', 'XF XF xt', '2017-02-22 14:05:00', '0000-00-00 00:00:00', 0, 0),
(37, 0, 1, 4, 'tes gelas', 'kirim gelas', '2017-02-22 14:16:20', '0000-00-00 00:00:00', 0, 0),
(38, 0, 1, 4, 'saya kena tilang', 'polisinya minta uang.tolong dong', '2017-02-22 14:20:56', '0000-00-00 00:00:00', 0, 0),
(39, 0, 2, 5, 'tes', 'teeees', '2017-02-22 15:02:30', '0000-00-00 00:00:00', 0, 0),
(40, 0, 1, 4, 'tes sepatu', 'sepatu', '2017-02-22 15:10:35', '0000-00-00 00:00:00', 0, 0),
(41, 0, 1, 4, 'gdhdhd', 'chdhdhd', '2017-02-28 12:07:36', '0000-00-00 00:00:00', 0, 0),
(42, 0, 2, 0, 'jJjidudud', 'jajajsjzjsjzizi', '2017-03-01 14:57:24', '0000-00-00 00:00:00', 0, 0),
(43, 0, 1, 0, 'hfjdhd', 'bdhdh', '2017-03-15 22:02:24', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_product`
--

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL,
  `ids` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `mainpict` text NOT NULL,
  `info` text NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `aktif` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_product`
--

INSERT INTO `tb_product` (`id`, `ids`, `name`, `price`, `mainpict`, `info`, `create_at`, `last_update`, `aktif`) VALUES
(1, 18, 'Bayam', 13000, 'fotoutamaproduk1.jpg', 'Hijab cantikaaa', '2018-08-01 00:00:00', '2018-08-03 00:34:30', 1),
(5, 18, 'Jagung', 5000, 'fotoutamaproduk5.jpg', 'Kiloanaaa', '2018-08-01 00:00:00', '2018-08-02 23:36:18', 1),
(4, 18, 'Buah Apel', 30000, 'fotoutamaproduk4.jpg', 'Rp 30.000 / Perkilo', '2018-08-01 00:00:00', '0000-00-00 00:00:00', 1),
(8, 18, 'hsjsjs', 4646464, '', 'bzbzbzhz', '2018-08-02 17:14:00', '2018-08-02 00:00:00', 1),
(10, 18, 'hzhshs', 9594955, '', 'cn nxjx', '2018-08-02 17:17:57', '2018-08-02 00:00:00', 1),
(12, 18, 'tghggg', 565666, '', 'dfxxghhg', '2018-08-02 17:29:04', '2018-08-02 00:00:00', 1),
(13, 18, 'hd gdgdhd', 655555, '', 'bdjdjd', '2018-08-02 17:34:37', '2018-08-02 00:00:00', 1),
(14, 18, 'cgfdd', 96555, 'fotoutamaproduk14.jpg', 'cgdggdffvj', '2018-08-02 17:37:42', '2018-08-02 00:00:00', 1),
(15, 18, 'yuuuu', 257000, 'fotoutamaproduk15.jpg', 'yg uviffgfuvjgug tsb', '2018-08-02 17:39:44', '2018-08-02 00:00:00', 1),
(16, 18, ' sk', 986868, 'fotoutamaproduk16.jpg', 'bxhzhshjsjshshs', '2018-08-02 18:13:24', '2018-08-02 00:00:00', 1),
(17, 18, 'fhfggg', 18000, 'fotoutamaproduk17.jpg', 'cvjjgukbg', '2018-08-02 18:28:15', '2018-08-02 00:00:00', 1),
(18, 18, 'yvcxdr', 25000, 'fotoutamaproduk18.jpg', 'bjghkgg', '2018-08-03 17:45:26', '2018-08-03 17:45:26', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_product_cat`
--

CREATE TABLE `tb_product_cat` (
  `id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `info` text NOT NULL,
  `color` varchar(20) NOT NULL,
  `icon` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_product_cat`
--

INSERT INTO `tb_product_cat` (`id`, `category`, `info`, `color`, `icon`) VALUES
(1, 'Sayuran', 'Sayur mayur segar', '#8ddd6f', 'ikonproduk1.png'),
(2, 'Daging', 'Berbagai macam daging', '#8ddd6f', 'ikonproduk2.png'),
(3, 'Pecah belah', 'Barang pecah belah', '#8ddd6f', 'ikonproduk3.png'),
(6, 'Makanan', 'menjual banyak makanan', '#8ddd6f', 'ikonproduk6.png'),
(7, 'Buah', 'Menjual Bermacam - macam Buah', '#ff6f52', 'ikonproduk7.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_recipient`
--

CREATE TABLE `tb_recipient` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_recipient`
--

INSERT INTO `tb_recipient` (`id`, `name`, `phone`) VALUES
(1, 'Budi', 2147483647),
(2, 'Udin', 2147483647),
(3, 'Udin', 2147483647),
(4, 'Udin', 2147483647),
(5, 'Udin', 2147483647),
(6, 'Udin', 2147483647),
(7, 'udin', 5563255),
(8, 'fumi', 87785599);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_rekening`
--

CREATE TABLE `tb_rekening` (
  `id` int(11) NOT NULL,
  `idu` int(11) NOT NULL,
  `namabank` varchar(20) DEFAULT NULL,
  `nomrek` varchar(20) DEFAULT NULL,
  `atasnama` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_rekening`
--

INSERT INTO `tb_rekening` (`id`, `idu`, `namabank`, `nomrek`, `atasnama`) VALUES
(13, 23, NULL, NULL, NULL),
(14, 18, NULL, NULL, NULL),
(15, 17, NULL, NULL, NULL),
(16, 16, NULL, NULL, NULL),
(17, 15, NULL, NULL, NULL),
(18, 9, NULL, NULL, NULL),
(19, 28, NULL, NULL, NULL),
(20, 29, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sessid`
--

CREATE TABLE `tb_sessid` (
  `id` int(11) NOT NULL,
  `sessid` text,
  `tabel` varchar(10) DEFAULT NULL,
  `idt` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_sessid`
--

INSERT INTO `tb_sessid` (`id`, `sessid`, `tabel`, `idt`) VALUES
(132, '8c4d27abfa2f8e30c456fecb4e5bd2ccba3541a5298a9c4808e3ad7f9265f2afb85140cc9af17af350fa50fb2e7eebcf8e07831b15793f291f17b8263637c3fc', 'klien', 5),
(147, '296194564d2d120dab18c13ea78b254b447650a83137a220576476c420d3310e92c52864a49c6c62c735d2bbfc096d9381181ae14cd3e2db1225614617cff6da', 'lawyer', 4),
(153, '8188a2ebcfbb441245eac9310077ddd9fd89d402b1f4f3e8d9d9e430ef94342cfe8d6a39be2aa89873a35003e7e34970841e297015a7f627aefa62625db6ea0c', 'lawyer', 5),
(154, '1892ca101eca90d7602c9d4e729198d57671b24b561a29ddea2fcf69a11c4ae918bdd01e009b2d8ea2a95d9efe72e7149c14bc06657da37567450d5ffdb4e72a', 'klien', 1),
(164, 'c0abe76495480e57d25c62fe0ce1f6995eee81a9a248d0e20f592b5a11f63ff454b76cdbe54ed598c7f5e6bb1154440244be369c894b7970a60f37b16942651a', 'klien', 15),
(172, 'ae8880146a80a7b8332e2bfbfc5918c57995dbc95c926ff49b2d94eb1d5dee851522d0dbeb0394263e9e84dbdc83c9910b6d3a48a016cf4895c95970b61c4ceb', 'klien', 14),
(174, '13e8592ff44b3835e9259e7ddace91a1011d81bf9d6f4f25c019d3b625a60cfb2971c94f81ce31617de5e2a8f099036f2471ef330ae77b1a1c54865d4691f496', 'klien', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_system_config`
--

CREATE TABLE `tb_system_config` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_system_config`
--

INSERT INTO `tb_system_config` (`id`, `label`, `value`, `description`) VALUES
(1, 'tax', '10', 'Pajak yang harus ditanggung pembeli'),
(2, 'ongkir', '2000', 'Biaya ongkir per toko\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_topup`
--

CREATE TABLE `tb_topup` (
  `id` int(11) NOT NULL,
  `topup_amount` int(11) DEFAULT NULL,
  `last_amount` int(11) NOT NULL,
  `idu` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `buktitransfer` varchar(30) DEFAULT NULL,
  `user_comment` text,
  `admin_comment` text,
  `bank_id` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `expired_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_topup`
--

INSERT INTO `tb_topup` (`id`, `topup_amount`, `last_amount`, `idu`, `status`, `buktitransfer`, `user_comment`, `admin_comment`, `bank_id`, `create_at`, `last_update`, `expired_date`) VALUES
(2, 200000, 0, 2, 3, NULL, 'Sudah saya  kirim', 'tes', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 200000, 0, 1, 1, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 200000, 0, 1, 2, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 550000, 0, 2, 3, 'buktitransfertopup_6_jpeg', 'silahkan dicek', 'Sudah diterima', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 750000, 0, 2, 3, 'buktitransfertopup_8_jpeg', 'sudah', 'Sudah diisi', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 200000, 0, 2, 3, NULL, 'sudah', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 256000, 0, 2, -1, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 350000, 0, 18, 3, NULL, 'ghthg', 'sadasdasd', 2, '2018-08-03 14:01:10', '2018-08-03 21:46:25', '2018-08-03 22:10:05'),
(12, 347000, 0, 18, 0, NULL, NULL, NULL, 6, '2018-08-03 14:42:03', '2018-08-03 14:42:03', '2018-08-03 22:10:05'),
(13, 55639, 0, 18, 2, NULL, NULL, 'afdsfdf', 7, '2018-08-03 14:45:26', '2018-08-03 14:45:26', '2018-08-03 22:10:05'),
(14, 32000, 0, 18, 3, NULL, NULL, '', 8, '2018-08-03 14:47:49', '2018-08-03 14:47:49', '2018-08-03 22:10:05'),
(15, 596688, 0, 18, 3, NULL, NULL, NULL, 9, '2018-08-03 14:50:28', '2018-08-03 14:50:28', '2018-08-03 22:10:05'),
(16, 32655, 0, 18, 3, NULL, NULL, 'erwerer', 10, '2018-08-03 14:51:53', '2018-08-03 14:51:53', '2018-08-03 22:10:05'),
(17, 32555, 0, 18, 0, NULL, NULL, NULL, 11, '2018-08-03 14:54:07', '2018-08-03 14:54:07', '2018-08-03 22:10:05'),
(18, 32000, 0, 18, 3, NULL, NULL, 'aadsfdsf', 12, '2018-08-03 15:00:30', '2018-08-03 15:00:30', '2018-08-03 22:10:05'),
(19, 34000, 0, 18, 0, NULL, NULL, NULL, 13, '2018-08-03 16:36:56', '2018-08-03 16:36:56', '0000-00-00 00:00:00'),
(20, 350000, 0, 18, 0, NULL, NULL, NULL, 15, '2018-08-03 16:46:22', '2018-08-03 16:46:22', '0000-00-00 00:00:00'),
(21, 450000, 0, 18, 0, NULL, NULL, NULL, 15, '2018-08-03 16:47:45', '2018-08-03 16:47:45', '2018-08-03 22:47:45'),
(22, 558000, 0, 18, 0, NULL, NULL, NULL, 16, '2018-08-03 17:00:51', '2018-08-03 17:00:51', '2018-08-03 23:00:51'),
(23, 34000, 0, 18, 0, NULL, 'opopoopbg', NULL, 17, '2018-08-03 17:10:16', '2018-08-03 17:11:01', '2018-08-03 23:10:16'),
(24, 285588, 0, 18, 1, NULL, 'vbvvv', NULL, 18, '2018-08-03 17:15:05', '2018-08-03 17:15:21', '2018-08-03 23:15:05'),
(25, 522555, 0, 18, 1, NULL, 'gbvvv', NULL, 19, '2018-08-03 17:20:37', '2018-08-03 17:24:13', '2018-08-03 23:20:37'),
(26, 25000, 0, 18, 1, NULL, 'cvvg', NULL, 20, '2018-08-03 17:30:47', '2018-08-03 17:31:09', '2018-08-03 23:30:47'),
(27, 154585, 0, 18, 1, NULL, 'vhjj', NULL, 21, '2018-08-03 17:43:48', '2018-08-03 17:44:09', '2018-08-03 23:43:48'),
(28, 855885, 0, 18, 1, 'buktitransfertopup_28.jpg', 'yjhgvgguuuuy', NULL, 22, '2018-08-03 17:46:27', '2018-08-03 21:39:57', '2018-08-03 23:46:27'),
(29, 6588525, 0, 18, 1, 'buktitransfertopup_29.jpg', 'vjthgg', NULL, 23, '2018-08-03 17:53:41', '2018-08-03 21:37:06', '2018-08-03 23:53:41'),
(30, 345552, 0, 18, 1, 'buktitransfertopup_30.jpg', 'vhgbggv', NULL, 24, '2018-08-03 17:56:09', '2018-08-03 17:56:48', '2018-08-03 23:56:09'),
(31, 99655, 0, 18, 1, 'buktitransfertopup_31.jpg', ' njmhgh', NULL, 25, '2018-08-03 17:59:32', '2018-08-03 17:59:53', '2018-08-03 23:59:32'),
(32, 9565553, 0, 18, 1, 'buktitransfertopup_32.jpg', 'hjnhh', NULL, 26, '2018-08-03 18:02:47', '2018-08-03 18:03:08', '2018-08-04 00:02:47'),
(33, 998566, 0, 18, 1, 'buktitransfertopup_33.jpg', 'hkyg', NULL, 27, '2018-08-03 18:07:51', '2018-08-03 18:08:11', '2018-08-04 00:07:51'),
(34, 89555, 0, 18, 1, 'buktitransfertopup_34.jpg', 'fjgyggg', NULL, 28, '2018-08-03 18:35:34', '2018-08-03 18:35:55', '2018-08-04 00:35:34'),
(35, 85698633, 0, 18, 0, NULL, NULL, NULL, 29, '2018-08-03 19:46:18', '2018-08-03 19:46:18', '2018-08-04 01:46:18'),
(36, 853952, 0, 18, 0, NULL, NULL, NULL, 30, '2018-08-03 19:48:06', '2018-08-03 19:48:06', '2018-08-04 01:48:06'),
(37, 250000, 0, 18, 0, NULL, NULL, NULL, 31, '2018-08-03 19:49:32', '2018-08-03 19:49:32', '2018-08-04 01:49:32'),
(38, 2658525, 0, 18, 0, NULL, NULL, NULL, 32, '2018-08-03 19:51:44', '2018-08-03 19:51:44', '2018-08-04 01:51:44'),
(39, 5589669, 0, 18, 0, NULL, NULL, NULL, 33, '2018-08-03 19:54:09', '2018-08-03 19:54:09', '2018-08-04 01:54:09'),
(40, 585966, 0, 18, 1, 'buktitransfertopup_40.jpg', '', NULL, 34, '2018-08-03 19:59:01', '2018-08-03 19:59:59', '2018-08-04 01:59:01'),
(41, 250000, 0, 17, 2, NULL, NULL, '', 35, '2018-08-05 15:30:45', '2018-08-05 15:30:45', '2018-08-05 21:30:45'),
(42, 50000, 0, 17, 3, NULL, 'vjggfffg', 'sudah diterima', 36, '2018-08-05 20:00:26', '2018-08-05 20:01:00', '2018-08-06 02:00:26'),
(43, 50000, 0, 17, 3, NULL, NULL, 'sudah ditambah', 37, '2018-08-05 20:23:30', '2018-08-05 20:23:30', '2018-08-06 02:23:30'),
(44, 36000, 2583700, 17, 0, NULL, NULL, NULL, 38, '2018-08-05 20:31:53', '2018-08-05 20:31:53', '2018-08-06 02:31:53'),
(45, 500000, 0, 29, 3, 'buktitransfertopup_45.jpg', 'selesai Di transfer', 'Sudah diterima', 39, '2018-08-06 00:11:32', '2018-08-06 00:12:42', '2018-08-06 06:11:32'),
(46, 300000, 500000, 29, 2, NULL, NULL, 'dana tidak valid', 39, '2018-08-06 00:17:35', '2018-08-06 00:17:35', '2018-08-06 06:17:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id` int(11) NOT NULL,
  `notrx` int(11) NOT NULL,
  `nogroup` int(11) NOT NULL,
  `idb` int(11) NOT NULL,
  `ids` int(11) NOT NULL,
  `idts` int(11) NOT NULL,
  `ida` int(11) NOT NULL,
  `idr` int(11) NOT NULL,
  `bill` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `ongkir` int(11) NOT NULL,
  `comment` text NOT NULL,
  `seller_comment` text NOT NULL,
  `admin_comment` text NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id`, `notrx`, `nogroup`, `idb`, `ids`, `idts`, `ida`, `idr`, `bill`, `tax`, `ongkir`, `comment`, `seller_comment`, `admin_comment`, `create_at`, `last_update`, `status`) VALUES
(2, 276392, 0, 18, 18, 2, 1, 2, 15500, 0, 0, 'jsnsjsnsnbss', 'popopo', '', '0000-00-00 00:00:00', '2018-08-04 22:42:09', 1),
(3, 324945, 0, 18, 18, 2, 1, 3, 15500, 0, 0, 'gjcghg', 'habis', '', '0000-00-00 00:00:00', '2018-08-04 23:10:59', 3),
(4, 41855582, 0, 18, 18, 2, 1, 4, 15500, 0, 0, 'gbchcfbjjg', 'lmlml', '', '0000-00-00 00:00:00', '2018-08-04 23:28:51', 3),
(5, 51851361, 0, 18, 18, 2, 1, 5, 26500, 0, 0, 'vjfbbh', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 61823022, 0, 18, 18, 2, 1, 6, 15500, 0, 0, 'bawa hati2', 'xfff', '', '0000-00-00 00:00:00', '2018-08-05 09:27:52', 3),
(7, 71881505, 0, 18, 18, 2, 1, 7, 24300, 0, 0, 'gbfkvgg', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 81886279, 1885485, 18, 18, 2, 1, 7, 24300, 0, 0, 'chgyvff', '', '', '2018-08-04 00:37:39', '2018-08-04 00:37:39', 0),
(9, 91810918, 1863713, 18, 18, 2, 1, 7, 1109855, 0, 0, '', '', '', '2018-08-04 00:55:01', '2018-08-04 00:55:01', 0),
(10, 101766971, 1713754, 17, 18, 2, 4, 8, 13000, 1300, 2000, 'makasih', '', '', '2018-08-05 11:52:25', '2018-08-05 12:08:14', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transport_service`
--

CREATE TABLE `tb_transport_service` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `detail` text NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transport_service`
--

INSERT INTO `tb_transport_service` (`id`, `name`, `detail`, `create_at`, `last_update`) VALUES
(2, 'Kurir Bendajaya Corp', 'Kurir resmi perusahaan', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '0',
  `filefoto` varchar(30) NOT NULL DEFAULT 'fotodefault.png',
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `salt` varchar(20) NOT NULL,
  `sessid` text,
  `token` text NOT NULL,
  `status` int(1) DEFAULT '1',
  `idr` int(11) DEFAULT NULL,
  `deviceid` text NOT NULL,
  `address` text NOT NULL,
  `idsaldo` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `level`, `name`, `email`, `phone`, `filefoto`, `username`, `password`, `salt`, `sessid`, `token`, `status`, `idr`, `deviceid`, `address`, `idsaldo`, `create_at`, `last_update`, `last_login`) VALUES
(9, 4, 'Bos Admin', 'bosadmin@yahoo.com', '088655332111', 'fotolawyer9.jpg', 'bosadmin', '24b596058c1c7fc2f517cdecf19b2d74f5234cd348acd6bccd751a7d5707fcf6047553a6e6b0fbb856bb29ab7e83f6a8c49bc7f8f2602bae6c3db4a17ad21380', 'garammanis587', NULL, '', 1, NULL, '', '', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 'Hardiansyah', 'hardi@yahoo.com', '08773332', 'fotodefault.png', 'cshardi', 'a9d77b32af58cdf32add9b8638484af7c3f85a9b8d1ace2944a40341208b7fb11b5146fa9781f856f75b7aeadba43ff874621548355bb9ba99753509b6475fa2', 'garammanis798', NULL, '', 1, 1, '', '', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 1, 'Joko', 'joko@yahoo.com', '0823333', 'fotodefault.png', 'joko', 'e1ebd1e18238d6f52787c7ee637f3932713c330f948f4cfaea20d5126549499e74f900c708eb9040e2164f91e996a708783ede3c2309a618c09d4c35a539e547', 'garammanis91', NULL, '', 1, 3, '', '', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 2, 'Fumi', 'fumi@yahoo.com', '085423333', 'fotodefault.png', 'fumi', 'fbada4352f2e88ed2125345bae11f8cf251567ce0cf91fd04536a256442e287bab8d0a906926d8188521e9f21734a697e8af014837b965aa48042dd265bfd01c', 'garammanis440', 'c4605502bb8fb4d5d70a7e5307055b8d385f1505273b5171985b05ba8d3fc6d572c08031b8029f78c6c56f54db2d5849d246ba60799a9ec48fd4750a1f21244d', '', 1, NULL, '', '', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 3, 'Sofian', 'sofian@yahoo.com', '08543333222', 'fotodefault.png', 'sofian', '5ef189b1f637a14c883dfcb0a2ba21c05560d7ccf1ea88d16e7fd5031fcb049d0ebeb7ddc75870ae115e44db3f2477250839d80227fb851ad3d35c550d1e38ae', 'garammanis810', 'e2198d4c7ca5e12ba21aeaf4e35f09919362eeabba7ed825fe8c3343475180688cb7a9c1e2756345256bdee040da1505effb2b265ca1606f2a51c9232d72f5ef', '', 1, NULL, '', '', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 4, 'Imam', 'imam@yahoo.com', '0823432424', 'fotodefault.png', 'imam', '2bc96703d4684cce3db8d8144e3ec5d038ef911d4866cc52a8520928d5df9b8611c37c0a6cda92309f0022cc740fde260fe11aedb4dd37ef571ab619d27735d4', 'garammanis639', NULL, '', 1, NULL, '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 3, 'Didin', 'didin@yahoo.com', '0', 'fotodefault.png', 'didin', '6c1eae54c8113d7f4d570b307cc1cb0bf4a93d5084c8d60e3cc0794b4993c1e265f4cc2d80afcd8622019a7d6159c7fc91b5e7bcb7b98ebfb37d9f857b35f98a', '7', NULL, '', 1, NULL, '', '', 3, '2018-08-05 21:47:33', '2018-08-05 21:47:33', '0000-00-00 00:00:00'),
(27, 3, 'Yudi', 'yudi@yahoo.com', '0', 'fotodefault.png', 'yudi', '53dbfdb998b00ea884f69153fc5fd868ff33c049d2faee66c8b3e9d6cdd2b0a958b5df74549c99769cc98a607452cca681b43a67f5c24ff3b746aac9b2660c75', 'garammanis116', '9bed3eb022e087221f90ba5ed8b14be5331c5b2386238b783ee35ccaa6073049b45022b36b23fb8e1cf4a61a4ea6cd51ad98163e2670dbeb7d285eb0d377a894', '', 1, NULL, '', '', 10, '2018-08-05 22:56:58', '2018-08-05 22:56:58', '0000-00-00 00:00:00'),
(28, 3, 'Somad', 'somad@yahoo.com', '0', 'fotodefault.png', 'somad', '6e324a3643de2c376b7bba52cb1a6830832909ca6309ac8853dffbbde45f412f79176a19650143d4d23f5d80a8ebced72b24c6f209f22e14ccb43be0abee7440', 'garammanis654', 'ab2d4e759d794f25ea3c3c139dcae5a973692f7775b1252c99e7c463ad64285faad5ad68ddca5db1daec66c49cf4a216ea02dc9083f3172adb88e09891651795', '', 1, 19, '', '', 11, '2018-08-05 23:06:11', '2018-08-05 23:06:11', '0000-00-00 00:00:00'),
(29, 3, 'juhardi', 'juhardihamsyah@gmail.com', '0', 'fotodefault.png', 'juhardi', '67b209a474921b0601e34d8843c54d56ed10c090d25b4ae73754aa4e64d8f25e410e36b53cb69f69892bb1133e4255bc427fa6df173417cf11b2c67415662d12', 'garammanis728', 'c8c20e577b33f016326337ba51410307f23165e8c2bea37f7a6756daa39cdef0a12a691a9e5efade3f3ca11e6e1ac9460a3d3e4c70f0809b03f55f61ea3173ea', '', 1, 20, '', '', 12, '2018-08-06 00:10:27', '2018-08-06 00:10:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_wallet`
--

CREATE TABLE `tb_wallet` (
  `id` int(11) NOT NULL,
  `ecash` int(11) NOT NULL,
  `detail` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_wallet`
--

INSERT INTO `tb_wallet` (`id`, `ecash`, `detail`) VALUES
(1, 5000000, 'Belong to administrator'),
(2, 4589843, 'Belong to seller Sofian'),
(3, 2583700, 'Belong to driver Fumi'),
(4, 3200000, 'Belong to buyer Joko'),
(5, 4100000, 'Belong to buyer Hardiansyah'),
(6, 10000000, 'Belong to administrator'),
(7, 0, 'Belong to Didin'),
(11, 0, 'Belong to Somad'),
(10, 0, 'Belong to Yudi'),
(12, 500000, 'Belong to juhardi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_withdraw`
--

CREATE TABLE `tb_withdraw` (
  `id` int(11) NOT NULL,
  `idl` int(11) DEFAULT NULL,
  `tanggalminta` datetime DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `komenlawyer` text,
  `komenpusat` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_withdraw`
--

INSERT INTO `tb_withdraw` (`id`, `idl`, `tanggalminta`, `nilai`, `status`, `komenlawyer`, `komenpusat`) VALUES
(1, 5, '2017-04-03 10:44:22', 500000, 3, 'Kirim segera', 'Sudah ditransfer\r\n\r\nTerimakasih'),
(2, 5, '2017-05-04 13:48:02', 550000, 3, 'Kirim segera', ''),
(3, 4, '2017-05-06 10:29:41', 300000, 3, 'gercep', 'Sudah ditransfer dari rekening ini jam  segini'),
(4, 5, '2017-05-07 15:15:12', 200000, 3, 'cepet', 'Sudah ditransfer'),
(5, 5, '2017-05-11 13:40:26', 562222, -1, 'tes', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_withdrawkomisi`
--

CREATE TABLE `tb_withdrawkomisi` (
  `id` int(11) NOT NULL,
  `idk` int(11) DEFAULT NULL,
  `tanggalminta` datetime DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `komenklien` text,
  `komenpusat` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_withdrawkomisi`
--

INSERT INTO `tb_withdrawkomisi` (`id`, `idk`, `tanggalminta`, `nilai`, `status`, `komenklien`, `komenpusat`) VALUES
(1, 2, '2017-05-18 10:30:25', 355000, 3, 'cepat', 'Sudah ditransfer'),
(2, 2, '2017-05-18 13:08:37', 100000, 0, '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_address`
--
ALTER TABLE `tb_address`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_call_history`
--
ALTER TABLE `tb_call_history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_cs`
--
ALTER TABLE `tb_cs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_dokumentasi`
--
ALTER TABLE `tb_dokumentasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_filepesan`
--
ALTER TABLE `tb_filepesan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_gambarproduk`
--
ALTER TABLE `tb_gambarproduk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jem_cs`
--
ALTER TABLE `tb_jem_cs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jem_lawyer`
--
ALTER TABLE `tb_jem_lawyer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jem_product_cat`
--
ALTER TABLE `tb_jem_product_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jem_transaksi_produk`
--
ALTER TABLE `tb_jem_transaksi_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_jenishukum`
--
ALTER TABLE `tb_jenishukum`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_klien`
--
ALTER TABLE `tb_klien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_lawyer`
--
ALTER TABLE `tb_lawyer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_metode_topup`
--
ALTER TABLE `tb_metode_topup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_mutasiklien`
--
ALTER TABLE `tb_mutasiklien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_product_cat`
--
ALTER TABLE `tb_product_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_recipient`
--
ALTER TABLE `tb_recipient`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_rekening`
--
ALTER TABLE `tb_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_sessid`
--
ALTER TABLE `tb_sessid`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_system_config`
--
ALTER TABLE `tb_system_config`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_topup`
--
ALTER TABLE `tb_topup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_transport_service`
--
ALTER TABLE `tb_transport_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_wallet`
--
ALTER TABLE `tb_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_withdraw`
--
ALTER TABLE `tb_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_withdrawkomisi`
--
ALTER TABLE `tb_withdrawkomisi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_address`
--
ALTER TABLE `tb_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_call_history`
--
ALTER TABLE `tb_call_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `tb_cs`
--
ALTER TABLE `tb_cs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_dokumentasi`
--
ALTER TABLE `tb_dokumentasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_filepesan`
--
ALTER TABLE `tb_filepesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `tb_gambarproduk`
--
ALTER TABLE `tb_gambarproduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_jem_cs`
--
ALTER TABLE `tb_jem_cs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_jem_lawyer`
--
ALTER TABLE `tb_jem_lawyer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tb_jem_product_cat`
--
ALTER TABLE `tb_jem_product_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `tb_jem_transaksi_produk`
--
ALTER TABLE `tb_jem_transaksi_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_jenishukum`
--
ALTER TABLE `tb_jenishukum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_klien`
--
ALTER TABLE `tb_klien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_lawyer`
--
ALTER TABLE `tb_lawyer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_metode_topup`
--
ALTER TABLE `tb_metode_topup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_mutasi`
--
ALTER TABLE `tb_mutasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_mutasiklien`
--
ALTER TABLE `tb_mutasiklien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tb_product_cat`
--
ALTER TABLE `tb_product_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_recipient`
--
ALTER TABLE `tb_recipient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_rekening`
--
ALTER TABLE `tb_rekening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tb_sessid`
--
ALTER TABLE `tb_sessid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT untuk tabel `tb_system_config`
--
ALTER TABLE `tb_system_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_topup`
--
ALTER TABLE `tb_topup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_transport_service`
--
ALTER TABLE `tb_transport_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `tb_wallet`
--
ALTER TABLE `tb_wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_withdraw`
--
ALTER TABLE `tb_withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_withdrawkomisi`
--
ALTER TABLE `tb_withdrawkomisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
