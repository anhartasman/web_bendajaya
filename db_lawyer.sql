-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: db_lawyer
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_admin`
--

DROP TABLE IF EXISTS `tb_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  `salt` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_admin`
--

LOCK TABLES `tb_admin` WRITE;
/*!40000 ALTER TABLE `tb_admin` DISABLE KEYS */;
INSERT INTO `tb_admin` VALUES (1,'Ujang','ujang','1c87e047981901fcd2ed04f528fe62cf188dbf3525074bcea4ca0f318c78304ff6ab1a8d6463c807181948fa3b492b797b52f07bc5aa98af80333d850ab3902c','garammanis',5,NULL,NULL),(2,'Udin','udin','9237a2f7e6c70c987620ebefd1f695b5b8eb37654729ca7f8dd438b8b9e94819f561ce5af1caf1b77d195719d3d40008af364d6e30cea36d1ab7aad34c6382bf','garammanis827',5,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `tb_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_call_history`
--

DROP TABLE IF EXISTS `tb_call_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_call_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `seconds` int(11) NOT NULL,
  `bill` int(11) NOT NULL,
  `rating` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_call_history`
--

LOCK TABLES `tb_call_history` WRITE;
/*!40000 ALTER TABLE `tb_call_history` DISABLE KEYS */;
INSERT INTO `tb_call_history` VALUES (1,2,5,'2017-02-23 19:33:54',15,1125,1),(2,2,5,'2017-02-24 15:01:06',15,1125,1),(3,2,5,'2017-02-24 15:02:49',10,750,3),(4,2,5,'2017-02-24 18:22:57',10,750,4),(5,2,4,'2017-02-25 20:41:41',10,1010,2),(6,2,5,'2017-03-01 15:59:15',5,375,4),(7,2,5,'2017-03-01 16:00:38',15,1125,2),(8,2,5,'2017-03-01 18:10:35',5,375,1),(9,2,5,'2017-03-02 06:52:45',5,375,4),(10,2,5,'2017-03-02 09:45:36',5,375,1),(11,2,5,'2017-03-02 09:51:55',5,375,1),(12,2,5,'2017-03-02 10:59:40',40,3000,4),(13,2,5,'2017-03-02 11:00:53',10,750,1),(14,2,5,'2017-03-02 11:02:23',15,1125,1),(15,2,5,'2017-03-02 11:17:57',5,375,5),(16,2,5,'2017-03-02 11:43:17',10,750,5),(17,2,5,'2017-03-02 11:48:14',5,375,5),(18,2,5,'2017-03-02 15:58:35',5,375,4),(19,2,5,'2017-03-02 16:01:49',5,375,4),(20,2,5,'2017-03-03 04:06:21',10,750,3),(21,2,5,'2017-03-03 04:07:21',5,375,4),(22,2,5,'2017-03-07 09:12:21',10,750,2),(23,2,5,'2017-03-07 09:15:40',5,375,3),(24,2,5,'2017-03-07 09:30:37',10,750,5),(25,2,5,'2017-03-10 13:58:11',15,1125,1),(26,2,5,'2017-03-10 14:06:36',15,1125,5),(27,2,5,'2017-03-14 10:01:17',5,375,5),(28,1,4,'2017-03-19 14:05:09',0,0,2),(29,2,5,'2017-04-05 05:59:31',0,0,2);
/*!40000 ALTER TABLE `tb_call_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cs`
--

DROP TABLE IF EXISTS `tb_cs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idu` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cs`
--

LOCK TABLES `tb_cs` WRITE;
/*!40000 ALTER TABLE `tb_cs` DISABLE KEYS */;
INSERT INTO `tb_cs` VALUES (1,15),(3,16);
/*!40000 ALTER TABLE `tb_cs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_dokumentasi`
--

DROP TABLE IF EXISTS `tb_dokumentasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_dokumentasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `idk` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT '0',
  `masalah` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_dokumentasi`
--

LOCK TABLES `tb_dokumentasi` WRITE;
/*!40000 ALTER TABLE `tb_dokumentasi` DISABLE KEYS */;
INSERT INTO `tb_dokumentasi` VALUES (1,'2017-04-27 18:07:57',0,1,0,'istri saya dan saya berkulit putih tetapi istri saya melahirkan seorang anak berkulit hitam, bagaimana cara memastikan dia tidak selingkuh dan bagaimana hukumenafkahi anak ini apabila dia merupakan hasil perselingkuhan'),(2,'2017-04-27 18:09:47',1,2,3,'Kemarin mantan pegawai saya datang dan menagih gaji yang pernah dia tidak terima, bagaimanakah hukumnya apakah saya wajib untuk membayar gaji dia atau tidak usah?'),(3,'2017-04-27 21:56:12',1,1,4,'<p>pada suatu hari di galaksi yang jauh di luar sana seorang jedi bertemu dengan para clones yang sedang kelelahan<br></p>'),(4,'2017-04-27 21:57:37',0,2,0,'<p>Saya mendapatkan hadiah mobil dari kantor, apakah mobil ini sebaiknya langsung saya terima atau dilacak terlebih dahulu asal usulnya?<br></p>'),(5,'2017-04-27 22:11:49',4,2,2,'<p>ini seharusnya diisi dengan kendala bla bla bla hohohohoho<br></p>');
/*!40000 ALTER TABLE `tb_dokumentasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_filepesan`
--

DROP TABLE IF EXISTS `tb_filepesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_filepesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idp` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ukuran` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_filepesan`
--

LOCK TABLES `tb_filepesan` WRITE;
/*!40000 ALTER TABLE `tb_filepesan` DISABLE KEYS */;
INSERT INTO `tb_filepesan` VALUES (1,3,'3_Slip Gaji Anhar Tasman Des 16.pdf.pdf','0'),(2,4,'4_manual-faq.txt.txt','0'),(3,5,'5_en_story.txt.txt','0'),(4,6,'6_en.txt.txt','0'),(5,7,'7_en.txt.txt','0'),(6,7,'7_IMG_20161002_111303.jpg.jpg','0'),(7,8,'8_en.txt.txt','0'),(8,8,'8_IMG_20161002_111303.jpg.jpg','0'),(9,10,'10_bbm_tone.wav.wav','0'),(10,11,'11_IMG_20170220_102506.jpg.jpg','0'),(11,12,'12_IMG_20170220_102506.jpg.jpg','0'),(12,12,'12_facebook_ringtone_pop.m4a.m4a','0'),(13,13,'13_IMG-20170217-WA0005.jpeg.jpeg','0'),(14,14,'14_IMG-20170217-WA0005.jpeg.jpeg','0'),(15,14,'14_IMG_20170130_174820.jpg.jpg','0'),(16,15,'15_IMG_20170220_043758.jpg.jpg','0'),(17,15,'15_IMG_20170220_102506.jpg.jpg','0'),(18,16,'16_IMG_20170220_102506.jpg.jpg','0'),(19,16,'16_IMG_20170220_043758.jpg.jpg','0'),(20,17,'17_IMG_20170220_102506.jpg.jpg','0'),(21,18,'18_IMG_20170220_043758.jpg.jpg','0'),(22,18,'18_1487590161949.jpg.jpg','0'),(23,19,'19_IMG_20170215_160920_386.jpg.jpg','0'),(24,19,'19_13x19cm_nulisbukutemplate.doc.doc.doc','0'),(25,20,'20_Wait-for-it.mp4.mp4','0'),(26,21,'21_VID_85290914_110147_282.mp4.mp4','0'),(27,21,'21_Its-a-trap-to-grow-up.jpg.jpg','0'),(28,23,'23_IMG-20170220-WA0011.jpeg',''),(29,24,'24_IMG-20170220-WA0011.jpeg',''),(30,25,'25_IMG-20170220-WA0011.jpeg',''),(31,26,'26_IMG-20170220-WA0011.jpeg',''),(32,27,'27_IMG-20170220-WA0011.jpeg','736 Kb'),(33,28,'28_IMG-20170220-WA0011.jpeg','736 Kb'),(34,28,'28_en_story.txt','89 Kb'),(35,28,'28_Slip Gaji Anhar Tasman Des 16.pdf','39 Kb'),(36,29,'29_Masya Allah dan Subhanallah.jpg','53 Kb'),(37,30,'30_Masya Allah dan Subhanallah.jpg','53 Kb'),(38,31,'31_1487725661534.jpg','36 Kb'),(39,32,'32_1487744087377.jpg','57 Kb'),(40,33,'33_1487744964351.jpg','64 Kb'),(41,34,'34_1487745389276.jpg','65 Kb'),(42,35,'35_1487745878132.jpg','53 Kb'),(43,36,'36_IMG-20170222-WA0003.jpg','81 Kb'),(44,37,'37_1487747776265.jpg','59 Kb'),(45,38,'38_1487748035968.jpg','61 Kb'),(46,38,'38_1487748047170.jpg','49 Kb'),(47,39,'39_1487750500119.jpg','32 Kb'),(48,40,'40_1487751030411.jpg','48 Kb'),(49,41,'41_1488258449285.jpg','36 Kb'),(50,43,'43_1489590158065.jpg','53 Kb');
/*!40000 ALTER TABLE `tb_filepesan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_infosistem`
--

DROP TABLE IF EXISTS `tb_infosistem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_infosistem` (
  `kolom` varchar(20) NOT NULL,
  `isi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_infosistem`
--

LOCK TABLES `tb_infosistem` WRITE;
/*!40000 ALTER TABLE `tb_infosistem` DISABLE KEYS */;
INSERT INTO `tb_infosistem` VALUES ('avidhape',2);
/*!40000 ALTER TABLE `tb_infosistem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_jem_cs`
--

DROP TABLE IF EXISTS `tb_jem_cs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_jem_cs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcs` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_jem_cs`
--

LOCK TABLES `tb_jem_cs` WRITE;
/*!40000 ALTER TABLE `tb_jem_cs` DISABLE KEYS */;
INSERT INTO `tb_jem_cs` VALUES (1,1,1),(3,1,2),(4,3,1),(5,3,3),(6,1,3);
/*!40000 ALTER TABLE `tb_jem_cs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_jem_lawyer`
--

DROP TABLE IF EXISTS `tb_jem_lawyer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_jem_lawyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idl` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_jem_lawyer`
--

LOCK TABLES `tb_jem_lawyer` WRITE;
/*!40000 ALTER TABLE `tb_jem_lawyer` DISABLE KEYS */;
INSERT INTO `tb_jem_lawyer` VALUES (3,4,2),(4,4,4),(5,5,3),(6,5,1),(7,4,1),(8,8,1),(9,8,3),(10,8,4),(11,7,1),(12,7,3),(13,9,1),(14,9,2),(15,10,1),(16,10,4),(17,11,2),(18,11,3),(19,11,4);
/*!40000 ALTER TABLE `tb_jem_lawyer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_jenishukum`
--

DROP TABLE IF EXISTS `tb_jenishukum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_jenishukum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `materil` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_jenishukum`
--

LOCK TABLES `tb_jenishukum` WRITE;
/*!40000 ALTER TABLE `tb_jenishukum` DISABLE KEYS */;
INSERT INTO `tb_jenishukum` VALUES (1,'Pidana',1),(2,'Perdata',1),(3,'Investasi',1),(4,'Perusahaan',1);
/*!40000 ALTER TABLE `tb_jenishukum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_klien`
--

DROP TABLE IF EXISTS `tb_klien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_klien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idu` int(11) NOT NULL,
  `online` int(1) DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  `saldo` int(11) DEFAULT '0',
  `token` text,
  `idch` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_klien`
--

LOCK TABLES `tb_klien` WRITE;
/*!40000 ALTER TABLE `tb_klien` DISABLE KEYS */;
INSERT INTO `tb_klien` VALUES (1,1,0,NULL,4763150,NULL,NULL),(2,2,0,NULL,5094970,'ej2M7um8Cgk:APA91bGQuXNyuxmrdsr75YXiEmM4MGAB-bL5jGlvvNkvKkf8A1ksjLwfVHhCE_NktOu9j_6J_08HDi0yrOy-hfYOt3ORMGQhnQFv-EdAmFQtz9b7jGPkEQpXS8v3jnNASPChTsesYDbd',NULL);
/*!40000 ALTER TABLE `tb_klien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lawyer`
--

DROP TABLE IF EXISTS `tb_lawyer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lawyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idu` int(11) NOT NULL,
  `online` int(1) DEFAULT '0',
  `onlinesejak` datetime DEFAULT NULL,
  `permenit` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `token` text,
  `latitude` varchar(15) DEFAULT '0',
  `longitude` varchar(15) DEFAULT '0',
  `firmlatitude` varchar(15) DEFAULT '0',
  `firmlongitude` varchar(15) DEFAULT '0',
  `tampilkanposisi` int(1) DEFAULT '0',
  `persenfee` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lawyer`
--

LOCK TABLES `tb_lawyer` WRITE;
/*!40000 ALTER TABLE `tb_lawyer` DISABLE KEYS */;
INSERT INTO `tb_lawyer` VALUES (4,3,1,NULL,6010,'2017-03-31 21:31:30',138370,'0','-6.183565','106.767870','-6.184706','106.768632',2,60),(5,4,1,'2017-04-03 12:47:47',4500,'2016-12-22 17:41:21',761250,'0','-6.18401142','106.76744529','0','0',1,60),(7,5,1,NULL,5500,'2016-12-23 09:30:14',279220,NULL,'0','0','0','0',0,60),(8,6,0,NULL,7800,NULL,5850,NULL,'0','0','0','0',0,60),(9,7,1,NULL,6500,NULL,15805,NULL,'0','0','0','0',0,60),(10,8,0,NULL,6500,NULL,1534720,NULL,'0','0','0','0',0,60),(11,12,0,NULL,4500,NULL,NULL,NULL,'0','0','0','0',0,60),(12,13,0,NULL,5000,NULL,NULL,NULL,'0','0','0','0',0,50);
/*!40000 ALTER TABLE `tb_lawyer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_metode_topup`
--

DROP TABLE IF EXISTS `tb_metode_topup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_metode_topup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metode` varchar(20) DEFAULT NULL,
  `nomakun` varchar(30) DEFAULT NULL,
  `namaakun` varchar(30) DEFAULT NULL,
  `petunjuk` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_metode_topup`
--

LOCK TABLES `tb_metode_topup` WRITE;
/*!40000 ALTER TABLE `tb_metode_topup` DISABLE KEYS */;
INSERT INTO `tb_metode_topup` VALUES (1,'Transfer BCA','644338877','TLCorp','Pergi ke ATM, lalu transfer ke rekening ini');
/*!40000 ALTER TABLE `tb_metode_topup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_mutasi`
--

DROP TABLE IF EXISTS `tb_mutasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_mutasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idl` int(11) DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `fee` int(11) DEFAULT NULL,
  `share` int(11) DEFAULT NULL,
  `tl` int(11) DEFAULT NULL,
  `withdraw` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_mutasi`
--

LOCK TABLES `tb_mutasi` WRITE;
/*!40000 ALTER TABLE `tb_mutasi` DISABLE KEYS */;
INSERT INTO `tb_mutasi` VALUES (1,5,'2017-04-20',500000,300000,200000,100000,3000000);
/*!40000 ALTER TABLE `tb_mutasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pesan`
--

DROP TABLE IF EXISTS `tb_pesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `tanggalupdate` datetime NOT NULL,
  `main` int(1) NOT NULL,
  `idbm` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pesan`
--

LOCK TABLES `tb_pesan` WRITE;
/*!40000 ALTER TABLE `tb_pesan` DISABLE KEYS */;
INSERT INTO `tb_pesan` VALUES (1,0,2,5,'zf','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,0,2,5,'','tes tes tes','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,0,2,5,'','tes tes tesxt Dy xgxgzgzg','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,0,2,5,'','ycycyd','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(5,0,2,5,'','xgxtxtzr','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(6,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(7,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(8,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(9,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(10,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(11,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(12,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(13,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(14,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(15,0,2,5,'','CJ dhhh','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(16,0,2,5,'','sxjHH hhchahgdycy','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(17,0,2,5,'','hHahaha','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(18,0,2,5,'','hahahahs','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(19,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(20,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(21,0,2,5,'','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(22,0,2,5,'','gsgvxh','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(23,0,2,5,'','ffc','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(24,0,2,5,'','ffc','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(25,0,2,5,'','SG sgsg','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(26,0,2,5,'','SG sgsg','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(27,0,2,5,'fhxhhxh','SG sgsg','2017-02-21 20:21:28','0000-00-00 00:00:00',0,0),(28,0,2,5,'fhxhhxh','SG sgsg','2017-02-21 20:22:14','0000-00-00 00:00:00',0,0),(29,0,1,4,'','','2017-02-22 07:33:26','0000-00-00 00:00:00',0,0),(30,0,1,4,'tes pesan','apakah bisa masuk?','2017-02-22 07:35:40','0000-00-00 00:00:00',0,0),(31,0,1,4,'saya ditilang','polisinya minta 100ribu? dikasih ga?','2017-02-22 08:03:22','0000-00-00 00:00:00',0,0),(32,0,1,5,'tes siang','lagi coba lagi','2017-02-22 13:14:53','0000-00-00 00:00:00',0,0),(33,0,1,4,'tes meeting','tes lagi meeting','2017-02-22 13:29:30','0000-00-00 00:00:00',0,0),(34,0,1,4,'tes air','kirim air lagi','2017-02-22 13:36:36','0000-00-00 00:00:00',0,0),(35,0,1,4,'tes makan','makan nasi enak','2017-02-22 13:44:42','0000-00-00 00:00:00',0,0),(36,0,2,5,'CC','XF XF xt','2017-02-22 14:05:00','0000-00-00 00:00:00',0,0),(37,0,1,4,'tes gelas','kirim gelas','2017-02-22 14:16:20','0000-00-00 00:00:00',0,0),(38,0,1,4,'saya kena tilang','polisinya minta uang.tolong dong','2017-02-22 14:20:56','0000-00-00 00:00:00',0,0),(39,0,2,5,'tes','teeees','2017-02-22 15:02:30','0000-00-00 00:00:00',0,0),(40,0,1,4,'tes sepatu','sepatu','2017-02-22 15:10:35','0000-00-00 00:00:00',0,0),(41,0,1,4,'gdhdhd','chdhdhd','2017-02-28 12:07:36','0000-00-00 00:00:00',0,0),(42,0,2,0,'jJjidudud','jajajsjzjsjzizi','2017-03-01 14:57:24','0000-00-00 00:00:00',0,0),(43,0,1,0,'hfjdhd','bdhdh','2017-03-15 22:02:24','0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `tb_pesan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_sessid`
--

DROP TABLE IF EXISTS `tb_sessid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sessid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessid` text,
  `tabel` varchar(10) DEFAULT NULL,
  `idt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_sessid`
--

LOCK TABLES `tb_sessid` WRITE;
/*!40000 ALTER TABLE `tb_sessid` DISABLE KEYS */;
INSERT INTO `tb_sessid` VALUES (132,'8c4d27abfa2f8e30c456fecb4e5bd2ccba3541a5298a9c4808e3ad7f9265f2afb85140cc9af17af350fa50fb2e7eebcf8e07831b15793f291f17b8263637c3fc','klien',5),(147,'296194564d2d120dab18c13ea78b254b447650a83137a220576476c420d3310e92c52864a49c6c62c735d2bbfc096d9381181ae14cd3e2db1225614617cff6da','lawyer',4),(153,'8188a2ebcfbb441245eac9310077ddd9fd89d402b1f4f3e8d9d9e430ef94342cfe8d6a39be2aa89873a35003e7e34970841e297015a7f627aefa62625db6ea0c','lawyer',5),(154,'1892ca101eca90d7602c9d4e729198d57671b24b561a29ddea2fcf69a11c4ae918bdd01e009b2d8ea2a95d9efe72e7149c14bc06657da37567450d5ffdb4e72a','klien',1),(164,'c0abe76495480e57d25c62fe0ce1f6995eee81a9a248d0e20f592b5a11f63ff454b76cdbe54ed598c7f5e6bb1154440244be369c894b7970a60f37b16942651a','klien',15),(172,'ae8880146a80a7b8332e2bfbfc5918c57995dbc95c926ff49b2d94eb1d5dee851522d0dbeb0394263e9e84dbdc83c9910b6d3a48a016cf4895c95970b61c4ceb','klien',14),(174,'13e8592ff44b3835e9259e7ddace91a1011d81bf9d6f4f25c019d3b625a60cfb2971c94f81ce31617de5e2a8f099036f2471ef330ae77b1a1c54865d4691f496','klien',2);
/*!40000 ALTER TABLE `tb_sessid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_topup`
--

DROP TABLE IF EXISTS `tb_topup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_topup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `idk` int(11) DEFAULT NULL,
  `saldosebelumnya` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `buktitransfer` varchar(30) DEFAULT NULL,
  `komenklien` text,
  `komenpusat` text,
  `idmt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_topup`
--

LOCK TABLES `tb_topup` WRITE;
/*!40000 ALTER TABLE `tb_topup` DISABLE KEYS */;
INSERT INTO `tb_topup` VALUES (1,'2017-05-02 14:37:23',200000,1,500000,0,NULL,NULL,NULL,0),(2,'2017-02-18 06:05:04',200000,2,500000,3,NULL,'Sudah saya  kirim','tes',1),(3,'2017-03-18 06:05:04',200000,1,500000,1,NULL,NULL,NULL,1),(4,'2017-04-18 06:05:04',200000,1,500000,2,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `tb_topup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '0',
  `filefoto` varchar(30) NOT NULL DEFAULT 'fotodefault.png',
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `salt` varchar(20) NOT NULL,
  `sessid` text,
  `status` int(1) DEFAULT '1',
  `idr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,1,'Cecep','cecepsuhaja@yahoo.com','','fotodefault.png','cecep','fd998c35e8595229e2e798bf6059467ab3c281a3b022289db760317a0fe1b50b423701a880576927d0029fcc0524af416e6627494c324333077e89945bb16b03','garammanis373','',1,1),(2,1,'Tomas','tomas@yahoo.com','08543333','fotodefault.png','tomas','715a1a709e287716ee7626b5a37bfd05dcbe7eeee20178b2f42709f731de8df7e68877f8f31025c396929979c18f5b42401e74f9fb1877353c5a74ef8fd68100','garammanis291','e5441e08ef07f08aa7d2da4df49f120f38bab4c78ffb706e214a1a54f36d506f4371fce495d015bf0ad67403ddfbaac1b37861ac372ab27d94c466293c1893bd',0,2),(3,2,'Ucok Salibatae','ucok_salibatea@yahoo.com','222333','fotolawyer4.jpg','la_ucok','d29b199143d89ea2b0c5d7b336b2aa29400eac9fe3ff04e475503b53f27875235abcdfed04ae42de98809b428e17fdba5a0243a8410d59d988f8cc6348ceca5d','garammanis542','',1,4),(4,2,'Otong','otong@yahoo.com','6287886820318','fotolawyer4.jpg','la_otong','606a2da87ddf10e9b0548fa3ee16e97bc94b81aed529e2602572830152e461f13326ef00d486eddd5b37836167abe07fbd0049cb1cd6308beb5b301970b914c5','garammanis342','8c4d27abfa2f8e30c456fecb4e5bd2ccba3541a5298a9c4808e3ad7f9265f2afb85140cc9af17af350fa50fb2e7eebcf8e07831b15793f291f17b8263637c3fc',1,5),(5,2,'Taji M. Sianturi','taji@yahoo.com','0','fotolawyer7.png','la_taji','e77556639111db3158bb6c43af3bcb921a687a649cb2618b3717def65bb3bca0939c9024291307c0404005ea26caea23f185d61103f69308b84b029fc535c888','garammanis987','',1,7),(6,2,'Todung Mulya Lubis','todung@yahoo.com','53444','fotolawyer8.png','la_todung','63001211f547d15a2c486aba3421c8280622e78a7533e31a26694aa5e266fee6f78ad0f5545be86c2ca70dfda94f50618c22e90d469066c9bae6af1ffaef7e59','garammanis854','',1,8),(7,2,'Juniver Girsang','juniver@yahoo.com','0','fotolawyer9.png','la_juniver','39c23206e06999a08a1bec38a55f85788940c9b91b781d4991ca224e67a2c5a6aadfa757afc404b8c09fed435166658e36ba0a62d3e16f959e8402430cc2d1b1','garammanis685','',1,9),(8,2,'Elza Syarief','elza@yahoo.com','0','fotolawyer10.png','la_elza','b686b2670304470fcbcd20475cf43547b489303d275f95d69c828c335bb91d1cc6ecc14d33ecf95baf6ee1db4bf574de21816f3c4a5a881068be8f75137465b3','garammanis637','',1,10),(9,6,'Bos Admin','bosadmin@yahoo.com','088655332','fotodefault.png','bosadmin','24b596058c1c7fc2f517cdecf19b2d74f5234cd348acd6bccd751a7d5707fcf6047553a6e6b0fbb856bb29ab7e83f6a8c49bc7f8f2602bae6c3db4a17ad21380','garammanis587',NULL,1,NULL),(12,2,'Sutejo','sutejo@yahoo.com','0876877777','fotolawyer11.jpg','sutejo','97558539e7e582bd3a44f3e62e33ab8e8593ec3b1edb413a68d90fa2f16267547357d1a72fc8cbdcd1a1bba4d6c30902715cc6546f239267cfff959108a0eb7e','garammanis804',NULL,1,11),(13,2,'Alibi','alibi@yahoo.com','065443333','fotolawyer12.jpg','alibi','47f511fea5eac9b2b5acc303f26d1caf4a697314fb419f08817ed46de0ed6cbb1f9c4e229c92068ba66b1c810c7e28667f1c62c439973cdea5ca86db7da4e175','garammanis711',NULL,1,12),(15,3,'Hardiansyah','hardi@yahoo.com','08773332','fotodefault.png','cshardi','a9d77b32af58cdf32add9b8638484af7c3f85a9b8d1ace2944a40341208b7fb11b5146fa9781f856f75b7aeadba43ff874621548355bb9ba99753509b6475fa2','garammanis798',NULL,1,1),(16,3,'Joko','joko@yahoo.com','0823333','fotodefault.png','joko','e1ebd1e18238d6f52787c7ee637f3932713c330f948f4cfaea20d5126549499e74f900c708eb9040e2164f91e996a708783ede3c2309a618c09d4c35a539e547','garammanis91',NULL,1,3),(17,4,'Fumi','fumi@yahoo.com','085423333','fotodefault.png','fumi','0b9bbadc6536c61a51eb4d8a818dedc4647582cbf4109e8fe0f4ec52e47722233dee079e0b635b707131387f5e9da145583d32e39d333d748f8a95a384798768','garammanis204',NULL,1,NULL),(18,5,'Sofian','sofian@yahoo.com','08543333222','fotodefault.png','sofian','5ef189b1f637a14c883dfcb0a2ba21c05560d7ccf1ea88d16e7fd5031fcb049d0ebeb7ddc75870ae115e44db3f2477250839d80227fb851ad3d35c550d1e38ae','garammanis810',NULL,1,NULL);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_withdraw`
--

DROP TABLE IF EXISTS `tb_withdraw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idl` int(11) DEFAULT NULL,
  `tanggalminta` datetime DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `komenlawyer` text,
  `komenpusat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_withdraw`
--

LOCK TABLES `tb_withdraw` WRITE;
/*!40000 ALTER TABLE `tb_withdraw` DISABLE KEYS */;
INSERT INTO `tb_withdraw` VALUES (1,5,'2017-05-04 00:03:31',500000,0,NULL,NULL);
/*!40000 ALTER TABLE `tb_withdraw` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-04 12:12:57
