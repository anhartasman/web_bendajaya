-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: db_testiket
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_airport`
--

DROP TABLE IF EXISTS `tb_airport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_airport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` char(30) DEFAULT NULL,
  `kode` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_airport`
--

LOCK TABLES `tb_airport` WRITE;
/*!40000 ALTER TABLE `tb_airport` DISABLE KEYS */;
INSERT INTO `tb_airport` VALUES (1,'Abu Dhabi, Uni Emirat Arab','AUH'),(2,'Alor','ARD'),(3,'Ambon','AMQ'),(4,'Ampana','AMP'),(5,'Amsterdam, Belanda','AMS'),(6,'Atambua','ABU'),(7,'Atlanta, Amerika Serikat','ATL'),(8,'Auckland, Selandia Baru','AKL'),(9,'Bajawa','BJW'),(10,'Bali','DPS'),(11,'Balikpapan','BPN'),(12,'Banda Aceh','BTJ'),(13,'Bandar Lampung','TKG'),(14,'Bandung','BDO'),(15,'Bangkok','BKK'),(16,'Bangkok, Thailand','DMK'),(17,'Banjarmasin','BDJ'),(18,'Banyuwangi','BWX'),(19,'Banyuwangi','DQJ'),(20,'Batam','BTH'),(21,'Baubau','BUW'),(22,'Beijing, China','PEK'),(23,'Bengkulu','BKS'),(24,'Berau','BEJ'),(25,'Biak','BIK'),(26,'Bima','BMU'),(27,'Bone','BNE'),(28,'Brunei Darussalam','BWN'),(29,'Bua','LLO'),(30,'Buli','WUB'),(31,'Buntok','BTK'),(32,'Buol','UOL'),(33,'Cairo','CAI'),(34,'Canton China','CAN'),(35,'Chiang Mai','CNX'),(36,'Darwin','DRW'),(37,'Dekai','DKI'),(38,'Dili','DIL'),(39,'Doha, Qatar','DOH'),(40,'Dubai, Uni Emirat Arab','DXB'),(41,'Dumai','DUM'),(42,'Ende','ENE'),(43,'Fak Fak','FKQ'),(44,'Galela','GLX'),(45,'Gold Coast','OOL'),(46,'Gorontalo','GTO'),(47,'Gunung Sitoli','GNS'),(48,'Ho Chi Minh City','SGN'),(49,'Hong Kong','HKG'),(50,'Istanbul, Turki','IST'),(51,'Jakarta','HLP'),(52,'Jakarta','CGK'),(53,'Jambi','DJB'),(54,'Jayapura','DJJ'),(55,'Jeddah','JED'),(56,'Jember','JBB'),(57,'Jogjakarta','JOG'),(58,'Johor Bahru','JHB'),(59,'Kaimana','KNG'),(60,'Kalimantan Barat','PSU'),(61,'Kalimantan Barat','SQG'),(62,'Kendari','KDI'),(63,'Ketapang','KTG'),(64,'Kota Baru','KBU'),(65,'Kota Kinabalu','BKI'),(66,'Kuala Kurun','KLK'),(67,'Kuala Lumpur','KUL'),(68,'Kuala Pembuang','KLP'),(69,'Kupang','KOE'),(70,'Labuan Bajo','LBJ'),(71,'Labuha','LAH'),(72,'Larantuka','LKA'),(73,'Lewoleba','LWE'),(74,'Lhokseumawe','LSW'),(75,'London','LHR'),(76,'Los Angeles, Amerika Serikat','LAX'),(77,'Lubuk Linggau','LLG'),(78,'Luwuk','LUW'),(79,'Madrid, Spanyol','MAD'),(80,'Makasar','UPG'),(81,'Malacca','MKSZ'),(82,'Malang','MLG'),(83,'Mamasa','MSA'),(84,'Mamuju','MJU'),(85,'Manado','MDC'),(86,'Manila, Thailand','MNL'),(87,'Manis Mata','MTA'),(88,'Manokwari','MKW'),(89,'Masamba','MXB'),(90,'Mataram','LOP'),(91,'Maumere','MOF'),(92,'Medan','KNO'),(93,'Medan','MES'),(94,'Melanguane','MNA'),(95,'Melbourne','MEL'),(96,'Merauke','MKQ'),(97,'Meulaboh','MEQ'),(98,'Muaro Bungo','MRB'),(99,'Nabire','NBX'),(100,'Nangapinoh','NPO'),(101,'Natuna Ranai','NTX'),(102,'New Delhi, India','DEL'),(103,'Nunukan','NNX'),(104,'Osaka','KIX'),(105,'Padang','PDG'),(106,'Pagar Alam','DPM'),(107,'Palangkaraya','PKY'),(108,'Palembang','PLM'),(109,'Palu','PLW'),(110,'Pangkal Pinang','PGK'),(111,'Pangkalan Bun','PKN'),(112,'Pekanbaru','PKU'),(113,'Penang','PEN'),(114,'Perth','PER'),(115,'Pomalaa','PUM'),(116,'Pontianak','PNK'),(117,'Porto, Potugal','OPO'),(118,'Poso','PSJ'),(119,'Puruk Cahu','PCU'),(120,'Puruk Cahu','PCU'),(121,'Raha','RAQ'),(122,'Rampi','RPI'),(123,'Rote','RTI'),(124,'Ruteng','RTG'),(125,'Sabang','SBG'),(126,'Samarinda','SRI'),(127,'Sampit','SMQ'),(128,'Sangata','SGQ'),(129,'Sarawak, Malaysia','KCH'),(130,'Saumlaki','SXK'),(131,'Seko','SKO'),(132,'Selayar','SLY'),(133,'Semarang','SRG'),(134,'Seoul, Korea Selatan','ICN'),(135,'Sibolga','FLZ'),(136,'Silangit','DTB'),(137,'Singapore','SIN'),(138,'Singkep','SIQ'),(139,'Solo','SOC'),(140,'Sorong','SOQ'),(141,'Subang Selangor, Malaysia','SZB'),(142,'Sumbawa','SWQ'),(143,'Surabaya','SUB'),(144,'Sydney','SYD'),(145,'Tahuna','NAH'),(146,'Tambolaka','TMC'),(147,'Tana Toraja','TTR'),(148,'Tana Toraja','TTR'),(149,'Tanjung Pandan','TJQ'),(150,'Tanjung Pinang','TNJ'),(151,'Tanjung Selor','TJS'),(152,'Tarakan','TRK'),(153,'Ternate','TTE'),(154,'Timika','TIM'),(155,'Timika','TIM'),(156,'Tobelo','KAZ'),(157,'Tokyo, Jepang','HND'),(158,'Tokyo, Jepang','NRT'),(159,'Tolitoli','TLI'),(160,'Tual','LUV'),(161,'Tumbang Samba','TSB'),(162,'Waingapu','WGP'),(163,'Wakatobi','WKB'),(164,'Wakatobi','WNI'),(165,'Wamena','WMX');
/*!40000 ALTER TABLE `tb_airport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_akun_admin`
--

DROP TABLE IF EXISTS `tb_akun_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_akun_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(10) NOT NULL,
  `password` varchar(10) DEFAULT NULL,
  `nama` text,
  `level` int(11) DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_akun_admin`
--

LOCK TABLES `tb_akun_admin` WRITE;
/*!40000 ALTER TABLE `tb_akun_admin` DISABLE KEYS */;
INSERT INTO `tb_akun_admin` VALUES (1,'udin','udin123','admin udinaa',3,'2016-09-13 10:17:30');
/*!40000 ALTER TABLE `tb_akun_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_buktipembayaran_airline`
--

DROP TABLE IF EXISTS `tb_buktipembayaran_airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_buktipembayaran_airline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idt` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `rekasal_napem` text,
  `rekasal_norek` char(30) DEFAULT NULL,
  `rekasal_bank` char(20) DEFAULT NULL,
  `rektuju_bank` char(30) DEFAULT NULL,
  `rektuju_norek` char(30) DEFAULT NULL,
  `rektuju_napem` text,
  `jumlahbayar` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `tanggalbayar` date DEFAULT NULL,
  `namafile` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_buktipembayaran_airline`
--

LOCK TABLES `tb_buktipembayaran_airline` WRITE;
/*!40000 ALTER TABLE `tb_buktipembayaran_airline` DISABLE KEYS */;
INSERT INTO `tb_buktipembayaran_airline` VALUES (1,1,NULL,'Udin Joni','BCA',NULL,NULL,NULL,NULL,356000,NULL,'2016-02-20',''),(2,1,NULL,'Hudini','BRI',NULL,NULL,NULL,NULL,356000,NULL,'2016-02-20','buktipembayaranAIR1000004-16090106752.png'),(3,2,NULL,'Udin Joni','BCA',NULL,NULL,NULL,NULL,356000,NULL,'2016-02-20','buktipembayaranAIR1000004-16091019433.png'),(4,3,NULL,'Hudini','BCA',NULL,NULL,NULL,NULL,356000,NULL,'0000-00-00','buktipembayaranAIR1000004-16091124274.png'),(5,4,NULL,'Hudini','BRI',NULL,NULL,NULL,NULL,356000,NULL,'0000-00-00','buktipembayaranAIR1000004-16091135675.png'),(6,5,NULL,'Hudini','BCA',NULL,NULL,NULL,NULL,356000,NULL,'0000-00-00','buktipembayaranAIR1000004-16091141456.png'),(7,6,NULL,'Hudini','BRI',NULL,NULL,NULL,NULL,356000,NULL,'0000-00-00','buktipembayaranAIR1000004-16091159447.png'),(8,3,NULL,'Hudini','BRI',NULL,NULL,NULL,NULL,356000,NULL,'2016-09-22','buktipembayaranAIR1000004-16091124278.png'),(9,7,NULL,'Udin Joni','BCA',NULL,NULL,NULL,NULL,356000,NULL,'2016-09-14','buktipembayaranAIR1000004-16091167469.png'),(10,8,0,'Usep','83473434','Mandiri','BCA','544332233344','Udin Joni',45000,NULL,'2016-09-15','buktipembayaranAIR1000004-160912726010.png'),(11,9,1,'Sutoyoaaa','BCA','BCAza','BCA','544332233344','Udin Joni',0,NULL,'2016-09-21','buktipembayaranAIR1000004-160912896011.png'),(12,9,0,'Hudiniaa','BRI','Mandiri','BCA','544332233344','Udin Joni',0,NULL,'2016-09-29','buktipembayaranAIR1000004-160912896012.png'),(13,9,NULL,'Hudini','BRI','Mandiri','BCA','544332233344','Udin Joni',0,NULL,'2016-09-29',''),(14,9,NULL,'Hudini','BRI','Mandiri','BCA','544332233344','Udin Joni',0,NULL,'2016-09-29','buktipembayaranAIR1000004-160912896014.png'),(15,9,1,'Hudiniaa','BRI','BCAza','BCA','544332233344','Udin Joni',59888,NULL,'2016-09-30','buktipembayaranAIR1000004-160912896015.png'),(16,10,1,'Hudiniaa','BRI','Mandiri','BCA','544332233344','Udin Joni',5984623,NULL,'2016-09-15','buktipembayaranAIR1000004-160913954216.png'),(17,11,NULL,'Surya Paloh','02837347343','Mandiri','BCA','544332233344','Udin Joni',2658999,NULL,'2016-09-29','buktipembayaranAIR1000004-1609131021017.png'),(18,12,1,'Hudiniaa','02837347343','Mandiri','BCA','544332233344','Udin Joni',7896521,NULL,'2016-09-22','buktipembayaranAIR1000004-1609131170318.png'),(19,1,NULL,'Hudini','02837347343','Mandiri','BCA','544332233344','Udin Joni',565888,NULL,'2016-09-16','buktipembayaranAIR1000004-160911013019.png');
/*!40000 ALTER TABLE `tb_buktipembayaran_airline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_penumpang_airline`
--

DROP TABLE IF EXISTS `tb_penumpang_airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_penumpang_airline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idt` int(11) DEFAULT NULL,
  `kat` int(11) DEFAULT NULL,
  `tit` char(10) DEFAULT NULL,
  `fn` char(15) DEFAULT NULL,
  `ln` char(15) DEFAULT NULL,
  `hp` char(15) DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `nat` char(10) DEFAULT NULL,
  `passno` char(20) DEFAULT NULL,
  `passnat` char(10) DEFAULT NULL,
  `passenddate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_penumpang_airline`
--

LOCK TABLES `tb_penumpang_airline` WRITE;
/*!40000 ALTER TABLE `tb_penumpang_airline` DISABLE KEYS */;
INSERT INTO `tb_penumpang_airline` VALUES (1,1,1,'MR','Sam','Wincester','0875453344',NULL,NULL,NULL,NULL,NULL),(2,2,1,'MR','Shinta','Rahna','92387434344',NULL,NULL,NULL,NULL,NULL),(3,3,1,'MR','Bejo','Udin','023823723',NULL,NULL,NULL,NULL,NULL),(4,4,1,'MR','Gumelar','Karta','0238832433',NULL,NULL,NULL,NULL,NULL),(5,5,1,'MR','Mulyani','Ningrum','0348348833',NULL,NULL,NULL,NULL,NULL),(6,6,1,'MR','Kamirul','Hakim','08763444333',NULL,NULL,NULL,NULL,NULL),(7,7,1,'MR','Bedu','Sarjono','0988232223',NULL,NULL,NULL,NULL,NULL),(8,8,1,'MR','Firman','Sentosa','0876633223',NULL,NULL,NULL,NULL,NULL),(9,9,1,'MR','Rudi','Sentosa','083435454555',NULL,NULL,NULL,NULL,NULL),(10,10,1,'MR','Darul','Muftah','03938478333',NULL,NULL,NULL,NULL,NULL),(11,11,1,'MR','Karmila','Putar','0384738387472',NULL,NULL,NULL,NULL,NULL),(12,12,1,'MR','Danang','Danuarta','083473433',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_penumpang_airline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_rekening`
--

DROP TABLE IF EXISTS `tb_rekening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_rekening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` char(20) DEFAULT NULL,
  `norek` char(20) DEFAULT NULL,
  `napem` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_rekening`
--

LOCK TABLES `tb_rekening` WRITE;
/*!40000 ALTER TABLE `tb_rekening` DISABLE KEYS */;
INSERT INTO `tb_rekening` VALUES (2,'BCA','544332233344','Udin Joni'),(3,'Mandiri','83737383','Sutoyo');
/*!40000 ALTER TABLE `tb_rekening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_style_color`
--

DROP TABLE IF EXISTS `tb_style_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_style_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` char(30) DEFAULT NULL,
  `valuehtml` char(20) DEFAULT NULL,
  `avb` int(11) DEFAULT NULL,
  `stat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_style_color`
--

LOCK TABLES `tb_style_color` WRITE;
/*!40000 ALTER TABLE `tb_style_color` DISABLE KEYS */;
INSERT INTO `tb_style_color` VALUES (1,'theme-1','theme-1',1,0),(2,'theme-2','theme-2',1,0),(3,'theme-3','theme-3',1,0),(4,'theme-4','theme-4',1,0);
/*!40000 ALTER TABLE `tb_style_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_transaksi_airline`
--

DROP TABLE IF EXISTS `tb_transaksi_airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_transaksi_airline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mmid` char(20) DEFAULT NULL,
  `acDep` char(5) DEFAULT NULL,
  `acRet` char(5) DEFAULT NULL,
  `org` char(10) DEFAULT NULL,
  `des` char(10) DEFAULT NULL,
  `daftran_dep` text,
  `daftran_ret` text,
  `flight` char(1) DEFAULT NULL,
  `tgl_dep` datetime DEFAULT NULL,
  `tgl_ret` datetime DEFAULT NULL,
  `adt` char(1) DEFAULT NULL,
  `chd` char(1) DEFAULT NULL,
  `inf` char(1) DEFAULT NULL,
  `selectedIDdep` char(20) DEFAULT NULL,
  `selectedIDret` char(20) DEFAULT NULL,
  `PNRDep` char(50) DEFAULT NULL,
  `PNRRet` char(50) DEFAULT NULL,
  `cpname` char(20) DEFAULT NULL,
  `cpmail` char(50) DEFAULT NULL,
  `cptlp` char(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `catatan` text,
  `notrx` char(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  `kodeunik` int(11) DEFAULT NULL,
  `tgl_bill_create` datetime DEFAULT NULL,
  `tgl_bill_exp` datetime DEFAULT NULL,
  `tgl_bill_acc` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_transaksi_airline`
--

LOCK TABLES `tb_transaksi_airline` WRITE;
/*!40000 ALTER TABLE `tb_transaksi_airline` DISABLE KEYS */;
INSERT INTO `tb_transaksi_airline` VALUES (1,'1000004','SJ',NULL,'CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"SJ592\",\"STD\":\"CGK\",\"ETD\":\"2016-09-12 09:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-12 12:20\"},{\"FlightNo\":\"SJ788\",\"STD\":\"UPG\",\"ETD\":\"2016-09-13 02:30\",\"STA\":\"MKW\",\"ETA\":\"2016-09-13 06:20\"}]}',NULL,'O','2016-09-12 09:05:00','1970-01-01 07:00:00','1','0','0','FARED696',NULL,NULL,NULL,'John Wincester','john_super@natural.com','05234442',3,'','AIR1000004-1609110130',1930000,727,'2016-09-11 09:38:13','2016-09-11 10:47:17','2016-09-13 09:59:09'),(2,'1000004','GA',NULL,'CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-14 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-15 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-15 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-15 08:00\"}]}',NULL,'O','2016-09-14 21:00:00','1970-01-01 07:00:00','1','0','0','FARE699,FARE700',NULL,NULL,NULL,'Shinta Rahmadanti','saguta@yahoo.com','08723233',0,NULL,'AIR1000004-1609111451',3052000,522,'2016-09-11 10:47:59','2016-09-11 11:47:59',NULL),(3,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-11 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-12 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-12 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-12 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-21 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-21 08:35\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-21 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-21 15:15\"}]}','R','2016-09-11 00:00:00','2016-09-21 00:00:00','1','0','0','FARE709,FARE710','FARED711',NULL,NULL,'Tora Sudiro','tora@vaganza.com','0823384384',2,NULL,'AIR1000004-1609112427',5001000,400,'2016-09-11 02:56:56','2016-09-11 03:56:56',NULL),(4,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-11 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-12 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-12 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-12 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-21 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-21 08:35\"},{\"FlightNo\":\"SJ593\",\"STD\":\"UPG\",\"ETD\":\"2016-09-21 13:20\",\"STA\":\"CGK\",\"ETA\":\"2016-09-21 14:25\"}]}','R','2016-09-11 00:00:00','2016-09-21 00:00:00','1','0','0','FARE712,FARE713','FARED714',NULL,NULL,'Ajeng Srukanti','ajeng@yahoo.com','0837437434',4,NULL,'AIR1000004-1609113567',5001000,435,'2016-09-11 15:13:22','2016-09-11 16:13:22',NULL),(5,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-12 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-13 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-13 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-13 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ571\",\"STD\":\"MKW\",\"ETD\":\"2016-09-25 09:30\",\"STA\":\"UPG\",\"ETA\":\"2016-09-25 12:00\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-25 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-25 15:15\"}]}','R','2016-09-12 00:00:00','2016-09-25 00:00:00','1','0','0','FARE715,FARE716','FARED717',NULL,NULL,'Wahyu Muhyani','muhyani@yahoo.com','02334349',4,NULL,'AIR1000004-1609114145',5925000,399,'2016-09-11 15:22:07','2016-09-11 16:22:07',NULL),(6,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-11 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-12 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-12 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-12 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-21 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-21 08:35\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-21 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-21 15:15\"}]}','R','2016-09-11 00:00:00','2016-09-21 00:00:00','1','0','0','FARE718,FARE719','FARED720',NULL,NULL,'Nurman Kamarul','nurman_kamarul@yahoo.com','0834787773',2,NULL,'AIR1000004-1609115944',5001000,18,'2016-09-11 15:52:54','2016-09-11 16:52:54',NULL),(7,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-12 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-13 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-13 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-13 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-21 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-21 08:35\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-21 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-21 15:15\"}]}','R','2016-09-12 00:00:00','2016-09-21 00:00:00','1','0','0','FARE721,FARE722','FARED723',NULL,NULL,'Manural Saral','manural@yahoo.com','03838384348',2,NULL,'AIR1000004-1609116746',5463000,86,'2016-09-11 16:29:02','2016-09-11 17:29:02',NULL),(8,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-12 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-13 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-13 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-13 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ571\",\"STD\":\"MKW\",\"ETD\":\"2016-09-25 09:30\",\"STA\":\"UPG\",\"ETA\":\"2016-09-25 12:00\"},{\"FlightNo\":\"SJ589\",\"STD\":\"UPG\",\"ETD\":\"2016-09-25 16:20\",\"STA\":\"CGK\",\"ETA\":\"2016-09-25 17:25\"}]}','R','2016-09-12 00:00:00','2016-09-25 00:00:00','1','0','0','FARE732,FARE733','FARED734',NULL,NULL,'Firman','firman_sentosa@yahoo.com','0876633223',4,NULL,'AIR1000004-1609127260',5925000,686,'2016-09-12 00:01:51','2016-09-12 01:01:51','2016-09-13 06:49:23'),(9,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-20 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-21 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-21 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-21 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-28 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-28 08:35\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-28 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-28 15:15\"}]}','R','2016-09-20 00:00:00','2016-09-28 00:00:00','1','0','0','FARE749,FARE750','FARED751',NULL,NULL,'Rudi Sentosa','rudi_sentosa@yahoo.com','0893483434',2,NULL,'AIR1000004-1609128960',5743000,549,'2016-09-12 23:11:22','2016-09-13 00:11:22',NULL),(10,'1000004','GA','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"GA650\",\"STD\":\"CGK\",\"ETD\":\"2016-09-14 21:00\",\"STA\":\"UPG\",\"ETA\":\"2016-09-15 00:30\"},{\"FlightNo\":\"GA698\",\"STD\":\"UPG\",\"ETD\":\"2016-09-15 03:15\",\"STA\":\"MKW\",\"ETA\":\"2016-09-15 08:00\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ789\",\"STD\":\"MKW\",\"ETD\":\"2016-09-24 07:15\",\"STA\":\"UPG\",\"ETA\":\"2016-09-24 08:45\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-24 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-24 15:15\"}]}','R','2016-09-14 00:00:00','2016-09-24 00:00:00','1','0','0','FARE778,FARE779','FARED780',NULL,NULL,'Darul Muftah','darul_muftah@yahoo.com','83738378373',2,NULL,'AIR1000004-1609139542',5309000,278,'2016-09-13 08:42:46','2016-09-13 09:42:46','2016-09-13 08:52:44'),(11,'1000004','SJ','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"SJ592\",\"STD\":\"CGK\",\"ETD\":\"2016-09-15 09:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-15 12:20\"},{\"FlightNo\":\"SJ584\",\"STD\":\"UPG\",\"ETD\":\"2016-09-16 02:30\",\"STA\":\"MKW\",\"ETA\":\"2016-09-16 06:20\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ789\",\"STD\":\"MKW\",\"ETD\":\"2016-09-15 07:15\",\"STA\":\"UPG\",\"ETA\":\"2016-09-15 08:45\"},{\"FlightNo\":\"SJ581\",\"STD\":\"UPG\",\"ETD\":\"2016-09-15 14:10\",\"STA\":\"CGK\",\"ETA\":\"2016-09-15 15:15\"}]}','R','2016-09-15 00:00:00','2016-09-15 00:00:00','1','0','0','FARED781','FARED782','UKPJBW','UKPJBW','Karmila Putar','karmila_putar@putarindong.com','09383483433',2,NULL,'AIR1000004-16091310210',4888000,225,'2016-09-13 09:03:24','2016-09-13 10:03:24','2016-09-13 09:06:01'),(12,'1000004','SJ','SJ','CGK','MKW','{\"jalurpergi\":[{\"FlightNo\":\"SJ580\",\"STD\":\"CGK\",\"ETD\":\"2016-09-16 21:45\",\"STA\":\"UPG\",\"ETA\":\"2016-09-17 01:00\"},{\"FlightNo\":\"SJ788\",\"STD\":\"UPG\",\"ETD\":\"2016-09-17 02:30\",\"STA\":\"MKW\",\"ETA\":\"2016-09-17 06:20\"}]}','{\"jalurpulang\":[{\"FlightNo\":\"SJ585\",\"STD\":\"MKW\",\"ETD\":\"2016-09-26 07:05\",\"STA\":\"UPG\",\"ETA\":\"2016-09-26 08:35\"},{\"FlightNo\":\"SJ589\",\"STD\":\"UPG\",\"ETD\":\"2016-09-26 16:20\",\"STA\":\"CGK\",\"ETA\":\"2016-09-26 17:25\"}]}','R','2016-09-16 00:00:00','2016-09-26 00:00:00','1','0','0','FARED783','FARED784','UOBSRL','UOBSRL','Danang Danuarta','danang_danuarta@yahoo.com','098766663',2,NULL,'AIR1000004-16091311703',5606000,91,'2016-09-13 09:12:00','2016-09-13 10:12:00','2016-09-13 09:12:44');
/*!40000 ALTER TABLE `tb_transaksi_airline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_blog`
--

DROP TABLE IF EXISTS `tb_webcont_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `judul` text,
  `isi` text,
  `kategori` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_blog`
--

LOCK TABLES `tb_webcont_blog` WRITE;
/*!40000 ALTER TABLE `tb_webcont_blog` DISABLE KEYS */;
INSERT INTO `tb_webcont_blog` VALUES (1,'2016-08-17 10:50:38','haha','huhu','cabe'),(2,'2016-08-18 00:51:48','hehe','testes','tes'),(3,'2016-07-17 17:00:00','hehe3','testes','tes'),(4,'2015-09-15 17:00:00','hehe4','testes','tes'),(5,'2016-08-17 21:11:44','artikel','hehe',NULL),(7,'2016-08-17 21:34:48','sads','adsad',NULL),(8,'2016-08-18 04:17:59','sdfsdfsdfsdf','<p>sdfsdfdsf <strong>dsfsdfsdf</strong> sdfsdfsdf</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>sdfsdfsdfsdfsdfdsf</p>\r\n',NULL),(9,'2016-08-19 22:49:56','Lorem ipsum doloreee 6','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(10,'2016-08-19 22:49:49','Lorem ipsum doloreee 5','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(11,'2016-08-19 22:49:43','Lorem ipsum doloreee 4','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(12,'2016-08-19 23:50:08','Lorem ipsum doloreee 3','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n\r\n<p><img alt=\"\" src=\"http://2.bp.blogspot.com/-LqPb5E12SQ0/VpSJDA4SZJI/AAAAAAAAA5Y/56NYenFU6Gk/s1600/pegunungan-gunung-berapi-bali.jpg\" style=\"height:205px; width:308px\" /></p>\r\n\r\n<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n\r\n<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(13,'2016-08-19 22:49:27','Lorem ipsum doloreee 2','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(14,'2016-08-19 22:49:21','Lorem ipsum doloreee 1','<p>Lorem ipsum doloraa sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</p>\r\n',NULL),(15,'2016-08-20 20:11:27','artikel baru','<p>sdfdsf</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>dsfsdfsdf</p>\r\n','tes aja'),(16,'2016-08-20 20:15:20','asdsad','<p>sadaaaaaaaaaaaaaa</p>\r\n','tes aja');
/*!40000 ALTER TABLE `tb_webcont_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_bukutamu`
--

DROP TABLE IF EXISTS `tb_webcont_bukutamu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_bukutamu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` char(30) DEFAULT NULL,
  `email` char(40) DEFAULT NULL,
  `isi` text,
  `testi` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_bukutamu`
--

LOCK TABLES `tb_webcont_bukutamu` WRITE;
/*!40000 ALTER TABLE `tb_webcont_bukutamu` DISABLE KEYS */;
INSERT INTO `tb_webcont_bukutamu` VALUES (1,'amazing santorini','anhar_tasman@yahoo.com','asdsad\r\nsad\r\nasdasdsad',NULL),(2,'amazing santorini','anhar_tasman@yahoo.com','asdsad\r\nsad\r\nasdasdsad',1),(3,'amazing santorini 3','anhar_tasman3@yahoo.com','asdsad\r\nsad\r\nasdasdsad 3',1),(4,'Banner Kedua','pembisnisonline@yahoo.com','sadsa\r\ndsad\r\nsadsadsad',1),(5,'Foto Gunung','asdasdasd','sadas\r\ndsadasdasdsad',1),(6,'Pendaki','pendaki@yahoo.com','laporan cuaca',NULL),(7,'Pendaki 2','pendaki 2@yahoo.com','laporan cuaca 2',1);
/*!40000 ALTER TABLE `tb_webcont_bukutamu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_faq`
--

DROP TABLE IF EXISTS `tb_webcont_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` text,
  `jawaban` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_faq`
--

LOCK TABLES `tb_webcont_faq` WRITE;
/*!40000 ALTER TABLE `tb_webcont_faq` DISABLE KEYS */;
INSERT INTO `tb_webcont_faq` VALUES (2,'cara melakukan deposit','<p>asdsad sa</p>\r\n\r\n<p><strong>sa d</strong></p>\r\n\r\n<p>sadsadsad</p>\r\n'),(4,'tes faq 2','<p>ewtesgfsdfsdfsdf</p>\r\n');
/*!40000 ALTER TABLE `tb_webcont_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_galeri`
--

DROP TABLE IF EXISTS `tb_webcont_galeri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_galeri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `namafoto` char(50) DEFAULT NULL,
  `keterangan` text,
  `kategori` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_galeri`
--

LOCK TABLES `tb_webcont_galeri` WRITE;
/*!40000 ALTER TABLE `tb_webcont_galeri` DISABLE KEYS */;
INSERT INTO `tb_webcont_galeri` VALUES (5,'isigaleri5.jpg','asdadrraa','<h1>asdsads <strong>ad</strong> sadsad</h1>\r\n\r\n<p>asdsadasdsads</p>\r\n\r\n<p>adsa</p>\r\n\r\n<p>d</p>\r\n\r\n<p>sadsadasdasdasdasdasdsad</p>\r\n','hotels'),(7,'isigaleri7.jpg','Dengan kategori','<p>sdfsdfsd</p>\r\n\r\n<p>fsdfsd</p>\r\n\r\n<p>fsdfsdfsdfsdfsdf</p>\r\n','pemandangan'),(8,'isigaleri8.jpg','gununga','<p>isi gununga</p>\r\n','pemandangan');
/*!40000 ALTER TABLE `tb_webcont_galeri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_kontak`
--

DROP TABLE IF EXISTS `tb_webcont_kontak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_kontak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` char(30) DEFAULT NULL,
  `isi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_kontak`
--

LOCK TABLES `tb_webcont_kontak` WRITE;
/*!40000 ALTER TABLE `tb_webcont_kontak` DISABLE KEYS */;
INSERT INTO `tb_webcont_kontak` VALUES (1,'alamat','isi alamat'),(2,'handphone','isi handphone'),(3,'email','isi email'),(4,'googlemap','isi googlemapaaa'),(5,'deskripsi','isideskripsi');
/*!40000 ALTER TABLE `tb_webcont_kontak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_logoweb`
--

DROP TABLE IF EXISTS `tb_webcont_logoweb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_logoweb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filegambar` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_logoweb`
--

LOCK TABLES `tb_webcont_logoweb` WRITE;
/*!40000 ALTER TABLE `tb_webcont_logoweb` DISABLE KEYS */;
INSERT INTO `tb_webcont_logoweb` VALUES (1,'logoweb.png');
/*!40000 ALTER TABLE `tb_webcont_logoweb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_policies`
--

DROP TABLE IF EXISTS `tb_webcont_policies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` char(30) DEFAULT NULL,
  `isi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_policies`
--

LOCK TABLES `tb_webcont_policies` WRITE;
/*!40000 ALTER TABLE `tb_webcont_policies` DISABLE KEYS */;
INSERT INTO `tb_webcont_policies` VALUES (2,'Prosedur Umroh','<p>sdfsdf sdf sdf ds fsdfsdfsdfsdfsdf</p>\r\n'),(3,'Aturan Bisnis','<p>sdfsdfsdf sdfds fsdfdsf sdfsdfsdfsdfsdfsdfsdfsdf</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>sdf</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>fsdfsdfsdfsdf</p>\r\n');
/*!40000 ALTER TABLE `tb_webcont_policies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_services`
--

DROP TABLE IF EXISTS `tb_webcont_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `namaservis` char(30) DEFAULT NULL,
  `keteranganservis` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_services`
--

LOCK TABLES `tb_webcont_services` WRITE;
/*!40000 ALTER TABLE `tb_webcont_services` DISABLE KEYS */;
INSERT INTO `tb_webcont_services` VALUES (1,'gambarservis1.png','happy clients','tes tes');
/*!40000 ALTER TABLE `tb_webcont_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_slider`
--

DROP TABLE IF EXISTS `tb_webcont_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `namabanner` char(50) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_slider`
--

LOCK TABLES `tb_webcont_slider` WRITE;
/*!40000 ALTER TABLE `tb_webcont_slider` DISABLE KEYS */;
INSERT INTO `tb_webcont_slider` VALUES (1,'gambarbanner1.jpg','amazing santorini','Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi quis leo elementum.'),(2,'gambarbanner2.jpg','Banner Kedua','dfsdfsdfsdfsdf');
/*!40000 ALTER TABLE `tb_webcont_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_team`
--

DROP TABLE IF EXISTS `tb_webcont_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `namaanggota` char(50) DEFAULT NULL,
  `jabatananggota` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_team`
--

LOCK TABLES `tb_webcont_team` WRITE;
/*!40000 ALTER TABLE `tb_webcont_team` DISABLE KEYS */;
INSERT INTO `tb_webcont_team` VALUES (3,'fototeam3.jpg','marry dols','Manager of Tourism');
/*!40000 ALTER TABLE `tb_webcont_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_webcont_testimonials`
--

DROP TABLE IF EXISTS `tb_webcont_testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_webcont_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sumbertesti` text,
  `isitesti` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_webcont_testimonials`
--

LOCK TABLES `tb_webcont_testimonials` WRITE;
/*!40000 ALTER TABLE `tb_webcont_testimonials` DISABLE KEYS */;
INSERT INTO `tb_webcont_testimonials` VALUES (1,'Joko','Webnya bagus'),(3,'grrr','wewewewe');
/*!40000 ALTER TABLE `tb_webcont_testimonials` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-14 16:10:16
