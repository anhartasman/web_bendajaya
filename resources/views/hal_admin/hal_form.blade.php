@extends('layouts.masteradmin')
@section('didalamhead')
<link href="{{ URL::asset('vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endsection
@section('statheader')
<?php
$namamenu=$namamenudaricont;
$dafmenu=$dafmenudaricont;
 ?>
@endsection
@section('kontenweb')
<!-- Main content -->
@if($errors->has())
           @foreach ($errors->all() as $error)
           <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ $error }}
          </div>
          @endforeach
   @endif
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff"
                           data-loop="true"></i> {{$namatabel}}
                    </h3>
                    <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                </div>
                <div class="panel-body">
                    <!-- errors -->
                    <!--main content-->
                    <form id="form-validation" action="{{$form_action}}" method="post"
                          class="form-horizontal" enctype="multipart/form-data">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                @foreach ($elfil as $eelfil)
                                <?php $attr=$eelfil['attr']; ?>
                                <div class="form-group">
                                  @if(isset($eelfil["label"]))
                                <label class="col-md-4 control-label" for="nama">
                                    {{$eelfil["label"]}}
                                    @if(isset($attr['required']))<span class="text-danger">*</span>@endif
                                </label>
                                @endif
                                <?php

                                switch($eelfil["type"]){
                                  case "select":
                                  ?>
                                  <div class="col-md-6">
                                     {{Form::select('size', $eelfil['list'],$eelfil['catch'],$attr)}}

                                      </div>
                                 <?php
                                   break;
                                   case "file":
                                 ?>
                                 <div class="col-md-6">
                                 {{Form::file('size', $attributes = $attr)}}
                                  </div>
                                  <?php
                                    break;
                                    case "text":
                                  ?>
                                  <div class="col-md-6">
                                  {{Form::text('size',isset($attr['value'])? $attr['value'] : null,$attr)}}
                                   </div>
                                   <?php
                                     break;
                                     case "textarea":
                                   ?>
                                   <div class="col-md-6">
                                   {{Form::textarea('size',isset($attr['value'])? $attr['value'] : null,$attr)}}
                                    </div>
                                  <?php
                                  break;
                                  case "richtext":
                                  ?>
                                  <div class="col-md-6">
                                  {{Form::textarea('size',isset($attr['value'])? $attr['value'] : null,$attr)}}
                                   </div>
                                  <?php
                                  break;
                                  case "unik_fotoprofil":
                                  ?>
                                     <div class="col-md-6">
                                          <div class="fileinput fileinput-new" data-provides="fileinput">
                                              <div class="fileinput-new thumbnail"
                                                   style="width: 200px; height: 200px;">
                                                  <img src="{{ url('/') }}/uploads/images/{{isset($eelfil['src'])? $eelfil['src'] : "fotodefault.png"}}" alt="profile pic"
                                                       class="profile_pic">
                                              </div>
                                              <div class="fileinput-preview fileinput-exists thumbnail"
                                                   style="max-width: 200px; max-height: 200px;"></div>
                                                @if(! isset($attr['disabled']))
                                              <div>
                                                      <span class="btn btn-default btn-file">
                          <span class="fileinput-new">Select image</span>
                                                      <span class="fileinput-exists">Change</span>
                                                     {{Form::file('size', $attributes = $attr)}}
                                                      </span>
                                                  <a href="#" class="btn btn-danger fileinput-exists"
                                                     data-dismiss="fileinput">Remove</a>
                                              </div>
                                              @endif
                                          </div>
                                      </div>
                                      <?php
                                      break;
                                      case "img":
                                      ?>

                                         <div class="col-md-6">
                                           <img src="{{ url('/') }}/uploads/images/{{$eelfil['src']}}" alt="gambar"
                                                class="profile_pic" width="300" height="300">
                                          </div>
                                      <?php
                                        break;
                                        case "hidden":
                                      ?>
                                      {{Form::hidden('size',isset($attr['value'])? $attr['value'] : null,$attr)}}
                                      <?php
                                        break;
                                        case "checkbox":
                                      ?>
                                      <div class="col-md-6">
                                      {{Form::checkbox('size',isset($attr['value'])? $attr['value'] : null,false,$attr)}}
                                     </div>
                                <?php
                                   break;
                                }
                                 ?>
                             </div>
                                @endforeach


                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-4">
                              @if(isset($tipeform)&&$tipeform==3)
                              {{Form::submit('Update',array('class'=>'btn btn-effect-ripple btn-primary'))}}
                              {{Form::reset('Reset',array('id'=>'buttonreset','class'=>'btn btn-effect-ripple btn-default reset_btn'))}}
                              @elseif(isset($tipeform)&&$tipeform==4)
                              {{Form::submit('Delete',array('class'=>'btn btn-effect-ripple btn-primary'))}}
                              @elseif(isset($tipeform)&&$tipeform==5)
                              @else
                              {{Form::submit('Insert',array('class'=>'btn btn-effect-ripple btn-primary'))}}
                              {{Form::reset('Reset',array('id'=>'buttonreset','class'=>'btn btn-effect-ripple btn-default reset_btn'))}}
                              @endif

                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    <!--row end-->
    <!--rightside bar -->
    @include("hal_admin.inc_rightside")
    <div class="background-overlay"></div>
</section>
  @endsection


  @section('bagianfooter')

  <!-- end of global js -->
  <!-- begining of page level js -->
  <script src="{{ URL::asset('vendors/moment/js/moment.min.js') }}"></script>
  <script src="{{ URL::asset('vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/select2/js/select2.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
 <!-- end of page level js -->
   <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}">
 <!-- CK Editor -->
 <script src="{{ URL::asset('dist/js/ckeditor.js') }}"></script>
 <!-- Bootstrap WYSIHTML5 -->
 <script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
@if($butuhdialog==1)
 <div id="dialog">Proses data yang diinput?</div>
@endif
  <script>
  //$('#{{$attr['id']}}').select2({  tags:[],tokenSeparators: [",", " "]});

@if($butuhdialog==1)
      var sudahoke=0;
      $( "#dialog" ).hide();
      $('#form-validation').on('submit', function() {
      		dialogalert();
          return (sudahoke==1);
      });

      function dialogalert(){
        $( "#dialog" ).dialog({
            modal: true,
          dialogClass: "no-close",
          buttons: [
            {
              text: "OK",
              click: function() {
                $( this ).dialog( "close" );
                sudahoke=1;
                  $( "#form-validation" ).submit();
              }
            },
              {
                text: "Cancel",
                click: function() {
                  $( this ).dialog( "close" );
                    $( "#buttonreset" ).click();
                }
              }
          ]
        });
      }
@endif

@foreach ($elfil as $eelfil)
<?php $attr=$eelfil['attr']; ?>
@if(isset($eelfil['selek2']))
$('#{{$attr['id']}}').select2();
@elseif($eelfil['type']=="richtext")
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace('{{$attr['id']}}');
//bootstrap WYSIHTML5 - text editor
$("#{{$attr['id']}}").wysihtml5();
@endif
@endforeach
      $('#form-validation').bootstrapValidator({
          fields: {
            <?php
            $pregnya='/"([a-zA-Z]+[a-zA-Z0-9_]*)":/'; ?>
            @foreach ($elfil as $eelfil)
            @if(isset($eelfil['validators']))
            <?php
            $attr=$eelfil['attr'];
            $arnya=array(
              $attr['name']=>array(
                'validators'=>$eelfil['validators']
              )
            );
            $arnya=json_encode($arnya[$attr['name']]);
            $arnya = preg_replace($pregnya,'$1:',$arnya);
             ?>
           {{$attr['name']}}:{!!$arnya!!},
             @endif
            @endforeach

          }
      }).on('reset', function (event) {
          $('#form-validation').data('bootstrapValidator').resetForm();
      });
$("#formpasswordlama").hide();
$('#newpassword').keyup(function(){ //the event here is change
    if ($(this).val().length >=1 ) //check the value into the select
    {
    $("#formpasswordlama").show();  }
    else
    {
      $("#formpasswordlama").hide();
    }
});

$(function () {

});

</script>

  @endsection
