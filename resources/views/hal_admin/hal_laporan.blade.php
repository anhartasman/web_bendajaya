@extends('layouts.masteradmin')
@section('didalamhead')
<link href="{{ URL::asset('vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
 <style>
#add{
  display:inline-block;
  padding-left: 1px;
  padding-right: 30px;
  float:left;
}
.bahaya{
   background-color: #D6D5C3;
}
</style>
@endsection
@section('statheader')
<?php
$namamenu=$namamenudaricont;
$dafmenu=$dafmenudaricont;
 ?>
@endsection
@section('kontenweb')

<!-- Main content -->
@if($errors->has())
           @foreach ($errors->all() as $error)
           <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ $error }}
          </div>
          @endforeach
   @endif
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title">
                        <i class="ti-layout-grid3"></i> {{$namatabel}}
                    </h3>
                    @if($tipetabel==2)
                        <div class="pull-right">
                            <a href="{{ url('/') }}/admin/{{$halamandaricont}}/add" class="btn btn-primary btn-sm" id="tombolTambah">Tambah</a>
                            <button type="button" class="btn btn-primary btn-sm" id="tombolEdit">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm" id="tombolHapus">Hapus</button>
                        </div>
                    @elseif($tipetabel==3)
                        <div class="pull-right">
                            <a href="{{ url('/') }}/admin/{{$halamandaricont}}/add" class="btn btn-primary btn-sm" id="tombolTambah">Tambah</a>
                            <button type="button" class="btn btn-primary btn-sm" id="tombolEdit">Edit</button>
                        </div>
                    @elseif($tipetabel==4)
                        <div class="pull-right">
                          <button type="button" class="btn btn-primary btn-sm" id="tombolEdit">Edit</button>
                        </div>
                    @elseif($tipetabel==5)
                        <div class="pull-right">
                          <button type="button" class="btn btn-primary btn-sm" id="tombolLihat">View</button>
                        </div>
                @elseif($tipetabel==6)
                    <div class="pull-right">
                          <a href="{{ url('/') }}/admin/{{$halamandaricont}}/add" class="btn btn-primary btn-sm" id="tombolTambah">Tambah</a>
                            <button type="button" class="btn btn-primary btn-sm" id="tombolLihat">View</button>
                          <button type="button" class="btn btn-primary btn-sm" id="tombolEdit">Edit</button>
                          <button type="button" class="btn btn-danger btn-sm" id="tombolHapus">Hapus</button>
                    </div>
                @endif
                </div>

                <div class="panel-body">
                    <div class="table-responsive">

                        <div class="box-header">
                          @foreach ($elfil as $eelfil)
                          <?php
                          $attr=$eelfil['attr'];
                          switch($eelfil["type"]){
                            case "select":
                            ?>
                            {{$eelfil["label"]}}   {{Form::select('size', $eelfil['list'],$eelfil['catch'],$attr)}}
                           <?php
                             break;
                          }
                           ?>
                          @endforeach

                        </div>
                        <table class="table table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                              @foreach($dafcol as $col)
                                <th>
                                    {{$col}}
                                </th>
                              @endforeach
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="{{count($dafcol)}}"><span style="float:right;"id="isifooter"></span></td>
                              </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
    <!--rightside bar -->
    @include("hal_admin.inc_rightside")
    <div class="background-overlay"></div>
</section>
  @endsection


  @section('bagianfooter')

  <!-- end of global js -->
  <!-- begining of page level js -->
  <script src="{{ URL::asset('vendors/moment/js/moment.min.js') }}"></script>
  <script src="{{ URL::asset('vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/select2/js/select2.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ URL::asset('vendors/datatables/js/jquery.dataTables.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<!-- end of page level js -->

  <script>


  var listidtabel;
  var pilihanidtabel;
  @foreach ($elfil as $eelfil)
  <?php
  $attr=$eelfil['attr'];
  switch($eelfil["type"]){
    case "select":
    ?>
    var isi{{$attr["id"]}};
    <?php
     break;
  }
   ?>
  @endforeach
  @foreach ($elfil as $eelfil)
  <?php
  $attr=$eelfil['attr'];
  switch($eelfil["type"]){
    case "select":
    ?>
    function pilihselect{{$attr["id"]}}(){
        isi{{$attr["id"]}}=$("#{{$attr["id"]}}").find(":selected").val();
    }
    <?php
     break;
  }
   ?>
  @endforeach

  @foreach ($needjsv as $isijsv)
  var {{$isijsv->name}};
  @endforeach

  @foreach ($needjsf as $isijsf)
  <?php
  switch($isijsf->type){
    case "ajax2v":
    ?>
    	var ajax{{$isijsf->var}};
      function {{$isijsf->name}}(){

        ajax{{$isijsf->var}} = buatajax();
        var url={!!$isijsf->url!!};
        //url=url+"?q="+nip;
        //url=url+"&sid="+Math.random();
        ajax{{$isijsf->var}}.onreadystatechange=stateChanged{{$isijsf->var}};
        ajax{{$isijsf->var}}.open("GET",url,true);
        ajax{{$isijsf->var}}.send(null);

      }

      function stateChanged{{$isijsf->var}}(){
        var datar;
         if (ajax{{$isijsf->var}}.readyState==4){
           {{$isijsf->var}}=ajax{{$isijsf->var}}.responseText;
             //alert({{$isijsf->var}});

          }
     }

    <?php
     break;
     case "ajax2s":
     ?>
     	var ajax{{$isijsf->var}};
       function {{$isijsf->name}}(){

         ajax{{$isijsf->var}} = buatajax();
         var url={!!$isijsf->url!!};
         //url=url+"?q="+nip;
         //url=url+"&sid="+Math.random();
         ajax{{$isijsf->var}}.onreadystatechange=stateChanged{{$isijsf->var}};
         ajax{{$isijsf->var}}.open("GET",url,true);
         ajax{{$isijsf->var}}.send(null);

       }

       function stateChanged{{$isijsf->var}}(){
         var datar;
          if (ajax{{$isijsf->var}}.readyState==4){

            document.getElementById('{{$isijsf->target}}').options.length = 0;


            {{$isijsf->var}}=ajax{{$isijsf->var}}.responseText;
            var obj=JSON.parse({{$isijsf->var}});
            $.each(obj, function(key, value) {
                $('#{{$isijsf->target}}').append($("<option/>", {
                  value: key,
                  text: value
                }));
              });
               //alert({{$isijsf->var}});

           }
      }


          <?php
           break;
           case "ajax2t":
           ?>
           	var ajax{{$isijsf->var}};
             function {{$isijsf->name}}(){

               ajax{{$isijsf->var}} = buatajax();
               var url={!!$isijsf->url!!};
               //url=url+"?q="+nip;
               //url=url+"&sid="+Math.random();
               ajax{{$isijsf->var}}.onreadystatechange=stateChanged{{$isijsf->var}};
               ajax{{$isijsf->var}}.open("GET",url,true);
               ajax{{$isijsf->var}}.send(null);

             }

             function stateChanged{{$isijsf->var}}(){
               var datar{{$isijsf->var}};
                if (ajax{{$isijsf->var}}.readyState==4){
                  datar{{$isijsf->var}}=ajax{{$isijsf->var}}.responseText;
                  if(datar{{$isijsf->var}}.length>0){
                    {{$isijsf->var}}=datar{{$isijsf->var}};
                      //    alert('ada');
                    //document.getElementById("hasilkirim").html = data;
                  //  $('#bodyexample1').html(data);
                  var string = "UpdateBootProfile,PASS,00:00:00";
                  var array = datar{{$isijsf->var}}.split(",");
                  var data = [
                      array
                  ];
                  $('#sample_1').dataTable().fnClearTable();
                  var obj=JSON.parse(datar{{$isijsf->var}});

                  if(obj.list!=null){
                    @if($tipetabel!=1)
                    if(obj.listidtabel==null){
                      alert("kosong");
                    }
                    listidtabel=obj.listidtabel;
                    @endif

                  $('#sample_1').dataTable().fnAddData(obj.list);
                    $("#isifooter").html(obj.isifooter);
                  }else{
                    $("#isifooter").html("");
                  }

                   }
                   //alert({{$isijsf->var}});

                 }
            }



     <?php
      break;
  }
   ?>
  @endforeach


    		var ajaxku;
        function ambildata(){

          ajaxku = buatajax();
          var url="{{ url('/') }}/admin/{{$halamandaricont}}/carijson/"+thn+"/"+bln;
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);

        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var datar;
            if (ajaxku.readyState==4){
              datar=ajaxku.responseText;
              //  alert(datar);
              if(datar.length>0){
                  //    alert('ada');
                //document.getElementById("hasilkirim").html = data;
              //  $('#bodyexample1').html(data);
              var string = "UpdateBootProfile,PASS,00:00:00";
          var array = datar.split(",");
              var data = [
                  array
              ];
              $('#sample_1').dataTable().fnClearTable();
              var obj=JSON.parse(datar);
              $('#sample_1').dataTable().fnAddData(obj.list);
                $("#isifooter").html(obj.isifooter);
              /**
              var testsTable = $('#example1').dataTable({
                      bJQueryUI: true,
                      aaData: JSON.parse(datar)
              });
              **/
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

var intVal=function(i){
  return typeof i==='string' ? i.replace(/[\$,]/g,'')*1:typeof i==='number'? i:0;
}

  $(document).ready(function() {
$("#tombolLihat").hide();
$("#tombolEdit").hide();
$("#tombolHapus").hide();
      var tabel=$('#sample_1').DataTable({
          "responsive": true
          ,"footerCallback":function(row, data,start, end, display){
/**
            var api=this.api();
            var total=api.column(2).data().reduce(function(a,b){
              var inttotal=intVal(a)+intVal(b);
              return inttotal;
            },0);
            if(total>0){
          //  $("#sample_1").append($('<tfoot/>').append("HAHA"+total));
          $("#isifooter").html(total);
          }
          **/
          }

      });

      var aa;

      $('#sample_1 tbody').on( 'click', 'tr', function () {
        //alert("HAHA");
        if ( $(this).hasClass('bahaya') ) {
            $(this).removeClass('bahaya');
            pilihanidtabel=null;
            $("#tombolEdit").hide();
            $("#tombolHapus").hide();
            $("#tombolLihat").hide();
        }
        else {
         $(aa).removeClass('bahaya');
            $(this).addClass('bahaya');
            var tr = $(this).closest("tr");
   //var rowindex = tr.index();
   var rowindex = tabel.row(this).index();
   var rowData = tabel.row(this).data();
   var selectedName = tabel.row(this)[0];
   var anotherWayToGetName = tabel.cell(rowindex, 0).data();
   console.log(rowindex, rowData, selectedName, anotherWayToGetName);
   //rowindex-=1;
   if(listidtabel!=null){
     @if($tipetabel!=1)
    pilihanidtabel=listidtabel[rowindex];
    @endif
    $("#tombolEdit").show();
    $("#tombolHapus").show();
    $("#tombolLihat").show();
    //alert(pilihanidtabel);
  }
            aa=this;
        }
    } );

/**
    $('#sample_1 tbody').on( 'click', 'tr', function () {
   $('#sample_1 tbody tr ').each(  function () {
       if ( $(this).hasClass('bahaya') ) {
           $(this).removeClass('bahaya');
           var rowindex = table.row(this).index();
           var rowData = table.row(this).data();
           var selectedName = table.row(this)[0];
           var anotherWayToGetName = table.cell(rowindex, 0).data();
           console.log(rowIndex, rowData, selectedName, anotherWayToGetName);
       }
    } );
} );
**/
    $("#sample_1 tbody tr").on('click',function(event) {
          $("#sample_1 tbody tr").removeClass('row_selected');
          $(this).addClass('row_selected');
      });
      $("#tombolEdit").on('click',function(event) {
            window.location.href = '{{$urledit}}/'+pilihanidtabel;
        });
      $("#tombolHapus").on('click',function(event) {
            window.location.href = '{{$urldelete}}/'+pilihanidtabel;
        });
      $("#tombolLihat").on('click',function(event) {
            window.location.href = '{{$urlview}}/'+pilihanidtabel;
        });

        @foreach ($needjsf as $isijsf)
        <?php
        switch($isijsf->type){
          case "main":
          ?>
          {!!$isijsf->content!!}
          <?php
           break;
        }
         ?>
        @endforeach
      //var api=tabel.api();
      //var column=tabel.column(3).data().sum();
      //$("#sample_1").append($('<tfoot/>').append("HAHA"));

/**
            $('#sample_1').dataTable({
                "responsive": true
                ,dom:'l<"#add">frtip'
            });


      $('<label/>').text("aa").appendTo('#add');
      $select=$('<select/>').appendTo('#add');
      $('<option/>').val('1').text('option 1').appendTo($select);
**/
@foreach ($elfil as $eelfil)
<?php
$attr=$eelfil['attr'];
switch($eelfil["type"]){
  case "select":
  ?>
  $("#{{$attr["id"]}}").on( 'change', function () {
    isi{{$attr["id"]}}=$("#{{$attr["id"]}}").find(":selected").val();
@if(isset($eelfil["onchange"]))
{!!$eelfil["onchange"]!!}
@endif
//alert(isi{{$attr["id"]}});
                         } );
  <?php
   break;
}
 ?>
@endforeach


  });


function setTahun(){
  thn=$("#pilthn").find(":selected").text();
  bln="0";
  var as=eval('bul'+thn);
  var asa=as.split(",");
  $('#pilbln').find('option').remove();
  $('#pilbln').append(eval('bul'+thn));
  $("#pilbln").val('0');

 ambildata();
}
function setBulan(){

  thn=$("#pilthn").find(":selected").text();
  bln=$("#pilbln").find(":selected").val();
  ambildata();
}
</script>

  @endsection
