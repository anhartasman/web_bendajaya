@extends('layouts.masteradmin')
@section('didalamhead')
<link href="{{ URL::asset('vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<style>
#add{
  display:inline-block;
  padding-left: 1px;
  padding-right: 30px;
  float:left;
}
</style>
@endsection
@section('statheader')
<?php
$namamenu=$namamenudaricont;
$dafmenu=$dafmenudaricont;
 ?>
@endsection
@section('kontenweb')

<!-- Main content -->
@if($errors->has())
           @foreach ($errors->all() as $error)
           <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ $error }}
          </div>
          @endforeach
   @endif
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel ">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="ti-layout-grid3"></i> {{$namatabel}}
                    </h3>
                    <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                      <?php
                      $thn=date('Y');
                      $bln=date('m');
                      if(isset($otomatiscari)){
                        $thn=$thnpilihan;
                      }

                      $bulan[1]="Januari";
                      $bulan[2]="Februari";
                      $bulan[3]="Maret";
                      $bulan[4]="April";
                      $bulan[5]="Mei";
                      $bulan[6]="Juni";
                      $bulan[7]="Juli";
                      $bulan[8]="Agustus";
                      $bulan[9]="September";
                      $bulan[10]="Oktober";
                      $bulan[11]="November";
                      $bulan[12]="Desember";
                      ?>
                        <div class="box-header">
                           Tahun <select id="pilthn">@foreach ($years as $year)<option value="{{$year->tahun}}" @if($year->tahun==$thn) selected @endif >{{$year->tahun}}</option>@endforeach</select>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                              @foreach($dafcol as $col)
                                <th>
                                    {{$col}}
                                </th>
                              @endforeach
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="{{count($dafcol)}}"><span style="float:right;"id="isifooter"></span></td>
                              </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row end-->
    <!--rightside bar -->
    <div class="background-overlay"></div>
</section>
  @endsection


  @section('bagianfooter')

  <!-- end of global js -->
  <!-- begining of page level js -->
  <script src="{{ URL::asset('vendors/moment/js/moment.min.js') }}"></script>
  <script src="{{ URL::asset('vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/select2/js/select2.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ URL::asset('vendors/datatables/js/jquery.dataTables.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<!-- end of page level js -->

  <script>

  var thn="{{$thn}}";
  var bln="{{$bln}}";
    		var ajaxku;
        function ambildata(){

          ajaxku = buatajax();
          var url="{{ url('/') }}/admin/{{$halamandaricont}}/carijson/"+thn;
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);

        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var datar;
            if (ajaxku.readyState==4){
              datar=ajaxku.responseText;
              //  alert(datar);
              if(datar.length>0){
                  //    alert('ada');
                //document.getElementById("hasilkirim").html = data;
              //  $('#bodyexample1').html(data);
              var string = "UpdateBootProfile,PASS,00:00:00";
          var array = datar.split(",");
              var data = [
                  array
              ];
              $('#sample_1').dataTable().fnClearTable();
              var obj=JSON.parse(datar);
              $('#sample_1').dataTable().fnAddData(obj.list);
                $("#isifooter").html(obj.isifooter);
              /**
              var testsTable = $('#example1').dataTable({
                      bJQueryUI: true,
                      aaData: JSON.parse(datar)
              });
              **/
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

var intVal=function(i){
  return typeof i==='string' ? i.replace(/[\$,]/g,'')*1:typeof i==='number'? i:0;
}

  $(document).ready(function() {

      var tabel=$('#sample_1').dataTable({
          "responsive": true
          ,"footerCallback":function(row, data,start, end, display){
/**
            var api=this.api();
            var total=api.column(2).data().reduce(function(a,b){
              var inttotal=intVal(a)+intVal(b);
              return inttotal;
            },0);
            if(total>0){
          //  $("#sample_1").append($('<tfoot/>').append("HAHA"+total));
          $("#isifooter").html(total);
          }
          **/
          }
      });
      //var api=tabel.api();
      //var column=tabel.column(3).data().sum();
      //$("#sample_1").append($('<tfoot/>').append("HAHA"));

/**
            $('#sample_1').dataTable({
                "responsive": true
                ,dom:'l<"#add">frtip'
            });


      $('<label/>').text("aa").appendTo('#add');
      $select=$('<select/>').appendTo('#add');
      $('<option/>').val('1').text('option 1').appendTo($select);
**/

    $("#pilthn").on( 'change', function () {

                               setTahun();
                           } );

  ambildata();
  });


function setTahun(){
  thn=$("#pilthn").find(":selected").text();
  bln="0";
  var as=eval('bul'+thn);
  var asa=as.split(",");
  $('#pilbln').find('option').remove();
  $('#pilbln').append(eval('bul'+thn));
  $("#pilbln").val('0');

 ambildata();
}
function setBulan(){

  thn=$("#pilthn").find(":selected").text();
  bln=$("#pilbln").find(":selected").val();
  ambildata();
}
</script>

  @endsection
