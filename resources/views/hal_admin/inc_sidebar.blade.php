
<section class="sidebar">
 <!-- Sidebar user panel -->
 <div class="user-panel">
   <div class="pull-left image">
     <img src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png" class="img-circle" alt="User Image">
   </div>
   <div class="pull-left info">
     <p>{{getUserInfo("user_nama")}}</p>
     <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
   </div>
 </div>
 <!-- search form -->

 <!-- /.search form -->
 <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu">
   <li class="header">MAIN NAVIGATION</li>
   <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
   @if(getUserInfo("user_pangkat")==3)
   <li><a href="{{ url('/') }}/admin/webtemplates"><i class="fa fa-paint-brush"></i> <span>Template Management</span></a></li>
   @endif

@if(getUserInfo("user_pangkat")==2)
   <li class="{{ Request::segment(2) === 'infoweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Informasi Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">

       <li><a href="{{ url('/') }}/admin/infoweb/about"><i class="fa fa-circle-o"></i> About</a></li>
       <li class="{{ Request::segment(3) === 'services' ? 'active' : null }}">
         <a href="#"><i class="fa fa-circle-o"></i> Services
           <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
           </span>
         </a>
         <ul class="treeview-menu">

    <li><a href="{{ url('/') }}/admin/infoweb/services"><i class="fa fa-circle-o"></i> Pengaturan</a></li>
       </ul>
       </li>


                          <li class="{{ Request::segment(3) === 'team' ? 'active' : null }}">
                            <a href="#"><i class="fa fa-circle-o"></i> Team
                              <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                            <ul class="treeview-menu">
                      <li><a href="{{ url('/') }}/admin/infoweb/team"><i class="fa fa-circle-o"></i>Pengaturan</a></li>
                            </ul>
                          </li>


                                 <li class="{{ Request::segment(3) === 'faq' ? 'active' : null }}">
                                   <a href="#"><i class="fa fa-circle-o"></i> FAQ
                                     <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                     </span>
                                   </a>
                                   <ul class="treeview-menu">

                             <li><a href="{{ url('/') }}/admin/infoweb/faq"><i class="fa fa-circle-o"></i> Pengaturan</a></li>
                                   </ul>
                                 </li>



                                        <li class="{{ Request::segment(3) === 'policies' ? 'active' : null }}">
                                          <a href="#"><i class="fa fa-circle-o"></i> Policies
                                            <span class="pull-right-container">
                                              <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                          </a>
                                          <ul class="treeview-menu">

                                    <li><a href="{{ url('/') }}/admin/infoweb/policies"><i class="fa fa-circle-o"></i> Pengaturan</a></li>
                                          </ul>
                                        </li>




                <li><a href="{{ url('/') }}/admin/infoweb/kontak"><i class="fa fa-circle-o"></i> Kontak</a></li>



                <li class="{{ Request::segment(3) === 'rekening' ? 'active' : null }}">
                  <a href="#"><i class="fa fa-circle-o"></i> Rekening
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                          </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}/admin/infoweb/rekening"><i class="fa fa-bank"></i> <span>Pengaturan</span></a></li>


                        </ul>
                </li>


     </ul>
   </li>
@endif
@if(getUserInfo("user_pangkat")==2)
  <li class="{{ Request::segment(2) === 'elemenweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Elemen Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">

        <li><a href="{{ url('/') }}/admin/elemenweb/logoweb"><i class="fa fa-circle-o"></i>Logo Web</a></li>

                <li class="{{ Request::segment(3) === 'style' ? 'active' : null }}">
                  <a href="#"><i class="fa fa-circle-o"></i> Tampilan
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}/admin/elemenweb/style/template"><i class="fa fa-circle-o"></i>Template</a></li>
                      <li><a href="{{ url('/') }}/admin/elemenweb/style/color"><i class="fa fa-circle-o"></i>Color</a></li>
                        <li><a href="{{ url('/') }}/admin/elemenweb/style/layout"><i class="fa fa-circle-o"></i>Layout</a></li>
                          <li><a href="{{ url('/') }}/admin/elemenweb/style/element"><i class="fa fa-circle-o"></i>Element</a></li>

                  </ul>
                </li>

                  <li class="{{ Request::segment(3) === 'slider' ? 'active' : null }}">
                    <a href="#"><i class="fa fa-picture-o"></i> Slider Banner
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">

                                   <li><a href="{{ url('/') }}/admin/elemenweb/slider/tambah"><i class="fa fa-circle-o"></i>Upload Banner</a></li>
                                   <li><a href="{{ url('/') }}/admin/elemenweb/slider"><i class="fa fa-circle-o"></i>Album</a></li>
                    </ul>
                 </li>

        <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
          <a href="#"><i class="fa fa-picture-o"></i> Galeri
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

                         <li><a href="{{ url('/') }}/admin/elemenweb/galeri/tambah"><i class="fa fa-circle-o"></i>Upload Foto</a></li>

                                <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
                                  <a href="#"><i class="fa fa-circle-o"></i> Album
                                    <span class="pull-right-container">
                                      <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                  </a>
                                  <ul class="treeview-menu">

                         <?php

                         foreach ($kategorisgaleri as $kategori) {
                             $segment5 = Request::segment(5);

                         ?>
                                                    <li><a href="{{ url('/') }}/admin/elemenweb/galeri/<?php  echo $kategori->id;?>"><i class="fa fa-circle-o"></i><?php  echo $kategori->category;?></a></li>
                         <?php
                         }
                          ?> <li><a href="{{ url('/') }}/admin/elemenweb/galeri"><i class="fa fa-circle-o"></i>All</a></li>
         </ul>
                                </li>
        </ul>
        </li>


     </ul>
   </li>
@endif

@if(getUserInfo("user_pangkat")<=2)
      <li class="{{ Request::segment(2) === 'blogging' ? 'active' : null }}  treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Penulisan Blog</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">

           <li><a href="{{ url('/') }}/admin/blogging"><i class="fa fa-circle-o"></i>Pengaturan</a></li>
    </ul>
    </li>

    <li class="{{ Request::segment(2) === 'bukutamu' ? 'active' : null }}"><a href="{{ url('/') }}/admin/bukutamu"><i class="fa fa-book"></i> <span>Buku Tamu</span></a></li>
@endif
   @if(getUserInfo("user_pangkat")==2)
    <li class="{{ Request::segment(2) === 'transaksi' ? 'active' : null }}  treeview">
      <a href="#">
        <i class="fa fa-reorder"></i> <span>Transaksi</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>

      <ul class="treeview-menu">

        <li><a href="{{ url('/') }}/admin/transaksi/setting"><i class="fa fa-circle-o"></i>Setting</a></li>
        <li class="{{ Request::segment(3) === 'airline' ? 'active' : null }}">
          <a href="#"><i class="fa fa-plane"></i> Airline
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/status/0"><i class="fa fa-circle-o"></i>Belum dibayar</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/status/1"><i class="fa fa-circle-o"></i>Menunggu konfirmasi</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/status/2"><i class="fa fa-circle-o"></i>Diterima</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/status/3"><i class="fa fa-circle-o"></i>Diterima dengan syarat</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/status/4"><i class="fa fa-circle-o"></i>Ditolak</a></li>

          </ul>
        </li>
        <li class="{{ Request::segment(3) === 'train' ? 'active' : null }}">
          <a href="#"><i class="fa fa-train"></i> Train
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{ url('/') }}/admin/transaksi/train/tran/status/0"><i class="fa fa-circle-o"></i>Belum dibayar</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/train/tran/status/1"><i class="fa fa-circle-o"></i>Menunggu konfirmasi</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/train/tran/status/2"><i class="fa fa-circle-o"></i>Diterima</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/train/tran/status/3"><i class="fa fa-circle-o"></i>Diterima dengan syarat</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/train/tran/status/4"><i class="fa fa-circle-o"></i>Ditolak</a></li>

          </ul>
        </li>
        <li class="{{ Request::segment(3) === 'hotel' ? 'active' : null }}">
          <a href="#"><i class="fa fa-hotel"></i> Hotel
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{ url('/') }}/admin/transaksi/hotel/tran/status/0"><i class="fa fa-circle-o"></i>Belum dibayar</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/hotel/tran/status/1"><i class="fa fa-circle-o"></i>Menunggu konfirmasi</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/hotel/tran/status/2"><i class="fa fa-circle-o"></i>Diterima</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/hotel/tran/status/3"><i class="fa fa-circle-o"></i>Diterima dengan syarat</a></li>
            <li><a href="{{ url('/') }}/admin/transaksi/hotel/tran/status/4"><i class="fa fa-circle-o"></i>Ditolak</a></li>

          </ul>
        </li>


      </ul>
    </li>
    @endif
    <li class="{{ Request::segment(2) === 'akun' ? 'active' : null }}  treeview">
      <a href="#">
        <i class="fa fa-user"></i> <span>Akun</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">

        <li><a href="{{ url('/') }}/admin/akun/pengaturan"><i class="fa fa-circle-o"></i>Pengaturan</a></li>

        <li class="{{ Request::segment(3) === 'level' ? 'active' : null }}">
          <a href="#"><i class="fa fa-user"></i> Tabel Akun
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            @if(getUserInfo("user_pangkat")==3)
            <li><a href="{{ url('/') }}/admin/akun/level/3"><i class="fa fa-circle-o"></i>Super Admin</a></li>
            @endif
            @if(getUserInfo("user_pangkat")<=2)
            <li><a href="{{ url('/') }}/admin/akun/level/2"><i class="fa fa-circle-o"></i>Admin</a></li>

            <li><a href="{{ url('/') }}/admin/akun/level/1"><i class="fa fa-circle-o"></i>Operator</a></li>
              @endif
         </ul>
        </li>
        <li><a href="{{ url('/') }}/admin/akun/tambah"><i class="fa fa-circle-o"></i>Tambah</a></li>



      </ul>
    </li>

 </ul>
</section>
