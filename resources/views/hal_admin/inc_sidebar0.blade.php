
<section class="sidebar">
 <!-- Sidebar user panel -->
 <div class="user-panel">
   <div class="pull-left image">
     <img src="{{ URL::asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
   </div>
   <div class="pull-left info">
     <p>Alexander Pierce</p>
     <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
   </div>
 </div>
 <!-- search form -->
 <form action="#" method="get" class="sidebar-form">
   <div class="input-group">
     <input type="text" name="q" class="form-control" placeholder="Search...">
         <span class="input-group-btn">
           <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
           </button>
         </span>
   </div>
 </form>
 <!-- /.search form -->
 <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu">
   <li class="header">MAIN NAVIGATION</li>
   <li class=" treeview">
     <a href="#">
       <i class="fa fa-dashboard"></i> <span>Dashboard</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">
       <li class=""><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
       <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
     </ul>
   </li>


   <li class="{{ Request::segment(2) === 'infoweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Informasi Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">

       <li class="{{ Request::segment(3) === 'services' ? 'active' : null }}">
         <a href="#"><i class="fa fa-circle-o"></i> Services
           <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
           </span>
         </a>
         <ul class="treeview-menu">

<?php

$services= DB::table('tb_webcont_services')->select(array('namaservis','id'))->get();
foreach ($services as $servis) {
    $segment5 = Request::segment(5);

?>
           <li class="<?php if($servis->id==$segment5){print("active");} ?>">
             <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $servis->namaservis;?>
               <span class="pull-right-container">
                 <i class="fa fa-angle-left pull-right"></i>
               </span>
             </a>
             <ul class="treeview-menu">
               <li><a href="{{ url('/') }}/admin/infoweb/services/view/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
               <li><a href="{{ url('/') }}/admin/infoweb/services/edit/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
               <li><a href="{{ url('/') }}/admin/infoweb/services/delete/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
             </ul>
           </li>
<?php
}
 ?>

   <li><a href="{{ url('/') }}/admin/infoweb/services/tambah"><i class="fa fa-circle-o"></i> Tambah Servis</a></li>
         </ul>
       </li>

             <li class="{{ Request::segment(3) === 'testimonials' ? 'active' : null }}">
               <a href="#"><i class="fa fa-circle-o"></i> Testimonials
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">

       <?php

       $testimonials= DB::table('tb_webcont_testimonials')->select(array('sumbertesti','id'))->get();
       foreach ($testimonials as $testimonial) {
          $segment5 = Request::segment(5);

       ?>
                 <li class="<?php if($testimonial->id==$segment5){print("active");} ?>">
                   <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $testimonial->sumbertesti;?>
                     <span class="pull-right-container">
                       <i class="fa fa-angle-left pull-right"></i>
                     </span>
                   </a>
                   <ul class="treeview-menu">
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/view/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/edit/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                     <li><a href="{{ url('/') }}/admin/infoweb/testimonials/delete/<?php  echo $testimonial->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                   </ul>
                 </li>
       <?php
       }
       ?>

         <li><a href="{{ url('/') }}/admin/infoweb/testimonials/tambah"><i class="fa fa-circle-o"></i> Tambah Testimonial</a></li>
               </ul>
             </li>


                          <li class="{{ Request::segment(3) === 'team' ? 'active' : null }}">
                            <a href="#"><i class="fa fa-circle-o"></i> Team
                              <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                            <ul class="treeview-menu">

                    <?php

                    $team= DB::table('tb_webcont_team')->select(array('namaanggota','id'))->get();
                    foreach ($team as $anggota) {
                       $segment5 = Request::segment(5);

                    ?>
                              <li class="<?php if($anggota->id==$segment5){print("active");} ?>">
                                <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $anggota->namaanggota;?>
                                  <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                  </span>
                                </a>
                                <ul class="treeview-menu">
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/view/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/edit/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                  <li><a href="{{ url('/') }}/admin/infoweb/team/delete/<?php  echo $anggota->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                </ul>
                              </li>
                    <?php
                    }
                    ?>

                      <li><a href="{{ url('/') }}/admin/infoweb/team/tambah"><i class="fa fa-circle-o"></i> Tambah Team</a></li>
                            </ul>
                          </li>


                                 <li class="{{ Request::segment(3) === 'faq' ? 'active' : null }}">
                                   <a href="#"><i class="fa fa-circle-o"></i> FAQ
                                     <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                     </span>
                                   </a>
                                   <ul class="treeview-menu">

                          <?php

                          $faqs= DB::table('tb_webcont_faq')->select(array('pertanyaan','id'))->get();
                          foreach ($faqs as $faq) {
                              $segment5 = Request::segment(5);

                          ?>
                                     <li class="<?php if($faq->id==$segment5){print("active");} ?>">
                                       <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $faq->pertanyaan;?>
                                         <span class="pull-right-container">
                                           <i class="fa fa-angle-left pull-right"></i>
                                         </span>
                                       </a>
                                       <ul class="treeview-menu">
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/view/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/edit/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                         <li><a href="{{ url('/') }}/admin/infoweb/faq/delete/<?php  echo $faq->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                       </ul>
                                     </li>
                          <?php
                          }
                           ?>

                             <li><a href="{{ url('/') }}/admin/infoweb/faq/tambah"><i class="fa fa-circle-o"></i> Tambah FAQ</a></li>
                                   </ul>
                                 </li>



                                        <li class="{{ Request::segment(3) === 'policies' ? 'active' : null }}">
                                          <a href="#"><i class="fa fa-circle-o"></i> Policies
                                            <span class="pull-right-container">
                                              <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                          </a>
                                          <ul class="treeview-menu">

                                 <?php

                                 $policies= DB::table('tb_webcont_policies')->select(array('judul','id'))->get();
                                 foreach ($policies as $policy) {
                                     $segment5 = Request::segment(5);

                                 ?>
                                            <li class="<?php if($policy->id==$segment5){print("active");} ?>">
                                              <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $policy->judul;?>
                                                <span class="pull-right-container">
                                                  <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                              </a>
                                              <ul class="treeview-menu">
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/view/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/edit/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                                <li><a href="{{ url('/') }}/admin/infoweb/policies/delete/<?php  echo $policy->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                              </ul>
                                            </li>
                                 <?php
                                 }
                                  ?>

                                    <li><a href="{{ url('/') }}/admin/infoweb/policies/tambah"><i class="fa fa-circle-o"></i> Tambah Policy</a></li>
                                          </ul>
                                        </li>


                                          <li class="{{ Request::segment(3) === 'services' ? 'active' : null }}">
                                            <a href="#"><i class="fa fa-circle-o"></i> Services
                                              <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                              </span>
                                            </a>
                                            <ul class="treeview-menu">

                                        <?php

                                        $services= DB::table('tb_webcont_services')->select(array('namaservis','id'))->get();
                                        foreach ($services as $servis) {
                                        $segment5 = Request::segment(5);

                                        ?>
                                              <li class="<?php if($servis->id==$segment5){print("active");} ?>">
                                                <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $servis->namaservis;?>
                                                  <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                  </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                  <li><a href="{{ url('/') }}/admin/infoweb/services/view/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> View</a></li>
                                                  <li><a href="{{ url('/') }}/admin/infoweb/services/edit/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Edit</a></li>
                                                  <li><a href="{{ url('/') }}/admin/infoweb/services/delete/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> Delete</a></li>
                                                </ul>
                                              </li>
                                        <?php
                                        }
                                        ?>

                                        <li><a href="{{ url('/') }}/admin/infoweb/services/tambah"><i class="fa fa-circle-o"></i> Tambah Servis</a></li>
                                            </ul>
                                          </li>


                  <li class="{{ Request::segment(3) === 'kontak' ? 'active' : null }}">
                    <a href="#"><i class="fa fa-circle-o"></i> Kontak
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                            </span>
                    </a>
                    <ul class="treeview-menu">
                <?php
                $services= DB::table('tb_webcont_kontak')->select(array('label','id'))->get();
                foreach ($services as $servis) {
                $segment5 = Request::segment(5);
                ?>
                      <li><a href="{{ url('/') }}/admin/infoweb/kontak/edit/<?php  echo $servis->id;?>"><i class="fa fa-circle-o"></i> <?php  echo $servis->label;?></a></li>
                        <?php
                      }
                      ?>

                         </ul>
                  </li>



     </ul>
   </li>


   <li class="{{ Request::segment(2) === 'elemenweb' ? 'active' : null }}  treeview">
     <a href="#">
       <i class="fa fa-laptop"></i> <span>Elemen Website</span>
       <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
       </span>
     </a>
     <ul class="treeview-menu">

       <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
         <a href="#"><i class="fa fa-picture-o"></i> Galeri
           <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
           </span>
         </a>
         <ul class="treeview-menu">

                        <li><a href="{{ url('/') }}/admin/elemenweb/galeri/tambah"><i class="fa fa-circle-o"></i>Upload Foto</a></li> 

                               <li class="{{ Request::segment(3) === 'galeri' ? 'active' : null }}">
                                 <a href="#"><i class="fa fa-circle-o"></i> Album
                                   <span class="pull-right-container">
                                     <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                                 </a>
                                 <ul class="treeview-menu">

                        <?php

                        $kategoris= DB::table('tb_webcont_galeri')->select(['kategori'])->groupBy('kategori')->get();
                        foreach ($kategoris as $kategori) {
                            $segment5 = Request::segment(5);

                        ?>
                                                   <li><a href="{{ url('/') }}/admin/elemenweb/galeri/<?php  echo $kategori->kategori;?>"><i class="fa fa-circle-o"></i><?php  echo $kategori->kategori;?></a></li>
                        <?php
                        }
                         ?> <li><a href="{{ url('/') }}/admin/elemenweb/galeri"><i class="fa fa-circle-o"></i>All</a></li>
        </ul>
                               </li>
   </ul>
       </li>

         <li class="{{ Request::segment(3) === 'slider' ? 'active' : null }}">
           <a href="#"><i class="fa fa-picture-o"></i> Slider Banner
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span>
           </a>
           <ul class="treeview-menu">

                          <li><a href="{{ url('/') }}/admin/elemenweb/slider/tambah"><i class="fa fa-circle-o"></i>Upload Banner</a></li>
                          <li><a href="{{ url('/') }}/admin/elemenweb/slider"><i class="fa fa-circle-o"></i>Album</a></li>
     </ul>
         </li>



     </ul>
   </li>



      <li class="{{ Request::segment(2) === 'blogging' ? 'active' : null }}  treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Penulisan Blog</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">

          <li><a href="{{ url('/') }}/admin/blogging/tambah"><i class="fa fa-circle-o"></i>Tulis Baru</a></li>


   <?php


//->select(array('judul','id'))
//$services = DB::table('tb_webcont_blog')->groupBy(DB::raw('YEAR(tanggal)'))->where( DB::raw('MONTH(tanggal)'), '=', date('n') )->get();
//$services = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('YEAR(tanggal) as tahun')])->groupBy(DB::raw('YEAR(tanggal)'))->orderBy('tanggal','desc')->get();
$services = DB::table('tb_webcont_blog')->select([DB::raw('YEAR(tanggal) as tahun'),DB::raw('MONTH(tanggal) as bulan')])->groupBy(DB::raw('YEAR(tanggal)'))->orderBy('tanggal','desc')->get();

    $segment5 = Request::segment(5);
    $segment4 = Request::segment(4);
   //$services= DB::table('tb_webcont_services')->get();
   foreach ($services as $servis) {

   ?>

             <li class="<?php if($servis->tahun==$segment4){print("active");} ?>">
               <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $servis->tahun;?>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <?php
                 //$servicesmonths->where()->get()
                 $servicesmonths = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('MONTH(tanggal) as bulan')]);
                $servicehasilget=$servicesmonths->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->groupBy(DB::raw('MONTH(tanggal)'))->get();
                    //$services= DB::table('tb_webcont_services')->get();
                    $bulansebelumnya=0;
                    $awal=0;
                    foreach ($servicehasilget as $servicesmonth) {
                      $monthNum  =$servicesmonth->bulan;
                      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                      $monthName = $dateObj->format('F'); // March
                      if($monthNum!=$bulansebelumnya){
                       ?>
              <li class="<?php if($servicesmonth->bulan==$segment5){print("active");} ?>">
                <a href="#"><i class="fa fa-circle-o"></i> <?php  echo $monthName." ".$servicesmonth->bulan;?>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <?php
                  $servicemonthlget = DB::table('tb_webcont_blog')->select(['tanggal','judul','id',DB::raw('MONTH(tanggal) as bulan')])->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->where(DB::raw('MONTH(tanggal)'), '=',$servicesmonth->bulan)->orderBy('tanggal','desc')->get();

                //  $servicemonthlget=$servicesmonths->where(DB::raw('YEAR(tanggal)'), '=',$servis->tahun )->where(DB::raw('MONTH(tanggal)'), '=',$servicesmonth->bulan)->orderBy('tanggal','desc')->get();

                  foreach ($servicemonthlget as $namar) {
                   ?>
                   <li><a href="{{ url('/') }}/admin/blogging/view/<?php  echo $servis->tahun;?>/<?php  echo $namar->bulan;?>/<?php  echo $namar->id;?>"><i class="fa fa-circle-o"></i> <?php  echo $namar->judul;?></a></li>

                     <?php
                      }
                      ?>

                 <?php
                 }
                 ?>
                 <?php
                 if($monthNum!=$bulansebelumnya){
                  ?>
               </ul>
             </li>

                              <?php
                              $bulansebelumnya =$monthNum;
                              }
                              ?>
                 <?php

               }
                  ?>

                          </ul>
                        </li>

   <?php
   }
    ?>



    </ul>
    </li>


 </ul>
</section>
