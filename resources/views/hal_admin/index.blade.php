@extends('layouts.masteradmin')
@section('kontenweb')
<section class="content-header">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-8">
            <div class="header-element">
                <h3>Clear/
                    <small>Dashboard</small>
                </h3>
            </div>
        </div>
        <div class="col-lg-4 col-lg-offset-2 col-md-6 col-sm-7 col-xs-4">
            <div class="header-object">
                <span class="option-search pull-right hidden-xs">
                    <span class="search-wrapper">
                        <input type="text" placeholder="Search here"><i class="ti-search"></i>
                    </span>
                </span>
            </div>
        </div>
    </div>
</section>
<section class="content">

    <!--rightside bar -->
    @include("hal_admin.inc_rightside")
    <!-- /#right -->
    <div class="background-overlay"></div>
</section>
@endsection
