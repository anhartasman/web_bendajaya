<!DOCTYPE html>
<html>


<!-- Mirrored from clear.lcrm.in/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Apr 2017 16:15:20 GMT -->
<head>
    <title>::Admin Login::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon.ico') }}"/>
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="{{ URL::asset('vendors/themify/css/themify-icons.css') }}" rel="stylesheet"/>
    <link href="{{ URL::asset('vendors/iCheck/css/all.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ URL::asset('css/login.css') }}" rel="stylesheet">
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="{{ URL::asset('img/loader.gif') }}" alt="loading..." height="64" width="64"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
            <div class="panel-header">
                <h2 class="text-center">
                    <img src="img/pages/clear_black.png" alt="Admin Login">
                </h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                      <form action="konfirmlogin" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if($errors->has())
                                 @foreach ($errors->all() as $error)
                                    <h1>{{ $error }}</h1>
                                @endforeach
                              @endif


                        <div class="form-group has-feedback">
                          <input name="username" required="" type="text" class="form-control" placeholder="Username">
                          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                          <input name="password" required=""  type="password" class="form-control" placeholder="Password">
                          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                        <img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">
                        <input type="text" id="captcha" required=""  name="captcha">
                        </div>
                        <div class="row">
                          <div class="col-xs-8">
                            <div class="checkbox icheck">
                              <label>
                                <input type="checkbox"> Remember Me
                              </label>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                          </div>
                          <!-- /.col -->
                        </div>
                      </form>
                    </div>
                </div>
                    <!--
                <div class="row text-center social">
                    <div class="col-xs-12">
                        <p class="alter">Sign in with</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-lg btn-facebook">
                                    <i class="ti-facebook"></i>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-lg btn-twitter">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-lg btn-google">
                                    <i class="ti-google"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 -->
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="{{ URL::asset('ajax/libs/jquery/1.12.4/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- end of global js -->
<!-- page level js -->
<script type="text/javascript" src="{{ URL::asset('vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ URL::asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('js/custom_js/login.js') }}"></script>
<!-- end of page level js -->

<!-- Mirrored from clear.lcrm.in/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Apr 2017 16:15:21 GMT -->
</html>
