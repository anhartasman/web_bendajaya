@extends('layouts.masteradmin')

@section('kontenweb')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Read Mail
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{url('/admin/mail/inbox')}}">Mailbox</a></li>
      <li class="active">{{$isipesan->judul}}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-3">
        <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Folders</h3>

            <div class="box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="mailbox.html"><i class="fa fa-inbox"></i> Inbox
                <span class="label label-primary pull-right">12</span></a></li>
              <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
              <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
              <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
              </li>
              <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Labels</h3>

            <div class="box-tools">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
              <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
              <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Read Mail</h3>

            <div class="box-tools pull-right">
              <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
              <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
            </div>
          </div>
          <!-- /.box-header -->
          <?php $datalawyer=datalawyerById($isipesan->idl); ?>
          <?php $dataklien=dataklienById($isipesan->idk); ?>
          <div class="box-body no-padding">
            <div class="mailbox-read-info">
              <h3>{{$isipesan->judul}}</h3>
              <h5>From: {{$dataklien->nama}} ({{$dataklien->email}})
                <span class="mailbox-read-time pull-right">15 Feb. 2016 11:03 PM</span></h5>
            </div>
            <!-- /.mailbox-read-info -->
            <div class="mailbox-controls with-border text-center">
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                  <i class="fa fa-trash-o"></i></button>
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                  <i class="fa fa-reply"></i></button>
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                  <i class="fa fa-share"></i></button>
              </div>
              <!-- /.btn-group -->
              <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                <i class="fa fa-print"></i></button>
            </div>
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
              {{$isipesan->pesan}}
            </div>
            <!-- /.mailbox-read-message -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
              <?php foreach($filepesan as $f){
                $filename=url('/')."/uploads/images/".$f->nama;
$file_parts = \pathinfo($filename);
$fanya="";
$fai="paperclip";
$gambar=0;
switch($file_parts['extension'])
{
    case "png":
    $fai="camera";
    $gambar=1;
    break;

    case "jpg":
    $fai="camera";
    $gambar=1;
    break;

    case "jpeg":
    $fai="camera";
    $gambar=1;
    break;

    case "gif":
    $fai="camera";
    $gambar=1;
    break;

    case "txt":
    $fanya="file-text";
    break;

    case "docx":
    $fanya="file-word-o";
    break;

    case "doc":
    $fanya="file-word-o";
    break;

    case "pdf":
    $fanya="file-pdf-o";
    break;

    case "mp3":
    $fanya="music";
    break;

    case "mp4":
    $fanya="youtube-play";
    break;

    case "3gp":
    $fanya="youtube-play";
    break;

    case "rar":
    $fanya="file-zip-o";
    break;

    case "zip":
    $fanya="file-zip-o";
    break;

    case "": // Handle file extension for files ending in '.'
    case NULL: // Handle no file extension
    break;
}
                 ?>

              <li>
                <span class="mailbox-attachment-icon">
                  @if($gambar==0)
                  <i class="fa fa-{{$fanya}}"></i>
                  @elseif($gambar==1)
                  <img src="{{$filename}}" alt="{{$f->nama}}" width="100" height="100">
                  @endif
                </span>

                <div class="mailbox-attachment-info">
                  <a href="{{$filename}}" class="mailbox-attachment-name"><i class="fa fa-{{$fai}}"></i> {{$f->nama}}</a>
                      <span class="mailbox-attachment-size">
                        {{$f->ukuran}}
                        <a href="{{$filename}}" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                      </span>
                </div>
              </li>
            <?php } ?>


            </ul>
          </div>
          <!-- /.box-footer -->
          <div class="box-footer">
            <div class="pull-right">
              <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
              <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
            </div>
            <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
            <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<!-- DataTables -->

@endsection
