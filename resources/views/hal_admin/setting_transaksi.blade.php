@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi
        <small>Setting </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting Transaksi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengaturan Batas Bayar</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="setting/save" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Batas jam bayar tagihan</label>
                      <select name="trx_batasbayar">
                        <?php for($i=1; $i<=10; $i++){ ?>
                        <option value="{{$i}}" @if($tb_infomember->trx_batasbayar==$i) selected="" @endif>{{$i}}</option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->
          </div>

          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Pengaturan Diskon</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="setting/savediskon" >
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body">
                      <div class="form-group">
                        <label for="QZ">QZ</label>
                        <input class="isiandiskon" type="text" name="QZ" id="QZ" value="{{rupiahrp($tb_diskon->QZ)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="QG">QG</label>
                        <input class="isiandiskon"  type="text" name="QG" id="QG" value="{{rupiahrp($tb_diskon->QG)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="GA">GA</label>
                        <input class="isiandiskon"  type="text" name="GA" id="GA" value="{{rupiahrp($tb_diskon->GA)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="KD">KD</label>
                        <input class="isiandiskon"  type="text" name="KD" id="KD" value="{{rupiahrp($tb_diskon->KD)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="JT">JT</label>
                        <input class="isiandiskon"  type="text" name="JT" id="JT" value="{{rupiahrp($tb_diskon->JT)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="SJ">SJ</label>
                        <input class="isiandiskon"  type="text" name="SJ" id="SJ" value="{{rupiahrp($tb_diskon->SJ)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="MV">MV</label>
                        <input class="isiandiskon"  type="text" name="MV" id="MV" value="{{rupiahrp($tb_diskon->MV)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="IL">IL</label>
                        <input class="isiandiskon"  type="text" name="IL" id="IL" value="{{rupiahrp($tb_diskon->IL)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="KAI">Kereta Api</label>
                        <input class="isiandiskon"  type="text" name="KAI" id="KAI" value="{{rupiahrp($tb_diskon->KAI)}}" required="">
                      </div>
                      <div class="form-group">
                        <label for="HTL">Hotel</label>
                        <input class="isiandiskon"  type="text" name="HTL" id="HTL" value="{{rupiahrp($tb_diskon->HTL)}}" required="">
                      </div>


                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
              </div>
              <!-- /.box --> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.maskMoney.min.js')}}"></script>
<script>

$(document).ready(function(){

  $('.isiandiskon').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

 });

</script>
@endsection
