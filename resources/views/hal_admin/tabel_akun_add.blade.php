@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Akun
        <small>Tambah </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penambahan Akun</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="add" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  @if($errors->has())
                  @foreach ($errors->all() as $error)
                  <h1>{{ $error }}</h1>
                  @endforeach
                  @endif
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input name="nama" required id="nama" type="text" class="form-control" value="{{Request::old('nama')}}"  >
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input name="username" required id="username" type="text" class="form-control" value="{{Request::old('nama')}}"  >
                      </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input name="sandi" required value="" type="password" class="form-control" id="sandi">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password</label>
                    <input name="sandi_confirmation" required type="password" class="form-control" id="sandi_confirmation">
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<script>

$(document).ready(function(){
  $('#sandi1').val('');
  $('#sandi2').val('');
 });

</script>
@endsection
