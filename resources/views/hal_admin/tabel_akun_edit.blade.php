@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Akun
        <small>Edit </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/akun/level/{{$level}}">Tabel Akun</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Perubahan Akun</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="save" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="idakun" value="{{$id}}">
                <div class="box-body">
                  <div class="form-group">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Level</label>
                      <select name="level">
                        <option value="1" @if($level==1) selected="" @endif>Operator</option>
                        @if(getUserInfo("user_pangkat")>1)
                        <option value="2" @if($level==2) selected="" @endif>Admin</option>
                        @endif
                        @if(getUserInfo("user_pangkat")==3)
                        <option value="3" @if($level==3) selected="" @endif>Super Admin</option>
                        @endif
                      </select>
                    </div>
                    <label for="exampleInputEmail1">Username</label>
                    <input name="username" id="username" value="{{$username}}" type="text" class="form-control" >
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input name="nama" id="nama" value="{{$nama}}" type="text" class="form-control" >
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection 
