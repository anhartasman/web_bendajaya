@extends('layouts.masteradmin')
@section('didalamhead')
<link href="{{ URL::asset('vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/select2/css/select2-bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endsection
@section('statheader')
<?php
$namamenu="Account Setting";
$dafmenu=array('#'=>'Users','null'=>'Edit Account');
 ?>
@endsection
@section('kontenweb')
<!-- Main content -->
@if($errors->has())
           @foreach ($errors->all() as $error)
           <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ $error }}
          </div>
          @endforeach
   @endif
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff"
                           data-loop="true"></i> User Profile
                    </h3>
                    <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                </div>
                <div class="panel-body">
                    <!-- errors -->
                    <!--main content-->
                    <form id="form-validation" action="{{url('/admin/akun/pengaturan/save')}}" method="post"
                          class="form-horizontal" enctype="multipart/form-data">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id="idakun" name="idakun" value="{{$tb_user->id}}">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="nama">
                                Name
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="name" name="name" class="form-control required"
                                       placeholder="Enter your name"value="{{$tb_user->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">
                                Email
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="email" name="email" class="form-control"
                                       placeholder="Enter your valid email"value="{{$tb_user->email}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="phone">
                                Phone
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="phone" name="phone" class="form-control"
                                       placeholder="Enter your username"value="{{$tb_user->phone}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic" class="col-md-4 control-label">Profile picture</label>
                            <div class="col-md-6">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 200px;">
                                        <img src="{{ url('/') }}/uploads/images/{{$tb_user->filefoto}}" alt="profile pic"
                                             class="profile_pic">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 200px;"></div>
                                    <div>
                                            <span class="btn btn-default btn-file">
                <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input id="filefoto" name="filefoto" type="file"
                                                   class="form-control"/>
                                            </span>
                                        <a href="#" class="btn btn-danger fileinput-exists"
                                           data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="username">
                                Username
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="username" name="username" class="form-control"
                                       placeholder="Enter your username"value="{{$tb_user->username}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="newpassword">
                                New Password
                            </label>
                            <div class="col-md-6">
                                <input type="newpassword" id="newpassword" name="newpassword" class="form-control"
                                       placeholder="Enter your password">
                            </div>
                        </div>
                        <div id="formpasswordlama">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="confirmnewpassword">
                                Confirm New Password
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="password" id="confirmnewpassword" name="confirmnewpassword"
                                       class="form-control" placeholder="Confirm password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="password">
                                Old Password
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="password" id="password" name="password" class="form-control"
                                       placeholder="Enter your password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="confirmpassword">
                                Confirm Old Password
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="password" id="confirmpassword" name="confirmpassword"
                                       class="form-control" placeholder="Confirm password">
                            </div>
                        </div>
                      </div>


                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                                <button type="reset" class="btn btn-effect-ripple btn-default reset_btn">Reset
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    <!--row end-->
    <!--rightside bar -->
    <div id="right">
        <div id="right-slim">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <div class="panel-tabs">
                        <ul class="nav nav-tabs nav-float" role="tablist">
                            <li class="active text-center">
                                <a href="#r_tab1" role="tab" data-toggle="tab"><i
                                        class="fa fa-fw ti-comments"></i></a>
                            </li>
                            <li class="text-center">
                                <a href="#r_tab2" role="tab" data-toggle="tab"><i class="fa fa-fw ti-bell"></i></a>
                            </li>
                            <li class="text-center">
                                <a href="#r_tab3" role="tab" data-toggle="tab"><i
                                        class="fa fa-fw ti-settings"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="r_tab1">
                            <div id="slim_t1">
                                <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                    <i class="menu-icon  fa fa-fw ti-user"></i>
                                    Contacts
                                </h5>
                                <ul class="list-unstyled margin-none">
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar6.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-primary"></i>
                                            Annette
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-primary"></i>
                                            Jordan
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar2.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-primary"></i>
                                            Stewart
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar3.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-warning"></i>
                                            Alfred
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar4.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-danger"></i>
                                            Eileen
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar5.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-muted"></i>
                                            Robert
                                        </a>
                                    </li>
                                    <li class="rightsidebar-contact-wrapper">
                                        <a class="rightsidebar-contact" href="#">
                                            <img src="img/authors/avatar7.jpg"
                                                 class="img-circle pull-right" alt="avatar-image">
                                            <i class="fa fa-circle text-xs text-muted"></i>
                                            Cassandra
                                        </a>
                                    </li>
                                </ul>

                                <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                    <i class="fa fa-fw ti-export"></i>
                                    Recent Updates
                                </h5>
                                <div>
                                    <ul class="list-unstyled">
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-comments-smiley fa-fw text-primary"></i>
                                                New Comment
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-twitter-alt fa-fw text-success"></i>
                                                3 New Followers
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-email fa-fw text-info"></i>
                                                Message Sent
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-write fa-fw text-warning"></i>
                                                New Task
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-export fa-fw text-danger"></i>
                                                Server Rebooted
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-info-alt fa-fw text-primary"></i>
                                                Server Not Responding
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-shopping-cart fa-fw text-success"></i>
                                                New Order Placed
                                            </a>
                                        </li>
                                        <li class="rightsidebar-notification">
                                            <a href="#">
                                                <i class="fa ti-money fa-fw text-info"></i>
                                                Payment Received
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="r_tab2">
                            <div id="slim_t2">
                                <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                    <i class="fa fa-fw ti-bell"></i>
                                    Notifications
                                </h5>
                                <ul class="list-unstyled m-t-15 notifications">
                                    <li>
                                        <a href="#" class="message icon-not striped-col">
                                            <img class="message-image img-circle"
                                                 src="img/authors/avatar3.jpg" alt="avatar-image">

                                            <div class="message-body">
                                                <strong>John Doe</strong>
                                                <br>
                                                5 members joined today
                                                <br>
                                                <small class="noti-date">Just now</small>
                                            </div>

                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="message icon-not">
                                            <img class="message-image img-circle"
                                                 src="img/authors/avatar.jpg" alt="avatar-image">
                                            <div class="message-body">
                                                <strong>Tony</strong>
                                                <br>
                                                likes a photo of you
                                                <br>
                                                <small class="noti-date">5 min</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="message icon-not striped-col">
                                            <img class="message-image img-circle"
                                                 src="img/authors/avatar6.jpg" alt="avatar-image">

                                            <div class="message-body">
                                                <strong>John</strong>
                                                <br>
                                                Dont forgot to call...
                                                <br>
                                                <small class="noti-date">11 min</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="message icon-not">
                                            <img class="message-image img-circle"
                                                 src="img/authors/avatar1.jpg" alt="avatar-image">
                                            <div class="message-body">
                                                <strong>Jenny Kerry</strong>
                                                <br>
                                                Done with it...
                                                <br>
                                                <small class="noti-date">1 Hour</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="message icon-not striped-col">
                                            <img class="message-image img-circle"
                                                 src="img/authors/avatar7.jpg" alt="avatar-image">

                                            <div class="message-body">
                                                <strong>Ernest Kerry</strong>
                                                <br>
                                                2 members joined today
                                                <br>
                                                <small class="noti-date">3 Days</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="text-right noti-footer"><a href="#">View All Notifications <i
                                            class="ti-arrow-right"></i></a></li>
                                </ul>
                                <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                    <i class="fa fa-fw ti-check-box"></i>
                                    Tasks
                                </h5>
                                <ul class="list-unstyled m-t-15">
                                    <li>
                                        <div>
                                            <p>
                                                <span>Button Design</span>
                                                <small class="pull-right text-muted">40%</small>
                                            </p>
                                            <div class="progress progress-xs progress-striped active">
                                                <div class="progress-bar progress-bar-success"
                                                     role="progressbar"
                                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                                     style="width: 40%">
                                                    <span class="sr-only">40% Complete (success)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <p>
                                                <span>Theme Creation</span>
                                                <small class="pull-right text-muted">20%</small>
                                            </p>
                                            <div class="progress progress-xs progress-striped active">
                                                <div class="progress-bar progress-bar-info" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                     style="width: 20%">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <p>
                                                <span>XYZ Task To Do</span>
                                                <small class="pull-right text-muted">60%</small>
                                            </p>
                                            <div class="progress progress-xs progress-striped active">
                                                <div class="progress-bar progress-bar-warning"
                                                     role="progressbar"
                                                     aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                     style="width: 60%">
                                                    <span class="sr-only">60% Complete (warning)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <p>
                                                <span>Transitions Creation</span>
                                                <small class="pull-right text-muted">80%</small>
                                            </p>
                                            <div class="progress progress-xs progress-striped active">
                                                <div class="progress-bar progress-bar-danger" role="progressbar"
                                                     aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                                     style="width: 80%">
                                                    <span class="sr-only">80% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="text-right"><a href="#">View All Tasks <i
                                            class="ti-arrow-right"></i></a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="r_tab3">
                            <div id="slim_t3">
                                <h5 class="rightsidebar-right-heading text-uppercase gen-sett-m-t text-xs">
                                    <i class="fa fa-fw ti-settings"></i>
                                    General
                                </h5>
                                <ul class="list-unstyled settings-list m-t-10">
                                    <li>
                                        <label for="status">Available</label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="status" name="my-checkbox" checked>
                                </span>
                                    </li>
                                    <li>
                                        <label for="email-auth">Login with Email</label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="email-auth" name="my-checkbox">
                                </span>
                                    </li>
                                    <li>
                                        <label for="update">Auto Update</label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="update" name="my-checkbox">
                                </span>
                                    </li>

                                </ul>
                                <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                    <i class="fa fa-fw ti-volume"></i>
                                    Sound & Notification
                                </h5>
                                <ul class="list-unstyled settings-list m-t-10">
                                    <li>
                                        <label for="chat-sound">Chat Sound</label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="chat-sound" name="my-checkbox" checked>
                                </span>
                                    </li>
                                    <li>
                                        <label for="noti-sound">Notification Sound</label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="noti-sound" name="my-checkbox">
                                </span>
                                    </li>
                                    <li>
                                        <label for="remain">Remainder </label>
                                        <span class="pull-right">
                                    <input type="checkbox" id="remain" name="my-checkbox" checked>
                                </span>

                                    </li>
                                    <li>
                                        <label for="vol">Volume</label>
                                        <input type="range" id="vol" min="0" max="100" value="15">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="background-overlay"></div>
</section>
  @endsection


  @section('bagianfooter')

  <!-- end of global js -->
  <!-- begining of page level js -->
  <script src="{{ URL::asset('vendors/moment/js/moment.min.js') }}"></script>
  <script src="{{ URL::asset('vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/select2/js/select2.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
 <!-- end of page level js -->

  <script>

      $('#form-validation').bootstrapValidator({
          fields: {
              nama: {
                  validators: {
                      notEmpty: {
                          message: 'Name is required and cannot be empty'
                      }
                  }
              },
              email: {
                  validators: {
                      notEmpty: {
                          message: 'The email address is required'
                      },
                      regexp: {
                          regexp: /^\S+@\S{1,}\.\S{1,}$/,
                          message: 'Please enter valid email format'
                      }
                  }
              },
              phone: {
                  validators: {
                      notEmpty: {
                          message: 'Phone number is required and cannot be empty'
                      },
                      regexp: {
                          regexp: /[2-9]{2}\d{8}/,
                          message: 'The phone number can only consist of numbers'
                      }
                  }
              },
              username: {
                  validators: {
                      notEmpty: {
                          message: 'username is required and cannot be empty'
                      }
                  }
              }
          }
      }).on('reset', function (event) {
          $('#form-validation').data('bootstrapValidator').resetForm();
      });
$("#formpasswordlama").hide();
$('#newpassword').keyup(function(){ //the event here is change
    if ($(this).val().length >=1 ) //check the value into the select
    {
    $("#formpasswordlama").show();  }
    else
    {
      $("#formpasswordlama").hide();
    }
});
</script>

  @endsection
