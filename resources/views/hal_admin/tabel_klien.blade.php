@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Klien
        <small>Tabel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabel Klien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">

                        <div class="box">
                          <div class="box-header">
                            <h3 class="box-title">Tabel Klien</h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <a href="{{url('/admin')}}/klien/tambah">Tambah data</a>
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th></th>
                                <th>Nama</th>
                                <th>Saldo</th>
                                <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php


                                foreach ($dafklien as $klien) {

                                ?>
                              <tr>
                                <td><img src="{{ url('/') }}/gambarlokal/{{$klien->filefoto}}/w/50/h/50"/>  </td>
                                <td><?php  echo $klien->nama;?></td>
                                <td><?php  echo rupiah($klien->saldo);?></td>
                                <td>  <a href="{{ url('/') }}/admin/klien/edit/{{$klien->id}}">Edit</a>  <a href="{{ url('/') }}/admin/klien/delete/{{$klien->id}}">Delete</a> </td>
                              </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                              <tfoot>
                              <tr>
                                <th></th>
                                <th>Nama</th>
                                <th></th>
                              </tr>
                              </tfoot>
                            </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->

                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<!-- DataTables -->
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


  var ajaxku;
  function ceklist(idnya){
    ajaxku = buatajax();
    var url="{{ url('/') }}/admin/bukutamu/toggle/"+idnya;
    //url=url+"?q="+nip;
    //url=url+"&sid="+Math.random();
    ajaxku.onreadystatechange=stateChanged;
    ajaxku.open("GET",url,true);
    ajaxku.send(null);
  }
  function buatajax(){
    if (window.XMLHttpRequest){
      return new XMLHttpRequest();
    }
    if (window.ActiveXObject){
       return new ActiveXObject("Microsoft.XMLHTTP");
     }
     return null;
   }
   function stateChanged(){
     var data;
      if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>0){
          //document.getElementById("hasilkirim").html = data;

         }else{
          // document.getElementById("hasilkirim").html = "";
                //   $('#hasilkirim').html("");
         }
       }
  }
</script>
@endsection
