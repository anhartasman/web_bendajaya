@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Klien
        <small>Tambah </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/klien/list">Daftar Klien</a></li>
        <li class="active">Tambah Klien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penambahan Klien</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="add" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input name="nama" required id="nama" type="text" class="form-control" value="{{Request::old('nama')}}"  >
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" required id="email" type="email" class="form-control" value="{{Request::old('email')}}"  >
                      </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Foto Klien</label>
                    <input name="foto" type="file" id="foto">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input name="username" required id="username" type="text" class="form-control" value="{{Request::old('username')}}"  >
                    </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input name="password" id="password" required type="text" class="form-control" value="{{Request::old('password')}}" >
                </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

  <!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
 
</script>
@endsection
