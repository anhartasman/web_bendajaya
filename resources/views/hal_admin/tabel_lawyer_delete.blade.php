@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lawyer
        <small>Hapus </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/lawyer/list">Daftar Lawyer</a></li>
        <li class="active">Hapus Lawyer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penghapusan Lawyer</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="erase" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bidang hukum</label>
                    <select name="bidanghukum[]" disabled   class="form-control" id="bidanghukum" multiple="multiple">
                      <?php $katada=array();$i=0; ?>
                        @foreach($kategori as $kat)
                        <option value="{{$kat->id}}" selected="">{{$kat->jenis}}</option>
                        <?php
                          $katada[$i]=$kat->jenis;
                          $i+=1;
                        ?>
                        @endforeach
                        @foreach($dafcat as $cat)
                        @if (! in_array($cat->jenis, $katada))
                      <option value="{{$cat->id}}" >{{$cat->jenis}}</option>
                      @endif
                      @endforeach

                    </select>
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input name="nama" disabled id="nama" type="text" class="form-control" value="{{$nama}}"  >
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" disabled id="email" type="email" class="form-control" value="{{$email}}"  >
                      </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Harga per menit</label>
                    <input name="hargapermenit" disabled type="text" class="form-control" id="hargapermenit" value="{{$permenit}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Foto pengacara</label>
                    <br><img src="{{ url('/') }}/gambarlokal/{{$filefoto}}/w/150/h/150"/></br>

                  </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Hapus</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

  <!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$('#bidanghukum').select2({  tags:[],tokenSeparators: [",", " "]});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('keterangan');
    //bootstrap WYSIHTML5 - text editor
    $(".keterangan").wysihtml5();
  });
</script>
@endsection
