@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lawyer
        <small>Edit </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/lawyer/list">Daftar Lawyer</a></li>
        <li class="active">Edit Lawyer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Lawyer</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="save" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bidang hukum</label>
                    <select name="bidanghukum[]" required class="form-control" id="bidanghukum" multiple="multiple">
                      <?php $katada=array();$i=0; ?>
                        @foreach($kategori as $kat)
                        <option value="{{$kat->id}}" selected="">{{$kat->jenis}}</option>
                        <?php
                          $katada[$i]=$kat->jenis;
                          $i+=1;
                        ?>
                        @endforeach
                        @foreach($dafcat as $cat)
                        @if (! in_array($cat->jenis, $katada))
                      <option value="{{$cat->id}}" >{{$cat->jenis}}</option>
                      @endif
                      @endforeach

                    </select>
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input name="nama" required id="nama" type="text" class="form-control" value="{{$nama}}"  >
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" required id="email" type="email" class="form-control" value="{{$email}}"  >
                      </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Harga per menit</label>
                    <input name="hargapermenit" required type="text" class="form-control" id="hargapermenit" value="{{$permenit}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Foto pengacara</label>
                    <br><img src="{{ url('/') }}/gambarlokal/{{$filefoto}}/w/150/h/150"/></br>
                    <input name="foto" type="file" id="foto">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input name="username" required type="text" class="form-control" id="username" value="{{$username}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input name="password" required type="text" class="form-control" id="password" value="{{$password}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Firm latitude</label>
                    <input name="firmlatitude" required type="text" class="form-control" id="firmlatitude" value="{{$firmlatitude}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Firm longitude</label>
                    <input name="firmlongitude" required type="text" class="form-control" id="firmlongitude" value="{{$firmlongitude}}" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tampilkan posisi</label>
                    <select name="tampilkanposisi">
                      <option value="0" <?php if($tampilkanposisi==0){print("selected");}?>>Tidak</option>
                      <option value="1" <?php if($tampilkanposisi==1){print("selected");}?>>Hanya lawyer</option>
                      <option value="2" <?php if($tampilkanposisi==2){print("selected");}?>>Lawyer dan kantor</option>
                      <option value="3" <?php if($tampilkanposisi==3){print("selected");}?>>Hanya kantor</option>
                    </select>
                  </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

  <!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$('#bidanghukum').select2({  tags:[],tokenSeparators: [",", " "]});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('keterangan');
    //bootstrap WYSIHTML5 - text editor
    $(".keterangan").wysihtml5();
  });
</script>
@endsection
