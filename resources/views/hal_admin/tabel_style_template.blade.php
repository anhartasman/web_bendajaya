@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Template
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Templates</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->


        <div class="row">
          <div class="col-md-12">
            <div class="nav-tabs-custom">

              <div class="tab-content">

                          <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body">
                              <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Template</th>
                                  <th>Select</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  foreach ($daftemplate as $template) {
                                  ?>
                                <tr>
                                  <td><?php  echo $template->nama;?></td>
                                  <td><input @if($template->stat==1) checked @endif type="radio" name="color" value="{{$template->id}}"onclick="ceklist({{$template->id}})"></td>

                               </tr>
                                <?php
                                }
                                ?>
                              </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                          </div>
                  <!-- /.post -->

                  <!-- Post -->
                   <!-- Post
                  <div class="post">
                    <div class="user-block">

                          <span >
                            <a href="asdasdsad">Adam Jones</a>
                           </span>

                      <span >Posted 5 photos - 5 days ago</span>
                    </div>
                    -->
                    <!-- /.user-block -->

                    <!-- /.row -->
                    </div>
                  <!-- /.post -->
                </div>
                <!-- /.tab-pane -->
                <!-- /.tab-pane -->
              </div>

        </div>

            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom --> 

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<!-- DataTables -->
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


  var ajaxku;
  function ceklist(idnya){
     //alert ('dapat '+idnya);
    ajaxku = buatajax();
    var url="{{ url('/') }}/admin/elemenweb/style/template/select/"+idnya;
    //url=url+"?q="+nip;
    //url=url+"&sid="+Math.random();
    ajaxku.onreadystatechange=stateChanged;
    ajaxku.open("GET",url,true);
    ajaxku.send(null);
  }

  function buatajax(){
    if (window.XMLHttpRequest){
      return new XMLHttpRequest();
    }
    if (window.ActiveXObject){
       return new ActiveXObject("Microsoft.XMLHTTP");
     }
     return null;
   }
   function stateChanged(){
     var data;
      if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>0){
          //document.getElementById("hasilkirim").html = data;

         }else{
          // document.getElementById("hasilkirim").html = "";
                //   $('#hasilkirim').html("");
         }
       }
  }
  </script>
@endsection
