@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>{{$notrx}} </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/transaksi/airline/tran/tahun/{{date('Y',strtotime($tgl_bill_create))}}/bulan/{{date('m',strtotime($tgl_bill_create))}}/status/{{$status}}"> Tabel Transaksi</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->

    <section class="invoice">
      <!-- title row -->
      @if($errors->has())
         @foreach ($errors->all() as $error)
            <h1>{{ $error }}</h1>
        @endforeach
      @endif
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Trx #{{$notrx}}
            <small class="pull-right">Date: {{$tgl_bill_create}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Departure
          <address>
            <strong>{{$org}} - {{$des}}</strong><br>
            <strong>Kode Booking : {{$PNRDep}}</strong><br>
            <?php
            $dafdep = json_decode($daftranpergi, true);
            ?>

 					 @foreach($dafdep['jalurpergi'] as $trans)
             {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
             @endforeach

          </address>
        </div>
        <!-- /.col -->
        @if($flight=="R")
        <div class="col-sm-4 invoice-col">
          Return
          <address>
            <strong>{{$des}} - {{$org}} ({{$PNRRet}})</strong><br>
            <strong>Kode Booking : {{$PNRRet}}</strong><br>
            <?php
            $dafret = json_decode($daftranpulang, true);
            ?>

 					 @foreach($dafret['jalurpulang'] as $trans)
             {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
             @endforeach
          </address>
        </div>
        @endif
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #{{$notrx}}</b><br>
          <b>Order ID:</b> {{$id}}<br>
          <b>Payment Due:</b> {{$tgl_bill_exp}}<br>
          <b>Nama:</b> {{$cpname}}<br>
          <b>Telepon:</b> {{$cptlp}}<br>
          <b>Email:</b> {{$cpmail}}<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Tabel penumpang dewasa-->
      <?php $nomadt=0; ?>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Adt</th>
              <th>Nama</th>
              <th>Handphone</th>
            </tr>
            </thead>
            <tbody>
                @foreach($dafpendewasa as $pen)
                <?php $nomadt+=1; ?>
              <tr>
                <td>{{$nomadt}}</td>
                <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                <td>{{$pen->hp}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>

@if($jumchd>0)
            <!-- Tabel penumpang anak-->
            <?php $nomchd=0; ?>
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Chd</th>
                    <th>Nama</th>
                    <th>Lahir</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($dafpenanak as $pen)
                      <?php $nomchd+=1; ?>
                    <tr>
                      <td>{{$nomchd}}</td>
                      <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                      <td>{{$pen->birth}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
@endif

@if($juminf>0)
            <!-- Tabel penumpang bayi-->
            <?php $nominf=0; ?>
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Inf</th>
                    <th>Nama</th>
                    <th>Lahir</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($dafpenbayi as $pen)
                      <?php $nominf+=1; ?>
                    <tr>
                      <td>{{$nominf}}</td>
                      <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                      <td>{{$pen->birth}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
@endif
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Bukti Pembayaran</p>

          @if(count($dafbuk)>0)
          <?php $nobuk=0; ?>
         @foreach($dafbuk as $bukti)
         <?php $nobuk+=1;?>
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           Tanggal : {{date('d-m-Y H:i:s',strtotime($bukti->tanggal))}} <br>
           Rekening Asal : {{$bukti->rekasal_bank}} - {{$bukti->rekasal_norek}} A/N {{$bukti->rekasal_napem}} <br>
           Rekening Tujuan : {{$bukti->rektuju_bank}} - {{$bukti->rektuju_norek}} A/N {{$bukti->rektuju_napem}} <br>

           <img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="" style="width:200px;height:200px;">
           <br>
           @if($status==1 || $status==3)
           Terima? <input type="checkbox" name="pilihan" onclick="ceklist({{$bukti->id}})" value="{{$bukti->id}}" <?php  if($bukti->status==1){print("checked");}?>></td>
           @elseif($bukti->status==1)
           Sah
           @endif
          </p>

          @endforeach

          @if($status==1 || $status==3)
          <form role="form" method="post" action="../pilihkeputusan/{{$id}}"  enctype = "multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
            Proses <select name="keputusan">
            <option value="4">Tolak</option>
            <option value="2">Terima</option>
            <option value="3">Terima dengan syarat</option>
            </select>
            </div>
                  <div class="form-group">
                  <img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">
                  <input type="text" id="captcha" name="captcha">
                  </div>
          <div class="form-group">
            Catatan<br>
            <textarea name="catatan" rows="4" cols="50">{{$catatan}}</textarea>
          </div>
        <div class="form-group ">
          <button type="submit"style="width:50%;" class="btn btn-block btn-primary">Proses</button>
        </div>
      </form>
        @endif
                              @endif


        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Batas Pembayaran : {{$tgl_bill_exp}}</p>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Biaya:</th>
                <td>{{$biayanormal}}</td>
              </tr>
              <tr>
                <th>Kode Unik</th>
                <td>{{$kodeunik}}</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>{{$biaya}}</td>
              </tr>
              @if($catatan!=null)
              <tr>
                <th>Catatan:</th>
                <td>{{$catatan}}</td>
              </tr>
              @endif
            </table>
          </div>
@if($status==2)
            <p class="lead">Tanggal Terima : {{$tgl_bill_acc}}</p>

@endif
        </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">

          <a href="{{ url('/') }}/admin/transaksi/airline/view/{{$id}}/pdf" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;" ><i class="fa fa-download"></i> Generate PDF</a>

        </div>
      </div>
    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<script>
  var ajaxku;

  function cetakpdf(){
    window.location.href = "{{ url('/') }}/admin/transaksi/airline/view/{{$id}}/pdf";
  }
  function ceklist(idnya){
    ajaxku = buatajax();
    var url="{{ url('/') }}/admin/transaksi/airline/setbukti/tran/{{$id}}/id/"+idnya;
    //url=url+"?q="+nip;
    //url=url+"&sid="+Math.random();
    ajaxku.onreadystatechange=stateChanged;
    ajaxku.open("GET",url,true);
    ajaxku.send(null);
  }
  function buatajax(){
    if (window.XMLHttpRequest){
      return new XMLHttpRequest();
    }
    if (window.ActiveXObject){
       return new ActiveXObject("Microsoft.XMLHTTP");
     }
     return null;
   }
   function stateChanged(){
     var data;
      if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>0){
          //document.getElementById("hasilkirim").html = data;

         }else{
          // document.getElementById("hasilkirim").html = "";
                //   $('#hasilkirim').html("");
         }
       }
  }
</script>
@endsection
