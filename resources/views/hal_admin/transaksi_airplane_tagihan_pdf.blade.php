<?php
$infomember= DB::table('tb_infomember')->where('mmid',getUserInfo("user_mmid"))->first();

$dafdep = json_decode($daftranpergi, true);
$dafret = json_decode($daftranpulang, true);

$bataskiri=14;
FPDF::SetTitle("Trx ".$notrx);
FPDF::AddPage();
FPDF::SetFont('Arial','',12);
FPDF::SetY(10);
FPDF::SetX($bataskiri);
FPDF::Cell(40,10,$infomember->cont_namatravel);
FPDF::Line($bataskiri,18,80,18);
FPDF::SetFont('Arial','',8);
FPDF::SetY(16);
FPDF::SetX($bataskiri);
FPDF::Cell(2,10,$infomember->cont_alamat);
FPDF::SetY(19);
FPDF::SetX($bataskiri);
FPDF::Cell(2,10,'TELP : '.$infomember->cont_handphone);
FPDF::SetY(22);
FPDF::SetX($bataskiri);
FPDF::Cell(2,10,'EMAIL : '.$infomember->cont_email);
FPDF::SetFont('Arial','',12);

FPDF::SetY(28);
FPDF::SetX($bataskiri);
FPDF::Cell(20,10,'TRANSAKSI '.$notrx,0,0,'L');
FPDF::SetFont('Arial','',12);

FPDF::SetY(36);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,'PESANAN',1,0,'C');
FPDF::Cell(50,6,'TANGGAL',1,0,'C');
FPDF::Cell(90,6,'STATUS',1,0,'C');
FPDF::SetY(42);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,namatransaksi("AIR"),1,0,'L');
FPDF::Cell(50,6,$tgl_bill_create,1,0,'L');
FPDF::Cell(90,6,statustransaksi($status),1,0,'L');



FPDF::SetFont('Arial','',12);
FPDF::SetY(54);
FPDF::SetX($bataskiri);
FPDF::Cell(70,6,'NAMA',1,0,'C');
FPDF::Cell(70,6,'EMAIL',1,0,'C');
FPDF::Cell(40,6,'NO. TELEPON',1,0,'C');
FPDF::SetY(60);
FPDF::SetX($bataskiri);
FPDF::Cell(70,6,$cpname,1,0,'L');
FPDF::Cell(70,6,$cpmail,1,0,'L');
FPDF::Cell(40,6,$cptlp,1,0,'L');


FPDF::SetY(72);
FPDF::SetX($bataskiri);
FPDF::Cell(180,6,"Penerbangan pergi (".airport_namakota($org)." - ".airport_namakota($des).") : ".DateToIndoTgl($tgl_dep),1,0,'L');
FPDF::SetY(78);
FPDF::SetX($bataskiri);
FPDF::Cell(10,6,'No',1,0,'C');
FPDF::Cell(30,6,'Pesawat',1,0,'C');
FPDF::Cell(60,6,'Jalur',1,0,'C');
FPDF::Cell(40,6,'Jam berangkat',1,0,'C');
FPDF::Cell(40,6,'Jam tiba',1,0,'C');


$nilaiY=84;
$nilaiYawal=$nilaiY;
FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
$i=0;
foreach($dafdep['jalurpergi'] as $trans){
$i+=1;

FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
FPDF::Cell(10,6,$i,1,0,'C');
FPDF::Cell(30,6,$trans['FlightNo'],1,0,'C');
FPDF::Cell(60,6,airport_namakota($trans['STD'])." - ".airport_namakota($trans['STA']),1,0,'C');
FPDF::Cell(40,6,DateToIndo($trans['ETD']),1,0,'C');
FPDF::Cell(40,6,DateToIndo($trans['ETA']),1,0,'C');
$nilaiY+=6;
}
if($flight=="R"){
  $nilaiY+=6;
  FPDF::SetY($nilaiY);
  FPDF::SetX($bataskiri);
  FPDF::Cell(180,6,"Penerbangan pulang (".airport_namakota($des)." - ".airport_namakota($org).") : ".DateToIndoTgl($tgl_ret),1,0,'L');

    $nilaiY+=6;
    FPDF::SetY($nilaiY);
  FPDF::SetX($bataskiri);
  FPDF::Cell(10,6,'No',1,0,'C');
  FPDF::Cell(30,6,'Pesawat',1,0,'C');
  FPDF::Cell(60,6,'Jalur',1,0,'C');
  FPDF::Cell(40,6,'Jam berangkat',1,0,'C');
  FPDF::Cell(40,6,'Jam tiba',1,0,'C');
  $nilaiY+=6;

  FPDF::SetY($nilaiY);
  FPDF::SetX($bataskiri);
  $i=0;
  foreach($dafret['jalurpulang'] as $trans){
  $i+=1;

  FPDF::SetY($nilaiY);
  FPDF::SetX($bataskiri);
  FPDF::Cell(10,6,$i,1,0,'C');
  FPDF::Cell(30,6,$trans['FlightNo'],1,0,'C');
  FPDF::Cell(60,6,airport_namakota($trans['STD'])." - ".airport_namakota($trans['STA']),1,0,'C');
  FPDF::Cell(40,6,DateToIndo($trans['ETD']),1,0,'C');
  FPDF::Cell(40,6,DateToIndo($trans['ETA']),1,0,'C');
  $nilaiY+=6;
  }
}
$nilaiY+=6;
FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
$labelpen="Penumpang : ".$jumadt." dewasa";
if($jumchd>0){
  $labelpen.=", ".$jumchd." anak";
}
if($juminf>0){
  $labelpen.=", ".$juminf." bayi";
}
FPDF::Cell(180,6,$labelpen,1,0,'L');
$nilaiY+=6;

$nom=0;
$nomadt=0;
$nomchd=0;
$nominf=0;
foreach($dafpendewasa as $pen){
$nom+=1;
$nomadt+=1;
FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
FPDF::Cell(180,6,$nom.". $pen->tit $pen->fn $pen->ln, $pen->hp",1,0,'L');
$nilaiY+=6;
}
foreach($dafpenanak as $pen){
$nom+=1;
$nomchd+=1;
FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
FPDF::Cell(180,6,$nom.". $pen->tit $pen->fn $pen->ln, ".DateToIndoTgl($pen->birth),1,0,'L');
$nilaiY+=6;
}
foreach($dafpenbayi as $pen){
$nom+=1;
$nominf+=1;
FPDF::SetY($nilaiY);
FPDF::SetX($bataskiri);
FPDF::Cell(180,6,$nom.". $pen->tit $pen->fn $pen->ln, ".DateToIndoTgl($pen->birth),1,0,'L');
$nilaiY+=6;
}
FPDF::Line($bataskiri,$nilaiY,194,$nilaiY);

FPDF::SetY(240);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,'Biaya',0,0,'L');
FPDF::Cell(10,6,': Rp',0,0,'L');
FPDF::Cell(50,6,rupiahNoRp($biayanormal_nominal),1,0,'R');

FPDF::SetY(246);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,'Kode Unik',0,0,'L');
FPDF::Cell(10,6,': ',0,0,'L');
FPDF::Cell(50,6,$kodeunik,1,0,'R');

FPDF::SetY(252);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,'Total',0,0,'L');
FPDF::Cell(10,6,': Rp',0,0,'L');
FPDF::Cell(50,6,rupiahNoRp($biaya_nominal),1,0,'R');

FPDF::SetY(258);
FPDF::SetX($bataskiri);
FPDF::Cell(40,6,'Terbilang',0,0,'L');
FPDF::Cell(10,6,':  ',0,0,'L');
FPDF::SetFont('Arial','',8);
FPDF::Cell(130,6,terbilang($biaya_nominal, $style=4)." rupiah",1,0,'L');


FPDF::Output();
exit;
 ?>
