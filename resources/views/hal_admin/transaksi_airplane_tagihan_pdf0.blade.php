<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body >
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          Trx No #{{$notrx}}
          <br><small class="pull-right">{{$org}} - {{$des}} ({{$flight}})</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <strong>Departure</strong>
        <address>
          {{$org}} - {{$des}}<br>
          @if($PNRDep!=null)<strong>Kode Booking</strong> : {{$PNRDep}}<br>@endif
          <strong>Penerbangan</strong><br>
          <?php
          $dafdep = json_decode($daftranpergi, true);
          ?>

         @foreach($dafdep['jalurpergi'] as $trans)
           {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
           @endforeach

        </address>
      </div>
      <!-- /.col -->
      @if($flight=="R")
      <div class="col-sm-4 invoice-col">
        Return
        <address>
          <strong>{{$des}} - {{$org}} ({{$PNRRet}})</strong><br>
          <strong>Kode Booking : {{$PNRRet}}</strong><br>
          <?php
          $dafret = json_decode($daftranpulang, true);
          ?>

         @foreach($dafret['jalurpulang'] as $trans)
           {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})<br>
           @endforeach
        </address>
      </div>
      @endif
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Payment Due:</b> {{$tgl_bill_exp}}<br>
        <b>Nama:</b> {{$cpname}}<br>
        <b>Telepon:</b> {{$cptlp}}<br>
        <b>Email:</b> {{$cpmail}}<br>
      </div>
      <div class="col-sm-4 invoice-col">
        <b>Daftar penumpang </b> <br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <!-- Tabel penumpang dewasa-->
    <?php $nomadt=0; ?>
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Adt</th>
            <th>Nama</th>
            <th>Handphone</th>
          </tr>
          </thead>
          <tbody>
              @foreach($dafpendewasa as $pen)
              <?php $nomadt+=1; ?>
            <tr>
              <td>{{$nomadt}}</td>
              <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
              <td>{{$pen->hp}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>

@if($jumchd>0)
          <!-- Tabel penumpang anak-->
          <?php $nomchd=0; ?>
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Chd</th>
                  <th>Nama</th>
                  <th>Lahir</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($dafpenanak as $pen)
                    <?php $nomchd+=1; ?>
                  <tr>
                    <td>{{$nomchd}}</td>
                    <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                    <td>{{$pen->birth}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
@endif

@if($juminf>0)
          <!-- Tabel penumpang bayi-->
          <?php $nominf=0; ?>
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Inf</th>
                  <th>Nama</th>
                  <th>Lahir</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($dafpenbayi as $pen)
                    <?php $nominf+=1; ?>
                  <tr>
                    <td>{{$nominf}}</td>
                    <td>{{$pen->tit}} {{$pen->fn}} {{$pen->ln}} </td>
                    <td>{{$pen->birth}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
@endif
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">



      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <p class="lead"><B>Batas Pembayaran</B> : {{$tgl_bill_exp}}</p>
        <p class="lead"><B>Biaya</B> : {{$biaya}}</p>
        <p class="lead"><B>Catatan</B> : {{$catatan}}</p>

@if($status==2)
          <p class="lead">Tanggal Terima : {{$tgl_bill_acc}}</p>

@endif
      </div>

      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
