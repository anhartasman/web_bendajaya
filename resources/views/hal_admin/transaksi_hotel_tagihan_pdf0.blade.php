<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body >
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          Trx No #{{$notrx}}
          <br><small class="pull-right">tes</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        <strong>Departure</strong>
        <address>
          tes<br>
         <strong>Kode Booking</strong> : tes<br>
          <strong>Kereta</strong><br>
         ({{$tgl_checkin}}) -  ({{$tgl_checkout}})<br>

        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Payment Due:</b> {{$tgl_bill_exp}}<br>
        <b>Nama:</b> {{$cpname}}<br>
        <b>Telepon:</b> {{$cptlp}}<br>
        <b>Email:</b> {{$cpmail}}<br>
      </div>
      <div class="col-sm-4 invoice-col">
        <b>Daftar penumpang </b> <br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <!-- Tabel penumpang dewasa-->
    <?php $nom=0; ?>
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Adt</th>
            <th>Nama</th>
            <th>Handphone</th>
          </tr>
          </thead>
          <tbody>
            @foreach($dafkamar as $daf)
            <?php $nom+=1; ?>
          <tr>
            <td>{{$nom}}</td>
            <td>{{$daf->kategori}} {{$daf->board}} {{$daf->bed}} </td>
            <td>{{$daf->price}}</td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
 
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">



      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <p class="lead"><B>Batas Pembayaran</B> : {{$tgl_bill_exp}}</p>
        <p class="lead"><B>Biaya</B> : {{$biaya}}</p>
        <p class="lead"><B>Catatan</B> : {{$catatan}}</p>

@if($status==2)
          <p class="lead">Tanggal Terima : {{$tgl_bill_acc}}</p>

@endif
      </div>

      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
