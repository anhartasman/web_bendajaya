@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi
        <small>Tabel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabel Transaksi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">

                        <div class="box">
                          <div class="box-header">
                            <h3 class="box-title">Tabel transaksi {{$kon}}</h3>

                          </div>
                          <?php
                          $thn=date('Y');
                          $bln=date('m');
                          if(isset($otomatiscari)){
                            $bln=$blnpilihan;
                            $thn=$thnpilihan;
                          }

                          $bulan[1]="Januari";
                          $bulan[2]="Februari";
                          $bulan[3]="Maret";
                          $bulan[4]="April";
                          $bulan[5]="Mei";
                          $bulan[6]="Juni";
                          $bulan[7]="Juli";
                          $bulan[8]="Agustus";
                          $bulan[9]="September";
                          $bulan[10]="Oktober";
                          $bulan[11]="November";
                          $bulan[12]="Desember";
                          ?>
                            <div class="box-header">
                               Tahun <select id="pilthn">@foreach ($years as $year)<option value="{{$year->tahun}}" @if($year->tahun==$thn) selected @endif >{{$year->tahun}}</option>@endforeach</select>
                               Bulan <select id="pilbln"><option value="0"></option>@foreach ($dafbul as $month)<option value="{{$month->bulan}}" @if($month->bulan==$bln) selected @endif >{{$bulan[$month->bulan]}}</option>@endforeach</select>

                            </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th>Tanggal</th>
                                <th>No. Trx</th>
                                <th>Biaya</th>
                                <th>Nama</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th></th>
                              </tr>
                              </thead>
                              <tbody>

                            </tbody>
                              <tfoot>
                              <tr>
                                <th>Tanggal</th>
                                <th>No. Trx</th>
                                <th>Biaya</th>
                                <th>Nama</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th></th>
                              </tr>
                              </tfoot>
                            </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->

                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<!-- DataTables -->
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
var thn="{{$thn}}";
var bln="{{$bln}}";
  		var ajaxku;
      function ambildata(){

        ajaxku = buatajax();
        var url="{{ url('/') }}/admin/transaksi/train/caritran/{{$status}}/"+thn+"/"+bln;
        //url=url+"?q="+nip;
        //url=url+"&sid="+Math.random();
        ajaxku.onreadystatechange=stateChanged;
        ajaxku.open("GET",url,true);
        ajaxku.send(null);

      }
      function buatajax(){
        if (window.XMLHttpRequest){
          return new XMLHttpRequest();
        }
        if (window.ActiveXObject){
           return new ActiveXObject("Microsoft.XMLHTTP");
         }
         return null;
       }
       function stateChanged(){
         var datar;
          if (ajaxku.readyState==4){
            datar=ajaxku.responseText;
            //  alert(datar);
            if(datar.length>0){
                //    alert('ada');
              //document.getElementById("hasilkirim").html = data;
            //  $('#bodyexample1').html(data);
            var string = "UpdateBootProfile,PASS,00:00:00";
        var array = datar.split(",");
            var data = [
                array
            ];
            $('#example1').dataTable().fnClearTable();
            $('#example1').dataTable().fnAddData(JSON.parse(datar));
            /**
            var testsTable = $('#example1').dataTable({
                    bJQueryUI: true,
                    aaData: JSON.parse(datar)
            });
            **/
             }else{
              // document.getElementById("hasilkirim").html = "";
                    //   $('#hasilkirim').html("");
             }
           }
      }

  $(function () {
    $("#example1").DataTable({
      "order": [[ 0, "desc" ]]
    });

    $("#pilthn").on( 'change', function () {

                               setTahun();
                           } );
    $("#pilbln").on( 'change', function () {
                             setBulan();
                         } );

  ambildata();
  });

  @foreach ($years as $year)
  var bul{{$year->tahun}} ="@foreach ($year->months as $month)<option value='{{$month->bulan}}'>{{$bulan[$month->bulan]}}</option>@endforeach <option value=0></option>";
  @endforeach

function setTahun(){
  thn=$("#pilthn").find(":selected").text();
  bln="0";
  var as=eval('bul'+thn);
  var asa=as.split(",");
  $('#pilbln').find('option').remove();
  $('#pilbln').append(eval('bul'+thn));
  $("#pilbln").val('0');

 ambildata();
}
function setBulan(){

  thn=$("#pilthn").find(":selected").text();
  bln=$("#pilbln").find(":selected").val();
  ambildata();
}
</script>
@endsection
