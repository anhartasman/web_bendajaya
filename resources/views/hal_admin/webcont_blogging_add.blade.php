@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Artikel
        <small>Tambah</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/')}}/admin/blogging"> Daftar Artikel</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penambahan Artikel</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="add" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input name="judul" type="text" class="form-control" id="exampleInputEmail1" value="{{Request::old('judul')}}" >
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kategori</label>
                     <select name="kategori[]" class="form-control" id="kategori" multiple="multiple">
                      @foreach($dafcat as $cat)
                       <option value="{{$cat->id}}" @if(Request::old('kategori')!=null && in_array($cat->id,Request::old('kategori'))) selected @endif>{{$cat->category}}</option>
                       @endforeach

                     </select>
                   </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Artikel</label>
                    <textarea  id="isi" name="isi" class="form-control">{{Request::old('isi')}}</textarea>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button id="button" type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

<!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$("#button").click(function(){
      // alert($("#kategori").val());
});

  $('#kategori').select2({  tags:[],tokenSeparators: [",", " "]});
  //$("#kategori").append($('<option>', {value: 1, text: 'new option'}));
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('isi');
    //bootstrap WYSIHTML5 - text editor
    $(".isi").wysihtml5();
  });
</script>
@endsection
