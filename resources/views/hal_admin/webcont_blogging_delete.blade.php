@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Artikel
        <small>Hapus </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/')}}/admin/blogging/tahun/{{date('Y',strtotime($tanggal))}}/bulan/{{date('m',strtotime($tanggal))}}"> Daftar Artikel</a></li>
        <li class="active">Hapus</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penghapusan Artikel</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../erase/<?php print($idartikel);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <div><?php print($judul);?></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Artikel</label>
                    <div><?php print(nl2br($isi));?></div>
                  </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Confirm Delete</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
