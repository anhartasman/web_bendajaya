@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/')}}/admin/blogging/tahun/{{date('Y',strtotime($tanggal))}}/bulan/{{date('m',strtotime($tanggal))}}"> Daftar Artikel</a></li>
        <li class="active">Tambah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Artikel</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../save/<?php print($idartikel);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input name="judul" type="text" class="form-control" value="<?php print($judul);?>" id="exampleInputEmail1" >
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kategori</label>
                      <select name="kategori[]" class="form-control" id="kategori" multiple="multiple">
                        <?php $katada=array();$i=0; ?>
                          @foreach($kategori as $kat)
                          <option value="{{$kat->id}}" selected="">{{$kat->category}}</option>
                          <?php
                            $katada[$i]=$kat->category;
                            $i+=1;
                          ?>
                          @endforeach
                          @foreach($dafcat as $cat)
                          @if (! in_array($cat->category, $katada))
                        <option value="{{$cat->id}}">{{$cat->category}}</option>
                        @endif
                        @endforeach

                      </select>
                    </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Artikel</label>
                    <textarea name="isi" class="form-control"><?php print($isi);?></textarea>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

  <!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$('#kategori').select2({  tags:[],tokenSeparators: [",", " "]});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('keterangan');
    //bootstrap WYSIHTML5 - text editor
    $(".keterangan").wysihtml5();
  });
</script>
@endsection
