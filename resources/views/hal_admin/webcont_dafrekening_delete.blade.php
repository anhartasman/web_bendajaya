@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Rekening
        <small>Delete </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/infoweb/rekening">Rekening</a></li>
        <li class="active">Delete</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penghapusan Rekening</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../erase/{{$id}}" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bank</label>
                    <input name="bank" type="text" class="form-control" id="exampleInputEmail1" value="{{$bank}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Rekening</label>
                    <input name="norek" type="text" class="form-control" id="exampleInputEmail1" value="{{$norek}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Atas Nama</label>
                    <input name="napem" type="text" class="form-control" id="exampleInputEmail1" value="{{$napem}}">
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Konfirm Hapus</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
