@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FAQ
        <small>Delete </small>
      </h1>
      <ol class="breadcrumb">
                <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ url('/') }}/admin/infoweb/faq">FAQ</a></li>
                <li class="active">Delete</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penghapusan FAQ</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../erase/<?php print($idfaq);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pertanyaan</label>
                    <div><?php print($pertanyaan);?></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Jawaban</label>
                    <div><?php print(nl2br($jawaban));?></div>
                  </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Confirm Delete</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
