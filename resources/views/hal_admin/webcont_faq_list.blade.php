@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FAQ
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">FAQ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <div class="box-header with-border">
              <h3 class="box-title"><a href="{{ url('/') }}/admin/infoweb/faq/tambah">Tambah</a>
</h3>
            </div>

            <div class="tab-content">

                        <div class="box">
                          <!-- /.box-header -->
                          <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th>Pertanyaan</th>
                                <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php


                                foreach ($isitabel as $masukan) {

                                ?>
                              <tr>
                                <td><?php  echo $masukan->pertanyaan;?></td>
                               <td>
                                <a href="{{ url('/') }}/admin/infoweb/faq/view/{{$masukan->id}}">View</a>
                                <a href="{{ url('/') }}/admin/infoweb/faq/edit/{{$masukan->id}}">Edit</a>
                                <a href="{{ url('/') }}/admin/infoweb/faq/delete/{{$masukan->id}}">Delete</a>
                              </td>
                              </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                            </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->

                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
@endsection
@section('bagianfooter')
<!-- DataTables -->
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


  var ajaxku;
  function ceklist(idnya){
    ajaxku = buatajax();
    var url="{{ url('/') }}/admin/bukutamu/toggle/"+idnya;
    //url=url+"?q="+nip;
    //url=url+"&sid="+Math.random();
    ajaxku.onreadystatechange=stateChanged;
    ajaxku.open("GET",url,true);
    ajaxku.send(null);
  }
  function buatajax(){
    if (window.XMLHttpRequest){
      return new XMLHttpRequest();
    }
    if (window.ActiveXObject){
       return new ActiveXObject("Microsoft.XMLHTTP");
     }
     return null;
   }
   function stateChanged(){
     var data;
      if (ajaxku.readyState==4){
        data=ajaxku.responseText;
        if(data.length>0){
          //document.getElementById("hasilkirim").html = data;

         }else{
          // document.getElementById("hasilkirim").html = "";
                //   $('#hasilkirim').html("");
         }
       }
  }
  </script>
  @endsection
