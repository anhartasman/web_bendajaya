@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Galeri
        <small>Photos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->
                  <div class="row margin-bottom">

                    <?php

                     foreach ($fotos as $foto) {

                    ?>
                    <!-- /.col -->
                    <div class="col-sm-3">  <a href="{{ url('/') }}/admin/elemenweb/galeri/view/<?php  echo $foto->id;?>"><?php  echo $foto->namafoto;?></a>
                      <div class="row">
                        <!-- /.col -->
                        <div class="col-sm-11">
                          <a href="{{ url('/') }}/admin/elemenweb/galeri/view/<?php  echo $foto->id;?>"><img class="img-responsive" src="{{ url('/') }}/uploads/images/<?php print($foto->gambar);?>" style="width:304px;height:200px;padding-right:10px;padding-bottom:10px;" alt="Photo"></a>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->

         <?php
         }
          ?>
                  </div>
                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom --> 

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
