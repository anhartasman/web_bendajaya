@extends('layouts.masteradmin')

@section('kontenweb')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Galeri
        <small>Upload foto </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/elemenweb/galeri">Galeri</a></li>
        <li class="active">Upload foto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->
                  <div class="row margin-bottom">

                    <!-- /.col -->
                    <div class="col-sm-10">
                      @if($errors->has())
                               @foreach ($errors->all() as $error)
                               <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ $error }}
                              </div>
                              @endforeach
                       @endif
                      <form role="form" method="post" action="add" enctype = "multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Foto</label>
                          <input name="nama" type="text" class="form-control" value="{{Request::old('nama')}}" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Kategori</label>
                          <select name="kategori[]" class="form-control" id="kategori" multiple="multiple">
                           @foreach($dafcat as $cat)
                            <option value="{{$cat->id}}" @if(Request::old('kategori')!=null && in_array($cat->id,Request::old('kategori'))) selected @endif>{{$cat->category}}</option>
                            @endforeach

                          </select>
                        </div>
                      <!-- /.row -->

                                        <div class="form-group">
                                          <label for="exampleInputFile">Upload Foto</label>
                                          <input name="foto" type="file" id="exampleInputFile">
                                        </div>

                                        <div class="form-group">
                                          <label for="exampleInputPassword1">Keterangan Foto</label>
                                          <textarea name="keterangan" class="form-control">{{Request::old('keterangan')}}</textarea>
                                        </div>
<br>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
                    </div>
                    <!-- /.col -->

                  </div>
                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<link href="{{ URL::asset('dist/css/select2blog.min.css')}}" rel="stylesheet" />
<script src="{{ URL::asset('dist/js/select2.min.js')}}"></script>

  <!-- CK Editor -->
<script src="{{ URL::asset('dist/js/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$('#kategori').select2({  tags:[],tokenSeparators: [",", " "]});

  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('keterangan');
    //bootstrap WYSIHTML5 - text editor
    $(".keterangan").wysihtml5();
  });
</script>
@endsection
