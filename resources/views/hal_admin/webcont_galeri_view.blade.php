@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/elemenweb/galeri">Galeri</a></li>
        <li class="active">View foto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->
                  <div class="row margin-bottom">

                    <!-- /.col -->
                    <div class="col-sm-10">  <h1><?php  echo $nama;?></h1>
                      <div class="row">
                        <!-- /.col -->
                        <div class="col-sm-11">
                          Kategori :
                          @foreach($kategoris as $kat)
                          {{$kat->category}},
                          @endforeach
                        </div>
                        <div class="col-sm-11">
                         <img class="img-responsive" src="{{ url('/') }}/uploads/images/<?php print($filegambar);?>"   alt="Photo">
                        </div>
                        <!-- /.col -->
                      </div>
                      <div><?php  print(nl2br($keterangan)); ;?></div>
                      <!-- /.row -->
<br>
                      <div><a href="{{ url('/') }}/admin/elemenweb/galeri/edit/<?php print($idfoto);?>" class="btn btn-default btn-flat">Edit</a></div>
                      <div><br></div>

                      <div><a href="{{ url('/') }}/admin/elemenweb/galeri/delete/<?php print($idfoto);?>"class="btn btn-default btn-flat">Hapus</a></div>

                    </div>
                    <!-- /.col -->

                  </div>
                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom --> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
