@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kontak
        <small>Setting </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kontak</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Info Kontak</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="kontak/save" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul web</label>
                    <input name="judulweb" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_judulweb);?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Deskripsi web</label>
                    <input name="deskripsi" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_deskripsi);?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Travel</label>
                    <input name="namatravel" type="text" class="form-control" id="namatravel" value="<?php print($infomember->cont_namatravel);?>">
                  </div>
                  <div class="form-group">
                  <label for="exampleInputPassword1">Alamat</label>
                  <textarea name="alamat" class="form-control"><?php print($infomember->cont_alamat);?></textarea>
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Handphone</label>
                      <input name="handphone" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_handphone);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input name="email" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_email);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Facebook</label>
                      <input name="facebook" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_facebook);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Twitter</label>
                      <input name="twitter" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_twitter);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Instagram</label>
                      <input name="instagram" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_instagram);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Google plus</label>
                      <input name="googleplus" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_googleplus);?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Linkedin</label>
                      <input name="linkedin" type="text" class="form-control" id="exampleInputEmail1" value="<?php print($infomember->cont_linkedin);?>">
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
