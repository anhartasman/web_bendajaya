@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Logo
        <small>Upload </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Logo web</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Logo Website</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="logoweb/save" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="row">
                <!-- /.col -->
                <div class="col-sm-11">
                 <img class="img-responsive" src="{{ url('/') }}/uploads/images/logoweb{{getUserInfo("user_mmid")}}.png"  style="width:304px;height:200px;"   alt="Logo">
                </div>
                <!-- /.col -->
              </div>
                <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputFile">Upload Foto (Hanya PNG)</label>
                  <input name="foto" type="file" id="foto">

                </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
