@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Policies
        <small>Edit </small>
      </h1>
      <ol class="breadcrumb">

          <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{ url('/') }}/admin/infoweb/policies">Policies</a></li>
          <li class="active">Edit</li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Policy</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../save/<?php print($idpolicy);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input name="judul" type="text" class="form-control" value="<?php print($judul);?>" id="exampleInputEmail1" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Isi</label>
                    <textarea name="isi" class="form-control"><?php print($isi);?></textarea>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('bagianfooter')
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('isi');
    //bootstrap WYSIHTML5 - text editor
    $(".isi").wysihtml5();
  });
</script>
@endsection
