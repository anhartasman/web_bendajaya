@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services
        <small>Delete</small>
      </h1>
      <ol class="breadcrumb"><li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li><a href="{{ url('/') }}/admin/infoweb/services">Services</a></li>
                  <li class="active">Delete</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penghapusan Services</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../erase/<?php print($idservis);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Servis </label>
                    <div><?php print($namaservis);?></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Keterangan</label>
                    <div><?php print(nl2br($keteranganservis));?></div>
                  </div>
                  <img src="{{ url('/') }}/uploads/images/<?php print($namafilegambar);?>" alt="Mountain View" style="width:200px;height:200px;">


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Confirm Delete</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
