@extends('layouts.masteradmin')

@section('kontenweb')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services
        <small>Edit </small>
      </h1>
      <ol class="breadcrumb">
                <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ url('/') }}/admin/infoweb/services">Services</a></li>
                <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Services</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../save/<?php print($idservis);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Servis </label>
                    <input name="namaservis" type="text" class="form-control" value="<?php print($namaservis);?>" id="exampleInputEmail1" placeholder="Nama Servis">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Keterangan</label>

                    <input name="keteranganservis" type="text" class="form-control" id="keteranganservis" placeholder="Keterangan Servis" value="<?php print($keteranganservis);?>" >

                  </div>
                  <img src="{{ url('/') }}/uploads/images/<?php print($namafilegambar);?>" alt="Mountain View" style="width:200px;height:200px;">

                  <div class="form-group">
                    <label for="exampleInputFile">Logo Servis</label>
                    <input name="logoservis" type="file" id="exampleInputFile">

                    <p class="help-block">Example block-level help text here.</p>
                  </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('bagianfooter')
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('keteranganservisaa');
    //bootstrap WYSIHTML5 - text editor
    $(".keteranganservisaa").wysihtml5();
  });
</script>
@endsection
