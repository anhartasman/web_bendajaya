@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Slider
        <small>View </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/elemenweb/slider">Galeri Slider</a></li>
        <li class="active">View Slider</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">

            <div class="tab-content">
                <!-- /.post -->

                <!-- Post -->
                 <!-- Post
                <div class="post">
                  <div class="user-block">

                        <span >
                          <a href="asdasdsad">Adam Jones</a>
                         </span>

                    <span >Posted 5 photos - 5 days ago</span>
                  </div>
                  -->
                  <!-- /.user-block -->
                  <div class="row margin-bottom">

                    <!-- /.col -->
                    <div class="col-sm-3">  <h1><?php  echo $nama;?></h1>
                      <div class="row">
                        <!-- /.col -->
                        <div class="col-sm-11">
                         <img class="img-responsive" src="{{ url('/') }}/uploads/images/<?php print($filegambar);?>"   alt="Photo">
                        </div>
                        <!-- /.col -->
                      </div>
                      <div><?php  print($keterangan); ;?></div>
                      <!-- /.row -->
<br>
                      <div><a href="{{ url('/') }}/admin/elemenweb/slider/edit/<?php print($idfoto);?>" class="btn btn-default btn-flat">Edit</a></div>
                      <div><br></div>

                      <div><a href="{{ url('/') }}/admin/elemenweb/slider/delete/<?php print($idfoto);?>"class="btn btn-default btn-flat">Hapus</a></div>

                    </div>
                    <!-- /.col -->

                  </div>
                  <!-- /.row -->
                  </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->  

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
