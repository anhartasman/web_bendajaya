@extends('layouts.masteradmin')

@section('kontenweb')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Team
        <small>Edit </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('/') }}/admin/infoweb/team">Team</a></li>
        <li class="active">Edit</li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pengeditan Anggota Tim</h3>
              </div>
              @if($errors->has())
                       @foreach ($errors->all() as $error)
                       <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $error }}
                      </div>
                      @endforeach
               @endif
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="../save/<?php print($idteam);?>" enctype = "multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Anggota </label>
                    <input name="namaanggota" type="text" class="form-control" value="<?php print($namaanggota);?>" id="exampleInputEmail1" >
                  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jabatan </label>
                      <input name="jabatananggota" type="text" class="form-control" value="<?php print($jabatananggota);?>" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email Anggota </label>
                      <input name="emailanggota" type="text" class="form-control" value="<?php print($emailanggota);?>" id="exampleInputEmail1" >
                    </div>
                  <img src="{{ url('/') }}/uploads/images/<?php print($namafilegambar);?>" alt="Mountain View" style="width:200px;height:200px;">

                  <div class="form-group">
                    <label for="exampleInputFile">Foto Anggota</label>
                    <input name="fotoanggota" type="file" id="exampleInputFile">

                    <p class="help-block">Example block-level help text here.</p>
                  </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
