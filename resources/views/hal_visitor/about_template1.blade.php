@extends('layouts.'.$namatemplate)
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/inner/bg_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
		  				<li><span>tentang kami</span></li>
		  			</ul>
		  			<h2 class="color-white">tentang kami</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-wraper padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">Pengenalan</h4>
				</div>
			</div>
		</div>
		<div class="row">
									<p><?php print($infowebsite['about']); ?></p>
	</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">Jasa kami</h4>
					<h2>kami yang terbaik</h2>
				</div>
			</div>
		</div>
		<div class="row">

			@foreach($services as $servis)
			<div class="col-xs-12 col-sm-4">
				<div class="team-entry style-2">
					<h3 class="team-name color-dark-2">{{$servis->namaservis}}</h3>
					<h5 class="team-position color-dark-2-light"></h5>
					<div class="image">
					   <img class="team-img img-responsive" width="370" height="334" src="{{url('/')}}/gambarlokal/{{$servis->gambar}}/w/370/h/334" alt="">
					</div>
					<p class="color-dark-2-light">{{$servis->keteranganservis}}</p>
					<div class="team-social">
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div>

<!-- TESTIMONALS -->
<div class="testimonials">
    <div class="arrows">
	  	<div class="clip">
		  	<div class="bg bg-bg-chrome" style="background-image:url({{ URL::asset('asettemplate1/img/inner/bg_4.jpg') }})">
		  	</div>
	   	</div>
   	   	<div class="swiper-container testi-3" data-autoplay="0" data-loop="1" data-speed="1000" data-center="0" data-slides-per-view="1" id="testi-slider-3">
		   <div class="swiper-wrapper">

	 	   @foreach($testimonials as $testimonial)
			  <div class="swiper-slide">
			         <div class="testi-wrap">
			           <div class="qq">
			           	 <img src="{{ URL::asset('asettemplate1/img/quote.png') }}" alt="">
			           </div>
			        	<p>{{$testimonial->isitesti}}</p>
			        	  <h4><b>{{$testimonial->sumbertesti}}</b></h4>
			         </div>
			  </div>
				@endforeach

		   	</div>
		   	<div class="pagination poin-style-1 pagination-hidden"></div>
		    <div class="arrow-wrapp arr-s-7">
				<div class="cont-1170">
					<div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
					<div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
				</div>
			</div>
	   	</div>
	</div>
</div>

<div class="main-wraper padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">Tim Kami</h4>
					<h2>Anggota Tim</h2>
				</div>
			</div>
		</div>
		<div class="row">

				 	   @foreach($team as $anggota)
			<div class="col-xs-12 col-sm-4">
				<div class="team-entry style-2">
					<h3 class="team-name color-dark-2">{{$anggota->namaanggota}}</h3>
					<h5 class="team-position color-dark-2-light">{{$anggota->jabatananggota}}</h5>
					<div class="image">
					   <img class="team-img img-responsive" width="370" height="334" src="{{url('/')}}/gambarlokal/{{$anggota->gambar}}/w/370/h/334" alt="">
					</div>
					<p class="color-dark-2-light">{{$anggota->emailanggota}}</p>
					<div class="team-social">
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
						<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div>
<!-- TEAM
<div class="main-wraper bg-dr-blue-2 padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-white underline">our team</h4>
					<h2 class="color-white">MEET OUR TEAM</h2>
				</div>
			</div>
		</div>
        <div class="circle-wrapper">
            <div class="row">
                <div class="col-mob-12 col-xs-12 col-sm-4 cust-md-5">
                    <div class="circle-entry clearfix">
                        <div class="circle" data-startdegree="0" data-dimension="170" data-text="100%" data-info="First Class" data-width="5" data-fontsize="32" data-percent="100" data-fgcolor="#fff" data-bgcolor="#022c54"></div>
                    </div>
                </div>
                <div class="col-mob-12 col-xs-12 col-sm-4 cust-md-5">
                    <div class="circle-entry clearfix">
                        <div class="circle" data-startdegree="0" data-dimension="170" data-text="88%" data-info="Packages" data-width="5" data-fontsize="32" data-percent="88" data-fgcolor="#fff" data-bgcolor="#022c54"></div>
                    </div>
                </div>
                <div class="col-mob-12 col-xs-12 col-sm-4 cust-md-5">
                    <div class="circle-entry clearfix">
                        <div class="circle" data-startdegree="0" data-dimension="170" data-text="80%" data-info="Model Vehicles" data-width="5" data-fontsize="32" data-percent="80" data-fgcolor="#fff" data-bgcolor="#022c54"></div>
                    </div>
                </div>
                <div class="col-mob-12 col-xs-12 col-sm-4 cust-md-5">
                    <div class="circle-entry clearfix">
                        <div class="circle" data-startdegree="0" data-dimension="170" data-text="75%" data-info="Price Guarantee" data-width="5" data-fontsize="32" data-percent="75" data-fgcolor="#fff" data-bgcolor="#022c54"></div>
                    </div>
                </div>
                 <div class="col-mob-12 col-xs-12 col-sm-4 cust-md-5">
                    <div class="circle-entry clearfix">
                        <div class="circle" data-startdegree="0" data-dimension="170" data-text="50%" data-info="Accommodations" data-width="5" data-fontsize="32" data-percent="50" data-fgcolor="#fff" data-bgcolor="#022c54"></div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
 -->
<!-- CONTACT-FORM -->
<div class="main-wraper bg-grey-2 padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">contact</h4>
					<h2>kirimkan saran, testimonial atau pertanyaan anda</h2>


				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<form class="contact-form" action="simpanbukutamu" method="post">
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="asal" value="about">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="input-style-1 type-2 color-2">
							  	<input type="text" name="nama" required="" placeholder="Enter your name">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="input-style-1 type-2 color-2">
							  	<input type="text" name="email" required="" placeholder="Enter your email">
							</div>
						</div>

						<div class="col-xs-12">
							<textarea class="area-style-1 color-1" name="isi" required="" placeholder="Enter your comment"></textarea>
							<button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>submit comment</span></button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="contact-about">
					<h4 class="color-dark-2"><strong>about us</strong></h4>
					           									 	   @foreach($datakontak as $dakon)
					           											 <?php
					           											 if($dakon->label=="handphone"){
					           												 $handphone=$dakon->isi;
					           											 }else if($dakon->label=="alamat"){
					           												 $alamat=$dakon->isi;
					           											 }if($dakon->label=="email"){
					           												 $email=$dakon->isi;
					           											 }if($dakon->label=="googlemap"){
					           												 $googlemap=$dakon->isi;
					           											 }if($dakon->label=="deskripsi"){
					           												 $deskripsi=$dakon->isi;
					           											 }
					           											  ?>
					           								 			@endforeach
				</div>
				<div class="contact-info">
					<h4 class="color-dark-2"><strong>contact info</strong></h4>
					<div class="contact-line color-grey-3"><img src="img/phone_icon_2_dark.png" alt="">Phone: <a class="color-dark-2" href="tel:{{$handphone}}">{{$handphone}}</a></div>
					<div class="contact-line color-grey-3"><img src="img/mail_icon_b_dark.png" alt="">Email us: <a class="color-dark-2 tt" href="#">{{$email}}</a></div>
					<div class="contact-line color-grey-3"><img src="img/loc_icon_dark.png" alt="">Address: <span class="color-dark-2 tt">{{$alamat}}</span></div>
				</div>
				<div class="contact-socail">
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-pinterest-p"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-instagram"></i></a>
					<a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-behance"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
