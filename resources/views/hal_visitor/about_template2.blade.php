@extends('layouts.'.$namatemplate)
@section('kontenweb')

<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Tentang Kami</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Tentang Kami</li>
				</ul>
		</div>
</div>

<section id="content">
		<div class="container">
				<div id="main">

					<div class="image-style style1 large-block">

							<p><?php print($infowebsite['about']); ?></p>

							<div class="clearfix"></div>
					</div>

					<div class="large-block">
							<h2>Layanan Kami</h2>
							<div class="row image-box style1 team">
										@foreach($services as $servis)
									<div class="col-sm-6 col-md-3">
											<article class="box">
													<figure>
															<a href="#"><img src="{{url('/')}}/gambarlokal/{{$servis->gambar}}/w/270/h/270" /></a>
													</figure>
													<div class="details">
															<h4 class="box-title"><a href="#">{{$servis->namaservis}}</a></h4>
															<p class="description">{{$servis->keteranganservis}}</p>

													</div>
											</article>
									</div>
									@endforeach


							</div>
					</div>



						<div class="large-block">
								<h2>Anggota Tim</h2>
								<div class="row image-box style1 team">
									   @foreach($team as $anggota)
										<div class="col-sm-6 col-md-3">
												<article class="box">
														<figure>
																<a href="#"><img src="{{url('/')}}/gambarlokal/{{$anggota->gambar}}/w/370/h/334" alt="" width="270" height="263" /></a>
														</figure>
														<div class="details">
																<h4 class="box-title"><a href="#">{{$anggota->namaanggota}}</a></h4>
																<p class="description">{{$anggota->jabatananggota}}</p>
																<p class="description">{{$anggota->emailanggota}}</p>

														</div>
												</article>
										</div>
										@endforeach


								</div>
						</div>


				</div>
		</div>
</section>
@endsection
