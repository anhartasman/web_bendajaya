@extends('layouts.'.$namatemplate)
@section('kontenweb')


        <div class="container">
            <h1 class="page-title">Tentang Kami</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="text-bigger"><?php print($infowebsite['about']); ?></p>
                   </div>
            </div>
            <div class="gap"></div>
        </div>
        <div class="bg-holder">
            <div class="bg-parallax" style="background-image:url(img/1280x852.png);"></div>
            <div class="bg-mask"></div>
            <div class="bg-holder-content">
                <div class="container">
                    <div class="gap gap-big text-white">
                        <div class="row">
                            <div class="col-md-10">
                                <h2>Layanan kami</h2>
                            </div>
														<div class="row row-wrap" data-gutter="60">
															@foreach($services as $servis)
																<div class="col-md-3">
								                    <div class="thumb text-center">
								                        <header class="thumb-header">
								                            <img class="round" src="{{url('/')}}/gambarlokal/{{$servis->gambar}}/w/300/h/300" title="{{$servis->namaservis}}"  />
								                        </header>
								                        <div class="thumb-caption">
								                            <h5 class="thumb-title">{{$servis->namaservis}}</h5>
								                            <p class="thumb-meta text-small">{{$servis->keteranganservis}}</p>
								                        </div>
								                    </div>
								                </div>
																@endforeach


								            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

            <div class="gap"></div>
            <h2>Anggota Tim</h2>
						<div class="row row-wrap" data-gutter="60">
							@foreach($team as $anggota)
								<div class="col-md-3">
										<div class="thumb text-center">
												<header class="thumb-header">
														<img class="round" src="{{url('/')}}/gambarlokal/{{$anggota->gambar}}/w/300/h/300" title="{{$servis->namaservis}}"  />
												</header>
												<div class="thumb-caption">
														<h5 class="thumb-title">{{$anggota->namaanggota}}</h5>
														<p class="thumb-meta text-small">{{$anggota->jabatananggota}}</p>
														<p class="thumb-meta text-small">{{$anggota->emailanggota}}</p>
												</div>
										</div>
								</div>
								@endforeach


						</div>

        </div>

        <div class="container">
        </div>



@endsection
