@extends('layouts.'.$namatemplate)

@section('kontenweb')
<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
		  				<li><span>blog</span></li>
		  			</ul>
		  			<h2 class="color-white">blog</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- BLOG -->

<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
       			<div class="blog-list">
					<div class="blog-list-entry">

							@foreach($articles as $artikel)
						<div class="blog-list-top">
		       				<div class="slider-wth-thumbs style-1 arrows">

							</div>
						</div>
						<?php
						$linkartikel=url('/')."/blog/".date('Y',strtotime($artikel->tanggal))."/".date('m',strtotime($artikel->tanggal))."/".str_replace(" ","-",$artikel->judul).".html";
						 ?>
						<h4 class="blog-list-title"><a class="color-dark-2 link-dr-blue-2" href="{{$linkartikel}}">{{ $artikel->judul }}</a></h4>
						<div class="tour-info-line clearfix">
							<div class="tour-info fl">
					  	 		<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png') }}" alt="">
					  	 		<span class="font-style-2 color-dark-2">{{ $artikel->tanggal }}</span>
					  	 	</div>
								<!--
							<div class="tour-info fl">
					  	 		<img src="img/people_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">By Emma Stonea</span>
					  	 	</div>
							<div class="tour-info fl">
					  	 		<img src="img/comment_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">10 commentsa</span>
					  	 	</div>
							-->
							<?php $content = html_cut($artikel->isi, 303); ?>
						</div>
						<div class="blog-list-text color-black-3"><?php echo $content;?></div>
						<a href="{{$linkartikel}}" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>read more</span></a>

						@endforeach

					</div>
					@include('hal_visitor.inc_gayapages_template1', ['paginator' => $articles])
				</div>
       		</div>
       		<div class="col-xs-12 col-md-4">
       			<div class="right-sidebar">

       			@include('hal_visitor.inc_blogsidebar_template1')
       			</div>
       		</div>
       	</div>
	</div>
</div>
@endsection
