@extends('layouts.'.$namatemplate)

@section('kontenweb')
<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Blog</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Blog</li>
				</ul>
		</div>
</div>

        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                        <div class="page">
                            <span style="display: none;" class="entry-title page-title">Blog</span>
                            <span style="display: none;" class="vcard"><span class="fn"><a rel="author" title="Posts by admin" href="#">admin</a></span></span>
                            <span style="display:none;" class="updated">2014-06-20T13:35:34+00:00</span>
															@foreach($articles as $artikel)
															<?php
															$linkartikel=url('/')."/blog/".date('Y',strtotime($artikel->tanggal))."/".date('m',strtotime($artikel->tanggal))."/".str_replace(" ","-",$artikel->judul).".html";
															 ?>
														<div class="post-content">
                                <div class="blog-infinite">
                                    <div class="post without-featured-item">
                                        <div class="post-content-wrapper">
                                            <div class="details">
                                                <h2 class="entry-title"><a href="{{$linkartikel}}">{{ $artikel->judul }}</a></h2>
                                                <div class="excerpt-container">
																									<?php $content = html_cut($artikel->isi, 303); ?>
                                                    <p><?php echo $content;?></p>
                                                </div>
                                                <div class="post-meta">
                                                    <div class="entry-date">
                                                        <label class="date">{{date('d',strtotime($artikel->tanggal))}}</label>
                                                        <label class="month">{{date('M',strtotime($artikel->tanggal))}}</label>
                                                    </div>
                                                    <div class="entry-author fn">
                                                        <i class="icon soap-icon-user"></i> Posted By: {{$artikel->namapengarang}}
                                                        <a href="#" class="author"> </a>
                                                    </div>
                                                    <div class="entry-action">
																											<?php $daftag=helpblog_tag($artikel->id);?>



                                                        <a href="#" class="button entry-comment btn-small"><i class="soap-icon-comment"></i><span>{{helpblog_jumlahkomen($artikel->id)}} Comments </span></a>
                                                         <span class="entry-tags"><i class="soap-icon-features"></i><span>@foreach ($daftag as $daf)<a href="{{ url('/') }}/blog/kategori/{{$daf->category}}">{{$daf->category}}</a>,@endforeach</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
																</div>
																@endforeach
																@include('hal_visitor.inc_gayapages_template2', ['paginator' => $articles])

                        </div>
                    </div>
										       			@include('hal_visitor.inc_blogsidebar_template2')
                </div>
            </div>
        </section>
@endsection

@section('akhirbody')
<script type="text/javascript">

</script>
@endsection
