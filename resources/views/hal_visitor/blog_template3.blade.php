@extends('layouts.'.$namatemplate)

@section('kontenweb')


				<div class="container">
						<div class="row">
								<div class="col-md-9">
										<!-- START BLOG POST -->
										@foreach($articles as $artikel)
										<?php
										$linkartikel=url('/')."/blog/".date('Y',strtotime($artikel->tanggal))."/".date('m',strtotime($artikel->tanggal))."/".str_replace(" ","-",$artikel->judul).".html";
										 ?>
										<div class="article post">
												<div class="post-inner">
														<h4 class="post-title"><a class="text-darken" href="{{$linkartikel}}">{{ $artikel->judul }}</a></h4>
														<ul class="post-meta">
																<li><i class="fa fa-calendar"></i><a href="#">{{date('d M, Y',strtotime($artikel->tanggal))}}</a>
																</li>
																<li><i class="fa fa-user"></i><a href="#">{{$artikel->namapengarang}}</a>
																</li>
																	<?php $daftag=helpblog_tag($artikel->id);?>
																<li><i class="fa fa-tags"></i>@foreach ($daftag as $daf)<a href="{{ url('/') }}/blog/kategori/{{$daf->category}}">{{$daf->category}}</a>,@endforeach
																</li>
																<li><i class="fa fa-comments"></i><a href="{{$linkartikel}}">{{helpblog_jumlahkomen($artikel->id)}} Comments</a>
																</li>
														</ul>
														<p class="post-desciption">
															<?php $content = html_cut($artikel->isi, 303); ?>
																<p><?php echo $content;?></p>
														</p><a class="btn btn-small btn-primary" href="{{$linkartikel}}">Read More</a>
												</div>
										</div>
											@endforeach
										<!-- END BLOG POST -->

										@include('hal_visitor.inc_gayapages_template3', ['paginator' => $articles])

								</div>
								@include('hal_visitor.inc_blogsidebar_template3')
						</div>
				</div>



				<div class="gap"></div>
@endsection

@section('akhirbody')
<script type="text/javascript">

</script>
@endsection
