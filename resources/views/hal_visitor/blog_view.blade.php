@extends('layouts.master')

@section('kontenweb')
<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><a class="link-dr-blue-2" href="{{ url('/') }}/about/blog">Blog</a> /</li>
		  				<li><span>view</span></li>
		  			</ul>
		  			<h2 class="color-white">{{ $judul }}</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- BLOG -->

<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
       			<div class="blog-list">
					<div class="blog-list-entry">

						<div class="blog-list-top">
		       				<div class="slider-wth-thumbs style-1 arrows">

							</div>
						</div>
						<h4 class="blog-list-title"><a class="color-dark-2 link-dr-blue-2" href="{{ url('/') }}/about/blog/view/{{$idartikel}}">{{ $judul }}</a></h4>
						<div class="tour-info-line clearfix">
							<div class="tour-info fl">
					  	 		<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png') }}" alt="">
					  	 		<span class="font-style-2 color-dark-2">{{ $tanggal}}</span>
					  	 	</div>
								<!--
							<div class="tour-info fl">
					  	 		<img src="img/people_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">By Emma Stonea</span>
					  	 	</div>
							<div class="tour-info fl">
					  	 		<img src="img/comment_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">10 commentsa</span>
					  	 	</div>
							-->
						</div>
						<div class="blog-list-text color-grey-3"><?php echo $isi;?></div>


					</div>

				</div>
       		</div>
       		<div class="col-xs-12 col-md-4">
       			<div class="right-sidebar">
       			@include('hal_visitor.inc_blogsidebar')
       			</div>
       		</div>
       	</div>
	</div>
</div>
@endsection
