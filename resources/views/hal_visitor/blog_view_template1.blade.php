@extends('layouts.'.$namatemplate)

@section('kontenweb')
<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><a class="link-dr-blue-2" href="{{ url('/') }}/about/blog">Blog</a> /</li>
		  				<li><span>view</span></li>
		  			</ul>
		  			<h2 class="color-white">{{ $judul }}</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- BLOG -->

<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
       			<div class="blog-list">
					<div class="blog-list-entry">

						<div class="blog-list-top">
		       				<div class="slider-wth-thumbs style-1 arrows">

							</div>
						</div>
						<h4 class="blog-list-title"><a class="color-dark-2 link-dr-blue-2" href="{{ url('/') }}/about/blog/{{$link}}">{{ $judul }}</a></h4>
						<div class="tour-info-line clearfix">
							<div class="tour-info fl">
					  	 		<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png') }}" alt="">
					  	 		<span class="font-style-2 color-dark-2">{{ $tanggal}}</span>
					  	 	</div>

							<div class="tour-info fl">
					  	 		<img src="img/people_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">By Emma Stonea</span>
					  	 	</div>
							<div class="tour-info fl">
					  	 		<img src="img/comment_icon_grey.png" alt="">
					  	 		<span class="font-style-2 color-dark-2">{{count($dafcom)}} comment(s)</span>
					  	 	</div>

						</div>
						<div class="blog-list-text color-black-3"><?php echo $isi;?></div>


					</div>

				</div>

				<div class="additional-block" id="kotakkomen">
					<h4 class="additional-title">comments <span class="color-dr-blue-2">({{count($dafcom)}})</span></h4>
					<ul class="comments-block">
<?php
$paramgrav=array(
		'size'   => 80,
		'fallback' => 'identicon',
		'secure' => false,
		'maximumRating' => 'g',
		'forceDefault' => false,
		'forceExtension' => 'jpg',
);
$argam = array("mm", "identicon", "monsterid", "wavatar", "retro");

 ?>
					 @foreach($dafcom as $com)
					 <li class="comment-entry clearfix">
						 <?php

							$rangam = array_rand($argam, 2);
						 $almgam=Gravatar::get($com->email,$paramgrav);
						 $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);  ?>
							<img class="commnent-img" src="{{$almgam}}" alt="">
							<div class="comment-content clearfix">
								<div class="tour-info-line">
									<div class="tour-info">
							  	 		<img src="img/calendar_icon_grey.png" alt="">
							  	 		<span class="font-style-2 color-dark-2" id="tanggalkomen{{$com->id}}">{{$com->tanggal}}</span>
							  	 	</div>
									<div class="tour-info">
							  	 		<img src="img/people_icon_grey.png" alt="">
							  	 		<span class="font-style-2 color-dark-2" id="namakomen{{$com->id}}">{{$com->nama}}</span>
							  	 	</div>
								</div>
								<div class="comment-text color-black" id="isikomen{{$com->id}}">{{$com->isi}}</div>
								<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#kotakreplyquote" onclick="replyquote({{$com->id}})">Reply</a>
							</div>
							<ul class="comments-block" id="dafreply{{$com->id}}">
							@if(count($com->balasan)>0)
								@foreach($com->balasan as $rep)
								<?php

	 							$rangam = array_rand($argam, 2);
	 						 $almgam=Gravatar::get($rep->email,$paramgrav);
	 						 $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);  ?>
								<li class="comment-entry clearfix">
									<img class="commnent-img" src="{{$almgam}}" alt="">
									<div class="comment-content clearfix">
										<div class="tour-info-line">
											<div class="tour-info">
									  	 		<img src="img/calendar_icon_grey.png" alt="">
									  	 		<span class="font-style-2 color-dark-2"id="tanggalkomen{{$rep->id}}">{{$rep->tanggal}}</span>
									  	 	</div>
											<div class="tour-info">
									  	 		<img src="img/people_icon_grey.png" alt="">
									  	 		<span class="font-style-2 color-dark-2" id="namakomen{{$rep->id}}">{{$rep->nama}}</span>
									  	 	</div>
										</div>
										<div class="comment-text color-black" id="isikomen{{$rep->id}}">{{$rep->isi}}</div>
										<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#kotakreplyquote" onclick="replyquote({{$rep->id}})">Reply</a>
									</div>
								</li>
								@endforeach

							@endif
						 </ul>
						</li>
						@endforeach

					</ul>

						<ul class="comments-block" id="kotakreplyquote">
							<li class="comment-entry clearfix">
								Reply to
								<div class="comment-content clearfix">
									<div class="tour-info-line">
										<div class="tour-info">
								  	 		<img src="img/calendar_icon_grey.png" alt="">
								  	 		<span class="font-style-2 color-dark-2" id="tanggalreply">03/07/2015</span>
								  	 	</div>
										<div class="tour-info">
								  	 		<img src="img/people_icon_grey.png" alt="">
								  	 		<span class="font-style-2 color-dark-2" id="namareply">By Emma Stone</span>
								  	 	</div>
									</div>
									<div class="comment-text color-black" id="isireply">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
									<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#kotakreplyquote" onclick="tutupreply()">Close</a>
								</div>
							</li>
						</ul>
						<form id="formreply" method="POST" action="{{url('/')."/submitblogcomment"}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" id="reply" name="reply" value="0">
							<input type="hidden" id="com_id" name="com_id" value="0">
							<input type="hidden" id="article_id" name="article_id" value="{{$idartikel}}">
							<input type="hidden" id="linkartikel" name="linkartikel" value="{{$link}}">

						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="form-block type-2 clearfix">
									<div class="input-style-1 b-50 brd-0 type-2 color-3">
										<input type="text" required="" name="nama" placeholder="Enter your name">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-block type-2 clearfix">
									<div class="input-style-1 b-50 brd-0 type-2 color-3">
										<input type="email" required="" name="email" placeholder="Enter your email">
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-block type-2 clearfix">
									<textarea class="area-style-1 type-2 color-3" name="isi" required="" placeholder="Write a comment..."></textarea>
								</div>
								<input type="submit" class="c-button b-40 fr bg-dr-blue-2 hv-dr-blue-2-o" value="post comment">
							</div>

						</div>
					</form>
				</div>


       		</div>
       		<div class="col-xs-12 col-md-4">
       			<div class="right-sidebar">
       			@include('hal_visitor.inc_blogsidebar_template1')
       			</div>
       		</div>
       	</div>
	</div>
</div>


				@section('akhirbody')
	<script type="text/javascript">
	var reply=0;
	var idreply=1;
	var namakomentator="";
	var emailkomentator="";
	var pendapatkomentator="";
	    $("#kotakreplyquote").hide();

			function replyquote(idcom){
			  $("#kotakreplyquote").show();
				$('#reply').val(1);
				$('#com_id').val(idcom);
				$('#tanggalreply').html($('#tanggalkomen'+idcom).html());
				$('#namareply').html($('#namakomen'+idcom).html());
				$('#isireply').html($('#isikomen'+idcom).html());
				reply=1;
			}

			function tutupreply(){
			$('#reply').val(0);
			    $("#kotakreplyquote").hide();
					reply=0;
			}
	</script>
	@endsection
@endsection
