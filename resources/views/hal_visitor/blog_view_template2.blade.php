@extends('layouts.'.$namatemplate)

@section('kontenweb')
<!-- INNER-BANNER -->
@section('bagiangaya')
#main {
  margin-bottom: 340px; }
	@endsection
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{ $judul }}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{url('/')}}">HOME</a></li>
                    <li><a href="{{url('/')}}/blog">BLOG</a></li>
                    <li class="active">{{ $judul }}</li>
                </ul>
            </div>
        </div>

        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                        <div class="post-content">

													<div class="blog-infinite">
															<div class="post without-featured-item">
																	<div class="post-content-wrapper">
																			<div class="details">
																				<?php
																				$linkartikel=url('/')."/blog/".date('Y',strtotime($tanggal))."/".date('m',strtotime($tanggal))."/".str_replace(" ","-",$judul).".html";
																				 ?>
																					<h2 class="entry-title"><a href="{{$linkartikel}}">{{ $judul }}</a></h2>
																					<div class="excerpt-container">
																						 <p><?php echo $isi;?></p>
																					</div>
																					<div class="post-meta">
																							<div class="entry-date">
																									<label class="date">{{date('d',strtotime($tanggal))}}</label>
																									<label class="month">{{date('M',strtotime($tanggal))}}</label>
																							</div>
																							<div class="entry-author fn">
																									<i class="icon soap-icon-user"></i> Posted By: {{$namapengarang}}
																									<a href="#" class="author"> </a>
																							</div>
																							<div class="entry-action">
																								<?php $daftag=helpblog_tag($idartikel);?>



																									<a href="#" class="button entry-comment btn-small"><i class="soap-icon-comment"></i><span>{{helpblog_jumlahkomen($idartikel)}} Comments </span></a>
																									 <span class="entry-tags"><i class="soap-icon-features"></i><span>@foreach ($daftag as $daf)<a href="{{ url('/') }}/blog/kategori/{{$daf->category}}">{{$daf->category}}</a>,@endforeach</span></span>
																							</div>
																					</div>
																			</div>
																	</div>
															</div>

													</div>
                            <div class="comments-container block">
                                <h2>{{count($dafcom)}} Comments</h2>
																<ul class="comments-block">
											<?php
											$paramgrav=array(
													'size'   => 80,
													'fallback' => 'identicon',
													'secure' => false,
													'maximumRating' => 'g',
													'forceDefault' => false,
													'forceExtension' => 'jpg',
											);
											$argam = array("mm", "identicon", "monsterid", "wavatar", "retro");

											 ?>
                                <ul class="comment-list travelo-box">
																	@foreach($dafcom as $com)
																	<?php
										 							$rangam = array_rand($argam, 2);
										 						 $almgam=Gravatar::get($com->email,$paramgrav);
										 						 $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);
																 ?>
                                    <li class="comment depth-1">
                                        <div class="the-comment">
                                            <div class="avatar">
                                                <img src="{{$almgam}}" width="72" height="72" alt="">
                                            </div>
                                            <div class="comment-box">
                                                <div class="comment-author">
                                                    <a href="#kotakreplyquote" onclick="replyquote({{$com->id}})" class="button btn-mini pull-right">REPLY</a>
                                                    <h4 class="box-title"><a href="#"  id="namakomen{{$com->id}}">{{$com->nama}}</a><small id="tanggalkomen{{$com->id}}">{{$com->tanggal}}</small></h4>
                                                </div>
                                                <div class="comment-text">
                                                    <p  id="isikomen{{$com->id}}">{{$com->isi}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="children">
																					@if(count($com->balasan)>0)
																						@foreach($com->balasan as $rep)
																						<?php

																						$rangam = array_rand($argam, 2);
																					 $almgam=Gravatar::get($rep->email,$paramgrav);
																					 $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);  ?>
                                            <li class="comment depth-2">
                                                <div class="the-comment">
                                                    <div class="avatar">
                                                        <img src="{{$almgam}}" width="72" height="72" alt="">
                                                    </div>
                                                    <div class="comment-box">
                                                        <div class="comment-author">
                                                            <a href="#kotakreplyquote" onclick="replyquote({{$rep->id}})" class="button btn-mini pull-right">REPLY</a>
                                                            <h4 class="box-title"><a href="#"  id="namakomen{{$rep->id}}">{{$rep->nama}}</a><small id="tanggalkomen{{$rep->id}}">{{$rep->tanggal}}</small></h4>
                                                        </div>
                                                        <div class="comment-text">
                                                            <p id="isikomen{{$rep->id}}">{{$rep->isi}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>

																						@endforeach

																					@endif
                                        </ul>
                                    </li>
                                     @endforeach
                                </ul>
                            </div>


                            <div class="post-comment block">
                                <h2 class="reply-title">Post a Comment</h2>
                                <div class="travelo-box">
																	<ul class="comment-list travelo-box" id="kotakreplyquote">

																	<li class="comment depth-1" >
																			<div class="the-comment">
																					<div class="comment-box">
																							<div class="comment-author">
																									<a href="#kotakreplyquote" onclick="tutupreply()" class="button btn-mini pull-right">CLOSE</a>
																									<h4 class="box-title"><a href="#"  id="namareply"></a><small  id="tanggalreply"></small></h4>
																							</div>
																							<div class="comment-text">
																									<p id="isireply"></p>
																							</div>
																					</div>
																			</div>
																	</li>
														 </ul>
																	<form class="comment-form" id="formreply" method="POST" action="{{url('/')."/submitblogcomment"}}">
																		<input type="hidden" name="_token" value="{{ csrf_token() }}">
																		<input type="hidden" id="reply" name="reply" value="0">
																		<input type="hidden" id="com_id" name="com_id" value="0">
																		<input type="hidden" id="article_id" name="article_id" value="{{$idartikel}}">
																		<input type="hidden" id="linkartikel" name="linkartikel" value="{{$link}}">
    															<div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Your Name</label>
                                                <input type="text"name="nama"  required="" class="input-text full-width">
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Your Email</label>
                                                <input type="email" name="email" required="" class="input-text full-width">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Your Message</label>
                                            <textarea rows="6" name="isi" required="" class="input-text full-width" placeholder="write message here"></textarea>
                                        </div>

                                        <button type="submit" class="btn-large full-width">SEND COMMENT</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                  	@include('hal_visitor.inc_blogsidebar_template2')
                </div>
            </div>
        </section>
				@section('akhirbody')
				<script type="text/javascript">
				var reply=0;
				var idreply=1;
				var namakomentator="";
				var emailkomentator="";
				var pendapatkomentator="";
				$("#kotakreplyquote").hide();

				function replyquote(idcom){
				$("#kotakreplyquote").show();
				$('#reply').val(1);
				$('#com_id').val(idcom);
				$('#tanggalreply').html($('#tanggalkomen'+idcom).html());
				$('#namareply').html($('#namakomen'+idcom).html());
				$('#isireply').html($('#isikomen'+idcom).html());
				reply=1;
				}

				function tutupreply(){
				$('#reply').val(0);
					$("#kotakreplyquote").hide();
					reply=0;
				}
				</script>
				@endsection

@endsection
