@extends('layouts.'.$namatemplate)

@section('kontenweb')
<!-- INNER-BANNER -->
@section('bagiangaya')
#main {
  margin-bottom: 340px; }
	@endsection
  <div class="container">
      <div class="row">
          <div class="col-md-9">
              <article class="post">
                  <div class="post-inner">
                      <h4 class="post-title text-darken">{{ $judul }}</h4>
                      <ul class="post-meta">
                          <li><i class="fa fa-calendar"></i><a href="#">{{date('d M, Y',strtotime($tanggal))}}</a>
                          </li>
                          <li><i class="fa fa-user"></i><a href="#">{{$namapengarang}}</a>
                          </li>
                          <?php $daftag=helpblog_tag($idartikel);?>

                          <li><i class="fa fa-tags"></i>@foreach ($daftag as $daf)<a href="{{ url('/') }}/blog/kategori/{{$daf->category}}">{{$daf->category}}</a>,@endforeach
                          </li>
                          <li><i class="fa fa-comments"></i><a href="#">{{count($dafcom)}} Comments</a>
                          </li>
                      </ul>
                      <p><?php print($isi);?></p>
                  </div>
              </article>
              <h2>Komentar</h2>
              <!-- START COMMENTS -->
              <?php
              $paramgrav=array(
                  'size'   => 80,
                  'fallback' => 'identicon',
                  'secure' => false,
                  'maximumRating' => 'g',
                  'forceDefault' => false,
                  'forceExtension' => 'jpg',
              );
              $argam = array("mm", "identicon", "monsterid", "wavatar", "retro");

               ?>
              <ul class="comments-list">
                @foreach($dafcom as $com)
                <?php
                $rangam = array_rand($argam, 2);
               $almgam=Gravatar::get($com->email,$paramgrav);
               $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);
               ?>
                  <li>
                      <div class="article comment" inline_comment="comment">
                          <div class="comment-author">
                              <img src="{{$almgam}}" width="50" height="50"  alt="Image Alternative text" title="Spidy" />
                          </div>
                          <div class="comment-inner"><span class="comment-author-name" id="namakomen{{$com->id}}">{{$com->nama}}</span>
                              <p class="comment-content" id="isikomen{{$com->id}}">{{$com->isi}}</p><span class="comment-time"id="tanggalkomen{{$com->id}}">{{$com->tanggal}}</span><a class="comment-reply" href="#kotakreplyquote"  onclick="replyquote({{$com->id}})" ><i class="fa fa-reply"></i> Reply</a>
                          </div>
                      </div>
                      <ul>
                        @if(count($com->balasan)>0)
                          @foreach($com->balasan as $rep)
                          <?php

                          $rangam = array_rand($argam, 2);
                         $almgam=Gravatar::get($rep->email,$paramgrav);
                         $almgam=str_replace("identicon",$argam[$rangam[0]],$almgam);  ?>

                          <li>
                              <div class="article comment" inline_comment="comment">
                                  <div class="comment-author">
                                      <img src="{{$almgam}}" width="50" height="50" alt="Image Alternative text" />
                                  </div>
                                  <div class="comment-inner"><span class="comment-author-name" id="namakomen{{$rep->id}}">{{$rep->nama}}</span>
                                      <p class="comment-content" id="isikomen{{$rep->id}}">{{$rep->isi}}</p><span class="comment-time"id="tanggalkomen{{$rep->id}}">{{$rep->tanggal}}</span><a class="comment-reply" href="#kotakreplyquote"onclick="replyquote({{$rep->id}})"><i class="fa fa-reply"></i> Reply</a>
                                  </div>
                              </div>
                            </li>

                            @endforeach

                          @endif
                      </ul>
                    </li>
                     @endforeach

              </ul>
              <!-- END COMMENTS -->
              <h3>Tinggalkan komentar</h3>
              <ul class="comment-list travelo-box" id="kotakreplyquote">
                <li>
                    <div class="article comment" inline_comment="comment">

                        <div class="comment-inner"><span class="comment-author-name" id="namareply"> </span>
                            <p class="comment-content" id="isireply"> </p><span class="comment-time"id="tanggalreply"> </span><a class="comment-reply" href="#kotakreplyquote"onclick="tutupreply()"><i class="fa fa-reply"></i> Close</a>
                        </div>
                    </div>
                  </li>
         </ul>
              <form class="comment-form" id="formreply" method="POST" action="{{url('/')."/submitblogcomment"}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="reply" name="reply" value="0">
                <input type="hidden" id="com_id" name="com_id" value="0">
                <input type="hidden" id="article_id" name="article_id" value="{{$idartikel}}">
                <input type="hidden" id="linkartikel" name="linkartikel" value="{{$link}}">
                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label>Nama</label>
                              <input name="nama"  required=""  class="form-control" type="text" />
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label>E-mail</label>
                              <input  type="email" name="email" required="" class="form-control"  />
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label>Komentar</label>
                      <textarea  name="isi" required="" class="form-control"></textarea>
                  </div>
                  <input class="btn btn-primary" type="submit" value="Leave a Comment" />
              </form>
          </div>
          <div class="col-md-3">
              <aside class="sidebar-right">
                  <div class="sidebar-widget">
                      <div class="Form">
                          <input class="form-control" placeholder="Search..." type="text" />
                      </div>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Categories</h4>
                      <ul class="icon-list list-category">
                          <li><a href="#"><i class="fa fa-angle-right"></i>Photos <small >(94)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Vacation <small >(61)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Flights <small >(92)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Travel Advices <small >(100)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Trending Now <small >(64)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Hotels <small >(90)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Places to Go <small >(74)</small></a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>Travel Stories <small >(55)</small></a>
                          </li>
                      </ul>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Popular Posts</h4>
                      <ul class="thumb-list">
                          <li>
                              <a href="#">
                                  <img src="img/70x70.png" alt="Image Alternative text" title="Viva Las Vegas" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                  <h5 class="thumb-list-item-title"><a href="#">Nulla faucibus</a></h5>
                                  <p class="thumb-list-item-desciption">Porttitor fermentum vel sociosqu mollis</p>
                              </div>
                          </li>
                          <li>
                              <a href="#">
                                  <img src="img/70x70.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                  <h5 class="thumb-list-item-title"><a href="#">Dapibus class</a></h5>
                                  <p class="thumb-list-item-desciption">Potenti luctus pretium mattis aliquam</p>
                              </div>
                          </li>
                          <li>
                              <a href="#">
                                  <img src="img/70x70.png" alt="Image Alternative text" title="Cup on red" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                  <h5 class="thumb-list-item-title"><a href="#">Purus sociis</a></h5>
                                  <p class="thumb-list-item-desciption">Dictum in aptent sociosqu nascetur</p>
                              </div>
                          </li>
                      </ul>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Twitter Feed</h4>
                      <div class="twitter" id="twitter"></div>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Recent Comments</h4>
                      <ul class="thumb-list thumb-list-right">
                          <li>
                              <a href="#">
                                  <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">5 minutes ago</p>
                                  <h4 class="thumb-list-item-title"><a href="#">Sarah Slater</a></h4>
                                  <p class="thumb-list-item-desciption">Porttitor sodales ipsum integer porta quam vitae...</p>
                              </div>
                          </li>
                          <li>
                              <a href="#">
                                  <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">7 minutes ago</p>
                                  <h4 class="thumb-list-item-title"><a href="#">Neil Davidson</a></h4>
                                  <p class="thumb-list-item-desciption">Commodo penatibus ornare gravida porttitor vulputate dignissim...</p>
                              </div>
                          </li>
                          <li>
                              <a href="#">
                                  <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                              </a>
                              <div class="thumb-list-item-caption">
                                  <p class="thumb-list-item-meta">9 minutes ago</p>
                                  <h4 class="thumb-list-item-title"><a href="#">Blake Hardacre</a></h4>
                                  <p class="thumb-list-item-desciption">Iaculis taciti quis pellentesque netus nostra eu...</p>
                              </div>
                          </li>
                      </ul>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Archive</h4>
                      <ul class="icon-list list-category">
                          <li><a href="#"><i class="fa fa-angle-right"></i>July 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>June 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>May 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>April 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>March 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>February 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>January 2014</a>
                          </li>
                          <li><a href="#"><i class="fa fa-angle-right"></i>December 2014</a>
                          </li>
                      </ul>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Gallery</h4>
                      <div class="row row-no-gutter">
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="Spidy" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="a dreamy jump" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="Me with the Uke" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="b and w camera" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="Happy Bokeh Day" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="Good job" />
                              </a>
                          </div>
                          <div class="col-md-4">
                              <a class="hover-img" href="#">
                                  <img src="img/100x100.png" alt="Image Alternative text" title="sunny wood" />
                              </a>
                          </div>
                      </div>
                  </div>
                  <div class="sidebar-widget">
                      <h4>Facebook</h4>
                      <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="1" data-header="1" data-show-border="1" data-width="233"></div>
                  </div>
              </aside>
          </div>
      </div>
  </div>



  <div class="gap"></div>
				@section('akhirbody')
				<script type="text/javascript">
				var reply=0;
				var idreply=1;
				var namakomentator="";
				var emailkomentator="";
				var pendapatkomentator="";
				$("#kotakreplyquote").hide();

				function replyquote(idcom){
				$("#kotakreplyquote").show();
				$('#reply').val(1);
				$('#com_id').val(idcom);
				$('#tanggalreply').html($('#tanggalkomen'+idcom).html());
				$('#namareply').html($('#namakomen'+idcom).html());
				$('#isireply').html($('#isikomen'+idcom).html());
				reply=1;
				}

				function tutupreply(){
				$('#reply').val(0);
					$("#kotakreplyquote").hide();
					reply=0;
				}
				</script>
				@endsection

@endsection
