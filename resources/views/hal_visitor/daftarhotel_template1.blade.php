@if (is_array($hotels))
@foreach($hotels as $h)
<?php
$biaya=$h['price'];
$biaya=angkaceil($biaya);
$hasildiskon=hasildiskon($settingan_member->mmid,"HTL",$biaya);
$selectedID=$h['selectedID'];
$namahotel=$h['name'];
$alamathotel=$h['address'];
$bintanghotel=$h['star'];
$alamatgambar=$h['image'];
$alamatgambar=str_replace("\\","",$alamatgambar);
$bahanid=$selectedID;
 ?>
 <div class="list-item-entry isiitem"  id="{{$bahanid}}" isiitem="1"  data-harga="{{$biaya}}" data-bintang="{{$bintanghotel}}">
   <div class="hotel-item style-3 bg-white">
     <div class="table-view">
         <div class="radius-top cell-view">
           <img src="http://{{$alamatgambar}}" alt="" style="width:100%; height:213.883px;">
         </div>
         <div class="title hotel-middle clearfix cell-view">
           <div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
           <div class="date grid-hidden"><strong><strike>{{rupiahceil($biaya)}}</strike> {{rupiahceil($hasildiskon)}} / </strong> malam</div>
             <h4><b>{{$namahotel}}</b></h4>

           <p class="f-14 grid-hidden">{{$alamathotel}}</p>
         </div>
         <div class="nomobile">
         <div class="title hotel-right clearfix cell-view">
           <div class="hotel-person color-dark-2"></div>
             <div class="hotel-person color-dark-2"></div>
           <div class="hotel-person color-dark-2"></div>
         </div>
         </div>
         <div class="title hotel-right clearfix cell-view">
           <div class="hotel-person color-dark-2">Bintang {{$bintanghotel}}</div>
     <a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="{{url('/')}}/hotel/{{$h['selectedID']}}">view more</a>
         </div>
       </div>
   </div>
</div>
@endforeach
@endif


<!--
<div class="list-item-entry isiitem"  id="{{$bahanid}}" isiitem="1"  data-harga="{{$biaya}}" data-bintang="{{$bintanghotel}}">
  <div class="hotel-item style-8 bg-white">
    <div class="table-view">
        <div class="radius-top cell-view">
          <img src="http://{{$alamatgambar}}" alt="" style="width:262.5px; height:213.883px;">
          <div class="price price-s-3 red tt">Bintang {{$bintanghotel}}</div>
        </div>
        <div class="title hotel-middle clearfix cell-view">
          <div class="hotel-person color-dark-2 list-hidden"><span></span></div>
              <div class="rate-wrap">
                <div class="rate">
        <span class="fa fa-star color-yellow"></span>
        <span class="fa fa-star color-yellow"></span>
        <span class="fa fa-star color-yellow"></span>
        <span class="fa fa-star color-yellow"></span>
        <span class="fa fa-star color-yellow"></span>
      </div>

              </div>
            <h4><b>{{$namahotel}}</b></h4>
          <p class="f-14">{{$alamathotel}}</p>
          <div class="hotel-icons-block grid-hidden">
            <img class="hotel-icon" src="{{ URL::asset('asettemplate1/img/tour_list/hotel_icon_1.png')}}" alt="">
            <img class="hotel-icon" src="{{ URL::asset('asettemplate1/img/tour_list/hotel_icon_2.png')}}" alt="">
            <img class="hotel-icon" src="{{ URL::asset('asettemplate1/img/tour_list/hotel_icon_3.png')}}" alt="">
            <img class="hotel-icon" src="{{ URL::asset('asettemplate1/img/tour_list/hotel_icon_4.png')}}" alt="">
            <img class="hotel-icon" src="{{ URL::asset('asettemplate1/img/tour_list/hotel_icon_5.png')}}" alt="">
          </div>
          <a href="#" class="c-button bg-dr-blue hv-dr-blue-o b-40 fl list-hidden">select</a>
          <a href="#" class="c-button color-dr-blue hv-o b-40 fr list-hidden"><img src="img/loc_icon_small_drak.png" alt="">view on map</a>
        </div>
        <div class="title hotel-right bg-dr-blue clearfix cell-view">
          <div class="hotel-person color-white"><span>{{rupiahceil($biaya)}}</span></div>
    <a class="c-button b-40 bg-white color-dark-2 hv-dark-2-o grid-hidden" href="#">view more</a>
        </div>
      </div>
  </div>
</div>
-->
