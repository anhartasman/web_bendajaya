@if (is_array($hotels))
@foreach($hotels as $h)
<?php
$biaya=$h['price'];
$biaya=angkaceil($biaya);
$hasildiskon=hasildiskon($settingan_member->mmid,"HTL",$biaya);
$selectedID=$h['selectedID'];
$namahotel=$h['name'];
$alamathotel=$h['address'];
$bintanghotel=$h['star'];
$alamatgambar=$h['image'];
$alamatgambar=str_replace("\\","",$alamatgambar);
$bahanid=$selectedID;

$arstar[0]="zero";
$arstar[1]="one";
$arstar[2]="two";
$arstar[3]="three";
$arstar[4]="four";
$arstar[5]="five";
 ?>
<article class="box isiitem" id="{{$bahanid}}" isiitem="1"  data-harga="{{$biaya}}" data-bintang="{{$bintanghotel}}">
    <figure class="col-sm-5 col-md-4">
        <img width="270" height="160" alt="" src="http://{{$alamatgambar}}">
    </figure>
    <div class="details col-sm-7 col-md-8">
        <div>
            <div>
                <h4 class="box-title">{{$namahotel}}</h4>
                <div class="amenities">
                    <i class="soap-icon-wifi circle"></i>
                    <i class="soap-icon-fitnessfacility circle"></i>
                    <i class="soap-icon-fork circle"></i>
                    <i class="soap-icon-television circle"></i>
                </div>
            </div>
            <div>
                <div class="{{$arstar[$bintanghotel]}}-stars-container">
                    <span class="{{$arstar[$bintanghotel]}}-stars" style="width: 80%;"></span>
                </div>
            </div>
        </div>
        <div>
            <p>{{$alamathotel}}</p>
            <div>
                <span class="price"><small>AVG/NIGHT</small><strike>{{rupiahceil($biaya)}}</strike> {{rupiahceil($hasildiskon)}}</span>
                <a class="button btn-small full-width text-center" title="" href="{{url('/')}}/hotel/{{$h['selectedID']}}">SELECT</a>
            </div>
        </div>
    </div>
</article>
@endforeach
@endif
