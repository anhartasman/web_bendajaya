@if (is_array($hotels))
@foreach($hotels as $h)
<?php
$biaya=$h['price'];
$biaya=angkaceil($biaya);
$hasildiskon=hasildiskon($settingan_member->mmid,"HTL",$biaya);
$selectedID=$h['selectedID'];
$namahotel=$h['name'];
$alamathotel=$h['address'];
$bintanghotel=$h['star'];
$alamatgambar=$h['image'];
$alamatgambar=str_replace("\\","",$alamatgambar);
$bahanid=$selectedID;

$arstar[0]="zero";
$arstar[1]="one";
$arstar[2]="two";
$arstar[3]="three";
$arstar[4]="four";
$arstar[5]="five";
 ?>
 <li class="isiitem" id="{{$bahanid}}" isiitem="1"  data-harga="{{$biaya}}" data-bintang="{{$bintanghotel}}">
     <a class="booking-item" href="{{url('/')}}/hotel/{{$h['selectedID']}}">
         <div class="row">
             <div class="col-md-3">
                 <div class="booking-item-img-wrap">
                     <img src="http://{{$alamatgambar}}" alt="" title="{{$namahotel}}" />

                 </div>
             </div>
             <div class="col-md-6">
                 <div class="booking-item-rating">
                     <ul class="icon-group booking-item-rating-stars">
                         <li><i class="fa fa-star<?php if($bintanghotel<1){print("-o");} ?>"></i>
                         </li>
                         <li><i class="fa fa-star<?php if($bintanghotel<2){print("-o");} ?>"></i>
                         </li>
                         <li><i class="fa fa-star<?php if($bintanghotel<3){print("-o");} ?>"></i>
                         </li>
                         <li><i class="fa fa-star<?php if($bintanghotel<4){print("-o");} ?>"></i>
                         </li>
                         <li><i class="fa fa-star<?php if($bintanghotel<5){print("-o");} ?>"></i>
                         </li>
                     </ul>
                 </div>
                 <h5 class="booking-item-title">{{$namahotel}}</h5>
                 <p class="booking-item-address"><i class="fa fa-map-marker"></i>{{$alamathotel}}</p>
             </div>
             <div class="col-md-3"><span class="booking-item-price-from">mulai dari </span><span class="booking-item-title"><strike>{{rupiahceil($biaya)}}</strike> {{rupiahceil($hasildiskon)}}</span><span>/malam</span><span class="btn btn-primary">Lihat</span>
             </div>
         </div>
     </a>
 </li>


@endforeach
@endif
