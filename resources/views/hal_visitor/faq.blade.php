@extends('layouts.master')

@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><a class="link-blue-2" href="#">FAQ</a> /</li>
		  				<li><span>faq</span></li>
		  			</ul>
		  			<h2 class="color-white">Tanya Jawab</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!--ACCORDION-->
<div class="main-wraper bg-dr-blue-2 padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h2 class="color-white">Pertanyaan & Jawaban</h2>
				</div>
			</div>
		</div>
        <div class="accordion style-6">
@foreach($faqs as $faq)
            <div class="acc-panel">
                <div class="acc-title"><span class="acc-icon"></span>{{$faq->pertanyaan}}</div>
                <div class="acc-body">
					<?php echo nl2br($faq->jawaban); ?>

                </div>
            </div>
	@endforeach

        </div>
	</div>
</div>

@endsection
