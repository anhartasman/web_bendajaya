@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Tanya Jawab</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Tanya Jawab</li>
				</ul>
		</div>
</div>

<section id="content">
		<div class="container">
				<div id="main" class="faqs style1">
						<div class="row">

								<div class="col-sm-8 col-md-12">
										<h2>Tanya jawab</h2>
										<div class="travelo-box question-list">
												<div class="toggle-container">
													<?php $i=0; ?>
													@foreach($faqs as $faq)
													<?php $i+=1; ?>
														<div class="panel style1">
																<h4 class="panel-title">
																		<a data-toggle="collapse" href="#tgg{{$i}}" class="collapsed">{{$faq->pertanyaan}}</a>
																</h4>
																<div id="tgg{{$i}}" class="panel-collapse collapse">
																		<div class="panel-content">
															<?php echo nl2br($faq->jawaban); ?>

																		</div>
																</div>
														</div>
													@endforeach




												</div>
										</div>

								</div>
						</div>
				</div>
		</div>
</section>

@endsection
