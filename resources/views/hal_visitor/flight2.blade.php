<!DOCTYPE html>
@extends('layouts.master')
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
    <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
 @endsection
@section('kontenweb')

            <!--
<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="#">home</a> /</li>
				<li><a class="link-blue-2" href="#">tours</a> /</li>
				<li><span class="color-red-3">list tours</span></li>
			</ul>
			<h2 class="color-white">all flights for you</h2>
			<h4 class="color-white">We found: <span class="color-red-3">640</span> flights</h4>
		</div>
	</div>
</div>
-->

  <div class="main-wraper padd-90 tabs-page">

      <div class="full-width" id="formatas">
      <div class="bg bg-bg-chrome act" style="background-image:url({{ URL::asset('asettemplate1/img/home_1/main_slide_1.jpg')}})">
      </div>
          <div class="container-fluid">
           <div class="row">
            <div class="col-md-12">
             <div class="baner-tabs">

                <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
                 <div class="tab-pane active in" id="one">
                   <div class="container">
                     <div class="row">
                       <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                         <div class="tabs-block">
                         <h5>Asal</h5>
                           <div class="input-style">
                           <select class="input-style" name="asal" id="pilasal">
                             	@foreach($areas as $area)
                             <option value="{{ $area['city'] }}-{{ $area['iata'] }}">{{ $area['city'] }} - {{ $area['iata'] }}</option>
                             @endforeach
                           </select> </div>
                         </div>
                       </div>
                         <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                           <div class="tabs-block">
                           <h5>Tujuan</h5>
                             <div class="input-style">
                             <select class="input-style" name="tujuan" id="piltujuan">
                               	@foreach($areas as $area)
                               <option value="{{ $area['city'] }}-{{ $area['iata'] }}">{{ $area['city'] }} - {{ $area['iata'] }}</option>
                               @endforeach
                             </select> </div>
                           </div>
                         </div>
                           <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                             <div class="tabs-block">
                             <h5>Rencana</h5>
                               <div class="input-style">
                               <select class="input-style" id="pilrencana" name="pilrencana">
                                 <option value="O" selected="selected">Sekali Jalan</option>
                                 <option value="R">Pulang Pergi</option>

                               </select> </div>
                             </div>
                           </div>
                       <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                         <div class="tabs-block">
                         <h5>Tanggal Berangkat</h5>
                           <div class="input-style">
                            <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                              <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                           </div>
                         </div>
                       </div>
                       <div id="tny_ret" class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                         <div class="tabs-block">
                         <h5>Tanggal Pulang</h5>
                           <div class="input-style">
                            <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                              <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                           </div>
                         </div>
                       </div>

                     </div>
                       <div class="row">

                         <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                           <div class="tabs-block">
                           <h5>Adults</h5>
                              <select class="input-style"id="adt" name="adt">

                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>

                              </select>
                           </div>
                         </div>
                         <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                           <div class="tabs-block">
                           <h5>Kids</h5>
                                 <select class="input-style"id="chd" name="chd">

                                   <option value="0">0</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                   <option value="6">6</option>

                                 </select>
                           </div>
                         </div>
                         <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                           <div class="tabs-block">
                           <h5>Infan</h5>
                                 <select class="input-style"id="inf" name="inf">

                                   <option value="0">0</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                   <option value="6">6</option>

                                 </select></div>
                         </div>

                         <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                           <a id="tomcari" href="#" class="c-button b-60 bg-aqua hv-transparent" onclick="cari()"><i class="fa fa-search"></i><span>search now</span></a>

                         </div>
                       </div>
                   </div>
                 </div>

               </div>
             </div>
           </div>
          </div>
         </div>
        </div>
      </div>
    </div>
  	<div class="list-wrapper bg-grey-2">
  		<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-9">


						<div class="list-content clearfix" id="dafloading" >


					<div class="list-item-entry" id="loading_qz">
							<div class="hotel-item style-10 bg-white">
								<div class="table-view">
										<div class="radius-top cell-view">
											<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-QZ.png')}}" alt="">
										</div>
										<div class="title hotel-middle cell-view">
											<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
											<h6 class="color-grey-3 list-hidden">one way flights</h6>
										 </div>
										<div class="title hotel-right clearfix cell-view grid-hidden">
									</div>
									</div>
							</div>
					</div>

					<div class="list-item-entry" id="loading_qg">
						<div class="hotel-item style-10 bg-white">
							<div class="table-view">
									<div class="radius-top cell-view">
										<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-QG.png')}}" alt="">
									</div>
									<div class="title hotel-middle cell-view">
										<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
										<h6 class="color-grey-3 list-hidden">one way flights</h6>
									 </div>
									<div class="title hotel-right clearfix cell-view grid-hidden">
								</div>
								</div>
						</div>
					</div>

					<div class="list-item-entry" id="loading_ga">
					<div class="hotel-item style-10 bg-white">
						<div class="table-view">
								<div class="radius-top cell-view">
									<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-GA.png')}}" alt="">
								</div>
								<div class="title hotel-middle cell-view">
									<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
									<h6 class="color-grey-3 list-hidden">one way flights</h6>
								 </div>
								<div class="title hotel-right clearfix cell-view grid-hidden">
							</div>
							</div>
					</div>
					</div>

					<div class="list-item-entry" id="loading_kd">
					<div class="hotel-item style-10 bg-white">
					<div class="table-view">
							<div class="radius-top cell-view">
								<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-KD.png')}}" alt="">
							</div>
							<div class="title hotel-middle cell-view">
								<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
								<h6 class="color-grey-3 list-hidden">one way flights</h6>
							 </div>
							<div class="title hotel-right clearfix cell-view grid-hidden">
						</div>
						</div>
					</div>
					</div>

					<div class="list-item-entry" id="loading_jt">
					<div class="hotel-item style-10 bg-white">
					<div class="table-view">
						<div class="radius-top cell-view">
							<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-JT.png')}}" alt="">
						</div>
						<div class="title hotel-middle cell-view">
							<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
							<h6 class="color-grey-3 list-hidden">one way flights</h6>
						 </div>
						<div class="title hotel-right clearfix cell-view grid-hidden">
					</div>
					</div>
					</div>
					</div>

					<div class="list-item-entry" id="loading_sj">
					<div class="hotel-item style-10 bg-white">
					<div class="table-view">
					<div class="radius-top cell-view">
						<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-SJ.png')}}" alt="">
					</div>
					<div class="title hotel-middle cell-view">
						<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
						<h6 class="color-grey-3 list-hidden">one way flights</h6>
					 </div>
					<div class="title hotel-right clearfix cell-view grid-hidden">
					</div>
					</div>
					</div>
					</div>

					<div class="list-item-entry" id="loading_mv">
					<div class="hotel-item style-10 bg-white">
					<div class="table-view">
					<div class="radius-top cell-view">
					<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-MV.png')}}" alt="">
					</div>
					<div class="title hotel-middle cell-view">
					<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
					<h6 class="color-grey-3 list-hidden">one way flights</h6>
					</div>
					<div class="title hotel-right clearfix cell-view grid-hidden">
					</div>
					</div>
					</div>
					</div>

					<div class="list-item-entry" id="loading_il">
					<div class="hotel-item style-10 bg-white">
					<div class="table-view">
					<div class="radius-top cell-view">
					<img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-IL.png')}}" alt="">
					</div>
					<div class="title hotel-middle cell-view">
					<img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
					<h6 class="color-grey-3 list-hidden">one way flights</h6>
					</div>
					<div class="title hotel-right clearfix cell-view grid-hidden">
					</div>
					</div>
					</div>
					</div>



					</div>

					</div>
          <div class="col-xs-12 col-sm-8 col-md-12" id="ketpilihanaa">
          <div class="list-header clearfix"  id="asdasd">
                <a href="#ketpilihan" class="c-button b-40 bg-red-3 hv-red-3-o" id="tomulangipencarian"  >ULANGI PENCARIAN</a>

  </div>
  	</div>
	<div class="col-xs-12 col-sm-8 col-md-12" id="ketpilihanaa">


									<div class="list-header clearfix"  style="background-color: #ff6600;"id="labelpilihanpergi">

											<h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Penerbangan yang dipilih : Padang (PDG) - Jakarta (CGK) | Dewasa: 1 Anak: 0 Bayi: 0</h4>

									</div>

                  									<div class="list-content clearfix" >


                  <div class="list-item-entry" id="pilihanpergi">
                <div class="hotel-item style-10 bg-white">
                <div class="table-view">
                <div class="radius-top cell-view"  >
                <img style="padding-left:20px;width:180px;height:50px;" id="pilihanpergi_gbr" src="{{ URL::asset('asettemplate1/img/ac/Airline-GA')}}.png" alt="">
                </div>
                <div class="title hotel-middle cell-view">
                <h5 id="pilihanpergi_dafpes">  DAF TRAN  </h5>

                 <strong class="color-red-3" id="pilihanpergi_harga">

                BIATA

                 </strong> </h5>

                <h6 class="color-grey-3 list-hidden">one way flights</h6>
                <!----  <h4><b>Cheap Flights to Paris</b></h4>-->
                  <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                  <div class="fi_block grid-hidden row row10">
                    <div class="flight-icon col-xs-6 col10">
                      <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                      <div class="fi_content">
                        <div class="fi_title color-dark-2">pergi</div>
                        <div class="fi_text color-grey" id="pilihanpergi_jampergi">$jampergiberangkat</div>
                      </div>
                    </div>
                    <div class="flight-icon col-xs-6 col10">
                      <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                      <div class="fi_content">
                        <div class="fi_title color-dark-2">tiba</div>
                        <div class="fi_text color-grey" id="pilihanpergi_jamtiba">$jampergitiba</div>
                      </div>
                    </div>
                  </div>
               <!----<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>-->
                </div>

                <div class="title hotel-right clearfix cell-view grid-hidden">
                    <!----  <div class="hotel-right-text color-dark-2">one way flights</div>
                    <div class="hotel-right-text color-dark-2">1 stop</div>
                  -->
                </div>
                </div>
                </div>
                </div>




<div class="list-item-entry" id="pilihanpulang" >
<div class="hotel-item style-10 bg-white">
<div class="table-view">
<div class="radius-top cell-view"  >
<img style="padding-left:20px;width:180px;height:50px;"id="pilihanpulang_gbr" src="{{ URL::asset('asettemplate1/img/ac/Airline-GA')}}.png" alt="">
</div>
<div class="title hotel-middle cell-view">
<h5 id="pilihanpulang_dafpes">  DAF TRAN  </h5>

<strong class="color-red-3" id="pilihanpulang_harga">

BIATA

</strong> </h5>

<h6 class="color-grey-3 list-hidden">one way flights</h6>
<!----  <h4><b>Cheap Flights to Paris</b></h4>-->
<p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
<div class="fi_block grid-hidden row row10">
<div class="flight-icon col-xs-6 col10">
<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
<div class="fi_content">
  <div class="fi_title color-dark-2">pergi</div>
  <div class="fi_text color-grey" id="pilihanpulang_jampergi">$jampergiberangkat</div>
</div>
</div>
<div class="flight-icon col-xs-6 col10">
<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
<div class="fi_content">
  <div class="fi_title color-dark-2">tiba</div>
  <div class="fi_text color-grey" id="pilihanpulang_jamtiba">$jampergitiba</div>
</div>
</div>
</div>
<!----<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>-->
</div>

<div class="title hotel-right clearfix cell-view grid-hidden">
<!----  <div class="hotel-right-text color-dark-2">one way flights</div>
<div class="hotel-right-text color-dark-2">1 stop</div>
-->
</div>
</div>
</div>
</div>



              </div>


</div>

					<div class=" col-xs-12 col-sm-6 col-md-6">
						<div class="list-header clearfix" style="background-color: #ff6600;">

								<h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkiri" >Jakarta (CKG) - Jogja (JOG)</h4>

						</div>
									<div class="list-content clearfix" id="dafpergi">

									<div class="list-item-entry">
														<div class="radius-top cell-view">
															<img src="{{ URL::asset('asettemplate1/img/tour_list/flight_grid_1.jpg')}}" alt="">
														</div>
														<div class="title hotel-middle cell-view">
																<h4><b>Cheap Flights to Paris</b></h4>
																<p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
																<div class="fi_block grid-hidden row row10">
																	<div class="flight-icon col-xs-6 col10">
																		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
																		<div class="fi_content">
																			<div class="fi_title color-dark-2">take off</div>
																			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
																		</div>
																	</div>
																	<div class="flight-icon col-xs-6 col10">
																		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
																		<div class="fi_content">
																			<div class="fi_title color-dark-2">take off</div>
																			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
																		</div>
																	</div>
																</div>

														</div>
														<div class="title hotel-right clearfix cell-view grid-hidden">
																	<div class="hotel-right-text color-dark-2">one way flights</div>
																	<div class="hotel-right-text color-dark-2">1 stop</div>
														</div>
									</div>




						</div>
					</div>

					<div class=" col-xs-12 col-sm-6 col-md-6">
						<div class="list-header clearfix" style="background-color: #ff6600;">

								 <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Jogja (JOG) - Jakarta (CKG)</h4>

						</div>
								<div class="list-content clearfix" id="dafpulang">

									<div class="list-item-entry">
														<div class="radius-top cell-view">
															<img src="{{ URL::asset('asettemplate1/img/tour_list/flight_grid_1.jpg')}}" alt="">
														</div>
														<div class="title hotel-middle cell-view">
																<h4><b>Cheap Flights to Paris</b></h4>
																<p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
																<div class="fi_block grid-hidden row row10">
																	<div class="flight-icon col-xs-6 col10">
																		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
																		<div class="fi_content">
																			<div class="fi_title color-dark-2">take off</div>
																			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
																		</div>
																	</div>
																	<div class="flight-icon col-xs-6 col10">
																		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
																		<div class="fi_content">
																			<div class="fi_title color-dark-2">take off</div>
																			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
																		</div>
																	</div>
																</div>

														 </div>
														<div class="title hotel-right clearfix cell-view grid-hidden">
																	<div class="hotel-right-text color-dark-2">one way flights</div>
																	<div class="hotel-right-text color-dark-2">1 stop</div>
														</div>
									</div>


								</div>

					</div>

				</div>
  		</div>
  	</div>

		@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
<script type="text/javascript">
var banasal="";
var kotasal="";
var labelasal="";
var kottujuan="";
var bantujuan="";
var labeltujuan="";
var pilrencana="O";
var tglber_d="";
var tglber_m="";
var tglber_y="";

var tglpul_d="";
var tglpul_m="";
var tglpul_y="";

var tglberangkat="<?php echo date("Y-m-d");?>";
var tglpulang="<?php echo date("Y-m-d");?>";

var jumadt="1";
var jumchd="0";
var juminf="0";

var urljadwal="{{ url('/') }}/jadwalPesawat/";
var urljadwalb=urljadwal;
$('#tomulangipencarian').hide('slow');


$('#tomulangipencarian').on('click', function() {


$('#formatas').show('slow');

//$('#labelpilihanpergi').hide('slow');
$('#tomulangipencarian').hide('slow');

});
$('#adt').on('change', function() {
jumadt=this.value;

});
$('#chd').on('change', function() {
jumchd=this.value;

});
$('#inf').on('change', function() {
juminf=this.value;

});


$('#labelpilihanpergi').hide();
$('#pilihanpergi').hide();
$('#pilihanpulang').hide();
  $('select').select2();
  $('#tny_ret').hide();
	                $('#loading_qz').hide();
	                $('#loading_qg').hide();
									$('#loading_ga').hide();
									$('#loading_kd').hide();
									$('#loading_jt').hide();
									$('#loading_sj').hide();
									$('#loading_mv').hide();
									$('#loading_il').hide();

$('#tglberangkat').datepicker({ dateFormat: 'dd-mm-yy' }).val();
$('#tglpulang').datepicker({ dateFormat: 'dd-mm-yy' }).val();

$('#tglberangkat').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

});

$('#tglpulang').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglpul_d=values[0];
tglpul_m=values[1];
tglpul_y=values[2];

tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;

});

$('#pilasal').on('change', function() {
  var values=this.value.split('-');
  var kotas=values[1];
  kotasal=values[0];
  //alert(kotas);
  banasal=kotas;
  labelasal=this.value;

});
$('#piltujuan').on('change', function() {
  var values=this.value.split('-');
  var kottuj=values[1];
  kottujuan=values[0];
  //alert(kottuj);
  bantujuan=kottuj;
  labeltujuan=this.value;

});
$('#pilrencana').on('change', function() {
  //alert( this.value );
  pilrencana=this.value;
  var tny_ret = document.getElementById("tny_ret").value;

if(this.value=="O"){
    $('#tny_ret').hide();
  }else{
    $('#tny_ret').show();
  }


});

function setkolom(){
labelutama();
  $('#labelkolomkiri').html(kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+")");
if(pilrencana=="R"){
  $('#labelkolomkanan').html(kottujuan+" ("+bantujuan+") - "+kotasal+" ("+banasal+")");
}
}
function labelutama(){
  $('#labelkolomutama').html('Penerbangan yang dipilih : '+kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+") | Dewasa: "+jumadt+" Anak: "+jumchd+" Bayi: "+juminf);

}
function pilihPergi(bahanid
  ,daftranpergi
  ,jampergiberangkat
  ,jampergitiba
  ,harga
  ,ikon
  ,stattransitpergi
  ){
$('#labelpilihanpergi').show('slow');
$('#pilihanpergi').show('slow');
//  alert(jampergitiba);
        $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
        //  alert($('#pilihanpergi_gbr').attr('src'));
        $('#pilihanpergi_dafpes').html(daftranpergi+" ("+stattransitpergi+")");
        $('#pilihanpergi_jampergi').html(jampergiberangkat);
        $('#pilihanpergi_jamtiba').html(jampergitiba);
        $('#pilihanpergi_harga').html(harga);


}
function pilihPulang(bahanid
  ,daftranpulang
  ,jampulangberangkat
  ,jampulangtiba
  ,harga
  ,ikon
  ,stattransitpulang
  ){
  $('#pilihanpulang').show('slow');
//  alert(jampergitiba);
        $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
        //  alert($('#pilihanpergi_gbr').attr('src'));
        $('#pilihanpulang_dafpes').html(daftranpulang+" ("+stattransitpulang+")");
        $('#pilihanpulang_jampergi').html(jampulangberangkat);
        $('#pilihanpulang_jamtiba').html(jampulangtiba);
        $('#pilihanpulang_harga').html(harga);

}

function sembunyi(){
      $('#loadmaskapai').hide('slow');
}
function cari(){
//  alert("Memulai pencarian");
        //  $('#hasilkirim').html("<b>Hello world!</b>");
        urljadwalb=urljadwal;
        urljadwalb+="org/"+banasal;
        urljadwalb+="/des/"+bantujuan;
        urljadwalb+="/flight/"+pilrencana;
        urljadwalb+="/tglberangkat/"+tglberangkat;
        urljadwalb+="/tglpulang/"+tglpulang;
        urljadwalb+="/jumadt/"+jumadt;
        urljadwalb+="/jumchd/"+jumchd;
        urljadwalb+="/juminf/"+juminf;
        //akhir urljadwalb+="ac/";
                $('#loading_qz').show('fadeOut');
								$('#loading_qg').show('fadeOut');
								$('#loading_ga').show('fadeOut');
								$('#loading_kd').show('fadeOut');
								$('#loading_jt').show('fadeOut');
								$('#loading_sj').show('fadeOut');
								$('#loading_mv').show('fadeOut');
								$('#loading_il').show('fadeOut');

                $('#dafpergi').html('');
								$('#dafpulang').html('');
                urljadwalb+="/ac/";

        ambildata_qz();
				ambildata_qg();
				ambildata_ga();
				ambildata_kd();
				ambildata_jt();
				ambildata_sj();
				ambildata_mv();
				ambildata_il();
        $('#formatas').hide('slow');
      //  $('#pilihanpergi').hide();
      //  $('#pilihanpulang').hide();

      $('#labelpilihanpergi').show('slow');
      $('#tomulangipencarian').show('slow');

      //  alert(urljadwalb);
      setkolom();
}
var ajaxku_il;
function ambildata_il(){
  ajaxku_il = buatajax();
  var url=urljadwalb+"IL";
  ajaxku_il.onreadystatechange=stateChanged_il;
  ajaxku_il.open("GET",url,true);
  ajaxku_il.send(null);
}

 function stateChanged_il(){
   var data;
    if (ajaxku_il.readyState==4){
      data=ajaxku_il.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_il').hide('slow');
     }
}

var ajaxku_mv;
function ambildata_mv(){
  ajaxku_mv = buatajax();
  var url=urljadwalb+"MV";
  ajaxku_mv.onreadystatechange=stateChanged_mv;
  ajaxku_mv.open("GET",url,true);
  ajaxku_mv.send(null);
}

 function stateChanged_mv(){
   var data;
    if (ajaxku_mv.readyState==4){
      data=ajaxku_mv.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_mv').hide('slow');
     }
}

var ajaxku_sj;
function ambildata_sj(){
  ajaxku_sj = buatajax();
  var url=urljadwalb+"SJ";
  ajaxku_sj.onreadystatechange=stateChanged_sj;
  ajaxku_sj.open("GET",url,true);
  ajaxku_sj.send(null);
}

 function stateChanged_sj(){
   var data;
    if (ajaxku_sj.readyState==4){
      data=ajaxku_sj.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_sj').hide('slow');
     }
}

var ajaxku_jt;
function ambildata_jt(){
  ajaxku_jt = buatajax();
  var url=urljadwalb+"JT";
  ajaxku_jt.onreadystatechange=stateChanged_jt;
  ajaxku_jt.open("GET",url,true);
  ajaxku_jt.send(null);
}

 function stateChanged_jt(){
   var data;
    if (ajaxku_jt.readyState==4){
      data=ajaxku_jt.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_jt').hide('slow');
     }
}

var ajaxku_kd;
function ambildata_kd(){
  ajaxku_kd = buatajax();
  var url=urljadwalb+"KD";
  ajaxku_kd.onreadystatechange=stateChanged_kd;
  ajaxku_kd.open("GET",url,true);
  ajaxku_kd.send(null);
}

 function stateChanged_kd(){
   var data;
    if (ajaxku_kd.readyState==4){
      data=ajaxku_kd.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_kd').hide('slow');
     }
}


var ajaxku_ga;
function ambildata_ga(){
  ajaxku_ga = buatajax();
  var url=urljadwalb+"GA";
  ajaxku_ga.onreadystatechange=stateChanged_ga;
  ajaxku_ga.open("GET",url,true);
  ajaxku_ga.send(null);
}

 function stateChanged_ga(){
   var data;
    if (ajaxku_ga.readyState==4){
      data=ajaxku_ga.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_ga').hide('slow');
     }
}

var ajaxku_qz;
function ambildata_qz(){
  ajaxku_qz = buatajax();
  var url=urljadwalb+"QZ";
  ajaxku_qz.onreadystatechange=stateChanged_qz;
  ajaxku_qz.open("GET",url,true);
  ajaxku_qz.send(null);
}
 function stateChanged_qz(){
   var data;
    if (ajaxku_qz.readyState==4){
      data=ajaxku_qz.responseText;
      if(data.length>0){
                tambahData(data);
       }else{
       }
			 				        $('#loading_qz').hide('slow');
     }
}


var ajaxku_qg;
function ambildata_qg(){
  ajaxku_qg = buatajax();
  var url=urljadwalb+"QG";
  ajaxku_qg.onreadystatechange=stateChanged_qg;
  ajaxku_qg.open("GET",url,true);
  ajaxku_qg.send(null);
}
 function stateChanged_qg(){
   var data;
    if (ajaxku_qg.readyState==4){
      data=ajaxku_qg.responseText;
      if(data.length>0){
        //document.getElementById("hasilkirim").html = data;

                tambahData(data);
       }else{
       }
			 				        $('#loading_qg').hide('slow');
     }
}

function tambahData(data){

	                $('#dafpergi').html(data+$('#dafpergi').html());
									$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
					 			  $('.bagpulang').hide('slow');
}

var ajaxku;
function ambildata(){
  ajaxku = buatajax();
  var url="{{ url('/') }}/jadwalPesawat";
  //url=url+"?q="+nip;
  //url=url+"&sid="+Math.random();
  ajaxku.onreadystatechange=stateChanged;
  ajaxku.open("GET",url,true);
  ajaxku.send(null);
}
function buatajax(){
  if (window.XMLHttpRequest){
    return new XMLHttpRequest();
  }
  if (window.ActiveXObject){
     return new ActiveXObject("Microsoft.XMLHTTP");
   }
   return null;
 }
 function stateChanged(){
   var data;
    if (ajaxku.readyState==4){
      data=ajaxku.responseText;
      if(data.length>0){
        //document.getElementById("hasilkirim").html = data;

				        $('#loading_qz').hide('show');
                $('#hasilkirim').append(data);
       }else{
        // document.getElementById("hasilkirim").html = "";
              //   $('#hasilkirim').html("");
       }
     }
}
</script>
@endsection

@endsection
