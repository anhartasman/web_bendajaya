<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>
<form class="contact-form"id="formgo" action="flight/flight_isiform" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_acDep" name="formgo_acDep">
<input type="hidden" id="formgo_acRet" name="formgo_acRet">
<input type="hidden" id="formgo_flight" name="formgo_flight" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_daftranpergi" name="formgo_daftranpergi" >
<input type="hidden" id="formgo_daftranpulang" name="formgo_daftranpulang" >
<input type="hidden" id="formgo_stattransitpergi" name="formgo_stattransitpergi" >
<input type="hidden" id="formgo_stattransitpulang" name="formgo_stattransitpulang" >
<input type="hidden" id="formgo_dafsubclasspergi" name="formgo_dafsubclasspergi" >
<input type="hidden" id="formgo_dafsubclasspulang" name="formgo_dafsubclasspulang" >
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>
<!--
<div class="inner-banner style-6">
<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
<div class="vertical-align">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
            <ul class="banner-breadcrumb color-white clearfix">
          	<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
				<li><span class="color-red-3">penerbangan</span></li>
      </ul>
      </div>
    </div>
</div>
</div>
</div>
-->

  <div class="main-wraper padd-90 tabs-page">
    <!-- BAGIAN PENCARIAN -->

      <div class="full-width">
      <div class="bg bg-bg-chrome act" style="background-image:url({{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}})">
      </div>
          <div class="container-fluid">

           <div class="row">
            <div class="col-md-12">
             <div class="baner-tabs">
                <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
                 <div class="tab-pane active in" id="one">
                   <div class="container">

                     				<div class="row">
                     					<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     						<div class="tabs-block ">
                     						<h5>Asal</h5>
                     							<div class="input-style">
                     							<select class="input-style selektwo" name="asal" id="pilasal">
                     								 @foreach($dafban as $area)
                     								<option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                     								@endforeach
                     							</select> </div>
                     						</div>
                     					</div>
                     						<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     							<div class="tabs-block">
                     							<h5>Tujuan</h5>
                     								<div class="input-style">
                     								<select class="input-style selektwo" name="tujuan" id="piltujuan">
                     									 @foreach($dafban as $area)
                     									<option value="{{ $area->nama }}-{{ $area->kode }}">{{ $area->nama }} - {{ $area->kode }}</option>
                     									@endforeach
                     								</select> </div>
                     							</div>
                     						</div>
                     							<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     								<div class="tabs-block">
                     								<h5>Rencana</h5>
                     									<div class="input-style">
                     									<select class="mainselection" id="pilrencana" name="pilrencana">
                     										<option value="O" selected="selected">Sekali Jalan</option>
                     										<option value="R">Pulang Pergi</option>

                     									</select> </div>
                     								</div>
                     							</div>
                     					<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                     						<div class="tabs-block">
                     						<h5>Tanggal Berangkat</h5>
                     							<div class="input-style">
                     							 <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                     								 <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                     							</div>
                     						</div>
                     					</div>
                     					<div id="tny_ret" class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                     						<div class="tabs-block">
                     						<h5>Tanggal Pulang</h5>
                     							<div class="input-style">
                     							 <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                     								 <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                     							</div>
                     						</div>
                     					</div>

                     				</div>
                     					<div class="row">

                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Adults</h5>
                     								 <select class="mainselection" id="adt" name="adt">

                     									 <option value="1">1</option>
                     									 <option value="2">2</option>
                     									 <option value="3">3</option>
                     									 <option value="4">4</option>
                     									 <option value="5">5</option>
                     									 <option value="6">6</option>
                     									 <option value="7">7</option>

                     								 </select>
                     							</div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Kids</h5>
                     										<select class="mainselection"id="chd" name="chd">

                     											<option value="0">0</option>
                     											<option value="1">1</option>
                     											<option value="2">2</option>
                     											<option value="3">3</option>
                     											<option value="4">4</option>
                     											<option value="5">5</option>
                     											<option value="6">6</option>

                     										</select>
                     							</div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Infan</h5>
                     										<select class="mainselection"id="inf" name="inf">

                     											<option value="0">0</option>
                     											<option value="1">1</option>
                     											<option value="2">2</option>
                     											<option value="3">3</option>
                     											<option value="4">4</option>
                     											<option value="5">5</option>
                     											<option value="6">6</option>

                     										</select></div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4"style="padding-top:2%;">
                     							<div >
                                 <input type="submit" id="tomcari" onclick="cari()" class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
                                   </div>
                     						</div>


                     	</div>

                   </div>
                 </div>

               </div>
             </div>
           </div>
          </div>
         </div>
        </div>
      </div>
    </div>
  	<div class="list-wrapper bg-grey-2">
  		<div class="container">
        <!-- sisi atas pilihan-->
  			<div class="row">
          	<div class="col-xs-12 col-sm-8 col-md-12">
                  <div class="list-content clearfix" id="dafloading" >

                    <div class="list-item-entry" id="loading_qz">
                        <div class="hotel-item style-10 bg-white">
                          <div class="table-view">
                              <div class="radius-top cell-view">
                                <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-QZ.png')}}" alt="">
                              </div>
                              <div class="title hotel-middle cell-view">
                                <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                                <h6 class="color-grey-3 list-hidden">one way flights</h6>
                               </div>
                              <div class="title hotel-right clearfix cell-view grid-hidden">
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="list-item-entry" id="loading_qg">
                      <div class="hotel-item style-10 bg-white">
                        <div class="table-view">
                            <div class="radius-top cell-view">
                              <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-QG.png')}}" alt="">
                            </div>
                            <div class="title hotel-middle cell-view">
                              <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                              <h6 class="color-grey-3 list-hidden">one way flights</h6>
                             </div>
                            <div class="title hotel-right clearfix cell-view grid-hidden">
                          </div>
                          </div>
                      </div>
                    </div>

                    <div class="list-item-entry" id="loading_ga">
                    <div class="hotel-item style-10 bg-white">
                      <div class="table-view">
                          <div class="radius-top cell-view">
                            <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-GA.png')}}" alt="">
                          </div>
                          <div class="title hotel-middle cell-view">
                            <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                            <h6 class="color-grey-3 list-hidden">one way flights</h6>
                           </div>
                          <div class="title hotel-right clearfix cell-view grid-hidden">
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_kd">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                        <div class="radius-top cell-view">
                          <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-KD.png')}}" alt="">
                        </div>
                        <div class="title hotel-middle cell-view">
                          <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                          <h6 class="color-grey-3 list-hidden">one way flights</h6>
                         </div>
                        <div class="title hotel-right clearfix cell-view grid-hidden">
                      </div>
                      </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_jt">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                      <div class="radius-top cell-view">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-JT.png')}}" alt="">
                      </div>
                      <div class="title hotel-middle cell-view">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                        <h6 class="color-grey-3 list-hidden">one way flights</h6>
                       </div>
                      <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_sj">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-SJ.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                      <h6 class="color-grey-3 list-hidden">one way flights</h6>
                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_mv">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                    <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-MV.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                    <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                    <h6 class="color-grey-3 list-hidden">one way flights</h6>
                    </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_il">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                    <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('asettemplate1/img/ac/Airline-IL.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                    <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                    <h6 class="color-grey-3 list-hidden">one way flights</h6>
                    </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>


                  </div>
            </div>
        </div>

  			<div class="row">

  				<div class="col-xs-12 col-sm-8 col-md-12">

              <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Penerbangan yang dipilih : Padang (PDG) - Jakarta (CGK) | Dewasa: 1 Anak: 0 Bayi: 0</h4>

              </div>
  					<div class="list-content clearfix" >
  						<div class="list-item-entry" id="pilihanpergi">
  					        <div class="hotel-item style-10 bg-white">
  					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img style="padding-left:20px;width:180px;height:50px;"id="pilihanpergi_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5 id="pilihanpergi_dafpes">  DAF TRAN  </h5>
							            <h6 class="color-grey-3 list-hidden">one wayaa flights</h6>
                          <h4 id="hsubclasspergi"><b>
                            SubClass
                            <select   name="pilsubclasspergi" id="pilsubclasspergi">
                                <option value="0" >sad </option>
                                  <option value="0" >sad1 </option>

                            </select>



                          </b></h4>
                            <h4><b id="pilihanpergi_harga">Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">Pergi</div>
						          	    			<div class="fi_text color-grey" id="pilihanpergi_jampergi">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">Tiba</div>
						          	    			<div class="fi_text color-grey" id="pilihanpergi_jamtiba">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>

							           </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	      <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>-->
                              <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>
                        </div>
					            </div>
					        </div>
  						</div>

    					<div class="list-item-entry" id="pilihanpulang" style="padding-bottom:0px;">
  					        <div class="hotel-item style-10 bg-white">
  					        	<div class="table-view">
  						          	<div class="radius-top cell-view">
  						          	 	<img style="padding-left:20px;width:180px;height:50px;" id="pilihanpulang_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
  						          	</div>
  						          	<div class="title hotel-middle cell-view">
  							          <h5 id="pilihanpulang_dafpes">  DAF TRAN  </h5>
  							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
                            <h4 id="hsubclasspulang"><b>
                              SubClass
                              <select   name="pilsubclasspulang" id="pilsubclasspulang">
                                  <option value="0" >sad </option>
                                    <option value="0" >sad1 </option>

                              </select>
                            </b></h4>
                              <h4><b id="pilihanpulang_harga">Cheap Flights to Paris</b></h4>
  						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
  						          	    <div class="fi_block grid-hidden row row10">
  						          	    	<div class="flight-icon col-xs-6 col10">
  						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
  						          	    		<div class="fi_content">
  						          	    			<div class="fi_title color-dark-2">Pergi</div>
  						          	    			<div class="fi_text color-grey" id="pilihanpulang_jampergi">wed nov 13, 2013 7:50 am</div>
  						          	    		</div>
  						          	    	</div>
  						          	    	<div class="flight-icon col-xs-6 col10">
  						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
  						          	    		<div class="fi_content">
  						          	    			<div class="fi_title color-dark-2">Tiba</div>
  						          	    			<div class="fi_text color-grey" id="pilihanpulang_jamtiba">wed nov 13, 2013 7:50 am</div>
  						          	    		</div>
  						          	    	</div>
  						          	    </div>
  							           </div>
  						            <div class="title hotel-right clearfix cell-view grid-hidden">
  					          	      <!----     <div class="hotel-right-text color-dark-2">one way flights</div>
  					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
                                -->

                      <button type="button" class="btn btn-block btn-primary" id="tomgoret">Lanjutkan Transaksi</button>
                          </div>
  					            </div>
  					        </div>
    						</div>



  					</div>


  				</div>
        </div>
  			<div class="row">
					<!-- sisi kiri -->
  				<div class="col-xs-12 col-sm-8 col-md-6" id="sisikiri">

              <div class="list-header clearfix" style="background-color: #ff6600;">

                  <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkiri" ></h4>

              </div>
  					<div class="list-content clearfix" id="dafpergi">
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o" id="tomteskir">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>

  					</div>

  				</div>
					<!-- sisi kanan -->
	  				<div class="col-xs-12 col-sm-8 col-md-6" id="sisikanan">

              <div class="list-header clearfix" style="background-color: #ff6600;">

                   <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" ></h4>

              </div>
	  					<div class="list-content clearfix" id="dafpulang">
	  						<div class="list-item-entry">
						        <div class="hotel-item style-10 bg-white">
						        	<div class="table-view">
							          	<div class="radius-top cell-view">
							          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
							          	</div>
							          	<div class="title hotel-middle cell-view">
								            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
								            <h6 class="color-grey-3 list-hidden">one way flights</h6>
							          	    <h4><b>Cheap Flights to Paris</b></h4>
							          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
							          	    <div class="fi_block grid-hidden row row10">
							          	    	<div class="flight-icon col-xs-6 col10">
							          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
							          	    		<div class="fi_content">
							          	    			<div class="fi_title color-dark-2">take off</div>
							          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
							          	    		</div>
							          	    	</div>
							          	    	<div class="flight-icon col-xs-6 col10">
							          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
							          	    		<div class="fi_content">
							          	    			<div class="fi_title color-dark-2">take off</div>
							          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
							          	    		</div>
							          	    	</div>
							          	    </div>
								            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
								            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
							            </div>
							            <div class="title hotel-right clearfix cell-view grid-hidden">
						          	        <div class="hotel-right-text color-dark-2">one way flights</div>
						          	        <div class="hotel-right-text color-dark-2">1 stop</div>
							            </div>
						            </div>
						        </div>
	  						</div>

	  					</div>

	  				</div>
  			</div>
  		</div>
  	</div>


				@section('akhirbody')
		<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
		<script type="text/javascript">

    $("#sisikanan").hide();
    $("#sisikiri").hide();
    <!--
    $('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
   }).val();
   -->

    //variabel

		var hargapulang="0";
		var hargapergi="0";
    var nilpil=0;
    var idpilper="";
    var idpilret="";
		var banasal="CGK";
		var kotasal="Jakarta";
		var labelasal="";
		var kottujuan="Manokwari";
		var bantujuan="MKW";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_dep').val(tglberangkat);
    $('#formgo_tgl_deppilihan').val("<?php echo date("Y-m-d");?>");
		var tglpulang="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_ret').val(tglpulang);
    $('#formgo_tgl_deppilihan').val("<?php echo date("Y-m-d");?>");

		var jumadt="1";
		var jumchd="0";
		var juminf="0";



		var urljadwal="{{ url('/') }}/jadwalPesawat/";
		var urljadwalb=urljadwal;
		$('#tomgoret').hide();
		$('#tomgoone').hide();
		$('#hsubclasspergi').hide();
		$('#hsubclasspulang').hide();
		$('#tomulangipencarian').hide('slow');

    $( "#tomgoone" ).click(function() {
      $( "#formgo" ).submit();
    });
    $( "#tomgoret" ).click(function() {
      $( "#formgo" ).submit();
    });
    $( "#tomteskir" ).click(function() {
      $( "#formgo" ).submit();
    });

		$('#tomulangipencarian').on('click', function() {


		$('#formatas').show('slow');

		//$('#labelpilihanpergi').hide('slow');
		$('#tomulangipencarian').hide('slow');

		});
		$('#adt').on('change', function() {
		jumadt=this.value;
    $('#formgo_adt').val(jumadt);

		});
		$('#chd').on('change', function() {
		jumchd=this.value;
    $('#formgo_chd').val(jumchd);

		});
		$('#inf').on('change', function() {
		juminf=this.value;
    $('#formgo_inf').val(juminf);

		});


		$('#labelpilihanpergi').hide();
		$('#pilihanpergi').hide();
		$('#pilihanpulang').hide();
		  $('.selektwo').select2();
		  $('#tny_ret').hide();
			                $('#loading_qz').hide();
			                $('#loading_qg').hide();
											$('#loading_ga').hide();
											$('#loading_kd').hide();
											$('#loading_jt').hide();
											$('#loading_sj').hide();
											$('#loading_mv').hide();
											$('#loading_il').hide();

		$('#tglberangkat').datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$('#tglpulang').datepicker({ dateFormat: 'dd-mm-yy' }).val();

		$('#tglberangkat').on('change', function() {

		var values=this.value.split('-');
		//alert(this.value);
		tglber_d=values[0];
		tglber_m=values[1];
		tglber_y=values[2];

		tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

    $('#formgo_tgl_deppilihan').val(tglberangkat);

		});

		$('#tglpulang').on('change', function() {

		var values=this.value.split('-');
		//alert(this.value);
		tglpul_d=values[0];
		tglpul_m=values[1];
		tglpul_y=values[2];

		tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
    $('#formgo_tgl_retpilihan').val(tglpulang);

		});

		$('#pilasal').on('change', function() {
		  var values=this.value.split('-');
		  var kotas=values[1];
		  kotasal=values[0];
		  //alert(kotas);
		  banasal=kotas;
		  labelasal=this.value;
      $('#formgo_labelorg').val(labelasal);
      $('#formgo_org').val(banasal);
      $('#formgo_kotorg').val(kotasal);

		});
		$('#piltujuan').on('change', function() {
		  var values=this.value.split('-');
		  var kottuj=values[1];
		  kottujuan=values[0];
		  //alert(kottuj);
		  bantujuan=kottuj;
		  labeltujuan=this.value;
      $('#formgo_labeldes').val(labeltujuan);
      $('#formgo_des').val(kottuj);
      $('#formgo_kotdes').val(kottujuan);

		});
		$('#pilrencana').on('change', function() {
		  //alert( this.value );
		  pilrencana=this.value;
      $('#formgo_flight').val(pilrencana);
		  var tny_ret = document.getElementById("tny_ret").value;

		if(this.value=="O"){
		    $('#tny_ret').hide();
     }else{
		    $('#tny_ret').show();
     }


		});

		function setkolom(){
		labelutama();
		  $('#labelkolomkiri').html(kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+")");
		if(pilrencana=="R"){
		  $('#labelkolomkanan').html(kottujuan+" ("+bantujuan+") - "+kotasal+" ("+banasal+")");
		}
		}
		function labelutama(){
		  $('#labelkolomutama').html('Penerbangan yang dipilih : '+kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+") | Dewasa: "+jumadt+" Anak: "+jumchd+" Bayi: "+juminf);

		}

function convertToRupiah(angka){
var rupiah = '';
var angkarev = angka.toString().split('').reverse().join('');
for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
return rupiah.split('',rupiah.length-1).reverse().join('');
}

$('#pilsubclasspergi').on('change', function() {
  hargapergi=this.value.split(",")[0];
  var totaladt=hargapergi*jumadt;
  var totalchd=hargapergi*jumchd;
  var totalinf=hargapergi*juminf;
  $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
  totalhargapergi=totaladt+totalchd+totalinf;
  $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(totalhargapergi));


});
$('#pilsubclasspulang').on('change', function() {
  hargapulang=this.value.split(",")[0];
  var totaladt=hargapulang*jumadt;
  var totalchd=hargapulang*jumchd;
  var totalinf=hargapulang*juminf;
  $('#formgo_selectedIDret').val(this.value.split(",")[1]);
  totalhargapulang=totaladt+totalchd+totalinf;
  $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(totalhargapulang));


});

		function pilihPergi(bahanid
		  ,daftranpergi
		  ,jampergiberangkat
		  ,jampergitiba
		  ,harga
		  ,ikon
		  ,stattransitpergi
		  ,dafsubclasspergi
		  ,kumpulaniddep
		  ){

        var totaladt=harga*jumadt;
        var totalchd=harga*jumchd;
        var totalinf=harga*juminf;
        harga=totaladt+totalchd+totalinf;

      		var subclasses=dafsubclasspergi.split('#');
          $('#formgo_selectedIDdep').val(kumpulaniddep);
          $('#pilsubclasspergi').find('option').remove();


            $.each(subclasses, function(key, value) {
         	var subclassesisi=value.split(',');
          if(subclassesisi[3]!=null){
     $('#pilsubclasspergi')
         .append($("<option></option>")
                    .attr("value",subclassesisi[2]+","+subclassesisi[3])
                    .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                  }
});


    $('#formgo_tgl_dep').val(jampergiberangkat);
    $('#formgo_tgl_dep_tiba').val(jampergitiba);
    $('#formgo_dafsubclasspergi').val(dafsubclasspergi);
    $('#formgo_stattransitpergi').val(stattransitpergi);
    $('#formgo_daftranpergi').val(daftranpergi);
		$('#formgo_acDep').val(ikon);
    $("#"+idpilper).removeClass("kotakpilihb cell-view");
    $("#"+idpilper).addClass("kotakpilih cell-view");

		$("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
    $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
idpilper=bahanid+"_gbr";

		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		$('#pilihanpergi').show('slow');
		//  alert(jampergitiba);
		        $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
		        $('#pilihanpergi_dafpes').html($('#labelkolomkiri').html()+' : '+daftranpergi+" ("+stattransitpergi+")");
		        $('#pilihanpergi_jampergi').html(jampergiberangkat);
		        $('#pilihanpergi_jamtiba').html(jampergitiba);
		        $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(harga));

            nilpil+=1;
            if(pilrencana2=="O"){
             mintaformgo();
           }else if(pilrencana2=="R"){
             if(nilpil=="2"){
             mintaformgo();
             }
           }

		}
		function pilihPulang(bahanid
		  ,daftranpulang
		  ,jampulangberangkat
		  ,jampulangtiba
		  ,harga
		  ,ikon
		  ,stattransitpulang
		  ,dafsubclasspulang
		  ,kumpulanidret
		  ){

                var totaladt=harga*jumadt;
                var totalchd=harga*jumchd;
                var totalinf=harga*juminf;
                harga=totaladt+totalchd+totalinf;
        var subclasses=dafsubclasspulang.split('#');
        $('#formgo_selectedIDret').val(kumpulanidret);
        $('#pilsubclasspulang').find('option').remove();


                    $.each(subclasses, function(key, value) {
                 	var subclassesisi=value.split(',');
                  if(subclassesisi[3]!=null){
             $('#pilsubclasspulang')
                 .append($("<option></option>")
                            .attr("value",subclassesisi[2]+","+subclassesisi[3])
                            .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                          }
        });

        $('#formgo_tgl_ret').val(jampulangberangkat);
        $('#formgo_tgl_ret_tiba').val(jampulangtiba);
        $('#formgo_dafsubclasspulang').val(dafsubclasspulang);
        $('#formgo_stattransitpulang').val(stattransitpulang);
        $('#formgo_daftranpulang').val(daftranpulang);
        $('#formgo_acRet').val(ikon);

        $("#"+idpilret).removeClass("kotakpilihb cell-view");
        $("#"+idpilret).addClass("kotakpilih cell-view");

    		$("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
        idpilret=bahanid+"_gbr";

		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		  $('#pilihanpulang').show('slow');
		//  alert(jampergitiba);
		        $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
		        $('#pilihanpulang_dafpes').html($('#labelkolomkanan').html()+' : '+daftranpulang+" ("+stattransitpulang+")");
		        $('#pilihanpulang_jampergi').html(jampulangberangkat);
		        $('#pilihanpulang_jamtiba').html(jampulangtiba);
		        $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(harga));
            nilpil+=1;
            if(pilrencana2=="R"){
              if(nilpil=="2"){
                mintaformgo();
              }
            }
		}
    function mintaformgo(){
    if(pilrencana2=="R"){
      $('#tomgoone').hide();
      $('#tomgoret').show('slow');
    }else if(pilrencana2=="O"){
      $('#tomgoret').hide();
  		$('#tomgoone').show('slow');
    }
    }

		function sembunyi(){
		      $('#loadmaskapai').hide('slow');
		}
		function cari(){
    nilpil=0;
    if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

    $('#tomgoone').hide();
    $('#tomgoret').hide();
		//  alert("Memulai pencarian");
		        //  $('#hasilkirim').html("<b>Hello world!</b>");
            if(pilrencana=="O"){
                $("#sisikanan").hide('slow');
                $("#sisikiri").show('slow');
                    $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                    $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
        		  }else{
                $("#sisikiri").show('slow');
                $("#sisikanan").show('slow');
                    $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                    $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                    $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                    $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
        		  }
		        urljadwalb=urljadwal;
		        urljadwalb+="org/"+banasal;
		        urljadwalb+="/des/"+bantujuan;
		        urljadwalb+="/flight/"+pilrencana;
		        urljadwalb+="/tglberangkat/"+tglberangkat;
		        urljadwalb+="/tglpulang/"+tglpulang;
		        urljadwalb+="/jumadt/"+jumadt;
		        urljadwalb+="/jumchd/"+jumchd;
		        urljadwalb+="/juminf/"+juminf;
		        //akhir urljadwalb+="ac/";
		                $('#loading_qz').show('fadeOut');
										$('#loading_qg').show('fadeOut');
										$('#loading_ga').show('fadeOut');
										$('#loading_kd').show('fadeOut');
										$('#loading_jt').show('fadeOut');
										$('#loading_sj').show('fadeOut');
										$('#loading_mv').show('fadeOut');
										$('#loading_il').show('fadeOut');

		                $('#dafpergi').html('');
										$('#dafpulang').html('');
		                urljadwalb+="/ac/";

		        ambildata_qz();
						ambildata_qg();
						ambildata_ga();
						ambildata_kd();
						ambildata_jt();
						ambildata_sj();
						ambildata_mv();
						ambildata_il();

            pilrencana2=pilrencana;
    $('#formatas').hide('slow');
		       $('#pilihanpergi').hide();
		        $('#pilihanpulang').hide();

          		      $('#labelkolomutama').hide('slow');
		      $('#labelpilihanpergi').hide('slow');
		      $('#tomulangipencarian').show('slow');


		      //  alert(urljadwalb);
		      setkolom();
        }else{
          $('#bahanmodal').dialog({ modal: true });
          // $( "#bahanmodal" ).show();
        //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
        }
		}
		var ajaxku_il;
		function ambildata_il(){
		  ajaxku_il = buatajax();
		  var url=urljadwalb+"IL";
		  ajaxku_il.onreadystatechange=stateChanged_il;
		  ajaxku_il.open("GET",url,true);
		  ajaxku_il.send(null);
		}

		 function stateChanged_il(){
		   var data;
		    if (ajaxku_il.readyState==4){
		      data=ajaxku_il.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_il').hide('slow');
		     }
		}

		var ajaxku_mv;
		function ambildata_mv(){
		  ajaxku_mv = buatajax();
		  var url=urljadwalb+"MV";
		  ajaxku_mv.onreadystatechange=stateChanged_mv;
		  ajaxku_mv.open("GET",url,true);
		  ajaxku_mv.send(null);
		}

		 function stateChanged_mv(){
		   var data;
		    if (ajaxku_mv.readyState==4){
		      data=ajaxku_mv.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_mv').hide('slow');
		     }
		}

		var ajaxku_sj;
		function ambildata_sj(){
		  ajaxku_sj = buatajax();
		  var url=urljadwalb+"SJ";
		  ajaxku_sj.onreadystatechange=stateChanged_sj;
		  ajaxku_sj.open("GET",url,true);
		  ajaxku_sj.send(null);
		}

		 function stateChanged_sj(){
		   var data;
		    if (ajaxku_sj.readyState==4){
		      data=ajaxku_sj.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_sj').hide('slow');
		     }
		}

		var ajaxku_jt;
		function ambildata_jt(){
		  ajaxku_jt = buatajax();
		  var url=urljadwalb+"JT";
		  ajaxku_jt.onreadystatechange=stateChanged_jt;
		  ajaxku_jt.open("GET",url,true);
		  ajaxku_jt.send(null);
		}

		 function stateChanged_jt(){
		   var data;
		    if (ajaxku_jt.readyState==4){
		      data=ajaxku_jt.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_jt').hide('slow');
		     }
		}

		var ajaxku_kd;
		function ambildata_kd(){
		  ajaxku_kd = buatajax();
		  var url=urljadwalb+"KD";
		  ajaxku_kd.onreadystatechange=stateChanged_kd;
		  ajaxku_kd.open("GET",url,true);
		  ajaxku_kd.send(null);
		}

		 function stateChanged_kd(){
		   var data;
		    if (ajaxku_kd.readyState==4){
		      data=ajaxku_kd.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_kd').hide('slow');
		     }
		}


		var ajaxku_ga;
		function ambildata_ga(){
		  ajaxku_ga = buatajax();
		  var url=urljadwalb+"GA";
		  ajaxku_ga.onreadystatechange=stateChanged_ga;
		  ajaxku_ga.open("GET",url,true);
		  ajaxku_ga.send(null);
		}

		 function stateChanged_ga(){
		   var data;
		    if (ajaxku_ga.readyState==4){
		      data=ajaxku_ga.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_ga').hide('slow');
		     }
		}

		var ajaxku_qz;
		function ambildata_qz(){
		  ajaxku_qz = buatajax();
		  var url=urljadwalb+"QZ";
		  ajaxku_qz.onreadystatechange=stateChanged_qz;
		  ajaxku_qz.open("GET",url,true);
		  ajaxku_qz.send(null);
		}
		 function stateChanged_qz(){
		   var data;
		    if (ajaxku_qz.readyState==4){
		      data=ajaxku_qz.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_qz').hide('slow');
		     }
		}


		var ajaxku_qg;
		function ambildata_qg(){
		  ajaxku_qg = buatajax();
		  var url=urljadwalb+"QG";
		  ajaxku_qg.onreadystatechange=stateChanged_qg;
		  ajaxku_qg.open("GET",url,true);
		  ajaxku_qg.send(null);
		}
		 function stateChanged_qg(){
		   var data;
		    if (ajaxku_qg.readyState==4){
		      data=ajaxku_qg.responseText;
		      if(data.length>0){
		        //document.getElementById("hasilkirim").html = data;

		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_qg').hide('slow');
		     }
		}

		function tambahData(data){

			                $('#dafpergi').html(data+$('#dafpergi').html());
											$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
							 			  $('.bagpulang').remove();
		}

		var ajaxku;
		function ambildata(){
		  ajaxku = buatajax();
		  var url="{{ url('/') }}/jadwalPesawat";
		  //url=url+"?q="+nip;
		  //url=url+"&sid="+Math.random();
		  ajaxku.onreadystatechange=stateChanged;
		  ajaxku.open("GET",url,true);
		  ajaxku.send(null);
		}
		function buatajax(){
		  if (window.XMLHttpRequest){
		    return new XMLHttpRequest();
		  }
		  if (window.ActiveXObject){
		     return new ActiveXObject("Microsoft.XMLHTTP");
		   }
		   return null;
		 }
		 function stateChanged(){
		   var data;
		    if (ajaxku.readyState==4){
		      data=ajaxku.responseText;
		      if(data.length>0){
		        //document.getElementById("hasilkirim").html = data;

						        $('#loading_qz').hide('show');
		                $('#hasilkirim').append(data);
		       }else{
		        // document.getElementById("hasilkirim").html = "";
		              //   $('#hasilkirim').html("");
		       }
		     }
		}

    @if($otomatiscari==1)
    banasal="{{$org}}";
    bantujuan="{{$des}}";
    pilrencana="{{$flight}}";
    tglberangkat="{{$tglberangkat}}";
    tglpulang="{{$tglpulang}}";
    jumadt="{{$jumadt}}";
    jumchd="{{$jumchd}}";
    juminf="{{$juminf}}";
    kotasal="{{$kotasal}}";
    kottujuan="{{$kottujuan}}";
    cari();
    @endif
		</script>
		@endsection

		@endsection
