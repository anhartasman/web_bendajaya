<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
@endsection
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent
<div class="inner-banner  ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">

			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
  				<li><a class="link-blue-2" href="{{$alamatbalik}}">penerbangan</a> /</li>
				<li><span class="color-red-3">formulir data diri</span></li>
			</ul>


		</div>
	</div>
</div>

<div class="detail-wrapper">
 <div class="container">
         <div class="row padd-90">
           <div class="col-xs-12 col-md-8">
       <form class="simple-from" method="POST" action="kirimdatadiri" id="formutama">
          <?php $jalurpergi="{\"jalurpergi\":["; ?>
          @foreach($dafdep as $dafdeparture)

                          @foreach($dafdeparture['Flights'] as $trans)
                          <?php $jalurpergi.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>
        @endforeach
       @endforeach
       <?php $jalurpergi.="]}";$jalurpergi=str_replace("},]}","}]}",$jalurpergi);  ?>
        @if($flight=="R")
     <?php $jalurpulang="{\"jalurpulang\":["; ?>
           @foreach($dafret as $dafreturn)

                       @foreach($dafreturn['Flights'] as $trans)
                         <?php $jalurpulang.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>

         @endforeach
         @endforeach
                  <?php $jalurpulang.="]}"; $jalurpulang=str_replace("},]}","}]}",$jalurpulang); ?>
   @endif
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <input type="hidden" id="totalamount" name="totalamount" value="{{Crypt::encrypt($totalamount)}}"  >
         <input type="hidden" id="angkaunik" name="angkaunik" value="{{Crypt::encrypt($angkaunik)}}"  >
         <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
         <input type="hidden" id="org" name="org" value="{{$org}}"  >
         <input type="hidden" id="des" name="des" value="{{$des}}" >
         <input type="hidden" id="acDep" name="acDep" value="{{$acDep}}"  >
         <input type="hidden" id="jalurpergi" name="jalurpergi" value="{{$jalurpergi}}"  >
         <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
         <input type="hidden" id="flight" name="flight" value="{{$flight}}"  >
         @if($flight=="R")
         <input type="hidden" id="acRet" name="acRet" value="{{$acRet}}"  >
         <input type="hidden" id="jalurpulang" name="jalurpulang" value="{{$jalurpulang}}"  >
         <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
         <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
     @endif
         <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
         <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
         <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
         <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >

     <input type="hidden" name="fields[code]" value="56345678safs_">
     <div class="justmobile">
     <h3 class="small-title">Informasi Perjalanan</h3>
     <div class="help-contact bg-grey-2">
       <h4 class="color-dark-2">{{$kotorg}} ke {{$kotdes}}</h4>
       @foreach($dafdep as $dafdeparture)

                        @foreach($dafdeparture['Flights'] as $trans)
     <p class="color-grey-2">{{$trans['FlightNo']}} : {{$trans['STD']}} - {{$trans['STA']}}</p>
     <p class="color-grey-2">Berangkat : {{$trans['ETD']}}</p>
     <p class="color-grey-2">Tiba : {{$trans['ETA']}}</p>
     @endforeach
     @endforeach
        @if($flight=="R")
      <h4 class="color-dark-2">{{$kotdes}} ke {{$kotorg}}</h4>

      <?php $jalurpulang="{\"jalurpulang\":["; ?>
         @foreach($dafret as $dafreturn)

                     @foreach($dafreturn['Flights'] as $trans)
                       <?php $jalurpulang.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>

       <p class="color-grey-2">{{$trans['FlightNo']}} : {{$trans['STD']}} - {{$trans['STA']}}</p>
       <p class="color-grey-2">Berangkat : {{$trans['ETD']}}</p>
       <p class="color-grey-2">Tiba : {{$trans['ETA']}}</p>
       @endforeach
       @endforeach
                <?php $jalurpulang.="]}"; $jalurpulang=str_replace("},]}","}]}",$jalurpulang); ?>

      @endif
     </div>
     </div>

     <div class="simple-group">
           <h3 class="small-title">Form Kontak</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="isiancpname" name="isiancpname" required="" placeholder="Nama">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Telepon</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="isiancptlp" name="isiancptlp" required="" placeholder="Nomor Telepon">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">E-mail</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="isiancpmail" name="isiancpmail" required="" placeholder="Email">
                 </div>
               </div>
             </div>

           </div>
         </div>
         <?php $jumpen=$adt+$chd+$inf;$jumadt=0; ?>
        @while ($jumadt<$adt)
           <?php $jumadt++; ?>
				<input type="hidden" name="isiantitadt_{{$jumadt}}" id="isiantitadt_{{$jumadt}}" value="MR"  >
         <div class="simple-group">
           <h3 class="small-title">Data diri dewasa #{{$jumadt}}</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Gelar</div>
								 <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="labeltitadt_{{$jumadt}}">MR</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#">MR</a>
											   <a href="#">MS</a>
												 <a href="#">MRS</a>
										 </span>
										</div>
								 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Depan</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="isianfnadt_{{$jumadt}}" id="isianfnadt_{{$jumadt}}" required="" placeholder="Nama Depan">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Belakang</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="isianlnadt_{{$jumadt}}" id="isianlnadt_{{$jumadt}}" required="" placeholder="Nama Belakang">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nomor Handphone</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" name="isianhpadt_{{$jumadt}}" id="isianhpadt_{{$jumadt}}" required="" placeholder="Nomor Handphone">
                 </div>
               </div>
             </div>

           </div>

         </div>
         @endwhile
         <?php $jumchd=0; ?>
         @while ($jumchd<$chd)
         <?php $jumchd++; ?>
				 <input type="hidden" name="isiantitchd_{{$jumchd}}" id="isiantitchd_{{$jumchd}}" value="MSTR"  >
          <div class="simple-group">
           <h3 class="small-title">Data diri anak #{{$jumchd}}</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Gelar</div>
								 <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="labeltitchd_{{$jumchd}}">MSTR</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#">MSTR</a>
											   <a href="#">MISS</a>
										 </span>
										</div>
								 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Depan</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="isianfnchd_{{$jumchd}}" id="isianfnchd_{{$jumchd}}" required="" placeholder="Nama Depan">
                </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Belakang</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="isianlnchd_{{$jumchd}}" id="isianlnchd_{{$jumchd}}" required="" placeholder="Nama Belakang">
               </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Tanggal lahir</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text"class="isianlahir"  name="isianbirthchd_{{$jumchd}}" id="isianbirthchd_{{$jumchd}}" required="" placeholder="Tanggal Lahir (DD-MM-YYYY)">
               </div>
               </div>
             </div>

           </div>

         </div>
         @endwhile
         <?php $juminf=0; ?>
         @while ($juminf<$inf)
        <?php $juminf++; ?>
				<input type="hidden" name="isiantitinf_{{$juminf}}" id="isiantitinf_{{$juminf}}" value="MSTR"  >
         <div class="simple-group">
          <h3 class="small-title">Data diri bayi #{{$juminf}}</h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Gelar</div>
								<div class="drop-wrap drop-wrap-s-4 color-5">
									<div class="drop">
									 <b id="labeltitinf_{{$juminf}}">MSTR</b>
										<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										<span>
												<a href="#">MSTR</a>
												<a href="#">MISS</a>
										</span>
									 </div>
								</div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Nama Depan</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text" name="isianfninf_{{$juminf}}" id="isianfninf_{{$juminf}}" required="" placeholder="Nama Depan">
                 </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Nama Belakang</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text" name="isianlninf_{{$juminf}}" id="isianlninf_{{$juminf}}" required="" placeholder="Nama Belakang">
              </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Tanggal lahir</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text"class="isianlahir"  name="isianbirthinf_{{$juminf}}" id="isianbirthinf_{{$juminf}}" required="" placeholder="Tanggal Lahir (DD-MM-YYYY)">
              </div>
              </div>
            </div>

          </div>

        </div>
         @endwhile
         <input type="submit" id="tombolsubmit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="confirm booking">
       </form>
           </div>
           <div class="col-xs-12 col-md-4 nomobile">
             <div class="right-sidebar">
               <div class="sidebar-text-label bg-dr-blue-2 color-white">informasi perjalanan</div>
               <div class="help-contact bg-grey-2">
                 <h4 class="color-dark-2">{{$kotorg}} ke {{$kotdes}}</h4>
                 @foreach($dafdep as $dafdeparture)

                                  @foreach($dafdeparture['Flights'] as $trans)
              <p class="color-grey-2">{{$trans['FlightNo']}} : {{$trans['STD']}} - {{$trans['STA']}}</p>
               <p class="color-grey-2">Berangkat : {{$trans['ETD']}}</p>
               <p class="color-grey-2">Tiba : {{$trans['ETA']}}</p>
               @endforeach
               @endforeach
                 @if($flight=="R")
                <h4 class="color-dark-2">{{$kotdes}} ke {{$kotorg}}</h4>

                <?php $jalurpulang="{\"jalurpulang\":["; ?>
                   @foreach($dafret as $dafreturn)

                               @foreach($dafreturn['Flights'] as $trans)
                                 <?php $jalurpulang.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>

                 <p class="color-grey-2">{{$trans['FlightNo']}} : {{$trans['STD']}} - {{$trans['STA']}}</p>
                 <p class="color-grey-2">Berangkat : {{$trans['ETD']}}</p>
                 <p class="color-grey-2">Tiba : {{$trans['ETA']}}</p>
                 @endforeach
                 @endforeach
                          <?php $jalurpulang.="]}"; $jalurpulang=str_replace("},]}","}]}",$jalurpulang); ?>

                @endif
               </div>




             </div>
           </div>
         </div>
 </div>
</div>
<!-- AKHIR FORM -->



  		</div>
  	</div>


				@section('akhirbody')
		<script type="text/javascript">
$( "#dialog-confirm" ).hide();
$('.isianlahir').datepicker({
  dateFormat: 'dd-mm-yy',
  yearRange: "-5:+0",
  changeYear: true}).val();

var sudahoke=0;
$('#formutama').on('submit', function() {

	<?php $jumadt=0; ?>
 @while ($jumadt<$adt)
 <?php $jumadt++; ?>
 $('#isiantitadt_{{$jumadt}}').val($('#labeltitadt_{{$jumadt}}').html());
 @endwhile


 <?php $jumchd=0; ?>
@while ($jumchd<$chd)
<?php $jumchd++; ?>
$('#isiantitchd_{{$jumchd}}').val($('#labeltitchd_{{$jumchd}}').html());
@endwhile

<?php $juminf=0; ?>
@while ($juminf<$inf)
<?php $juminf++; ?>
$('#isiantitinf_{{$juminf}}').val($('#labeltitinf_{{$juminf}}').html());
@endwhile


      dialogalert();
            return (sudahoke==1);
});


        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
