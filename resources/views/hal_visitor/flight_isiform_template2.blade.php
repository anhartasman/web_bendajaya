@extends('layouts.'.$namatemplate)
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Pengisian Data Penumpang</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{ url('/') }}">HOME</a></li>
            <li><a href="{{$alamatbalik}}">Penerbangan</a></li>
            <li class="active">Pengisian Data Penumpang</li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sms-6 col-sm-8 col-md-9">

                <div class="booking-section travelo-box">
                  <div class="tab-pane fade in active" id="flight-details">

                      <h2>{{$kotorg}} ke {{$kotdes}}</h2>
                      <div class="intro table-wrapper full-width hidden-table-sm box">
                          <div class="col-md-4 table-cell travelo-box">
                              <dl class="term-description">
                                  <dt>Maskapai:</dt><dd>{{maskapai($acDep)}}</dd>
                                  <?php $jampergiberangkat=date("d-m-Y H:i",strtotime($dafdep[0]['Flights'][0]['ETD']));?>
                                  <dt>Berangkat:</dt><dd>{{$jampergiberangkat}}</dd>
                                  <dt>Tiba:</dt><dd>{{$tgl_dep_tiba}}</dd>
                                  <dt>Penerbangan:</dt><dd>{{$stattransitpergi}}</dd>
                              </dl>
                          </div>
                          <div class="col-md-8 table-cell">
                              <div class="detailed-features booking-details">
                                  <div class="travelo-box">
                                      <?php $jumjalur=count($dafdep[0]['Flights']); $nom=0;?>
                                      <a href="#" class="button btn-mini yellow pull-right">{{$jumjalur-1}} STOP</a>
                                      <h4 class="box-title">Daftar jalur<small>{{$stattransitpergi}}</small></h4>
                                  </div>
                                  <div class="table-wrapper flights">
                                    @foreach($dafdep[0]['Flights'] as $trans)
                                    <?php $nom+=1; ?>
                                     <div class="table-row @if($nom<$jumjalur)first-flight @endif">
                                          <div class="table-cell logo">
                                              <img src="{{ URL::asset('img/ac/Airline-')}}{{$acDep}}.png" width="140" height="30" alt="">
                                              <label>{{$trans['FlightNo']}} ({{$trans['STD']}} - {{$trans['STA']}})</label>
                                          </div>
                                          <div class="table-cell timing-detail">
                                              <div class="timing">
                                                  <div class="check-in">
                                                      <label>Take off</label>
                                                      <span>{{date("d-m-Y H:i",strtotime($trans['ETD']))}}</span>
                                                  </div>
                                                  <div class="duration text-center">
                                                      <i class="soap-icon-clock"></i>
                                                      <span>{{lamajalan($trans['ETD'],$trans['ETA'],'%02d h, %02d m')}}</span>
                                                  </div>
                                                  <div class="check-out">
                                                      <label>landing</label>
                                                      <span>{{date("d-m-Y H:i",strtotime($trans['ETA']))}}</span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      @endforeach

                                  </div>
                              </div>
                          </div>
                      </div>

                      @if($flight=="R")
                      <h2>{{$kotdes}} ke {{$kotorg}}</h2>
                      <div class="intro table-wrapper full-width hidden-table-sm box">
                          <div class="col-md-4 table-cell travelo-box">
                              <dl class="term-description">
                                  <dt>Maskapai:</dt><dd>{{maskapai($acRet)}}</dd>
                                  <?php $jampulangberangkat=date("d-m-Y H:i",strtotime($dafret[0]['Flights'][0]['ETD']));?>
                                  <dt>Berangkat:</dt><dd>{{$jampulangberangkat}}</dd>
                                  <dt>Tiba:</dt><dd>{{$tgl_ret_tiba}}</dd>
                                  <dt>Penerbangan:</dt><dd>{{$stattransitpulang}}</dd>
                              </dl>
                          </div>
                          <div class="col-md-8 table-cell">
                              <div class="detailed-features booking-details">
                                  <div class="travelo-box">
                                      <?php $jumjalur=count($dafret[0]['Flights']); $nom=0;?>
                                      <a href="#" class="button btn-mini yellow pull-right">{{$jumjalur-1}} STOP</a>
                                      <h4 class="box-title">Daftar jalur<small>{{$stattransitpulang}}</small></h4>
                                  </div>
                                  <div class="table-wrapper flights">
                                    @foreach($dafret[0]['Flights'] as $trans)
                                    <?php $nom+=1; ?>
                                     <div class="table-row @if($nom<$jumjalur)first-flight @endif">
                                          <div class="table-cell logo">
                                              <img src="{{ URL::asset('img/ac/Airline-')}}{{$acRet}}.png" width="140" height="30" alt="">
                                              <label>{{$trans['FlightNo']}} ({{$trans['STD']}} - {{$trans['STA']}})</label>
                                          </div>
                                          <div class="table-cell timing-detail">
                                              <div class="timing">
                                                  <div class="check-in">
                                                      <label>Take off</label>
                                                      <span>{{date("d-m-Y H:i",strtotime($trans['ETD']))}}</span>
                                                  </div>
                                                  <div class="duration text-center">
                                                      <i class="soap-icon-clock"></i>
                                                      <span>{{lamajalan($trans['ETD'],$trans['ETA'],'%02d h, %02d m')}}</span>
                                                  </div>
                                                  <div class="check-out">
                                                      <label>landing</label>
                                                      <span>{{date("d-m-Y H:i",strtotime($trans['ETA']))}}</span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      @endforeach

                                  </div>
                              </div>
                          </div>
                      </div>
                      @endif

                    </div>
                    <form class="simple-from" method="POST" action="kirimdatadiri" id="formutama">
                       <?php $jalurpergi="{\"jalurpergi\":["; ?>
                       @foreach($dafdep as $dafdeparture)

                                       @foreach($dafdeparture['Flights'] as $trans)
                                       <?php $jalurpergi.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>
                     @endforeach
                    @endforeach
                    <?php $jalurpergi.="]}";$jalurpergi=str_replace("},]}","}]}",$jalurpergi);  ?>
                     @if($flight=="R")
                  <?php $jalurpulang="{\"jalurpulang\":["; ?>
                        @foreach($dafret as $dafreturn)

                                    @foreach($dafreturn['Flights'] as $trans)
                                      <?php $jalurpulang.="{\"FlightNo\":"."\"".$trans['FlightNo']."\"".","."\"STD\":"."\"".$trans['STD']."\"".","."\"ETD\":"."\"".$trans['ETD']."\"".","."\"STA\":"."\"".$trans['STA']."\"".","."\"ETA\":"."\"".$trans['ETA']."\""."},"; ?>

                      @endforeach
                      @endforeach
                               <?php $jalurpulang.="]}"; $jalurpulang=str_replace("},]}","}]}",$jalurpulang); ?>
                @endif
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" id="totalamount" name="totalamount" value="{{Crypt::encrypt($totalamount)}}"  >
                      <input type="hidden" id="angkaunik" name="angkaunik" value="{{Crypt::encrypt($angkaunik)}}"  >
                      <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
                      <input type="hidden" id="org" name="org" value="{{$org}}"  >
                      <input type="hidden" id="des" name="des" value="{{$des}}" >
                      <input type="hidden" id="acDep" name="acDep" value="{{$acDep}}"  >
                      <input type="hidden" id="jalurpergi" name="jalurpergi" value="{{$jalurpergi}}"  >
                      <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
                      <input type="hidden" id="flight" name="flight" value="{{$flight}}"  >
                      @if($flight=="R")
                      <input type="hidden" id="acRet" name="acRet" value="{{$acRet}}"  >
                      <input type="hidden" id="jalurpulang" name="jalurpulang" value="{{$jalurpulang}}"  >
                      <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
                      <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
                      @endif
                      <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
                      <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
                      <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
                      <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >

                      <div class="person-information">
                          <h2>Form Kontak</h2>
                          <div class="form-group row">
                              <div class="col-sm-6 col-md-5">
                                  <label>Nama</label>
                                  <input type="text" id="isiancpname" name="isiancpname" required=""  class="input-text full-width" value="" placeholder="" />
                              </div>
                              <div class="col-sm-6 col-md-5">
                                  <label>Telepon</label>
                                  <input type="text" id="isiancptlp" name="isiancptlp" required="" class="input-text full-width" value="" placeholder="" />
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col-sm-6 col-md-5">
                                  <label>E-mail Address</label>
                                  <input type="email"  id="isiancpmail" name="isiancpmail" required="" class="input-text full-width" value="" placeholder="" />
                              </div>
                          </div>

                      </div>
                      <hr />

                      <?php $jumpen=$adt+$chd+$inf;$jumadt=0; ?>
                      @while ($jumadt<$adt)
                        <?php $jumadt++; ?>
                        <div class="card-information">
                            <h2>Data diri dewasa #{{$jumadt}}</h2>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Panggilan</label>
                                    <div class="selector">
                                        <select name="isiantitadt_{{$jumadt}}" id="isiantitadt_{{$jumadt}}" style="width:100%;" class="full-width">
                                          <option value="MR">MR</option>
                                          <option value="MS">MS</option>
                                          <option value="MRS">MRS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Depan</label>
                                    <input type="text"name="isianfnadt_{{$jumadt}}" id="isianfnadt_{{$jumadt}}" required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="isianlnadt_{{$jumadt}}" id="isianlnadt_{{$jumadt}}" required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nomor Handphone</label>
                                    <input type="text" name="isianhpadt_{{$jumadt}}" id="isianhpadt_{{$jumadt}}" required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>

                        </div>
                        <hr />
                        @endwhile
                        <?php $jumchd=0; ?>
                        @while ($jumchd<$chd)
                        <?php $jumchd++; ?>
                          <div class="card-information">
                              <h2>Data diri anak #{{$jumchd}}</h2>
                              <div class="form-group row">
                                  <div class="col-sm-6 col-md-5">
                                      <label>Panggilan</label>
                                      <div class="selector">
                                          <select name="isiantitchd_{{$jumchd}}" id="isiantitchd_{{$jumchd}}" style="width:100%;" class="full-width">
                                            <option value="MSTR">MSTR</option>
                                            <option value="MISS">MISS</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-sm-6 col-md-5">
                                      <label>Nama Depan</label>
                                      <input type="text" name="isianfnchd_{{$jumchd}}" id="isianfnchd_{{$jumchd}}"  required="" class="input-text full-width" value="" placeholder="" />
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <div class="col-sm-6 col-md-5">
                                      <label>Nama Belakang</label>
                                      <input type="text" name="isianlnchd_{{$jumchd}}" id="isianlnchd_{{$jumchd}}" required="" class="input-text full-width" value="" placeholder="" />
                                  </div>
                                  <div class="col-sm-6 col-md-5">
                                    <label>Tanggal lahir</label>
                                    <input type="text" name="isianbirthchd_{{$jumchd}}" id="isianbirthchd_{{$jumchd}}" required=""  class="input-text full-width isianlahir" value="" placeholder="" />
                                  </div>
                              </div>

                          </div>
                          <hr />
                          @endwhile
                          <?php $juminf=0; ?>
                          @while ($juminf<$inf)
                            <?php $juminf++; ?>
                            <div class="card-information">
                                <h2>Data diri bayi #{{$juminf}}</h2>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <label>Gelar</label>
                                        <div class="selector">
                                            <select name="isiantitinf_{{$juminf}}" id="isiantitinf_{{$juminf}}" style="width:100%;" class="full-width">
                                              <option value="MSTR">MSTR</option>
                                              <option value="MISS">MISS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-5">
                                        <label>Nama Depan</label>
                                        <input type="text" name="isianfninf_{{$juminf}}" id="isianfninf_{{$juminf}}"  required="" class="input-text full-width" value="" placeholder="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 col-md-5">
                                        <label>Nama Belakang</label>
                                        <input type="text" name="isianlninf_{{$juminf}}" id="isianlninf_{{$juminf}}" required="" class="input-text full-width" value="" placeholder="" />
                                    </div>
                                    <div class="col-sm-6 col-md-5">
                                        <label>Tanggal lahir</label>
                                        <input type="text" name="isianbirthinf_{{$juminf}}" id="isianbirthinf_{{$juminf}}" required="" class="input-text full-width isianlahir" value="" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            @endwhile

                        <div class="form-group row">
                            <div class="col-sm-6 col-md-5">
                                <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="sidebar col-sms-6 col-sm-4 col-md-3">
                <div class="booking-details travelo-box">
                    <h4>Booking Details</h4>
                    <article class="flight-booking-details">
                        <figure class="clearfix">
                            <a title="" href="flight-detailed.html" class="middle-block"><img class="middle-item" alt="" src="{{url('/')}}/gambarac/{{$acDep}}/w/75/h/75"></a>
                            <div class="travel-title">
                                <h5 class="box-title">{{$kotorg}} ke {{$kotdes}}<small>{{$stattransitpergi}}</small></h5>
                            </div>
                        </figure>
                        <div class="details">
                            <div class="constant-column-3 timing clearfix">
                                <div class="check-in">
                                    <label>Berangkat</label>
                                    <span>{{$jampergiberangkat}}</span>
                                </div>
                                <div class="duration text-center">
                                    <i class="soap-icon-clock"></i>
                                    <span>{{lamajalan($jampergiberangkat,$tgl_dep_tiba,"%02d h, %02d m")}}</span>
                                </div>
                                <div class="check-out">
                                    <label>Sampai</label>
                                    <span>{{$tgl_dep_tiba}}</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    @if($flight=="R")
                    <article class="flight-booking-details">
                        <figure class="clearfix">
                            <a title="" href="flight-detailed.html" class="middle-block"><img class="middle-item" alt="" src="{{url('/')}}/gambarac/{{$acRet}}/w/75/h/75"></a>
                            <div class="travel-title">
                                <h5 class="box-title">{{$kotdes}} ke {{$kotorg}}<small>{{$stattransitpulang}}</small></h5>
                            </div>
                        </figure>
                        <div class="details">
                            <div class="constant-column-3 timing clearfix">
                                <div class="check-in">
                                    <label>Berangkat</label>
                                    <span>{{$jampulangberangkat}}</span>
                                </div>
                                <div class="duration text-center">
                                    <i class="soap-icon-clock"></i>
                                    <span>{{lamajalan($jampulangberangkat,$tgl_ret_tiba,"%02d h, %02d m")}}</span>
                                </div>
                                <div class="check-out">
                                    <label>Sampai</label>
                                    <span>{{$tgl_ret_tiba}}</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endif

                    <dl class="other-details">
                        <dt class="total-price">Total Biaya</dt><dd class="total-price-value">{{rupiah($totalamount)}}</dd>
                    </dl>
                </div>

                <div class="travelo-box contact-box">
                  <h4>Butuh bantuan?</h4>
                  <p>Costumer Service kami siap melayani</p>
                  <address class="contact-details">
                      <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                      <br>
                      <a class="contact-email" href="#">{{$infowebsite['email']}}</a>
                  </address>
                </div>
            </div>
        </div>
    </div>
</section>


		@section('akhirbody')
    <script type="text/javascript">
$( "#dialog-confirm" ).hide();
    $('.isianlahir').datepicker({
      dateFormat: 'dd-mm-yy',
      yearRange: "-5:+0",
      changeYear: true}).val();
    $( "#tombolsubmit" ).click(function() {
    //  alert( "Handler for .click() called." );
      $('#isiancpname').val($('#cpname').val());
      $('#coba').val($('#cptlp').val());
      $('#isiancptlp').val($('#cptlp').val());
      $('#isiancpmail').val($('#cpmail').val());


  $( "#formutama" ).submit();
    });


    var sudahoke=0;
    $('#formutama').on('submit', function() {


          dialogalert();
                return (sudahoke==1);
    });



        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
