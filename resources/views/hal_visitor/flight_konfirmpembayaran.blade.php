@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
		<script type="text/javascript" src="{{ URL::asset('asettemplate1/js/jquery.maskMoney.min.js')}}"></script>
		<link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<!-- INNER-BANNER -->
<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/flight">tiket pesawat</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">{{$kotorg}} ke {{$kotdes}}</h2>
  				</div>
			</div>
		</div>
	</div>

</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
					<?php $otomatiscari=0;?>
					<div class="col-xs-12 col-sm-4 col-md-3 nomobile">
		        <div class="sidebar style-2 clearfix">
		        <div class="sidebar-block">
		          <h4 class="sidebar-title color-dark-2">search</h4>
							<div class="search-inputs">
		            <div class="form-block clearfix">
		              <h5>Asal</h5><div class="input-style b-50 color-3">
		              <select class="input-style selektwo"placeholder="Check In"  name="asal" id="pilasal">
		                   <option value=""></option>
		                   @foreach($dafban as $area)
		                  <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $org==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
		                  @endforeach
		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Tujuan</h5>
		                <div class="input-style">
		                <select class="input-style selektwo" name="tujuan" id="piltujuan">
		                   <option value=""></option>
		                   @foreach($dafban as $area)
		                  <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $des==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
		                  @endforeach
		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Rencana</h5>
		                <div class="input-style">
		                <select class="mainselection" id="pilrencana" name="pilrencana" style="">
		                  <option value="O" <?php if($otomatiscari==1 && $flight=="O"){print("selected=\"selected\"");} ?>>Sekali Jalan</option>
		                  <option value="R" <?php if($otomatiscari==1 && $flight=="R"){print("selected=\"selected\"");} ?>>Pulang Pergi</option>

		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Tanggal Berangkat</h5>
		                <div class="input-style">
		                  <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglberangkat)){print($tglberangkat);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div id="tny_ret" class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Tanggal Pulang</h5>
		                <div class="input-style">
		                  <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglpulang)){print($tglpulang);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Dewasa</h5>
		                <div class="input-style">
		                  <select class="mainselection" id="adt" name="adt" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=1; $op<=7;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select>
		                </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Anak</h5>
		                <div class="input-style">
		                  <select class="mainselection"id="chd" name="chd" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=0; $op<=6;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Bayi</h5>
		                <div class="input-style">
		                  <select class="mainselection"id="inf" name="inf" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=0; $op<=6;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>
		          </div>
		          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
		        </div>


		        </div>
		      </div>
       		<div class="col-xs-12 col-md-9">
						<?php
						  $dafdep = json_decode($daftranpergi, true);
 				    ?>
						<div class="detail-content-block">
							<h3 class="small-title">Penerbangan dari {{$kotorg}} ke {{$kotdes}}</h3>
							<div class="table-responsive">
									<table class="table style-1 type-2 striped">
											<tbody>
												 @foreach($dafdep['jalurpergi'] as $trans)
												<tr>
														<td class="table-label color-black">{{$trans['FlightNo']}}</td>
														<td class="table-label color-dark-2"><strong>{{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})</strong></td>
												</tr>
												@endforeach
											</tbody>
									</table>
								</div>
						 </div>

						 @if($flight=="R")
						 <?php
  						$dafret = json_decode($daftranpulang, true);
              ?>
						 <div class="detail-content-block">
 							<h3 class="small-title">Penerbangan dari {{$kotdes}} ke {{$kotorg}}</h3>
 							<div class="table-responsive">
 									<table class="table style-1 type-2 striped">
 											<tbody>
 												 @foreach($dafret['jalurpulang'] as $trans)
 												<tr>
 														<td class="table-label color-black">{{$trans['FlightNo']}}</td>
 														<td class="table-label color-dark-2"><strong>{{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})</strong></td>
 												</tr>
 												@endforeach
 											</tbody>
 									</table>
 								</div>
 						 </div>
						 @endif

<div >
					@if($adt>0)
					 <?php $nomurut=0; foreach($dafpendewasa as $pen){ $nomurut+=1;?>
				<div class="detail-content-block">
					<h3 class="small-title">Dewasa #{{$nomurut}}</h3>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
						        <tr>
						          	<td class="table-label color-black">Gelar</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->tit}}</strong></td>
						        </tr>
										<tr>
						          	<td class="table-label color-black">Nama</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->fn}} {{$pen->ln}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-black">Handphone:</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->hp}}</strong></td>
						        </tr>

					      	</tbody>
					    </table>
				    </div>
				</div>
				<?php }?>
				@endif

				@if($chd>0)
				 <?php $nomurut=0; foreach($dafpenanak as $dataanak){ $nomurut+=1;?>
			<div class="detail-content-block">
				<h3 class="small-title">Anak #{{$nomurut}}</h3>
				<div class="table-responsive">
						<table class="table style-1 type-2 striped">
								<tbody>
									<tr>
											<td class="table-label color-black">Gelar</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->tit}}</strong></td>
									</tr>
									<tr>
											<td class="table-label color-black">Nama</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->fn}} {{$dataanak->ln}}</strong></td>
									</tr>
									<tr>
											<td class="table-label color-black">Lahir:</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->birth}}</strong></td>
									</tr>

								</tbody>
						</table>
					</div>
			</div>
			<?php }?>
			@endif

			@if($inf>0)
			 <?php $nomurut=0; foreach($dafpenbayi as $databayi){ $nomurut+=1;?>
		<div class="detail-content-block">
			<h3 class="small-title">Bayi #{{$nomurut}}</h3>
			<div class="table-responsive">
					<table class="table style-1 type-2 striped">
							<tbody>
								<tr>
										<td class="table-label color-black">Gelar</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->tit}}</strong></td>
								</tr>
								<tr>
										<td class="table-label color-black">Nama</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->fn}} {{$databayi->ln}}</strong></td>
								</tr>
								<tr>
										<td class="table-label color-black">Lahir:</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->birth}}</strong></td>
								</tr>

							</tbody>
					</table>
				</div>
		</div>
		<?php }?>
		@endif

				<div class="detail-content-block">
					<h3 class="small-title">Informasi Tagihan</h3>
					<div class="table-responsive">
							<table class="table style-1 type-2 striped">
									<tbody>
										<tr>
												<td class="table-label color-black">No. Transaksi:</td>
												<td class="table-label color-dark-2"><strong>{{$notrx}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$labelbiaya}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Batas pembayaran:</td>
												<td class="table-label color-dark-2"><strong>{{DateToIndo($tgl_bill_exp)}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Status tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$statustransaksi}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan untuk:</td>
												<td class="table-label color-dark-2"><strong>{{$cpname}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">EMAIL:</td>
												<td class="table-label color-dark-2"><strong>{{$cpmail}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">NOMOR TELEPHONE:</td>
												<td class="table-label color-dark-2"><strong>{{$cptlp}}</strong></td>
										</tr>

									</tbody>
							</table>
						</div>
				</div>

</div>
@if($status==0 || $status==1 || $status==3)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Upload bukti pembayaran</h3>
						<p class="color-grey">Gunakan link dibawah ini untuk memudahkan konfirmasi pembayaran</p>
						<form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="exampleInputEmail1">Jumlah Bayar</label>
								<input name="jumlahbayar" id="jumlahbayar" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Tanggal Bayar</label>
								<input name="tanggalbayar" id="tanggalbayar"  type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Dari Bank</label>
								<input name="bank" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nomor Rekening</label>
								<input name="rekening" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Atas Nama</label>
								<input name="napem" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ke Rekening</label>
								<select class="form-control" name="rektuju" id="rektuju">
									 @foreach($dafrek as $rek)
									<option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
									@endforeach
								</select>
							</div>
						<div class="form-group">
							<label for="exampleInputFile">Gambar Bukti Transfer</label>
							<input name="foto" type="file" id="exampleInputFile">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
						<div class="custom-panel bg-grey-2 radius-4">
				      @foreach($dafbuk as $bukti)
					    <img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Bukti Pembayaran" style="width:200px;height:200px;">
					    <br>
							@endforeach
						</div>
					</div>
				</div>
@endif

       		</div>

       	</div>
	</div>
</div>

@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
	<script type="text/javascript">


	    <!--
	    $('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
	   }).val();
	   -->

	    //variabel

			var hargapulang="0";
			var hargapergi="0";
	    var nilpil=0;
	    var idpilper="";
	    var idpilret="";
			var banasal="CGK";
			var kotasal="Jakarta";
			var labelasal="";
			var kottujuan="Manokwari";
			var bantujuan="MKW";
			var labeltujuan="";
			var pilrencana="O";
			var pilrencana2="O";
			var tglber_d="";
			var tglber_m="";
			var tglber_y="";

			var tglpul_d="";
			var tglpul_m="";
			var tglpul_y="";

			var tglberangkat="<?php echo date("Y-m-d");?>";
	    $('#formgo_tgl_dep').val(tglberangkat);
	    $('#formgo_tgl_deppilihan').val(tglberangkat);
			var tglpulang="<?php echo date("Y-m-d");?>";
	    $('#formgo_tgl_ret').val(tglpulang);
	    $('#formgo_tgl_retpilihan').val(tglpulang);

			var jumadt="1";
			var jumchd="0";
			var juminf="0";

			var urljadwal="{{ url('/') }}/flight/otomatiscari/";
			var urljadwalb=urljadwal;

	    var dafHTMLPergi = [];
	    var dafHTMLPulang = [];

			$('#tomgoret').hide();

	    $( "#tompilulang" ).click(function() {
	      nilpil=0;
	    });
	    $( "#tomgoone" ).click(function() {
	      $( "#formgo" ).submit();
	    });
	    $( "#tomgoret" ).click(function() {
	      $( "#formgo" ).submit();
	    });
	    $( "#tomteskir" ).click(function() {
	      $( "#formgo" ).submit();
	    });

			$('#adt').on('change', function() {
			jumadt=this.value;
	    $('#formgo_adt').val(jumadt);

			});
			$('#chd').on('change', function() {
			jumchd=this.value;
	    $('#formgo_chd').val(jumchd);

			});
			$('#inf').on('change', function() {
			juminf=this.value;
	    $('#formgo_inf').val(juminf);

			});
			  $('.selektwo').select2();
	      <?php if(($otomatiscari==1 && $flight=="O")||$otomatiscari==0){?>
			  $('#tny_ret').hide();
	      <?php }?>

			$('#tglberangkat').datepicker({ dateFormat: 'dd-mm-yy' }).val();
			$('#tglpulang').datepicker({ dateFormat: 'dd-mm-yy' }).val();

			$('#tglberangkat').on('change', function() {

			var values=this.value.split('-');
			//alert(this.value);
			tglber_d=values[0];
			tglber_m=values[1];
			tglber_y=values[2];

			tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

	    $('#formgo_tgl_deppilihan').val(tglberangkat);

			});

			$('#tglpulang').on('change', function() {

			var values=this.value.split('-');
			//alert(this.value);
			tglpul_d=values[0];
			tglpul_m=values[1];
			tglpul_y=values[2];

			tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
	    $('#formgo_tgl_retpilihan').val(tglpulang);

			});

			$('#pilasal').on('change', function() {
			  var values=this.value.split('-');
			  var kotas=values[1];
			  kotasal=values[0];
			  //alert(kotas);
			  banasal=kotas;
			  labelasal=this.value;

			});
			$('#piltujuan').on('change', function() {
			  var values=this.value.split('-');
			  var kottuj=values[1];
			  kottujuan=values[0];
			  //alert(kottuj);
			  bantujuan=kottuj;
			  labeltujuan=this.value;

			});
			$('#pilrencana').on('change', function() {
			  //alert( this.value );
			  pilrencana=this.value;
	      $('#formgo_flight').val(pilrencana);
			  var tny_ret = document.getElementById("tny_ret").value;

			if(this.value=="O"){
			    $('#tny_ret').hide();
	     }else{
			    $('#tny_ret').show();
	     }


			});


	function convertToRupiah(angka){
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return rupiah.split('',rupiah.length-1).reverse().join('');
	}

			function cari(){
	    nilpil=0;
	    if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

			        urljadwalb=urljadwal;
			        urljadwalb+=banasal;
			        urljadwalb+="/"+bantujuan;
			        urljadwalb+="/"+pilrencana;
			        urljadwalb+="/"+tglberangkat;
			        urljadwalb+="/"+tglpulang;
			        urljadwalb+="/"+jumadt;
			        urljadwalb+="/"+jumchd;
			        urljadwalb+="/"+juminf;
							//alert(urljadwalb);
							$(location).attr('href', urljadwalb)
	        }else{
	          $('#bahanmodal').dialog({ modal: true });
	        }
			}


</script>
@endsection

@endsection
