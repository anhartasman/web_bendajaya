@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
		<script type="text/javascript" src="{{ URL::asset('asettemplate1/js/jquery.maskMoney.min.js')}}"></script>
@endsection
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/flight">penerbangan</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">thank you</h2>
  				</div>
			</div>
		</div>
	</div>

</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">

				<div class="detail-content-block">
					<h3 class="small-title">Informasi Tagihan</h3>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
						        <tr>
						          	<td class="table-label color-grey">NOMOR TRANSAKSI</td>
						          	<td class="table-label color-dark-2"><strong>{{$notrx}}</strong></td>
						        </tr>
										<tr>
						          	<td class="table-label color-grey">TOTAL TAGIHAN</td>
						          	<td class="table-label color-dark-2"><strong>{{$labelbiaya}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-grey">NAMA:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cpname}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-grey">EMAIL:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cpmail}}</strong></td>
					        	</tr>
						        <tr>
						          	<td class="table-label color-grey">NOMOR TELEPHONE:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cptlp}}</strong></td>
					        	</tr>

					      	</tbody>
					    </table>
				    </div>
				</div>

				@if($adt>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang dewasa</h3>

						 <?php foreach($dafpendewasa as $pen){?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$pen->tit}} {{$pen->fn}} {{$pen->ln}}  ({{$pen->hp}})
						</div>
						<?php }?>

					</div>
				</div>
				@endif

				@if($chd>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang anak </h3>
				   <?php foreach($dafpenanak as $dataanak){?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$dataanak->tit}} {{$dataanak->fn}} {{$dataanak->ln}}  ({{$dataanak->birth}})
						</div>

						<?php }?>

					</div>
				</div>
				@endif

				@if($inf>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang bayi</h3>
				   <?php foreach($dafpenbayi as $databayi){?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$databayi->tit}} {{$databayi->fn}} {{$databayi->ln}}  ({{$databayi->birth}})
					</div>

					<?php }?>
					</div>
				</div>
				@endif

@if($status==0 || $status==1 || $status==3)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Upload bukti pembayaran</h3>
						<p class="color-grey">Gunakan link dibawah ini untuk memudahkan konfirmasi pembayaran</p>
						<form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="exampleInputEmail1">Jumlah Bayar</label>
								<input name="jumlahbayar" id="jumlahbayar" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Tanggal Bayar</label>
								<input name="tanggalbayar" id="tanggalbayar"  type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Dari Bank</label>
								<input name="bank" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nomor Rekening</label>
								<input name="rekening" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Atas Nama</label>
								<input name="napem" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ke Rekening</label>
								<select class="form-control" name="rektuju" id="rektuju">
									 @foreach($dafrek as $rek)
									<option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
									@endforeach
								</select>
							</div>
						<div class="form-group">
							<label for="exampleInputFile">Gambar Bukti Transfer</label>
							<input name="foto" type="file" id="exampleInputFile">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
						<div class="custom-panel bg-grey-2 radius-4">
				      @foreach($dafbuk as $bukti)
					    <img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Bukti Pembayaran" style="width:200px;height:200px;">
					    <br>
							@endforeach
						</div>
					</div>
				</div>
@endif

       		</div>
       		<div class="col-xs-12 col-md-4">
       		<div class="right-sidebar">
				 <!-- SIDE KANAN -->
					<div class="sidebar-text-label bg-dr-blue-2 color-white">informasi perjalanan</div>
					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">{{$org}} - {{$des}}</h4>
						<?php
						                  $dafdep = json_decode($daftranpergi, true);

											        //$dafdep=$hasildecode['jalurpergi']['departure'];

 				   ?>
					 @foreach($dafdep['jalurpergi'] as $trans)
				<p class="color-grey-2"> {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})</p>
					 @endforeach

					 @if($flight=="R")
 						<h4 class="color-dark-2">{{$des}} - {{$org}}</h4>
 						<?php
 						                  $dafret = json_decode($daftranpulang, true);

  				   ?>
 					 @foreach($dafret['jalurpulang'] as $trans)
 				<p class="color-grey-2"> {{$trans['FlightNo']}} : {{$trans['STD']}} ({{$trans['ETD']}}) - {{$trans['STA']}} ({{$trans['ETA']}})</p>
 					 @endforeach
					 @endif
        	</div>
					<div  style="padding-top:10%" ></div>
					<div class="sidebar-text-label bg-dr-blue-2 color-white">Batas pembayaran</div>
					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">{{$tgl_bill_exp}}</h4>
						@if($status==0 && ($tgl_bill_exp>date('Y-m-d H:i:s')))
						<p class="color-grey-2"> <span id="time">{{$sisawaktub}}</span></p>
					  @endif
					</div>
					<div  style="padding-top:10%" ></div>
					@if($status==2)
					<div class="sidebar-text-label bg-dr-blue-2 color-white">Tanggal terima</div>
					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">{{$tgl_bill_acc}}</h4>
						@if($status==0)
						<p class="color-grey-2"> <span id="time">{{$sisawaktub}}</span></p>
					  @endif
					</div>
					@endif
					<div  style="padding-top:10%" ></div>
					<!--
					<div class="sidebar-text-label bg-dr-blue-2 color-white">informasi keberangkatan</div>
					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">Transit</h4>
						<p class="color-grey-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a class="help-phone color-dark-2 link-dr-blue-2" href="tel:0200059600"><img src="img/detail/phone24-dark-2.png" alt="">020 00 59 600</a>
						<a class="help-mail color-dark-2 link-dr-blue-2" href="mailto:let’s_travel@world.com"><img src="img/detail/letter-dark-2.png" alt="">let’s_travel@world.com</a>
					</div>
				-->

       			</div>
       		</div>
       	</div>
	</div>
</div>

@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
	<script type="text/javascript">
		$('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
   }).val();
	 function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        hours = parseInt((timer /3600)%24, 10);
        minutes = parseInt((timer / 60)%60, 10)
        seconds = parseInt(timer % 60, 10);

				hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours+":"+minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 5;
        display = document.querySelector('#time');
  startTimer({{$sisawaktu}}, display);
};

$(document).ready(function(){
	$('#jumlahbayar').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
});
</script>
@endsection

@endsection
