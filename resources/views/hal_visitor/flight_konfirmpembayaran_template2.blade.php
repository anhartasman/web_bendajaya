@extends('layouts.'.$namatemplate)
@section('kontenweb')
<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Detail Transaksi</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{ url('/') }}">HOME</a></li>
						<li><a href="{{ url('/') }}/cekpesanan">Cek Pesanan</a></li>
						<li class="active">Detail</li>
				</ul>
		</div>
</div>
<section id="content" class="gray-area">
		<div class="container">
				<div class="row">
						<div id="main" class="col-sm-8 col-md-9">
							@if (session()->has('notiftagihan'))
							@if(isset(session('notiftagihan')['created']))
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Transaksi berhasil dibuat!</strong> Silahkan upload bukti pembayaran setelah Anda melakukan pembayaran. Terimakasih
							</div>
							@endif
							@if(isset(session('notiftagihan')['uploaded']))
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Upload bukti pembayaran berhasil!</strong> Silahkan tunggu email pemberitahuan dari kami. Terimakasih
							</div>
							@endif
							@endif
							<?php
							  $dafdep = json_decode($daftranpergi, true);
	 				    ?>
								<div class="booking-information travelo-box">
										<h2>Trx No. {{$notrx}}</h2>
										<hr/>
										<h2>Penerbangan dari {{$kotorg}} ke {{$kotdes}}</h2>
										<dl class="term-description">
											<?php $nompes=0;?>
 											@foreach($dafdep['jalurpergi'] as $trans)
											<?php $nompes+=1;?>
 												 <dt>Pesawat @if($flight=="R") #{{$nompes}} @endif</dt><dd>{{$trans['FlightNo']}}</dd>
 												 <dt>Dari</dt><dd>{{$trans['STD']}}</dd>
												 <dt>Menuju</dt><dd>{{$trans['STA']}}</dd>
												 <dt>Berangkat</dt><dd>{{$trans['ETD']}}</dd>
												 <dt>Sampai</dt><dd>{{$trans['ETA']}}</dd>
												 <dt></dt><dd></dd>
 										 @endforeach

										</dl>
										<hr />
										@if($flight=="R")
			 						 <?php
			   						$dafret = json_decode($daftranpulang, true);
			               ?>
										 <h2>Penerbangan dari {{$kotdes}} ke {{$kotorg}}</h2>
 										<dl class="term-description">
											<?php $nompes=0;?>
 											@foreach($dafret['jalurpulang'] as $trans)
											<?php $nompes+=1;?>
 												 <dt>Pesawat @if($flight=="R") #{{$nompes}} @endif</dt><dd>{{$trans['FlightNo']}}</dd>
 												 <dt>Dari</dt><dd>{{$trans['STD']}}</dd>
												 <dt>Menuju</dt><dd>{{$trans['STA']}}</dd>
												 <dt>Berangkat</dt><dd>{{$trans['ETD']}}</dd>
												 <dt>Sampai</dt><dd>{{$trans['ETA']}}</dd>
												 <dt></dt><dd></dd>
 										 @endforeach

 										</dl>
 										<hr />
										 @endif

											<?php $nomurut=0; foreach($dafpendewasa as $pen){ $nomurut+=1;?>
										<h2>Dewasa #{{$nomurut}}</h2>
										<dl class="term-description">
												<dt>Gelar:</dt><dd>{{$pen->tit}}</dd>
												<dt>Nama:</dt><dd>{{$pen->fn}} {{$pen->ln}}</dd>
												<dt>Handphone:</dt><dd>{{$pen->hp}}</dd>
										</dl>
										<hr />
										<?php } ?>
										<?php $nomurut=0; foreach($dafpenanak as $pen){ $nomurut+=1;?>
									<h2>Anak #{{$nomurut}}</h2>
									<dl class="term-description">
											<dt>Gelar:</dt><dd>{{$pen->tit}}</dd>
											<dt>Nama:</dt><dd>{{$pen->fn}} {{$pen->ln}}</dd>
											<dt>Lahir:</dt><dd>{{$pen->birth}}</dd>
									</dl>
									<hr />
									<?php } ?>
									<?php $nomurut=0; foreach($dafpenbayi as $pen){ $nomurut+=1;?>
								<h2>Bayi #{{$nomurut}}</h2>
								<dl class="term-description">
										<dt>Gelar:</dt><dd>{{$pen->tit}}</dd>
										<dt>Nama:</dt><dd>{{$pen->fn}} {{$pen->ln}}</dd>
										<dt>Lahir:</dt><dd>{{$pen->birth}}</dd>
								</dl>
								<hr />
								<?php } ?>

										<h2>Informasi Tagihan</h2>
										<dl class="term-description">
												<dt>Total biaya:</dt><dd>{{$labelbiaya}}</dd>
												<dt>Ditujukan kepada:</dt><dd>{{$cpname}}</dd>
												<dt>Telepon:</dt><dd>{{$cptlp}}</dd>
												<dt>Email:</dt><dd>{{$cpmail}}</dd>
												<dt>Batas pembayaran:</dt><dd>{{$tgl_bill_exp}}</dd>
												<dt>Status tagihan:</dt><dd>{{$statustransaksi}}</dd>
										</dl>
										<hr/>
										@if(count($dafbuk)>0)
										<?php $nombukti=0;?>
										Daftar bukti pembayaran
											<div class="custom-panel bg-grey-2 radius-4">
												@foreach($dafbuk as $bukti)
												<?php $nombukti+=1;?>
												Bukti #{{$nombukti}}
												<div class="form-group">
													<table class="table">
															<tbody>
																<tr>
																	@if($bukti->namafile!=null)
																		<td class="" width="50"><img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Bukti Pembayaran" style="width:200px;height:200px;"></td>
																	@endif
																		<td class="table-label color-dark-2 nomobile">Tanggal upload : {{$bukti->tanggal}}<BR>Dari bank : {{$bukti->rekasal_bank}}<BR>Atas nama : {{$bukti->rekasal_napem}}<BR>Pembayaran ke bank : {{$bukti->rektuju_bank}}</td>
																</tr>
															</tbody>
													</table>
													<div class="justmobile">
														Tanggal upload : {{$bukti->tanggal}}<BR>Dari bank : {{$bukti->rekasal_bank}}<BR>Atas nama : {{$bukti->rekasal_napem}}<BR>Pembayaran ke bank : {{$bukti->rektuju_bank}}
													</div>

												</div>
												<br>
												@endforeach
											</div>
											@endif
										@if($status==0 || $status==1 || $status==3)
										@if($status==1 || $status==3)
										<span id="tommintaupload" onclick="$('#formuploadbukti').show();$(this).hide();" style="color: #ff6600; cursor:pointer;">Upload ulang bukti pembayaran</span>
										@endif
														<div class="detail-content-block" id="formuploadbukti">
															<div class="simple-text">
																<h4>Upload bukti pembayaran</h3>
																<form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
																	<input type="hidden" name="_token" value="{{ csrf_token() }}">
																	 <div class="form-group">
																		<label for="exampleInputEmail1">Dari Bank</label>
																		<input name="bank" required="" type="text" class="form-control" >
																	</div>
																	<div class="form-group">
																		<label for="exampleInputEmail1">Atas Nama</label>
																		<input name="napem" required="" type="text" class="form-control" >
																	</div>
																	<div class="form-group">
																		<label for="exampleInputEmail1">Ke Rekening</label>
																		<select class="form-control" name="rektuju" id="rektuju">
																			 @foreach($dafrek as $rek)
																			<option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
																			@endforeach
																		</select>
																	</div>
																<div class="form-group">
																	<label for="exampleInputFile">Gambar Bukti Transfer</label>
																	<input name="foto" type="file" id="exampleInputFile">
																</div>
																<div class="form-group">
																	<button type="submit" class="btn btn-primary">Kirim bukti</button>
																		@if($status==1 || $status==3)
																		<button type="button" onclick="$('#formuploadbukti').hide();$('#tommintaupload').show();" class="btn btn-danger">Cancel</button>
																		@endif
																</div>
															</form>

										       		</div>

										       	</div>
										@endif

										<a class="button btn-medium sky-blue1" href="{{URL('/')}}/flight/transaksi/{{$notrx}}/pdf">CETAK PDF</a>
									</div>
						</div>
						<div class="sidebar col-sm-4 col-md-3 nomobile">
								<div class="travelo-box contact-box">
										<h4>Butuh bantuan?</h4>
										<p>Costumer Service kami siap melayani</p>
										<address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                        <br>
                        <a class="contact-email" href="#">{{$infowebsite['email']}}</a>
                    </address>
								</div>

						</div>
				</div>
		</div>
</section>

@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
	<script type="text/javascript">
	@if($status==1)
	$('#formuploadbukti').hide();
	@endif
	 function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        hours = parseInt((timer /3600)%24, 10);
        minutes = parseInt((timer / 60)%60, 10)
        seconds = parseInt(timer % 60, 10);

				hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours+":"+minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 5;
        display = document.querySelector('.time');
  startTimer({{$sisawaktu}}, display);
};

$(document).ready(function(){
	$('#jumlahbayar').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
});

</script>
@endsection

@endsection
