@extends('layouts.'.$namatemplate)
@section('kontenweb')

        <div class="container">
					<ul class="breadcrumb">
							<li><a href="{{url('/')}}">Home</a>
							</li>
							<li><a href="{{url('/')}}/cekpesanan">Cek pesanan</a>
							</li>
							<li class="active">Detail</li>
					</ul>
            <div class="row">
							@if (session()->has('notiftagihan'))
							@if(isset(session('notiftagihan')['created']))
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Transaksi berhasil dibuat!</strong> Silahkan upload bukti pembayaran setelah Anda melakukan pembayaran. Terimakasih
							</div>
							@endif
							@if(isset(session('notiftagihan')['uploaded']))
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Upload bukti pembayaran berhasil!</strong> Silahkan tunggu email pemberitahuan dari kami. Terimakasih
							</div>
							@endif
							@endif
                <div class="col-md-4">
                    <h4>Trx no. {{$notrx}}</h4>
                    <p>Tagihan untuk: {{$cpname}}</p>
                    <p>Telepon: {{$cptlp}}</p>
                    <p>Email: {{$cpmail}}</p>
                    <p>Sebesar: {{$labelbiaya}}</p>
                    <p>Status: {{$statustransaksi}}</p>
                    <p>Batas waktu pembayaran: {{$tgl_bill_exp}}</p>
                </div>

                <div class="col-md-4">
                    @if(count($dafbuk)>0)
                    <?php $nombukti=0;?>
                    <h4>Daftar bukti pembayaran</h4>
                    <ul class="card-select">
                        @foreach($dafbuk as $bukti)
                        <?php $nombukti+=1;?>
                        <li>
                            <img class="card-select-img" src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Image Alternative text" title="Foto Bukti Transfer" />
                            <div class="card-select-data">
                                <p class="card-select-number">Tanggal upload : {{$bukti->tanggal}}</p>
                                <p class="card-select-number">Dari bank : {{$bukti->rekasal_bank}}</p>
                                <p class="card-select-number">Atas nama : {{$bukti->rekasal_napem}}</p>
                                <p class="card-select-number">Pembayaran ke bank : {{$bukti->rektuju_bank}}</p>

                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="gap gap-small"></div>
                    @endif
                    @if($status==0 || $status==1 || $status==3)
										@if($status==1 || $status==3)
										<span id="tommintaupload" onclick="$('#formuploadbukti').show();$(this).hide();" style="color: #ff6600; cursor:pointer;">Upload ulang bukti pembayaran</span>
										@endif
                    <div id="formuploadbukti">
                    <h4>Upload bukti pembayaran</h4>
                    <form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="clearfix">
                            <div class="form-group form-group-cc-number">
                                <label>Dari Bank</label>
                                <input name="bank" required="" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="form-group form-group-cc-name">
                                <label>Atas Nama</label>
                                <input name="napem" required="" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="form-group form-group-cc-name">
                                <label>Ke Rekening</label>
                                <select class="form-control" name="rektuju" id="rektuju">
                                   @foreach($dafrek as $rek)
                                  <option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="form-group form-group-cc-name">
                                <label>Gambar Bukti Transfer</label>
                                <input name="foto" type="file" id="exampleInputFile">
                            </div>
                        </div>
                        <div class="clearfix">
                          <div class="form-group">
                            <div class="checkbox checkbox-small">
                                <label>
                                    <input required="" class="i-check" type="checkbox" />Saya menyatakan bukti yang saya upload adalah valid</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Kirim bukti</button>
                              @if($status==1 || $status==3)
                              <button type="button" onclick="$('#formuploadbukti').hide();$('#tommintaupload').show();" class="btn btn-danger">Cancel</button>
                              @endif
                          </div>
                        </div>
                    </form>
                    </div>
                    @endif
                </div>

								<div class="col-md-4">
				            <div class="booking-item-payment">
				                <header class="clearfix">
				                    <h5 class="mb0">{{$kotorg}} ke {{$kotdes}} : {{maskapai($acDep)}}</h5>
				                </header>
				                <ul class="booking-item-payment-details">
				                    <li>

				                        <div class="booking-item-payment-flight">
																	<?php
																	  $dafdep = json_decode($daftranpergi, true);
																		$nom=0;
											 				    ?>
				                          @foreach($dafdep['jalurpergi'] as $trans)
				                          <?php $nom+=1; ?>
				                          <p>{{$trans['FlightNo']}}</p>
				                          <div class="row">
				                                <div class="col-md-9">
				                                    <div class="booking-item-flight-details">
				                                        <div class="booking-item-departure"><i class="fa fa-plane"></i>
				                                            <h5>{{date("H:i A",strtotime($trans['ETD']))}}</h5>
				                                            <p class="booking-item-date">{{date("D, M d",strtotime($trans['ETD']))}}</p>
				                                            <p class="booking-item-destination">{{airport_namakota($trans['STD'])}}</p>
				                                        </div>
				                                        <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
				                                          <h5>{{date("H:i A",strtotime($trans['ETA']))}}</h5>
				                                          <p class="booking-item-date">{{date("D, M d",strtotime($trans['ETA']))}}</p>
				                                          <p class="booking-item-destination">{{airport_namakota($trans['STA'])}}</p>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-md-3">
				                                    <div class="booking-item-flight-duration">
				                                        <p>Durasi</p>
				                                        <h5>{{lamajalan($trans['ETD'],$trans['ETA'],'%02d h, %02d m')}}</h5>
				                                    </div>
				                                </div>
				                            </div>
				                            @endforeach
				                        </div>
				                    </li>
				                </ul>

				                @if($flight=="R")
				                <header class="clearfix">
				                    <h5 class="mb0">{{$kotdes}} ke {{$kotorg}} : {{maskapai($acRet)}}</h5>
				                </header>
				                <ul class="booking-item-payment-details">
				                    <li>

				                        <div class="booking-item-payment-flight">
																	<?php
							 			   						$dafret = json_decode($daftranpulang, true);
																	$nom=0;
							 			               ?>
				                          @foreach($dafret['jalurpulang'] as $trans)
				                          <?php $nom+=1; ?>
				                          <p>{{$trans['FlightNo']}}</p>
				                          <div class="row">
				                                <div class="col-md-9">
				                                    <div class="booking-item-flight-details">
				                                        <div class="booking-item-departure"><i class="fa fa-plane"></i>
				                                            <h5>{{date("H:i A",strtotime($trans['ETD']))}}</h5>
				                                            <p class="booking-item-date">{{date("D, M d",strtotime($trans['ETD']))}}</p>
				                                            <p class="booking-item-destination">{{airport_namakota($trans['STD'])}}</p>
				                                        </div>
				                                        <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
				                                          <h5>{{date("H:i A",strtotime($trans['ETA']))}}</h5>
				                                          <p class="booking-item-date">{{date("D, M d",strtotime($trans['ETA']))}}</p>
				                                          <p class="booking-item-destination">{{airport_namakota($trans['STA'])}}</p>
				                                        </div>
				                                    </div>
				                                </div>
				                                <div class="col-md-3">
				                                    <div class="booking-item-flight-duration">
				                                        <p>Durasi</p>
				                                        <h5>{{lamajalan($trans['ETD'],$trans['ETA'],'%02d h, %02d m')}}</h5>
				                                    </div>
				                                </div>
				                            </div>
				                            @endforeach
				                        </div>
				                    </li>
				                </ul>
				                @endif

				                <p class="booking-item-payment-total">Total biaya: <span>{{$labelbiaya}}</span>
				                </p>
				            </div>	<a class="btn btn-primary" href="{{URL('/')}}/flight/transaksi/{{$notrx}}/pdf">CETAK PDF</a>

				        </div>
            </div>
            <div class="gap"></div>
        </div>


@section('akhirbody')

	<script type="text/javascript">
	@if($status==1)
	$('#formuploadbukti').hide();
	@endif
	 function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        hours = parseInt((timer /3600)%24, 10);
        minutes = parseInt((timer / 60)%60, 10)
        seconds = parseInt(timer % 60, 10);

				hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours+":"+minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 5;
        display = document.querySelector('.time');
  startTimer({{$sisawaktu}}, display);
};

$(document).ready(function(){
	$('#jumlahbayar').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
});

</script>
@endsection

@endsection
