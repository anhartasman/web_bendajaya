<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')

<form class="contact-form"id="formgo" action="{{url('/')}}/flight/flight_isiform" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_acDep" name="formgo_acDep">
<input type="hidden" id="formgo_acRet" name="formgo_acRet">
<input type="hidden" id="formgo_flight" name="formgo_flight" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_daftranpergi" name="formgo_daftranpergi" >
<input type="hidden" id="formgo_daftranpulang" name="formgo_daftranpulang" >
<input type="hidden" id="formgo_stattransitpergi" name="formgo_stattransitpergi" >
<input type="hidden" id="formgo_stattransitpulang" name="formgo_stattransitpulang" >
<input type="hidden" id="formgo_dafsubclasspergi" name="formgo_dafsubclasspergi" >
<input type="hidden" id="formgo_dafsubclasspulang" name="formgo_dafsubclasspulang" >
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>

<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{url('/')}}">Home</a> /</li>
				<li><span class="color-red-3">Pesawat</span></li>
			</ul>
			<h2 class="color-white">Tiket Pesawat</h2>

		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
  <div class="container">
    <div class="row">
      <div id="sebelahkiri" class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar bg-white clearfix">
        <div class="sidebar-block">
          <h4 class="sidebar-title color-dark-2">search</h4>
          <div class="search-inputs">
            <div class="form-block clearfix">
              <h5>Asal</h5><div class="input-style b-50 color-3">
              <select class="input-style selektwo"placeholder="Check In"  name="asal" id="pilasal">
                   <option value=""></option>
                   @foreach($dafban as $area)
                  <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $org==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
                  @endforeach
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Tujuan</h5>
                <div class="input-style">
                <select class="input-style selektwo" name="tujuan" id="piltujuan">
                   <option value=""></option>
                   @foreach($dafban as $area)
                  <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $des==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
                  @endforeach
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Rencana</h5>
                <div class="input-style">
                <select class="mainselection" id="pilrencana" name="pilrencana" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">
                  <option value="O" <?php if($otomatiscari==1 && $flight=="O"){print("selected=\"selected\"");} ?>>Sekali Jalan</option>
                  <option value="R" <?php if($otomatiscari==1 && $flight=="R"){print("selected=\"selected\"");} ?>>Pulang Pergi</option>

                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Berangkat</h5>
                <div class="input-style">
                  <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglberangkat)){print(date("d-m-Y",strtotime($tglberangkat)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div id="tny_ret" class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Pulang</h5>
                <div class="input-style">
                  <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglpulang)){print(date("d-m-Y",strtotime($tglpulang)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Dewasa</h5>
                <div class="input-style">
                  <select class="mainselection" id="adt" name="adt" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=7;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select>
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Anak</h5>
                <div class="input-style">
                  <select class="mainselection"id="chd" name="chd" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=0; $op<=6;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Bayi</h5>
                <div class="input-style">
                  <select class="mainselection"id="inf" name="inf" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=0; $op<=6;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
          </div>
          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
        </div>


        </div>
      </div>

      <input style="margin-left:10px;margin-bottom:10px;" type="submit" id="tomback" class="c-button b-40 bg-red-3 hv-red-3-o" value="Cari jadwal lain">

      <div class="col-xs-12 col-sm-8 col-md-9">

              <div class="clearfix">

                        <div id="titikup"></div>
        				<div class="col-xs-12 col-sm-8 col-md-12" id="divterpilih">

                    <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                      <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Penerbangan yang dipilih : Padang (PDG) - Jakarta (CGK) | Dewasa: 1 Anak: 0 Bayi: 0</h4>

                    </div>

        					<div class="list-content clearfix" id="pilihanpergi">
        						<div class="list-item-entry" >
        					        <div class="hotel-item style-10 bg-white">
        					        	<div class="table-view">
      						          	<div class="radius-top cell-view">
      						          	 	<img style="padding-left:20px;width:100%;height:50px;"id="pilihanpergi_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
      						          	</div>
                              <div class="title hotel-middle clearfix cell-view">
                                <div class="date grid-hidden"><strong class="color-red-3" id="pilihanpergi_pesawal"></strong></div>
                                  <h4><b id="pilihanpergi_harga"></b></h4>
                              <h4><b id="pilihanpergi_jampergi"></b> - <b id="pilihanpergi_jamtiba"></b></h4>

                              </div>
                              <div class="nomobile">
                              <div class="title hotel-right clearfix cell-view">
                                <div class="hotel-person color-dark-2"></div>
                                  <div class="hotel-person color-dark-2" id="pilihanpergi_stat">Langsung</div>
                                <div class="hotel-person color-dark-2"></div>
                              </div>
                              </div>
                              <div class="title hotel-right clearfix cell-view">
                              <span id="tomubahpilihanpergi" style="cursor:pointer;margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" >Ubah</span>
                              </div>
      					            </div>
      					        </div>
        						</div>
                    <div class=" clearfix" style="background-color: #ff6600;" id="labelpilihanpulang">

                      <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkedua">Penerbangan pergi</h4>

                    </div>
          					<div class="list-item-entry" id="pilihanpulang">
        					        <div class="hotel-item style-10 bg-white">
        					        	<div class="table-view">
        						          	<div class="radius-top cell-view">
        						          	 	<img style="padding-left:20px;width:100%;height:50px;" id="pilihanpulang_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
        						          	</div>
                                <div class="title hotel-middle clearfix cell-view">
                                  <div class="date grid-hidden"><strong class="color-red-3" id="pilihanpulang_pesawal"></strong></div>
                                    <h4><b id="pilihanpulang_harga"></b></h4>
                                <h4><b id="pilihanpulang_jampergi"></b> - <b id="pilihanpulang_jamtiba"></b></h4>

                                </div>
                                <div class="nomobile">
                                <div class="title hotel-right clearfix cell-view">
                                  <div class="hotel-person color-dark-2"></div>
                                    <div class="hotel-person color-dark-2" id="pilihanpulang_stat">Langsung</div>
                                  <div class="hotel-person color-dark-2"></div>
                                </div>
                                </div>
                                <div class="title hotel-right clearfix cell-view">
                                <span id="tomubahpilihanpulang"  style="cursor:pointer;margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" >Ubah</span>
                                </div>
        					            </div>
        					        </div>
          						</div>



        					</div>


        				</div>
              </div>
        <div class="list-header clearfix" id="barissorting">
          <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
            <div style="color:BLACK;">
             <span  class="nomobile">Urut berdasarkan</span>
             </div>
          </div>
            <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
              <div class="drop"style="color:BLACK;">
               <b>-</b>
                <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                <span>
                  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">Harga terkecil</a>
                  <a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">Harga terbesar</a>
                  <a href="#" onclick="sortAsc('data-waktu')"style="color:BLACK;">Jam terdekat</a>
                  <a href="#" onclick="sortDesc('data-waktu')"style="color:BLACK;">Jam terakhir </a>
                </span>
               </div>
            </div>


        </div>

        <div  id="sisikiri">
@if($errors->has())
            <div class="list-header clearfix" style="background-color: #ff6600;"id="divtulisanerror">
              @foreach ($errors->all() as $error)
              <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;"  >
              {{ $error }}</h4>

          </div>
             @endforeach
             @endif
             <div class="list-header clearfix" style="background-color: #ff6600;">

                <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkiri" >

                            Pilih penerbangan pergi</h4>

            </div>
            <div class="" id="dafloading" >

                    <div class="list-item-entry" id="loading_qz">
                        <div class="hotel-item style-10 bg-white">
                          <div class="table-view">
                              <div class="radius-top cell-view">
                                <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-QZ.png')}}" alt="">
                              </div>
                              <div class="title hotel-middle cell-view">
                                <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                                <h6 class="color-grey-3 list-hidden">loading..</h6>
                               </div>
                              <div class="title hotel-right clearfix cell-view grid-hidden">
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="list-item-entry" id="loading_qg">
                      <div class="hotel-item style-10 bg-white">
                        <div class="table-view">
                            <div class="radius-top cell-view">
                              <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-QG.png')}}" alt="">
                            </div>
                            <div class="title hotel-middle cell-view">
                              <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                              <h6 class="color-grey-3 list-hidden">loading..</h6>
                             </div>
                            <div class="title hotel-right clearfix cell-view grid-hidden">
                          </div>
                          </div>
                      </div>
                    </div>

                    <div class="list-item-entry" id="loading_ga">
                    <div class="hotel-item style-10 bg-white">
                      <div class="table-view">
                          <div class="radius-top cell-view">
                            <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-GA.png')}}" alt="">
                          </div>
                          <div class="title hotel-middle cell-view">
                            <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                            <h6 class="color-grey-3 list-hidden">loading..</h6>
                           </div>
                          <div class="title hotel-right clearfix cell-view grid-hidden">
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_kd">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                        <div class="radius-top cell-view">
                          <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KD.png')}}" alt="">
                        </div>
                        <div class="title hotel-middle cell-view">
                          <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                          <h6 class="color-grey-3 list-hidden">loading..</h6>
                         </div>
                        <div class="title hotel-right clearfix cell-view grid-hidden">
                      </div>
                      </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_jt">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                      <div class="radius-top cell-view">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-JT.png')}}" alt="">
                      </div>
                      <div class="title hotel-middle cell-view">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                        <h6 class="color-grey-3 list-hidden">loading..</h6>
                       </div>
                      <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_sj">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-SJ.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                      <h6 class="color-grey-3 list-hidden">loading..</h6>
                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_mv">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                    <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-MV.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                    <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                    <h6 class="color-grey-3 list-hidden">loading..</h6>
                    </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="list-item-entry" id="loading_il">
                    <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                    <div class="radius-top cell-view">
                    <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-IL.png')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                    <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                    <h6 class="color-grey-3 list-hidden">loading..</h6>
                    </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                    </div>
                    </div>
                    </div>
                    </div>


                  </div>
        <div class="list-content clearfix" id="dafpergi">
        </div>
        </div>
        <div  id="sisikanan">

            <div class="list-header clearfix" style="background-color: #ff6600;">

                 <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Pilih penerbangan pulang</h4>

            </div>
        <div class="list-content clearfix" id="dafpulang">
        </div>
        </div>

        <div class="row" id="divpilihnextorulang">

  					<!-- sisi kanantombol -->
    				<div class="col-xs-12 col-sm-8 col-md-12">

                <div class="list-header clearfix" style="background-color: #ff6600;">

                   <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>

                </div>

    				</div>
          </div>
      </div>
    </div>
  </div>
</div>


				@section('akhirbody')
		<script type="text/javascript">

    $("#labelpilihanpulang").hide();
    $("#sisikanan").hide();
    $("#barissorting").hide();
    $("#sisikiri").hide();

    //variabel

		var hargapulang="0";
		var hargapergi="0";
    var nilpil=0;
    var satuaja=0;
    var idpilper="";
    var idpilret="";
		var banasal="CGK";
		var kotasal="Jakarta";
		var labelasal="";
		var kottujuan="Manokwari";
		var bantujuan="MKW";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_dep').val(tglberangkat);
    $('#formgo_tgl_deppilihan').val(tglberangkat);
		var tglpulang="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_ret').val(tglpulang);
    $('#formgo_tgl_retpilihan').val(tglpulang);

    @if($otomatiscari==1)
    $('#formgo_tgl_deppilihan').val("{{$tglberangkat}}");
    $('#formgo_tgl_retpilihan').val("{{$tglpulang}}");
    $('#formgo_flight').val("{{$flight}}");
    $('#formgo_org').val("{{$org}}");
    $('#formgo_des').val("{{$des}}");
    $('#formgo_kotorg').val("{{$kotasal}}");
    $('#formgo_labelorg').val("{{$kotasal}} ({{$org}})");
    $('#formgo_kotdes').val("{{$kottujuan}}");
    $('#formgo_labeldes').val("{{$kottujuan}} ({{$des}})");
    $('#formgo_chd').val("{{$jumchd}}");
    $('#formgo_adt').val("{{$jumadt}}");
    $('#formgo_inf').val("{{$juminf}}");
    @endif

		var jumadt="1";
		var jumchd="0";
		var juminf="0";



		var urljadwal="{{ url('/') }}/jadwalPesawat/";
		var urljadwalb=urljadwal;

    var dafHTMLPergi = [];
    var dafHTMLPulang = [];


		$('#tomback').hide();
		$('#divpilihnextorulang').hide();

    $( "#tomgoone" ).click(function() {
      $( "#formgo" ).submit();
    });

		$('#adt').on('change', function() {
		jumadt=this.value;
    $('#formgo_adt').val(jumadt);

		});
		$('#chd').on('change', function() {
		jumchd=this.value;
    $('#formgo_chd').val(jumchd);

		});
		$('#inf').on('change', function() {
		juminf=this.value;
    $('#formgo_inf').val(juminf);

		});


		$('#labelpilihanpergi').hide();
		$('#pilihanpergi').hide();
		$('#pilihanpulang').hide();
		  $('.selektwo').select2();
      <?php if(($otomatiscari==1 && $flight=="O")||$otomatiscari==0){?>
		  $('#tny_ret').hide();
      <?php }?>
			                $('#loading_qz').hide();
			                $('#loading_qg').hide();
											$('#loading_ga').hide();
											$('#loading_kd').hide();
											$('#loading_jt').hide();
											$('#loading_sj').hide();
											$('#loading_mv').hide();
											$('#loading_il').hide();


    $("#tglberangkat").datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    maxDate: '+2y'
});

$("#tglpulang").datepicker({
    dateFormat: 'dd-mm-yy',
    minDate: new Date(),
    changeMonth: true,
    changeYear: true
});

		$('#tglberangkat').on('change', function() {

		var values=this.value.split('-');
		//alert(this.value);
		tglber_d=values[0];
		tglber_m=values[1];
		tglber_y=values[2];

		tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

    $('#formgo_tgl_deppilihan').val(tglberangkat);

    var selectedDate = new Date(tglberangkat);
    var msecsInADay = 86400000;
    var endDate = new Date(selectedDate.getTime());
            $("#tglpulang").datepicker( "option", "minDate", endDate );
            $("#tglpulang").datepicker( "option", "maxDate", '+2y' );
                settglpulang($("#tglpulang").val());
		});

		$('#tglpulang').on('change', function() {

    settglpulang(this.value);
		});

    function settglpulang(val){

      		var values=val.split('-');
      		//alert(this.value);
      		tglpul_d=values[0];
      		tglpul_m=values[1];
      		tglpul_y=values[2];

      		tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
          $('#formgo_tgl_retpilihan').val(tglpulang);
    }

		$('#pilasal').on('change', function() {
		  var values=this.value.split('-');
		  var kotas=values[1];
		  kotasal=values[0];
		  //alert(kotas);
		  banasal=kotas;
		  labelasal=this.value;
      $('#formgo_labelorg').val(labelasal);
      $('#formgo_org').val(banasal);
      $('#formgo_kotorg').val(kotasal);

		});
		$('#piltujuan').on('change', function() {
		  var values=this.value.split('-');
		  var kottuj=values[1];
		  kottujuan=values[0];
		  //alert(kottuj);
		  bantujuan=kottuj;
		  labeltujuan=this.value;
      $('#formgo_labeldes').val(labeltujuan);
      $('#formgo_des').val(kottuj);
      $('#formgo_kotdes').val(kottujuan);

		});
		$('#pilrencana').on('change', function() {
		  //alert( this.value );
		  pilrencana=this.value;
      $('#formgo_flight').val(pilrencana);
		  var tny_ret = document.getElementById("tny_ret").value;

		if(this.value=="O"){
		    $('#tny_ret').hide();
     }else{
		    $('#tny_ret').show();
     }


		});

		function labelutama(){
		  $('#labelkolomutama').html('Penerbangan '+kotasal+" ke "+kottujuan);
		}

function convertToRupiah(angka){
var rupiah = '';
var angkarev = angka.toString().split('').reverse().join('');
for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
return rupiah.split('',rupiah.length-1).reverse().join('');
}

$('#pilsubclasspergi').on('change', function() {
  hargapergi=this.value.split(",")[0];
  var totaladt=hargapergi*jumadt;
  var totalchd=hargapergi*jumchd;
  var totalinf=hargapergi*juminf;
  $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
  totalhargapergi=totaladt+totalchd+totalinf;
  $('#pilihanpergi_harga').html("IDR "+convertToRupiah(totalhargapergi));


});
$('#pilsubclasspulang').on('change', function() {
  hargapulang=this.value.split(",")[0];
  var totaladt=hargapulang*jumadt;
  var totalchd=hargapulang*jumchd;
  var totalinf=hargapulang*juminf;
  $('#formgo_selectedIDret').val(this.value.split(",")[1]);
  totalhargapulang=totaladt+totalchd+totalinf;
  $('#pilihanpulang_harga').html("Rp "+convertToRupiah(totalhargapulang));


});

		function pilihPergi(bahanid
		  ,daftranpergi
  		,pesawatawal
		  ,jampergiberangkat
		  ,jampergitiba
		  ,harga
		  ,ikon
		  ,stattransitpergi
		  ,dafsubclasspergi
		  ,kumpulaniddep
      ,hasildiskon
		  ){

        var totaladt=harga*jumadt;
        var totalchd=harga*jumchd;
        var totalinf=harga*juminf;
      //  harga=totaladt+totalchd+totalinf;

        var totaladtdiskon=hasildiskon*jumadt;
        var totalchddiskon=hasildiskon*jumchd;
        var totalinfdiskon=hasildiskon*juminf;
      //  hasildiskon=totaladtdiskon+totalchddiskon+totalinfdiskon;

      		var subclasses=dafsubclasspergi.split('#');
          $('#formgo_selectedIDdep').val(kumpulaniddep);
          $('#pilsubclasspergi').find('option').remove();


            $.each(subclasses, function(key, value) {
         	var subclassesisi=value.split(',');
          if(subclassesisi[3]!=null){
          $('#pilsubclasspergi')
         .append($("<option></option>")
                    .attr("value",subclassesisi[2]+","+subclassesisi[3])
                    .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                  }
           });


    $('#formgo_tgl_dep').val(jampergiberangkat);
    $('#formgo_tgl_dep_tiba').val(jampergitiba);
    $('#formgo_dafsubclasspergi').val(dafsubclasspergi);
    $('#formgo_stattransitpergi').val(stattransitpergi);
    $('#formgo_daftranpergi').val(daftranpergi);
		$('#formgo_acDep').val(ikon);
    $("#"+idpilper).removeClass("kotakpilihb cell-view");
    $("#"+idpilper).addClass("kotakpilih cell-view");

		$("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
    $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
idpilper=bahanid+"_gbr";

		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		$('#pilihanpergi').show('slow');
    $('#divterpilih').show('slow');
    $('#divtulisanerror').hide();

		//  alert(jampergitiba);
		        $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
		        $('#pilihanpergi_dafpes').html(kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+")"+' : '+daftranpergi+" ("+stattransitpergi+")");
		        $('#pilihanpergi_pesawal').html(pesawatawal);
		        $('#pilihanpergi_jampergi').html(jampergiberangkat);
		        $('#pilihanpergi_jamtiba').html(jampergitiba);
		        $('#pilihanpergi_harga').html("Rp "+convertToRupiah(hasildiskon));
            $('#pilihanpergi_stat').html(stattransitpergi);

            nilpil+=1;
            $(location).attr('href', '#labelkolomutama');

          //  alert ('tes tes');
              $( "#labelkolomutama" ).focusin();
            if(pilrencana2=="O" || (pilrencana2=="R" && satuaja==1)){
             $('#sisikiri').hide('slow');
              $('#barissorting').hide('slow');
             mintaformgo();
           }else if(pilrencana2=="R" && satuaja==0){
             mintaformret();
           }
           satuaja=0;

		}
		function pilihPulang(bahanid
		  ,daftranpulang
  		,pesawatawal
		  ,jampulangberangkat
		  ,jampulangtiba
		  ,harga
		  ,ikon
		  ,stattransitpulang
		  ,dafsubclasspulang
		  ,kumpulanidret
      ,hasildiskon
		  ){

                var totaladt=harga*jumadt;
                var totalchd=harga*jumchd;
                var totalinf=harga*juminf;
            //    harga=totaladt+totalchd+totalinf;

                var totaladtdiskon=hasildiskon*jumadt;
                var totalchddiskon=hasildiskon*jumchd;
                var totalinfdiskon=hasildiskon*juminf;
            //    hasildiskon=totaladtdiskon+totalchddiskon+totalinfdiskon;
        var subclasses=dafsubclasspulang.split('#');
        $('#formgo_selectedIDret').val(kumpulanidret);
        $('#pilsubclasspulang').find('option').remove();


                    $.each(subclasses, function(key, value) {
                 	var subclassesisi=value.split(',');
                  if(subclassesisi[3]!=null){
             $('#pilsubclasspulang')
                 .append($("<option></option>")
                            .attr("value",subclassesisi[2]+","+subclassesisi[3])
                            .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                          }
        });

        $('#formgo_tgl_ret').val(jampulangberangkat);
        $('#formgo_tgl_ret_tiba').val(jampulangtiba);
        $('#formgo_dafsubclasspulang').val(dafsubclasspulang);
        $('#formgo_stattransitpulang').val(stattransitpulang);
        $('#formgo_daftranpulang').val(daftranpulang);
        $('#formgo_acRet').val(ikon);

        $("#"+idpilret).removeClass("kotakpilihb cell-view");
        $("#"+idpilret).addClass("kotakpilih cell-view");

    		$("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
        idpilret=bahanid+"_gbr";

   $('#barissorting').hide('slow');
		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		  $('#pilihanpulang').show('slow');
      $('#divterpilih').show('slow');
		//  alert(jampergitiba);

            $("#labelpilihanpulang").show('slow');
            $("#labelkolomkedua").html('Penerbangan '+kottujuan+" ke "+kotasal);
		        $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
            $('#pilihanpulang_pesawal').html(pesawatawal);
		        $('#pilihanpulang_dafpes').html(kottujuan+" ("+bantujuan+") - "+kotasal+" ("+banasal+")"+' : '+daftranpulang+" ("+stattransitpulang+")");
		        $('#pilihanpulang_jampergi').html(jampulangberangkat);
		        $('#pilihanpulang_jamtiba').html(jampulangtiba);
		        $('#pilihanpulang_harga').html("Rp "+convertToRupiah(hasildiskon));
            $('#pilihanpulang_stat').html(stattransitpulang);
             mintaformgo();
             satuaja=0;
          $(location).attr('href', '#labelkolomutama');

          $("#sisikanan").hide('slow');
    }
    function mintaformgo(){
      $('#divpilihnextorulang').show('slow');
    }
    function mintaformret(){
       $("#sisikiri").hide('slow');
       $("#sisikanan").show('slow');
       $('#divpilihnextorulang').hide();
    }


    $('#tomubahpilihanpergi').on('click', function() {
    satuaja=1;
    $('#divterpilih').hide('slow');
    $('#sisikiri').show('slow');
    $('#barissorting').show('slow');
    });
    $('#tomubahpilihanpulang').on('click', function() {
    $('#sisikanan').show('slow');
    $('#divterpilih').hide('slow');
    $('#barissorting').show('slow');
    });
    $('#tomback').on('click', function() {
    $('#tomback').hide('slow');
    $('#sebelahkiri').show('slow');
    });

    $('.tomup').on('click', function() {
      $('#titikup')[0].scrollIntoView(true);
    });
		function cari(){
    nilpil=0;
    if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
    //  $('html, body').animate({scrollTop:$('#titikup').position().top}, 'slow');
   //$('html, body').scrollTo($('#titikup'), 1000);
   var isMobile = window.matchMedia("only screen and (max-width: 760px)");

       if (isMobile.matches) {
         $("#tomback").show('slow');
         $("#sebelahkiri").hide('slow');
         //$('#titikup')[0].scrollIntoView(true);
       }

    $('#labelkolomkedua').hide();

$("#barissorting").show('slow');
    $('#sisikanan').hide();
    $('#divpilihnextorulang').hide();
		//  alert("Memulai pencarian");
		        //  $('#hasilkirim').html("<b>Hello world!</b>");
            if(pilrencana=="O"){
                $("#sisikanan").hide('slow');
                $("#sisikiri").show('slow');
                  //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                  //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
        		  }else{
                $("#sisikiri").show('slow');
                    $('#labelkolomkedua').show();
                //$("#sisikanan").show('slow');
                  //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                  //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                  //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                  //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
        		  }
		        urljadwalb=urljadwal;
		        urljadwalb+="org/"+banasal;
		        urljadwalb+="/des/"+bantujuan;
		        urljadwalb+="/flight/"+pilrencana;
		        urljadwalb+="/tglberangkat/"+tglberangkat;
		        urljadwalb+="/tglpulang/"+tglpulang;
		        urljadwalb+="/jumadt/"+jumadt;
		        urljadwalb+="/jumchd/"+jumchd;
		        urljadwalb+="/juminf/"+juminf;
		        //akhir urljadwalb+="ac/";
		                $('#loading_qz').show('fadeOut');
										$('#loading_qg').show('fadeOut');
										$('#loading_ga').show('fadeOut');
										$('#loading_kd').show('fadeOut');
										$('#loading_jt').show('fadeOut');
										$('#loading_sj').show('fadeOut');
										$('#loading_mv').show('fadeOut');
										$('#loading_il').show('fadeOut');

		                $('#dafpergi').html('');
										$('#dafpulang').html('');
		                urljadwalb+="/ac/";

		        ambildata_qz();
						ambildata_qg();
						ambildata_ga();
						ambildata_kd();
						ambildata_jt();
						ambildata_sj();
						ambildata_mv();
						ambildata_il();

            pilrencana2=pilrencana;
    $('#formatas').hide('slow');
		       $('#pilihanpergi').hide();
		        $('#pilihanpulang').hide();

          		      $('#labelkolomutama').hide('slow');
		      $('#labelpilihanpergi').hide('slow');

		      //  alert(urljadwalb);
      		labelutama();
        }else{
        //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
        }
		}
		var ajaxku_il;
		function ambildata_il(){
		  ajaxku_il = buatajax();
		  var url=urljadwalb+"IL";
		  ajaxku_il.onreadystatechange=stateChanged_il;
		  ajaxku_il.open("GET",url,true);
		  ajaxku_il.send(null);
		}

		function stateChanged_il(){
		   var data;
		    if (ajaxku_il.readyState==4){
		      data=ajaxku_il.responseText;
		      if(data.length>0){
		        tambahData(data);
		       }else{
		       }
			   $('#loading_il').hide('slow');
		    }
		}

		var ajaxku_mv;
		function ambildata_mv(){
		  ajaxku_mv = buatajax();
		  var url=urljadwalb+"MV";
		  ajaxku_mv.onreadystatechange=stateChanged_mv;
		  ajaxku_mv.open("GET",url,true);
		  ajaxku_mv.send(null);
		}

		 function stateChanged_mv(){
		   var data;
		    if (ajaxku_mv.readyState==4){
		      data=ajaxku_mv.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_mv').hide('slow');
		     }
		}

		var ajaxku_sj;
		function ambildata_sj(){
		  ajaxku_sj = buatajax();
		  var url=urljadwalb+"SJ";
		  ajaxku_sj.onreadystatechange=stateChanged_sj;
		  ajaxku_sj.open("GET",url,true);
		  ajaxku_sj.send(null);
		}

		 function stateChanged_sj(){
		   var data;
		    if (ajaxku_sj.readyState==4){
		      data=ajaxku_sj.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_sj').hide('slow');
		     }
		}

		var ajaxku_jt;
		function ambildata_jt(){
		  ajaxku_jt = buatajax();
		  var url=urljadwalb+"JT";
		  ajaxku_jt.onreadystatechange=stateChanged_jt;
		  ajaxku_jt.open("GET",url,true);
		  ajaxku_jt.send(null);
		}

		 function stateChanged_jt(){
		   var data;
		    if (ajaxku_jt.readyState==4){
		      data=ajaxku_jt.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_jt').hide('slow');
		     }
		}

		var ajaxku_kd;
		function ambildata_kd(){
		  ajaxku_kd = buatajax();
		  var url=urljadwalb+"KD";
		  ajaxku_kd.onreadystatechange=stateChanged_kd;
		  ajaxku_kd.open("GET",url,true);
		  ajaxku_kd.send(null);
		}

		 function stateChanged_kd(){
		   var data;
		    if (ajaxku_kd.readyState==4){
		      data=ajaxku_kd.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_kd').hide('slow');
		     }
		}


		var ajaxku_ga;
		function ambildata_ga(){
		  ajaxku_ga = buatajax();
		  var url=urljadwalb+"GA";
		  ajaxku_ga.onreadystatechange=stateChanged_ga;
		  ajaxku_ga.open("GET",url,true);
		  ajaxku_ga.send(null);
		}

		 function stateChanged_ga(){
		   var data;
		    if (ajaxku_ga.readyState==4){
		      data=ajaxku_ga.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_ga').hide('slow');
		     }
		}

		var ajaxku_qz;
		function ambildata_qz(){
		  ajaxku_qz = buatajax();
		  var url=urljadwalb+"QZ";
		  ajaxku_qz.onreadystatechange=stateChanged_qz;
		  ajaxku_qz.open("GET",url,true);
		  ajaxku_qz.send(null);
		}
		 function stateChanged_qz(){
		   var data;
		    if (ajaxku_qz.readyState==4){
		      data=ajaxku_qz.responseText;
		      if(data.length>0){
		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_qz').hide('slow');
		     }
		}


		var ajaxku_qg;
		function ambildata_qg(){
		  ajaxku_qg = buatajax();
		  var url=urljadwalb+"QG";
		  ajaxku_qg.onreadystatechange=stateChanged_qg;
		  ajaxku_qg.open("GET",url,true);
		  ajaxku_qg.send(null);
		}
		 function stateChanged_qg(){
		   var data;
		    if (ajaxku_qg.readyState==4){
		      data=ajaxku_qg.responseText;
		      if(data.length>0){
		        //document.getElementById("hasilkirim").html = data;

		                tambahData(data);
		       }else{
		       }
					 				        $('#loading_qg').hide('slow');
		     }
		}

    var kolomsort="";
    function sorterAsc(a, b) {
    return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
    };
    function sorterDesc(a, b) {
    return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
    };


    function sortAsc(kolom){
      kolomsort=kolom;
      var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
      console.log(sortedDivs);
      $.each(sortedDivs, function (index, value) {
          $('#dafpergi').append(value);
      });


      var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
      console.log(sortedDivs);
      $.each(sortedDivs, function (index, value) {
          $('#dafpulang').append(value);
      });
    }

    function sortDesc(kolom){
      kolomsort=kolom;
        var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
        console.log(sortedDivs);
        $.each(sortedDivs, function (index, value) {
            $('#dafpergi').append(value);
        });


        var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
        console.log(sortedDivs);
        $.each(sortedDivs, function (index, value) {
            $('#dafpulang').append(value);
        });
    }

		function tambahData(data){

			                //$('#dafpergi').html(data+$('#dafpergi').html());
											//$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
							 			  //$('.bagpulang').remove();
                      $(data).each(function(){
                        if($(this).attr('isiitem')=="1"){
                        if($(this).attr('untuk')=="pergi"){
                        var temp_id=$(this).attr('id');
                        var biaya=$(this).attr('data-harga');
                        var waktu=$(this).attr('data-waktu');

                        var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
                        //alert(waktu);
                        var valueToPush = new Array();
                        valueToPush[0] = temp_id;
                        valueToPush[1] = biaya;
                        valueToPush[2] = waktu;
                        valueToPush[3] = isiHTML;
                        //dafHTMLPergi.push(valueToPush);
                        $('#dafpergi').html(isiHTML+$('#dafpergi').html());


                        }else if($(this).attr('untuk')=="pulang"){
                        var temp_id=$(this).attr('id');
                        var biaya=$(this).attr('data-harga');
                        var waktu=$(this).attr('data-waktu');

                          var isiHTML='<div class="list-item-entry isiitempulang isiitem" id="'+temp_id+'" data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
                          //alert(temp_id);

                        var valueToPush = new Array();
                        valueToPush[0] = temp_id;
                        valueToPush[1] = biaya;
                        valueToPush[2] = waktu;
                        valueToPush[3] = isiHTML;
                        //dafHTMLPulang.push(valueToPush);
                        $('#dafpulang').html(isiHTML+$('#dafpulang').html());


                      }
                    }});
		}

		var ajaxku;
		function ambildata(){
		  ajaxku = buatajax();
		  var url="{{ url('/') }}/jadwalPesawat";
		  //url=url+"?q="+nip;
		  //url=url+"&sid="+Math.random();
		  ajaxku.onreadystatechange=stateChanged;
		  ajaxku.open("GET",url,true);
		  ajaxku.send(null);
		}
		function buatajax(){
		  if (window.XMLHttpRequest){
		    return new XMLHttpRequest();
		  }
		  if (window.ActiveXObject){
		     return new ActiveXObject("Microsoft.XMLHTTP");
		   }
		   return null;
		 }
		 function stateChanged(){
		   var data;
		    if (ajaxku.readyState==4){
		      data=ajaxku.responseText;
		      if(data.length>0){
		        //document.getElementById("hasilkirim").html = data;

						        $('#loading_qz').hide('show');
		                $('#hasilkirim').append(data);
		       }else{
		        // document.getElementById("hasilkirim").html = "";
		              //   $('#hasilkirim').html("");
		       }
		     }
		}

    @if($otomatiscari==1)
    banasal="{{$org}}";
    bantujuan="{{$des}}";
    pilrencana="{{$flight}}";
    tglberangkat="{{$tglberangkat}}";
    tglpulang="{{$tglpulang}}";
    jumadt="{{$jumadt}}";
    jumchd="{{$jumchd}}";
    juminf="{{$juminf}}";
    kotasal="{{$kotasal}}";
    kottujuan="{{$kottujuan}}";
    cari();
    @endif
		</script>
		@endsection

		@endsection
