@extends('layouts.'.$namatemplate)

@section('kontenweb')
<form class="contact-form"id="formgo" action="{{url('/')}}/flight/flight_isiform" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_acDep" name="formgo_acDep">
<input type="hidden" id="formgo_acRet" name="formgo_acRet">
<input type="hidden" id="formgo_flight" name="formgo_flight" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_daftranpergi" name="formgo_daftranpergi" >
<input type="hidden" id="formgo_daftranpulang" name="formgo_daftranpulang" >
<input type="hidden" id="formgo_stattransitpergi" name="formgo_stattransitpergi" >
<input type="hidden" id="formgo_stattransitpulang" name="formgo_stattransitpulang" >
<input type="hidden" id="formgo_dafsubclasspergi" name="formgo_dafsubclasspergi" >
<input type="hidden" id="formgo_dafsubclasspulang" name="formgo_dafsubclasspulang" >
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">Cari Tiket Pesawat</li>
    </ul>
    @if($otomatiscari==1)
    <h3 class="booking-title">Pilihan penerbangan dari {{airport_namakota($org)}} ke {{airport_namakota($des)}} {{statustravel($flight)}}</h3>
    @endif
    <div class="row">
        <div class="col-md-3" id="sebelahkiri">
            <form class="booking-item-dates-change mb30">
              <div class="form-group">
                  <label>Perjalanan</label>
                  <ul class="nav nav-pills nav-sm nav-no-br mb10" >
                      <li class="<?php if($otomatiscari==1 && $flight=="O"){print("active");} if($otomatiscari==0){print("active");}?>"><a href="#flight-search-1" data-toggle="tab" onclick="pilihrencana('O')">Sekali jalan</a>
                      </li>
                      <li class="<?php if($otomatiscari==1 && $flight=="R"){print("active");} ?>"><a href="#flight-search-2" data-toggle="tab" onclick="pilihrencana('R')">Pulang pergi</a>
                      </li>
                  </ul>
              </div>
                <div class="form-group">
                    <label>Asal</label>
                    <select style="width:100%" class="selektwo full-width" name="pilasal" id="pilasal">
                      <option value=""></option>
                      @foreach($dafban as $area)
                     <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $org==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
                     @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Tujuan</label>
                    <select style="width:100%"  class="full-width selektwo" name="tujuan" id="piltujuan">
                       <option value=""></option>
                       @foreach($dafban as $area)
                      <option value="{{ $area->nama }}-{{ $area->kode }}" <?php if($otomatiscari==1 && $des==$area->kode){print("selected=\"selected\"");} ?>>{{ $area->nama }} - {{ $area->kode }}</option>
                      @endforeach
                    </select>
                </div>
                <div class="input-daterange">
                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                        <label>Tanggal berangkat</label>
                        <input id="tglberangkat" onchange="settglberangkat(this.value);" name="tglberangkat"  value="<?php if($otomatiscari==1 && isset($tglberangkat)){print(date("d-m-Y",strtotime($tglberangkat)));}else{echo date("d-m-Y");} ?>" class="form-control" type="text" />
                    </div>
                    <div class="form-group form-group-icon-left" id="tny_ret" ><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                        <label>Tanggal pulang</label>
                        <input  id="tglpulang" onchange="settglpulang(this.value);" name="tglpulang"  value="<?php if($otomatiscari==1 && isset($tglpulang)){print(date("d-m-Y",strtotime($tglpulang)));}else{echo date("d-m-Y");} ?>" class="form-control" type="text" />
                    </div>
                </div>

                <div class="form-group  ">
                  <label>Dewasa</label>
                  <select class="full-width" id="adt"  name="adt" >
                    <?php for($op=1; $op<=7;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group  ">
                  <label>Anak</label>
                  <select class="full-width" id="chd" name="chd"  >

                    <?php for($op=0; $op<=6;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select>
                </div>
                <div class="form-group  ">
                  <label>Bayi</label>
                  <select class="full-width" id="inf" name="inf"  >

                      <?php for($op=0; $op<=6;$op++){
                        print("<option value=\"$op\"");
                        if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
                        print(">$op</option>");
                      }
                      ?>

                  </select>
                </div>


                <input class="btn btn-primary" type="button" onclick="cari()" value="Cari" />
            </form>


        </div>
        <div class="col-md-9">
          <div class="row">
              <div class="col-md-6 text-left">
                  <p><a  style="cursor:pointer" data-effect="mfp-zoom-out" id="tomback">Ulangi pencarian</a>
                  </p>
              </div>
          </div>
          <div id="diverror">
          @if($errors->has())
          @foreach ($errors->all() as $error)
          <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong>{{ $error }}
          </div>
          @endforeach
          @endif
          </div>
            <div class="row" id="dafloading" >
              <div class="nav-drop booking-sort" id="loading_qz">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-QZ.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_qg">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-QG.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_ga">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-GA.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_kd">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KD.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_jt">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-JT.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_sj">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-SJ.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_mv">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-MV.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
              <div class="nav-drop booking-sort" id="loading_il">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-IL.png')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
            </div>
            <div id="titikup"></div>
            <div id="divterpilih">
              <div class="booking-list" >
                <div class="page-title-container" id="labelpilihanpergi">
                <h4 id="labelkolomutama" >Pilihan Penerbangan Berangkat</h4>
                </div>
                    <li id="pilihanpergi">
                        <div class="booking-item-container">
                            <div class="booking-item">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-airline-logo">
                                            <img id="pilihanpergi_gbr"  src="img/croatia.jpg"  />
                                            <p id="pilihanpergi_pesawal">Croatia Airlines</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-flight-details">
                                            <div class="booking-item-departure"><i class="fa fa-plane"></i>
                                                <h5  id="pilihanpergi_jampergi">10:25 PM</h5>
                                            </div>
                                            <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
                                                <h5 id="pilihanpergi_jamtiba">12:25 PM</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h5  id="pilihanpergi_totalwaktu">22h 50m</h5>

                                    </div>
                                    <div class="col-md-3"><h5 id="pilihanpergi_harga">$270</h5>
                                     <a class="btn btn-primary"  id="tomubahpilihanpergi">Ubah</a>
                                    </div>
                                </div>
                            </div>
                            <div class="booking-item-details" id="pilihanpergi_detail">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Flight Details</p>
                                        <h5 class="list-title">London (LHR) to Charlotte (CLT)</h5>
                                        <ul class="list">
                                            <li>US Airways 731</li>
                                            <li>Economy / Coach Class ( M), AIRBUS INDUSTRIE A330-300</li>
                                            <li>Depart 09:55 Arrive 15:10</li>
                                            <li>Duration: 9h 15m</li>
                                        </ul>
                                        <h5>Stopover: Charlotte (CLT) 7h 1m</h5>
                                        <h5 class="list-title">Charlotte (CLT) to New York (JFK)</h5>
                                        <ul class="list">
                                            <li>US Airways 1873</li>
                                            <li>Economy / Coach Class ( M), Airbus A321</li>
                                            <li>Depart 22:11 Arrive 23:53</li>
                                            <li>Duration: 1h 42m</li>
                                        </ul>
                                        <p>Total trip time: 17h 58m</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <div class="page-title-container" id="labelpilihanpulang">
                    <h4 id="labelkolomkedua" >Pilihan Penerbangan Pulang</h4>
                    </div>
                        <li id="pilihanpulang">
                            <div class="booking-item-container">
                                <div class="booking-item">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="booking-item-airline-logo">
                                                <img id="pilihanpulang_gbr"  src="img/croatia.jpg"  />
                                                <p id="pilihanpulang_pesawal">Croatia Airlines</p>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><i class="fa fa-plane"></i>
                                                    <h5  id="pilihanpulang_jampergi">10:25 PM</h5>
                                                </div>
                                                <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
                                                    <h5 id="pilihanpulang_jamtiba">12:25 PM</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <h5  id="pilihanpulang_totalwaktu">22h 50m</h5>

                                        </div>
                                        <div class="col-md-3"><h5 id="pilihanpulang_harga">$270</h5>
                                         <a class="btn btn-primary"  id="tomubahpilihanpulang">Ubah</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="booking-item-details" id="pilihanpulang_detail">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p>Flight Details</p>
                                            <h5 class="list-title">London (LHR) to Charlotte (CLT)</h5>
                                            <ul class="list">
                                                <li>US Airways 731</li>
                                                <li>Economy / Coach Class ( M), AIRBUS INDUSTRIE A330-300</li>
                                                <li>Depart 09:55 Arrive 15:10</li>
                                                <li>Duration: 9h 15m</li>
                                            </ul>
                                            <h5>Stopover: Charlotte (CLT) 7h 1m</h5>
                                            <h5 class="list-title">Charlotte (CLT) to New York (JFK)</h5>
                                            <ul class="list">
                                                <li>US Airways 1873</li>
                                                <li>Economy / Coach Class ( M), Airbus A321</li>
                                                <li>Depart 22:11 Arrive 23:53</li>
                                                <li>Duration: 1h 42m</li>
                                            </ul>
                                            <p>Total trip time: 17h 58m</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
              </div>
            </div>
            <div id="barissorting" class="nav-drop booking-sort">
                <h5 class="booking-sort-title"><a href="#" id="labelurut">Urut berdasarkan: -<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                <ul class="nav-drop-menu">
                    <li><a href="#" onclick="sortAsc('data-harga',' Harga terendah')">Harga terendah</a>
                    </li>
                    <li><a href="#" onclick="sortDesc('data-harga',' Harga tertinggi')">Harga tertinggi</a>
                    </li>
                    <li><a href="#" onclick="sortAsc('data-waktu',' Penerbangan terdekat')">Penerbangan terdekat</a>
                    </li>
                    <li><a href="#" onclick="sortDesc('data-waktu',' Penerbangan terakhir')">Penerbangan terakhir</a>
                    </li>
                </ul>
            </div>
            <div id="sisikiri">
              <div class="booking-list" >
                <h4>Pilihan penerbangan pergi</h4>
              </div>
            <ul class="booking-list"  id="dafpergi">

            </ul>
          </div>
          <div id="sisikanan">
            <div class="booking-list" >
              <h4>Pilihan penerbangan pulang</h4>
            </div>
            <ul class="booking-list"  id="dafpulang">

            </ul>
          </div>
            <div  id="divpilihnextorulang">
                  <button  class="full-width btn btn-primary" id="tomgoone" style="margin-top:10px;">Lanjutkan Transaksi</button>


            </div>

        </div>
    </div>
    <div class="gap"></div>
</div>

				@section('akhirbody')
        <script type="text/javascript">

        $("#labelpilihanpulang").hide();
        $("#sisikanan").hide();
        $("#barissorting").hide();
        $("#sisikiri").hide();

        //variabel

       var hargapulang="0";
       var hargapergi="0";
        var nilpil=0;
        var satuaja=0;
        var idpilper="";
        var idpilret="";
       var banasal="CGK";
       var kotasal="Jakarta";
       var labelasal="";
       var kottujuan="Manokwari";
       var bantujuan="MKW";
       var labeltujuan="";
       var pilrencana="O";
       var pilrencana2="O";
       var tglber_d="";
       var tglber_m="";
       var tglber_y="";

       var tglpul_d="";
       var tglpul_m="";
       var tglpul_y="";

       function bedawaktu (wawal,wakhir){

         var startDate = new Date(wawal);
         var endDate = new Date(wakhir);
         var startminute = Math.floor(startDate.getMinutes());
         var endminute = Math.floor(endDate.getMinutes());
         var diffMnt= Math.floor(endminute-startminute);
         if(diffMnt<0){
         diffMnt= Math.floor(startminute-endminute);
         }
  var hourDiff = endDate - startDate;
  var diffHrs = Math.floor((hourDiff % 86400000) / 3600000);

  var starthour = Math.floor(startDate.getHours());
  var endhour = Math.floor(endDate.getHours());

      alert('Hours diff:' + diffHrs +' hours and '+diffMnt+' minutes');

       }
       var tglberangkat="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_dep').val(tglberangkat);
        $('#formgo_tgl_deppilihan').val(tglberangkat);
       var tglpulang="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_ret').val(tglpulang);
        $('#formgo_tgl_retpilihan').val(tglpulang);

        @if($otomatiscari==1)
        $('#formgo_tgl_deppilihan').val("{{$tglberangkat}}");
        $('#formgo_tgl_retpilihan').val("{{$tglpulang}}");
        $('#formgo_flight').val("{{$flight}}");
        $('#formgo_org').val("{{$org}}");
        $('#formgo_des').val("{{$des}}");
        $('#formgo_kotorg').val("{{$kotasal}}");
        $('#formgo_labelorg').val("{{$kotasal}} ({{$org}})");
        $('#formgo_kotdes').val("{{$kottujuan}}");
        $('#formgo_labeldes').val("{{$kottujuan}} ({{$des}})");
        $('#formgo_chd').val("{{$jumchd}}");
        $('#formgo_adt').val("{{$jumadt}}");
        $('#formgo_inf').val("{{$juminf}}");
        @endif

       var jumadt="1";
       var jumchd="0";
       var juminf="0";



       var urljadwal="{{ url('/') }}/jadwalPesawat/";
       var urljadwalb=urljadwal;

        var dafHTMLPergi = [];
        var dafHTMLPulang = [];

       $('#tomback').hide();
       $('#divpilihnextorulang').hide();
       $('#hsubclasspergi').hide();
       $('#hsubclasspulang').hide();
       $('#tomulangipencarian').hide('slow');


        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#divpilihnextorulang').hide();
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
          $( "#formgo" ).submit();
        });

       $('#tomulangipencarian').on('click', function() {


       $('#formatas').show('slow');

       //$('#labelpilihanpergi').hide('slow');
       $('#tomulangipencarian').hide('slow');

       });
       $('#adt').on('change', function() {
       jumadt=this.value;
        $('#formgo_adt').val(jumadt);

       });
       $('#chd').on('change', function() {
       jumchd=this.value;
        $('#formgo_chd').val(jumchd);

       });
       $('#inf').on('change', function() {
       juminf=this.value;
        $('#formgo_inf').val(juminf);

       });


       $('#labelpilihanpergi').hide();
       $('#pilihanpergi').hide();
       $('#pilihanpulang').hide();
         $('.selektwo').select2();
          <?php if(($otomatiscari==1 && $flight=="O")||$otomatiscari==0){?>
         $('#tny_ret').hide();
          <?php }?>
                         $('#loading_qz').hide();
                         $('#loading_qg').hide();
                         $('#loading_ga').hide();
                         $('#loading_kd').hide();
                         $('#loading_jt').hide();
                         $('#loading_sj').hide();
                         $('#loading_mv').hide();
                         $('#loading_il').hide();
                         $("#tglberangkat").datepicker({
                           format: 'dd-mm-yyyy',
                           startDate: '+0d',
                           autoclose: true,
                     });

                     $("#tglpulang").datepicker({
                       format: 'dd-mm-yyyy',
                       startDate: '+0d',
                       autoclose: true,
                     });
       function settglberangkat(val) {

       var values=val.split('-');
       //alert(this.value);
       tglber_d=values[0];
       tglber_m=values[1];
       tglber_y=values[2];

       tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

       $('#tglberangkat').val(tglber_d+"-"+tglber_m+"-"+tglber_y);
        $('#formgo_tgl_deppilihan').val(tglberangkat);

        $('#tglpulang').data('datepicker').setStartDate(new Date(tglberangkat));
        var x = new Date(tglberangkat);
        var y = new Date(tglpulang);
        if(x>y){
        $('#tglpulang').val(val);
        setulangtglpulang(val);
        }

      }

       function setulangtglpulang(val){

             var values=val.split('-');
             //alert(val);
             tglpul_d=values[0];
             tglpul_m=values[1];
             tglpul_y=values[2];

             tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
             $('#tglpulang').val(tglpul_d+"-"+tglpul_m+"-"+tglpul_y);
              $('#formgo_tgl_retpilihan').val(tglpulang);

       }

       function settglpulang(val){
            setulangtglpulang(val);
       }

       $('#pilasal').on('change', function() {
         var values=this.value.split('-');
         var kotas=values[1];
         kotasal=values[0];
         //alert(kotas);
         banasal=kotas;
         labelasal=this.value;
          $('#formgo_labelorg').val(labelasal);
          $('#formgo_org').val(banasal);
          $('#formgo_kotorg').val(kotasal);

       });
       $('#piltujuan').on('change', function() {
         var values=this.value.split('-');
         var kottuj=values[1];
         kottujuan=values[0];
         //alert(kottuj);
         bantujuan=kottuj;
         labeltujuan=this.value;
          $('#formgo_labeldes').val(labeltujuan);
          $('#formgo_des').val(kottuj);
          $('#formgo_kotdes').val(kottujuan);

       });
       $('#pilrencana').on('change', function() {
         //alert( this.value );
         pilrencana=this.value;
          $('#formgo_flight').val(pilrencana);
         var tny_ret = document.getElementById("tny_ret").value;

       if(this.value=="O"){
           $('#tny_ret').hide();
         }else{
           $('#tny_ret').show();
         }


       });

       function pilihrencana(val){
         if(val=="O"){
             $('#tny_ret').hide();
           }else{
             $('#tny_ret').show();
           }
           pilrencana=val;
            $('#formgo_flight').val(pilrencana);
       }

       function setkolom(){
       labelutama();
       //  $('#labelkolomkiri').html(kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+")");
       if(pilrencana=="R"){
       //  $('#labelkolomkanan').html(kottujuan+" ("+bantujuan+") - "+kotasal+" ("+banasal+")");
       }
       }
       function labelutama(){
         $('#labelkolomutama').html('Penerbangan '+kotasal+" ke "+kottujuan);
       }

    function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
    }

    $('#pilsubclasspergi').on('change', function() {
      hargapergi=this.value.split(",")[0];
      var totaladt=hargapergi*jumadt;
      var totalchd=hargapergi*jumchd;
      var totalinf=hargapergi*juminf;
      $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
      totalhargapergi=totaladt+totalchd+totalinf;
      $('#pilihanpergi_harga').html("IDR "+convertToRupiah(totalhargapergi));


    });
    $('#pilsubclasspulang').on('change', function() {
      hargapulang=this.value.split(",")[0];
      var totaladt=hargapulang*jumadt;
      var totalchd=hargapulang*jumchd;
      var totalinf=hargapulang*juminf;
      $('#formgo_selectedIDret').val(this.value.split(",")[1]);
      totalhargapulang=totaladt+totalchd+totalinf;
      $('#pilihanpulang_harga').html("IDR "+convertToRupiah(totalhargapulang));


    });

       function pilihPergi(bahanid
         ,daftranpergi
         ,pesawatawal
         ,jampergiberangkat
         ,jampergitiba
         ,harga
         ,ikon
         ,stattransitpergi
         ,dafsubclasspergi
         ,kumpulaniddep
         ,hasildiskon
         ,maskapai
         ){
           //alert(bedawaktu("01-11-2016 18:50 ","01-11-2016 20:45 "));

            var totaladt=harga*jumadt;
            var totalchd=harga*jumchd;
            var totalinf=harga*juminf;
            //harga=totaladt+totalchd+totalinf;

            var totaladtdiskon=hasildiskon*jumadt;
            var totalchddiskon=hasildiskon*jumchd;
            var totalinfdiskon=hasildiskon*juminf;
            //hasildiskon=totaladtdiskon+totalchddiskon+totalinfdiskon;


             var subclasses=dafsubclasspergi.split('#');
              $('#formgo_selectedIDdep').val(kumpulaniddep);
              $('#pilsubclasspergi').find('option').remove();


                $.each(subclasses, function(key, value) {
               var subclassesisi=value.split(',');
              if(subclassesisi[3]!=null){
              $('#pilsubclasspergi')
             .append($("<option></option>")
                        .attr("value",subclassesisi[2]+","+subclassesisi[3])
                        .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                      }
               });


        $('#formgo_tgl_dep').val(jampergiberangkat);
        $('#formgo_tgl_dep_tiba').val(jampergitiba);
        $('#formgo_dafsubclasspergi').val(dafsubclasspergi);
        $('#formgo_stattransitpergi').val(stattransitpergi);
        $('#formgo_daftranpergi').val(daftranpergi);
       $('#formgo_acDep').val(ikon);
        $("#"+idpilper).removeClass("kotakpilihb cell-view");
        $("#"+idpilper).addClass("kotakpilih cell-view");

       $("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
    idpilper=bahanid+"_gbr";

       $('#labelpilihanpergi').show('slow');
      $('#labelkolomutama').show('slow');
      $('#divterpilih').show('slow');
       $('#pilihanpergi').show('slow');
       //  alert(jampergitiba);
               $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
               //  alert($('#pilihanpergi_gbr').attr('src'));
               $('#pilihanpergi_dafpes').html(kotasal+" ("+banasal+") - "+kottujuan+" ("+bantujuan+")"+' : '+daftranpergi+" ("+stattransitpergi+")");
               $('#pilihanpergi_pesawal').html(maskapai+" "+pesawatawal);
               $('#pilihanpergi_jampergi').html(jampergiberangkat);
               $('#pilihanpergi_jamtiba').html(jampergitiba);
               $('#pilihanpergi_harga').html("Rp "+convertToRupiah(hasildiskon));
               $('#pilihanpergi_detail').html($('#'+bahanid+'_detailtransit').html());

                nilpil+=1;
                $(location).attr('href', '#labelkolomutama');

               $('#pilihanpergi_totalwaktu').html($('#'+bahanid+'_totalwaktu').html());

              //  alert ('tes tes');
                  $( "#labelkolomutama" ).focusin();
                if(pilrencana2=="O" || (pilrencana2=="R" && satuaja==1)){
                 $('#sisikiri').hide('slow');
                  $('#barissorting').hide('slow');
                 mintaformgo();
               }else if(pilrencana2=="R" && satuaja==0){
                 mintaformret();
               }
               satuaja=0;

       }
       $('#tomubahpilihanpergi').on('click', function() {
       satuaja=1;
       $('#divterpilih').hide('slow');
       $('#sisikiri').show('slow');
       $('#barissorting').show('slow');
       });
       $('#tomubahpilihanpulang').on('click', function() {
       $('#sisikanan').show('slow');
       $('#divterpilih').hide('slow');
       $('#barissorting').show('slow');
       });
       $('#tomback').on('click', function() {
       $('#tomback').hide('slow');
       $('#sebelahkiri').show('slow');
       });
       $('.tomup').on('click', function() {
         $('#titikup')[0].scrollIntoView(true);
       });
       function pilihPulang(bahanid
         ,daftranpulang
         ,pesawatawal
         ,jampulangberangkat
         ,jampulangtiba
         ,harga
         ,ikon
         ,stattransitpulang
         ,dafsubclasspulang
         ,kumpulanidret
         ,hasildiskon
         ,maskapai
         ){

                    var totaladt=harga*jumadt;
                    var totalchd=harga*jumchd;
                    var totalinf=harga*juminf;
                    //harga=totaladt+totalchd+totalinf;

                    var totaladtdiskon=hasildiskon*jumadt;
                    var totalchddiskon=hasildiskon*jumchd;
                    var totalinfdiskon=hasildiskon*juminf;
                    //hasildiskon=totaladtdiskon+totalchddiskon+totalinfdiskon;

            var subclasses=dafsubclasspulang.split('#');
            $('#formgo_selectedIDret').val(kumpulanidret);
            $('#pilsubclasspulang').find('option').remove();


                        $.each(subclasses, function(key, value) {
                       var subclassesisi=value.split(',');
                      if(subclassesisi[3]!=null){
                 $('#pilsubclasspulang')
                     .append($("<option></option>")
                                .attr("value",subclassesisi[2]+","+subclassesisi[3])
                                .text(subclassesisi[0]+" - Rp "+convertToRupiah(subclassesisi[2])));
                              }
            });

            $('#formgo_tgl_ret').val(jampulangberangkat);
            $('#formgo_tgl_ret_tiba').val(jampulangtiba);
            $('#formgo_dafsubclasspulang').val(dafsubclasspulang);
            $('#formgo_stattransitpulang').val(stattransitpulang);
            $('#formgo_daftranpulang').val(daftranpulang);
            $('#formgo_acRet').val(ikon);

            $("#"+idpilret).removeClass("kotakpilihb cell-view");
            $("#"+idpilret).addClass("kotakpilih cell-view");

           $("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
            $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
            idpilret=bahanid+"_gbr";

       $('#barissorting').hide('slow');
       $('#labelpilihanpergi').show('slow');
      $('#labelkolomutama').show('slow');
         $('#pilihanpulang').show('slow');
       //  alert(jampergitiba);

                $("#labelpilihanpulang").show('slow');
                $("#labelkolomkedua").html('Penerbangan '+kottujuan+" ke "+kotasal);
               $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
               //  alert($('#pilihanpergi_gbr').attr('src'));
                $('#pilihanpulang_pesawal').html(maskapai+" "+pesawatawal);
               $('#pilihanpulang_dafpes').html(kottujuan+" ("+bantujuan+") - "+kotasal+" ("+banasal+")"+' : '+daftranpulang+" ("+stattransitpulang+")");
               $('#pilihanpulang_jampergi').html(jampulangberangkat);
               $('#pilihanpulang_jamtiba').html(jampulangtiba);
               $('#pilihanpulang_harga').html("Rp "+convertToRupiah(hasildiskon));
               $('#pilihanpulang_detail').html($('#'+bahanid+'_detailtransit').html());

               $('#divterpilih').show('slow');

                    mintaformgo();
                    satuaja=0;
                $('#pilihanpulang_totalwaktu').html($('#'+bahanid+'_totalwaktu').html());

              $(location).attr('href', '#labelkolomutama');

              $("#sisikanan").hide('slow');
        }
        function mintaformgo(){
          $('#divpilihnextorulang').show('slow');
        }
        function mintaformret(){
           $("#sisikiri").hide('slow');
           $("#sisikanan").show('slow');
           $('#divpilihnextorulang').hide();
        }

       function sembunyi(){
             $('#loadmaskapai').hide('slow');
       }
       function cari(){
        nilpil=0;
        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          if (isMobile.matches) {
            $("#tomback").show('slow');
            $("#sebelahkiri").hide('slow');
            //$('#titikup')[0].scrollIntoView(true);
          }

    $("#barissorting").show('slow');
        $('#sisikanan').hide();
        $('#divpilihnextorulang').hide();
       //  alert("Memulai pencarian");
               //  $('#hasilkirim').html("<b>Hello world!</b>");
                if(pilrencana=="O"){
                    $("#sisikanan").hide('slow');
                    $("#sisikiri").show('slow');
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
                 }else{
                    $("#sisikiri").show('slow');
                    //$("#sisikanan").show('slow');
                      //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
                 }
               urljadwalb=urljadwal;
               urljadwalb+="org/"+banasal;
               urljadwalb+="/des/"+bantujuan;
               urljadwalb+="/flight/"+pilrencana;
               urljadwalb+="/tglberangkat/"+tglberangkat;
               urljadwalb+="/tglpulang/"+tglpulang;
               urljadwalb+="/jumadt/"+jumadt;
               urljadwalb+="/jumchd/"+jumchd;
               urljadwalb+="/juminf/"+juminf;
               //akhir urljadwalb+="ac/";
                       $('#loading_qz').show('fadeOut');
                       $('#loading_qg').show('fadeOut');
                       $('#loading_ga').show('fadeOut');
                       $('#loading_kd').show('fadeOut');
                       $('#loading_jt').show('fadeOut');
                       $('#loading_sj').show('fadeOut');
                       $('#loading_mv').show('fadeOut');
                       $('#loading_il').show('fadeOut');

                       $('#dafpergi').html('');
                       $('#dafpulang').html('');
                       urljadwalb+="/ac/";

               ambildata_qz();
               ambildata_qg();
               ambildata_ga();
               ambildata_kd();
               ambildata_jt();
               ambildata_sj();
               ambildata_mv();
               ambildata_il();

                pilrencana2=pilrencana;
        $('#formatas').hide('slow');
              $('#pilihanpergi').hide();
               $('#pilihanpulang').hide();

                       $('#labelkolomutama').hide('slow');
             $('#labelpilihanpergi').hide('slow');
             $('#tomulangipencarian').show('slow');


             //  alert(urljadwalb);
             setkolom();
            }else{
              //$('#bahanmodal').dialog({ modal: true });
              // $( "#bahanmodal" ).show();
             alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
            }
       }
       var ajaxku_il;
       function ambildata_il(){
         ajaxku_il = buatajax();
         var url=urljadwalb+"IL";
         ajaxku_il.onreadystatechange=stateChanged_il;
         ajaxku_il.open("GET",url,true);
         ajaxku_il.send(null);
       }

       function stateChanged_il(){
          var data;
           if (ajaxku_il.readyState==4){
             data=ajaxku_il.responseText;
             if(data.length>0){
               tambahData(data);
              }else{
              }
            $('#loading_il').hide('slow');
           }
       }

       var ajaxku_mv;
       function ambildata_mv(){
         ajaxku_mv = buatajax();
         var url=urljadwalb+"MV";
         ajaxku_mv.onreadystatechange=stateChanged_mv;
         ajaxku_mv.open("GET",url,true);
         ajaxku_mv.send(null);
       }

        function stateChanged_mv(){
          var data;
           if (ajaxku_mv.readyState==4){
             data=ajaxku_mv.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_mv').hide('slow');
            }
       }

       var ajaxku_sj;
       function ambildata_sj(){
         ajaxku_sj = buatajax();
         var url=urljadwalb+"SJ";
         ajaxku_sj.onreadystatechange=stateChanged_sj;
         ajaxku_sj.open("GET",url,true);
         ajaxku_sj.send(null);
       }

        function stateChanged_sj(){
          var data;
           if (ajaxku_sj.readyState==4){
             data=ajaxku_sj.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_sj').hide('slow');
            }
       }

       var ajaxku_jt;
       function ambildata_jt(){
         ajaxku_jt = buatajax();
         var url=urljadwalb+"JT";
         ajaxku_jt.onreadystatechange=stateChanged_jt;
         ajaxku_jt.open("GET",url,true);
         ajaxku_jt.send(null);
       }

        function stateChanged_jt(){
          var data;
           if (ajaxku_jt.readyState==4){
             data=ajaxku_jt.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_jt').hide('slow');
            }
       }

       var ajaxku_kd;
       function ambildata_kd(){
         ajaxku_kd = buatajax();
         var url=urljadwalb+"KD";
         ajaxku_kd.onreadystatechange=stateChanged_kd;
         ajaxku_kd.open("GET",url,true);
         ajaxku_kd.send(null);
       }

        function stateChanged_kd(){
          var data;
           if (ajaxku_kd.readyState==4){
             data=ajaxku_kd.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_kd').hide('slow');
            }
       }


       var ajaxku_ga;
       function ambildata_ga(){
         ajaxku_ga = buatajax();
         var url=urljadwalb+"GA";
         ajaxku_ga.onreadystatechange=stateChanged_ga;
         ajaxku_ga.open("GET",url,true);
         ajaxku_ga.send(null);
       }

        function stateChanged_ga(){
          var data;
           if (ajaxku_ga.readyState==4){
             data=ajaxku_ga.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_ga').hide('slow');
            }
       }

       var ajaxku_qz;
       function ambildata_qz(){
         ajaxku_qz = buatajax();
         var url=urljadwalb+"QZ";
         ajaxku_qz.onreadystatechange=stateChanged_qz;
         ajaxku_qz.open("GET",url,true);
         ajaxku_qz.send(null);
       }
        function stateChanged_qz(){
          var data;
           if (ajaxku_qz.readyState==4){
             data=ajaxku_qz.responseText;
             if(data.length>0){
                       tambahData(data);
              }else{
              }
                             $('#loading_qz').hide('slow');
            }
       }


       var ajaxku_qg;
       function ambildata_qg(){
         ajaxku_qg = buatajax();
         var url=urljadwalb+"QG";
         ajaxku_qg.onreadystatechange=stateChanged_qg;
         ajaxku_qg.open("GET",url,true);
         ajaxku_qg.send(null);
       }
        function stateChanged_qg(){
          var data;
           if (ajaxku_qg.readyState==4){
             data=ajaxku_qg.responseText;
             if(data.length>0){
               //document.getElementById("hasilkirim").html = data;

                       tambahData(data);
              }else{
              }
                             $('#loading_qg').hide('slow');
            }
       }

        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom,isi){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });


          var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpulang').append(value);
          });
          $("#labelurut").html("Urut berdasarkan:"+isi);
        }

        function sortDesc(kolom,isi){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });


            var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpulang').append(value);
            });
            $("#labelurut").html("Urut berdasarkan:"+isi);
          }

       function tambahData(data){

                         //$('#dafpergi').html(data+$('#dafpergi').html());
                         //$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
                         //$('.bagpulang').remove();
                          $(data).each(function(){
                            if($(this).attr('isiitem')=="1"){
                            if($(this).attr('untuk')=="pergi"){
                            var temp_id=$(this).attr('id');
                            var biaya=$(this).attr('data-harga');
                            var waktu=$(this).attr('data-waktu');

                            var isiHTML='<li class="isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</li>';
                            //alert(waktu);
                            var valueToPush = new Array();
                            valueToPush[0] = temp_id;
                            valueToPush[1] = biaya;
                            valueToPush[2] = waktu;
                            valueToPush[3] = isiHTML;
                            //dafHTMLPergi.push(valueToPush);
                            $('#dafpergi').html(isiHTML+$('#dafpergi').html());


                            }else if($(this).attr('untuk')=="pulang"){
                            var temp_id=$(this).attr('id');
                            var biaya=$(this).attr('data-harga');
                            var waktu=$(this).attr('data-waktu');

                              var isiHTML='<li class="isiitempulang isiitem" id="'+temp_id+'" data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</li>';
                              //alert(temp_id);

                            var valueToPush = new Array();
                            valueToPush[0] = temp_id;
                            valueToPush[1] = biaya;
                            valueToPush[2] = waktu;
                            valueToPush[3] = isiHTML;
                            //dafHTMLPulang.push(valueToPush);
                            $('#dafpulang').html(isiHTML+$('#dafpulang').html());


                          }
                        }});
       }

       var ajaxku;
       function ambildata(){
         ajaxku = buatajax();
         var url="{{ url('/') }}/jadwalPesawat";
         //url=url+"?q="+nip;
         //url=url+"&sid="+Math.random();
         ajaxku.onreadystatechange=stateChanged;
         ajaxku.open("GET",url,true);
         ajaxku.send(null);
       }
       function buatajax(){
         if (window.XMLHttpRequest){
           return new XMLHttpRequest();
         }
         if (window.ActiveXObject){
            return new ActiveXObject("Microsoft.XMLHTTP");
          }
          return null;
        }
        function stateChanged(){
          var data;
           if (ajaxku.readyState==4){
             data=ajaxku.responseText;
             if(data.length>0){
               //document.getElementById("hasilkirim").html = data;

                       $('#loading_qz').hide('show');
                       $('#hasilkirim').append(data);
              }else{
               // document.getElementById("hasilkirim").html = "";
                     //   $('#hasilkirim').html("");
              }
            }
       }

        @if($otomatiscari==1)
        banasal="{{$org}}";
        bantujuan="{{$des}}";
        pilrencana="{{$flight}}";
        tglberangkat="{{$tglberangkat}}";
        tglpulang="{{$tglpulang}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        juminf="{{$juminf}}";
        kotasal="{{$kotasal}}";
        kottujuan="{{$kottujuan}}";
        cari();
        @endif
       </script>
		@endsection

		@endsection
