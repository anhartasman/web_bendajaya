@extends('layouts.'.$namatemplate)
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/flight">penerbangan</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">thank you</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
				<div class="detail-content-block">
					<h3 class="small-title">Status Pesanan</h3>
					<div class="confirm-label bg-dr-blue-2 radius-5">
						<img class="confirm-img" src="img/thx_icon.png" alt="">
						<div class="confirm-title color-white">Terimakasih. Pesanan Anda sudah tercatat.</div>
						<div class="confirm-text color-white-light">Informasi tagihan sudah dikirim ke alamat email Anda.</div>
						<a href="#" class="confirm-print c-button b-40 bg-white hv-white-o">print details</a>
					</div>
				</div>
				<div class="detail-content-block">
					<h3 class="small-title">Informasi Tagihan</h3>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
						        <tr>
						          	<td class="table-label color-grey">NOMOR TRANSAKSI</td>
						          	<td class="table-label color-dark-2"><strong>{{$notrx}}</strong></td>
						        </tr>
										<tr>
						          	<td class="table-label color-grey">TOTAL TAGIHAN</td>
						          	<td class="table-label color-dark-2"><strong>{{$labelbiaya}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-grey">NAMA:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cpname}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-grey">EMAIL:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cpmail}}</strong></td>
					        	</tr>
						        <tr>
						          	<td class="table-label color-grey">NOMOR TELEPHONE:</td>
						          	<td class="table-label color-dark-2"><strong>{{$cptlp}}</strong></td>
					        	</tr>

					      	</tbody>
					    </table>
				    </div>
				</div>
				@if($adt>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang dewasa</h3>
						<?php $jumadt=0; ?>
						@while($jumadt<$adt)
						<?php $jumadt++; ?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$datadewasa['titadt_'.$jumadt]}} {{$datadewasa['fnadt_'.$jumadt]}} {{$datadewasa['lnadt_'.$jumadt]}}  ({{$datadewasa['hpadt_'.$jumadt]}})
						</div>
						@endwhile

					</div>
				</div>
				@endif

				@if($chd>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang anak </h3>
						<?php $jumchd=0; ?>
						@while($jumchd<$chd)
						<?php $jumchd++; ?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$dataanak['titchd_'.$jumchd]}} {{$dataanak['fnchd_'.$jumchd]}} {{$dataanak['lnchd_'.$jumchd]}}  ({{$dataanak['birthchd_'.$jumchd]}})
						</div>
						@endwhile

					</div>
				</div>
				@endif

				@if($inf>0)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Daftar penumpang bayi</h3>
						<?php $juminf=0; ?>
						@while($juminf<$inf)
						<?php $juminf++; ?>
					 	<div class="custom-panel bg-grey-2 radius-4">
					{{$databayi['titinf_'.$juminf]}} {{$databayi['fninf_'.$juminf]}} {{$databayi['lninf_'.$juminf]}}  ({{$databayi['birthinf_'.$juminf]}})
						</div>
						@endwhile
					</div>
				</div>
				@endif

				<div class="detail-content-block">
					<div class="simple-text">
						<h3>link informasi tagihan</h3>
						<p class="color-grey">Gunakan link dibawah ini untuk memudahkan konfirmasi pembayaran</p>
						<div class="custom-panel bg-grey-2 radius-4">
							<a class="color-dr-blue-2 link-dark-2" href="{{ url('/') }}/flight/konfirmasi/{{$notrx}}">{{ url('/') }}/flight/konfirmasi/{{$notrx}}</a>
						</div>
					</div>
				</div>
       		</div>
       		<div class="col-xs-12 col-md-4">
       		<div class="right-sidebar">
						<!----
					<div class="sidebar-text-label bg-dr-blue-2 color-white">useful information</div>

					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">Need Help?</h4>
						<p class="color-grey-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a class="help-phone color-dark-2 link-dr-blue-2" href="tel:0200059600"><img src="img/detail/phone24-dark-2.png" alt="">020 00 59 600</a>
						<a class="help-mail color-dark-2 link-dr-blue-2" href="mailto:let’s_travel@world.com"><img src="img/detail/letter-dark-2.png" alt="">let’s_travel@world.com</a>
					</div>
				-->
       			</div>
       		</div>
       	</div>
	</div>
</div>


@endsection
