<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
@endsection
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  			<li><span class="color-red-3">Cek pesanan</span></li>
		  			</ul>
		  			<h2 class="color-white">Cek Pesanan</h2>
  				</div>
			</div>
		</div>
	</div>

</div>
<div class="detail-wrapper">
 <div class="container">
         <div class="row padd-90">
           <div class="col-xs-12 col-md-8">
       <form class="simple-from" role="form" method="get" action="cekpesanan" enctype = "multipart/form-data">

         <input type="hidden" id="jenis" name="jenis" value="AIR">
				 <input type="hidden" id="pdf" name="pdf" value="0">

				 @if($errors->has())
				 <div class="detail-content-block">
					 <div class="confirm-label bg-dr-blue-2 radius-5">
						 <img class="confirm-img" src="{{ URL::asset('asettemplate1/img/search_icon.png')}}" alt="">
						 <div class="confirm-title color-white">Nomor transaksi tidak ditemukan</div>
						 <div class="confirm-text color-white-light">Silahkan masukkan nomor yang benar</div>
					 </div>
				 </div>
				 @endif
         <div class="simple-group">
           <h3 class="small-title">Cek Pesanan</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Transaksi</div>
                 <div class="drop-wrap drop-wrap-s-4 color-5">
                   <div class="drop">
                    <b>Pilih transaksi</b>
                     <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                     <span>
                         <a href="#" onclick="settran('AIR')" >Pesawat</a>
                       <a href="#" onclick="settran('KAI')">Kereta</a>
                         <a href="#" onclick="settran('HTL')">Hotel</a>
                     </span>
                    </div>
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nomor transaksi</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" required="" id="notrx" name="notrx" placeholder="Masukkan nomor transaksi Anda">
                 </div>
               </div>
             </div>




           </div>

         </div>
         <input type="submit" onClick='$("#pdf").val(0);' class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="Lihat pesanan">
				 <input type="submit" onClick='$("#pdf").val(1);' class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="Cetak PDF">
       </form>
           </div>
           <div class="col-xs-12 col-md-4 nomobile">
             <div class="right-sidebar">

         <div class="sidebar-text-label bg-dr-blue-2 color-white">useful information</div>

         <div class="help-contact bg-grey-2">
           <h4 class="color-dark-2">Butuh bantuan?</h4>
           <p class="color-grey-2">Customer service kami siap melayani Anda </p>
           <a class="help-phone color-dark-2 link-dr-blue-2" href="tel:{{$infowebsite['handphone']}}"><img src="img/detail/phone24-dark-2.png" alt="">020 00 59 600</a>
           <a class="help-mail color-dark-2 link-dr-blue-2" href="mailto:{{$infowebsite['email']}}"><img src="img/detail/letter-dark-2.png" alt="">let’s_travel@world.com</a>
         </div>
             </div>
           </div>
         </div>
 </div>
</div>


				@section('akhirbody')
		<script type="text/javascript">
    function settran(val){
$('#jenis').val(val);
    }

		</script>
		@endsection

		@endsection
