@extends('layouts.'.$namatemplate)

@section('kontenweb')
<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Cek Pesanan</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Cek Pesanan</li>
				</ul>
		</div>
</div>

        <section id="content">
            <div class="container">
								<form method="get" action="cekpesanan" >
			 				 <input type="hidden" id="pdf" name="pdf" value="0">
                <div class="row">
                    <div id="main" class="travelo-box col-sm-8 col-md-12">
											<div class="col-md-3">
													<h4 class="title">Masukkan nomor transaksi</h4>
													<div class="form-group">
															<input type="text" required="" id="notrx" name="notrx"  class="input-text full-width" placeholder="Nomor transaksi" />
													</div>
											</div>

											<div class="col-md-3">
													<h4 class="title">Jenis transaksi</h4>
													<div class="form-group  ">
																	<div class="selector">
																			<select class="full-width" id="jenis" name="jenis" >
																					<option value="AIR">Pesawat</option>
																					<option value="KAI">Kereta</option>
																					<option value="HTL">Hotel</option>
																			</select>
																	</div>
													</div>
											</div>
											<div class="col-md-4">
												<h4 class="title">&nbsp;</h4>
												<div class="form-group row">
														<div class="col-xs-4">
																<div class="selector">
																		<button  onClick='$("#pdf").val(0);' >LIHAT PESANAN</button>
																</div>
														</div>
																<div class="col-xs-4">
																		<div class="selector">
																				<button onClick='$("#pdf").val(1);' >CETAK PDF</button>
																		</div>
																</div>
												</div>
											</div>

                    </div>
                </div>
								</form>
            </div>
        </section>
@endsection

@section('akhirbody')
<script type="text/javascript">

</script>
@endsection
