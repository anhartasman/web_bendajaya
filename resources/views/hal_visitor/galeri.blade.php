@extends('layouts.'.$namatemplate)

@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/gallery/bg_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><span>galeri</span></li>
		  			</ul>
		  			<h2 class="color-white">Galeri</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- TEAM -->
<div class="main-wraper padd-70-0">
    <div class="filter style-2">
        <ul class="filter-nav">
            <li class="selected"><a href="#all" data-filter="*">all</a></li>
@foreach($kategoris as $kategori)
<li><a href="#{{$kategori->category}}" data-filter=".{{$kategori->category}}">{{$kategori->category}}</a></li>
	@endforeach
		 </ul>
    </div>
	<div class="filter-content row">
		<div class="grid-sizer col-mob-12 col-xs-6 col-sm-3 no-padding"></div>
@foreach($fotos as $foto)
<div class="item @foreach($foto->dafkat as $kat) {{$kat->category}} @endforeach gal-item style-2 col-mob-12 col-xs-6 col-sm-3 no-padding">
		<a class="black-hover" href="{{ url('/') }}/galeri/view/{{$foto->id}}">
			<img class="img-full img-responsive" src="{{ url('/') }}/uploads/images/{{$foto->gambar}}" alt="">
			<div class="tour-layer delay-1"></div>
			<div class="vertical-align">
				<h3 class="color-white"><b>{{$foto->namafoto}}</b></h3>
				<!--<h5 class="color-white">{{$foto->namafoto}}</h5> -->
			</div>
		</a>
	</div>

		@endforeach


	</div>
</div>
@endsection
