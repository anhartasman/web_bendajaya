@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Galeri</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Galeri</li>
				</ul>
		</div>
</div>

<section id="content">
		<div class="container">
				<div id="main">
						<div class="gallery-filter box">
							@foreach($kategoris as $kategori)
							<a href="{{ url('/') }}/galeri/kategori/{{$kategori->category}}" class="button btn-medium">{{$kategori->category}}</a>
							@endforeach
						</div>
						<div class="items-container row add-clearfix image-box style9">
								 @foreach($fotos as $foto)
								<div class="col-sms-6 col-sm-6 col-md-3">
										<article class="box">
												<figure>
														<a class="hover-effect" title="" href="{{ url('/') }}/galeri/view/{{$foto->id}}"><img width="270" height="160" alt="" src="{{ url('/') }}/uploads/images/{{$foto->gambar}}"></a>
												</figure>
												<div class="details">
														<h4 class="box-title">{{$foto->namafoto}}<small>
															@foreach($foto->dafkat as $t )
															{{$t->category}},
															@endforeach</small></h4>
												</div>
										</article>
								</div>
										@endforeach
						</div>
						@include('hal_visitor.inc_pagesgaleri_template2', ['paginator' => $fotos])
				</div>
		</div>
</section>

@endsection
