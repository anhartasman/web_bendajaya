@extends('layouts.'.$namatemplate)

@section('kontenweb')


		<div class="container">

					<ul class="breadcrumb">
							<li><a href="{{url('/')}}">Home</a>
							</li>
							<li class="active">Galeri</li>
					</ul>

				<div class="container">
 

						<div class="gallery-filter box">
              <div class="form-group">
						<ul class="nav nav-pills nav-sm nav-no-br mb10" >
							<li class="<?php if($namakategori==""){print("active");} ?>">	<a data-toggle="tab" style="cursor:pointer;" onclick="$(location).attr('href','{{ url('/') }}/galeri')"  href="{{ url('/') }}/galeri" class="button btn-medium">Semua foto</a>
							</li>
							@foreach($kategoris as $kategori)
						  <li class="<?php if($kategori->category==$namakategori){print("active");} ?>">	<a data-toggle="tab" style="cursor:pointer;" onclick="$(location).attr('href','{{ url('/') }}/galeri/kategori/{{$kategori->category}}')"  href="{{ url('/') }}/galeri/kategori/{{$kategori->category}}" class="button btn-medium">{{$kategori->category}}</a>
							</li>@endforeach
							  </ul>
						</div>
				</div>
						<div class="row row-col-gap">
								 @foreach($fotos as $foto)
								<div class="col-md-3">
												<figure>
														<a class="hover-effect" title="" href="{{ url('/') }}/galeri/view/{{$foto->id}}"><img width="270" height="160" alt="" src="{{ url('/') }}/uploads/images/{{$foto->gambar}}"></a>
												</figure>
												<div class="details">
														<h4 class="box-title">{{$foto->namafoto}} </h4>
												</div>
								</div>
										@endforeach
						</div>
						@include('hal_visitor.inc_pagesgaleri_template3', ['paginator' => $fotos])
				</div>
		</div>


@endsection
