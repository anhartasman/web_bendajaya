@extends('layouts.'.$namatemplate)

@section('kontenweb')
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/gallery/bg_2.jpg') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-dr-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><a class="link-dr-blue-2" href="{{ url('/') }}/galeri">Galeri</a> /</li>
		  				<li><span>Lihat Foto</span></li>
		  			</ul>
		  			<h2 class="color-white">{{$nama}}</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- GALLERY-DETAIL -->
<div class="main-wraper padd-70-70">
	<div class="container">

		<div class="gallery-detail">

		    <div class="top-baner arrows">
		     	<div data-autoplay="0" data-loop="1" data-speed="1000" data-center="0" data-slides-per-view="1" id="tour-slide-2">
				    <div>

							<img class="img-responsive" src="{{ url('/') }}/uploads/images/{{$filegambar}}" alt="">

					</div>
					<div class="pagination pagination-hidden poin-style-1"></div>
				</div>
		        <div class="arrow-wrapp arr-s-1">

				</div>
		    </div>
			<div class="gd-content">
			<?php echo nl2br($keterangan); ?>	</div>
			<div class="photo-arrows clearfix">
			<?php if($idprev>0){ ?>
				<a href="{{ url('/') }}/galeri/view/{{$idprev}}" class="c-button b-50 photo-ar-left bg-dr-blue-2">
					prev image
					<img class="arrow-img" src="{{ URL::asset('asettemplate1/img/gallery/arrow-left.jpg') }}" alt="">
					<div class="photo-prev">
						<div class="photo-prev-title color-dark-2">{{$namaprev}}</div>
					</div>
				</a>
				<?php } if($idnext>0){ ?>
				<a href="{{ url('/') }}/galeri/view/{{$idnext}}" class="c-button b-50 photo-ar-right bg-dr-blue-2">
					next image
					<img class="arrow-img" src="{{ URL::asset('asettemplate1/img/gallery/arrow-left.jpg') }}" alt="">
					<div class="photo-next">
						<div class="photo-next-title color-dark-2">{{$namanext}}</div>
					</div>
				</a>
				<?php }?>
			</div>

		</div>

		<div class="related-block">
			<h3 class="related-title">Related Photos</h3>

			<div class="row">
				<?php $i=0;
				?>
				@foreach ($gambars as $gambar)
				<div class="item hotels gal-item style-3 col-xs-12 col-sm-4">
					<a class="black-hover" href="{{ url('/') }}/galeri/view/{{$gambar->id}}">
						<div class="gal-item-icon">
							<img class="img-full img-responsive" src="{{ url('/') }}/uploads/images/{{$gambar->gambar}}" style="width:400px;" alt="">
							<div class="tour-layer delay-1"></div>
							<div class="vertical-align">
								<span class="c-button small bg-white delay-2"><span>view more</span></span>
							</div>
						</div>
						<div class="gal-item-desc delay-1">
							<h4><b>{{$gambar->namafoto}}</b></h4>
						</div>
					</a>
				</div>
				<?php $i+=1;
				if($i==3){
					break;
				}
				?>
				@endforeach


			</div>
		</div>

	</div>
</div>
@endsection
