@extends('layouts.'.$namatemplate)

@section('kontenweb')

		<div class="container">
		<ul class="breadcrumb">
				<li><a href="{{url('/')}}">Home</a>
				</li>
				<li><a href="{{url('/')}}/galeri">Galeri</a></li>
				<li class="active">{{$nama}}</li>
		</ul>

						<h2>{{$nama}}</h2>
						<div id="gallery" class="flexslider photo-gallery style2 block" data-fix-control-nav-pos="1">

												<article class="box">
														<figure><img src="{{ url('/') }}/uploads/images/{{$filegambar}}" alt=""></figure>
														<div class="details">
																<p class="description" style="text-align:left"><?php echo nl2br($keterangan); ?>	</p>
														</div>
												</article> 

						</div>

						<h2>Related Photos</h2>
						<div class="items-container row image-box style9">
									<?php $i=0;
									?>
									@foreach ($gambars as $gambar)

									<div class="col-sm-4">
										<article class="box">
												<figure>
														<a class="hover-effect" title="" href="{{ url('/') }}/galeri/view/{{$gambar->id}}"><img width="370" height="190" alt="" src="{{ url('/') }}/uploads/images/{{$gambar->gambar}}"></a>
												</figure>
												<div class="details">
														<h4 class="box-title">{{$gambar->namafoto}}</h4>
												</div>
										</article>
								</div>
								<?php $i+=1;
								if($i==3){
									break;
								}
								?>
								@endforeach

						</div>

		</div>


@endsection
