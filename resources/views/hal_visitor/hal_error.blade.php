@extends('layouts.'.$namatemplate)
@section('kontenweb')

<div class="not-found">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/special/bg-2.jpg')}}" alt="">
	<div class="not-found-box">
		<div class="not-found-title">ouch!</div>
		<div class="not-found-message">

@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif

</div>
		<a href="#" class="c-button b-60 bg-white hv-white-o"><span>view more</span></a>
	</div>
</div>

@endsection
