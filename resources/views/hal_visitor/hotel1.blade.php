<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<form class="contact-form"id="formgo" action="{{url('/')}}/train/train_pilihbangku" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_wilayah" name="formgo_wilayah" value="CGK">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_tglcheckin" name="formgo_tglcheckin" >
<input type="hidden" id="formgo_tglcheckinpilihan" name="formgo_tglcheckinpilihan" >
<input type="hidden" id="formgo_tglcheckout" name="formgo_tglcheckout" >
<input type="hidden" id="formgo_tglcheckoutpilihan" name="formgo_tglcheckoutpilihan" >
<input type="hidden" id="formgo_jumkamar" name="formgo_jumkamar" value="1">
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >
<input type="hidden" id="formgo_roomtype" name="formgo_roomtype" >

</form>

<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('img/inner_banner_train.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{url('/')}}">Home</a> /</li>
				<li><span class="color-red-3">Hotel</span></li>
			</ul>
			<h2 class="color-white">Hotel</h2>
		</div>
	</div>
</div>

<div class="list-wrapper bg-grey-2">
  <div class="container">
    <ul class="list-breadcrumb clearfix">
      <li><a class="color-grey link-dr-blue" href="#">home</a> /</li>
      <li><a class="color-grey link-dr-blue" href="#">hotels</a> /</li>
      <li><span class="color-dr-blue">list hotels</span></li>
    </ul>
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar style-2 clearfix">
        <div class="sidebar-block">
          <h4 class="sidebar-title color-dark-2">search</h4>
          <div class="search-inputs">
            <div class="form-block clearfix">
              <h5>Wilayah</h5><div class="input-style b-50 color-3">
              <select onchange="pilihwilayah(this.value)" class="mainselection"placeholder="Check In"  name="asal" id="pilasal"style="padding:1px; border: 1px solid #aaa;">
                <option value="1">Domestik</option>
                <option value="2">Internasional</option>
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Tujuan</h5>
                <div class="input-style">
                <select class="input-style selektwo" name="tujuan" id="pildes">

                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Kamar</h5>
                <div class="input-style">
                <select onchange="pilihroomtype(this.value)" class="mainselection" id="pilroomtype" name="pilroomtype" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">
                  <option value="twin" <?php if($otomatiscari==1 && $pilroomtype=="twin"){print("selected=\"selected\"");} ?>>Twin</option>
                  <option value="one" <?php if($otomatiscari==1 && $pilroomtype=="one"){print("selected=\"selected\"");} ?>>Double</option>

                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Jumlah kamar</h5>
                <div class="input-style">
                  <select class="mainselection" id="jumkamar" name="jumkamar" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=5;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumkamar){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Check In</h5>
                <div class="input-style">
                  <input id="tglcheckin" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglcheckin)){print($tglcheckin);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Check Out</h5>
                <div class="input-style">
                  <input id="tglcheckout" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglcheckout)){print($tglcheckout);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Dewasa</h5>
                <div class="input-style">
                  <select class="mainselection" id="adt" name="adt" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=7;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>

            <div class="form-block clearfix">
              <h5>Anak</h5>
                <div class="input-style">
                  <select class="mainselection"id="chd" name="chd" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=0; $op<=6;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
          </div>
          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
        </div>


        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-9">
        <div class="" id="dafloading" >

          <div class="list-item-entry" id="loading_hotel">
              <div class="hotel-item style-10 bg-white">
                <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KAI.jpg')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                      <h6 class="color-grey-3 list-hidden">one way flights</h6>
                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                  </div>
                  </div>
              </div>
          </div>

        </div>
        @if($errors->has())
                    <div class="list-header clearfix" style="background-color: #ff6600;">
                      @foreach ($errors->all() as $error)
                      <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" >
                      {{ $error }}</h4>

                  </div>
                     @endforeach
                     @endif
        <div class="list-header clearfix" id="barissorting">
          <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
            <div style="color:BLACK;">
             Urut berdasarkan
             </div>
          </div>
            <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
              <div class="drop"style="color:BLACK;">
               <b>-</b>
                <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                <span>
                  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">Harga terkecil</a>
                  <a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">Harga terbesar</a>
                  <a href="#" onclick="sortAsc('data-waktu')"style="color:BLACK;">Jam terdekat</a>
                  <a href="#" onclick="sortDesc('data-waktu')"style="color:BLACK;">Jam terakhir </a>
                </span>
               </div>
            </div>

        </div>
        <div class="list-content clearfix" id="pilihanpergi">
          <div class="list-item-entry" >
                <div class="hotel-item style-10 bg-white">
                  <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:20px;width:180px;height:50px;"id="pilihanpergi_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <h5 id="pilihanpergi_namakereta">  DAF TRAN  </h5>
                      <h6 class="color-grey-3 list-hidden">one wayaa flights</h6>
                      <h4 id="hsubclasspergi"><b>
                        SubClass
                        <select   name="pilsubclasspergi" id="pilsubclasspergi">
                            <option value="0" >sad </option>
                              <option value="0" >sad1 </option>

                        </select>



                      </b></h4>
                        <h4><b id="pilihanpergi_harga">Cheap Flights to Paris</b></h4>
                        <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                        <div class="fi_block grid-hidden row row10">
                          <div class="flight-icon col-xs-6 col10">
                            <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                            <div class="fi_content">
                              <div class="fi_title color-dark-2">Pergi</div>
                              <div class="fi_text color-black" id="pilihanpergi_jampergi">wed nov 13, 2013 7:50 am</div>
                            </div>
                          </div>
                          <div class="flight-icon col-xs-6 col10">
                            <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                            <div class="fi_content">
                              <div class="fi_title color-dark-2">Tiba</div>
                              <div class="fi_text color-black" id="pilihanpergi_jamtiba">wed nov 13, 2013 7:50 am</div>
                            </div>
                          </div>
                        </div>

                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                        <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
                          <div class="hotel-right-text color-dark-2">1 stop</div>-->


                    </div>
                  </div>
              </div>
          </div>





        </div>
        <div  id="sisikiri">
        <div class="list-content clearfix" id="dafpergi">
        </div>
        </div>



      </div>
    </div>
  </div>
</div>

<footer class="bg-dark type-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="footer-block">
          <img src="img/theme-1/logo.png" alt="" class="logo-footer">
          <div class="f_text color-grey-7">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  magna aliqua. Ut aliquip ex ea commodo consequat.</div>
          <div class="footer-share">
            <a href="#"><span class="fa fa-facebook"></span></a>
            <a href="#"><span class="fa fa-twitter"></span></a>
            <a href="#"><span class="fa fa-google-plus"></span></a>
            <a href="#"><span class="fa fa-pinterest"></span></a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
       <div class="footer-block">
        <h6>Travel News</h6>
        <div class="f_news clearfix">
          <a class="f_news-img black-hover" href="#">
            <img class="img-responsive" src="img/home_6/news_1.jpg" alt="">
            <div class="tour-layer delay-1"></div>
          </a>
          <div class="f_news-content">
            <a class="f_news-tilte color-white link-red" href="#">amazing place</a>
            <span class="date-f">Mar 18, 2015</span>
            <a href="#" class="r-more">read more</a>
          </div>
        </div>
        <div class="f_news clearfix">
          <a class="f_news-img black-hover" href="#">
            <img class="img-responsive" src="img/home_6/news_2.jpg" alt="">
            <div class="tour-layer delay-1"></div>
          </a>
          <div class="f_news-content">
            <a class="f_news-tilte color-white link-red" href="#">amazing place</a>
            <span class="date-f">Mar 18, 2015</span>
            <a href="#" class="r-more">read more</a>
          </div>
        </div>
        <div class="f_news clearfix">
          <a class="f_news-img black-hover" href="#">
            <img class="img-responsive" src="img/home_6/news_1.jpg" alt="">
            <div class="tour-layer delay-1"></div>
          </a>
          <div class="f_news-content">
            <a class="f_news-tilte color-white link-red" href="#">amazing place</a>
            <span class="date-f">Mar 18, 2015</span>
            <a href="#" class="r-more">read more</a>
          </div>
        </div>
       </div>
    </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="footer-block">
                 <h6>Tags:</h6>
            <a href="#" class="tags-b">flights</a>
            <a href="#" class="tags-b">traveling</a>
            <a href="#" class="tags-b">sale</a>
            <a href="#" class="tags-b">cruises</a>
            <a href="#" class="tags-b">cars</a>
            <a href="#" class="tags-b">hotels</a>
            <a href="#" class="tags-b">tours</a>
            <a href="#" class="tags-b">booking</a>
            <a href="#" class="tags-b">countries</a>
       </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="footer-block">
                 <h6>Contact Info</h6>
                   <div class="contact-info">
                    <div class="contact-line color-grey-3"><i class="fa fa-map-marker"></i><span>Aenean vulputate porttitor</span></div>
        <div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="tel:93123456789">+93 123 456 789</a></div>
        <div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="mailto:">letstravel@mail.com</a></div>
        <div class="contact-line color-grey-3"><i class="fa fa-globe"></i><a href="#">let’s_travel@world.com</a></div>

      </div>
       </div>
    </div>
    </div>
  </div>
  <div class="footer-link bg-black">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <div class="copyright">
        <span>&copy; 2015 All rights reserved. LET'STRAVEL</span>
      </div>
          <ul>
        <li><a class="link-aqua" href="#">Privacy Policy </a></li>
        <li><a class="link-aqua" href="#">About Us</a></li>
        <li><a class="link-aqua" href="#">Support </a></li>
        <li><a class="link-aqua" href="#">FAQ</a></li>
        <li><a class="link-aqua" href="#">Blog</a></li>
        <li><a class="link-aqua" href="#"> Forum</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>


				@section('akhirbody')
        <script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
        <script type="text/javascript">
        //variabel
        var roomtype="twin";
        var wilayah="1";
        var hargapergi="0";
        var nilpil=0;
        var idpilper="";
        var idpilret="";
        var kotdes="Bali";
        var kodedes="MA05110750";
        var labeltujuan="";
        var tglcheckin_d="";
        var tglcheckin_m="";
        var tglcheckin_y="";

        var tglcheckout_d="";
        var tglcheckout_m="";
        var tglcheckout_y="";

        var tglcheckin="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_checkin').val(tglcheckin);
        $('#formgo_tgl_checkinpilihan').val(tglcheckin);
        var tglcheckout="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_checkout').val(tglcheckout);
        $('#formgo_tgl_checkoutpilihan').val(tglcheckout);

            @if($otomatiscari==1)
            $('#formgo_wilayah').val("{{$wilayah}}");
            $('#formgo_roomtype').val("{{$roomtype}}");
            $('#formgo_adt').val("{{$adt}}");
            $('#formgo_chd').val("{{$chd}}");
            $('#formgo_des').val("{{$des}}");
            $('#formgo_kotdes').val("{{$kotdes}}");
            @endif

        var jumkamar="1";
        var jumadt="1";
        var jumchd="0";
        var juminf="0";



        var urljadwal="{{ url('/') }}/daftarHotel/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#tomgoret').hide();
        $('#divpilihnextorulang').hide();
        $('#hsubclasspergi').hide();
        $('#tomulangipencarian').hide('slow');


        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#divpilihnextorulang').hide();
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });

        $('#tomulangipencarian').on('click', function() {


        $('#formatas').show('slow');

        //$('#labelpilihanpergi').hide('slow');
        $('#tomulangipencarian').hide('slow');

        });

        $('#jumkamar').on('change', function() {
        jumkamar=this.value;
        $('#formgo_jumkamar').val(jumkamar);

        });
        $('#adt').on('change', function() {
        jumadt=this.value;
        $('#formgo_adt').val(jumadt);

        });
        $('#chd').on('change', function() {
        jumchd=this.value;
        $('#formgo_chd').val(jumchd);

        });
        $('#inf').on('change', function() {
        juminf=this.value;
        $('#formgo_inf').val(juminf);

        });


        $('#labelpilihanpergi').hide();
        $('#pilihanpergi').hide();
        $('#pilihanpulang').hide();
        $('.selektwo').select2();

        $('#loading_hotel').hide();


        $('#tglcheckin').datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $('#tglcheckout').datepicker({ dateFormat: 'dd-mm-yy' }).val();

        $('#tglcheckin').on('change', function() {

        var values=this.value.split('-');
        tglcheckin_d=values[0];
        tglcheckin_m=values[1];
        tglcheckin_y=values[2];

        tglcheckin=tglcheckin_y+"-"+tglcheckin_m+"-"+tglcheckin_d;

        $('#formgo_tglcheckinpilihan').val(tglcheckin);

        });

        $('#tglcheckout').on('change', function() {

        var values=this.value.split('-');
        //alert(this.value);
        tglcheckout_d=values[0];
        tglcheckout_m=values[1];
        tglcheckout_y=values[2];

        tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
        $('#formgo_tglcheckoutpilihan').val(tglcheckout);

        });

        $('#pildes').on('change', function() {
          var values=this.value.split('-');
          var kode=values[0];
          kotdes=values[1];
          kodedes=kode;
          labeltujuan=this.value;
          $('#formgo_des').val(kodedes);
          $('#formgo_kotdes').val(kotdes);

        });
        var tes=[[2,3,4],[5,3,4]];
        var daftardaerah= {
                       domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
                       internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

                   };
      //alert(daftardaerah.internasional[4][0]+" "+daftardaerah.internasional[4][1]+" "+daftardaerah.internasional[4][2]);

      function pilihwilayah(i){
        var ambildaftar=null;
        if(i==1){
          ambildaftar=daftardaerah.domestik;
        }else{
          ambildaftar=daftardaerah.internasional;
        }
        var optionsAsString = "<option value=\"\"></option>";
              for(var i = 0; i < ambildaftar.length; i++) {
                optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
                @if($otomatiscari==1)
                if(i=="{{$des}}"){
                optionsAsString +='selected="selected"';
                }
                @endif
                optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
              }
              $( '#pildes' ).html( optionsAsString );
      }
      pilihwilayah(1);

      function pilihroomtype(kmr){
        roomtype=kmr;

        $('#formgo_roomtype').val(roomtype);
      }

      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

      $('#pilsubclasspergi').on('change', function() {
      hargapergi=this.value.split(",")[0];
      var totaladt=hargapergi*jumadt;
      var totalchd=hargapergi*jumchd;
      var totalinf=hargapergi*juminf;
      $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
      totalhargapergi=totaladt+totalchd+totalinf;
      $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(totalhargapergi));


      });

        function pilihKamar(bahanid
          ,jampergiberangkat
          ,jampergitiba
          ,harga
          ,namakereta
          ,kumpulaniddep
          ){
            var totaladt=harga*jumadt;
            var totalchd=harga*jumchd;
            var totalinf=harga*juminf;
            harga=totaladt+totalchd+totalinf;

            $('#formgo_selectedIDdep').val(kumpulaniddep);
             $('#pilsubclasspergi').find('option').remove();

        $('#formgo_TrainNoDep').val(bahanid);
        $('#formgo_keretaDep').val(namakereta);
        $('#formgo_hargaDep').val(harga);
        $('#formgo_tgl_dep').val(jampergiberangkat);
        $('#formgo_tgl_dep_tiba').val(jampergitiba);
        /**
       $("#"+idpilper).removeClass("kotakpilihb cell-view");
        $("#"+idpilper).addClass("kotakpilih cell-view");

        $("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
      idpilper=bahanid+"_gbr";
      **/

        $('#labelpilihanpergi').show('slow');
      $('#labelkolomutama').show('slow');
        $('#pilihanpergi').show('slow');
        //  alert(jampergitiba);
                $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
                //  alert($('#pilihanpergi_gbr').attr('src'));
                 $('#pilihanpergi_jampergi').html(jampergiberangkat);
                $('#pilihanpergi_jamtiba').html(jampergitiba);
               $('#pilihanpergi_namakereta').html(namakereta+' '+bahanid);
                $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(harga));

                nilpil+=1;
                $(location).attr('href', '#labelkolomutama');

              //  alert ('tes tes');
                  $( "#labelkolomutama" ).focusin();
                 $('#sisikiri').hide('slow');
                  $('#barissorting').hide('slow');
                 mintaformgo();

        }

        function mintaformgo(){
          $('#tomgoret').hide();
          $('#divpilihnextorulang').show('slow');
        }
        function cari(){
        nilpil=0;

        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

          //alert('haha');
        $("#barissorting").show('slow');
                urljadwalb=urljadwal;
                urljadwalb+="checkin/"+tglcheckin;
                urljadwalb+="/checkout/"+tglcheckout;
                urljadwalb+="/des/"+kodedes;
                urljadwalb+="/room/"+jumkamar;
                urljadwalb+="/roomtype/"+roomtype;
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                //akhir urljadwalb+="ac/";
                        $('#loading_hotel').show('fadeOut');

                        $('#dafpergi').html('');

                ambildata_hotel();

        $('#formatas').hide('slow');
               $('#pilihanpergi').hide();
                $('#pilihanpulang').hide();

                        $('#labelkolomutama').hide('slow');
              $('#labelpilihanpergi').hide('slow');
              $('#tomulangipencarian').show('slow');


                        alert(urljadwalb);
            }else{
              $('#bahanmodal').dialog({ modal: true });

            }
        }


        var ajaxku_hotel;
        function ambildata_hotel(){
          ajaxku_hotel = buatajax();
          var url=urljadwalb;
          ajaxku_hotel.onreadystatechange=stateChanged_hotel;
          ajaxku_hotel.open("GET",url,true);
          ajaxku_hotel.send(null);
        }
         function stateChanged_hotel(){
           var data;
            if (ajaxku_hotel.readyState==4){
              data=ajaxku_hotel.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_hotel').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var waktu=$(this).attr('data-waktu');

              var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              var valueToPush = new Array();
              valueToPush[0] = temp_id;
              valueToPush[1] = biaya;
              valueToPush[2] = waktu;
              valueToPush[3] = isiHTML;
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());

          }});


        }

        jQuery.fn.sortElements = (function(){

            var sort = [].sort;

            return function(comparator, getSortable) {

                getSortable = getSortable || function(){return this;};

                var placements = this.map(function(){

                    var sortElement = getSortable.call(this),
                        parentNode = sortElement.parentNode,

                        // Since the element itself will change position, we have
                        // to have some way of storing its original position in
                        // the DOM. The easiest way is to have a 'flag' node:
                        nextSibling = parentNode.insertBefore(
                            document.createTextNode(''),
                            sortElement.nextSibling
                        );

                    return function() {

                        if (parentNode === this) {
                            throw new Error(
                                "You can't sort elements if any one is a descendant of another."
                            );
                        }

                        // Insert before flag:
                        parentNode.insertBefore(this, nextSibling);
                        // Remove flag:
                        parentNode.removeChild(nextSibling);

                    };

                });

                return sort.call(this, comparator).each(function(i){
                    placements[i].call(getSortable.call(this));
                });

            };

        })();
        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });

        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });

        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        kodedes="{{$des}}";
        roomtype="{{$roomtype}}";
        tglcheckin="{{$tglcheckin}}";
        tglcheckout="{{$tglcheckout}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        juminf="{{$juminf}}";
        wilayah="{{$wilayah}}";
        kotdes="{{$kottujuan}}";
        cari();
        @endif
        </script>
		@endsection

		@endsection
