@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
		<script type="text/javascript" src="{{ URL::asset('asettemplate1/js/jquery.maskMoney.min.js')}}"></script>
		<link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<!-- INNER-BANNER -->
<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/hotel">hotel</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">{{$namahotel}}</h2>
  				</div>
			</div>
		</div>
	</div>

</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
					<?php $otomatiscari=0;?>
					<div class="col-xs-12 col-sm-4 col-md-3 nomobile">
		        <div class="sidebar style-2 clearfix">
		        <div class="sidebar-block">
		          <h4 class="sidebar-title color-dark-2">search</h4>
		          <div class="search-inputs">
		            <div class="form-block clearfix">
		              <h5>Wilayah</h5><div class="input-style b-50 color-3">
		              <select onchange="pilihwilayah(this.value)" class="mainselection"placeholder="Check In"  name="asal" id="pilasal"style="padding:1px; border: 1px solid #aaa;">
		                <option value="1"  <?php if($otomatiscari==1 && $wilayah=="1"){print("selected=\"selected\"");} ?>>Domestik</option>
		                <option value="2"  <?php if($otomatiscari==1 && $wilayah=="2"){print("selected=\"selected\"");} ?>>Internasional</option>
		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Tujuan</h5>
		                <div class="input-style">
		                <select class="input-style selektwo" name="tujuan" id="pildes">

		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Kamar</h5>
		                <div class="input-style">
		                <select onchange="pilihroomtype(this.value)" class="mainselection" id="pilroomtype" name="pilroomtype" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">
		                  <option value="twin" <?php if($otomatiscari==1 && $roomtype=="twin"){print("selected=\"selected\"");} ?>>Twin</option>
		                  <option value="one" <?php if($otomatiscari==1 && $roomtype=="one"){print("selected=\"selected\"");} ?>>Double</option>

		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Jumlah kamar</h5>
		                <div class="input-style">
		                  <select class="mainselection" id="jumkamar" name="jumkamar" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=1; $op<=5;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$room){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Check In</h5>
		                <div class="input-style">
		                  <input id="tglcheckin" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($checkin)){print($checkin);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Check Out</h5>
		                <div class="input-style">
		                  <input id="tglcheckout" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($checkout)){print($checkout);}else{echo date("Y-m-d");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Dewasa per kamar</h5>
		                <div class="input-style">
		                  <select class="mainselection" id="adt" name="adt" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=1; $op<=7;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>

		            <div class="form-block clearfix">
		              <h5>Anak per kamar</h5>
		                <div class="input-style">
		                  <select class="mainselection"id="chd" name="chd" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=0; $op<=6;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>
		          </div>
		          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
		        </div>


		        </div>
		      </div>
       		<div class="col-xs-12 col-md-9">
						<div class="detail-content-block">
							<h3 class="small-title">Informasi Penginapan</h3>
							<div class="table-responsive">
									<table class="table style-1 type-2 striped">
											<tbody>
												<tr>
														<td class="table-label color-black">Daerah</td>
														<td class="table-label color-dark-2"><strong>{{$citydes}},{{$countrydes}}</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Hotel</td>
														<td class="table-label color-dark-2"><strong>{{$namahotel}}</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Alamat:</td>
														<td class="table-label color-dark-2"><strong>{{$alamathotel}}</strong></td>
												</tr><tr>
														<td class="table-label color-black">Checkin:</td>
														<td class="table-label color-dark-2"><strong>{{$tgl_checkin}}</strong></td>
												</tr><tr>
														<td class="table-label color-black">Checkout:</td>
														<td class="table-label color-dark-2"><strong>{{$tgl_checkout}}</strong></td>
												</tr><tr>
														<td class="table-label color-black">Lama:</td>
														<td class="table-label color-dark-2"><strong>{{lamahari($tgl_checkin,$tgl_checkout)}} hari</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Kamar:</td>
														<td class="table-label color-dark-2"><strong>{{count($dafkamar)}}</strong></td>
												</tr>

											</tbody>
									</table>
								</div>
						</div>

<div >
				<?php $nomurut=0; foreach($dafkamar as $daf){ $nomurut+=1;?>
				<div class="detail-content-block">
					<h3 class="small-title">Kamar {{$nomurut}}</h3>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
						        <tr>
						          	<td class="table-label color-black">Kategori</td>
						          	<td class="table-label color-dark-2"><strong>{{$daf->kategori}}</strong></td>
						        </tr>
										<tr>
						          	<td class="table-label color-black">Tempat tidur</td>
						          	<td class="table-label color-dark-2"><strong>{{$daf->bed}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-black">Board:</td>
						          	<td class="table-label color-dark-2"><strong>{{$daf->board}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-black">Biaya per malam :</td>
						          	<td class="table-label color-dark-2"><strong>{{rupiah($daf->price)}}</strong></td>
					        	</tr>

					      	</tbody>
					    </table>
				    </div>
				</div>

				<?php }?>
				<div class="detail-content-block">
					<h3 class="small-title">Informasi Tagihan</h3>
					<div class="table-responsive">
							<table class="table style-1 type-2 striped">
									<tbody>
										<tr>
												<td class="table-label color-black">No. Transaksi:</td>
												<td class="table-label color-dark-2"><strong>{{$notrx}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$labelbiaya}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Batas pembayaran:</td>
												<td class="table-label color-dark-2"><strong>{{DateToIndo($tgl_bill_exp)}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Status tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$statustransaksi}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan untuk:</td>
												<td class="table-label color-dark-2"><strong>{{$cpname}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">EMAIL:</td>
												<td class="table-label color-dark-2"><strong>{{$cpmail}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">NOMOR TELEPHONE:</td>
												<td class="table-label color-dark-2"><strong>{{$cptlp}}</strong></td>
										</tr>

									</tbody>
							</table>
						</div>
				</div>

</div>
@if($status==0 || $status==1 || $status==3)
				<div class="detail-content-block">
					<div class="simple-text">
						<h3>Upload bukti pembayaran</h3>
						<p class="color-grey">Gunakan link dibawah ini untuk memudahkan konfirmasi pembayaran</p>
						<form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="exampleInputEmail1">Jumlah Bayar</label>
								<input name="jumlahbayar" id="jumlahbayar" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Tanggal Bayar</label>
								<input name="tanggalbayar" id="tanggalbayar"  type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Dari Bank</label>
								<input name="bank" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nomor Rekening</label>
								<input name="rekening" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Atas Nama</label>
								<input name="napem" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ke Rekening</label>
								<select class="form-control" name="rektuju" id="rektuju">
									 @foreach($dafrek as $rek)
									<option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
									@endforeach
								</select>
							</div>
						<div class="form-group">
							<label for="exampleInputFile">Gambar Bukti Transfer</label>
							<input name="foto" type="file" id="exampleInputFile">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
						<div class="custom-panel bg-grey-2 radius-4">
				      @foreach($dafbuk as $bukti)
					    <img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Bukti Pembayaran" style="width:200px;height:200px;">
					    <br>
							@endforeach
						</div>
					</div>
				</div>
@endif

       		</div>

       	</div>
	</div>
</div>

@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
	<script type="text/javascript">
		$('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
   }).val();
	 function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        hours = parseInt((timer /3600)%24, 10);
        minutes = parseInt((timer / 60)%60, 10)
        seconds = parseInt(timer % 60, 10);

				hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours+":"+minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 5;
        display = document.querySelector('.time');
  startTimer({{$sisawaktu}}, display);
};

$(document).ready(function(){
	$('#jumlahbayar').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
});

var roomtype="twin";
var wilayah="1";
var hargapergi="0";
var nilpil=0;
var idpilper="";
var idpilret="";
var kotdes="Bali";
var kodedes="MA05110750";
var labeltujuan="";
var tglcheckin_d="";
var tglcheckin_m="";
var tglcheckin_y="";

var tglcheckout_d="";
var tglcheckout_m="";
var tglcheckout_y="";

var tglcheckin="<?php echo date("Y-m-d");?>";
$('#formgo_tgl_checkin').val(tglcheckin);
$('#formgo_tgl_checkinpilihan').val(tglcheckin);
var tglcheckout="<?php echo date("Y-m-d");?>";
$('#formgo_tgl_checkout').val(tglcheckout);
$('#formgo_tgl_checkoutpilihan').val(tglcheckout);

		@if($otomatiscari==1)

		$('#formgo_roomtype').val("{{$roomtype}}");
		$('#formgo_adt').val("{{$jumadt}}");
		$('#formgo_chd').val("{{$jumchd}}");
		$('#formgo_des').val("{{$des}}");
		$('#formgo_wilayah').val("{{$wilayah}}");
		@endif

var jumkamar="1";
var jumadt="1";
var jumchd="0";
var juminf="0";



var urljadwal="{{ url('/') }}/hotel/otomatiscari/";
var urljadwalb=urljadwal;
var dafIDPergi = [];
var dafBiayaPergi = [];
var dafHTMLPergi = [];
var dafIDPulang = [];
var dafBiayaPulang = [];
var dafHTMLPulang = [];


$('#tomgoret').hide();
$('#divpilihnextorulang').hide();
$('#hsubclasspergi').hide();
$('#tomulangipencarian').hide('slow');


$( "#tompilulang" ).click(function() {
	nilpil=0;
	$('#divpilihnextorulang').hide();
	$('#sisikiri').show('slow');
	$('#barissorting').show('slow');
});
$( "#tomgoone" ).click(function() {
$( "#formgo" ).submit();
});
$( "#tomgoret" ).click(function() {
$( "#formgo" ).submit();
});

$('#tomulangipencarian').on('click', function() {


$('#formatas').show('slow');

//$('#labelpilihanpergi').hide('slow');
$('#tomulangipencarian').hide('slow');

});

$('#jumkamar').on('change', function() {
jumkamar=this.value;
$('#formgo_jumkamar').val(jumkamar);

});
$('#adt').on('change', function() {
jumadt=this.value;
$('#formgo_adt').val(jumadt);

});
$('#chd').on('change', function() {
jumchd=this.value;
$('#formgo_chd').val(jumchd);

});
$('#inf').on('change', function() {
juminf=this.value;
$('#formgo_inf').val(juminf);

});


$('.selektwo').select2();


$('#tglcheckin').datepicker({ dateFormat: 'dd-mm-yy' }).val();
$('#tglcheckout').datepicker({ dateFormat: 'dd-mm-yy' }).val();

$('#tglcheckin').on('change', function() {

var values=this.value.split('-');
tglcheckin_d=values[0];
tglcheckin_m=values[1];
tglcheckin_y=values[2];

tglcheckin=tglcheckin_y+"-"+tglcheckin_m+"-"+tglcheckin_d;

$('#formgo_tglcheckinpilihan').val(tglcheckin);

});

$('#tglcheckout').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglcheckout_d=values[0];
tglcheckout_m=values[1];
tglcheckout_y=values[2];

tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
$('#formgo_tglcheckoutpilihan').val(tglcheckout);

});

$('#pildes').on('change', function() {
	var values=this.value.split('-');
	var kode=values[0];
	kotdes=values[1];
	kodedes=kode;
	labeltujuan=this.value;
	$('#formgo_des').val(kodedes);

});
var tes=[[2,3,4],[5,3,4]];
var daftardaerah= {
							 domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
							 internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

					 };
//alert(daftardaerah.internasional[4][0]+" "+daftardaerah.internasional[4][1]+" "+daftardaerah.internasional[4][2]);

function pilihwilayah(i){
wilayah=i;
var ambildaftar=null;
if(i==1){
	ambildaftar=daftardaerah.domestik;
}else{
	ambildaftar=daftardaerah.internasional;
}
var optionsAsString = "<option value=\"\"></option>";
			for(var i = 0; i < ambildaftar.length; i++) {
				optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
				@if($otomatiscari==1)
				if(i=="{{$des}}"){
				optionsAsString +='selected="selected"';
				}
				@endif
				optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
			}
			$( '#pildes' ).html( optionsAsString );
}

pilihwilayah(<?php if($otomatiscari==1){print($wilayah);}else{print("1");} ?>);

function pilihroomtype(kmr){
roomtype=kmr;

$('#formgo_roomtype').val(roomtype);
}




function cari(){
nilpil=0;

if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

				urljadwalb=urljadwal;
				urljadwalb+="wilayah/"+wilayah;
				urljadwalb+="/checkin/"+tglcheckin;
				urljadwalb+="/checkout/"+tglcheckout;
				urljadwalb+="/des/"+kodedes;
				urljadwalb+="/room/"+jumkamar;
				urljadwalb+="/roomtype/"+roomtype;
				urljadwalb+="/jumadt/"+jumadt;
				urljadwalb+="/jumchd/"+jumchd;
				//akhir urljadwalb+="ac/";
        //alert(urljadwalb);
				$(location).attr('href', urljadwalb)

		}else{
			$('#bahanmodal').dialog({ modal: true });

		}
}


</script>
@endsection

@endsection
