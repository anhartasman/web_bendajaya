@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')


<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_2.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{url('/')}}">Home</a> /</li>
				<li><span class="color-red-3">Hotel</span></li>
			</ul>
			<h2 class="color-white">Hotel</h2>
		</div>
	</div>
</div>

<div class="list-wrapper bg-grey-2">
  <div class="container">
    <div class="row">
      <div id="sebelahkiri" class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar style-2 clearfix">
        <div class="sidebar-block">
          <h4 class="sidebar-title color-dark-2">search</h4>
          <div class="search-inputs">
            <div class="form-block clearfix">
              <h5>Destinasi</h5><div class="input-style b-50 color-3">
              <select onchange="pilihwilayah(this.value)" class="mainselection"placeholder="Check In"  name="asal" id="pilasal"style="padding:1px; border: 1px solid #aaa;">
                <option value="1"  <?php if($otomatiscari==1 && $wilayah=="1"){print("selected=\"selected\"");} ?>>Domestik</option>
                <option value="2"  <?php if($otomatiscari==1 && $wilayah=="2"){print("selected=\"selected\"");} ?>>Internasional</option>
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Kota</h5>
                <div class="input-style">
                <select class="input-style selektwo" name="tujuan" id="pildes">

                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Jumlah kamar</h5>
                <div class="input-style">
                  <select class="mainselection" id="jumkamar" name="jumkamar" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=5;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$room){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Check In</h5>
                <div class="input-style">
                  <input id="tglcheckin" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($checkin)){print(date("d-m-Y",strtotime($checkin)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Check Out</h5>
                <div class="input-style">
                  <input id="tglcheckout" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($checkout)){print(date("d-m-Y",strtotime($checkout)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Dewasa per kamar</h5>
                <div class="input-style">
                  <select class="mainselection" id="adt" name="adt" style="padding:1px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=2;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>

            <div class="form-block clearfix">
              <h5>Anak per kamar</h5>
                <div class="input-style">
                  <select class="mainselection"id="chd" name="chd" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=0; $op<=2;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumchd){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
          </div>
          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
        </div>


        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-9">
        <input style="margin-left:10px;margin-bottom:10px;" type="submit" id="tomback" class="c-button b-40 bg-red-3 hv-red-3-o" value="Cari jadwal lain">
        <div class="" id="dafloading" >

          <div class="list-item-entry" id="loading_hotel">
              <div class="hotel-item style-10 bg-white">
                <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:10px;width:50px;height:30px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                      <h6 class="color-grey-3 list-hidden">loading</h6>
                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                  </div>
                  </div>
              </div>
          </div>

        </div>
        @if($errors->has())
                    <div class="list-header clearfix" style="background-color: #ff6600;">
                      @foreach ($errors->all() as $error)
                      <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" >
                      {{ $error }}</h4>

                    </div>
                     @endforeach
                     @endif

        <div class="list-header clearfix" id="barissorting">
          <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
            <div style="color:BLACK;">
             Urut berdasarkan
             </div>
          </div>
            <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
              <div class="drop"style="color:BLACK;">
               <b>-</b>
                <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                <span>
                  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">Harga terendah</a>
                  <a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">Harga tertinggi</a>
                  <a href="#" onclick="sortAsc('data-bintang')"style="color:BLACK;">Bintang terendah</a>
                  <a href="#" onclick="sortDesc('data-bintang')"style="color:BLACK;">Bintang tertinggi </a>
                </span>
               </div>
            </div>

        </div>
        <div class="list-content clearfix" id="pilihanpergi">
          <div class="list-item-entry" >
                <div class="hotel-item style-10 bg-white">
                  <div class="table-view">
                    <div class="radius-top cell-view">
                      <img style="padding-left:20px;width:180px;height:50px;"id="pilihanpergi_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
                    </div>
                    <div class="title hotel-middle cell-view">
                      <h5 id="pilihanpergi_namakereta">  DAF TRAN  </h5>
                      <h6 class="color-grey-3 list-hidden">loading</h6>
                      <h4 id="hsubclasspergi"><b>
                        SubClass
                        <select   name="pilsubclasspergi" id="pilsubclasspergi">
                            <option value="0" >sad </option>
                              <option value="0" >sad1 </option>

                        </select>



                      </b></h4>
                        <h4><b id="pilihanpergi_harga">Cheap Flights to Paris</b></h4>
                        <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                        <div class="fi_block grid-hidden row row10">
                          <div class="flight-icon col-xs-6 col10">
                            <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                            <div class="fi_content">
                              <div class="fi_title color-dark-2">Pergi</div>
                              <div class="fi_text color-black" id="pilihanpergi_jampergi">wed nov 13, 2013 7:50 am</div>
                            </div>
                          </div>
                          <div class="flight-icon col-xs-6 col10">
                            <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                            <div class="fi_content">
                              <div class="fi_title color-dark-2">Tiba</div>
                              <div class="fi_text color-black" id="pilihanpergi_jamtiba">wed nov 13, 2013 7:50 am</div>
                            </div>
                          </div>
                        </div>

                     </div>
                    <div class="title hotel-right clearfix cell-view grid-hidden">
                        <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
                          <div class="hotel-right-text color-dark-2">1 stop</div>-->


                    </div>
                  </div>
              </div>
          </div>





        </div>
        <div  id="sisikiri">
        <div class="list-content clearfix" id="dafpergi">
        </div>
        </div>



      </div>
    </div>
  </div>
</div>


				@section('akhirbody') <script type="text/javascript">
        //variabel
        var roomtype="twin";
        var wilayah="1";
        var hargapergi="0";
        var nilpil=0;
        var idpilper="";
        var idpilret="";
        var kotdes="Bali";
        var kodedes="MA05110750";
        var labeltujuan="";
        var tglcheckin_d="";
        var tglcheckin_m="";
        var tglcheckin_y="";

        var tglcheckout_d="";
        var tglcheckout_m="";
        var tglcheckout_y="";

        var tglcheckin="<?php echo date("Y-m-d");?>";
        var tglcheckout="<?php echo date("Y-m-d");?>";


        var jumkamar="1";
        var jumadt="1";
        var jumchd="0";
        var juminf="0";



        var urljadwal="{{ url('/') }}/daftarHotel/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];

        $('#tomback').hide();
        $('#tomgoret').hide();
        $('#divpilihnextorulang').hide();
        $('#hsubclasspergi').hide();
        $('#tomulangipencarian').hide('slow');


        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#divpilihnextorulang').hide();
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });

        $('#tomulangipencarian').on('click', function() {


        $('#formatas').show('slow');

        //$('#labelpilihanpergi').hide('slow');
        $('#tomulangipencarian').hide('slow');

        });

        $('#jumkamar').on('change', function() {
        jumkamar=this.value;

        });
        $('#adt').on('change', function() {
        jumadt=this.value;

        });
        $('#chd').on('change', function() {
        jumchd=this.value;

        });
        $('#inf').on('change', function() {
        juminf=this.value;

        });


        $('#labelpilihanpergi').hide();
        $('#pilihanpergi').hide();
        $('#pilihanpulang').hide();
        $('.selektwo').select2();

        $('#loading_hotel').hide();

        $("#tglcheckin").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
            maxDate: '+2y'
        });

        $("#tglcheckout").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            changeMonth: true,
            changeYear: true
        });

        $('#tglcheckin').on('change', function() {

        var values=this.value.split('-');
        tglcheckin_d=values[0];
        tglcheckin_m=values[1];
        tglcheckin_y=values[2];

        tglcheckin=tglcheckin_y+"-"+tglcheckin_m+"-"+tglcheckin_d;

        var selectedDate = new Date(tglcheckin);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime());
                $("#tglcheckout").datepicker( "option", "minDate", endDate );
                $("#tglcheckout").datepicker( "option", "maxDate", '+2y' );
                    settglcheckout($("#tglcheckout").val());

        });
        function settglcheckout(val){
          var values=val.split('-');
          //alert(this.value);
          tglcheckout_d=values[0];
          tglcheckout_m=values[1];
          tglcheckout_y=values[2];

          tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
        }
        $('#tglcheckout').on('change', function() {

                        settglcheckout(this.value);
        });

        $('#pildes').on('change', function() {
          var values=this.value.split('-');
          var kode=values[0];
          if(wilayah==1){
            jspilihandom=kode;
          }else{
            jspilihanin=kode;
          }
          kotdes=values[1];
          kodedes=kode;
          labeltujuan=this.value;

        });
        var tes=[[2,3,4],[5,3,4]];
        var daftardaerah= {
                       domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
                       internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

                   };
      //alert(daftardaerah.internasional[4][0]+" "+daftardaerah.internasional[4][1]+" "+daftardaerah.internasional[4][2]);
      var jspilihandom="";
      var jspilihanin="";
      @if($otomatiscari==1)
      jspilihandom="{{$des}}";
      jspilihanin="{{$des}}";
      @endif
    function pilihwilayah(i){
      wilayah=i;
      var ambildaftar=null;
      var jsdesnya="";
      if(i==1){
        ambildaftar=daftardaerah.domestik;
        jsdesnya=jspilihandom;
      }else{
        ambildaftar=daftardaerah.internasional;
        jsdesnya=jspilihanin;
      }
      var optionsAsString = "<option value=\"\"></option>";
            for(var i = 0; i < ambildaftar.length; i++) {
              optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";

              if(ambildaftar[i][0]==jsdesnya){

                  optionsAsString +='selected="selected"';

              }

              optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
            }
            $( '#pildes' ).html( optionsAsString );
    }
      pilihwilayah(<?php if($otomatiscari==1){print($wilayah);}else{print("1");} ?>);

      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

      $('#tomback').on('click', function() {
      $('#tomback').hide('slow');
      $('#sebelahkiri').show('slow');
      });

        function cari(){
        nilpil=0;

        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          var isMobile = window.matchMedia("only screen and (max-width: 760px)");

              if (isMobile.matches) {
                $("#tomback").show('slow');
                $("#sebelahkiri").hide('slow');
                //$('#titikup')[0].scrollIntoView(true);
              }
          //alert('haha');
        $("#barissorting").show('slow');
                urljadwalb=urljadwal;
                urljadwalb+="checkin/"+tglcheckin;
                urljadwalb+="/checkout/"+tglcheckout;
                urljadwalb+="/des/"+kodedes;
                urljadwalb+="/room/"+jumkamar;
                urljadwalb+="/roomtype/"+roomtype;
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                //akhir urljadwalb+="ac/";
                        $('#loading_hotel').show('fadeOut');

                        $('#dafpergi').html('');

                ambildata_hotel();

        $('#formatas').hide('slow');
               $('#pilihanpergi').hide();
                $('#pilihanpulang').hide();

                        $('#labelkolomutama').hide('slow');
              $('#labelpilihanpergi').hide('slow');
              $('#tomulangipencarian').show('slow');


//                        alert(urljadwalb);
            }else{
              $('#bahanmodal').dialog({ modal: true });

            }
        }


        var ajaxku_hotel;
        function ambildata_hotel(){
          ajaxku_hotel = buatajax();
          var url=urljadwalb;
          ajaxku_hotel.onreadystatechange=stateChanged_hotel;
          ajaxku_hotel.open("GET",url,true);
          ajaxku_hotel.send(null);
        }
         function stateChanged_hotel(){
           var data;
            if (ajaxku_hotel.readyState==4){
              data=ajaxku_hotel.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_hotel').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var bintang=$(this).attr('data-bintang');

              var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-bintang="'+bintang+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());

          }});


        }

        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });

        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });

        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        kodedes="{{$des}}";
        roomtype="{{$roomtype}}";
        tglcheckin="{{$checkin}}";
        tglcheckout="{{$checkout}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        jumkamar="{{$room}}";
        wilayah="{{$wilayah}}";
        cari();
        @endif
        </script>
		@endsection

		@endsection
