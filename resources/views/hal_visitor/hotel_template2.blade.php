@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Cari Kamar Hotel</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li class="active">Hotel</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="row">
                <div class="col-sm-4 col-md-3" id="sebelahkiri">
                   <div class="toggle-container filters-container">
                        <div class="panel style1 arrow-right">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" href="#accomodation-type-filter" class="" aria-expanded="true">Search</a>
                          </h4>
                          <div id="modify-search-panel" class="panel-collapse collapse in" aria-expanded="true">

                                <div class="panel-content">
                                        <div class="form-group">
                                            <label>Destinasi</label>
                                            <div class="selector">
                                                <select class="full-width" onchange="pilihwilayah(this.value)" name="wilayah" id="wilayah">
                                                    <option value="1">Domestik</option>
                                                    <option value="2">Internasional</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Kota</label>
                                              <select style="width:100%" class="full-width selektwo"   name="pildes" id="pildes">

                                              </select>
                                        </div>
                                        <div class="form-group">
                                            <label>check in</label>
                                            <div class="datepicker-wrap">
                                                <input id="tglcheckin" onchange="setcheckinhotel(this.value);" name="tglcheckin" value="<?php if($otomatiscari==1 && isset($checkin)){print(date("d-m-Y",strtotime($checkin)));}else{echo date("d-m-Y");} ?>" type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>check out</label>
                                            <div class="datepicker-wrap">
                                                <input id="tglcheckout" onchange="setcheckouthotel(this.value);" name="tglcheckout"value="<?php if($otomatiscari==1 && isset($checkout)){print(date("d-m-Y",strtotime($checkout)));}else{echo date("d-m-Y");} ?>"  type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Rooms</label>
                                            <div class="selector">
                                                <select class="full-width" id="jumkamar">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                          <label>Dewasa</label>
                                          <div class="selector">
                                              <select class="full-width" id="adt" >
                                                  <option value="1">01</option>
                                                  <option value="2">02</option>
                                              </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label>Anak</label>
                                          <div class="selector">
                                              <select class="full-width" id="chd" >
                                                  <option value="0">00</option>
                                                  <option value="1">01</option>
                                                  <option value="2">02</option>
                                              </select>
                                          </div>
                                        </div>
                                        <br />
                                        <button onclick="cari()" class="btn-medium icon-check uppercase full-width">search again</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                  <button style="margin-left:auto;margin-right:auto;margin-bottom:20px;" class="tomup full-width btn-small" id="tomback">Cari hotel lain</button>

                    <div class="sort-by-section clearfix">

                        <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
                        <h4 class="sort-by-title block-sm">Urut berdasarkan:</h4>
                        <select id="chd" style="margin-top:10px;" >
                            <option value="0" onclick="sortAsc('data-harga')">Harga Terendah</option>
                            <option value="1" onclick="sortDesc('data-harga')">Harga Tertinggi</option>
                            <option value="1" onclick="sortAsc('data-bintang')">Bintang terendah</option>
                            <option value="1" onclick="sortDesc('data-bintang')">Bintang tertinggi </option>
                        </select>
                      </div>

                    </div>
                    <div class="hotel-list listing-style3 hotel">
                      <div class="list-item-entry" id="loading_hotel">
                          <div class="hotel-item style-10 bg-white">
                            <div class="table-view">
                                <div class="radius-top cell-view">
                                  <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                                </div>
                                <div class="title hotel-middle cell-view">
                                  <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                                 </div>
                                <div class="title hotel-right clearfix cell-view grid-hidden">
                              </div>
                              </div>
                          </div>
                      </div>
                      <div  id="sisikiri">
                      <div id="dafpergi">
                      </div>
                      </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


				@section('akhirbody')
         <script type="text/javascript">
        //variabel
        var roomtype="twin";
        var wilayah="1";
        var hargapergi="0";
        var nilpil=0;
        var idpilper="";
        var idpilret="";
        var kotdes="Bali";
        var kodedes="MA05110750";
        var labeltujuan="";
        var tglcheckin_d="";
        var tglcheckin_m="";
        var tglcheckin_y="";

        var tglcheckout_d="";
        var tglcheckout_m="";
        var tglcheckout_y="";

        var tglcheckin="<?php echo date("Y-m-d");?>";
        var tglcheckout="<?php echo date("Y-m-d");?>";


        var jumkamar="1";
        var jumadt="1";
        var jumchd="0";
        var juminf="0";
        $('#tomback').hide();



        var urljadwal="{{ url('/') }}/daftarHotel/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#tomgoret').hide();

        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });

        $('#jumkamar').on('change', function() {
        jumkamar=this.value;

        });
        $('#adt').on('change', function() {
        jumadt=this.value;

        });
        $('#chd').on('change', function() {
        jumchd=this.value;

        });
        $('#inf').on('change', function() {
        juminf=this.value;

        });

        $('#tomback').on('click', function() {
        $('#tomback').hide('slow');
        $('#sebelahkiri').show('slow');
        });
        $('.selektwo').select2();
        
        $('#loading_hotel').hide();
        $("#tglcheckin").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
            maxDate: '+2y'
        });

        $("#tglcheckout").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            changeMonth: true,
            changeYear: true
        });


        function setcheckinhotel(val){
          		var values=val.split('-');
              var tgl=values[2]+"-"+values[1]+"-"+values[0];
              tglcheckin=tgl;
              $('#tglcheckin').val(values[0]+"-"+values[1]+"-"+values[2]);
              var selectedDate = new Date(tglcheckin);
              var msecsInADay = 86400000;
              var endDate = new Date(selectedDate.getTime());
                      $("#tglcheckout").datepicker( "option", "minDate", endDate );
                      $("#tglcheckout").datepicker( "option", "maxDate", '+2y' );
                          settglcheckout($("#tglcheckout").val());

        }
        function settglcheckout(val){
          var values=val.split('-');
          //alert(this.value);
          tglcheckout_d=values[0];
          tglcheckout_m=values[1];
          tglcheckout_y=values[2];

          tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
          $('#tglcheckout').val(values[0]+"-"+values[1]+"-"+values[2]);
        }

        function setcheckouthotel(val){
             settglcheckout(val);
        }
      $('#pildes').on('change', function() {
        var values=this.value.split('-');
        var kode=values[0];
        if(wilayah==1){
          jspilihandom=kode;
        }else{
          jspilihanin=kode;
        }
        kotdes=values[1];
        kodedes=kode;
        labeltujuan=this.value;

      });
        var tes=[[2,3,4],[5,3,4]];
        var daftardaerah= {
                       domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
                       internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

                   };
      //alert(daftardaerah.internasional[4][0]+" "+daftardaerah.internasional[4][1]+" "+daftardaerah.internasional[4][2]);

      var jspilihandom="";
      var jspilihanin="";
      @if($otomatiscari==1)
      jspilihandom="{{$des}}";
      jspilihanin="{{$des}}";
      @endif
    function pilihwilayah(i){
      wilayah=i;
      var ambildaftar=null;
      var jsdesnya="";
      if(i==1){
        ambildaftar=daftardaerah.domestik;
        jsdesnya=jspilihandom;
      }else{
        ambildaftar=daftardaerah.internasional;
        jsdesnya=jspilihanin;
      }
      var optionsAsString = "<option value=\"\"></option>";
            for(var i = 0; i < ambildaftar.length; i++) {
              optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";

              if(ambildaftar[i][0]==jsdesnya){

                  optionsAsString +='selected="selected"';

              }

              optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
            }
            $( '#pildes' ).html( optionsAsString );
    }

      pilihwilayah(<?php if($otomatiscari==1){print($wilayah);}else{print("1");} ?>);


      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

        function cari(){
        nilpil=0;

        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          if (isMobile.matches) {
            $("#tomback").show('slow');
            $("#sebelahkiri").hide('slow');
            //$('#titikup')[0].scrollIntoView(true);
          }
          //alert('haha');
        $("#barissorting").show('slow');
                urljadwalb=urljadwal;
                urljadwalb+="checkin/"+tglcheckin;
                urljadwalb+="/checkout/"+tglcheckout;
                urljadwalb+="/des/"+kodedes;
                urljadwalb+="/room/"+jumkamar;
                urljadwalb+="/roomtype/"+"one";
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                //akhir urljadwalb+="ac/";
                        $('#loading_hotel').show('fadeOut');

                        $('#dafpergi').html('');

                ambildata_hotel();

        $('#formatas').hide('slow');


                        // alert(urljadwalb);
            }else{
              $('#bahanmodal').dialog({ modal: true });

            }
        }


        var ajaxku_hotel;
        function ambildata_hotel(){
          ajaxku_hotel = buatajax();
          var url=urljadwalb;
          ajaxku_hotel.onreadystatechange=stateChanged_hotel;
          ajaxku_hotel.open("GET",url,true);
          ajaxku_hotel.send(null);
        }
         function stateChanged_hotel(){
           var data;
            if (ajaxku_hotel.readyState==4){
              data=ajaxku_hotel.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_hotel').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var bintang=$(this).attr('data-bintang');

              var isiHTML='<div class="box isiitem" isiitem="1" id="'+temp_id+'"  data-bintang="'+bintang+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());

          }});


        }

        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitem").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });

        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitem").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });

        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        kodedes="{{$des}}";
        roomtype="{{$roomtype}}";
        tglcheckin="{{$checkin}}";
        tglcheckout="{{$checkout}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        jumkamar="{{$room}}";
        wilayah="{{$wilayah}}";
        cari();
        @endif
                // alert(urljadwalb);
        </script>
		@endsection

		@endsection
