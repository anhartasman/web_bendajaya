@extends('layouts.'.$namatemplate)

@section('kontenweb')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">Cari Hotel</li>
    </ul>
    @if($otomatiscari==1)
    <h3 class="booking-title">Pilihan hotel di {{daerahhotel_detailByCode($des)["city"]}} tanggal {{date("d M",strtotime($checkin))}} - {{date("d M",strtotime($checkout))}}</h3>
    @endif
    <div class="row">
        <div class="col-md-3" id="sebelahkiri">
            <form class="booking-item-dates-change mb30">
              <div class="form-group">
                  <label>Destinasi</label>
                  <ul class="nav nav-pills nav-sm nav-no-br mb10" >
                      <li class="<?php if($otomatiscari==1 && isset($wilayah)){ if($wilayah==1){print("active");}} if($otomatiscari==0){print("active");}?>"><a href="#flight-search-1" data-toggle="tab" onclick="pilihwilayah(1)">Domestik</a>
                      </li>
                      <li class="<?php if($otomatiscari==1 && isset($wilayah)){ if($wilayah==2){print("active");}}?>"><a href="#flight-search-2" data-toggle="tab" onclick="pilihwilayah(2)">Internasional</a>
                      </li>
                  </ul>
              </div>
                <div class="form-group">
                    <label>Kota</label>
                    <select style="width:100%" class="full-width selektwo"   name="pildes" id="pildes">

                    </select>
                </div>
                <div class="input-daterange">
                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                        <label>Check in</label>
                        <input id="tglcheckin" onchange="setcheckinhotel(this.value);" name="tglcheckin" value="<?php if($otomatiscari==1 && isset($checkin)){print(date("d-m-Y",strtotime($checkin)));}else{echo date("d-m-Y");} ?>" class="form-control" type="text" />
                    </div>
                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                        <label>Check out</label>
                        <input id="tglcheckout" onchange="setcheckouthotel(this.value);" name="tglcheckout"value="<?php if($otomatiscari==1 && isset($checkout)){print(date("d-m-Y",strtotime($checkout)));}else{echo date("d-m-Y");} ?>" class="form-control" type="text" />
                    </div>
                </div>
                <div class="form-group form-group- form-group-select-plus">
                  <label>Kamar</label>
                  <div  id="pilihan_kamar" class="btn-group btn-group-select-num" data-toggle="buttons">

                      <label class="btn btn-primary active">
                          <input type="radio" name="options" value="1" />1</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="2"  />2</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="3"  />3</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="4"  />3+</label>

                  </div>
                  <select class="form-control hidden" onchange="set_kamar(this.value)">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                  </select>
                </div>
                <div class="form-group  ">
                  <label>Dewasa per kamar </label>
                  <div  id="pilihan_adt" class="btn-group btn-group-select-num" data-toggle="buttons">

                      <label class="btn btn-primary active">
                          <input type="radio" name="options" value="1" />1</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="2"  />2</label>


                  </div>
                </div>
                <div class="form-group  ">
                  <label>Anak per kamar </label>
                  <div  id="pilihan_chd" class="btn-group btn-group-select-num" data-toggle="buttons">
                    <label class="btn btn-primary active">
                        <input type="radio" name="options" value="0" />0</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="1" />1</label>
                      <label class="btn btn-primary">
                          <input type="radio" name="options" value="2"  />2</label>


                  </div>
                </div>


                <input class="btn btn-primary" type="button" onclick="cari()" value="Cari" />
            </form>


        </div>
        <div class="col-md-9">
          <div class="row">
              <div class="col-md-6 text-left">
                  <p><a  style="cursor:pointer" data-effect="mfp-zoom-out" id="tomback">Ulangi pencarian</a>
                  </p>
              </div>
          </div>
            <div class="row" id="dafloading" >
              <div class="nav-drop booking-sort" id="loading_hotel">
                  <div class="row">
                      <div class="col-md-4">
                        <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                      </div>
                      <div class="col-md-8">
                        <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">

                      </div>
                  </div>
              </div>
            </div>
            <div id="barissorting" class="nav-drop booking-sort">
                <h5 class="booking-sort-title"><a href="#" id="labelurut">Urut berdasarkan: -<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                <ul class="nav-drop-menu">
                    <li><a href="#" onclick="sortAsc('data-harga',' Harga terendah')">Harga terendah</a>
                    </li>
                    <li><a href="#" onclick="sortDesc('data-harga',' Harga tertinggi')">Harga tertinggi</a>
                    </li>
                    <li><a href="#" onclick="sortAsc('data-bintang',' Bintang terendah')">Bintang terendah</a>
                    </li>
                    <li><a href="#" onclick="sortDesc('data-bintang',' Bindang tertinggi')">Bindang tertinggi</a>
                    </li>
                </ul>
            </div>
            <ul class="booking-list"  id="sisikiri">
              <div id="dafpergi">
              </div>

            </ul>

        </div>
    </div>
    <div class="gap"></div>
</div>

				@section('akhirbody')
         <script type="text/javascript">
        //variabel
        var roomtype="twin";
        var wilayah="1";
        var hargapergi="0";
        var nilpil=0;
        var idpilper="";
        var idpilret="";
        var kotdes="Bali";
        var kodedes="MA05110750";
        var labeltujuan="";
        var tglcheckin_d="";
        var tglcheckin_m="";
        var tglcheckin_y="";

        var tglcheckout_d="";
        var tglcheckout_m="";
        var tglcheckout_y="";

        var tglcheckin="<?php echo date("Y-m-d");?>";
        var tglcheckout="<?php echo date("Y-m-d");?>";



        var jumkamar="1";
        var jumadt="1";
        var jumchd="0";
        var juminf="0";
        $('#tomback').hide();



        var urljadwal="{{ url('/') }}/daftarHotel/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#pilihan_kamar input').on('change', function() {
          jumkamar=$('input[name=options]:checked', '#pilihan_kamar').val();
        });
            $('#pilihan_adt input').on('change', function() {
              jumadt=$('input[name=options]:checked', '#pilihan_adt').val();

            });
            $('#pilihan_chd input').on('change', function() {
              jumchd=$('input[name=options]:checked', '#pilihan_chd').val();
            });

        function set_kamar(jum){
          jumkamar=jum;
        }

        $('#tomgoret').hide();

        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });

        $('#jumkamar').on('change', function() {
        jumkamar=this.value;

        });
        $('#adt').on('change', function() {
        jumadt=this.value;

        });
        $('#chd').on('change', function() {
        jumchd=this.value;

        });
        $('#inf').on('change', function() {
        juminf=this.value;

        });


        $('#tomback').on('click', function() {
        $('#tomback').hide('slow');
        $('#sebelahkiri').show('slow');
        });
        $('.selektwo').select2();

        $('#loading_hotel').hide();
        $("#tglcheckin").datepicker({
          format: 'dd-mm-yyyy',
          startDate: '+0d',
          autoclose: true,
        });

        $("#tglcheckout").datepicker({
          format: 'dd-mm-yyyy',
          startDate: '+0d',
          autoclose: true,
        });


        function setcheckinhotel(val){
          		var values=val.split('-');
              var tgl=values[2]+"-"+values[1]+"-"+values[0];
              tglcheckin=tgl;
              $('#tglcheckin').val(values[0]+"-"+values[1]+"-"+values[2]);

              $('#tglcheckout').data('datepicker').setStartDate(new Date(tglcheckin));
        			var x = new Date(tglcheckin);
        			var y = new Date(tglcheckout);
        			if(x>y){
              $('#tglcheckout').val(val);
        			settglcheckout(tgl);
        			}

        }
        function settglcheckout(val){
          var values=val.split('-');
          //alert(this.value);
          tglcheckout_d=values[0];
          tglcheckout_m=values[1];
          tglcheckout_y=values[2];

          tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;

        }

        function setcheckouthotel(val){
             settglcheckout(val);
        }
      $('#pildes').on('change', function() {
        var values=this.value.split('-');
        var kode=values[0];
        if(wilayah==1){
          jspilihandom=kode;
        }else{
          jspilihanin=kode;
        }
        kotdes=values[1];
        kodedes=kode;
        labeltujuan=this.value;
        $('#formgo_des').val(kodedes);

      });
        var tes=[[2,3,4],[5,3,4]];
        var daftardaerah= {
                       domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
                       internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

                   };
      //alert(daftardaerah.internasional[4][0]+" "+daftardaerah.internasional[4][1]+" "+daftardaerah.internasional[4][2]);
        var jspilihandom="";
        var jspilihanin="";
        @if($otomatiscari==1)
        jspilihandom="{{$des}}";
        jspilihanin="{{$des}}";
        @endif
      function pilihwilayah(i){
        wilayah=i;
        var ambildaftar=null;
        var jsdesnya="";
        if(i==1){
          ambildaftar=daftardaerah.domestik;
          jsdesnya=jspilihandom;
        }else{
          ambildaftar=daftardaerah.internasional;
          jsdesnya=jspilihanin;
        }
        var optionsAsString = "<option value=\"\"></option>";
              for(var i = 0; i < ambildaftar.length; i++) {
                optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";

                if(ambildaftar[i][0]==jsdesnya){

                    optionsAsString +='selected="selected"';

                }

                optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
              }
              $( '#pildes' ).html( optionsAsString );
      }

      pilihwilayah(<?php if($otomatiscari==1){print($wilayah);}else{print("1");} ?>);


      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

        function cari(){
        nilpil=0;

        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          if (isMobile.matches) {
            $("#tomback").show('slow');
            $("#sebelahkiri").hide('slow');
            //$('#titikup')[0].scrollIntoView(true);
          }
          //alert('haha');

        $("#barissorting").show('slow');
                urljadwalb=urljadwal;
                urljadwalb+="checkin/"+tglcheckin;
                urljadwalb+="/checkout/"+tglcheckout;
                urljadwalb+="/des/"+kodedes;
                urljadwalb+="/room/"+jumkamar;
                urljadwalb+="/roomtype/"+"one";
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                //akhir urljadwalb+="ac/";
                        $('#loading_hotel').show('fadeOut');

                        $('#dafpergi').html('');

                ambildata_hotel();

        $('#formatas').hide('slow');


                        // alert(urljadwalb);
            }else{
              $('#bahanmodal').dialog({ modal: true });

            }
        }


        var ajaxku_hotel;
        function ambildata_hotel(){
          ajaxku_hotel = buatajax();
          var url=urljadwalb;
          ajaxku_hotel.onreadystatechange=stateChanged_hotel;
          ajaxku_hotel.open("GET",url,true);
          ajaxku_hotel.send(null);
        }
         function stateChanged_hotel(){
           var data;
            if (ajaxku_hotel.readyState==4){
              data=ajaxku_hotel.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_hotel').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var bintang=$(this).attr('data-bintang');

              var isiHTML='<div class="box isiitem" isiitem="1" id="'+temp_id+'"  data-bintang="'+bintang+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());

          }});


        }

        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom,isi){
          kolomsort=kolom;
          var sortedDivs = $(".isiitem").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });
          $("#labelurut").html("Urut berdasarkan:"+isi);
        }

        function sortDesc(kolom,isi){
          kolomsort=kolom;
            var sortedDivs = $(".isiitem").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });
            $("#labelurut").html("Urut berdasarkan:"+isi);

        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        kodedes="{{$des}}";
        roomtype="{{$roomtype}}";
        tglcheckin="{{$checkin}}";
        tglcheckout="{{$checkout}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        jumkamar="{{$room}}";
        wilayah="{{$wilayah}}";
        cari();
        @endif
                // alert(urljadwalb);
        </script>
		@endsection

		@endsection
