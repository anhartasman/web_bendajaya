<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<div class="inner-banner style-7">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_2.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{url('/')}}">home</a> /</li>
		  				<li><a class="link-blue-2" href="{{url('/')}}/hotel">hotels</a> /</li>
		  				<li><span>detail</span></li>
		  			</ul>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
		<div class="detail-header">
			<div class="row">
				<div class="col-xs-12 col-sm-8">
					<h2 class="detail-title color-dark-2">{{$namahotel}}</h2>

			    </div>
			    <div class="col-xs-12 col-sm-4">
			    	<div class="detail-price color-dark-2">Mulai dari <span class="color-dr-blue">{{rupiahceil($biayaawal)}}</span> /malam</div>
			    </div>
	       	</div>
       	</div>
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
       			<div class="detail-content color-1">
       				<div class="detail-top slider-wth-thumbs style-2">
						<div class="swiper-container thumbnails-preview" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
			                <div class="swiper-wrapper">
                        <?php $fi=0; ?>
                        @foreach($images as $gbr)
		                    	<div class="swiper-slide <?php  $gbr=str_replace("\\","",$gbr); if($fi==0){ print("active");}  ?>" data-val="{{$fi}}"> <?php $fi+=1;?>
                            <?php
                             $gbr=str_replace("/","-----",$gbr);
                          //  $image = Image::make("http://".$gbr)->resize(770, 455);
                                 echo "<img class=\"img-responsive img-full\" src=\"".url('/')."/gambarhttp/".$gbr."/w/770/h/455"."\">";
                                  ?>
		                    	</div>
                        @endforeach

		                    </div>
			                <div class="pagination pagination-hidden"></div>
			            </div>
			            <div class="swiper-container thumbnails" data-autoplay="0"
			            data-loop="0" data-speed="500" data-center="0"
			            data-slides-per-view="responsive" data-xs-slides="3"
			            data-sm-slides="{{count($images)}}" data-md-slides="{{count($images)}}" data-lg-slides="{{count($images)}}"
			            data-add-slides="{{count($images)}}">
			                <div class="swiper-wrapper">
                <?php $fi=0; ?>
                @foreach($images as $image)
								<div class="swiper-slide <?php $image=str_replace("\\","",$image); if($fi==0){print("current active");} ?>" data-val="{{$fi}}"><?php $fi+=1;?>
									<img class="img-responsive img-full" src="http://{{$image}}" alt="" style="width: 150px; height: 101.667px;">
								</div>
                @endforeach

							</div>
							<div class="pagination hidden"></div>
						</div>
					</div>

					<div class="detail-content-block">
						<h5>Informasi kontak</h5>
						<p>Alamat : {{$alamathotel}} Email : {{$email}} Website : {{$website}}</p>

						<h5>Kebijakan</h5>
            @foreach($policies as $policy)
            <p>- {{$policy}}</p>
            @endforeach
            <div class="justmobile">
            <h5>Fasilitas</h5>
            @foreach($fasilitas as $f)
             <p>{{$f}}</p>
            @endforeach
          </div>
					</div>
					<div class="detail-content-block">
						<h3>daftar harga</h3>
	          <div class="accordion style-2">

            @foreach($rooms as $r)
            <div class="accordeon-entry active">
						<h5>{{rupiahceil($r['price'])}}</h5>
						<div class="toggle-content act">
						  <span class="accordeon-wrap">
                <p>Kategori : {{$r['characteristic']}}</p>
                <p>Ranjang : {{$r['bed']}} <p>
                <p>{{$r['board']}} <p>
              </span>
						</div>
					  </div>
            @endforeach

	          </div>
					</div>
					<div class="detail-content-block">
          <form class="simple-from"  method="POST" action="kirimdatadiri" id="formutama">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="totalamount" name="totalamount" value=""  >
            <input type="hidden" id="selectedID" name="selectedID" value="{{$selectedID}}"  >
            <input type="hidden" name="fields[code]" value="56345678safs_">
            <input type="hidden" id="isijson" name="isijson" value="{{$isijson}}"  >

            <div class="simple-group">
              <h3 class="small-title">Info Kontak</h3>
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-block type-2 clearfix">
                    <div class="form-label color-dark-2">Gelar</div>
                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                      <select name="isiancptit" id="isiancptit"class="mainselection" >

                        <option value="MR">MR</option>
                        <option value="MRS">MRS</option>
                        <option value="MS">MS</option>

                      </select>
                    </div>
                  </div>
                </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-block type-2 clearfix">
                      <div class="form-label color-dark-2">Nama</div>
                      <div class="input-style-1 b-50 brd-0 type-2 color-3">
                        <input type="text" id="isiancpname" name="isiancpname" required="" placeholder="Nama">
                      </div>
                    </div>
                  </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-block type-2 clearfix">
                    <div class="form-label color-dark-2">Telepon</div>
                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                      <input type="text" id="isiancptlp" name="isiancptlp" required="" placeholder="Nomor Telepon">
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-block type-2 clearfix">
                    <div class="form-label color-dark-2">E-mail</div>
                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                      <input type="text" id="isiancpmail" name="isiancpmail" required="" placeholder="Email">
                    </div>
                  </div>
                </div>

              </div>
            </div>


            <?php $urutkamar=0; ?>
            @while ($urutkamar<$jumlahkamar)
              <?php $urutkamar++; ?>
              <input type="hidden" id="kamar_harga_hidden{{$urutkamar}}" name="kamar_harga_hidden{{$urutkamar}}" value="{{$rooms[0]['price']}}"  >
              <input type="hidden" id="isianidroom{{$urutkamar}}" name="isianidroom{{$urutkamar}}" value="{{$rooms[0]['selectedIDroom']}}"  >

            <div class="simple-group">
             <h3 class="small-title">Tentukan Kamar # {{$urutkamar}}</h3>
             <div class="row">
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Harga</div>
                   <select name="kamar_harga" onchange="pilihKamar{{$urutkamar}}(this.value)" id="kamar_harga"class="mainselection" >
                     @foreach($rooms  as $r)
                     <option value="{{$r['selectedIDroom']}}">{{rupiahceil($r['price'])}}</option>
                     @endforeach

                   </select>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Kategori</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" name="kamar_kategori{{$urutkamar}}" id="kamar_kategori{{$urutkamar}}" required="" placeholder="pilih harga terlebih dahulu" value="{{$rooms[0]['characteristic']}}">
                    </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Ranjang</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <select name="kamar_bed{{$urutkamar}}" onchange="pilihKamar{{$urutkamar}}(this.value)" id="kamar_bed{{$urutkamar}}"class="mainselection" >

                       <option value="Twin">Twin</option>
                       <option value="Single">Single</option>


                     </select>
                 </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Board</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" name="kamar_board{{$urutkamar}}" id="kamar_board{{$urutkamar}}" required="" placeholder="pilih harga terlebih dahulu" value="{{$rooms[0]['board']}}">
                   </div>
                 </div>
               </div>

             </div>

            </div>
            @endwhile

            <input type="submit" id="tombolsubmit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="confirm booking">


          </form>
        </div>
				</div>
       		</div>
       		<div class="col-xs-12 col-md-4 nomobile">
       			<div class="right-sidebar">
       				<div class="detail-block bg-dr-blue">
       					<h4 class="color-white">Fasilitas</h4>
       					<div class="details-desc">
                  @foreach($fasilitas as $f)
							     <p class="color-grey-9"><span class="color-white">{{$f}}</span></p>
                  @endforeach
                </div>

       				</div>

       			</div>
       		</div>
       	</div>

	</div>
</div>


				@section('akhirbody')
        <script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
        <script type="text/javascript">
        <?php $jum=count($rooms);$i=0; ?>
        var infohotel= {
          @foreach($rooms as $r)
            a{{$r['selectedIDroom']}}:{characteristic:"{{$r['characteristic']}}",bed:"{{$r['bed']}}",board:"{{$r['board']}}",price:"{{$r['price']}}"}<?php $i+=1; if($i<$jum){print(",");} ?>
          @endforeach
          };


          <?php $urutkamar=0; ?>
          @while ($urutkamar<$jumlahkamar)
            <?php $urutkamar++; ?>
        function pilihKamar{{$urutkamar}}(selID){
          $('#kamar_kategori{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_harga_hidden{{$urutkamar}}').val(infohotel["a"+selID].price);
          $('#isianidroom{{$urutkamar}}').val(selID);
        }
        @endwhile
        </script>
		@endsection

		@endsection
