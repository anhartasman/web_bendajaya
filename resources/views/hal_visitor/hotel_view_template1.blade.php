<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent

<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_2.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row"style="margin-top:50px;">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{url('/')}}">home</a> /</li>
		  				<li><a class="link-blue-2" href="{{url('/')}}/hotel">hotels</a> /</li>
		  				<li><span>detail</span></li>
		  			</ul>
            <h2 class="color-white">{{$namahotel}}</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
            <div class="detail-top slider-wth-thumbs style-2">
          <div class="swiper-container thumbnails-preview" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                    <div class="swiper-wrapper">
                      <?php $fi=0; ?>
                      @foreach($images as $gbr)
                      <div class="swiper-slide <?php  $gbr=str_replace("\\","",$gbr); if($fi==0){ print("active");}  ?>" data-val="{{$fi}}"> <?php $fi+=1;?>
                        <?php
                         $gbr=str_replace("/","-----",$gbr);
                      //  $image = Image::make("http://".$gbr)->resize(770, 455);
                             echo "<img class=\"img-responsive img-full\" src=\"".url('/')."/gambarhttp/".$gbr."/w/770/h/455"."\">";
                              ?>
                      </div>
                      @endforeach

                      </div>
                    <div class="pagination pagination-hidden"></div>
                </div>
                <div class="swiper-container thumbnails" data-autoplay="0"
                data-loop="0" data-speed="500" data-center="0"
                data-slides-per-view="responsive" data-xs-slides="3"
                data-sm-slides="{{count($images)}}" data-md-slides="{{count($images)}}" data-lg-slides="{{count($images)}}"
                data-add-slides="{{count($images)}}">
                    <div class="swiper-wrapper">
              <?php $fi=0; ?>
              @foreach($images as $image)
              <div class="swiper-slide <?php $image=str_replace("\\","",$image); if($fi==0){print("current active");} ?>" data-val="{{$fi}}"><?php $fi+=1;?>
                <img class="img-responsive img-full" src="http://{{$image}}" alt="" style="width: 150px; height: 101.667px;">
              </div>
              @endforeach

            </div>
            <div class="pagination hidden"></div>
          </div>
        </div>
       			<div class="detail-content color-1">

					<div class="detail-content-block">
          <form class="simple-from"  method="POST" action="kirimdatadiri" id="formutama">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="totalamount" name="totalamount" value=""  >
            <input type="hidden" id="selectedID" name="selectedID" value="{{$selectedID}}"  >
            <input type="hidden" name="fields[code]" value="56345678safs_">
            <input type="hidden" id="isijson" name="isijson" value="{{Crypt::encrypt($isijson)}}"  >

            <input type="hidden" name="isiancptit" id="isiancptit" value="MSTR"  >



            <?php $urutkamar=0; ?>
            @while ($urutkamar<$jumlahkamar)
              <?php $urutkamar++; ?>
              <input type="hidden" id="kamar_board_hidden{{$urutkamar}}" name="kamar_board_hidden{{$urutkamar}}" value="{{$rooms[0]['board']}}"  >
              <input type="hidden" id="kamar_kategori_hidden{{$urutkamar}}" name="kamar_kategori_hidden{{$urutkamar}}" value="{{$rooms[0]['characteristic']}}"  >
              <input type="hidden" id="kamar_harga_hidden{{$urutkamar}}" name="kamar_harga_hidden{{$urutkamar}}" value="{{$rooms[0]['price']}}"  >
              <input type="hidden" id="kamar_bed{{$urutkamar}}" name="kamar_bed{{$urutkamar}}" value="Twin"  >
              <input type="hidden" id="isianidroom{{$urutkamar}}" name="isianidroom{{$urutkamar}}" value="{{$rooms[0]['selectedIDroom']}}"  >

            <div class="simple-group">
             <h4 class="small-title">Tentukan Kamar # {{$urutkamar}}</h4>
             <div class="row">
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Pilih Harga</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
  									 <div class="drop">
  										<b>{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$biayaawal))}}</b>
  										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
  										 <span>
  												 @foreach($rooms  as $r)
  											   <a href="#" onclick="pilihKamar{{$urutkamar}}({{$r['selectedIDroom']}})">{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$r['price']))}}</a>
                           @endforeach
  										 </span>
  										</div>
  								 </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Pilih Ranjang</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="kamar_bed{{$urutkamar}}"name="kamar_bed{{$urutkamar}}" >Twin</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#" onclick="pilihBed{{$urutkamar}}('Twin')" >Twin</a>
                         <a href="#" onclick="pilihBed{{$urutkamar}}('Single')" >Single</a>
                     </span>
										</div>
								 </div>
                 </div>

               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Kategori</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" disabled name="kamar_kategori{{$urutkamar}}" id="kamar_kategori{{$urutkamar}}" required="" placeholder="pilih harga terlebih dahulu" value="{{$rooms[0]['characteristic']}}">
                    </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Board</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" disabled name="kamar_board{{$urutkamar}}" id="kamar_board{{$urutkamar}}" required="" placeholder="pilih harga terlebih dahulu" value="{{$rooms[0]['board']}}">
                   </div>
                 </div>
               </div>

             </div>

            </div>
            @endwhile

            <div class="simple-group">
             <h4 class="small-title">Info Kontak</h4>
             <div class="row">
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Gelar</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
                    <div class="drop">
                     <b id="labelcptit">MR</b>
                      <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                      <span>
                          <a href="#">MR</a>
                          <a href="#">MRS</a>
                          <a href="#">MS</a>
                      </span>
                     </div>
                  </div>
                 </div>
               </div>
                 <div class="col-xs-12 col-sm-6">
                   <div class="form-block type-2 clearfix">
                     <div class="form-label color-dark-2">Nama</div>
                     <div class="input-style-1 b-50 brd-0 type-2 color-3">
                       <input type="text" id="isiancpname" name="isiancpname" required="" placeholder="Nama">
                     </div>
                   </div>
                 </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Telepon</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" id="isiancptlp" name="isiancptlp" required="" placeholder="Nomor Telepon">
                   </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">E-mail</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="email"   id="isiancpmail" name="isiancpmail" required="" placeholder="Email">
                   </div>
                 </div>
               </div>

             </div>
            </div>
            <input type="submit" id="tombolsubmit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="confirm booking">


          </form>
        </div>
				</div>
       		</div>
       		<div class="col-xs-12 col-md-4">
       			<div class="right-sidebar">
              <div class="detail-block bg-dr-blue">
       					<h4 class="color-white">Detail</h4>
       					<div class="details-desc">
							<p class="color-grey-9">Lokasi: <span class="color-white">{{$alamathotel}}</span></p>
              @foreach($fasilitas as $f)
							<p class="color-grey-9">{{$f}}: <span class="color-white">YES</span></p>
              @endforeach
            </div>


       				</div>

              <div class="popular-tours bg-grey-2">
                <h4 class="color-dark-2">Hotel lain di {{$namakota}}</h4>
                @if($dafhot!=null)
                  @foreach($dafhot as $h)
                  <?php $bahannama=$h->nama;
                  $bahannama=str_replace(" ","_",$bahannama);

                   $json_images = json_decode($h->json_images, true);


                   $gbr=$json_images[0]; $gbr=str_replace("http://","",$gbr);  $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                <div class="hotel-small style-2 clearfix">
                  <a class="hotel-img black-hover" href="{{url('/infohotel/'.$bahannama)}}">
                    <img class="img-responsive radius-0" src="{{url('/')}}/gambarhttp/{{$gbr}}/w/120/h/99" alt="">
                    <div class="tour-layer delay-1"></div>
                  </a>
                  <div class="hotel-desc">
                      <h4>{{$h->nama}}</h4>
                    <div class="hotel-loc tt">{{$h->alamat}}</div>
                  </div>
                </div>
                @endforeach
                @endif

                 
              </div>
       			</div>
       		</div>

       	</div>

	</div>
</div>


				@section('akhirbody')
        <script type="text/javascript">
        $( "#dialog-confirm" ).hide();

        var sudahoke=0;
        $('#formutama').on('submit', function() {
          $('#isiancptit').val($('#labelcptit').html());
          dialogalert();
                    return (sudahoke==1);
        });



        <?php $jum=count($rooms);$i=0; ?>
        var infohotel= {
          @foreach($rooms as $r)
            a{{$r['selectedIDroom']}}:{characteristic:"{{$r['characteristic']}}",bed:"{{$r['bed']}}",board:"{{$r['board']}}",price:"{{$r['price']}}"}<?php $i+=1; if($i<$jum){print(",");} ?>
          @endforeach
          };


          <?php $urutkamar=0; ?>
          @while ($urutkamar<$jumlahkamar)
            <?php $urutkamar++; ?>
        function pilihKamar{{$urutkamar}}(selID){
          $('#kamar_kategori_hidden{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board_hidden{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_kategori{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_harga_hidden{{$urutkamar}}').val(infohotel["a"+selID].price);
          $('#isianidroom{{$urutkamar}}').val(selID);
        }
    function pilihBed{{$urutkamar}}(val){
      $('#kamar_bed{{$urutkamar}}').val(val);
    }
        @endwhile
        </script>
		@endsection

		@endsection
