@extends('layouts.'.$namatemplate)
@section('sebelumtitel')

<script src="{{ URL::asset('asettemplate2/js/require.js')}}"></script>
@endsection
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{$namahotel}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
          <li><a href="{{ url('/') }}">HOME</a></li>
          <li><a href="{{ url('/') }}/hotel">Hotel</a></li>
          <li class="active">Pilih Kamar</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="tab-container style1" id="hotel-main-content">
                    <div class="tab-content">
                        <div id="photos-tab" class="tab-pane fade in active">
                            <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                                <ul class="slides">
                                  @foreach($images as $gbr)
                                  <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                    <li><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/900/h/500" alt="" /></li>
                                  @endforeach
                                </ul>
                            </div>
                            <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                <ul class="slides">
                                  @foreach($images as $gbr)
                                  <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                  <li><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/70/h/70" alt="" /></li>
                                  @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="hotel-features" class="tab-container">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="hotel-description">
                            <div class="long-description">
                              <h2>Fasilitas</h2>
                              <ul class="amenities clearfix style2">
                              @foreach($fasilitas as $f)
                              <li class="col-md-4 col-sm-6">
                                  <div class="icon-box">{{$f}}</div>
                              </li>
                              @endforeach
                              </ul><BR>
                              <h2>Kebijakan</h2>
                              <p style="padding-left:10px">@foreach($policies as $policy)
                              <?php print ($policy); ?><br /><br />
                              @endforeach</p>
                              <h2 class="no-padding">Daftar Harga</h2>
                              <ul class="amenities clearfix style2"style="padding-left:10px">
                              @foreach($rooms as $r)
                              <li class="col-md-4 col-sm-6 clearfix no-padding">
                              <b>{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$r['price']))}}</b></br>
                              Kategori : {{$r['characteristic']}}</br>
                              Ranjang : {{$r['bed']}}</br>
                              Board : {{$r['board']}}</br></br>
                              </li>
                                @endforeach
                                </ul>
                                <form class="review-form"  method="POST" action="kirimdatadiri" id="formutama">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" id="totalamount" name="totalamount" value=""  >
                                  <input type="hidden" id="selectedID" name="selectedID" value="{{$selectedID}}"  >
                                  <input type="hidden" id="isijson" name="isijson" value="{{Crypt::encrypt($isijson)}}"  >

                            <h2>Pesan {{$jumlahkamar}} kamar</h2>
                            <?php $urutkamar=0; ?>
                            @while ($urutkamar<$jumlahkamar)
                              <?php $urutkamar++; ?>
                              <div class="update-search clearfix no-float"style="padding-left:10px">
                                <h4 class="title">Kamar #{{$urutkamar}}</h4>

                                  <div class="col-md-5 no-padding ">
                                      <div class="row">
                                          <div class="col-xs-6">
                                            <label>Pilih Harga</label>
                                            <div class="selector">
                                                <select  name="kamar_harga"  id="kamar_harga" class="full-width" onchange="pilihKamar{{$urutkamar}}(this.value)" >
                                                  @foreach($rooms  as $r)
                                                  <option value="{{$r['selectedIDroom']}}">{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$r['price']))}}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                          </div>
                                          <div class="col-xs-6">
                                              <label>Kategori</label>
                                              <input type="text" disabled name="kamar_kategori{{$urutkamar}}" id="kamar_kategori{{$urutkamar}}" class="input-text full-width" value="{{$rooms[0]['characteristic']}}" placeholder="enter a review title" />

                                          </div>

                                      </div>
                                  </div>

                                  <div class="col-md-5">
                                      <div class="row no-float no-padding">
                                        <div class="col-xs-6">
                                          <label>Board</label>
                                          <div class="selector">
                                              <input type="text" disabled name="kamar_board{{$urutkamar}}" id="kamar_board{{$urutkamar}}" class="input-text full-width"  value="{{$rooms[0]['board']}}" placeholder="enter a review title" />
                                          </div>
                                        </div>
                                        <div class="col-xs-6">
                                          <label>Bed</label>
                                          <div class="selector">
                                            <select name="kamar_bed{{$urutkamar}}" onchange="pilihKamar{{$urutkamar}}(this.value)" id="kamar_bed{{$urutkamar}}"class="mainselection" >
                                              <option value="Twin">Twin</option>
                                              <option value="Single">Single</option>
                                            </select></div>
                                        </div>

                                      </div>
                                  </div>
                              </div>
                              <input type="hidden" id="kamar_board_hidden{{$urutkamar}}" name="kamar_board_hidden{{$urutkamar}}" value="{{$rooms[0]['board']}}"  >
                              <input type="hidden" id="kamar_kategori_hidden{{$urutkamar}}" name="kamar_kategori_hidden{{$urutkamar}}" value="{{$rooms[0]['characteristic']}}"  >
                              <input type="hidden" id="kamar_harga_hidden{{$urutkamar}}" name="kamar_harga_hidden{{$urutkamar}}" value="{{$rooms[0]['price']}}"  >
                              <input type="hidden" id="isianidroom{{$urutkamar}}" name="isianidroom{{$urutkamar}}" value="{{$rooms[0]['selectedIDroom']}}"  >


                            @endwhile
                        </div>

                        </div>
                    </div>

                </div>
                <div class="booking-section travelo-box">

                        <div class="booking-form card-information"style="padding-left:7px;">
                            <h2>Formulir Kontak</h2>
                            <div class="form-group row" style="padding-left:10px">
                                <div class="col-sm-6 col-md-5">
                                    <label>Panggilan</label>
                                    <div class="selector">
                                        <select name="isiancptit" id="isiancptit"  class="full-width">
                                          <option value="MR">MR</option>
                                          <option value="MRS">MRS</option>
                                          <option value="MS">MS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama</label>
                                    <input type="text" id="isiancpname" required name="isiancpname"  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row" style="padding-left:10px">
                                <div class="col-sm-6 col-md-5">
                                    <label>Telepon</label>
                                    <input type="text" id="isiancptlp" required name="isiancptlp" class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Email</label>
                                    <input type="email" id="isiancpmail" name="isiancpmail" required  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <hr />

                        <div class="form-group row">
                            <div class="col-sm-6 col-md-5">
                                <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>

            <div class="sidebar col-md-3 nomobile">
                <article class="detailed-logo">
                    <figure>
                      <?php $gbr=$images[0]; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                      <img width="114" height="85" src="{{url('/')}}/gambarhttp/{{$gbr}}/w/114/h/85" alt="">
                    </figure>
                    <div class="details">
                        <h2 class="box-title">{{$namahotel}}</h2>
                        <span class="price clearfix">
                            <small class="pull-left">avg/night</small>
                            <span class="pull-right">{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$biayaawal))}}</span>
                        </span>
                        <p class="description">{{$alamathotel}}</p>
                    </div>
                </article>
                <div class="travelo-box">
                    <h4>Hotel lain di {{$namakota}}</h4>
                    <div class="image-box style14">
                      @if($dafhot!=null)
                        @foreach($dafhot as $h)
                        <?php $bahannama=$h->nama;
                        $bahannama=str_replace(" ","_",$bahannama);
                         ?>
                        <article class="box">
                            <figure>
                                <a href="{{url('/infohotel/'.$bahannama)}}"><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/63/h/59" alt="" /></a>
                            </figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">{{$h->nama}}</a></h5>
                            </div>
                        </article>
                        @endforeach
                        @endif

                    </div>
                </div>
                <div class="travelo-box contact-box">
                    <h4>Butuh bantuan?</h4>
                    <p>Costumer Service kami siap melayani</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                        <br>
                        <a class="contact-email" href="#">{{$infowebsite['email']}}</a>
                    </address>
                </div>

            </div>
        </div>
    </div>
</section>
				@section('akhirbody')
         <script type="text/javascript">
         $( "#dialog-confirm" ).hide();

         var sudahoke=0;
         $('#formutama').on('submit', function() {
          dialogalert();
                     return (sudahoke==1);
         });

        <?php $jum=count($rooms);$i=0; ?>
        var infohotel= {
          @foreach($rooms as $r)
            a{{$r['selectedIDroom']}}:{characteristic:"{{$r['characteristic']}}",bed:"{{$r['bed']}}",board:"{{$r['board']}}",price:"{{$r['price']}}"}<?php $i+=1; if($i<$jum){print(",");} ?>
          @endforeach
          };


          <?php $urutkamar=0; ?>
          @while ($urutkamar<$jumlahkamar)
            <?php $urutkamar++; ?>
        function pilihKamar{{$urutkamar}}(selID){
          $('#kamar_kategori_hidden{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board_hidden{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_kategori{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_harga_hidden{{$urutkamar}}').val(infohotel["a"+selID].price);
          $('#isianidroom{{$urutkamar}}').val(selID);
        }
        @endwhile
        </script>
		@endsection

		@endsection
