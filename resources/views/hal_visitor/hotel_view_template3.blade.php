@extends('layouts.'.$namatemplate)
@section('sebelumtitel')

<script src="{{ URL::asset('asettemplate2/js/require.js')}}"></script>
@endsection
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent




        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a>
                </li>
                <li><a href="{{url('/')}}/hotel">Hotel</a>
                </li>
                <li class="active">{{$namahotel}}</li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="lh1em">{{$namahotel}}</h2>
                         </div>
                        <div class="col-md-5">
                              <p class="booking-item-header-price"><small>Mulai dari</small>  <span class="text-lg">{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$biayaawal))}}</span>/malam</p>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-6">
                        <div class="tabbable booking-details-tabbable">

                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                    <div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">

                                        @foreach($images as $gbr)
                                        <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                          <img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/800/h/600" alt="" title="{{$namahotel}}" />
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="booking-item-meta">
                             <div class="booking-item-rating">
                                <ul class="icon-list icon-group booking-item-rating-stars">
                                  <li><i class="fa fa-star<?php if($bintanghotel<1){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<2){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<3){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<4){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<5){print("-o");} ?>"></i>
                                  </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-11">
                                <h4 class="lh1em">Alamat</h4>
                                <ul class="list booking-item-raiting-summary-list">
                                  <p class="mb30">{{$alamathotel}}</p>
                                </ul>
                                <h4>Fasilitas</h4>
                                <?php $nom=0;?>
                                @foreach($fasilitas as $f)
                                <?php $nom+=1;?>
                               <div class="col-md-6">
                              - {{$f}}
                                </div>
                                 @endforeach


                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <h4 class="lh1em">&nbsp;</h4>
                                <h4 class="lh1em">Kebijakan</h4>
                                <ul class="list booking-item-raiting-summary-list">
                                    <?php $i=0; ?>
                                    @foreach($policies as $policy)
                                      <?php $i+=1; ?>
                                    <li>
                                        <p>{{$i}}. <?php print ($policy); ?></p>

                                    </li>
                                    @endforeach
                                </ul>

                            </div>

                        </div>

                    </div>
                </div>

                <h3>Pilih Kamar</h3>

                <div class="row">
                    <div class="col-md-9">
                      <form  method="POST" action="kirimdatadiri" id="formutama">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="totalamount" name="totalamount" value=""  >
                        <input type="hidden" id="selectedID" name="selectedID" value="{{$selectedID}}"  >
                        <input type="hidden" name="fields[code]" value="56345678safs_">
                        <input type="hidden" id="isijson" name="isijson" value="{{Crypt::encrypt($isijson)}}"  >

                        <ul class="booking-list">
                          <?php $urutkamar=0; ?>
                          @while ($urutkamar<$jumlahkamar)
                            <?php $urutkamar++; ?>
                            <input type="hidden" id="kamar_board_hidden{{$urutkamar}}" name="kamar_board_hidden{{$urutkamar}}" value="{{$rooms[0]['board']}}"  >
                            <input type="hidden" id="kamar_kategori_hidden{{$urutkamar}}" name="kamar_kategori_hidden{{$urutkamar}}" value="{{$rooms[0]['characteristic']}}"  >
                            <input type="hidden" id="kamar_harga_hidden{{$urutkamar}}" name="kamar_harga_hidden{{$urutkamar}}" value="{{$rooms[0]['price']}}"  >
                            <input type="hidden" id="kamar_bed{{$urutkamar}}" name="kamar_bed{{$urutkamar}}" value="Twin"  >
                            <input type="hidden" id="isianidroom{{$urutkamar}}" name="isianidroom{{$urutkamar}}" value="{{$rooms[0]['selectedIDroom']}}"  >

                            <li>
                              <h5>#{{$urutkamar}}</h5>
                                <a class="booking-item">
                                    <div class="row">
                                      <div class="row" style="padding:5px;">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Tipe kamar</label>
                                                  <select class="form-control">
                                                    @foreach($rooms  as $r)
                                                      <option onclick="pilihKamar{{$urutkamar}}({{$r['selectedIDroom']}})">{{$r['characteristic']}}</option>
                                                    @endforeach
                                                  </select>
                                              </div>
                                              <div class="form-group">
                                                  <label>Board</label>
                                                  <input class="form-control" disabled name="kamar_board{{$urutkamar}}" id="kamar_board{{$urutkamar}}" required=""  value="{{$rooms[0]['board']}}" placeholder="Write something" type="text" />
                                              </div>

                                          </div>
                                          <div class="col-md-6">

                                              <div class="form-group">
                                                  <label>Harga per malam</label>
                                                  <input class="form-control" disabled  name="kamar_harga{{$urutkamar}}" id="kamar_harga{{$urutkamar}}" required="" placeholder="pilih harga terlebih dahulu" value="{{rupiahceil(hasildiskon($settingan_member->mmid,"HTL",$rooms[0]['price']))}}" type="text" />
                                              </div>

                                              <div class="form-group">
                                                  <label>Tempat tidur</label>
                                                  <select class="form-control">
                                                      <option onclick="pilihBed{{$urutkamar}}('Twin')" >Twin</option>
                                                      <option onclick="pilihBed{{$urutkamar}}('Single')" >Single</option>
                                                  </select>
                                              </div>

                                          </div>
                                      </div>

                                    </div>
                                </a>
                            </li>
                            @endwhile

<h4>Data diri</h4>
<li>
    <a class="booking-item">
        <div class="row">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Panggilan</label>
                      <select class="form-control" name="isiancptit" id="isiancptit" >
                        <option value="MR">MR</option>
                        <option value="MRS">MRS</option>
                        <option value="MS">MS</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Nama</label>
                      <input class="form-control"  id="isiancpname" required name="isiancpname" type="text" />
                  </div>

              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Telepon</label>
                      <input class="form-control" id="isiancptlp" required name="isiancptlp" type="text" />
                  </div>
                  <div class="form-group">
                      <label>Email</label>
                      <input class="form-control"  type="email" id="isiancpmail" name="isiancpmail"  />
                  </div>

              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-5">
                  <button type="submit" class="full-width btn btn-primary">CONFIRM BOOKING</button>
              </div>
          </div>

        </div>
    </a>
</li>
                        </ul>

                      </form>
                    </div>
                    <div class="col-md-3">
                        <h4>Hotel lain di {{$namakota}}</h4>
                        <ul class="booking-list">
                          @if($dafhot!=null)
                            @foreach($dafhot as $h)
                            <?php $bahannama=$h->nama;
                            $bahannama=str_replace(" ","_",$bahannama);
                             ?>
                            <li>
                            <a href="{{url('/infohotel/'.$bahannama)}}">
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                          <?php
                                          $json_images = json_decode($h->json_images, true);


                                          $gbr=$json_images[0]; $gbr=str_replace("http://","",$gbr);  $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>

                                            <img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/800/h/600" alt="Image Alternative text" title="{{$h->nama}}" />
                                        </div>
                                        <div class="col-xs-8">
                                            <h5 class="booking-item-title">{{$h->nama}}</h5>
                                            <?php $bintanghotel=$h->bintang;?>
                                            <ul class="icon-group booking-item-rating-stars">
                                              <li><i class="fa fa-star<?php if($bintanghotel<1){print("-o");} ?>"></i>
                                              </li>
                                              <li><i class="fa fa-star<?php if($bintanghotel<2){print("-o");} ?>"></i>
                                              </li>
                                              <li><i class="fa fa-star<?php if($bintanghotel<3){print("-o");} ?>"></i>
                                              </li>
                                              <li><i class="fa fa-star<?php if($bintanghotel<4){print("-o");} ?>"></i>
                                              </li>
                                              <li><i class="fa fa-star<?php if($bintanghotel<5){print("-o");} ?>"></i>
                                              </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                              </a>
                            </li>
                            @endforeach
                            @endif

                        </ul>
                    </div>
                </div>

            </div>
            <div class="gap gap-small"></div>
        </div>

				@section('akhirbody')
         <script type="text/javascript">
         $( "#dialog-confirm" ).hide();

         var sudahoke=0;
         function mintadialog(){
           dialogalert();
         }
         $('#formutama').on('submit', function() {
          dialogalert();
                     return (sudahoke==1);
         });

        <?php $jum=count($rooms);$i=0; ?>
        var infohotel= {
          @foreach($rooms as $r)
            a{{$r['selectedIDroom']}}:{characteristic:"{{$r['characteristic']}}",bed:"{{$r['bed']}}",board:"{{$r['board']}}",price:"{{$r['price']}}",pricenocrypt:"{{rupiah($r['price'])}}"}<?php $i+=1; if($i<$jum){print(",");} ?>
          @endforeach
          };


          <?php $urutkamar=0; ?>
          @while ($urutkamar<$jumlahkamar)
            <?php $urutkamar++; ?>
        function pilihKamar{{$urutkamar}}(selID){
          $('#kamar_kategori_hidden{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board_hidden{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_kategori{{$urutkamar}}').val(infohotel["a"+selID].characteristic);
          $('#kamar_board{{$urutkamar}}').val(infohotel["a"+selID].board);
          $('#kamar_harga_hidden{{$urutkamar}}').val(infohotel["a"+selID].price);
          $('#kamar_harga{{$urutkamar}}').val(infohotel["a"+selID].pricenocrypt);
          $('#isianidroom{{$urutkamar}}').val(selID);
        }
        @endwhile
        </script>
		@endsection

		@endsection
