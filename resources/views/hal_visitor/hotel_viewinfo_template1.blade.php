@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
@parent

<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_2.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row"style="margin-top:50px;">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{url('/')}}">home</a> /</li>
		  				<li><a class="link-blue-2" href="{{url('/')}}/hotel">hotels</a> /</li>
		  				<li><span>detail</span></li>
		  			</ul>
            <h2 class="color-white">{{$namahotel}}</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
            <div class="detail-top slider-wth-thumbs style-2">
          <div class="swiper-container thumbnails-preview" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                    <div class="swiper-wrapper">
                      <?php $fi=0; ?>
                      @foreach($images as $gbr)
                      <div class="swiper-slide <?php  $gbr=str_replace("\\","",$gbr); if($fi==0){ print("active");}  ?>" data-val="{{$fi}}"> <?php $fi+=1;?>
                        <?php
                         $gbr=str_replace("/","-----",$gbr);
                      //  $image = Image::make("http://".$gbr)->resize(770, 455);
                             echo "<img class=\"img-responsive img-full\" src=\"".url('/')."/gambarhttp/".$gbr."/w/770/h/455"."\">";
                              ?>
                      </div>
                      @endforeach

                      </div>
                    <div class="pagination pagination-hidden"></div>
                </div>
                <div class="swiper-container thumbnails" data-autoplay="0"
                data-loop="0" data-speed="500" data-center="0"
                data-slides-per-view="responsive" data-xs-slides="3"
                data-sm-slides="{{count($images)}}" data-md-slides="{{count($images)}}" data-lg-slides="{{count($images)}}"
                data-add-slides="{{count($images)}}">
                    <div class="swiper-wrapper">
              <?php $fi=0; ?>
              @foreach($images as $image)
              <div class="swiper-slide <?php $image=str_replace("\\","",$image); if($fi==0){print("current active");} ?>" data-val="{{$fi}}"><?php $fi+=1;?>
                <img class="img-responsive img-full" src="http://{{$image}}" alt="" style="width: 150px; height: 101.667px;">
              </div>
              @endforeach

            </div>
            <div class="pagination hidden"></div>
          </div>
        </div>
       			<div class="detail-content color-1">

					<div class="detail-content-block">
          <form class="simple-from"  method="POST" action="kirimdatadiri" id="formutama">


            <div class="simple-group">
             <h4 class="small-title">Cari Kamar</h4>
             <div class="row">
               <div class="col-xs-12 col-sm-6">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Checkin</div>
                   <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text"  name="tglcheckin" id="tglcheckin" required="" value="<?php print(date('d-m-Y'));?>">

                                   </div>
                 </div>
               </div>
                 <div class="col-xs-12 col-sm-6">
                   <div class="form-block type-2 clearfix">
                     <div class="form-label color-dark-2">Checkout</div>
                     <div class="input-style-1 b-50 brd-0 type-2 color-3">
                       <input type="text" id="tglcheckout" name="tglcheckout" required=""  value="<?php print(date('d-m-Y'));?>">
                     </div>
                   </div>
                 </div>
               <div class="col-xs-12 col-sm-2">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Kamar</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="jumkamar"name="jumkamar" style="text-align:center;" >1</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#" onclick="setjumkamar('1')" >1</a>
                         <a href="#" onclick="setjumkamar('2')" >2</a>
    												 <a href="#" onclick="setjumkamar('3')" >3</a>
                             <a href="#" onclick="setjumkamar('4')" >4</a>
                             <a href="#" onclick="setjumkamar('5')" >5</a>
                     </span>
										</div>
								 </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Dewasa per kamar</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="jumdewasa"name="jumdewasa" style="text-align:center;" >1</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#" onclick="setdewasa('1')" >1</a>
                         <a href="#" onclick="setdewasa('2')" >2</a>
                     </span>
										</div>
								 </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                 <div class="form-block type-2 clearfix">
                   <div class="form-label color-dark-2">Anak per kamar</div>
                   <div class="drop-wrap drop-wrap-s-4 color-5">
									 <div class="drop">
										<b id="jumanak"name="jumanak" style="text-align:center;" >0</b>
										 <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										 <span>
												 <a href="#" onclick="setanak('0')" >0</a>
												 <a href="#" onclick="setanak('1')" >1</a>
                         <a href="#" onclick="setanak('2')" >2</a>
                     </span>
										</div>
								 </div>
                 </div>
               </div>
               <div class="col-xs-12 col-sm-3">
                 <div class="form-block type-2 clearfix">
                    <div class="form-label color-dark-2">&nbsp;</div>
                    <img id="gifloading" style="padding-left:10px;width:80px;height:80px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                    <input type="button" id="tombolcari" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" onclick="cari()" value="cari">

                 </div>
               </div>


             </div>
            </div>


          </form>
        </div>
				</div>
       		</div>
       		<div class="col-xs-12 col-md-4">
       			<div class="right-sidebar">
              <div class="detail-block bg-dr-blue">
       					<h4 class="color-white">Detail</h4>
       					<div class="details-desc">
							<p class="color-grey-9">Lokasi: <span class="color-white">{{$alamathotel}}</span></p>
              @foreach($fasilitas as $f)
							<p class="color-grey-9">{{$f}}: <span class="color-white">YES</span></p>
              @endforeach
            </div>


       				</div>

              <div class="popular-tours bg-grey-2">
                <h4 class="color-dark-2">Hotel lain di {{$namakota}}</h4>
                @if($dafhot!=null)
                  @foreach($dafhot as $h)
                  <?php $bahannama=$h->nama;
                  $bahannama=str_replace(" ","_",$bahannama);

                   $json_images = json_decode($h->json_images, true);


                   $gbr=$json_images[0]; $gbr=str_replace("http://","",$gbr);  $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                <div class="hotel-small style-2 clearfix">
                  <a class="hotel-img black-hover" href="{{url('/infohotel/'.$bahannama)}}">
                    <img class="img-responsive radius-0" src="{{url('/')}}/gambarhttp/{{$gbr}}/w/120/h/99" alt="">
                    <div class="tour-layer delay-1"></div>
                  </a>
                  <div class="hotel-desc">
                      <h4>{{$h->nama}}</h4>
                    <div class="hotel-loc tt">{{$h->alamat}}</div>
                  </div>
                </div>
                @endforeach
                @endif


              </div>
       			</div>
       		</div>

       	</div>

	</div>
</div>


				@section('akhirbody')
        <script type="text/javascript">
        //variabel
        var tglcheckin_d="";
        var tglcheckin_m="";
        var tglcheckin_y="";

        var tglcheckout_d="";
        var tglcheckout_m="";
        var tglcheckout_y="";

        var tglcheckin="<?php echo date("Y-m-d");?>";
        var tglcheckout="<?php echo date("Y-m-d");?>";



        var jumkamar="1";
        var jumadt="1";
        var jumchd="0";
        var juminf="0";
        $('#gifloading').hide();



        var urljadwal="{{ url('/') }}/carikamarhotel/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#pilihan_kamar input').on('change', function() {
          jumkamar=$('input[name=options]:checked', '#pilihan_kamar').val();
        });
            $('#pilihan_adt input').on('change', function() {
              jumadt=$('input[name=options]:checked', '#pilihan_adt').val();

            });
            $('#pilihan_chd input').on('change', function() {
              jumchd=$('input[name=options]:checked', '#pilihan_chd').val();

            });
        function setjumkamar(jum){
          jumkamar=jum;
        }
    function setdewasa(jum){
      jumadt=jum;
    }
function setanak(jum){
  jumchd=jum;
}



        $('#jumkamar').on('change', function() {
        jumkamar=this.value;

        });
        $('#adt').on('change', function() {
        jumadt=this.value;

        });
        $('#chd').on('change', function() {
        jumchd=this.value;

        });
        $('#inf').on('change', function() {
        juminf=this.value;

        });

        $("#tglcheckin").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
            maxDate: '+2y'
        });

        $("#tglcheckout").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            changeMonth: true,
            changeYear: true
        });

        $('#tglcheckin').on('change', function() {

        var values=this.value.split('-');
        tglcheckin_d=values[0];
        tglcheckin_m=values[1];
        tglcheckin_y=values[2];

        tglcheckin=tglcheckin_y+"-"+tglcheckin_m+"-"+tglcheckin_d;

        var selectedDate = new Date(tglcheckin);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime());
                $("#tglcheckout").datepicker( "option", "minDate", endDate );
                $("#tglcheckout").datepicker( "option", "maxDate", '+2y' );
                    settglcheckout($("#tglcheckout").val());

        });
        function settglcheckout(val){
          var values=val.split('-');
          //alert(this.value);
          tglcheckout_d=values[0];
          tglcheckout_m=values[1];
          tglcheckout_y=values[2];

          tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
        }
        $('#tglcheckout').on('change', function() {

                        settglcheckout(this.value);
        });
        function cari(){
        nilpil=0;

        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          $('#gifloading').show();
          $('#tombolcari').hide();
          //alert('haha');
                urljadwalb=urljadwal;
                urljadwalb+="{{$namahotel}}";
                urljadwalb+="/"+"{{$kodedes}}";
                urljadwalb+="/"+tglcheckin;
                urljadwalb+="/"+tglcheckout;
                urljadwalb+="/"+jumkamar;
                urljadwalb+="/one";
                urljadwalb+="/"+jumadt;
                urljadwalb+="/"+jumchd;
                urljadwalb+="/"+juminf;
           //     alert(urljadwalb);
                //akhir urljadwalb+="ac/";

                ambildata_hotel();
           //  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)


                        // alert(urljadwalb);
            }else{
              $('#bahanmodal').dialog({ modal: true });

            }
        }


        var ajaxku_hotel;
        function ambildata_hotel(){
          ajaxku_hotel = buatajax();
          var url=urljadwalb;
          ajaxku_hotel.onreadystatechange=stateChanged_hotel;
          ajaxku_hotel.open("GET",url,true);
          ajaxku_hotel.send(null);
        }
         function stateChanged_hotel(){
           var data;
            if (ajaxku_hotel.readyState==4){
              data=ajaxku_hotel.responseText;
              if(data=="001"){
                alert("Kamar tidak tersedia!");
                 $('#gifloading').hide('slow');
                  $('#tombolcari').show('slow');
               }else{
                alert("Anda akan dibawa ke halaman pemilihan kamar");
                $(location).attr('href',data);
               }
             }
        }


        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }


        </script>
		@endsection

		@endsection
