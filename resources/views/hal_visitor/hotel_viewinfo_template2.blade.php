@extends('layouts.'.$namatemplate)
@section('sebelumtitel')

<script src="{{ URL::asset('asettemplate2/js/require.js')}}"></script>
@endsection
@section('kontenweb')
@parent
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">{{$namahotel}}</h2>
        </div>
        <ul class="breadcrumbs pull-right">
          <li><a href="{{ url('/') }}">HOME</a></li>
          <li><a href="{{ url('/') }}/hotel">Hotel</a></li>
          <li class="active">{{$namahotel}}</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="tab-container style1" id="hotel-main-content">
                    <div class="tab-content">
                        <div id="photos-tab" class="tab-pane fade in active">
                            <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                                <ul class="slides">
                                  @foreach($images as $gbr)
                                  <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                    <li><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/900/h/500" alt="" /></li>
                                  @endforeach
                                </ul>
                            </div>
                            <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                <ul class="slides">
                                  @foreach($images as $gbr)
                                  <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                  <li><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/70/h/70" alt="" /></li>
                                  @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="hotel-features" class="tab-container">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="hotel-description">
                            <div class="long-description">
                              <h2>Fasilitas</h2>
                              <ul class="amenities clearfix style2">
                              @foreach($fasilitas as $f)
                              <li class="col-md-4 col-sm-6">
                                  <div class="icon-box">{{$f}}</div>
                              </li>
                              @endforeach
                              </ul><BR>


                        </div>

                        </div>
                    </div>

                </div>
                <div class="booking-section travelo-box">

                        <div class="booking-form card-information"style="padding-left:7px;">
                            <h2>Cari kamar</h2>
                            <div class="form-group row" style="padding-left:10px">
                                <div class="col-sm-6 col-md-5">
                                  <label>check in</label>
                                  <div class="datepicker-wrap">
                                      <input id="tglcheckin" onchange="setcheckinhotel(this.value);" name="tglcheckin" value="<?php print(date("d-m-Y")); ?>" type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                  </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                  <label>check out</label>
                                  <div class="datepicker-wrap">
                                      <input id="tglcheckout" onchange="setcheckouthotel(this.value);" name="tglcheckout"value="<?php print(date("d-m-Y")); ?>"  type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                  </div>
                                </div>
                            </div>
                            <div class="form-group row" style="padding-left:10px">
                              <div class="col-sm-6 col-md-3">
                                <label>Kamar</label>
                                <div class="selector">
                                    <select class="full-width" id="jumkamar" >
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                        <option value="4">04</option>
                                        <option value="5">05</option>
                                    </select>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3">
                                  <label>Dewasa per kamar</label>
                                  <div class="selector">
                                      <select class="full-width" id="adt" >
                                          <option value="1">01</option>
                                          <option value="2">02</option>
                                      </select>
                                  </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                  <label>Anak per kamar</label>
                                  <div class="selector">
                                      <select class="full-width" id="chd" >
                                          <option value="0">00</option>
                                          <option value="1">01</option>
                                          <option value="2">02</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                  <label>&nbsp;</label>
                                  <img id="gifloading" style="padding-left:10px;width:80px;height:80px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                                  <button  id="tombolcari" type="button" class="full-width btn-medium" onclick="cari()">CARI KAMAR</button>
                                </div>
                            </div>
                        </div>
                        <hr />



                </div>

            </div>

            <div class="sidebar col-md-3 nomobile">
                <article class="detailed-logo">
                    <figure>
                      <?php $gbr=$images[0]; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                      <img width="114" height="85" src="{{url('/')}}/gambarhttp/{{$gbr}}/w/114/h/85" alt="">
                    </figure>
                    <div class="details">
                        <h2 class="box-title">{{$namahotel}}</h2>

                        <p class="description">{{$alamathotel}}</p>
                    </div>
                </article>
                <div class="travelo-box">
                    <h4>Hotel lain di {{$namakota}}</h4>
                    <div class="image-box style14">
                      @if($dafhot!=null)
                        @foreach($dafhot as $h)
                        <?php $bahannama=$h->nama;
                        $bahannama=str_replace(" ","_",$bahannama);
                         ?>
                        <article class="box">
                            <figure>
                                <a href="{{url('/infohotel/'.$bahannama)}}"><img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/63/h/59" alt="" /></a>
                            </figure>
                            <div class="details">
                                <h5 class="box-title"><a href="#">{{$h->nama}}</a></h5>
                            </div>
                        </article>
                        @endforeach
                        @endif

                    </div>
                </div>
                <div class="travelo-box contact-box">
                    <h4>Butuh bantuan?</h4>
                    <p>Costumer Service kami siap melayani</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                        <br>
                        <a class="contact-email" href="#">{{$infowebsite['email']}}</a>
                    </address>
                </div>

            </div>
        </div>
    </div>
</section>
				@section('akhirbody')
         <script type="text/javascript">

         //variabel
         var tglcheckin_d="";
         var tglcheckin_m="";
         var tglcheckin_y="";

         var tglcheckout_d="";
         var tglcheckout_m="";
         var tglcheckout_y="";

         var tglcheckin="<?php echo date("Y-m-d");?>";
         var tglcheckout="<?php echo date("Y-m-d");?>";



         var jumkamar="1";
         var jumadt="1";
         var jumchd="0";
         var juminf="0";
         $('#gifloading').hide();



         var urljadwal="{{ url('/') }}/carikamarhotel/";
         var urljadwalb=urljadwal;
         var dafIDPergi = [];
         var dafBiayaPergi = [];
         var dafHTMLPergi = [];
         var dafIDPulang = [];
         var dafBiayaPulang = [];
         var dafHTMLPulang = [];


         $('#pilihan_kamar input').on('change', function() {
           jumkamar=$('input[name=options]:checked', '#pilihan_kamar').val();
         });
             $('#pilihan_adt input').on('change', function() {
               jumadt=$('input[name=options]:checked', '#pilihan_adt').val();

             });
             $('#pilihan_chd input').on('change', function() {
               jumchd=$('input[name=options]:checked', '#pilihan_chd').val();

             });
         function set_kamar(jum){
           jumkamar=jum;
         }



         $('#jumkamar').on('change', function() {
         jumkamar=this.value;

         });
         $('#adt').on('change', function() {
         jumadt=this.value;

         });
         $('#chd').on('change', function() {
         jumchd=this.value;

         });
         $('#inf').on('change', function() {
         juminf=this.value;

         });

         $("#tglcheckin").datepicker({
             dateFormat: 'dd-mm-yy',
             changeMonth: true,
             changeYear: true,
             minDate: new Date(),
             maxDate: '+2y'
         });

         $("#tglcheckout").datepicker({
             dateFormat: 'dd-mm-yy',
             minDate: new Date(),
             changeMonth: true,
             changeYear: true
         });


         function setcheckinhotel(val){
           		var values=val.split('-');
               var tgl=values[2]+"-"+values[1]+"-"+values[0];
               tglcheckin=tgl;
               $('#tglcheckin').val(values[0]+"-"+values[1]+"-"+values[2]);
               var selectedDate = new Date(tglcheckin);
               var msecsInADay = 86400000;
               var endDate = new Date(selectedDate.getTime());
                       $("#tglcheckout").datepicker( "option", "minDate", endDate );
                       $("#tglcheckout").datepicker( "option", "maxDate", '+2y' );
                           settglcheckout($("#tglcheckout").val());

         }
         function settglcheckout(val){
           var values=val.split('-');
           //alert(this.value);
           tglcheckout_d=values[0];
           tglcheckout_m=values[1];
           tglcheckout_y=values[2];

           tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;
           $('#tglcheckout').val(values[0]+"-"+values[1]+"-"+values[2]);
         }

         function setcheckouthotel(val){
              settglcheckout(val);
         }

         function cari(){
         nilpil=0;

         if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
           $('#gifloading').show();
           $('#tombolcari').hide();
           //alert('haha');
                 urljadwalb=urljadwal;
                 urljadwalb+="{{$namahotel}}";
                 urljadwalb+="/"+"{{$kodedes}}";
                 urljadwalb+="/"+tglcheckin;
                 urljadwalb+="/"+tglcheckout;
                 urljadwalb+="/"+jumkamar;
                 urljadwalb+="/one";
                 urljadwalb+="/"+jumadt;
                 urljadwalb+="/"+jumchd;
                 urljadwalb+="/"+juminf;
            //     alert(urljadwalb);
                 //akhir urljadwalb+="ac/";

                 ambildata_hotel();
            //  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)


                         // alert(urljadwalb);
             }else{
               $('#bahanmodal').dialog({ modal: true });

             }
         }


         var ajaxku_hotel;
         function ambildata_hotel(){
           ajaxku_hotel = buatajax();
           var url=urljadwalb;
           ajaxku_hotel.onreadystatechange=stateChanged_hotel;
           ajaxku_hotel.open("GET",url,true);
           ajaxku_hotel.send(null);
         }
          function stateChanged_hotel(){
            var data;
             if (ajaxku_hotel.readyState==4){
               data=ajaxku_hotel.responseText;
               if(data=="001"){
                 alert("Kamar tidak tersedia!");
                  $('#gifloading').hide('slow');
                   $('#tombolcari').show('slow');
                }else{
                 alert("Anda akan dibawa ke halaman pemilihan kamar");
                 $(location).attr('href',data);
                }
              }
         }


         var ajaxku;
         function ambildata(){
           ajaxku = buatajax();
           var url="{{ url('/') }}/jadwalPesawat";
           //url=url+"?q="+nip;
           //url=url+"&sid="+Math.random();
           ajaxku.onreadystatechange=stateChanged;
           ajaxku.open("GET",url,true);
           ajaxku.send(null);
         }
         function buatajax(){
           if (window.XMLHttpRequest){
             return new XMLHttpRequest();
           }
           if (window.ActiveXObject){
              return new ActiveXObject("Microsoft.XMLHTTP");
            }
            return null;
          }
          function stateChanged(){
            var data;
             if (ajaxku.readyState==4){
               data=ajaxku.responseText;
               if(data.length>0){
                 //document.getElementById("hasilkirim").html = data;

                         $('#hasilkirim').append(data);
                }else{
                 // document.getElementById("hasilkirim").html = "";
                       //   $('#hasilkirim').html("");
                }
              }
         }

        </script>
		@endsection

		@endsection
