@extends('layouts.'.$namatemplate)
@section('sebelumtitel')

<script src="{{ URL::asset('asettemplate2/js/require.js')}}"></script>
@endsection
@section('kontenweb')
@parent




        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a>
                </li>
                <li><a href="{{url('/')}}/hotel">Hotel</a>
                </li>
                <li class="active">{{$namahotel}}</li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="lh1em">{{$namahotel}}</h2>
                         </div>
                           <!--
                        <div class="col-md-5">
                            <p class="booking-item-header-price"><small>Mulai dari</small>  <span class="text-lg"> </span>/malam</p>
                        </div>
                      -->
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-6">
                        <div class="tabbable booking-details-tabbable">

                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                    <div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">

                                        @foreach($images as $gbr)
                                        <?php $asli=$gbr; $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>
                                          <img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/800/h/600" alt="" title="{{$namahotel}}" />
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="booking-item-meta">
                             <div class="booking-item-rating">
                                <ul class="icon-list icon-group booking-item-rating-stars">
                                  <li><i class="fa fa-star<?php if($bintanghotel<1){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<2){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<3){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<4){print("-o");} ?>"></i>
                                  </li>
                                  <li><i class="fa fa-star<?php if($bintanghotel<5){print("-o");} ?>"></i>
                                  </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-11">
                                <h4 class="lh1em">Alamat</h4>
                                <ul class="list booking-item-raiting-summary-list">
                                  <p class="mb30">{{$alamathotel}}</p>
                                </ul>
                                <h4>Fasilitas</h4>
                                <?php $nom=0;?>
                                @foreach($fasilitas as $f)
                                <?php $nom+=1;?>
                               <div class="col-md-6">
                              - {{$f}}
                                </div>
                                 @endforeach


                            </div>


                        </div>

                    </div>
                </div>
                <div class="gap"></div>

                <h3>Pesan sekarang!</h3>

                <div class="row">
                    <div class="col-md-9">
                      <form  method="POST" action="kirimdatadiri" id="formutama">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="totalamount" name="totalamount" value=""  >
                        <input type="hidden" name="fields[code]" value="56345678safs_">

                        <ul class="booking-list">


<h4>Rencana penginapan</h4>
<li>
    <a class="booking-item">
        <div class="row">
          <div class="row" style="padding:5px;">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Checkin</label>
                      <input id="tglcheckin" onchange="setcheckinhotel(this.value);" name="tglcheckin" value="<?php  echo date("d-m-Y"); ?>" class="form-control" type="text" />
                  </div>
                  <div class="form-group">
                      <label>Checkout</label>
                      <input id="tglcheckout" onchange="setcheckouthotel(this.value);" name="tglcheckout"value="<?php  echo date("d-m-Y"); ?>" class="form-control" type="text" />
                  </div>


              </div>
              <div class="col-md-6">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Dewasa per kamar</label>
                      <div  id="pilihan_adt" class="btn-group btn-group-select-num" data-toggle="buttons">

                          <label class="btn btn-primary active">
                              <input type="radio" name="options" value="1" />1</label>
                          <label class="btn btn-primary">
                              <input type="radio" name="options" value="2"  />2</label>


                      </div></div>
                    </div>
                      <div class="col-md-6">
                  <div class="form-group">
                      <label>Anak per kamar</label>
                      <div  id="pilihan_chd" class="btn-group btn-group-select-num" data-toggle="buttons">

                        <label class="btn btn-primary active">
                            <input type="radio" name="options" value="0" />0</label>
                        <label class="btn btn-primary  ">
                              <input type="radio" name="options" value="1" />1</label>
                          <label class="btn btn-primary">
                              <input type="radio" name="options" value="2"  />2</label>


                      </div></div>
                    </div>
                    <div class="col-md-6">
                        <label>Jumlah kamar</label>
                        <div  id="pilihan_kamar" class="btn-group btn-group-select-num" data-toggle="buttons">

                            <label class="btn btn-primary active">
                                <input type="radio" name="options" value="1" />1</label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" value="2"  />2</label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" value="3"  />3</label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" value="4"  />4</label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" value="5"  />5</label>


                        </div>

                    </div>

              </div>

          </div>
          <div class="form-group row" style="padding:5px;">
              <div class="col-sm-6 col-md-5">
                <img id="gifloading" style="padding-left:10px;width:80px;height:80px;" src="{{ URL::asset('img/loading_gif.gif')}}" alt="">
                <button id="tombolcari" type="button" class="full-width btn btn-primary" onclick="cari()">CARI KAMAR</button>
              </div>
          </div>

        </div>
    </a>
</li>
                        </ul>

                      </form>
                    </div>
                    <div class="col-md-3 nomobile">
                      <h4>Hotel lain di {{$namakota}}</h4>
                      <ul class="booking-list">
                        @if($dafhot!=null)
                          @foreach($dafhot as $h)
                          <?php $bahannama=$h->nama;
                          $bahannama=str_replace(" ","_",$bahannama);
                           ?>
                          <li>
                          <a href="{{url('/infohotel/'.$bahannama)}}">
                              <div class="booking-item booking-item-small">
                                  <div class="row">
                                      <div class="col-xs-4">
                                        <?php

                                        $json_images = json_decode($h->json_images, true);

                                        $gbr=$json_images[0]; $gbr=str_replace("http://","",$gbr);  $gbr=str_replace("\\","",$gbr); $gbr=str_replace("/","-----",$gbr);?>

                                          <img src="{{url('/')}}/gambarhttp/{{$gbr}}/w/800/h/600" alt="Image Alternative text" title="{{$h->nama}}" />
                                      </div>
                                      <div class="col-xs-8">
                                          <h5 class="booking-item-title">{{$h->nama}}</h5>
                                          <?php $bintanghotel=$h->bintang;?>
                                          <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star<?php if($bintanghotel<1){print("-o");} ?>"></i>
                                            </li>
                                            <li><i class="fa fa-star<?php if($bintanghotel<2){print("-o");} ?>"></i>
                                            </li>
                                            <li><i class="fa fa-star<?php if($bintanghotel<3){print("-o");} ?>"></i>
                                            </li>
                                            <li><i class="fa fa-star<?php if($bintanghotel<4){print("-o");} ?>"></i>
                                            </li>
                                            <li><i class="fa fa-star<?php if($bintanghotel<5){print("-o");} ?>"></i>
                                            </li>
                                          </ul>
                                      </div>

                                  </div>
                              </div>
                            </a>
                          </li>
                          @endforeach
                          @endif

                      </ul>
                    </div>
                </div>

            </div>
            <div class="gap gap-small"></div>
        </div>

				@section('akhirbody')
         <script type="text/javascript">
         //variabel
         var tglcheckin_d="";
         var tglcheckin_m="";
         var tglcheckin_y="";

         var tglcheckout_d="";
         var tglcheckout_m="";
         var tglcheckout_y="";

         var tglcheckin="<?php echo date("Y-m-d");?>";
         var tglcheckout="<?php echo date("Y-m-d");?>";



         var jumkamar="1";
         var jumadt="1";
         var jumchd="0";
         var juminf="0";
         $('#gifloading').hide();



         var urljadwal="{{ url('/') }}/carikamarhotel/";
         var urljadwalb=urljadwal;
         var dafIDPergi = [];
         var dafBiayaPergi = [];
         var dafHTMLPergi = [];
         var dafIDPulang = [];
         var dafBiayaPulang = [];
         var dafHTMLPulang = [];


         $('#pilihan_kamar input').on('change', function() {
           jumkamar=$('input[name=options]:checked', '#pilihan_kamar').val();
         });
             $('#pilihan_adt input').on('change', function() {
               jumadt=$('input[name=options]:checked', '#pilihan_adt').val();

             });
             $('#pilihan_chd input').on('change', function() {
               jumchd=$('input[name=options]:checked', '#pilihan_chd').val();

             });
         function set_kamar(jum){
           jumkamar=jum;
         }



         $('#jumkamar').on('change', function() {
         jumkamar=this.value;

         });
         $('#adt').on('change', function() {
         jumadt=this.value;

         });
         $('#chd').on('change', function() {
         jumchd=this.value;

         });
         $('#inf').on('change', function() {
         juminf=this.value;

         });

         $("#tglcheckin").datepicker({
           format: 'dd-mm-yyyy',
           startDate: '+0d',
           autoclose: true,
         });

         $("#tglcheckout").datepicker({
           format: 'dd-mm-yyyy',
           startDate: '+0d',
           autoclose: true,
         });


         function setcheckinhotel(val){
           		var values=val.split('-');
               var tgl=values[2]+"-"+values[1]+"-"+values[0];
               tglcheckin=tgl;
               $('#tglcheckin').val(values[0]+"-"+values[1]+"-"+values[2]);

               $('#tglcheckout').data('datepicker').setStartDate(new Date(tglcheckin));
         			var x = new Date(tglcheckin);
         			var y = new Date(tglcheckout);
         			if(x>y){
               $('#tglcheckout').val(val);
         			settglcheckout(tgl);
         			}

         }
         function settglcheckout(val){
           var values=val.split('-');
           //alert(this.value);
           tglcheckout_d=values[0];
           tglcheckout_m=values[1];
           tglcheckout_y=values[2];

           tglcheckout=tglcheckout_y+"-"+tglcheckout_m+"-"+tglcheckout_d;

         }

         function setcheckouthotel(val){
              settglcheckout(val);
         }

         function cari(){
         nilpil=0;

         if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
           $('#gifloading').show();
           $('#tombolcari').hide();
           //alert('haha');
                 urljadwalb=urljadwal;
                 urljadwalb+="{{$namahotel}}";
                 urljadwalb+="/"+"{{$kodedes}}";
                 urljadwalb+="/"+tglcheckin;
                 urljadwalb+="/"+tglcheckout;
                 urljadwalb+="/"+jumkamar;
                 urljadwalb+="/one";
                 urljadwalb+="/"+jumadt;
                 urljadwalb+="/"+jumchd;
                 urljadwalb+="/"+juminf;
            //     alert(urljadwalb);
                 //akhir urljadwalb+="ac/";

                 ambildata_hotel();
            //  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)


                         // alert(urljadwalb);
             }else{
               $('#bahanmodal').dialog({ modal: true });

             }
         }


         var ajaxku_hotel;
         function ambildata_hotel(){
           ajaxku_hotel = buatajax();
           var url=urljadwalb;
           ajaxku_hotel.onreadystatechange=stateChanged_hotel;
           ajaxku_hotel.open("GET",url,true);
           ajaxku_hotel.send(null);
         }
          function stateChanged_hotel(){
            var data;
             if (ajaxku_hotel.readyState==4){
               data=ajaxku_hotel.responseText;
               if(data=="001"){
                 alert("Kamar tidak tersedia!");
                  $('#gifloading').hide('slow');
                   $('#tombolcari').show('slow');
                }else{
                 alert("Anda akan dibawa ke halaman pemilihan kamar");
                 $(location).attr('href',data);
                }
              }
         }


         var ajaxku;
         function ambildata(){
           ajaxku = buatajax();
           var url="{{ url('/') }}/jadwalPesawat";
           //url=url+"?q="+nip;
           //url=url+"&sid="+Math.random();
           ajaxku.onreadystatechange=stateChanged;
           ajaxku.open("GET",url,true);
           ajaxku.send(null);
         }
         function buatajax(){
           if (window.XMLHttpRequest){
             return new XMLHttpRequest();
           }
           if (window.ActiveXObject){
              return new ActiveXObject("Microsoft.XMLHTTP");
            }
            return null;
          }
          function stateChanged(){
            var data;
             if (ajaxku.readyState==4){
               data=ajaxku.responseText;
               if(data.length>0){
                 //document.getElementById("hasilkirim").html = data;

                         $('#hasilkirim').append(data);
                }else{
                 // document.getElementById("hasilkirim").html = "";
                       //   $('#hasilkirim').html("");
                }
              }
         }


        </script>
		@endsection

		@endsection
