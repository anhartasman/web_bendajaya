<div class="col-md-3">
    <aside class="sidebar-right">
        <div class="sidebar-widget">
            <div class="Form">
              &nbsp;
            </div>
        </div>
        <div class="sidebar-widget">
            <h4>Kategori</h4>
            <ul class="icon-list list-category">
              @foreach($kategorisblog as $kategori)
              <?php if($kategori->category!=null){?>
                <li><a href="{{ url('/') }}/blog/kategori/{{$kategori->category}}"><i class="fa fa-angle-right"></i>{{$kategori->category}} <small >({{$kategori->jumkat}})</small></a>
                </li>
                <?Php }?>
                @endforeach

            </ul>
        </div>

        <div class="sidebar-widget">
            <h4>Arsip</h4>
            <ul class="icon-list list-category">
              @foreach($daftahbul as $tahbul)
                <li><a href="{{url('/')}}/blog/{{date("Y/m",strtotime($tahbul->tanggal))}}"><i class="fa fa-angle-right"></i>{{date("M Y",strtotime($tahbul->tanggal))}}</a>
                </li>
              @endforeach

            </ul>
        </div>

    </aside>
</div>
