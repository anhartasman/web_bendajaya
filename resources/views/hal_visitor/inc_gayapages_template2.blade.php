@if ($paginator->lastPage() > 1)
<div class="row" style="width:100%;margin-left:auto; margin-right:auto; ">

    <ul class="social-icons clearfix " style="text-align: center; ">
        <li class="pull-left">
        <a style="width:100px;"  href="{{ $paginator->url($paginator->currentPage()-1) }}" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl">Prev Page</a>
        </li>
<div class=" col-md-offset-5">
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li  >
            <a  style="{{ ($paginator->currentPage() == $i) ? ' background:#01b7f2;' : '' }}"href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
</div>

        <li class=" pull-right">
           <a style="width:100px;" href="{{ $paginator->url($paginator->currentPage()+1) }}" class="next" >Next Page</a>
        </li>
           </ul>
</div>
@endif
