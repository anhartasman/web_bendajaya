@if ($paginator->lastPage() > 1)

  <ul class="pagination">
        <li class="prev">
        <a  href="{{ $paginator->url($paginator->currentPage()-1) }}" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl">Prev Page</a>
        </li>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li  class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}" >
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor 

        <li class="next">
           <a  href="{{ $paginator->url($paginator->currentPage()+1) }}" class="next" >Next Page</a>
        </li>
           </ul>
@endif
