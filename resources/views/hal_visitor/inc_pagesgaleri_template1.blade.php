@if ($paginator->lastPage() > 1)
<div class="c_pagination clearfix" style="height:50px;">


    <ul class="cp_content color-3">
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    </ul>

</div>
@endif
