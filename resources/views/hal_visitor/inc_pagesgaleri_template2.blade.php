@if ($paginator->lastPage() > 1)
<ul class="pagination clearfix">
    @if($paginator->currentPage()>1)
    <li class="prev"><a href="{{ $paginator->url($paginator->currentPage()-1) }}">Previous</a></li>
    @else
    <li class="prev disabled"><a href="#">Previous</a></li>
    @endif
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
    <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}"><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
    @endfor
        @if($paginator->currentPage()<$paginator->lastPage())
    <li class="next"><a href="{{ $paginator->url($paginator->currentPage()+1) }}">Next</a></li>
    @else
    <li class="next disabled"><a href="{{ $paginator->url($paginator->currentPage()+1) }}">Next</a></li>
    @endif
  </ul>
@endif
