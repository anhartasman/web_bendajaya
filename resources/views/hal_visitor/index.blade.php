@extends('layouts.master')
@section('kontenweb')

	<div class="top-baner swiper-animate arrows">
			<div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">
				<div class="swiper-wrapper">
<!--HAHAHAHA -->
<?php $nomban=0; ?>
					@foreach($fotos as $foto)
					<div class="swiper-slide active" data-val="{{$nomban}}" >
					  <div class="clip">
						 <div class="bg bg-bg-chrome act" style="background-image:url({{ url('/') }}/uploads/images/{{$foto->gambar}})">
						 </div>
					  </div>
						<div class="vertical-align">
						  <div class="container">
							<div class="row">
							  <div class="col-md-12">
								<div class="main-title vert-title">
								  <h1 class="color-white delay-1">{{$foto->namabanner}}</h1>
									<p class="color-white-op delay-2">{{$foto->keterangan}}</p>
									 </div>
							   </div>
							  </div>
							</div>
						 </div>
					</div>
					<?php $nomban+=1; ?>
					@endforeach

				</div>
				  <div class="pagination pagination-hidden poin-style-1"></div>
			</div>
				  <div class="arrow-wrapp m-200">
					<div class="cont-1170">
						<div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
						<div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
					</div>
				  </div>

                  <div class="baner-tabs">
                   <div class="text-center">
                     <div class="drop-tabs">
                       <b>hotels</b>
                        <a href="#" class="arrow-down"><i class="fa fa-angle-down"></i></a>
						 <ul class="nav-tabs tpl-tabs tabs-style-1">
							<li class="active click-tabs"><a href="#one" data-toggle="tab" aria-expanded="false">hotels</a></li>
							<li class="click-tabs"><a href="#two" data-toggle="tab" aria-expanded="false">flights</a></li>
							<li class="click-tabs"><a href="#three" data-toggle="tab" aria-expanded="false">cars</a></li>
							<li class="click-tabs"><a href="#four" data-toggle="tab" aria-expanded="false">CRUISES</a></li>
							<li class="click-tabs"><a href="#five" data-toggle="tab" aria-expanded="false">activities</a></li>
						 </ul>
                     </div>
	               </div>
		             <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
						<div class="tab-pane active in" id="one">
                            <div class="container">
                            	<div class="row">
                            		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                           		      <div class="tabs-block">
                            		    <h5>Your Destinationss</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/loc_icon_small.png" alt="">
                            		      	   <input type="text" placeholder="Enter a destination or hotel name">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check In</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check Out</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Kids</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 kids</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 kids</a>
															<a href="#">02 kids</a>
															<a href="#">03 kids</a>
															<a href="#">04 kids</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Adults</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 adult</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 adult</a>
															<a href="#">02 adult</a>
															<a href="#">03 adult</a>
															<a href="#">04 adult</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Rooms</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 room</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 room</a>
															<a href="#">02 room</a>
															<a href="#">03 room</a>
															<a href="#">04 room</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            		  <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>

                            		</div>
                            	</div>
                            </div>
						</div>
						<div class="tab-pane" id="two">
                            <div class="container">
                            	<div class="row">
                            		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                           		      <div class="tabs-block">
                            		    <h5>Your Destinationss</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/loc_icon_small.png" alt="">
                            		      	   <input type="text" placeholder="Enter a destination or flight name">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check In</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check Out</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Kids</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 kids</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 kids</a>
															<a href="#">02 kids</a>
															<a href="#">03 kids</a>
															<a href="#">04 kids</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Adults</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 adult</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 adult</a>
															<a href="#">02 adult</a>
															<a href="#">03 adult</a>
															<a href="#">04 adult</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Rooms</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 room</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 room</a>
															<a href="#">02 room</a>
															<a href="#">03 room</a>
															<a href="#">04 room</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            		  <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
                            		</div>
                            	</div>
                            </div>
						</div>
						<div class="tab-pane" id="three">
                            <div class="container">
                            	<div class="row">
                            		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                           		      <div class="tabs-block">
                            		    <h5>Your Destinationss</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/loc_icon_small.png" alt="">
                            		      	   <input type="text" placeholder="Enter a destination or car name">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check In</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check Out</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Kids</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 kids</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 kids</a>
															<a href="#">02 kids</a>
															<a href="#">03 kids</a>
															<a href="#">04 kids</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Adults</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 adult</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 adult</a>
															<a href="#">02 adult</a>
															<a href="#">03 adult</a>
															<a href="#">04 adult</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Rooms</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 room</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 room</a>
															<a href="#">02 room</a>
															<a href="#">03 room</a>
															<a href="#">04 room</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            		  <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
                            		</div>
                            	</div>
                            </div>
						</div>
						<div class="tab-pane" id="four">
                            <div class="container">
                            	<div class="row">
                            		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                           		      <div class="tabs-block">
                            		    <h5>Your Destinationss</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/loc_icon_small.png" alt="">
                            		      	   <input type="text" placeholder="Enter a destination or cruise name">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check In</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check Out</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Kids</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 kids</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 kids</a>
															<a href="#">02 kids</a>
															<a href="#">03 kids</a>
															<a href="#">04 kids</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Adults</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 adult</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 adult</a>
															<a href="#">02 adult</a>
															<a href="#">03 adult</a>
															<a href="#">04 adult</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Rooms</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 room</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 room</a>
															<a href="#">02 room</a>
															<a href="#">03 room</a>
															<a href="#">04 room</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            		  <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
                            		</div>
                            	</div>
                            </div>
						</div>
						<div class="tab-pane" id="five">
                            <div class="container">
                            	<div class="row">
                            		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                           		      <div class="tabs-block">
                            		    <h5>Your Destinationss</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/loc_icon_small.png" alt="">
                            		      	   <input type="text" placeholder="Enter a destination or activities name">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check In</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                            		  <div class="tabs-block">
                            		    <h5>Check Out</h5>
                            		      <div class="input-style">
                            		      	 <img src="img/calendar_icon.png" alt="">
                            		      	   <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                            		      </div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Kids</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 kids</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 kids</a>
															<a href="#">02 kids</a>
															<a href="#">03 kids</a>
															<a href="#">04 kids</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Adults</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 adult</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 adult</a>
															<a href="#">02 adult</a>
															<a href="#">03 adult</a>
															<a href="#">04 adult</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            		  <div class="tabs-block">
                            		    <h5>Rooms</h5>
                            		       <div class="drop-wrap">
											  <div class="drop">
												 <b>01 room</b>
													<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
														<span>
															<a href="#">01 room</a>
															<a href="#">02 room</a>
															<a href="#">03 room</a>
															<a href="#">04 room</a>
														</span>
											   </div>
											</div>
                            		  </div>
                            		</div>
                            		<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            		  <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
                            		</div>
                            	</div>
                            </div>
						</div>
					</div>
			    </div>
	</div>



 </div>
</div>

@endsection
