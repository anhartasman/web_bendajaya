@extends('layouts.'.$namatemplate)

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
@parent

<div class="top-baner map-baner">
<div id="map-canvas" class="style-1" data-lat="34.0531457" data-lng="-118.2126053" data-zoom="12" data-style="4">
  <div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1" >
    <div class="swiper-wrapper">


  <?php $nomban=0; ?>
      @foreach($fotos as $foto)
      <div class="swiper-slide active"  data-val="{{$nomban}}" >
        <div class="clip">
         <div class="bg bg-bg-chrome act" style="background-image:url({{ url('/') }}/uploads/images/{{$foto->gambar}})">
         </div>
        </div>
        <div class="vertical-align">
          <div class="container">
          <div class="row">
            <div class="col-md-12">
            <div class="main-title vert-title">
              <h1 class="color-white delay-1"> </h1>
              <p class="color-white-op delay-2"> </p>
               </div>
             </div>
            </div>
          </div>
         </div>
      </div>
      <?php $nomban+=1; ?>
      @endforeach

    </div>
      <div class="pagination pagination-hidden poin-style-1"></div>
  </div>

</div>




   <div class="container">
    <form action="#" class="hotel-filter">
       <div class="baner-bar cars-bar">
<div class="text-center">
<div class="drop-tabs">
<b>flights</b>
<a href="#" class="arrow-down"><i class="fa fa-angle-down"></i></a>
<ul class="nav-tabs tpl-tabs tabs-style-1">
<li class="active click-tabs" ><a href="#one" data-toggle="tab" aria-expanded="false">pesawat</a></li>
<li class="click-tabs" ><a href="#two" data-toggle="tab" aria-expanded="false">kereta</a></li>
<li class="click-tabs" ><a href="#three" data-toggle="tab" aria-expanded="false">hotel</a></li>
</ul>
</div>
</div>




        <div id="kotakbiru"  class="tab-content tpl-tabs-cont section-text t-con-style-1">
<div class="row text-center" style="color:#fff; padding:5px; width:100%">
 Cari tiket pesawat murah & promo secara online dengan cepat dan mudah di sini!
</div>
          <div class="tab-pane  active" id="one">
            <div class="container" >

                     <div class="row">
                       <div  class="col-md-3" style="background-color:#fff;">
                          <div class="hotels-block">
                         <h4>Asal</h4>
                       <div class="input-style">
                        <select  class="input-style selektwo" name="asal" id="flight_pilasal">
                           <option value=""></option>
                            @foreach($dafban as $area)
                           <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                           @endforeach
                         </select></div>
                       </div>
                       </div>

                       <div  class="col-md-3" style="background-color:#fff;">
                          <div class="hotels-block">
                         <h4>Tujuan</h4>
                       <div class="input-style-1">
                        <select class="input-style selektwo" name="tujuan" id="flight_piltujuan">
                            <option value=""></option>
                            @foreach($dafban as $area)
                           <option value="{{ $area->nama }}-{{ $area->kode }}">{{ $area->nama }} - {{ $area->kode }}</option>
                           @endforeach
                         </select></div>
                       </div>
                       </div>
                     </div>
                     <div class="row" >
              				   <div class="timePiker"></div>
              					<div class="col-md-2" >
              					  <div class="hotels-block">
              					   <h4>Tanggal Berangkat</h4>
              						<div class="input-style-1">
              							<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
              							  <input id="flight_tglberangkat"  type="text" placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
              						</div>
              					  </div>
              					</div>
                       <div class="col-md-2" >
              					  <div class="hotels-block">
              					   <h4>Perjalanan</h4>
                       <input id="flight_togglerencana"  checked data-toggle="toggle" data-on="Sekali jalan" data-off="Pulang pergi" data-onstyle="success" data-offstyle="default" type="checkbox">
                    </div>
                    </div>
             					<div class="col-md-2" id="flight_tny_ret">
             					  <div class="hotels-block">
             					   <h4>Tanggal Pulang</h4>
             						<div class="input-style-1">
             							<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
             							  <input type="text" id="flight_tglpulang"  placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
             						</div>
             					  </div>
             					</div>


             				</div>



                       <div class="row">
                         <div class="col-md-1">
                           <div class="hotels-block">
                            <h4>Dewasa</h4>
                           <div class="input-style-1">
                             <select class="mainselection" id="flight_adt" name="adt" style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="4">4</option>
                               <option value="5">5</option>
                               <option value="6">6</option>
                               <option value="7">7</option>

                             </select></div>
                           </div>
                         </div>
                         <div class="col-md-1">
                           <div class="hotels-block">
                            <h4>Anak</h4>
                           <div class="time-input">
                             <select class="mainselection"id="flight_chd" name="chd"style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                               <option value="0">0</option>
                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="4">4</option>
                               <option value="5">5</option>
                               <option value="6">6</option>

                             </select>	</div>
                           </div>
                         </div>
                         <div class="col-md-1"  >
                           <div class="hotels-block">
                            <h4>Bayi</h4>
                           <div class="input-style-1">
                             <select class="mainselection"id="flight_inf" name="inf"style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                               <option value="0">0</option>
                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="4">4</option>
                               <option value="5">5</option>
                               <option value="6">6</option>

                             </select>
                           </div>
                           </div>
                         </div>

                      </div>
                      <div class="row" style="margin-bottom:10px;">
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                          <input id="flight_tomcari" onclick="cari_flight()"class="c-button b-60 bg-white hv-orange" type="button" value="search now">

                        </div>
                      </div>

            </div>
          </div>
   <div class="tab-pane" id="two">
     <div class="container" >

              <div class="row">
                <div class="col-md-3" style="background-color:#fff;">
                   <div class="hotels-block">
                  <h4>Asal</h4>
                <div class="input-style">
                 <select style="width:100%" class="input-style selektwo" name="asala" id="train_pilasal">
                    <option value=""></option>
                    @foreach($dafstat as $area)
                   <option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                   @endforeach
                  </select></div>
                </div>
                </div>

                <div  class="col-md-3" style="background-color:#fff;">
                   <div class="hotels-block">
                  <h4>Tujuan</h4>
                <div class="input-style-1">
                 <select  style="width:100%" class="input-style selektwo" name="tujuan" id="train_piltujuan">
                   <option value=""></option>
                   @foreach($dafstat as $area)
                  <option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                  @endforeach
                  </select></div>
                </div>
                </div>
              </div>

              <div class="row" >
                 <div class="timePiker"></div>
                <div class="col-md-2" >
                  <div class="hotels-block">
                   <h4>Tanggal Berangkat</h4>
                  <div class="input-style-1">
                    <img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
                      <input id="train_tglberangkat"  type="text" placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
                  </div>
                  </div>
                </div>
                <div class="col-md-2" >
                   <div class="hotels-block" >
                    <h4>Perjalanan</h4>
                    <input id="train_togglerencana"   checked data-toggle="toggle" data-on="Sekali jalan" data-off="Pulang pergi" data-onstyle="success" data-offstyle="default" type="checkbox">
                    </div>
             </div>
               <div class="col-md-2" id="train_tny_ret">
                 <div class="hotels-block">
                  <h4>Tanggal Pulang</h4>
                 <div class="input-style-1">
                   <img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
                     <input type="text" id="train_tglpulang"  placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
                 </div>
                 </div>
               </div>


             </div>



                <div class="row">
                  <div class="col-md-1">
                    <div class="hotels-block">
                     <h4>Dewasa</h4>
                    <div class="input-style-1">
                      <select class="mainselection" id="train_adt" name="adt" style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>

                      </select></div>
                    </div>
                  </div>
                  <div class="col-md-1"  >
                    <div class="hotels-block">
                     <h4>Bayi</h4>
                    <div class="input-style-1">
                      <select class="mainselection"id="train_inf" name="inf"style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>

                      </select>
                    </div>
                    </div>
                  </div>

               </div>
               <div class="row" style="margin-bottom:10px;">
                 <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                   <input id="train_tomcari" onclick="cari_train()"class="c-button b-60 bg-white hv-orange" type="button" value="search now">

                 </div>
               </div>

     </div>
   </div>
   <div class="tab-pane" id="three">
     <div class="container" >

              <div class="row">
                <div class="col-md-2" >
                   <div class="hotels-block" >
                    <h4>Destinasi</h4>
                    <input id="hotel_toggledestinasi"   checked data-toggle="toggle" data-on="Domestik" data-off="Internasional" data-onstyle="success" data-offstyle="default" type="checkbox">
                    </div>
             </div>

                <div  class="col-md-3" style="background-color:#fff;">
                   <div class="hotels-block">
                  <h4>Tujuan</h4>
                <div class="input-style-1">
                 <select  style="width:100%" class="input-style selektwo" name="pildes" id="pildes">

                  </select></div>
                </div>
                </div>
                <div class="col-md-1">
                  <div class="hotels-block">
                   <h4>Kamar</h4>
                  <div class="input-style-1">
                    <select class="mainselection" id="pilkamar" name="pilkamar" style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">
                      <option value="1" selected="selected">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>

                    </select></div>
                  </div>
                </div>

              </div>

              <div class="row" >
                 <div class="timePiker"></div>
                <div class="col-md-2" >
                  <div class="hotels-block">
                   <h4>Checkin</h4>
                  <div class="input-style-1">
                    <img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
                      <input id="checkin"  name="checkin"  type="text" placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
                  </div>
                  </div>
                </div>

               <div class="col-md-2" id="train_tny_ret">
                 <div class="hotels-block">
                  <h4>Checkout</h4>
                 <div class="input-style-1">
                   <img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
                     <input type="text"  id="checkout" name="checkout"   placeholder="" class="datepicker" value="<?php echo date("d-m-Y");?>" required>
                 </div>
                 </div>
               </div>


             </div>



                <div class="row">
                  <div class="col-md-1">
                    <div class="hotels-block">
                     <h4>Dewasa</h4>
                    <div class="input-style-1">
                      <select class="mainselection" id="hotel_adt" name="hotel_adt" style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                        <option value="1">1</option>
                        <option value="2">2</option>

                      </select></div>
                    </div>
                  </div>
                  <div class="col-md-1"  >
                    <div class="hotels-block">
                     <h4>Anak</h4>
                    <div class="input-style-1">
                      <select class="mainselection"id="hotel_chd" name="hotel_inf"style="width:50px;padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>

                      </select>
                    </div>
                    </div>
                  </div>

               </div>
               <div class="row" style="margin-bottom:10px;">
                 <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                   <input id="hotel_tomcari" onclick="cari_hotel()"class="c-button b-60 bg-white hv-orange" type="button" value="search now">

                 </div>
               </div>

     </div>
   </div>


 </div>


   </div>
   </form>
</div>

</div>
<div class="main-wraper hotel-items">
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="second-title">
     </div>
      </div>
    </div>




</div>
</div>
@section('akhirbody')
<script type="text/javascript">
//variabel

var hargapulang="0";
var hargapergi="0";
var nilpil=0;
var idpilper="";
var idpilret="";
var banasal_flight="CGK";
var kotasal_flight="Jakarta";
var banasal_train="CGK";
var kotasal_train="Jakarta";
var banasal="CGK";
var kotasal="Jakarta";
var labelasal="";
var kottujuan_flight="Manokwari";
var bantujuan_flight="MKW";
var kottujuan_train="Manokwari";
var bantujuan_train="MKW";
var kottujuan="Manokwari";
var bantujuan="MKW";
var labeltujuan="";
var pilrencana="O";
var pilrencana_flight="O";
var pilrencana_train="O";
var pilrencana2="O";
var tglber_d="";
var tglber_m="";
var tglber_y="";

var tglpul_d="";
var tglpul_m="";
var tglpul_y="";

var tglberangkat="<?php echo date("Y-m-d");?>";
var tglpulang="<?php echo date("Y-m-d");?>";
var tglberangkat_flight="<?php echo date("Y-m-d");?>";
var tglpulang_flight="<?php echo date("Y-m-d");?>";
var tglberangkat_train="<?php echo date("Y-m-d");?>";
var tglpulang_train="<?php echo date("Y-m-d");?>";

var jumadt_flight="1";
var jumchd_flight="0";
var juminf_flight="0";

var jumadt_train="1";
var jumchd_train="0";
var juminf_train="0";

var jumadt="1";
var jumchd="0";
var juminf="0";

var urljadwal="{{ url('/') }}/jadwalPesawat/";
var urljadwalb=urljadwal;

$('#kategori').select2({  tags:[],tokenSeparators: [",", " "],maximumSelectionSize: 1});
$("#kategori").select2({
      placeholder: "Owner ID",
      tags: [],
      tokenSeparators: [",", " ", ";"],
      maximumInputLength: 12,
      selectOnBlur: true,
      dropdownCssClass: "hiddenSelect2DropDown"
  });

$('#flight_adt').on('change', function() {
jumadt_flight=this.value;

});
$('#flight_chd').on('change', function() {
jumchd_flight=this.value;

});
$('#flight_inf').on('change', function() {
juminf_flight=this.value;

});


$('.selektwo').select2();
$('#flight_tny_ret').hide();


$("#flight_tglberangkat").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#flight_tglpulang").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#flight_tglberangkat').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

tglberangkat_flight=tglber_y+"-"+tglber_m+"-"+tglber_d;

var selectedDate = new Date(tglberangkat_flight);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#flight_tglpulang").datepicker( "option", "minDate", endDate );
        $("#flight_tglpulang").datepicker( "option", "maxDate", '+2y' );

    setflighttglpulang($("#flight_tglpulang").val());
});


    $('#flight_togglerencana').change(function() {
      if(pilrencana_flight=="O"){
        pilrencana_flight="R";
        $('#flight_tny_ret').show();
      }else{
        pilrencana_flight="O";
        $('#flight_tny_ret').hide();
      }
    });
    function setflighttglpulang(val){

      		var values=val.split('-');
      		//alert(this.value);
      		tglpul_d=values[0];
      		tglpul_m=values[1];
      		tglpul_y=values[2];

      		tglpulang_flight=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
   }

$('#flight_tglpulang').on('change', function() {

      setflighttglpulang(this.value);

});

$('#flight_pilasal').on('change', function() {
var values=this.value.split('-');
var kotas=values[1];
kotasal_flight=values[0];

banasal_flight=kotas;
labelasal=this.value;


});
$('#flight_piltujuan').on('change', function() {
var values=this.value.split('-');
var kottuj=values[1];
kottujuan_flight=values[0];

bantujuan_flight=kottuj;
labeltujuan=this.value;

});

$('#flight_pilrencana').on('change', function() {
//alert( this.value );
pilrencana=this.value;

var tny_ret = document.getElementById("flight_tny_ret").value;

if(this.value=="O"){
  $('#flight_tny_ret').hide();
}else{
  $('#flight_tny_ret').show();
}

});

$('#train_tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
}).val();
$('#train_adt').on('change', function() {
jumadt_train=this.value;

});
$('#train_chd').on('change', function() {
jumchd_train=this.value;

});
$('#train_inf').on('change', function() {
juminf_train=this.value;

});


$('.selektwo').select2();
$('#train_tny_ret').hide();
$('#train_togglerencana').change(function() {
  if(pilrencana_train=="O"){
    pilrencana_train="R";
    $('#train_tny_ret').show();
  }else{
    pilrencana_train="O";
    $('#train_tny_ret').hide();
  }
});

$("#train_tglberangkat").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#train_tglpulang").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#train_tglberangkat').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

tglberangkat_train=tglber_y+"-"+tglber_m+"-"+tglber_d;



var selectedDate = new Date(tglberangkat_train);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#train_tglpulang").datepicker( "option", "minDate", endDate );
        $("#train_tglpulang").datepicker( "option", "maxDate", '+2y' );

    settraintglpulang($("#train_tglpulang").val());
});

function settraintglpulang(val){

      var values=val.split('-');
      //alert(this.value);
      tglpul_d=values[0];
      tglpul_m=values[1];
      tglpul_y=values[2];

      tglpulang_train=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
}

$('#train_tglpulang').on('change', function() {

  settraintglpulang(this.value);
});

$('#train_pilasal').on('change', function() {
var values=this.value.split('-');
var kotas=values[1];
kotasal_train=values[0];

banasal_train=kotas;
labelasal=this.value;


});
$('#train_piltujuan').on('change', function() {
var values=this.value.split('-');
var kottuj=values[1];
kottujuan_train=values[0];

bantujuan_train=kottuj;
labeltujuan=this.value;

});
$('#train_pilrencana').on('change', function() {
//alert( this.value );
pilrencana_train=this.value;
var tny_ret = document.getElementById("train_tny_ret").value;

if(this.value=="O"){
  $('#train_tny_ret').hide();
}else{
  $('#train_tny_ret').show();
}

});

function cari_flight(){

if((Number(jumadt_flight)+Number(jumchd_flight)+Number(juminf_flight))<=7){
  $(location).attr('href', '{{ url('/') }}/flight/otomatiscari/'+banasal_flight+'/'+bantujuan_flight+'/'+pilrencana_flight+'/'+tglberangkat_flight+'/'+tglpulang_flight+'/'+jumadt_flight+'/'+jumchd_flight+'/'+juminf_flight)

}
}

function cari_train(){

if((Number(jumadt_train)+Number(jumchd_train)+Number(juminf_train))<=4){
  $(location).attr('href', '{{ url('/') }}/train/otomatiscari/'+banasal_train+'/'+bantujuan_train+'/'+pilrencana_train+'/'+tglberangkat_train+'/'+tglpulang_train+'/'+jumadt_train+'/'+jumchd_train+'/'+juminf_train)

}
}
var checkinhotel="<?php echo date("Y-m-d");?>";
var checkouthotel="<?php echo date("Y-m-d");?>";

$("#checkin").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#checkout").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#checkin').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

checkinhotel=tglber_y+"-"+tglber_m+"-"+tglber_d;

var selectedDate = new Date(checkinhotel);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#checkout").datepicker( "option", "minDate", endDate );
        $("#checkout").datepicker( "option", "maxDate", '+2y' );

    setcheckout($("#checkout").val());

});
function setcheckout(val){

      var values=val.split('-');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      checkouthotel=tglber_y+"-"+tglber_m+"-"+tglber_d;
}
$('#checkout').on('change', function() {

        setcheckout(this.value);
});
var kodedes="";
$('#pildes').on('change', function() {
  var values=this.value.split('-');
  var kode=values[0];
  kotdes=values[1];
  kodedes=kode;
  labeltujuan=this.value;

});

  var wilayah=1;
$('#hotel_toggledestinasi').change(function() {
  if(wilayah==1){
    pilihwilayah(2)
  }else{
    pilihwilayah(1);
  }
});
function cari_hotel(){
  var des=$('#pildes').val();
  var kamar=$('#pilkamar').val();
  var adt=$('#hotel_adt').val();
  var chd=$('#hotel_chd').val();
  var inf=0;
  var checkin=checkinhotel;
  var checkout=checkouthotel;
  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)
  //alert(checkin);
}

var daftardaerah= {
               domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
               internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

           };
function pilihwilayah(i){
  wilayah=i;
  var ambildaftar=null;
  if(i==1){
    ambildaftar=daftardaerah.domestik;
  }else{
    ambildaftar=daftardaerah.internasional;
  }
  var optionsAsString = "<option value=\"\"></option>";
        for(var i = 0; i < ambildaftar.length; i++) {
          optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
          optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
        }
        $( '#pildes' ).html( optionsAsString );
}

    if (isMobile.matches==false) {
       $( '#kotakbiru' ).width(700 );
          $( '.selektwo' ).width( 500 );
    }

   //$( '#train_togglerencana' ).width($( '#flight_togglerencana' ).width());
pilihwilayah(1);

$( document ).ready(function() {

                 if (isMobile.matches) {
              $( '.toggle' ).width('80%' );
                 }else{
              $( '.toggle' ).width(140 );
            }
});

</script>
@endsection
@endsection
