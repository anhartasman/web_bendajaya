@extends('layouts.'.$namatemplate)

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2front.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
@parent
	<div class="top-baner swiper-animate arrows">
			<div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1" >
				<div class="swiper-wrapper">


<?php $nomban=0; ?>
					@foreach($fotos as $foto)
					<div class="swiper-slide active"  data-val="{{$nomban}}" >
					  <div class="clip">
						 <div class="bg bg-bg-chrome act" style="background-image:url({{ url('/') }}/uploads/images/{{$foto->gambar}})">
						 </div>
					  </div>
						<div class="vertical-align">
						  <div class="container">
							<div class="row">
							  <div class="col-md-12">
								<div class="main-title vert-title">
								  <h1 class="color-white delay-1">{{$foto->namabanner}}</h1>
									<p class="color-white-op delay-2">{{$foto->keterangan}}</p>
									 </div>
							   </div>
							  </div>
							</div>
						 </div>
					</div>
					<?php $nomban+=1; ?>
					@endforeach

				</div>
				  <div class="pagination pagination-hidden poin-style-1"></div>
			</div>
				  <div class="arrow-wrapp m-200">
					<div class="cont-1170">
						<div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
						<div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
					</div>
				  </div>

                  <div class="baner-tabs style-2">
                   <div class="text-center">
                     <div class="drop-tabs">
                         <b>flights</b>
                        <a href="#" class="arrow-down"><i class="fa fa-angle-down"></i></a>
						 <ul class="nav-tabs tpl-tabs tabs-style-1">
						 <li class="active click-tabs" ><a href="#one" data-toggle="tab" aria-expanded="false">pesawat</a></li>
						 <li class="click-tabs" ><a href="#two" data-toggle="tab" aria-expanded="false">kereta</a></li>
             <li class="click-tabs" ><a href="#three" data-toggle="tab" aria-expanded="false">hotel</a></li>
						  </ul>
                     </div>
	               </div>
		             <div class="tab-content tpl-tabs-cont section-text t-con-style-1">

                   <div class="tab-pane active" id="one">
                     <div class="container">

                              <div class="row">
                                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                  <div class="tabs-block ">
                                  <h5>Asal</h5>
                                    <div class="input-style">
                                    <select class="input-style selektwo" name="asal" id="flight_pilasal">
                                      <option value=""></option>
                                       @foreach($dafban as $area)
                                      <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                      @endforeach
                                    </select> </div>
                                  </div>
                                </div>
                                  <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                    <div class="tabs-block">
                                    <h5>Tujuan</h5>
                                      <div class="input-style">
                                      <select class="input-style selektwo" name="tujuan" id="flight_piltujuan">
                                         <option value=""></option>
                                         @foreach($dafban as $area)
                                        <option value="{{ $area->nama }}-{{ $area->kode }}">{{ $area->nama }} - {{ $area->kode }}</option>
                                        @endforeach
                                      </select> </div>
                                    </div>
                                  </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                      <div class="tabs-block">
                                      <h5>Rencana</h5>
                                        <div class="input-style">
                                        <select class="mainselection2" id="flight_pilrencana" name="pilrencana">
                                          <option value="O" selected="selected">Sekali Jalan</option>
                                          <option value="R">Pulang Pergi</option>

                                        </select> </div>
                                      </div>
                                    </div>
                                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                                  <div class="tabs-block">
                                  <h5>Tanggal Berangkat</h5>
                                    <div class="input-style">
                                     <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                       <input id="flight_tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                                    </div>
                                  </div>
                                </div>
                                <div id="flight_tny_ret" class="tglpulang col-lg-2 col-md-4 col-sm-4 col-xs-6">
                                  <div class="tabs-block">
                                  <h5>Tanggal Pulang</h5>
                                    <div class="input-style">
                                     <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                       <input id="flight_tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                                    </div>
                                  </div>
                                </div>

                              </div>
                                <div class="row">

                                  <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                                    <div class="tabs-block">
                                    <h5>Dewasa</h5>
                                       <select class="mainselection2" id="flight_adt" name="adt" style="">

                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                         <option value="5">5</option>
                                         <option value="6">6</option>
                                         <option value="7">7</option>

                                       </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                                    <div class="tabs-block">
                                    <h5>Anak</h5>
                                          <select class="mainselection2"id="flight_chd" name="chd">

                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>

                                          </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                                    <div class="tabs-block">
                                    <h5>Bayi</h5>
                                          <select class="mainselection2"id="flight_inf" name="inf">

                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>

                                          </select></div>
                                  </div>
                                  <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                                    <div class="tabs-block" >
                                   <input type="submit" id="flight_tomcari" onclick="cari_flight()" class="c-button b-30 bg-red-3 hv-red-3-o" value="search">
                                     </div>
                                  </div>


                        </div>

                     </div>
                   </div>
            <div class="tab-pane" id="two">
              <div class="container">

                       <div class="row">
                         <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                           <div class="tabs-block ">
                           <h5>Asal</h5>
                             <div class="input-style">
                             <select style="width:100%" class="input-style selektwo" name="asal" id="train_pilasal">
                               <option value=""></option>
                               @foreach($dafstat as $area)
                              <option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                              @endforeach
                             </select> </div>
                           </div>
                         </div>
                           <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                             <div class="tabs-block">
                             <h5>Tujuan</h5>
                               <div class="input-style">
                               <select style="width:100%" class="input-style selektwo" name="tujuan" id="train_piltujuan">
                                 <option value=""></option>
                                 @foreach($dafstat as $area)
                                <option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                                @endforeach
                               </select> </div>
                             </div>
                           </div>
                             <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                               <div class="tabs-block">
                               <h5>Rencana</h5>
                                 <div class="input-style">
                                 <select style="width:100%" class="mainselection2" id="train_pilrencana" name="pilrencana">
                                   <option value="O" selected="selected">Sekali Jalan</option>
                                   <option value="R">Pulang Pergi</option>

                                 </select> </div>
                               </div>
                             </div>
                         <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                           <div class="tabs-block">
                           <h5>Tanggal Berangkat</h5>
                             <div class="input-style">
                              <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                <input id="train_tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                             </div>
                           </div>
                         </div>
                         <div id="train_tny_ret" class="tglpulang col-lg-2 col-md-4 col-sm-4 col-xs-6">
                           <div class="tabs-block">
                           <h5>Tanggal Pulang</h5>
                             <div class="input-style">
                              <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                <input id="train_tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                             </div>
                           </div>
                         </div>

                       </div>
                         <div class="row">

                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                             <div class="tabs-block">
                             <h5>Dewasa</h5>
                                <select class="mainselection2" id="train_adt" name="adt">

                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>

                                </select>
                             </div>
                           </div>
                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                             <div class="tabs-block">
                             <h5>Bayi</h5>
                                   <select class="mainselection2"id="train_inf" name="inf">

                                     <option value="0">0</option>
                                     <option value="1">1</option>
                                     <option value="2">2</option>
                                     <option value="3">3</option>
                                     <option value="4">4</option>

                                   </select></div>
                           </div>
                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4" >
                             <div >
                            <input type="submit" id="train_tomcari" onclick="cari_train()" class="c-button b-30 bg-red-3 hv-red-3-o" value="search">
                              </div>
                           </div>


                 </div>

              </div>
            </div>
            <div class="tab-pane" id="three">
              <div class="container">

                       <div class="row">
                         <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                           <div class="tabs-block ">
                           <h5>Destinasi</h5>
                             <div class="input-style">
                             <select style="width:100%" onchange="pilihwilayah(this.value)" class="input-style selektwo" name="wilayah" id="wilayah">
                               <option value="1">Domestik</option>
                               <option value="2">Internasional</option>
                             </select> </div>
                           </div>
                         </div>
                           <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                             <div class="tabs-block">
                             <h5>Tujuan</h5>
                               <div class="input-style">
                               <select  style="width:100%" class="input-style selektwo" name="pildes" id="pildes" >

                               </select> </div>
                             </div>
                           </div>
                             <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                               <div class="tabs-block">
                               <h5>Kamar</h5>
                                 <div class="input-style">
                                 <select class="mainselection2" id="pilkamar" name="pilkamar">
                                   <option value="1" selected="selected">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                 </select>
                                 </div>
                               </div>
                             </div>
                         <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                           <div class="tabs-block">
                           <h5>Checkin</h5>
                             <div class="input-style">
                              <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                <input id="checkin" name="checkin" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                             </div>
                           </div>
                         </div>
                         <div id="train_tny_ret" class="tglpulang col-lg-2 col-md-4 col-sm-4 col-xs-6">
                           <div class="tabs-block">
                           <h5>Checkout</h5>
                             <div class="input-style">
                              <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                                <input id="checkout" name="checkout" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("d-m-Y");?>" >
                             </div>
                           </div>
                         </div>

                       </div>
                         <div class="row">

                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                             <div class="tabs-block">
                             <h5>Dewasa</h5>
                                <select class="mainselection2" id="hotel_adt" name="adt">

                                  <option value="1">1</option>
                                  <option value="2">2</option>

                                </select>
                             </div>
                           </div>
                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                             <div class="tabs-block">
                             <h5>Anak</h5>
                                   <select class="mainselection2" id="hotel_chd" name="chd">

                                     <option value="0">0</option>
                                     <option value="1">1</option>
                                     <option value="2">2</option>

                                   </select>
                             </div>
                           </div>

                           <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4" >
                             <div >
                            <input type="submit" id="hotel_tomcari" onclick="cari_hotel()" class="c-button b-30 bg-red-3 hv-red-3-o" value="search">
                              </div>
                           </div>


                 </div>

              </div>
            </div>


					</div>
			    </div>
	</div>



 </div>
</div>
@section('akhirbody')
<script type="text/javascript">
//variabel

var hargapulang="0";
var hargapergi="0";
var nilpil=0;
var idpilper="";
var idpilret="";
var banasal="CGK";
var kotasal="Jakarta";
var labelasal="";
var kottujuan="Manokwari";
var bantujuan="MKW";
var labeltujuan="";
var pilrencana="O";
var pilrencana2="O";
var tglber_d="";
var tglber_m="";
var tglber_y="";

var tglpul_d="";
var tglpul_m="";
var tglpul_y="";

var tglberangkat="<?php echo date("Y-m-d");?>";
var tglpulang="<?php echo date("Y-m-d");?>";

var jumadt="1";
var jumchd="0";
var juminf="0";

var urljadwal="{{ url('/') }}/jadwalPesawat/";
var urljadwalb=urljadwal;


$('#flight_adt').on('change', function() {
jumadt=this.value;
$('#flight_formgo_adt').val(jumadt);

});
$('#flight_chd').on('change', function() {
jumchd=this.value;
$('#flight_formgo_chd').val(jumchd);

});
$('#flight_inf').on('change', function() {
juminf=this.value;
$('#flight_formgo_inf').val(juminf);

});


$('.selektwo').select2();
$('#flight_tny_ret').hide();


$("#flight_tglberangkat").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#flight_tglpulang").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#flight_tglberangkat').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

var selectedDate = new Date(tglberangkat);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#flight_tglpulang").datepicker( "option", "minDate", endDate );
        $("#flight_tglpulang").datepicker( "option", "maxDate", '+2y' );

    setflighttglpulang($("#flight_tglpulang").val());
});


    function setflighttglpulang(val){

      		var values=val.split('-');
      		//alert(this.value);
      		tglpul_d=values[0];
      		tglpul_m=values[1];
      		tglpul_y=values[2];

      		tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
   }

$('#flight_tglpulang').on('change', function() {

      setflighttglpulang(this.value);

});

$('#flight_pilasal').on('change', function() {
var values=this.value.split('-');
var kotas=values[1];
kotasal=values[0];

banasal=kotas;
labelasal=this.value;


});
$('#flight_piltujuan').on('change', function() {
var values=this.value.split('-');
var kottuj=values[1];
kottujuan=values[0];

bantujuan=kottuj;
labeltujuan=this.value;

});
$('#flight_pilrencana').on('change', function() {
//alert( this.value );
pilrencana=this.value;

var tny_ret = document.getElementById("flight_tny_ret").value;

if(this.value=="O"){
  $('#flight_tny_ret').hide();
}else{
  $('#flight_tny_ret').show();
}

});

$('#train_tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
}).val();
$('#train_adt').on('change', function() {
jumadt=this.value;
$('#train_formgo_adt').val(jumadt);

});
$('#train_chd').on('change', function() {
jumchd=this.value;
$('#train_formgo_chd').val(jumchd);

});
$('#train_inf').on('change', function() {
juminf=this.value;
$('#train_formgo_inf').val(juminf);

});


$('.selektwo').select2();
$('#train_tny_ret').hide();

$("#train_tglberangkat").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#train_tglpulang").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#train_tglberangkat').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;



var selectedDate = new Date(tglberangkat);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#train_tglpulang").datepicker( "option", "minDate", endDate );
        $("#train_tglpulang").datepicker( "option", "maxDate", '+2y' );

    settraintglpulang($("#train_tglpulang").val());
});

function settraintglpulang(val){

      var values=val.split('-');
      //alert(this.value);
      tglpul_d=values[0];
      tglpul_m=values[1];
      tglpul_y=values[2];

      tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
}

$('#train_tglpulang').on('change', function() {

  settraintglpulang(this.value);
});

$('#train_pilasal').on('change', function() {
var values=this.value.split('-');
var kotas=values[1];
kotasal=values[0];

banasal=kotas;
labelasal=this.value;


});
$('#train_piltujuan').on('change', function() {
var values=this.value.split('-');
var kottuj=values[1];
kottujuan=values[0];

bantujuan=kottuj;
labeltujuan=this.value;

});
$('#train_pilrencana').on('change', function() {
//alert( this.value );
pilrencana=this.value;
var tny_ret = document.getElementById("train_tny_ret").value;

if(this.value=="O"){
  $('#train_tny_ret').hide();
}else{
  $('#train_tny_ret').show();
}

});

function cari_flight(){

if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
  $(location).attr('href', '{{ url('/') }}/flight/otomatiscari/'+banasal+'/'+bantujuan+'/'+pilrencana+'/'+tglberangkat+'/'+tglpulang+'/'+jumadt+'/'+jumchd+'/'+juminf)

}
}

function cari_train(){

if((Number(jumadt)+Number(jumchd)+Number(juminf))<=4){
  $(location).attr('href', '{{ url('/') }}/train/otomatiscari/'+banasal+'/'+bantujuan+'/'+pilrencana+'/'+tglberangkat+'/'+tglpulang+'/'+jumadt+'/'+jumchd+'/'+juminf)

}
}
var checkinhotel="<?php echo date("Y-m-d");?>";
var checkouthotel="<?php echo date("Y-m-d");?>";

$("#checkin").datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true,
minDate: new Date(),
maxDate: '+2y'
});

$("#checkout").datepicker({
dateFormat: 'dd-mm-yy',
minDate: new Date(),
changeMonth: true,
changeYear: true
});


$('#checkin').on('change', function() {

var values=this.value.split('-');
//alert(this.value);
tglber_d=values[0];
tglber_m=values[1];
tglber_y=values[2];

checkinhotel=tglber_y+"-"+tglber_m+"-"+tglber_d;

var selectedDate = new Date(checkinhotel);
var msecsInADay = 86400000;
var endDate = new Date(selectedDate.getTime());
        $("#checkout").datepicker( "option", "minDate", endDate );
        $("#checkout").datepicker( "option", "maxDate", '+2y' );

    setcheckout($("#checkout").val());

});
function setcheckout(val){

      var values=val.split('-');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      checkouthotel=tglber_y+"-"+tglber_m+"-"+tglber_d;
}
$('#checkout').on('change', function() {

        setcheckout(this.value);
});
var kodedes="";
$('#pildes').on('change', function() {
  var values=this.value.split('-');
  var kode=values[0];
  kotdes=values[1];
  kodedes=kode;
  labeltujuan=this.value;

});
function cari_hotel(){
  var wilayah=$('#wilayah').val();
  var des=$('#pildes').val();
  var kamar=$('#pilkamar').val();
  var adt=$('#hotel_adt').val();
  var chd=$('#hotel_chd').val();
  var inf=0;
  var checkin=checkinhotel;
  var checkout=checkouthotel;
  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)
  //alert(checkin);
}

var daftardaerah= {
               domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
               internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

           };
function pilihwilayah(i){
  wilayah=i;
  var ambildaftar=null;
  if(i==1){
    ambildaftar=daftardaerah.domestik;
  }else{
    ambildaftar=daftardaerah.internasional;
  }
  var optionsAsString = "<option value=\"\"></option>";
        for(var i = 0; i < ambildaftar.length; i++) {
          optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
          optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
        }
        $( '#pildes' ).html( optionsAsString );
}
pilihwilayah(1);

</script>
@endsection
@endsection
