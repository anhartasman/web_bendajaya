<?php $front=1;?>
@extends('layouts.'.$namatemplate)

@section('sebelumtitel')
 	<link href="{{ URL::asset('asettemplate2/css/select2.min.css')}}" rel="stylesheet" />
	 		<script src="{{ URL::asset('asettemplate2/js/select2.min.js')}}"></script>
      <style>
          section#content {  min-height: 1000px; padding: 0; position: relative; overflow: hidden; }
          #main { padding-top: 200px; }
          .page-title, .page-description { color: #fff; }
          .page-title { font-size: 4.1667em; font-weight: bold; }
          .page-description { font-size: 2.5em; margin-bottom: 50px; }
          .featured { position: absolute; right: 50px; bottom: 50px; z-index: 9; margin-bottom: 0;  text-align: right; }
          .featured figure a { border: 2px solid #fff; }
          .featured .details { margin-right: 10px; }
          .featured .details > * { color: #fff; line-height: 1.25em; margin: 0; font-weight: bold; text-shadow: 2px -2px rgba(0, 0, 0, 0.2); }
      </style>
@endsection
	  @section('kontenweb')

    <section id="content" class="slideshow-bg">
      <div id="slideshow">
          <div class="flexslider">
              <ul class="slides">
                @foreach($fotos as $foto)
                  <li>
                      <div class="slidebg" style="background-image: url('{{ url('/') }}/uploads/images/{{$foto->gambar}}');"></div>
                  </li>
                   @endforeach
              </ul>
          </div>
      </div>
        <div class="container">
            <div id="main" style="padding-top:160px;">
                <h1 class="page-title">Selamat datang!</h1>
                <h2 class="page-description col-md-6 no-float no-padding">Cari tiket pesawat murah & promo secara online dengan cepat dan mudah di sini!</h2>
                <div class="search-box-wrapper style2">
                    <div class="search-box">
                        <ul class="search-tabs clearfix">
                            <li class="active"><a href="#hotels-tab" data-toggle="tab"><i class="soap-icon-hotel"></i><span>HOTELS</span></a></li>
                            <li><a href="#flights-tab" data-toggle="tab"><i class="soap-icon-plane-right takeoff-effect"></i><span>PESAWAT</span></a></li>
                            <li><a href="#flight-and-hotel-tab" data-toggle="tab"><i class="fa fa-train"></i><span>KERETA</span></a></li>
                            <li class="advanced-search visible-lg"><a href="#advanced-search-tab" data-toggle="tab"><span>Cek Pesanan</span></a></li>
                        </ul>
                        <div class="visible-mobile">
                            <ul id="mobile-search-tabs" class="search-tabs clearfix">
                                <li class="active"><a href="#hotels-tab">HOTELS</a></li>
                                <li><a href="#flights-tab">PESAWAT</a></li>
                                <li><a href="#flight-and-hotel-tab">KERETA</a></li>
                                <li><a href="#advanced-search-tab">Cek Pesanan</a></li>
                            </ul>
                        </div>

                        <div class="search-tab-content">
                            <div class="tab-pane fade active in" id="hotels-tab">

                                  <div class="row">
                                    <div class="form-group col-sm-6 col-md-3">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label>Destinasi</label>
                                                <div class="selector">
                                                    <select class="full-width" onchange="pilihwilayah(this.value)" name="wilayah" id="wilayah">
                                                        <option value="1">Domestik</option>
                                                        <option value="2">Internasional</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Wilayah</label>
                                                <select class="full-width selektwo" onchange="setkodedes(this.value)" name="pildes" id="pildes">

                                               </select>
                                            </div>
                                        </div>
                                    </div>

                                      <div class="form-group col-sm-6 col-md-4">
                                          <div class="row">
                                              <div class="col-xs-6">
                                                  <label>Check In</label>
                                                  <div class="datepicker-wrap">
                                                      <input id="checkin" name="checkin" onchange="setcheckinhotel(this.value);"  type="text" class="input-text full-width" placeholder="dd/mm/yy" />
                                                  </div>
                                              </div>
                                              <div class="col-xs-6">
                                                  <label>Check Out</label>
                                                  <div class="datepicker-wrap">
                                                      <input id="checkout" name="checkout" onchange="setcheckouthotel(this.value);"  type="text" class="input-text full-width" placeholder="dd/mm/yy" />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group col-sm-6 col-md-3">
                                          <div class="row">
                                              <div class="col-xs-4">
                                                  <label>Rooms</label>
                                                  <div class="selector">
                                                      <select class="full-width" id="pilkamar">
                                                          <option value="1">01</option>
                                                          <option value="2">02</option>
                                                          <option value="3">03</option>
                                                          <option value="4">04</option>
                                                          <option value="5">05</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-xs-4">
                                                  <label>Adults</label>
                                                  <div class="selector">
                                                      <select class="full-width" id="hotel_adt" >
                                                          <option value="1">01</option>
                                                          <option value="2">02</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-xs-4">
                                                  <label>Kids</label>
                                                  <div class="selector">
                                                      <select class="full-width" id="hotel_chd" >
                                                          <option value="0">00</option>
                                                          <option value="1">01</option>
                                                          <option value="2">02</option>
                                                      </select>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group col-sm-6 col-md-2  ">
                                          <label class="hidden-xs">&nbsp;</label>
                                          <button class="full-width" onclick="cari_hotel()" >SEARCH NOW</button>
                                      </div>
                                  </div>

                            </div>
                            <div class="tab-pane fade" id="flights-tab">

                                    <h4 class="title">Pilih rute keberangkatan</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Asal</label>
                                              <select style="width:100%" class="full-width selektwo" name="asal" id="flight_pilasal">
                                                <option value=""></option>
                                                 @foreach($dafban as $area)
                                                <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Tujuan</label>
                                              <select style="width:100%" class="full-width selektwo" name="tujuan" id="flight_piltujuan">
                                                <option value=""></option>
                                                 @foreach($dafban as $area)
                                                <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Tanggal berangkat</label>
                                            <div class="form-group row">
                                                <div class="col-xs-6">
                                                    <div class="datepicker-wrap">
                                                      <input type="text" class="input-text full-width" placeholder="Tanggal berangkat" onchange="setflight_tglberangkat(this.value);" id="flight_tglberangkat" name="flight_tglberangkat" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="selector">
                                                      <input id="flight_togglerencana"  checked data-toggle="toggle" data-on="Sekali jalan" data-off="Pulang pergi" data-onstyle="success" data-offstyle="default" type="checkbox">

                                                    </div>
                                                </div>
                                            </div>
                                        <div id="flight_tny_ret">
                                                <label>Tanggal pulang</label>
                                            <div class="form-group row" >
                                                <div class="col-xs-6">
                                                    <div class="datepicker-wrap">
                                                        <input value="<?php echo date("d/m/Y");?>"  type="text" class="input-text full-width"  placeholder="Tanggal pulang" onchange="setflight_tglpulang(this.value);" id="flight_tglpulang" name="flight_tglpulang" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <div class="col-xs-3">
                                                        <label>Dewasa</label>
                                                    <div class="selector">
                                                        <select class="full-width" id="flight_adt" name="flight_adt">

                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                            <option value="7">07</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                        <label>Anak</label>
                                                    <div class="selector">
                                                        <select class="full-width" id="flight_chd" name="flight_chd">

                                                            <option value="0">00</option>
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                        <label>Bayi</label>
                                                    <div class="selector">
                                                        <select class="full-width" id="flight_inf" name="flight_inf">

                                                            <option value="0">00</option>
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">

                                                <div class="col-xs-6">
                                                        <label>&nbsp;</label>
                                                    <button class="full-width" onclick="cari_flight()">SERACH NOW</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                            </div>
                            <div class="tab-pane fade" id="flight-and-hotel-tab">
                              <h4 class="title">Pilih rute keberangkatan</h4>
                              <div class="row">
                                  <div class="col-md-4">
                                      <div class="form-group">
                                              <label>Asal </label>
                                        <select style="width:100%" class="full-width selektwo"  name="train_pilasal" id="train_pilasal">
                                          <option value=""> </option>
                                          @foreach($dafstat as $area)
                                         <option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                                         @endforeach
                                        </select>
                                      </div>
                                      <div class="form-group">
                                              <label>Tujuan </label>
                                        <select style="width:100%" class="full-width selektwo" name="tujuan" id="train_piltujuan">
                                          <option value=""> </option>
                                          @foreach($dafstat as $area)
                                         <option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                                         @endforeach
                                        </select>
                                      </div>
                                  </div>

                                  <div class="col-md-4">
                                      <div class="form-group row">
                                          <div class="col-xs-6">
                                                  <label>Tanggal berangkat </label>
                                              <div class="datepicker-wrap">
                                                <input type="text" class="input-text full-width" placeholder="Tanggal berangkat" onchange="settrain_tglberangkat(this.value);" id="train_tglberangkat" name="train_tglberangkat" />
                                              </div>
                                          </div>
                                          <div class="col-xs-6">
                                                  <label>&nbsp;</label>
                                              <div class="selector">
                                                <input id="train_togglerencana"  checked data-toggle="toggle" data-on="Sekali jalan" data-off="Pulang pergi" data-onstyle="success" data-offstyle="default" type="checkbox">

                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group row"  id="train_tny_ret">
                                          <div class="col-xs-6">
                                                  <label>Tanggal pulang </label>
                                              <div class="datepicker-wrap">
                                                  <input value="<?php echo date("d/m/Y");?>"  type="text" class="input-text full-width"  placeholder="Tanggal pulang" onchange="settrain_tglpulang(this.value);" id="train_tglpulang" name="train_tglpulang" />
                                              </div>
                                          </div>

                                      </div>
                                  </div>

                                  <div class="col-md-4">
                                      <div class="form-group row">
                                          <div class="col-xs-3">
                                                  <label>Dewasa</label>
                                              <div class="selector">
                                                  <select class="full-width" id="train_adt" name="train_adt">

                                                      <option value="1">01</option>
                                                      <option value="2">02</option>
                                                      <option value="3">03</option>
                                                      <option value="4">04</option>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="col-xs-3">
                                                  <label>Bayi</label>
                                              <div class="selector">
                                                  <select class="full-width" id="train_inf" name="train_inf">

                                                      <option value="0">00</option>
                                                      <option value="1">01</option>
                                                      <option value="2">02</option>
                                                      <option value="3">03</option>
                                                      <option value="4">04</option>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="col-xs-6">
                                                  <label>&nbsp;</label>
                                            <button class="full-width" onclick="cari_train()">SERACH NOW</button>

                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class="tab-pane fade" id="advanced-search-tab">
                                <form method="get" action="cekpesanan" >
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4 class="title">Masukkan nomor transaksi</h4>
                                            <div class="form-group">
                                                <input type="text" required="" id="notrx" name="notrx"  class="input-text full-width" placeholder="Nomor transaksi" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <h4 class="title">Jenis transaksi</h4>
                                            <div class="form-group row">
                                                    <div class="selector">
                                                        <select class="full-width" id="jenis" name="jenis" >
                                                            <option value="AIR">Pesawat</option>
                                                            <option value="KAI">Kereta</option>
                                                            <option value="HTL">Hotel</option>
                                                        </select>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                          <h4 class="title">&nbsp;</h4>
                                          <div class="form-group row">
                                              <div class="col-xs-4">
                                                  <div class="selector">
                                                      <button class="full-width">CEK PESANAN</button>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="featured image-box">
            <div class="details pull-left">
                <h3>Tropical Beach,<br/>Hotel and Resorts</h3>
                <h5>THAILAND</h5>
            </div>
            <figure class="pull-left">
                <a class="badge-container" href="#">
                    <span class="badge-content right-side">From $200</span>
                    <img width="64" height="64" alt="" src="http://placehold.it/64x64" class="">
                </a>
            </figure>
        </div>
      -->
    </section>

		@section('akhirbody')
		<script type="text/javascript">
		//variabel

		var hargapulang="0";
		var hargapergi="0";
		var nilpil=0;
		var idpilper="";
		var idpilret="";
		var banasal="CGK";
		var kotasal="Jakarta";
		var banasal_flight="CGK";
		var kotasal_flight="Jakarta";
		var banasal_train="CGK";
		var kotasal_train="Jakarta";
		var labelasal="";
		var kottujuan="Manokwari";
		var bantujuan="MKW";
		var kottujuan_flight="Manokwari";
		var bantujuan_flight="MKW";
		var kottujuan_train="Manokwari";
		var bantujuan_train="MKW";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana_flight="O";
		var pilrencana_train="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";

		  var wilayah=1;

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
		var tglpulang="<?php echo date("Y-m-d");?>";

		var tglberangkat_flight="<?php echo date("Y-m-d");?>";
		var tglpulang_flight="<?php echo date("Y-m-d");?>";

		var tglberangkat_train="<?php echo date("Y-m-d");?>";
		var tglpulang_train="<?php echo date("Y-m-d");?>";

		var jumadt="1";
		var jumchd="0";
		var juminf="0";

    var jumadt_flight="1";
    var jumchd_flight="0";
    var juminf_flight="0";

    var jumadt_train="1";
    var jumchd_train="0";
    var juminf_train="0";

		var urljadwal="{{ url('/') }}/jadwalPesawat/";
		var urljadwalb=urljadwal;
    $( document ).ready(function() {

                     if (isMobile.matches) {
                  $( '.toggle' ).width('80%' );
                     }else{
                  $( '.toggle' ).width('70%' );
                }
    });

		$('#flight_adt').on('change', function() {
		jumadt_flight=this.value;

		});
		$('#flight_chd').on('change', function() {
		jumchd_flight=this.value;

		});

		$('#flight_inf').on('change', function() {
		juminf_flight=this.value;

		});


		$('.selektwo').select2();
		$('#flight_tny_ret').hide();
		$('#train_tny_ret').hide();


    $("#flight_tglberangkat").datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    maxDate: '+2y'
    });

    $("#flight_tglpulang").datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: new Date(),
    changeMonth: true,
    changeYear: true
    });


    $('#flight_togglerencana').change(function() {
      if(pilrencana_flight=="O"){
        pilrencana_flight="R";
        $('#flight_tny_ret').show();
      }else{
        pilrencana_flight="O";
        $('#flight_tny_ret').hide();
      }
    });

    function setflight_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat_flight=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#flight_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);


      var selectedDate = new Date(tglberangkat_flight);
      var msecsInADay = 86400000;
      var endDate = new Date(selectedDate.getTime());
              $("#flight_tglpulang").datepicker( "option", "minDate", endDate );
              $("#flight_tglpulang").datepicker( "option", "maxDate", '+2y' );


  setflighttglpulang($("#flight_tglpulang").val());
    }

    function setflighttglpulang(val){

          var values=val.split('/');
          //alert(val);
          tglpul_d=values[0];
          tglpul_m=values[1];
          tglpul_y=values[2];

          tglpulang_flight=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;

    }

    function setflight_tglpulang(tgl){
       setflighttglpulang(tgl);
    }

		$('#flight_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal_flight=values[0];

		banasal_flight=kotas;
		labelasal=this.value;


		});
		$('#flight_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan_flight=values[0];

		bantujuan_flight=kottuj;
		labeltujuan=this.value;

		});
		$('#flight_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana_flight=this.value;

		var tny_ret = document.getElementById("flight_tny_ret").value;

		if(this.value=="O"){
		  $('#flight_tny_ret').hide();
		}else{
		  $('#flight_tny_ret').show();
		}

		});

		$('#train_adt').on('change', function() {
		jumadt_train=this.value;

		});
		$('#train_chd').on('change', function() {
		jumchd_train=this.value;

		});
		$('#train_inf').on('change', function() {
		juminf_train=this.value;

		});

    $("#train_tglberangkat").datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    maxDate: '+2y'
    });

    $("#train_tglpulang").datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: new Date(),
    changeMonth: true,
    changeYear: true
    });

    function settrain_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat_train=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#train_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

            var selectedDate = new Date(tglberangkat_train);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime());
                    $("#train_tglpulang").datepicker( "option", "minDate", endDate );
                    $("#train_tglpulang").datepicker( "option", "maxDate", '+2y' );


        setflighttglpulang($("#train_tglpulang").val());

    }

        function settraintglpulang(val){

              var values=val.split('/');
              //alert(val);
              tglpul_d=values[0];
              tglpul_m=values[1];
              tglpul_y=values[2];

              tglpulang_train=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
              $('#train_tglpulang').val(tglpul_d+"/"+tglpul_m+"/"+tglpul_y);

        }

    function settrain_tglpulang(tgl){
      settraintglpulang(tgl);
    }

    $('#train_togglerencana').change(function() {
      if(pilrencana_train=="O"){
        pilrencana_train="R";
        $('#train_tny_ret').show();
      }else{
        pilrencana_train="O";
        $('#train_tny_ret').hide();
      }
    });

		$('#train_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal_train=values[0];

		banasal_train=kotas;
		labelasal=this.value;


		});
		$('#train_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan_train=values[0];

		bantujuan_train=kottuj;
		labeltujuan=this.value;

		});
		$('#train_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana_train=this.value;
		var tny_ret = document.getElementById("train_tny_ret").value;

		if(this.value=="O"){
		  $('#train_tny_ret').hide();
		}else{
		  $('#train_tny_ret').show();
		}

		});

		function cari_flight(){

		if((Number(jumadt_flight)+Number(jumchd_flight)+Number(juminf_flight))<=7){
		  $(location).attr('href', '{{ url('/') }}/flight/otomatiscari/'+banasal_flight+'/'+bantujuan_flight+'/'+pilrencana_flight+'/'+tglberangkat_flight+'/'+tglpulang_flight+'/'+jumadt_flight+'/'+jumchd_flight+'/'+juminf_flight)

		}
		}

		function cari_train(){

		if((Number(jumadt_train)+Number(jumchd_train)+Number(juminf_train))<=4){
		  $(location).attr('href', '{{ url('/') }}/train/otomatiscari/'+banasal_train+'/'+bantujuan_train+'/'+pilrencana_train+'/'+tglberangkat_train+'/'+tglpulang_train+'/'+jumadt_train+'/'+jumchd_train+'/'+juminf_train)

		}
		}
		var checkinhotel="<?php echo date("Y-m-d");?>";
		var checkouthotel="<?php echo date("Y-m-d");?>";
    $('#checkin').val("<?php echo date("d/m/Y");?>");
    $('#checkout').val("<?php echo date("d/m/Y");?>");
    $("#checkin").datepicker({
    dateFormat: 'dd/mm/yy',
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    maxDate: '+2y'
    });

    $("#checkout").datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: new Date(),
    changeMonth: true,
    changeYear: true
    });

    function setcheckinhotel(val){
      		var values=val.split('/');
          var tgl=values[2]+"-"+values[1]+"-"+values[0];
          checkinhotel=tgl;
          $('#checkin').val(values[0]+"/"+values[1]+"/"+values[2]);


					var selectedDate = new Date(checkinhotel);
					var msecsInADay = 86400000;
					var endDate = new Date(selectedDate.getTime());
					        $("#checkout").datepicker( "option", "minDate", endDate );
					        $("#checkout").datepicker( "option", "maxDate", '+2y' );

					    setcheckout($("#checkout").val());
	  }
		function setcheckout(val){

			var values=val.split('/');
			var tgl=values[2]+"-"+values[1]+"-"+values[0];
			checkouthotel=tgl;
			$('#checkout').val(values[0]+"/"+values[1]+"/"+values[2]);
	}
    function setcheckouthotel(val){
     setcheckout(val);
	 }

		var kodedes="";
    function setkodedes(val){
      var values=val.split('-');
		  var kode=values[0];
		  kotdes=values[1];
		  kodedes=kode;
		  labeltujuan=val;
    }

		function cari_hotel(){
		  var kamar=$('#pilkamar').val();
		  var adt=$('#hotel_adt').val();
		  var chd=$('#hotel_chd').val();
		  var inf=0;
		  var checkin=checkinhotel;
		  var checkout=checkouthotel;
		  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)
		  //alert(checkin);
		}

		var daftardaerah= {
		              domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
		              internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]
    };
		function pilihwilayah(i){
		  wilayah=i;
		  var ambildaftar=null;
		  if(i==1){
		    ambildaftar=daftardaerah.domestik;
		  }else{
		    ambildaftar=daftardaerah.internasional;
		  }
		  var optionsAsString = "<option value=\"\"></option>";
		        for(var i = 0; i < ambildaftar.length; i++) {
		          optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
		          optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
		        }
		        $( '#pildes' ).html( optionsAsString );
		}
		pilihwilayah(1);

		</script>
		@endsection
	@endsection
