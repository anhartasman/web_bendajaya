@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
			<script type="application/javascript" src="{{ URL::asset('asettemplate1/js/jquery-2.2.2.min.js')}}"></script>

			<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js') }}"></script>
			 <script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js') }}"></script>
			 <script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js') }}"></script>
			 <script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js') }}"></script>
			 <script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js') }}"></script>

			 <script type="application/javascript" src="{{ URL::asset('asettemplate1/js/jquery-2.2.2.min.js')}}"></script>
	 		<link href="{{ URL::asset('asettemplate2/css/select2.min.css')}}" rel="stylesheet" />
	 		<script src="{{ URL::asset('asettemplate2/js/select2.min.js')}}"></script>

@endsection
	  @section('kontenweb')
    <div id="slideshow">
        <div class="fullwidthbanner-container">
            <div class="revolution-slider" style="height: 0; overflow: hidden;">
                <ul>    <!-- SLIDE  -->
                    <!-- Slide3 -->
										@foreach($fotos as $foto)
                    <li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500">
                        <!-- MAIN IMAGE -->
                        <img src="{{ url('/') }}/uploads/images/{{$foto->gambar}}" alt="">
                    </li>
										@endforeach
                </ul>
            </div>
        </div>
    </div>

    <section id="content">
        <div class="search-box-wrapper">
            <div class="search-box container">
                <ul class="search-tabs clearfix">
                    <li class="active"><a href="#hotels-tab" data-toggle="tab">HOTELS</a></li>
                    <li><a href="#flights-tab" data-toggle="tab">FLIGHTS</a></li>
                    <li><a href="#trains-tab" data-toggle="tab">TRAINS</a></li>
                </ul>
                <div class="visible-mobile">
                    <ul id="mobile-search-tabs" class="search-tabs clearfix">
                        <li class="active"><a href="#hotels-tab">HOTELS</a></li>
                        <li><a href="#flights-tab">FLIGHTS</a></li>
                        <li><a href="#trains-tab">TRAINS</a></li>
                    </ul>
                </div>

                <div class="search-tab-content">
                    <div class="tab-pane fade active in" id="hotels-tab">
                        <form action="hotel-list-view.html" method="post">
                            <div class="row">
                              <div class="form-group col-sm-6 col-md-3">
                                  <h4 class="title">Where</h4>
                                  <div class="row">
                                      <div class="col-xs-6">
                                          <label>Destination</label>
                                          <div class="selector">
                                              <select class="full-width" onchange="pilihwilayah(this.value)" name="wilayah" id="wilayah">
                                                  <option value="1">Domestik</option>
                                                  <option value="2">Internasional</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="col-xs-6">
                                          <label>Region</label>
                                          <select class="full-width selektwo" onchange="setkodedes(this.value)" name="pildes" id="pildes">

                                      		</select>
                                      </div>
                                  </div>
                              </div>

                                <div class="form-group col-sm-6 col-md-4">
                                    <h4 class="title">When</h4>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label>Check In</label>
                                            <div class="datepicker-wrap">
                                                <input id="checkin" name="checkin" onchange="setcheckinhotel(this.value);"  type="text" class="input-text full-width" placeholder="dd/mm/yy" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Check Out</label>
                                            <div class="datepicker-wrap">
                                                <input id="checkout" name="checkout" onchange="setcheckouthotel(this.value);"  type="text" class="input-text full-width" placeholder="dd/mm/yy" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6 col-md-3">
                                    <h4 class="title">Who</h4>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label>Rooms</label>
                                            <div class="selector">
                                                <select class="full-width" id="pilkamar">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>Adults</label>
                                            <div class="selector">
                                                <select class="full-width" id="hotel_adt" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>Kids</label>
                                            <div class="selector">
                                                <select class="full-width" id="hotel_chd" >
                                                    <option value="0">00</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6 col-md-2 fixheight">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <button type="button" onclick="cari_hotel()" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">SEARCH NOW</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="flights-tab">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">

                                  <div class="form-group row">
                                    <h4 class="title">Where</h4>
                                        <div class="col-xs-6">
                                        <label>Leaving From</label>
                                        <select style="width:100%" class="full-width selektwo" name="asal" id="flight_pilasal">
                                          <option value=""></option>
                                           @foreach($dafban as $area)
                                          <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                          @endforeach
                                        </select>
                                          </div>
                                       <div class="col-xs-6">
                                        <label>Flight</label>
                                            <div class="selector">
                                        <select class="full-width"  id="flight_pilrencana" name="pilrencana">
                                            <option value="O">One Way</option>
                                            <option value="R">Return</option>
                                        </select>
                                      </div>
                                      </div>
                                </div>
                                <div class="form-group row">
                                      <div class="col-xs-6">
                                      <label>Going To</label>
                                      <select style="width:100%" class="full-width selektwo" name="tujuan" id="flight_piltujuan">
                                        <option value=""></option>
                                         @foreach($dafban as $area)
                                        <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                        @endforeach
                                      </select>
                                        </div>

                              </div>
                                </div>

                                <div class="col-md-2">
                                    <h4 class="title">When</h4>
                                    <label>Departure</label>
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <div class="datepicker-wrap">
                                                <input type="text" class="input-text full-width" placeholder="dd/mm/yy" onchange="setflight_tglberangkat(this.value);" id="flight_tglberangkat" name="flight_tglberangkat" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="flight_tny_ret">
                                    <label>Return</label>
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <div class="datepicker-wrap">
                                                <input type="text" class="input-text full-width" placeholder="dd/mm/yy"  onchange="setflight_tglpulang(this.value);" id="flight_tglpulang" name="flight_tglpulang" />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <h4 class="title">Who</h4>
                                    <div class="form-group row">
                                        <div class="col-xs-3">
                                            <label>Adults</label>
                                            <div class="selector">
                                                <select class="full-width" id="flight_adt">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <label>Kids</label>
                                            <div class="selector">
                                                <select class="full-width" id="flight_chd">
                                                    <option value="0">00</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-3">
                                            <label>Infants</label>
                                            <div class="selector">
                                                <select class="full-width"id="flight_inf">
                                                    <option value="0">00</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pull-right">
                                            <label>&nbsp;</label>
                                            <button id="flight_tomcari" onclick="cari_flight()" class="full-width icon-check">SERACH NOW</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="tab-pane fade" id="trains-tab">
                      <div class="row">
                          <div class="col-sm-6 col-md-4">

                            <div class="form-group row">
                              <h4 class="title">Where</h4>
                                  <div class="col-xs-6">
                                  <label>Asal</label>
                                  <select style="width:100%" class="full-width selektwo" name="train_pilasal" id="train_pilasal">
                                    <option value=""></option>
                                     @foreach($dafstat as $area)
                                    <option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                                    @endforeach
                                  </select>
                                    </div>
                                 <div class="col-xs-6">
                                  <label>Rencana</label>
                                      <div class="selector">
                                  <select class="full-width"  id="train_pilrencana" name="pilrencana">
                                      <option value="O">Sekali jalan</option>
                                      <option value="R">Pulang pergi</option>
                                  </select>
                                </div>
                                </div>
                          </div>
                          <div class="form-group row">
                                <div class="col-xs-6">
                                <label>Tujuan</label>
                                <select style="width:100%" class="full-width selektwo" name="tujuan" id="train_piltujuan">
                                  <option value=""></option>
                                  @foreach($dafstat as $area)
                                 <option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                                 @endforeach
                                </select>
                                  </div>

                        </div>
                          </div>

                          <div class="col-md-2">
                              <h4 class="title">When</h4>
                              <label>Berangkat</label>
                              <div class="form-group row">
                                  <div class="col-xs-12">
                                      <div class="datepicker-wrap">
                                          <input type="text" class="input-text full-width" placeholder="dd/mm/yy" onchange="settrain_tglberangkat(this.value);" id="train_tglberangkat" name="train_tglberangkat" />
                                      </div>
                                  </div>
                              </div>
                              <div id="train_tny_ret">
                              <label>Kembali</label>
                              <div class="form-group row">
                                  <div class="col-xs-12">
                                      <div class="datepicker-wrap">
                                          <input type="text" class="input-text full-width" placeholder="dd/mm/yy"  onchange="settrain_tglpulang(this.value);" id="train_tglpulang" name="train_tglpulang" />
                                      </div>
                                  </div>
                              </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <h4 class="title">Who</h4>
                              <div class="form-group row">
                                  <div class="col-xs-3">
                                      <label>Dewasa</label>
                                      <div class="selector">
                                          <select class="full-width" id="train_adt">
                                              <option value="1">01</option>
                                              <option value="2">02</option>
                                              <option value="3">03</option>
                                              <option value="4">04</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xs-3">
                                      <label>Bayi</label>
                                      <div class="selector">
                                          <select class="full-width" id="train_inf">
                                              <option value="0">00</option>
                                              <option value="1">01</option>
                                              <option value="2">02</option>
                                              <option value="3">03</option>
                                              <option value="4">04</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <div class="col-xs-6 pull-right">
                                      <label>&nbsp;</label>
                                      <button id="train_tomcari" onclick="cari_train()" class="full-width icon-check">SERACH NOW</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>


                </div>
            </div>
        </div>

    </section>
		@section('akhirbody')
		<script type="text/javascript">
		//variabel

		var hargapulang="0";
		var hargapergi="0";
		var nilpil=0;
		var idpilper="";
		var idpilret="";
		var banasal="CGK";
		var kotasal="Jakarta";
		var labelasal="";
		var kottujuan="Manokwari";
		var bantujuan="MKW";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
		var tglpulang="<?php echo date("Y-m-d");?>";

		var jumadt="1";
		var jumchd="0";
		var juminf="0";

		var urljadwal="{{ url('/') }}/jadwalPesawat/";
		var urljadwalb=urljadwal;


		$('#flight_adt').on('change', function() {
		jumadt=this.value;

		});
		$('#flight_chd').on('change', function() {
		jumchd=this.value;

		});

		$('#flight_inf').on('change', function() {
		juminf=this.value;
		$('#flight_formgo_inf').val(juminf);

		});


		$('.selektwo').select2();
		$('#flight_tny_ret').hide();
		$('#train_tny_ret').hide();


    function setflight_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#flight_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

      $('#flight_formgo_tgl_deppilihan').val(tglberangkat);

    }
    function setflight_tglpulang(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglpulang=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#flight_tglpulang').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

      $('#flight_formgo_tgl_retpilihan').val(tglpulang);

    }

		$('#flight_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal=values[0];

		banasal=kotas;
		labelasal=this.value;


		});
		$('#flight_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan=values[0];

		bantujuan=kottuj;
		labeltujuan=this.value;

		});
		$('#flight_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana=this.value;

		var tny_ret = document.getElementById("flight_tny_ret").value;

		if(this.value=="O"){
		  $('#flight_tny_ret').hide();
		}else{
		  $('#flight_tny_ret').show();
		}

		});

		$('#train_adt').on('change', function() {
		jumadt=this.value;
		$('#train_formgo_adt').val(jumadt);

		});
		$('#train_chd').on('change', function() {
		jumchd=this.value;
		$('#train_formgo_chd').val(jumchd);

		});
		$('#train_inf').on('change', function() {
		juminf=this.value;
		$('#train_formgo_inf').val(juminf);

		});



    function settrain_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#train_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

      $('#train_formgo_tgl_deppilihan').val(tglberangkat);

    }
    function settrain_tglpulang(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglpulang=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#train_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

      $('#train_formgo_tgl_retpilihan').val(tglpulang);

    }



		$('#train_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal=values[0];

		banasal=kotas;
		labelasal=this.value;


		});
		$('#train_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan=values[0];

		bantujuan=kottuj;
		labeltujuan=this.value;

		});
		$('#train_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana=this.value;
		var tny_ret = document.getElementById("train_tny_ret").value;

		if(this.value=="O"){
		  $('#train_tny_ret').hide();
		}else{
		  $('#train_tny_ret').show();
		}

		});

		function cari_flight(){

		if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
		  $(location).attr('href', '{{ url('/') }}/flight/otomatiscari/'+banasal+'/'+bantujuan+'/'+pilrencana+'/'+tglberangkat+'/'+tglpulang+'/'+jumadt+'/'+jumchd+'/'+juminf)

		}
		}

		function cari_train(){

		if((Number(jumadt)+Number(jumchd)+Number(juminf))<=4){
		  $(location).attr('href', '{{ url('/') }}/train/otomatiscari/'+banasal+'/'+bantujuan+'/'+pilrencana+'/'+tglberangkat+'/'+tglpulang+'/'+jumadt+'/'+jumchd+'/'+juminf)

		}
		}
		var checkinhotel="<?php echo date("Y-m-d");?>";
		var checkouthotel="<?php echo date("Y-m-d");?>";
    $('#checkin').val("<?php echo date("d/m/Y");?>");
    $('#checkout').val("<?php echo date("d/m/Y");?>");

    function setcheckinhotel(val){
      		var values=val.split('/');
          var tgl=values[2]+"-"+values[1]+"-"+values[0];
          checkinhotel=tgl;
          $('#checkin').val(values[0]+"/"+values[1]+"/"+values[2]);
    }
    function setcheckouthotel(val){
      		var values=val.split('/');
          var tgl=values[2]+"-"+values[1]+"-"+values[0];
          checkouthotel=tgl;
          $('#checkout').val(values[0]+"/"+values[1]+"/"+values[2]);
    }

		var kodedes="";
    function setkodedes(val){
      var values=val.split('-');
		  var kode=values[0];
		  kotdes=values[1];
		  kodedes=kode;
		  labeltujuan=val;
    }

		function cari_hotel(){
		  var wilayah=$('#wilayah').val();
		  var kamar=$('#pilkamar').val();
		  var adt=$('#hotel_adt').val();
		  var chd=$('#hotel_chd').val();
		  var inf=0;
		  var checkin=checkinhotel;
		  var checkout=checkouthotel;
		  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)
		  //alert(checkin);
		}

		var daftardaerah= {
		               domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
		               internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

		           };
		function pilihwilayah(i){
		  wilayah=i;
		  var ambildaftar=null;
		  if(i==1){
		    ambildaftar=daftardaerah.domestik;
		  }else{
		    ambildaftar=daftardaerah.internasional;
		  }
		  var optionsAsString = "<option value=\"\"></option>";
		        for(var i = 0; i < ambildaftar.length; i++) {
		          optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
		          optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
		        }
		        $( '#pildes' ).html( optionsAsString );
		}
		pilihwilayah(1);

		</script>
		@endsection
	@endsection
