<?php $front=1;?>
@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
@endsection
	  @section('kontenweb')
    <!-- TOP AREA -->
    <div class="top-area show-onload">
        <div class="bg-holder full">
            <div class="bg-mask"></div>
						<?php
						$fotopilihan=$fotos[rand(0,count($fotos)-1)]->gambar;
						?>
            <div class="bg-parallax" style="background-image:url({{url('/')}}/gambarlokal/{{$fotopilihan}}/w/2038/h/1365);"></div>
            <div class="bg-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="search-tabs search-tabs-bg mt50">
                                <h4 style="color:#fff"> Cari tiket pesawat murah & promo secara online dengan cepat dan mudah di sini! </h4>
                                <div class="tabbable">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-building-o"></i> <span >Hotel</span></a>
                                        </li>
                                        <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-plane"></i> <span >Pesawat</span></a>
                                        </li>
                                        <li><a href="#tab-3" data-toggle="tab"><i class="fa fa-train"></i> <span >Kereta</span></a>
                                        </li>
                                        <li><a href="#tab-4" data-toggle="tab"><i class="fa fa-search"></i> <span >Cek pesanan</span></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab-1">
                                            <h2>Cari Hotel</h2>
                                            <form>
                                              <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
                                                  <li class="active"><a href="#flight-search-1" data-toggle="tab" onclick="pilihwilayah(1)">Domestik</a>
                                                  </li>
                                                  <li><a href="#flight-search-2" data-toggle="tab" onclick="pilihwilayah(2)">Internasional</a>
                                                  </li>
                                              </ul>
                                                <div class="form-group form-group-lg form-group-icon-left">
                                                    <label>Destinasi?</label>
                                                     <select class="form-control  selektwo" onchange="setkodedes(this.value)" name="pildes" id="pildes">

                                                   </select>
                                                </div>
                                                <div class="input-daterange" data-date-format="M d, D">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                <label>Check-in</label>
                                                                <input class="form-control" id="checkin" name="checkin" onchange="setcheckinhotel(this.value);" type="text" value="<?php echo date("d/m/Y");?>"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                <label>Check-out</label>
                                                                <input class="form-control" id="checkout" name="checkout" onchange="setcheckouthotel(this.value);" type="text" value="<?php echo date("d-m-Y");?>"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-select-plus">
                                                                <label>Kamar</label>
                                                                <div  id="pilihanhotel_kamar" class="btn-group btn-group-select-num" data-toggle="buttons">

                                                                    <label class="btn btn-primary active">
                                                                        <input type="radio" name="options" value="1" />1</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="2"  />2</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="3"  />3</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="4"  />3+</label>

                                                                </div>
                                                                <select class="form-control hidden" onchange="sethotel_kamar(this.value)">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4" selected="selected">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg">
                                                                <label>Dewasa per kamar</label>
                                                                <div id="pilihanhotel_adt"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                    <label class="btn btn-primary active">
                                                                        <input type="radio" name="options"  value="1"  />1</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="2"   />2</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-daterange" data-date-format="M d, D">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg">
                                                                <label>Anak per kamar</label>
                                                                <div id="pilihanhotel_chd"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                    <label class="btn btn-primary active">
                                                                        <input type="radio" name="options" value="0"  />0</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="1"  />1</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" value="2"  />2</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary btn-lg" type="button" onclick="cari_hotel()">Cari Hotel</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab-2">
                                            <h2>Cari Tiket Pesawat</h2>
                                            <form>
                                                <div class="tabbable">
                                                    <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
                                                        <li class="active"><a style="cursor:pointer"  onclick="setflight_rencana('O')" data-toggle="tab">Sekali jalan</a>
                                                        </li>
                                                        <li><a style="cursor:pointer" onclick="setflight_rencana('R')"  data-toggle="tab">Pulang pergi</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane fade in active" id="flight-search-1">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group form-group-lg form-group-icon-left">
                                                                        <label>Asal</label>
                                                                        <select style="width:100%" class="full-width selektwo" name="asal" id="flight_pilasal">
                                                                          <option value=""></option>
                                                                           @foreach($dafban as $area)
                                                                          <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                                                          @endforeach
                                                                        </select>
                                                                      </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group form-group-lg form-group-icon-left">
                                                                        <label>Tujuan</label>
                                                                        <select style="width:100%" class="full-width selektwo" name="tujuan" id="flight_piltujuan">
                                                                          <option value=""></option>
                                                                           @foreach($dafban as $area)
                                                                          <option value="{{ $area->nama }}-{{ $area->kode }}" >{{ $area->nama }} - {{ $area->kode }}</option>
                                                                          @endforeach
                                                                        </select>
                                                                      </div>
                                                                </div>
                                                            </div>
                                                            <div class="input-daterange" data-date-format="M d, D">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                            <label>Pergi</label>
                                                                            <input class="form-control"  onchange="setflight_tglberangkat(this.value);" id="flight_tglberangkat" name="flight_tglberangkat"  type="text" />
                                                                        </div>
                                                                    </div>
                                                                    <div id="flight_tny_ret" class="col-md-3">
                                                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                            <label>Pulang</label>
                                                                            <input class="form-control"  onchange="setflight_tglpulang(this.value);" id="flight_tglpulang" name="flight_tglpulang" type="text"  value="<?php echo date("d/m/Y");?>"  />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Dewasa</label>
                                                                            <div  id="pilihanflight_adt"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                <label class="btn btn-primary active">
                                                                                    <input type="radio" name="options" value="1" />1</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="2"  />2</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="3"  />3</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="4"  />3+</label>
                                                                            </div>
                                                                            <select class="form-control hidden" onchange="setflight_adt(this.value)">
                                                                                <option value="1" >1</option>
                                                                                <option value="2" >2</option>
                                                                                <option value="3" >3</option>
                                                                                <option value="4"  selected="selected">4</option>
                                                                                <option value="5" >5</option>
                                                                                <option value="6" >6</option>
                                                                                <option value="7" >7</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Anak</label>
                                                                            <div id="pilihanflight_chd"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                <label class="btn btn-primary active">
                                                                                    <input type="radio" name="options" value="0"  />0</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="1"  />1</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="2"  />2</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="3"  />2+</label>
                                                                            </div>
                                                                            <select class="form-control hidden" onchange="setflight_chd(this.value)">
                                                                                <option value="0" >0</option>
                                                                                <option value="1" >1</option>
                                                                                <option value="2" >2</option>
                                                                                <option value="3"  selected="selected" >3</option>
                                                                                <option value="4">4</option>
                                                                                <option value="5" >5</option>
                                                                                <option value="6" >6</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Bayi</label>
                                                                            <div id="pilihanflight_inf"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                <label class="btn btn-primary active">
                                                                                    <input type="radio" name="options" value="0"  />0</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="1"  />1</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="2"  />2</label>
                                                                                <label class="btn btn-primary">
                                                                                    <input type="radio" name="options" value="3"  />2+</label>
                                                                            </div>
                                                                            <select class="form-control hidden" onchange="setflight_inf(this.value)">
                                                                              <option value="0" >0</option>
                                                                              <option value="1" >1</option>
                                                                              <option value="2" >2</option>
                                                                              <option value="3"  selected="selected" >3</option>
                                                                              <option value="4">4</option>
                                                                              <option value="5" >5</option>
                                                                              <option value="6" >6</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <button class="btn btn-primary btn-lg" type="button" onclick="cari_flight()">Cari Tiket Pesawat</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab-3">
                                          <h2>Cari Tiket Kereta</h2>
                                          <form>
                                              <div class="tabbable">
                                                  <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
                                                      <li class="active"><a style="cursor:pointer"  onclick="settrain_rencana('O')" data-toggle="tab">Sekali jalan</a>
                                                      </li>
                                                      <li><a style="cursor:pointer" onclick="settrain_rencana('R')"  data-toggle="tab">Pulang pergi</a>
                                                      </li>
                                                  </ul>
                                                  <div class="tab-content">
                                                      <div class="tab-pane fade in active" id="flight-search-1">
                                                          <div class="row">
                                                              <div class="col-md-6">
                                                                  <div class="form-group form-group-lg form-group-icon-left">
                                                                      <label>Asal</label>
                                                                      <select style="width:100%" class="full-width selektwo"  name="train_pilasal" id="train_pilasal">
                                                                        <option value=""> </option>
                                                                        @foreach($dafstat as $area)
                                                                       <option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                                                                       @endforeach
                                                                      </select>
                                                                    </div>
                                                              </div>
                                                              <div class="col-md-6">
                                                                  <div class="form-group form-group-lg form-group-icon-left">
                                                                      <label>Tujuan</label>
                                                                      <select style="width:100%" class="full-width selektwo" name="tujuan" id="train_piltujuan">
                                                                        <option value=""> </option>
                                                                        @foreach($dafstat as $area)
                                                                       <option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                                                                       @endforeach
                                                                      </select>
                                                                    </div>
                                                              </div>
                                                          </div>
                                                          <div class="input-daterange" data-date-format="M d, D">
                                                              <div class="row">
                                                                  <div class="col-md-3">
                                                                      <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                          <label>Pergi</label>
                                                                          <input class="form-control"  onchange="settrain_tglberangkat(this.value);" id="train_tglberangkat" name="train_tglberangkat"  type="text" />
                                                                      </div>
                                                                  </div>
                                                                  <div id="train_tny_ret" class="col-md-3">
                                                                      <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                          <label>Pulang</label>
                                                                          <input class="form-control"  onchange="settrain_tglpulang(this.value);" id="train_tglpulang" name="train_tglpulang" type="text"  value="<?php echo date("d/m/Y");?>"  />
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-3">
                                                                      <div class="form-group form-group-lg  ">
                                                                          <label>Dewasa</label>
                                                                          <div  id="pilihantrain_adt"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                              <label class="btn btn-primary active">
                                                                                  <input type="radio" name="options" value="1" />1</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="2"  />2</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="3"  />3</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="4"  />4</label>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-3">
                                                                      <div class="form-group form-group-lg form-group-select-plus ">
                                                                          <label>Bayi</label>
                                                                          <div id="pilihantrain_inf"  class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                              <label class="btn btn-primary active">
                                                                                  <input type="radio" name="options" value="0"  />0</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="1"  />1</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="2"  />2</label>
                                                                              <label class="btn btn-primary">
                                                                                  <input type="radio" name="options" value="3"  />2+</label>
                                                                          </div>
																																					<select class="form-control hidden" onchange="settrain_inf(this.value)">
					                                                                    <option value="1">1</option>
					                                                                    <option value="2">2</option>
					                                                                    <option value="3" selected="selected">3</option>
					                                                                    <option value="4">4</option>
					                                                                </select>

                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>
                                              </div>
                                              <button class="btn btn-primary btn-lg" type="button" onclick="cari_train()">Cari Tiket Kereta</button>
                                          </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab-4">
                                            <h2>Cek atau Cetak Bukti Transaksi</h2>
																						<form class="simple-from" role="form" method="get" action="cekpesanan" enctype = "multipart/form-data">
																							<input type="hidden" id="jenis" name="jenis" value="HTL">
																							<input type="hidden" id="pdf" name="pdf" value="0">

																							<ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
																									<li class="active"><a style="cursor:pointer"   onclick="settran('HTL')" data-toggle="tab">Hotel</a>
																									</li>
																									<li><a style="cursor:pointer"  onclick="settran('AIR')"   data-toggle="tab">Tiket Pesawat</a>
																									</li>
																									<li><a style="cursor:pointer" onclick="settran('KAI')"  data-toggle="tab">Tiket Kereta</a>
																									</li>
																							</ul>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-lg ">
                                                            <label>Nomor Transaksi</label>
                                                            <input class="  form-control" type="text"required="" id="notrx" name="notrx" placeholder="Masukkan nomor transaksi Anda" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <button class="btn btn-primary btn-lg" type="submit"  onClick='$("#pdf").val(0);'  >Cari</button>
																								<button class="btn btn-primary btn-lg" type="submit"  onClick='$("#pdf").val(1);' >Cetak PDF</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-2">
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END TOP AREA  -->
<!--
    <div class="gap"></div>


    <div class="container">
        <div class="row row-wrap" data-gutter="60">
            <div class="col-md-4">
                <div class="thumb">
                    <header class="thumb-header"><i class="fa fa-briefcase box-icon-md round box-icon-black animate-icon-top-to-bottom"></i>
                    </header>
                    <div class="thumb-caption">
                        <h5 class="thumb-title"><a class="text-darken" href="#">Combine & Save</a></h5>
                        <p class="thumb-desc">Sagittis non laoreet augue nulla lectus auctor accumsan cubilia sollicitudin mattis leo</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumb">
                    <header class="thumb-header"><i class="fa fa-thumbs-o-up box-icon-md round box-icon-black animate-icon-top-to-bottom"></i>
                    </header>
                    <div class="thumb-caption">
                        <h5 class="thumb-title"><a class="text-darken" href="#">Best Travel Agent</a></h5>
                        <p class="thumb-desc">Vel morbi class sollicitudin cubilia quisque penatibus dictumst faucibus dui natoque ultricies</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumb">
                    <header class="thumb-header"><i class="fa fa-lock box-icon-md round box-icon-black animate-icon-top-to-bottom"></i>
                    </header>
                    <div class="thumb-caption">
                        <h5 class="thumb-title"><a class="text-darken" href="#">Trust & Safety</a></h5>
                        <p class="thumb-desc">Montes congue pellentesque aliquet lectus dictum est volutpat class odio elementum quis</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="gap gap-small"></div>
    </div>
    <div class="bg-holder">
        <div class="bg-mask"></div>
				<?php
				$fotopilihan=$fotos[rand(0,count($fotos)-1)]->gambar;
				?>
        <div class="bg-parallax" style="background-image:url({{url('/')}}/gambarlokal/{{$fotopilihan}}/w/2048/h/1310);"></div>
        <div class="bg-content">
            <div class="container">
                <div class="gap gap-big text-center text-white">
                    <h2 class="text-uc mb20">Last Minute Deal</h2>
                    <ul class="icon-list list-inline-block mb0 last-minute-rating">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                    </ul>
                    <h5 class="last-minute-title">The Peninsula - New York</h5>
                    <p class="last-minute-date">Fri 14 Mar - Sun 16 Mar</p>
                    <p class="mb20"><b>$120</b> / person</p><a class="btn btn-lg btn-white btn-ghost" href="#">Book Now <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="gap"></div>
        <h2 class="text-center">Top Destinations</h2>
        <div class="gap">
            <div class="row row-wrap">
                <div class="col-md-3">
                    <div class="thumb">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                            </a>
                        </header>
                        <div class="thumb-caption">
                            <h4 class="thumb-title">USA</h4>
                            <p class="thumb-desc">Scelerisque montes class curabitur class aenean aliquam eu</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumb">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="img/800x600.png" alt="Image Alternative text" title="lack of blue depresses me" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                            </a>
                        </header>
                        <div class="thumb-caption">
                            <h4 class="thumb-title">Greece</h4>
                            <p class="thumb-desc">Condimentum odio eget curabitur scelerisque vivamus ipsum congue</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumb">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="img/800x600.png" alt="Image Alternative text" title="people on the beach" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                            </a>
                        </header>
                        <div class="thumb-caption">
                            <h4 class="thumb-title">Australia</h4>
                            <p class="thumb-desc">Ornare cras scelerisque volutpat nulla porttitor commodo cubilia</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumb">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="img/400x300.png" alt="Image Alternative text" title="the journey home" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                            </a>
                        </header>
                        <div class="thumb-caption">
                            <h4 class="thumb-title">Africa</h4>
                            <p class="thumb-desc">Dictumst risus montes ipsum faucibus vel sodales cubilia</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
-->
		@section('akhirbody')
		<script type="text/javascript">
		//variabel

		var hargapulang="0";
		var hargapergi="0";
		var nilpil=0;
		var idpilper="";
		var idpilret="";
		var banasal="CGK";
		var kotasal="Jakarta";
		var banasal_flight="CGK";
		var kotasal_flight="Jakarta";
		var banasal_train="CGK";
		var kotasal_train="Jakarta";
		var labelasal="";
		var kottujuan="Manokwari";
		var bantujuan="MKW";
		var kottujuan_flight="Manokwari";
		var bantujuan_flight="MKW";
		var kottujuan_train="Manokwari";
		var bantujuan_train="MKW";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana_flight="O";
		var pilrencana_train="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";
		var wilayah=1;

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
		var tglpulang="<?php echo date("Y-m-d");?>";

		var tglberangkat_flight="<?php echo date("Y-m-d");?>";
		var tglpulang_flight="<?php echo date("Y-m-d");?>";

		var tglberangkat_train="<?php echo date("Y-m-d");?>";
		var tglpulang_train="<?php echo date("Y-m-d");?>";

		var jumadt="1";
		var jumchd="0";
		var juminf="0";

    var jumadt_flight="1";
    var jumchd_flight="0";
    var juminf_flight="0";

    var jumadt_train="1";
    var jumchd_train="0";
    var juminf_train="0";

		var jumkamar_hotel="1";
    var jumadt_hotel="1";
    var jumchd_hotel="0";

		var urljadwal="{{ url('/') }}/jadwalPesawat/";
		var urljadwalb=urljadwal;
		function settran(val){
$('#jenis').val(val);
    }

		$('#pilihanhotel_kamar input').on('change', function() {
			jumkamar_hotel=$('input[name=options]:checked', '#pilihanhotel_kamar').val();

		});
				$('#pilihanhotel_adt input').on('change', function() {
					jumadt_hotel=$('input[name=options]:checked', '#pilihanhotel_adt').val();

				});
				$('#pilihanhotel_chd input').on('change', function() {
					jumchd_hotel=$('input[name=options]:checked', '#pilihanhotel_chd').val();

				});

		function sethotel_kamar(jum){
			jumkamar_hotel=jum;
		}

				$('#pilihanflight_adt input').on('change', function() {
					jumadt_flight=$('input[name=options]:checked', '#pilihanflight_adt').val();

				});
				function setflight_adt(jum){
					jumadt_flight=jum;

				}
				$('#pilihanflight_chd input').on('change', function() {
					jumchd_flight=$('input[name=options]:checked', '#pilihanflight_chd').val();

				});
				function setflight_chd(jum){
					jumchd_flight=jum;

				}
				$('#pilihanflight_inf input').on('change', function() {
					juminf_flight=$('input[name=options]:checked', '#pilihanflight_inf').val();

				});
				function setflight_inf(jum){
					juminf_flight=jum;

				}




    $( document ).ready(function() {

                     if (isMobile.matches) {
                  $( '.toggle' ).width('80%' );
                     }else{
                  $( '.toggle' ).width('70%' );
                }
    });

		$('#flight_adt').on('change', function() {
		jumadt_flight=this.value;

		});
		$('#flight_chd').on('change', function() {
		jumchd_flight=this.value;

		});

		$('#flight_inf').on('change', function() {
		juminf_flight=this.value;

		});


		$('.selektwo').select2();
    //alert("HAHA");
		$('#flight_tny_ret').hide();
		$('#train_tny_ret').hide();


    $("#flight_tglberangkat").datepicker({
      format: 'dd/mm/yyyy',
      startDate: '+0d',
      autoclose: true,
    });

    $("#flight_tglpulang").datepicker({
      format: 'dd/mm/yyyy',
      startDate: '+0d',
      autoclose: true,
    });


		  function setflight_rencana(r) {
		      if(r=="O"){
		        pilrencana_flight="O";
		        $('#flight_tny_ret').hide();
		      }else{
		        pilrencana_flight="R";
		        $('#flight_tny_ret').show();
		      }
		    };

    function setflight_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat_flight=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#flight_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

			$('#flight_tglpulang').data('datepicker').setStartDate(new Date(tglberangkat_flight));
			var x = new Date(tglberangkat_flight);
			var y = new Date(tglpulang_flight);
			if(x>y){
      $('#flight_tglpulang').val(tglber_d+"/"+tglber_m+"/"+tglber_y);
			setflighttglpulang(tgl);
			}
    }

    function setflighttglpulang(val){

          var values=val.split('/');
          //alert(val);
          tglpul_d=values[0];
          tglpul_m=values[1];
          tglpul_y=values[2];

          tglpulang_flight=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;

		}

    function setflight_tglpulang(tgl){
       setflighttglpulang(tgl);
    }

		$('#flight_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal_flight=values[0];

		banasal_flight=kotas;
		labelasal=this.value;


		});
		$('#flight_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan_flight=values[0];

		bantujuan_flight=kottuj;
		labeltujuan=this.value;

		});
		$('#flight_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana_flight=this.value;

		var tny_ret = document.getElementById("flight_tny_ret").value;

		if(this.value=="O"){
		  $('#flight_tny_ret').hide();
		}else{
		  $('#flight_tny_ret').show();
		}

		});

		$('#train_adt').on('change', function() {
		jumadt_train=this.value;

		});
		$('#train_chd').on('change', function() {
		jumchd_train=this.value;

		});
		$('#train_inf').on('change', function() {
		juminf_train=this.value;

		});

    $("#train_tglberangkat").datepicker({
			format: 'dd/mm/yyyy',
			startDate: '+0d',
			autoclose: true,
    });

    $("#train_tglpulang").datepicker({
			format: 'dd/mm/yyyy',
			startDate: '+0d',
			autoclose: true,
    });

    function settrain_tglberangkat(tgl){
      var values=tgl.split('/');
      //alert(this.value);
      tglber_d=values[0];
      tglber_m=values[1];
      tglber_y=values[2];

      tglberangkat_train=tglber_y+"-"+tglber_m+"-"+tglber_d;
      $('#train_tglberangkat').val(tglber_d+"/"+tglber_m+"/"+tglber_y);

			$('#train_tglpulang').data('datepicker').setStartDate(new Date(tglberangkat_train));
			var x = new Date(tglberangkat_train);
			var y = new Date(tglpulang_train);
			if(x>y){
			$('#train_tglpulang').val(tglber_d+"/"+tglber_m+"/"+tglber_y);
			settraintglpulang(tgl);
			}


    }

        function settraintglpulang(val){

              var values=val.split('/');
              //alert(val);
              tglpul_d=values[0];
              tglpul_m=values[1];
              tglpul_y=values[2];

              tglpulang_train=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;

        }

    function settrain_tglpulang(tgl){
      settraintglpulang(tgl);
    }


		function settrain_rencana(r) {
				if(r=="O"){
					pilrencana_train="O";
					$('#train_tny_ret').hide();
				}else{
					pilrencana_train="R";
					$('#train_tny_ret').show();
				}
			};

		$('#train_pilasal').on('change', function() {
		var values=this.value.split('-');
		var kotas=values[1];
		kotasal_train=values[0];

		banasal_train=kotas;
		labelasal=this.value;


		});
		$('#train_piltujuan').on('change', function() {
		var values=this.value.split('-');
		var kottuj=values[1];
		kottujuan_train=values[0];

		bantujuan_train=kottuj;
		labeltujuan=this.value;

		});
		$('#train_pilrencana').on('change', function() {
		//alert( this.value );
		pilrencana_train=this.value;
		var tny_ret = document.getElementById("train_tny_ret").value;

		if(this.value=="O"){
		  $('#train_tny_ret').hide();
		}else{
		  $('#train_tny_ret').show();
		}

		});

		$('#pilihantrain_adt input').on('change', function() {
			jumadt_train=$('input[name=options]:checked', '#pilihantrain_adt').val();

		});

		$('#pilihantrain_inf input').on('change', function() {
			juminf_train=$('input[name=options]:checked', '#pilihantrain_inf').val();

		});
		function settrain_inf(jum){
			juminf_train=jum;

		}

		function cari_flight(){

		if((Number(jumadt_flight)+Number(jumchd_flight)+Number(juminf_flight))<=7){
		  $(location).attr('href', '{{ url('/') }}/flight/otomatiscari/'+banasal_flight+'/'+bantujuan_flight+'/'+pilrencana_flight+'/'+tglberangkat_flight+'/'+tglpulang_flight+'/'+jumadt_flight+'/'+jumchd_flight+'/'+juminf_flight)

		}
		}

		function cari_train(){

		if((Number(jumadt_train)+Number(jumchd_train)+Number(juminf_train))<=4){
		  $(location).attr('href', '{{ url('/') }}/train/otomatiscari/'+banasal_train+'/'+bantujuan_train+'/'+pilrencana_train+'/'+tglberangkat_train+'/'+tglpulang_train+'/'+jumadt_train+'/'+jumchd_train+'/'+juminf_train)

		}
		}
		var checkinhotel="<?php echo date("Y-m-d");?>";
		var checkouthotel="<?php echo date("Y-m-d");?>";
    $("#checkin").datepicker({
    format: 'dd/mm/yyyy',
    startDate: '+0d',
    autoclose: true,
    });

    $("#checkout").datepicker({
      format: 'dd/mm/yyyy',
      startDate: '+0d',
      autoclose: true,
    });
    $('#checkin').val("<?php echo date("d/m/Y");?>");
    $('#checkout').val("<?php echo date("d/m/Y");?>");
    function setcheckinhotel(val){
      		var values=val.split('/');
          var tgl=values[2]+"-"+values[1]+"-"+values[0];
          checkinhotel=tgl;
          $('#checkin').val(values[0]+"/"+values[1]+"/"+values[2]);


					var selectedDate = new Date(checkinhotel);
					var msecsInADay = 86400000;
					var endDate = new Date(selectedDate.getTime());
					    //    $("#checkout").datepicker( "option", "startDate", endDate );
          $('#checkout').data('datepicker').setStartDate(new Date(tgl));
					var x = new Date(checkinhotel);
					var y = new Date(checkouthotel);
					if(x>y){
						$('#checkout').val(values[0]+"/"+values[1]+"/"+values[2]);
						setcheckout(val);
					}
	  }
		function setcheckout(val){

			var values=val.split('/');
			var tgl=values[2]+"-"+values[1]+"-"+values[0];
			checkouthotel=tgl;

	}
    function setcheckouthotel(val){
     setcheckout(val);
	 }

		var kodedes="";
    function setkodedes(val){
      var values=val.split('-');
		  var kode=values[0];
		  kotdes=values[1];
		  kodedes=kode;
		  labeltujuan=val;
    }

		function cari_hotel(){
		  var kamar=jumkamar_hotel;
		  var adt=jumadt_hotel;
		  var chd=jumchd_hotel;
		  var inf=0;
		  var checkin=checkinhotel;
		  var checkout=checkouthotel;
		  $(location).attr('href', '{{ url('/') }}/hotel/otomatiscari/wilayah/'+wilayah+'/checkin/'+checkinhotel+'/checkout/'+checkouthotel+'/des/'+kodedes+'/room/'+kamar+'/roomtype/one/jumadt/'+adt+'/jumchd/'+chd)
		  //alert(checkin);
		}

		var daftardaerah= {
		               domestik: [<?php $urutan=0; $jumnya=count($dafregdom); foreach($dafregdom as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>],
		               internasional: [<?php $urutan=0; $jumnya=count($dafregin); foreach($dafregin as $reg){$urutan+=1; print("[\"".$reg->code."\",\"".$reg->city."\",\"".$reg->country."\"]"); if($urutan!=$jumnya){print(",");}}?>]

		           };
		function pilihwilayah(i){
		  wilayah=i;
		  var ambildaftar=null;
		  if(i==1){
		    ambildaftar=daftardaerah.domestik;
		  }else{
		    ambildaftar=daftardaerah.internasional;
		  }
		  var optionsAsString = "<option value=\"\"></option>";
		        for(var i = 0; i < ambildaftar.length; i++) {
		          optionsAsString += "<option value='"+ambildaftar[i][0]+"-"+ambildaftar[i][1]+"'";
		          optionsAsString +=">"+ambildaftar[i][1]+"-"+ambildaftar[i][2]+ "</option>";
		        }
		        $( '#pildes' ).html( optionsAsString );
		}
		pilihwilayah(1);

		</script>
		@endsection
	@endsection
