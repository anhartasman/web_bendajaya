<div style="line-height:150%;clear:both;vertical-align:top;text-align:center;width:600px;display:block;position:relative;background-color:#ffffff;padding-left:15px;padding-top:15px;padding-right:15px;padding-bottom:15px; background-position:left top;background-repeat:no-repeat;" id="yui_3_16_0_1_1479951516702_9032">
  <img itemprop="image" style="line-height:150%;clear:both;vertical-align:top;display:block;position:relative;" src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="Page Konfirmasi pendaftaran " id="yui_3_16_0_1_1479951516702_9031">
  <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:40px;margin-bottom:20px;" id="yui_3_16_0_1_1479951516702_9033">
    Yth Anhar Tasman,<br><br>
    Terima kasih, permintaan Anda sudah diterima,<br><br>
    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     Transaksi</div>
    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>Asal</b>
      </span>
      :Jakarta
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>Tujuan</b>
      </span>
      :Padang
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>Tanggal berangkat </b>
      </span>
      :Padang
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>Tanggal pulang </b>
      </span>
      :Padang
    </div>

    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:1px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     </div>
    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     Data penumpang : Dewasa 1, anak 2</div>
    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>1</b>
      </span>
      :Jakarta
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>2</b>
      </span>
      :Padang
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>3 </b>
      </span>
      :Padang
      <BR>
      <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
        <b>4 </b>
      </span>
      :Padang
    </div>

    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:1px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     </div>
     <BR>
    Langkah selanjutnya adalah :
    <ol id="yui_3_16_0_1_1479951516702_9034" type="1">
      <br>
      <li id="yui_3_16_0_1_1479951516702_9035">Lakukan pembayaran dengan cara yang tertera dibawah email ini.
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_9036">Email bukti pembayaran ke {{$infowebsite['email']}} dengan subject "Pembayaran transaksi 102343434". atau upload di link berikut : <a href="{{URL('/flight/transaksi')}}/{{$notrx}}">{{URL('/flight/transaksi')}}/{{$notrx}}</a>
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_12901">Kemudian kami akan memeriksa bukti pembayaran Anda.
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_12902">Anda akan mendapat email pemberitahuan berikutnya mengenai valid atau tidaknya bukti pembayaran.
      </li>
      <br>
    </ol>
    Jika masih ada yang belum Anda pahami, bisa hubungi kami via telepon di {{$infowebsite['handphone']}}<br><br>
    Cara melakukan pembayaran :
  </div>

  <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;border-top:1px solid #bf2600;border-bottom:1px solid #bf2600;padding-top:20px;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_9051">
    Total biaya - Rp 1,740,000<br>
  </div>
        <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
          <b id="yui_3_16_0_1_1479951516702_12903">Transfer ke salah satu rekening berikut :</b><br><br>
          @foreach($dafrek as $rek)
          {{ $rek->bank }}<br>
          {{ $rek->norek }}<br>
          {{ $rek->napem }}<br><br>
          @endforeach
        </div>
          <div style="line-height:150%;  font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">

          </div>
          <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-top:1px solid #bf2600;padding-top:20px;" id="yui_3_16_0_1_1479951516702_10676">
             <br><br>
              Jangan Lupa untuk baca halaman tanya jawab kami <a rel="nofollow" target="_blank" href="http://goo.gl/5kAtQk">
                <span style="line-height:150%;clear:both;vertical-align:top;color:#ff0000;">Disini</span></a><br><br>
                Untuk pertanyaan, bisa hubungi kami via Telepon di 083877022823, pada Jam kerja 09.00 - 18.00 - Senin s/d Sabtu - dengan Retha<br><br>
                Atas perhatian dan kerjasamanya, kami ucapkan terima kasih :)<br><br>
                Management {{$infowebsite['namatravel']}}
          </div>

</div>
