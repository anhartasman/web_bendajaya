<div style="line-height:150%;clear:both;vertical-align:top;text-align:center;width:600px;display:block;position:relative;background-color:#ffffff;padding-left:15px;padding-top:15px;padding-right:15px;padding-bottom:15px; background-position:left top;background-repeat:no-repeat;" id="yui_3_16_0_1_1479951516702_9032">
  <img itemprop="image" style="line-height:150%;clear:both;vertical-align:top;display:block;position:relative;" src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="Page Konfirmasi pendaftaran " id="yui_3_16_0_1_1479951516702_9031">
  <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:40px;margin-bottom:20px;" id="yui_3_16_0_1_1479951516702_9033">
    Yth {{$cpname}},<br><br>
    Terima kasih, permintaan Anda sudah diterima,<br><br>
    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     Transaksi {{$notrx}}</div>
     <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">
       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>Destinasi</b>
       </span>
       <?php
      $detdes=daerahhotel_detailByCode($des);
        ?>
       : {{$detdes['city']}} - {{$detdes['country']}}
       <BR>

       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>Tanggal checkin </b>
       </span>
       : {{$tgl_checkin}}
       <BR>
       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>Tanggal checkout </b>
       </span>
       : {{$tgl_checkout}}
       <BR>
       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>Hotel </b>
       </span>
       : {{$namahotel}}
       <BR>
       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>Alamat Hotel </b>
       </span>
       : {{$alamathotel}}
     </div>

    <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:1px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
     </div>
     <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
       {{$room}} kamar</div>
        <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">
     <?php
       $nomurut=0;
       while($nomurut<$room){
        $nomurut+=1; ?>
       <span style="line-height:150%;clear:both;vertical-align:top;color:#bf2600;">
         <b>{{$nomurut}}</b>
       </span>
       : kamar  {{$data_kategori[$nomurut]}}, tempat tidur {{$data_bed[$nomurut]}}, paket makanan :  {{$data_board[$nomurut]}}
       <BR>
         <?php } ?>

     </div>

      <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:1px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
       </div>
     <BR>
    Langkah selanjutnya adalah :
    <ol id="yui_3_16_0_1_1479951516702_9034" type="1">
      <br>
      <li id="yui_3_16_0_1_1479951516702_9035">Lakukan pembayaran dengan cara yang tertera dibawah email ini.
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_9036">Email bukti pembayaran ke {{$infowebsite['email']}} dengan subject "Pembayaran transaksi {{$notrx}}". atau upload di link berikut : <a href="{{URL('/train/transaksi')}}/{{$notrx}}">{{URL('/train/transaksi')}}/{{$notrx}}</a>
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_12901">Kemudian kami akan memeriksa bukti pembayaran Anda.
      </li>
      <br>
      <li id="yui_3_16_0_1_1479951516702_12902">Anda akan mendapat email pemberitahuan berikutnya mengenai valid atau tidaknya bukti pembayaran.
      </li>
      <br>
    </ol>
    Jika masih ada yang belum Anda pahami, bisa hubungi kami via telepon di {{$infowebsite['handphone']}}<br><br>
    Cara melakukan pembayaran :
  </div>

  <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;border-top:1px solid #bf2600;border-bottom:1px solid #bf2600;padding-top:20px;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_9051">
    Total biaya - {{rupiah($biaya)}}<br>
  </div>
        <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-bottom:4px solid #bf2600;padding-bottom:20px;" id="yui_3_16_0_1_1479951516702_10674">
          <b id="yui_3_16_0_1_1479951516702_12903">Transfer ke salah satu rekening berikut :</b><br><br>
          @foreach($dafrek as $rek)
          {{ $rek->bank }}<br>
          {{ $rek->norek }}<br>
          {{ $rek->napem }}<br><br>
          @endforeach
        </div>
          <div style="line-height:150%;  font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-left:8px solid #bf2600;padding-left:20px;" id="yui_3_16_0_1_1479951516702_10675">

          </div>
          <div style="line-height:150%;clear:both;vertical-align:top;font-family:'Poppins', 'Arial';font-weight:300;font-style:normal;text-decoration:none;text-align:left;font-size:11pt;letter-spacing:0px;color:#333333;display:block;position:relative;margin-top:20px;border-top:1px solid #bf2600;padding-top:20px;" id="yui_3_16_0_1_1479951516702_10676">
             <br><br>
                Untuk pertanyaan, bisa hubungi kami via Telepon di {{$infowebsite['handphone']}}<br><br>
                Atas perhatian dan kerjasamanya, kami ucapkan terima kasih :)<br><br>
                Management {{$infowebsite['namatravel']}}
          </div>

</div>
