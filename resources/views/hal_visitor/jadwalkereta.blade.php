@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0]['SeatAvb']>0)
<?php
$dafseat="";
$biayapergi=$dafdeparture['Fares'][0]['TotalFare'];
$biayapergi=angkaceil($biayapergi);
$jampergiberangkat=DateToIndo($dafdeparture['ETD']);
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid=$dafdeparture['TrainNo'];
$jampergitiba=DateToIndo($dafdeparture['ETA']);
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";

$kumpulaniddep=$dafdeparture['Fares'][0]['selectedIDdep'];
$sisabangku=$dafdeparture['Fares'][0]['SeatAvb'];


 ?>
 <div class="list-item-entry isiitempergi isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
 <div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
 <div class="table-view">
   <div class="radius-top cell-view " id="{{$bahanid}}_gbr">

     <img src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" id="{{$bahanid}}_img"  alt="" style="padding-left:20px;width:100%;height:50px;">
   </div>
   <div class="title hotel-middle  cell-view">
     <div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
     <div class="date grid-hidden"><strong class="color-red-3">{{$dafdeparture['TrainName']}} {{$dafdeparture['TrainNo']}}</strong></div>
       <h4><b><span class="justmobile" style="padding-left:5px;">Harga : </span>{{rupiahceil($biayapergi)}}</b></h4>
   <h4 class="nomobile"><b>{{$jampergiberangkat}} - {{$jampergitiba}}</b></h4>
   <div class="justmobile" style="padding-left:5px;"><h4><b>Pergi : {{$jampergiberangkat}}</b></h4> <h4><b>Pulang : {{$jampergitiba}}</b></h4> </div>
   </div>
   <div class="title hotel-middle clearfix cell-view">
     <div class="hotel-person color-dark-2">{{$sisabangku}} bangku</div>

   </div>
   <div class="title hotel-right  cell-view">
<div class="nomobile">
   <a style="margin-right:10px;" class="c-button b-40 bg-red-3 hv-red-3-o" href="#ketpilihan"
   onclick="pilihPergi('{{$bahanid}}'
   ,'{{$jampergiberangkat}}'
   ,'{{$jampergitiba}}'
   ,'{{$biayapergi}}'
   ,'{{$dafdeparture['TrainName']}}'
   ,'{{$kumpulaniddep}}'
   )">pilih</a>
</div>
<div class="justmobile">
   <a class="c-button b-40 bg-red-3 hv-red-3-o" href="#ketpilihan"
   onclick="pilihPergi('{{$bahanid}}'
   ,'{{$jampergiberangkat}}'
   ,'{{$jampergitiba}}'
   ,'{{$biayapergi}}'
   ,'{{$dafdeparture['TrainName']}}'
   ,'{{$kumpulaniddep}}'
   )">pilih</a>
</div>
   </div>
   </div>
   </div>
   </div>
@endif
@endforeach
@endif

@if ($jenter == 'R')

  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              $jampulangberangkat=DateToIndo($dafreturn['ETD']);
              $biayapulang=$dafreturn['Fares'][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba=DateToIndo($dafreturn['ETA']);
              $bahanid=$dafreturn['TrainNo'];

                  $biayapulang=angkaceil($biayapulang);

                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $i=0;
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";

                 $kumpulanidret=$dafreturn['Fares'][0]['selectedIDret'];
                 $sisabangku=$dafreturn['Fares'][0]['SeatAvb'];


              foreach($dafreturn['Fares'] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }


               ?>
               <div class="list-item-entry isiitempulang isiitem" id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
               <div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
               <div class="table-view">
                 <div class="radius-top cell-view " id="{{$bahanid}}_gbr">

                   <img src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" id="{{$bahanid}}_img"  alt="" style="padding-left:20px;width:100%;height:50px;">
                 </div>
                 <div class="title hotel-middle  cell-view">
                   <div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
                   <div class="date grid-hidden"><strong class="color-red-3">{{$dafreturn['TrainName']}} {{$dafreturn['TrainNo']}}</strong></div>
                     <h4><b><span class="justmobile" style="padding-left:5px;">Harga : </span>{{rupiahceil($biayapulang)}}</b></h4>
                 <h4 class="nomobile"><b>{{$jampulangberangkat}} - {{$jampulangtiba}}</b></h4>
                 <div class="justmobile" style="padding-left:5px;"><h4><b>Pergi : {{$jampulangberangkat}}</b></h4> <h4><b>Pulang : {{$jampulangtiba}}</b></h4> </div>
                 </div>
                 <div class="title hotel-middle clearfix cell-view">
                   <div class="hotel-person color-dark-2">{{$sisabangku}} bangku</div>

                 </div>
                 <div class="title hotel-right  cell-view">
               <div class="nomobile">
                 <a style="margin-right:10px;" class="c-button b-40 bg-red-3 hv-red-3-o" href="#ketpilihan"
                 onclick="pilihPulang('{{$bahanid}}'
                 ,'{{$jampulangberangkat}}'
                 ,'{{$jampulangtiba}}'
                 ,'{{$biayapulang}}'
                 ,'{{$dafreturn['TrainName']}}'
                 ,'{{$kumpulanidret}}'
                 )">pilih</a>
               </div>
               <div class="justmobile">
                 <a style="margin-right:10px;" class="c-button b-40 bg-red-3 hv-red-3-o" href="#ketpilihan"
                 onclick="pilihPulang('{{$bahanid}}'
                 ,'{{$jampulangberangkat}}'
                 ,'{{$jampulangtiba}}'
                 ,'{{$biayapulang}}'
                 ,'{{$dafreturn['TrainName']}}'
                 ,'{{$kumpulanidret}}'
                 )">pilih</a>
               </div>
                 </div>
                 </div>
                 </div>
                 </div>
@endif
@endforeach
@endif

@endif
