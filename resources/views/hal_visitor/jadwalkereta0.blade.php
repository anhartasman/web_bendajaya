@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0]['SeatAvb']>0)
<?php
$dafseat="";
$biayapergi=$dafdeparture['Fares'][0]['TotalFare'];
$biayapergi=angkaceil($biayapergi);
$jampergiberangkat=$dafdeparture['ETD'];
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid=$dafdeparture['TrainNo'];
$jampergitiba=$dafdeparture['ETA'];
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";

$kumpulaniddep=$dafdeparture['Fares'][0]['selectedIDdep'];


 ?>
<div class="list-item-entry isiitempergi isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
<div class="hotel-item bg-white" id="{{$bahanid}}_div">
<div class="table-view">
  <div class="title hotel-right clearfix cell-view grid-hidden"  id="{{$bahanid}}_gbr"style="padding-left:20px;" >
    <h5 style="padding-left:20px;"> {{$dafdeparture['TrainName']}}  </h5>
    <div style="display:none;" id="{{$bahanid}}_numbiaya">
      {{$biayapergi}}
    </div>
    <div style="display:none;" id="{{$bahanid}}_numwaktu">
      {{date('Ymd',strtotime($jampergiberangkat))}}
    </div>
 </div>
<div  class="title  hotel-right   cell-view grid-hidden">

        <div class=" color-black">{{$jampergiberangkat}}</div>
</div>
<div  class="title  hotel-right   cell-view grid-hidden">

       <div class=" color-black">{{$jampergitiba}}</div>
</div>
<div  class="title hotel-right    cell-view grid-hidden">

  <strong class="color-red-3" >

  3 Jam

  </strong>
</div>
<div  class="title hotel-right   cell-view grid-hidden">

  <strong class="color-red-3" >

  {{rupiah($biayapergi)}}

  </strong>
</div>
<div  class="title hotel-right   cell-view grid-hidden"style="padding-right:20px;">
  <!----  <div class="hotel-right-text color-dark-2">one way flights</div>
  <div class="hotel-right-text color-dark-2">1 stop</div>
-->
<a href="#ketpilihan"class="c-button b-40 bg-red-3 hv-red-3-o"
onclick="pilihPergi('{{$bahanid}}'
,'{{$jampergiberangkat}}'
,'{{$jampergitiba}}'
,'{{$biayapergi}}'
,'{{$dafdeparture['TrainName']}}'
,'{{$kumpulaniddep}}'
)">pilih</a>
</div>
</div>
</div>
</div>
@endif
@endforeach
@endif

@if ($jenter == 'R')

  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              $jampulangberangkat=DateToIndo($dafreturn['ETD']);
              $biayapulang=$dafreturn['Fares'][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba=DateToIndo($dafreturn['ETA']);
              $bahanid=$dafreturn['TrainNo'];

                  $biayapulang=angkaceil($biayapulang);

                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $i=0;
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";

                 $kumpulanidret=$dafreturn['Fares'][0]['selectedIDret'];



              foreach($dafreturn['Fares'] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }


               ?>
    <div class="list-item-entry isiitempulang isiitem" id="{{$bahanid}}" isiitem="1"  biaya="{{$biayapulang}}"  untuk="pulang" data-waktu="{{strtotime($jampulangberangkat)}}">
    <div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
      <div class="table-view">
        <div class="title hotel-right clearfix cell-view grid-hidden"  id="{{$bahanid}}_gbr"style="padding-left:20px;" >
          <h5 style="padding-left:20px;"> {{$dafreturn['TrainName']}}  </h5>
          <div style="display:none;" id="{{$bahanid}}_numbiaya">
            {{$biayapulang}}
          </div>
          <div style="display:none;" id="{{$bahanid}}_numwaktu">
            {{date('Ymd',strtotime($jampulangberangkat))}}
          </div>
        </div>
        <div  class="title  hotel-right   cell-view grid-hidden">

              <div class=" color-black">{{$jampulangberangkat}}</div>
        </div>
        <div  class="title  hotel-right   cell-view grid-hidden">

             <div class=" color-black">{{$jampulangtiba}}</div>
        </div>
        <div  class="title hotel-right    cell-view grid-hidden">

        <strong class="color-red-3" >

        3 Jam

        </strong>
        </div>
        <div  class="title hotel-right   cell-view grid-hidden">

        <strong class="color-red-3" >

        {{rupiah($biayapulang)}}

        </strong>
        </div>
        <div  class="title hotel-right   cell-view grid-hidden"style="padding-right:20px;">
        <!----  <div class="hotel-right-text color-dark-2">one way flights</div>
        <div class="hotel-right-text color-dark-2">1 stop</div>
        -->
        <a href="#ketpilihan"class="c-button b-40 bg-red-3 hv-red-3-o"
        onclick="pilihPulang('{{$bahanid}}'
        ,'{{$jampulangberangkat}}'
        ,'{{$jampulangtiba}}'
        ,'{{$biayapulang}}'
        ,'{{$dafreturn['TrainName']}}'
        ,'{{$kumpulanidret}}'
        )">pilih</a>
        </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endif

@endif
