@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0]['SeatAvb']>0)
<?php
$dafseat="";
$biayapergi=$dafdeparture['Fares'][0]['TotalFare'];
$biayapergi=angkaceil($biayapergi);
$jampergiberangkat=DateToIndo($dafdeparture['ETD']);
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid=$dafdeparture['TrainNo'];
$jampergitiba=DateToIndo($dafdeparture['ETA']);
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";

$kumpulaniddep=$dafdeparture['Fares'][0]['selectedIDdep'];
$sisabangku=$dafdeparture['Fares'][0]['SeatAvb'];


 ?>
<div class="list-item-entry isiitempergi isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
<div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
<div class="table-view">
<div class="kotakpilih cell-view"  id="{{$bahanid}}_gbr" >
<img style="padding-left:20px;width:180px;height:50px;" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" alt="">
</div>
<div class="title hotel-middle cell-view">
<h5>{{$dafdeparture['TrainName']}} {{$dafdeparture['TrainNo']}}</h5>
<strong class="color-red-3">

{{rupiah($biayapergi)}}

</strong>

<h6 class="color-grey-3 list-hidden">one way flights</h6>
<!----  <h4><b>Cheap Flights to Paris</b></h4>-->
 <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
 <div class="fi_block grid-hidden row row10">
   <div class="flight-icon col-xs-6 col10">
     <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
     <div class="fi_content">
       <div class="fi_title color-dark-2">pergi</div>
       <div class="fi_text color-black">{{$jampergiberangkat}}</div>
     </div>
   </div>
   <div class="flight-icon col-xs-6 col10">
     <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
     <div class="fi_content">
       <div class="fi_title color-dark-2">tiba</div>
       <div class="fi_text color-black">{{$jampergitiba}}</div>
     </div>
   </div>
 </div>
<a href="#ketpilihan" class="c-button b-40 bg-red-3 hv-red-3-o"
onclick="pilihPergi('{{$bahanid}}'
,'{{$jampergiberangkat}}'
,'{{$jampergitiba}}'
,'{{$biayapergi}}'
,'{{$dafdeparture['TrainName']}}'
,'{{$kumpulaniddep}}'
)">pilih</a>
<!----<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>-->
</div>

<div class="title hotel-middle clearfix cell-view grid-hidden">
  Sisa bangku : {{$sisabangku}}
</div>
</div>
</div>
</div>
@endif
@endforeach
@endif

@if ($jenter == 'R')

  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              $jampulangberangkat=DateToIndo($dafreturn['ETD']);
              $biayapulang=$dafreturn['Fares'][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba=DateToIndo($dafreturn['ETA']);
              $bahanid=$dafreturn['TrainNo'];

                  $biayapulang=angkaceil($biayapulang);

                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $i=0;
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";

                 $kumpulanidret=$dafreturn['Fares'][0]['selectedIDret'];
                 $sisabangku=$dafreturn['Fares'][0]['SeatAvb'];


              foreach($dafreturn['Fares'] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }


               ?>
               <div class="list-item-entry isiitempulang isiitem" id="{{$bahanid}}" isiitem="1"  data-harga="{{$biayapulang}}"  untuk="pulang" data-waktu="{{strtotime($jampulangberangkat)}}">
                <div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
                  <div class="table-view">
                      <div class="kotakpilih cell-view"  id="{{$bahanid}}_gbr" >
                        <img style="padding-left:20px;width:180px;height:50px;" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" alt="">
                      </div>
                      <div class="title hotel-middle cell-view">
                        <h5>{{$dafreturn['TrainName']}} {{$dafreturn['TrainNo']}}</h5>
                        <strong class="color-red-3">

            {{rupiah($biayapulang)}}

                         </strong>

                        <h6 class="color-grey-3 list-hidden">one way flights</h6>
                        <!----  <h4><b>Cheap Flights to Paris</b></h4>-->
                          <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                          <div class="fi_block grid-hidden row row10">
                            <div class="flight-icon col-xs-6 col10">
                              <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                              <div class="fi_content">
                                <div class="fi_title color-dark-2">pergi</div>
                                <div class="fi_text color-black">{{$jampulangberangkat}}</div>
                              </div>
                            </div>
                            <div class="flight-icon col-xs-6 col10">
                              <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                              <div class="fi_content">
                                <div class="fi_title color-dark-2">tiba</div>
                                <div class="fi_text color-black">{{$jampulangtiba}}</div>
                              </div>
                            </div>
                          </div>
                        <a href="#ketpilihan" class="c-button b-40 bg-red-3 hv-red-3-o" onclick="pilihPulang('{{$bahanid}}'
                        ,'{{$jampulangberangkat}}'
                        ,'{{$jampulangtiba}}'
                        ,'{{$biayapulang}}'
                        ,'{{$dafreturn['TrainName']}}'
                        ,'{{$kumpulanidret}}'
                        )">pilih</a>
                        <!----<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>-->
                      </div>

                      <div class="title hotel-middle clearfix cell-view grid-hidden">
                        Sisa bangku : {{$sisabangku}}
                      </div>
                    </div>
                </div>
            </div>
@endif
@endforeach
@endif

@endif
