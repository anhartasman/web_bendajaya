@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0]['SeatAvb']>0)
<?php
$dafseat="";
$biayapergi=$dafdeparture['Fares'][0]['TotalFare'];
$biayapergi=angkaceil($biayapergi);
$hasildiskon=hasildiskon($settingan_member->mmid,"KAI",$biayapergi);
$jampergiberangkat=DateToIndo($dafdeparture['ETD']);
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid=$dafdeparture['TrainNo'];
$jampergitiba=DateToIndo($dafdeparture['ETA']);
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";

$kumpulaniddep=$dafdeparture['Fares'][0]['selectedIDdep'];
$sisabangku=$dafdeparture['Fares'][0]['SeatAvb'];


 ?>
 <div class="flight-list listing-style3 flight isiitempergi isiitem"  id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
  <article class="box "  id="{{$bahanid}}_div">
      <figure class="col-xs-3 col-sm-2" id="{{$bahanid}}_gbr">
          <span><img alt="" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" width="270" height="160"></span>
      </figure>
      <div class="details col-xs-9 col-sm-10">
          <div class="details-wrapper">
              <div class="first-row">
                  <div>
                      <h4 class="box-title">{{$dafdeparture['TrainName']}} {{$dafdeparture['TrainNo']}}<small>{{$sisabangku}} bangku</small></h4>

                  </div>
                  <div>
                      <span class="price"><small>Harga</small><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</span>
                  </div>
              </div>
              <div class="second-row">
                  <div class="time">
                      <div class="take-off col-sm-4">
                          <div>
                              <span class="skin-color">Pergi</span><br />{{$jampergiberangkat}}
                          </div>
                      </div>
                      <div class="landing col-sm-4">
                          <div>
                              <span class="skin-color">Sampai</span><br />{{$jampergitiba}}
                          </div>
                      </div>
                      <div class="total-time col-sm-4">
                          <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                          <div>
                            <?php
                            $to_time = strtotime($jampergitiba);
                            $from_time = strtotime($jampergiberangkat);
                            $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                            ?>
                              <span class="skin-color">Lama perjalanan</span><br /><span id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</span>
                          </div>
                      </div>
                  </div>
                  <div class="action">
                      <a href="#ketpilihan" class="button btn-small full-width"
                      onclick="pilihPergi('{{$bahanid}}'
                      ,'{{$jampergiberangkat}}'
                      ,'{{$jampergitiba}}'
                      ,'{{$biayapergi}}'
                      ,'{{$dafdeparture['TrainName']}}'
                      ,'{{$kumpulaniddep}}'
                      ,'{{angkaceil($hasildiskon)}}'
                      ,'{{Crypt::encrypt(angkaceil($hasildiskon))}}'
                      )">SELECT</a>
                  </div>
              </div>
          </div>
      </div>
  </article>
</div>
@endif
@endforeach
@endif

@if ($jenter == 'R')

  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              $jampulangberangkat=DateToIndo($dafreturn['ETD']);
              $biayapulang=$dafreturn['Fares'][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba=DateToIndo($dafreturn['ETA']);
              $bahanid=$dafreturn['TrainNo'];

                  $biayapulang=angkaceil($biayapulang);
                  $hasildiskon=hasildiskon($settingan_member->mmid,"KAI",$biayapulang);

                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $i=0;
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";

                 $kumpulanidret=$dafreturn['Fares'][0]['selectedIDret'];
                 $sisabangku=$dafreturn['Fares'][0]['SeatAvb'];


              foreach($dafreturn['Fares'] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }


               ?>
               <div class="flight-list listing-style3 flight isiitempulang isiitem"  id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
                <article class="box "  id="{{$bahanid}}_div">
                    <figure class="col-xs-3 col-sm-2" id="{{$bahanid}}_gbr">
                        <span><img alt="" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" width="270" height="160"></span>
                    </figure>
                    <div class="details col-xs-9 col-sm-10">
                        <div class="details-wrapper">
                            <div class="first-row">
                                <div>
                                    <h4 class="box-title">{{$dafreturn['TrainName']}} {{$dafreturn['TrainNo']}}<small>{{$sisabangku}} bangku</small></h4>

                                </div>
                                <div>
                                    <span class="price"><small>Harga</small><strike>{{rupiahceil($biayapulang)}}</strike> {{rupiahceil($hasildiskon)}}</span>
                                </div>
                            </div>
                            <div class="second-row">
                                <div class="time">
                                    <div class="take-off col-sm-4">
                                        <div>
                                            <span class="skin-color">Pergi</span><br />{{$jampulangberangkat}}
                                        </div>
                                    </div>
                                    <div class="landing col-sm-4">
                                        <div>
                                            <span class="skin-color">Sampai</span><br />{{$jampulangtiba}}
                                        </div>
                                    </div>
                                    <div class="total-time col-sm-4">
                                        <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                        <div>
                                          <?php
                                          $to_time = strtotime($jampulangtiba);
                                          $from_time = strtotime($jampulangberangkat);
                                          $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                                          ?>
                                            <span class="skin-color">Lama perjalanan</span><br /><span id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="action">
                                    <a href="#ketpilihan" class="button btn-small full-width"
                                    onclick="pilihPulang('{{$bahanid}}'
                                    ,'{{$jampulangberangkat}}'
                                    ,'{{$jampulangtiba}}'
                                    ,'{{$biayapulang}}'
                                    ,'{{$dafreturn['TrainName']}}'
                                    ,'{{$kumpulanidret}}'
                                    ,'{{angkaceil($hasildiskon)}}'
                                    ,'{{Crypt::encrypt(angkaceil($hasildiskon))}}'
                                    )">SELECT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
               </div>
@endif
@endforeach
@endif

@endif
