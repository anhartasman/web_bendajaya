@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0]['SeatAvb']>0)
<?php
$dafseat="";
$biayapergi=$dafdeparture['Fares'][0]['TotalFare'];
$biayapergi=angkaceil($biayapergi);
$hasildiskon=hasildiskon($settingan_member->mmid,"KAI",$biayapergi);
$jampergiberangkat=DateToIndo($dafdeparture['ETD']);
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid=$dafdeparture['TrainNo'];
$jampergitiba=DateToIndo($dafdeparture['ETA']);
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";

$kumpulaniddep=$dafdeparture['Fares'][0]['selectedIDdep'];
$sisabangku=$dafdeparture['Fares'][0]['SeatAvb'];


 ?>
 <li class="isiitempergi isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
   <div class="booking-item-container" id="{{$bahanid}}_div">
       <div class="booking-item">
           <div class="row">
               <div class="col-md-2">
                   <div class="booking-item-airline-logo" id="{{$bahanid}}_gbr">
                       <img  id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg"  title="{{$dafdeparture['TrainName']}} {{$dafdeparture['TrainNo']}}" />
                       <p>{{$dafdeparture['TrainName']}} {{$dafdeparture['TrainNo']}}</p>
                   </div>
               </div>
               <div class="col-md-5">

                   <div class="booking-item-flight-details">
                     <?php
                     $to_time = strtotime($jampergitiba);
                     $from_time = strtotime($jampergiberangkat);
                     $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                     $detstation=station_detailByCode($org);
                     $pilihanpergi_tempatpergi=$detstation["st_city"].",".$detstation["st_name"];
                     ?>
                       <div class="booking-item-departure">
                           <h5>{{date('h:i A', $from_time)}}</h5>
                           <p class="booking-item-destination">{{date('D', $from_time)}}, {{date('m d', $to_time)}}</p>
                           <p class="booking-item-destination">{{$pilihanpergi_tempatpergi}}</p>
                       </div>
                       <?php
                       $to_time = strtotime($jampergitiba);
                       $from_time = strtotime($jampergiberangkat);
                       $totalminuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                      $detstation=station_detailByCode($des);
                      $pilihanpergi_tempattiba=$detstation["st_city"].",".$detstation["st_name"];
                      ?>
                       <div class="booking-item-arrival">
                         <h5>{{date('h:i A', $to_time)}}</h5>
                         <p class="booking-item-destination">{{date('D', $to_time)}}, {{date('m d', $to_time)}}</p>
                         <p class="booking-item-destination">{{$pilihanpergi_tempattiba}}</p>
                       </div>
                   </div>
               </div>
               <div class="col-md-2">
                   <h5 id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</h5>
               </div>
               <div class="col-md-3">
                 <h5><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</h5>
                 <a class="btn btn-primary" href="#ketpilihan"
                 onclick="pilihPergi('{{$bahanid}}'
                 ,'{{$jampergiberangkat}}'
                 ,'{{$jampergitiba}}'
                 ,'{{$biayapergi}}'
                 ,'{{$dafdeparture['TrainName']}}'
                 ,'{{$kumpulaniddep}}'
                 ,'{{angkaceil($hasildiskon)}}'
                 ,'{{Crypt::encrypt(angkaceil($hasildiskon))}}'
                 ,'{{$pilihanpergi_tempatpergi}}'
                 ,'{{$pilihanpergi_tempattiba}}'
                 )">Select</a>
               </div>
           </div>
       </div>

   </div>

 </li>

@endif
@endforeach
@endif
@if ($jenter == 'R')
  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              $jampulangberangkat=DateToIndo($dafreturn['ETD']);
              $biayapulang=$dafreturn['Fares'][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba=DateToIndo($dafreturn['ETA']);
              $bahanid=$dafreturn['TrainNo'];

                  $biayapulang=angkaceil($biayapulang);
                  $hasildiskon=hasildiskon($settingan_member->mmid,"KAI",$biayapulang);

                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $i=0;
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";

                 $kumpulanidret=$dafreturn['Fares'][0]['selectedIDret'];
                 $sisabangku=$dafreturn['Fares'][0]['SeatAvb'];

              foreach($dafreturn['Fares'] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }

               ?>
               <li class=" flight isiitempulang isiitem" id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
                 <div class="booking-item-container" id="{{$bahanid}}_div">
                     <div class="booking-item">
                         <div class="row">
                             <div class="col-md-2">
                                 <div class="booking-item-airline-logo" id="{{$bahanid}}_gbr">
                                     <img  id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-KAI')}}.jpg" title="{{$dafreturn['TrainName']}} {{$dafreturn['TrainNo']}}" />
                                     <p>{{$dafreturn['TrainName']}} {{$dafreturn['TrainNo']}}</p>
                                 </div>
                             </div>
                             <div class="col-md-5">

                                 <div class="booking-item-flight-details">
                                   <?php
                                   $to_time = strtotime($jampulangtiba);
                                   $from_time = strtotime($jampulangberangkat);
                                   $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                                   $detstation=station_detailByCode($des);
                                   $pilihanpulang_tempatpergi=$detstation["st_city"].",".$detstation["st_name"];
                                   ?>
                                     <div class="booking-item-departure">
                                         <h5>{{date('h:i A', $to_time)}}</h5>
                                         <p class="booking-item-destination">{{date('D', $to_time)}}, {{date('m d', $to_time)}}</p>
                                         <p class="booking-item-destination">{{$pilihanpulang_tempatpergi}}</p>
                                     </div>
                                     <?php
                                     $to_time = strtotime($jampulangtiba);
                                     $from_time = strtotime($jampulangberangkat);
                                     $totalminuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                                     $detstation=station_detailByCode($org);
                                     $pilihanpulang_tempattiba=$detstation["st_city"].",".$detstation["st_name"];
                                     ?>
                                     <div class="booking-item-arrival">
                                       <h5>{{date('h:i A', $to_time)}}</h5>
                                       <p class="booking-item-destination">{{date('D', $to_time)}}, {{date('m d', $to_time)}}</p>
                                       <p class="booking-item-destination">{{$pilihanpulang_tempattiba}}</p>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-2">
                                 <h5 id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</h5>
                             </div>
                             <div class="col-md-3">
                               <h5><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</h5>
                               <a class="btn btn-primary" href="#ketpilihan"
                               onclick="pilihPulang('{{$bahanid}}'
                               ,'{{$jampulangberangkat}}'
                               ,'{{$jampulangtiba}}'
                               ,'{{$biayapulang}}'
                               ,'{{$dafreturn['TrainName']}}'
                               ,'{{$kumpulanidret}}'
                               ,'{{angkaceil($hasildiskon)}}'
                               ,'{{Crypt::encrypt(angkaceil($hasildiskon))}}'
                               ,'{{$pilihanpulang_tempatpergi}}'
                               ,'{{$pilihanpulang_tempattiba}}'
                               )">Select</a>
                             </div>
                         </div>
                     </div>

                 </div>

               </li>

@endif
@endforeach
@endif
@endif
