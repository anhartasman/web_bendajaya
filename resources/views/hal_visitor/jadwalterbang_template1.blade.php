@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0][0]['SeatAvb']>0)
<?php
$dafseat="";
//$biayapergi=$dafdeparture['Fares'][0][0]['TotalFare'];
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid="";
$jampergitiba="";

    foreach($dafdeparture['Flights'] as $trans){
    //echo "TRANS ".$trans['FlightNo']. " : ".$trans['ETA']. "<BR>";
      if($aw==1){
      $daftranpergi=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      $pesawatawal=$trans['FlightNo'];
        $aw=0;
      }else{
      $daftranpergi.=",".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      }
      $jampergitiba=$trans['ETA'];
      $bahanid.=$trans['FlightNo'];

    }

$biayapergi=0;
    foreach($dafdeparture['Fares'] as $dafhar){
      if($dafhar!=null){
        $biayapergi+=$dafhar[0]['TotalFare'];
      }
    //  $biayapergi+=$dafhar[0]['TotalFare'];
    }

    $biayapergi=angkaceil($biayapergi);
    $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapergi);

$jampergiberangkat=DateToIndo($dafdeparture['Flights'][0]['ETD']);
$stattransitpergi="";
$iddepawal=$dafdeparture['Fares'][0][0]['selectedIDdep'];
//penting $iddeptran=$dafdeparture['Fares'][1][0]['selectedIDdep'];
$jumtran=COUNT($dafdeparture['Fares']);
$i=0;
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";
while($i<$jumtran){

  $iddep1=$dafdeparture['Fares'][$i][0]['selectedIDdep'];
  //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
  //echo "id dep : ".$iddep1;
  //echo "<BR>";
  if($i>0){
    $kumpulaniddep.=",";
  }
  $kumpulaniddep.=$iddep1;
  $i+=1;
}



foreach($dafdeparture['Fares'][0] as $subclass){
$banim=implode(",",$subclass);
$dafseat.="#".$banim;

}
if(count($dafdeparture['Flights'])>1){
  $stattransitpergi=(count($dafdeparture['Flights'])-1)."x transit";
}else{
  $stattransitpergi="langsung";
}

    $jampergitiba=DateToIndo($jampergitiba);
 ?>
<div class="list-item-entry isiitempergi isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
<div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
<div class="table-view">
  <div class="radius-top cell-view " id="{{$bahanid}}_gbr">

    <img src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" id="{{$bahanid}}_img"  alt="" style="padding-left:20px;width:100%;height:50px;">
  </div>
  <div class="title hotel-middle clearfix cell-view">
    <div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
    <div class="date grid-hidden"><strong class="color-red-3">{{$pesawatawal}}</strong></div>
      <h4><b><span class="justmobile" style="padding-left:5px;">Harga : </span><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</b></h4>
  <h4 class="nomobile"><b>{{$jampergiberangkat}} - {{$jampergitiba}}</b></h4>
  <div class="justmobile" style="padding-left:5px;"><h4><b>Pergi : {{$jampergiberangkat}}</b></h4> <h4><b>Pulang : {{$jampergitiba}}</b></h4> </div>
  </div>
  <div class="nomobile">
  <div class="title hotel-right clearfix cell-view">
    <div class="hotel-person color-dark-2"></div>
      <div class="hotel-person color-dark-2">{{$stattransitpergi}}</div>
    <div class="hotel-person color-dark-2"></div>
  </div>
  </div>
  <div class="title hotel-right clearfix cell-view">
  <a style="margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" href="#ketpilihan"
  onclick="pilihPergi('{{$bahanid}}'
  ,'{{$daftranpergi}}'
  ,'{{$pesawatawal}}'
  ,'{{$jampergiberangkat}}'
  ,'{{$jampergitiba}}'
  ,'{{$biayapergi}}'
  ,'{{$ac}}'
  ,'{{$stattransitpergi}}'
  ,'{{$dafseat}}'
  ,'{{$kumpulaniddep}}'
  ,'{{angkaceil($hasildiskon)}}'
  )">pilih</a>
  </div>
  </div>
  </div>
  </div>
@endif
@endforeach
@endif
@if ($jenter == 'R')
  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              //$biayapulang=$dafreturn['Fares'][0][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba="";
              $bahanid="";

                  foreach($dafreturn['Flights'] as $trans){
                    if($aw==1){
                    $daftranpulang=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    $pesawatawal=$trans['FlightNo'];
                      $aw=0;
                    }else{
                    $daftranpulang.=", ".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    }
                    $jampulangtiba=$trans['ETA'];
                    $bahanid.=$trans['FlightNo'];
                  }
                  $biayapulang=0;
                  foreach($dafreturn['Fares'] as $dafhar){
                    if($dafhar!=null){
                      $biayapulang+=$dafhar[0]['TotalFare'];
                    }
                  }

                  $biayapulang=angkaceil($biayapulang);
                  $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapulang);

                  $jampulangberangkat=DateToIndo($dafreturn['Flights'][0]['ETD']);
                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Fares']);
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";
                  $i=0;
                  while($i<$jumtran){

                    $idret1=$dafreturn['Fares'][$i][0]['selectedIDret'];
                    //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
                    //echo "id dep : ".$iddep1;
                    //echo "<BR>";
                    if($i>0){
                      $kumpulanidret.=",";
                    }
                    $kumpulanidret.=$idret1;
                    $i+=1;
                  }


              foreach($dafreturn['Fares'][0] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }
              if(count($dafreturn['Flights'])>1){
                $stattransitpulang=(count($dafreturn['Flights'])-1)."x transit";
              }else{
                $stattransitpulang="langsung";
              }

                  $jampulangtiba=DateToIndo($jampulangtiba);
               ?>
               <div class="list-item-entry isiitempulang isiitem" id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
               <div class="hotel-item style-10 bg-white" id="{{$bahanid}}_div">
               <div class="table-view">
                 <div class="radius-top cell-view " id="{{$bahanid}}_gbr">
                   <img src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" id="{{$bahanid}}_img"  alt="" style="padding-left:20px;width:100%;height:50px;">
                 </div>
                 <div class="title hotel-middle clearfix cell-view">
                   <div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
                   <div class="date grid-hidden nomobile"><strong class="color-red-3">{{$pesawatawal}}</strong></div>
                     <h4><b><span class="justmobile" style="padding-left:5px;">Harga : </span><strike>{{rupiahceil($biayapulang)}}</strike> {{rupiahceil($hasildiskon)}}</b></h4>
                 <h4 class="nomobile"><b>{{$jampulangberangkat}} - {{$jampulangtiba}}</b></h4>
                 <div class="justmobile" style="padding-left:5px;"><h4><b>Pergi : {{$jampulangberangkat}}</b></h4> <h4><b>Pulang : {{$jampulangtiba}}</b></h4> </div>
                 </div>
                 <div class="nomobile">
                 <div class="title hotel-right clearfix cell-view">
                   <div class="hotel-person color-dark-2"></div>
                     <div class="hotel-person color-dark-2">{{$stattransitpulang}}</div>
                   <div class="hotel-person color-dark-2"></div>
                 </div>
                 </div>
                 <div class="title hotel-right clearfix cell-view">
                 <a style="margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" href="#ketpilihan"
                 onclick="pilihPulang('{{$bahanid}}'
                 ,'{{$daftranpulang}}'
                 ,'{{$pesawatawal}}'
                 ,'{{$jampulangberangkat}}'
                 ,'{{$jampulangtiba}}'
                 ,'{{$biayapulang}}'
                 ,'{{$ac}}'
                 ,'{{$stattransitpulang}}'
                 ,'{{$dafseat}}'
                 ,'{{$kumpulanidret}}'
                 ,'{{angkaceil($hasildiskon)}}')">pilih</a>
                 </div>
                 </div>
                 </div>
                 </div>
@endif
@endforeach
@endif
@endif
