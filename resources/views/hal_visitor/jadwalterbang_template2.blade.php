@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0][0]['SeatAvb']>0)
<?php
$dafseat="";
//$biayapergi=$dafdeparture['Fares'][0][0]['TotalFare'];
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid="";
$jampergitiba="";

    foreach($dafdeparture['Flights'] as $trans){
    //echo "TRANS ".$trans['FlightNo']. " : ".$trans['ETA']. "<BR>";
      if($aw==1){
      $daftranpergi=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      $pesawatawal=$trans['FlightNo'];
        $aw=0;
      }else{
      $daftranpergi.=",".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      }
      $jampergitiba=$trans['ETA'];
      $bahanid.=$trans['FlightNo'];

    }

$biayapergi=0;
    foreach($dafdeparture['Fares'] as $dafhar){
      if($dafhar!=null){
        $biayapergi+=$dafhar[0]['TotalFare'];
      }
    //  $biayapergi+=$dafhar[0]['TotalFare'];
    }

    $biayapergi=angkaceil($biayapergi);
    $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapergi);


$jampergiberangkat=DateToIndo($dafdeparture['Flights'][0]['ETD']);
$stattransitpergi="";
$iddepawal=$dafdeparture['Fares'][0][0]['selectedIDdep'];
//penting $iddeptran=$dafdeparture['Fares'][1][0]['selectedIDdep'];
$jumtran=COUNT($dafdeparture['Flights']);
$i=0;
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";
while($i<$jumtran){

  $iddep1=$dafdeparture['Fares'][$i][0]['selectedIDdep'];
  //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
  //echo "id dep : ".$iddep1;
  //echo "<BR>";
  if($i>0){
    $kumpulaniddep.=",";
  }
  $kumpulaniddep.=$iddep1;
  $i+=1;
}



foreach($dafdeparture['Fares'][0] as $subclass){
$banim=implode(",",$subclass);
$dafseat.="#".$banim;

}
if(count($dafdeparture['Flights'])>1){
  $stattransitpergi="transit";
}else{
  $stattransitpergi="langsung";
}

    $jampergitiba=DateToIndo($jampergitiba);
 ?>
 <div class="flight-list listing-style3 flight isiitempergi isiitem"  id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
  <article class="box "  id="{{$bahanid}}_div">
      <figure class="col-xs-3 col-sm-2" id="{{$bahanid}}_gbr">
          <span><img alt="" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" width="270" height="160"></span>
      </figure>
      <div class="details col-xs-9 col-sm-10">
          <div class="details-wrapper">
              <div class="first-row">
                  <div>
                      <h4 class="box-title">{{$pesawatawal}}<small>{{$stattransitpergi}}</small></h4>
                      <a class="button btn-mini stop">{{($jumtran-1)}} STOP</a>
                      <div class="amenities">
                          <i class="soap-icon-wifi circle"></i>
                          <i class="soap-icon-entertainment circle"></i>
                          <i class="soap-icon-fork circle"></i>
                          <i class="soap-icon-suitcase circle"></i>
                      </div>
                  </div>
                  <div>
                      <span class="price"><small>Harga</small><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</span>
                  </div>
              </div>
              <div class="second-row">
                  <div class="time">
                      <div class="take-off col-sm-4">
                          <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                          <div>
                              <span class="skin-color">Pergi</span><br />{{$jampergiberangkat}}
                          </div>
                      </div>
                      <div class="landing col-sm-4">
                          <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                          <div>
                              <span class="skin-color">Sampai</span><br />{{$jampergitiba}}
                          </div>
                      </div>
                      <div class="total-time col-sm-4">
                          <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                          <div>
                            <?php
                            $to_time = strtotime($jampergitiba);
                            $from_time = strtotime($jampergiberangkat);
                            $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                            ?>
                              <span class="skin-color">Lama perjalanan</span><br /><span id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</span>
                          </div>
                      </div>
                  </div>
                  <div class="action">
                      <a href="#ketpilihan" class="button btn-small full-width"
                      onclick="pilihPergi('{{$bahanid}}'
                      ,'{{$daftranpergi}}'
                      ,'{{$pesawatawal}}'
                      ,'{{$jampergiberangkat}}'
                      ,'{{$jampergitiba}}'
                      ,'{{$biayapergi}}'
                      ,'{{$ac}}'
                      ,'{{$stattransitpergi}}'
                      ,'{{$dafseat}}'
                      ,'{{$kumpulaniddep}}'
                      ,'{{angkaceil($hasildiskon)}}'
                      )">SELECT</a>
                  </div>
              </div>
          </div>
      </div>
  </article>
</div>
@endif
@endforeach
@endif
@if ($jenter == 'R')
  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              //$biayapulang=$dafreturn['Fares'][0][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba="";
              $bahanid="";

                  foreach($dafreturn['Flights'] as $trans){
                    if($aw==1){
                    $daftranpulang=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    $pesawatawal=$trans['FlightNo'];
                      $aw=0;
                    }else{
                    $daftranpulang.=", ".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    }
                    $jampulangtiba=$trans['ETA'];
                    $bahanid.=$trans['FlightNo'];
                  }
                  $biayapulang=0;
                  foreach($dafreturn['Fares'] as $dafhar){
                    if($dafhar!=null){
                      $biayapulang+=$dafhar[0]['TotalFare'];
                    }
                  }

                  $biayapulang=angkaceil($biayapulang);
                  $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapulang);

                  $jampulangberangkat=DateToIndo($dafreturn['Flights'][0]['ETD']);
                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Flights']);
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";
                  $i=0;
                  while($i<$jumtran){

                    $idret1=$dafreturn['Fares'][$i][0]['selectedIDret'];
                    //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
                    //echo "id dep : ".$iddep1;
                    //echo "<BR>";
                    if($i>0){
                      $kumpulanidret.=",";
                    }
                    $kumpulanidret.=$idret1;
                    $i+=1;
                  }


              foreach($dafreturn['Fares'][0] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }
              if(count($dafreturn['Flights'])>1){
                $stattransitpulang="transit";
              }else{
                $stattransitpulang="langsung";
              }

                  $jampulangtiba=DateToIndo($jampulangtiba);
               ?>
               <div class="flight-list listing-style3 flight isiitempulang isiitem"  id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
                <article class="box "  id="{{$bahanid}}_div">
                    <figure class="col-xs-3 col-sm-2" id="{{$bahanid}}_gbr">
                        <span><img alt="" id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" width="270" height="160"></span>
                    </figure>
                    <div class="details col-xs-9 col-sm-10">
                        <div class="details-wrapper">
                            <div class="first-row">
                                <div>
                                    <h4 class="box-title">{{$pesawatawal}}<small>{{$stattransitpulang}}</small></h4>
                                    <a class="button btn-mini stop">{{($jumtran-1)}} STOP</a>
                                    <div class="amenities">
                                        <i class="soap-icon-wifi circle"></i>
                                        <i class="soap-icon-entertainment circle"></i>
                                        <i class="soap-icon-fork circle"></i>
                                        <i class="soap-icon-suitcase circle"></i>
                                    </div>
                                </div>
                                <div>
                                    <span class="price"><small>Harga</small><strike>{{rupiahceil($biayapulang)}}</strike> {{rupiahceil($hasildiskon)}}</span>
                                </div>
                            </div>
                            <div class="second-row">
                                <div class="time">
                                    <div class="take-off col-sm-4">
                                        <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                        <div>
                                            <span class="skin-color">Pergi</span><br />{{$jampulangberangkat}}
                                        </div>
                                    </div>
                                    <div class="landing col-sm-4">
                                        <div class="icon"><i class="soap-icon-plane-right yellow-color"></i></div>
                                        <div>
                                            <span class="skin-color">Sampai</span><br />{{$jampulangtiba}}
                                        </div>
                                    </div>
                                    <div class="total-time col-sm-4">
                                        <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                        <div>
                                          <?php
                                          $to_time = strtotime($jampulangtiba);
                                          $from_time = strtotime($jampulangberangkat);
                                          $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                                          ?>
                                            <span class="skin-color">Lama perjalanan</span><br /><span id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="action">
                                    <a href="#ketpilihan" class="button btn-small full-width"
                                    onclick="pilihPulang('{{$bahanid}}'
                                    ,'{{$daftranpulang}}'
                                    ,'{{$pesawatawal}}'
                                    ,'{{$jampulangberangkat}}'
                                    ,'{{$jampulangtiba}}'
                                    ,'{{$biayapulang}}'
                                    ,'{{$ac}}'
                                    ,'{{$stattransitpulang}}'
                                    ,'{{$dafseat}}'
                                    ,'{{$kumpulanidret}}'
                                    ,'{{angkaceil($hasildiskon)}}')">SELECT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
               </div>
@endif
@endforeach
@endif
@endif
