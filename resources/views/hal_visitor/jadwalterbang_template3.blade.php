@if (is_array($dafdep))
@foreach($dafdep as $dafdeparture)
@if ($dafdeparture['Fares'][0][0]['SeatAvb']>0)
<?php
$dafseat="";
//$biayapergi=$dafdeparture['Fares'][0][0]['TotalFare'];
$daftranpergi="";
$pesawatawal="";
$aw=1;
$bahanid="";
$jampergitiba="";

    foreach($dafdeparture['Flights'] as $trans){
    //echo "TRANS ".$trans['FlightNo']. " : ".$trans['ETA']. "<BR>";
      if($aw==1){
      $daftranpergi=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      $pesawatawal=$trans['FlightNo'];
        $aw=0;
      }else{
      $daftranpergi.=",".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
      }
      $jampergitiba=$trans['ETA'];
      $bahanid.=$trans['FlightNo'];

    }

$biayapergi=0;
    foreach($dafdeparture['Fares'] as $dafhar){
      if($dafhar!=null){
        $biayapergi+=$dafhar[0]['TotalFare'];
      }
    //  $biayapergi+=$dafhar[0]['TotalFare'];
    }

    $biayapergi=angkaceil($biayapergi);
    $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapergi);


$jampergiberangkat=DateToIndo($dafdeparture['Flights'][0]['ETD']);
$stattransitpergi="";
$iddepawal=$dafdeparture['Fares'][0][0]['selectedIDdep'];
//penting $iddeptran=$dafdeparture['Fares'][1][0]['selectedIDdep'];
$jumtran=COUNT($dafdeparture['Flights']);
$i=0;
$kumpulaniddep="";
//echo "JUM ".$jumtran. "<BR>";
while($i<$jumtran){

  $iddep1=$dafdeparture['Fares'][$i][0]['selectedIDdep'];
  //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
  //echo "id dep : ".$iddep1;
  //echo "<BR>";
  if($i>0){
    $kumpulaniddep.=",";
  }
  $kumpulaniddep.=$iddep1;
  $i+=1;
}



foreach($dafdeparture['Fares'][0] as $subclass){
$banim=implode(",",$subclass);
$dafseat.="#".$banim;

}
if(count($dafdeparture['Flights'])>1){
  $stattransitpergi="transit";
}else{
  $stattransitpergi="langsung";
}

    $jampergitiba=DateToIndo($jampergitiba);
 ?>
 <li class="isiitem" id="{{$bahanid}}" isiitem="1" untuk="pergi" data-harga="{{$biayapergi}}" data-waktu="{{strtotime($jampergiberangkat)}}">
   <div class="booking-item-container" id="{{$bahanid}}_div">
       <div class="booking-item">
           <div class="row">
               <div class="col-md-2">
                   <div class="booking-item-airline-logo" id="{{$bahanid}}_gbr">
                       <img  id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" alt="Image Alternative text" title="{{maskapai($ac)}}" />
                       <p>{{maskapai($ac)}}</p>
                   </div>
               </div>
               <div class="col-md-5">

                   <div class="booking-item-flight-details">
                     <?php
                     $totime=strtotime($jampergiberangkat);
                     ?>
                       <div class="booking-item-departure"><i class="fa fa-plane"></i>
                           <h5>{{date('h:i A', $totime)}}</h5>
                           <p class="booking-item-date">{{date('D', $totime)}}, {{date('m d', $totime)}}</p>
                           <p class="booking-item-destination">{{airport_namakota($org)}}</p>
                       </div>
                       <?php
                       $totime=strtotime($jampergitiba);
                       ?>
                       <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
                         <h5>{{date('h:i A', $totime)}}</h5>
                         <p class="booking-item-date">{{date('D', $totime)}}, {{date('m d', $totime)}}</p>
                         <p class="booking-item-destination">{{airport_namakota($des)}}</p>
                       </div>
                   </div>
               </div>
               <div class="col-md-2">
                 <?php
                 $to_time = strtotime($jampergitiba);
                 $from_time = strtotime($jampergiberangkat);
                 $totalminuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                 ?>
                   <h5 id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</h5>
                   <p>{{($jumtran-1)}} stop</p>
               </div>
               <div class="col-md-3">
                 <h5><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</h5>
                 <a class="btn btn-primary" href="#ketpilihan"
                 onclick="pilihPergi('{{$bahanid}}'
                 ,'{{$daftranpergi}}'
                 ,'{{$pesawatawal}}'
                 ,'{{$jampergiberangkat}}'
                 ,'{{$jampergitiba}}'
                 ,'{{$biayapergi}}'
                 ,'{{$ac}}'
                 ,'{{$stattransitpergi}}'
                 ,'{{$dafseat}}'
                 ,'{{$kumpulaniddep}}'
                 ,'{{angkaceil($hasildiskon)}}'
                 ,'{{maskapai($ac)}}'
                 )">Select</a>
               </div>
           </div>
       </div>
       <div class="booking-item-details" id="{{$bahanid}}_detailtransit">
           <div class="row">
               <div class="col-md-8">
                   <p>Detail penerbangan</p>
                   @foreach($dafdeparture['Flights'] as $trans)
                   <?php
                   $to_time = strtotime($trans['ETA']);
                   $from_time = strtotime($trans['ETD']);
                   $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                   ?>
                   <h5 class="list-title">{{airport_namakota($trans['STD'])}}({{$trans['STD']}}) ke {{airport_namakota($trans['STA'])}}({{$trans['STA']}})</h5>
                   <ul class="list">
                       <li>{{$trans['FlightNo']}}</li>
                       <li>Berangkat {{DateToIndo($trans['ETD'])}} Sampai {{DateToIndo($trans['ETA'])}}</li>
                       <li>Durasi: {{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</li>
                   </ul>
                   @endforeach

                   <p>Total trip time: {{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</p>
               </div>
           </div>
       </div>
   </div>

 </li>

@endif
@endforeach
@endif
@if ($jenter == 'R')
  @if (is_array($dafret))
              @foreach($dafret as $dafreturn)
              @if ($dafreturn['Fares'][0][0]['SeatAvb']>0)
              <?php
              $dafseat="";
              $pesawatawal="";
              //$biayapulang=$dafreturn['Fares'][0][0]['TotalFare'];
              $daftranpulang="";
              $aw=1;
              $jampulangtiba="";
              $bahanid="";

                  foreach($dafreturn['Flights'] as $trans){
                    if($aw==1){
                    $daftranpulang=$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    $pesawatawal=$trans['FlightNo'];
                      $aw=0;
                    }else{
                    $daftranpulang.=", ".$trans['FlightNo']."(".$trans['STD']."-".$trans['STA'].")";
                    }
                    $jampulangtiba=$trans['ETA'];
                    $bahanid.=$trans['FlightNo'];
                  }
                  $biayapulang=0;
                  foreach($dafreturn['Fares'] as $dafhar){
                    if($dafhar!=null){
                      $biayapulang+=$dafhar[0]['TotalFare'];
                    }
                  }

                  $biayapulang=angkaceil($biayapulang);
                  $hasildiskon=hasildiskon($settingan_member->mmid,$ac,$biayapulang);

                  $jampulangberangkat=DateToIndo($dafreturn['Flights'][0]['ETD']);
                  $stattransitpulang="";
                  $jumtran=COUNT($dafreturn['Flights']);
                  $kumpulanidret="";
                  //echo "JUM ".$jumtran. "<BR>";
                  $i=0;
                  while($i<$jumtran){

                    $idret1=$dafreturn['Fares'][$i][0]['selectedIDret'];
                    //echo "JUM ".COUNT($dafdeparture['Fares'][$i]). "<BR>";
                    //echo "id dep : ".$iddep1;
                    //echo "<BR>";
                    if($i>0){
                      $kumpulanidret.=",";
                    }
                    $kumpulanidret.=$idret1;
                    $i+=1;
                  }


              foreach($dafreturn['Fares'][0] as $subclass){
              $banim=implode(",",$subclass);
              $dafseat.="#".$banim;

              }
              if(count($dafreturn['Flights'])>1){
                $stattransitpulang="transit";
              }else{
                $stattransitpulang="langsung";
              }

                  $jampulangtiba=DateToIndo($jampulangtiba);
               ?>
               <li class="isiitem" id="{{$bahanid}}" isiitem="1" untuk="pulang" data-harga="{{$biayapulang}}" data-waktu="{{strtotime($jampulangberangkat)}}">
                 <div class="booking-item-container" id="{{$bahanid}}_div">
                     <div class="booking-item">
                         <div class="row">
                             <div class="col-md-2">
                                 <div class="booking-item-airline-logo" id="{{$bahanid}}_gbr">
                                     <img  id="{{$bahanid}}_img" src="{{ URL::asset('img/ac/Airline-')}}{{$ac}}.png" alt="Image Alternative text" title="{{maskapai($ac)}}" />
                                     <p>{{maskapai($ac)}}</p>
                                 </div>
                             </div>
                             <div class="col-md-5">

                                 <div class="booking-item-flight-details">
                                   <?php
                                   $totime=strtotime($jampulangberangkat);
                                   ?>
                                     <div class="booking-item-departure"><i class="fa fa-plane"></i>
                                         <h5>{{date('h:i A', $totime)}}</h5>
                                         <p class="booking-item-date">{{date('D', $totime)}}, {{date('m d', $totime)}}</p>
                                         <p class="booking-item-destination">{{airport_namakota($org)}}</p>
                                     </div>
                                     <?php
                                     $totime=strtotime($jampulangtiba);
                                     ?>
                                     <div class="booking-item-arrival"><i class="fa fa-plane fa-flip-vertical"></i>
                                       <h5>{{date('h:i A', $totime)}}</h5>
                                       <p class="booking-item-date">{{date('D', $totime)}}, {{date('m d', $totime)}}</p>
                                       <p class="booking-item-destination">{{airport_namakota($des)}}</p>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-md-2">
                               <?php
                               $to_time = strtotime($jampulangtiba);
                               $from_time = strtotime($jampulangberangkat);
                               $totalminuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                               ?>
                                 <h5 id="{{$bahanid}}_totalwaktu">{{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</h5>
                                 <p>{{($jumtran-1)}} stop</p>
                             </div>
                             <div class="col-md-3">
                               <h5><strike>{{rupiahceil($biayapergi)}}</strike> {{rupiahceil($hasildiskon)}}</h5>
                               <a class="btn btn-primary" href="#ketpilihan"
                               onclick="pilihPulang('{{$bahanid}}'
                               ,'{{$daftranpulang}}'
                               ,'{{$pesawatawal}}'
                               ,'{{$jampulangberangkat}}'
                               ,'{{$jampulangtiba}}'
                               ,'{{$biayapulang}}'
                               ,'{{$ac}}'
                               ,'{{$stattransitpulang}}'
                               ,'{{$dafseat}}'
                               ,'{{$kumpulanidret}}'
                               ,'{{angkaceil($hasildiskon)}}'
                               ,'{{maskapai($ac)}}')">Select</a>
                             </div>
                         </div>
                     </div>
                     <div class="booking-item-details" id="{{$bahanid}}_detailtransit">
                         <div class="row">
                             <div class="col-md-8">
                                 <p>Detail penerbangan</p>
                                 @foreach($dafreturn['Flights'] as $trans)
                                 <?php
                                 $to_time = strtotime($trans['ETA']);
                                 $from_time = strtotime($trans['ETD']);
                                 $minuteride=round(abs($to_time - $from_time) / 60,2). " minute";
                                 ?>
                                 <h5 class="list-title">{{airport_namakota($trans['STD'])}}({{$trans['STD']}}) ke {{airport_namakota($trans['STA'])}}({{$trans['STA']}})</h5>
                                 <ul class="list">
                                     <li>{{$trans['FlightNo']}}</li>
                                     <li>Berangkat {{DateToIndo($trans['ETD'])}} Sampai {{DateToIndo($trans['ETA'])}}</li>
                                     <li>Durasi: {{convertToHoursMins($minuteride,'%02d hours, %02d minutes')}}</li>
                                 </ul>
                                 @endforeach

                                 <p>Total trip time: {{convertToHoursMins($totalminuteride,'%02d hours, %02d minutes')}}</p>
                             </div>
                         </div>
                     </div>
                 </div>

               </li>

@endif
@endforeach
@endif
@endif
