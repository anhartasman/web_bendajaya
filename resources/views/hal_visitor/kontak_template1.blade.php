@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="main-wraper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">contact info</h4>
					<h2>get in touch</h2>
				</div>
			</div>
		</div>
		<div class="contact-row">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="contact-entry">
						<img class="contact-icon" src="{{ URL::asset('asettemplate1/img/loc_icon_2_dark.png') }}" alt="">
						<div class="contact-label color-dark-2">Address:</div>
						<div class="contact-text color-dark-2">{{$infowebsite['alamat']}}</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="contact-entry">
						<img class="contact-icon" src="{{ URL::asset('asettemplate1/img/mail_icon_l_dark.png') }}" alt="">
						<div class="contact-label color-dark-2">Email us:</div>
						<a class="contact-text color-dark-2 link-dr-blue-2" href="mailto:{{$infowebsite['email']}}">{{$infowebsite['email']}}</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<div class="contact-entry">
						<img class="contact-icon" src="{{ URL::asset('asettemplate1/img/phone_icon_3_dark.png') }}" alt="">
						<div class="contact-label color-dark-2">Phone:</div>
						<a class="contact-text color-dark-2 link-dr-blue-2" href="tel:">{{$infowebsite['handphone']}}</a>
					</div>
				</div>
			</div>
		</div>
		<!--
		<div class="share style-2 clearfix">
			<ul>
		      	<li class="color-in"><a href="#"><i class="fa fa-linkedin"></i>linkedin<span class="color-in-2">19</span></a></li>
		      	<li class="color-fb"><a href="#"><i class="fa fa-facebook"></i>facebook<span class="color-fb-2">12</span></a></li>
		      	<li class="color-tw"><a href="#"><i class="fa fa-twitter"></i>twitter<span class="color-tw-2">27</span></a></li>
		      	<li class="color-gg"><a href="#"><i class="fa fa-google-plus"></i>google +<span class="color-gg-2">51</span></a></li>
		      	<li class="color-pin"><a href="#"><i class="fa fa-pinterest"></i>pinterest<span class="color-pin-2">70</span></a></li>
	      	</ul>
		</div>
	-->
	</div>
</div>


<!-- CONTACT-FORM -->
<div class="main-wraper padd-90">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h4 class="subtitle color-dr-blue-2 underline">contact form</h4>
					<h2>Punya pertanyaan?</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<div class="second-description text-center color-dark-2">
					Tulis data diri serta pertanyaan Anda di form dibawah ini, kami akan membalas secepatnya</div>
					@if($errors->has())
		               @foreach ($errors->all() as $error)
									 <div class="second-description text-center color-dark-2">
									 {{ $error }}</div>
		              @endforeach
		            @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<form class="contact-form" action="simpanbukutamu" method="post">
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="asal" value="kontak">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="input-style-1 type-2 color-2">
							  	<input type="text" name="nama" required="" placeholder="Enter your name" value="{{Request::old('nama')}}">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="input-style-1 type-2 color-2">
							  	<input type="text" name="email" required="" placeholder="Enter your email" value="{{Request::old('email')}}">
							</div>
						</div>
<div class="row">
						<div class="col-xs-12">
							<textarea class="area-style-1 color-1" name="isi" required="" placeholder="Enter your comment">{{Request::old('isi')}}</textarea>
							<div class="text-center">
								<img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">
							 	</div>
								<div  style="padding-top:25px;" class="text-center input-style-1 type-2 color-2">
									 	<input style="width:300px;" type="text" id="captcha" name="captcha" placeholder="Masukkan Captcha">
								 	</div>	</div>	</div>
	<div class="row">
						<div class="col-xs-12" style="padding-top:25px;">

					<div class="text-center">
						<button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>submit comment</span></button>
					</div>
					<input type="hidden" name="fields[code]" value="56345678safs_">

						</div>
					</div>
					</div>
				</form>
				<div class="ajax-result">
                  <div class="success"></div>
                  <div class="error"></div>
                </div>
                <div class="ajax-loader"></div>
			</div>
		</div>
	</div>
</div>

@endsection
