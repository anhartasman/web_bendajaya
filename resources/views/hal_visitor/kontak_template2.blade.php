@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Kontak</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Kontak</li>
				</ul>
		</div>
</div>

<section id="content">
		<div class="container">
				<div id="main">
						<div class="contact-address row block">
								<div class="col-md-4">
										<div class="icon-box style5">
												<i class="soap-icon-phone"></i>
												<div class="description">
														<small>Telepon kami</small>
														<h5>{{$infowebsite['handphone']}}</h5>
												</div>
										</div>
								</div>
								<div class="col-md-4">
										<div class="icon-box style5">
												<i class="soap-icon-message"></i>
												<div class="description">
														<small>Email kami</small>
														<h5>{{$infowebsite['email']}}</h5>
												</div>
										</div>
								</div>
								<div class="col-md-4">
										<div class="icon-box style5">
												<i class="soap-icon-address"></i>
												<div class="description">
														<small>Temui kami di</small>
														<h5>{{$infowebsite['alamat']}}</h5>
												</div>
										</div>
								</div>
						</div>
						<div class="travelo-box box-full">
							@if($errors->has())
				               @foreach ($errors->all() as $error)
											 <div class="second-description text-center color-dark-2">
											 {{ $error }}</div>
				              @endforeach
				            @endif
								<div class="contact-form">
										<h2>Kirim pesan</h2>
										<form class="contact-form" action="simpanbukutamu" method="post">
											  <input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="asal" value="kontak">
												<div class="row">
														<div class="col-sm-4">
																<div class="form-group">
																		<label>Nama Anda</label>
																		<input type="text" style="background: #bfb7b7;"  name="nama"  required=""  class="input-text full-width" value="{{Request::old('nama')}}">
																</div>
																<div class="form-group">
																		<label>Email Anda</label>
																		<input type="email" style="background: #bfb7b7;"  name="email" class="input-text full-width"value="{{Request::old('email')}}">
																</div>
																<div class="form-group">
																		<label>Captcha</label>
																		<img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">

																		<input type="text" style="background: #bfb7b7;"  name="captcha"  required=""  class="input-text full-width">
																</div>
														</div>
														<div class="col-sm-8">
																<div class="form-group">
																		<label>Pesan Anda</label>
																		<textarea name="isi" style="background: #bfb7b7;"  required=""  rows="8" class="input-text full-width" placeholder="write message here">{{Request::old('isi')}}</textarea>
																</div>
														</div>
												</div>

												<div class="col-sms-offset-6 col-sm-offset-6 col-md-offset-8 col-lg-offset-9">
														<button class="btn-medium full-width">KIRIM</button>
												</div>
										</form>
								</div>
						</div>
				</div>
		</div>
</section>

@endsection
