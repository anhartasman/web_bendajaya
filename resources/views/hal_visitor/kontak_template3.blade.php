@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="container">
		<h1 class="page-title">Kontak Kami</h1>
</div>

<div class="container">
		<div class="row">
				<div class="col-md-7">
					<p>Kirimkan pertanyaan, testimonial, kritik atau saran Anda</p>
					@if($errors->has())
									 @foreach ($errors->all() as $error)
									 <div class="second-description text-center color-dark-2">
									 {{ $error }}</div>
									@endforeach
								@endif
								<form class="mt30" action="simpanbukutamu" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="asal" value="kontak">
									<div class="row">
										<div class="col-md-6">
												<div class="form-group">
														<label>Nama</label>
														<input  name="nama"  value="{{Request::old('nama')}}" required=""  class="form-control" type="text" />
												</div>
										</div>
										<div class="col-md-6">
												<div class="form-group">
														<label>E-mail</label>
														<input   name="email"value="{{Request::old('email')}}" class="form-control" type="email" />
												</div>
										</div>
								</div>
								<div class="row">
								<div class="col-md-12">
								<div class="form-group">
										<label>Pesan</label>
										<textarea  name="isi" required=""  class="form-control">{{Request::old('isi')}}</textarea>
								</div></div></div>
								<div class="row">
								<div class="col-md-6">
								<div class="form-group">
										<label>Captcha</label>
										<img src="{{captcha_src()}}" width="200px" height="40px" alt="User Image">

										<input type="text" style="background: #bfb7b7;"  name="captcha"  required=""  class="input-text full-width">
								</div>
								</div></div>
								<input class="btn btn-primary" type="submit" value="Send Message" />
						</form>
				</div>
				<div class="col-md-4">
						<aside class="sidebar-right">
								<ul class="address-list list">
										<li>
												<h5>Email</h5><a href="#">{{$infowebsite['email']}}</a>
										</li>
										<li>
												<h5>Phone Number</h5><a href="#">{{$infowebsite['handphone']}}</a>
										</li>
										<li>
												<h5>Address</h5><address>{{$infowebsite['alamat']}}</address>
										</li>
								</ul>
						</aside>
				</div>
		</div>
		<div class="gap"></div>
</div>
@endsection
