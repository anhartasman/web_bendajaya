@extends('layouts.'.$namatemplate)

@section('kontenweb')
<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
		  				<li><a class="link-blue-2" href="{{ url('/') }}/">Home</a> /</li>
		  				<li><span>Kebijakan</span></li>
		  			</ul>
		  			<h2 class="color-white">Kebijakan</h2>
  				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-wraper padd-90 bg-grey-2 tabs-page">
       <div class="container">
     	  <div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="second-title">
					<h2>Kebijakan Kami</h2>
				</div>
			</div>
		  </div>
		  <div class="row">
				 <div class="col-md-12">
					<div class="simple-tab type-2 tab-wrapper">
						<div class="tab-nav-wrapper">
							<div  class="nav-tab  clearfix">

									<?php $nom=0; ?>
								@foreach($kebijakans as $kebijakan)
								<div class="nav-tab-item <?php if($nom==0){print("active");}?>">
									{{$kebijakan->judul}}
								</div><?php $nom+=1; ?>
								@endforeach

							</div>
						</div>
						<div class="tabs-content tabs-wrap-style clearfix">
							<?php $nom=0; ?>
							 @foreach($kebijakans as $kebijakan)
								<div class="tab-info <?php if($nom==0){print("active");}?>">
								   <div class="acc-body" style="color: #000;">
							        <div class="acc-body-block">
								      <div class="alert bg-1">
										<span>{{$kebijakan->judul}}</span>
										<i class="fa fa-times-circle-o"></i>
									  </div>
									<?php $nom+=1; echo(nl2br($kebijakan->isi)); ?></div>

								</div>
								</div>
								@endforeach


					  </div>
				 </div>
			  </div>
	   </div>
	 </div>
 </div>

@endsection
