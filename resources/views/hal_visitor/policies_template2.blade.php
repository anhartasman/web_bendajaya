@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
		<div class="container">
				<div class="page-title pull-left">
						<h2 class="entry-title">Kebijakan</h2>
				</div>
				<ul class="breadcrumbs pull-right">
						<li><a href="{{url('/')}}">HOME</a></li>
						<li class="active">Kebijakan</li>
				</ul>
		</div>
</div>

<section id="content">
		<div class="container">
				<div id="main">
						<div class="tab-container style1 travelo-policies">
								<ul class="tabs full-width">

										<?php $nom=0; ?>
										@foreach($kebijakans as $kebijakan)
										<li class="<?php if($nom==0){print("active");}?>"><a data-toggle="tab" href="#{{$kebijakan->id}}-services">{{$kebijakan->judul}}</a></li>
										<?php $nom+=1; ?>
										@endforeach
							 	</ul>
								<div class="tab-content">
									<?php $nom=0; ?>
									 @foreach($kebijakans as $kebijakan)
										<div id="{{$kebijakan->id}}-services" class="tab-pane fade in <?php if($nom==0){print("active");}?>">


												<div class="policy">

														<h2>{{$kebijakan->judul}}</h2>
														<?php $nom+=1; echo(nl2br($kebijakan->isi)); ?>

												</div>
												<hr>


										</div>
										@endforeach

								</div>
						</div>
				</div>
		</div>
</section>

@endsection
