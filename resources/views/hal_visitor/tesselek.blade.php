<!DOCTYPE html>
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->
<head>
		<meta charset="utf-8">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no" />
 	  <link rel="shortcut icon" href="favicon.ico"/>
		<link href="{{ URL::asset('asettemplate1/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{ URL::asset('asettemplate1/css/jquery-ui.structure.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{ URL::asset('asettemplate1/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="../../../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link href="{{ URL::asset('asettemplate1/css/style.css')}}" rel="stylesheet" type="text/css"/>

    <script type="application/javascript" src="{{ URL::asset('asettemplate1/js/jquery-2.2.2.min.js')}}"></script>
    <script type="application/javascript" src="{{ URL::asset('asettemplate1/js/MSelectDBox.js')}}"></script>
    <script type="application/javascript">
      $(document).ready(function(){

        var eventLog = function(ctx, e){
          /**
          var html = [
            "name: " + ctx.get("name"),
            "event.type: " + e.type
          ];
          $(".events-plate").html(html.join("<br />"));
          **/
        };

        Math.rand = function(min,max){
          return Math.floor(Math.random() * (max - min + 1)) + min;
        };

        $("#msdb-a").mSelectDBox({
          "list": (function(){
            var arr = [];
            var counter = 0;
            for(var c=0; c<100; c++){
              arr.push(counter += Math.round(Math.random() * 99) * 10);
            }
            return arr;
          })(),
          "multiple": false,
          "autoComplete": true,
          "input:empty": eventLog,
          "onselect": eventLog,
          "name": "a"
        });



      });
    </script>
    <title>Let's Travel</title>

	</head>
<body data-color="theme-1">

@include('hal_visitor.inc_gaya')

<div class="loading red">
	<div class="loading-center">
		<div class="loading-center-absolute">
			<div class="object object_four"></div>
			<div class="object object_three"></div>
			<div class="object object_two"></div>
			<div class="object object_one"></div>
		</div>
	</div>
</div>

       			@include('hal_visitor.inc_header')
<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="#">home</a> /</li>
				<li><a class="link-blue-2" href="#">tours</a> /</li>
				<li><span class="color-red-3">list tours</span></li>
			</ul>
			<h2 class="color-white">all flights for you</h2>
			<h4 class="color-white">We found: <span class="color-red-3">640</span> flights</h4>
		</div>
	</div>
</div>

  <div class="main-wraper padd-90 tabs-page">
      <div class="container">
       <div class="row">
     <div class="col-xs-12 col-sm-8 col-sm-offset-2">
       <div class="second-title">
         <h2>Cari Jadwal </h2>
       </div>
     </div>
     </div>
    </div>
      <div class="full-width">
      <div class="bg bg-bg-chrome act" style="background-image:url({{ URL::asset('asettemplate1/img/home_1/main_slide_1.jpg')}})">
      </div>
          <div class="container-fluid">
           <div class="row">
            <div class="col-md-12">
             <div class="baner-tabs">

                <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
                 <div class="tab-pane active in" id="one">
                   <div class="container">
                     <div class="row">
                       <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                         <div class="tabs-block">
                         <h5>Your Destinationss</h5>
                           <div class="input-style">
                            <img src="{{ URL::asset('asettemplate1/img/loc_icon_small.png')}}" alt="">

                              <input id="msdb-a" class="std-input" type="text">
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                         <div class="tabs-block">
                         <h5>Check In</h5>
                           <div class="input-style">
                            <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                              <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                         <div class="tabs-block">
                         <h5>Check Out</h5>
                           <div class="input-style">
                            <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                              <input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                         <div class="tabs-block">
                         <h5>Adults</h5>
                            <div class="drop-wrap">
                             <div class="drop">
                              <b>01 adult</b>
                               <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                 <span>
                                   <a href="#">01 adult</a>
                                   <a href="#">02 adult</a>
                                   <a href="#">03 adult</a>
                                   <a href="#">04 adult</a>
                                 </span>
                              </div>
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                         <div class="tabs-block">
                         <h5>Kids</h5>
                            <div class="drop-wrap">
                             <div class="drop">
                              <b>0 kids</b>
                               <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                 <span>
                                   <a href="#">0 kids</a>
                                   <a href="#">01 kids</a>
                                   <a href="#">02 kids</a>
                                   <a href="#">03 kids</a>
                                   <a href="#">04 kids</a>
                                 </span>
                              </div>
                           </div>
                         </div>
                       </div>
                       <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                         <div class="tabs-block">
                         <h5>Infan</h5>
                            <div class="drop-wrap">
                             <div class="drop">
                              <b>0 kids</b>
                               <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                 <span>
                                   <a href="#">0 kids</a>
                                   <a href="#">01 kids</a>
                                   <a href="#">02 kids</a>
                                   <a href="#">03 kids</a>
                                   <a href="#">04 kids</a>
                                 </span>
                              </div>
                           </div>
                         </div>
                       </div>

                       <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                         <a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>

                       </div>
                     </div>
                   </div>
                 </div>

               </div>
             </div>
           </div>
          </div>
         </div>
        </div>
      </div>
    </div>
  	<div class="list-wrapper bg-grey-2">
  		<div class="container">
  			<div class="row">
  				<div class="col-xs-12 col-sm-4 col-md-3">
  					<div class="sidebar bg-white clearfix">
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">search</h4>
							<div class="search-inputs">
								<div class="form-block clearfix">
									<div class="input-style-1 b-50 color-3">
										<img src="{{ URL::asset('asettemplate1/img/loc_icon_small_grey.png')}}" alt="">
										<input type="text" placeholder="Kota Asal">
									</div>
								</div>
  								<div class="form-block clearfix">
  									<div class="input-style-1 b-50 color-3">
  										<img src="{{ URL::asset('asettemplate1/img/loc_icon_small_grey.png')}}" alt="">
  										<input type="text" placeholder="Kota Tujuan">
  									</div>
  								</div>
								<div class="form-block clearfix">
									<div class="input-style-1 b-50 color-3">
										<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
										<input type="text" placeholder="Check In" class="datepicker">
									</div>
								</div>
								<div class="form-block clearfix">
									<div class="input-style-1 b-50 color-3">
										<img src="{{ URL::asset('asettemplate1/img/calendar_icon_grey.png')}}" alt="">
										<input type="text" placeholder="Check Out" class="datepicker">
									</div>
								</div>
							</div>
							<input type="submit" class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
						</div>
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">categories</h4>
							<ul class="sidebar-category color-4">
								<li class="active">
									<a class="cat-drop" href="#">tours <span class="fr">(68)</span></a>
									<ul>
										<li><a href="#">sea tours (785)</a></li>
										<li><a href="#">food tours (85)</a></li>
										<li><a href="#">romantic tours (125)</a></li>
										<li><a href="#">honeymoon tours (70)</a></li>
										<li><a href="#">mountain tours (159)</a></li>
									</ul>
								</li>
								<li>
									<a class="cat-drop" href="#">hotels <span class="fr">(125)</span></a>
									<ul>
										<li><a href="#">sea tours (785)</a></li>
										<li><a href="#">food tours (85)</a></li>
										<li><a href="#">romantic tours (125)</a></li>
										<li><a href="#">honeymoon tours (70)</a></li>
										<li><a href="#">mountain tours (159)</a></li>
									</ul>
								</li>
								<li>
									<a class="cat-drop" href="#">cruises <span class="fr">(75)</span></a>
									<ul>
										<li><a href="#">sea tours (785)</a></li>
										<li><a href="#">food tours (85)</a></li>
										<li><a href="#">romantic tours (125)</a></li>
										<li><a href="#">honeymoon tours (70)</a></li>
										<li><a href="#">mountain tours (159)</a></li>
									</ul>
								</li>
								<li>
									<a class="cat-drop" href="#">flights  <span class="fr">(93)</span></a>
									<ul>
										<li><a href="#">sea tours (785)</a></li>
										<li><a href="#">food tours (85)</a></li>
										<li><a href="#">romantic tours (125)</a></li>
										<li><a href="#">honeymoon tours (70)</a></li>
										<li><a href="#">mountain tours (159)</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">price range</h4>
							<div class="slider-range color-4 clearfix" data-counter="$" data-position="start" data-from="0" data-to="1500" data-min="0" data-max="2000">
								<div class="range"></div>
								<input type="text" class="amount-start" readonly value="$0">
								<input type="text" class="amount-end" readonly value="$1500">
							</div>
							<input type="submit" class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
						</div>
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">flight times</h4>
							<div class="slider-range color-4 clearfix" data-counter=":00" data-position="end" data-from="0" data-to="1500" data-min="0" data-max="24">
								<div class="range"></div>
								<input type="text" class="amount-start" readonly value="$0">
								<input type="text" class="amount-end" readonly value="$1500">
							</div>
							<input type="submit" class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
						</div>
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">airlines</h4>
							<div class="sidebar-rating">
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-1" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-1" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">Air tahiti nui</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-2" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-2" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">Air France</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-3" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-3" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">US Airways</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-4" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-4" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">Alitalia</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-5" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-5" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">Delta Airlines</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-6" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-6" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">United Airlines</span>
									</label>
								</div>
								<div class="input-entry color-7">
									<input class="checkbox-form" id="text-7" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="text-7" >
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">Major Airline</span>
									</label>
								</div>
							</div>
						</div>
						<div class="sidebar-block">
							<h4 class="sidebar-title color-dark-2">Review Score</h4>
							<div class="sidebar-score">
								<div class="input-entry type-2 color-8">
									<input class="checkbox-form" id="score-5" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="score-5" >
										<span class="checkbox-text">
											5
											<span class="rate">
												<span class="fa fa-star color-yellow"></span>
											</span>
										</span>
										<span class="sp-check"><i class="fa fa-check"></i></span>
									</label>
								</div>
								<div class="input-entry type-2 color-8">
									<input class="checkbox-form" id="score-4" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="score-4" >
										<span class="checkbox-text">
											4
											<span class="rate">
												<span class="fa fa-star color-yellow"></span>
											</span>
										</span>
										<span class="sp-check"><i class="fa fa-check"></i></span>
									</label>
								</div>
								<div class="input-entry type-2 color-8">
									<input class="checkbox-form" id="score-3" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="score-3" >
										<span class="checkbox-text">
											3
											<span class="rate">
												<span class="fa fa-star color-yellow"></span>
											</span>
										</span>
										<span class="sp-check"><i class="fa fa-check"></i></span>
									</label>
								</div>
								<div class="input-entry type-2 color-8">
									<input class="checkbox-form" id="score-2" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="score-2" >
										<span class="checkbox-text">
											2
											<span class="rate">
												<span class="fa fa-star color-yellow"></span>
											</span>
										</span>
										<span class="sp-check"><i class="fa fa-check"></i></span>
									</label>
								</div>
								<div class="input-entry type-2 color-8">
									<input class="checkbox-form" id="score-1" type="checkbox" name="checkbox" value="climat control">
									<label class="clearfix" for="score-1" >
										<span class="checkbox-text">
											1
											<span class="rate">
												<span class="fa fa-star color-yellow"></span>
											</span>
										</span>
										<span class="sp-check"><i class="fa fa-check"></i></span>
									</label>
								</div>
							</div>
						</div>
  					</div>
  				</div>
  				<div class="col-xs-12 col-sm-8 col-md-9">
					<div class="list-header clearfix">
						<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
						  <div class="drop">
							 <b>Sort by price</b>
								<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
								<span>
								    <a href="#">ASC</a>
									<a href="#">DESC</a>
								</span>
						   </div>
						</div>
						<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
						  <div class="drop">
							 <b>Sort by ranking</b>
								<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
								<span>
								    <a href="#">ASC</a>
									<a href="#">DESC</a>
								</span>
						   </div>
						</div>
						<div class="list-view-change">
							<div class="change-grid color-4 fr"><i class="fa fa-th"></i></div>
							<div class="change-list color-4 fr active"><i class="fa fa-bars"></i></div>
							<div class="change-to-label fr color-grey-8">View:</div>
						</div>
					</div>
  					<div class="list-content clearfix">
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_2.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to london</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Flights to monaco</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_3.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_3.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to london</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_4.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Flights to monaco</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_2.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_4.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to london</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Flights to monaco</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_3.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_2.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to london</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Flights to monaco</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-grey">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>
  					</div>

  					<div class="c_pagination clearfix padd-120">
						<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o fl">prev page</a>
						<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o fr">next page</a>
						<ul class="cp_content color-4">
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">...</a></li>
							<li><a href="#">10</a></li>
						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
 	@include('hal_visitor.inc_footer') 
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>


</body>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:59 GMT -->
</html>
