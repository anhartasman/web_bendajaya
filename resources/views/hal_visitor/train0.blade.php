<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<form class="contact-form"id="formgo" action="{{url('/')}}/train/train_isiform" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_TrainNoDep" name="formgo_TrainNoDep">
<input type="hidden" id="formgo_TrainNoRet" name="formgo_TrainNoRet">
<input type="hidden" id="formgo_keretaDep" name="formgo_keretaDep">
<input type="hidden" id="formgo_keretaRet" name="formgo_keretaRet">
<input type="hidden" id="formgo_hargaDep" name="formgo_hargaDep">
<input type="hidden" id="formgo_hargaRet" name="formgo_hargaRet">
<input type="hidden" id="formgo_trip" name="formgo_trip" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>
<!--
<div class="inner-banner style-6">
<img class="center-image" src="{{ URL::asset('asettemplate1/img/tour_list/inner_banner_3.jpg')}}" alt="">
<div class="vertical-align">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
            <ul class="banner-breadcrumb color-white clearfix">
          	<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
				<li><span class="color-red-3">penerbangan</span></li>
      </ul>
      </div>
    </div>
</div>
</div>
</div>
-->

  <div class="main-wraper padd-90 tabs-page">
    <!-- BAGIAN PENCARIAN -->

      <div class="full-width">
      <div class="bg bg-bg-chrome act" style="background-image:url({{ URL::asset('img/inner_banner_train.jpg')}})">
      </div>
          <div class="container-fluid">

           <div class="row">
            <div class="col-md-12">
             <div class="baner-tabs">
                <div class="tab-content tpl-tabs-cont section-text t-con-style-1">
                 <div class="tab-pane active in" id="one">
                   <div class="container">

                     				<div class="row">
                     					<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     						<div class="tabs-block ">
                     						<h5>Asal</h5>
                     							<div class="input-style">
                     							<select class="input-style selektwo" name="asal" id="pilasal">
                     								<option value=""></option>
                                     @foreach($dafstat as $area)
                     								<option value="{{ $area->st_name }}-{{ $area->st_code }}" >{{ $area->st_name }} - {{ $area->st_code }}</option>
                     								@endforeach
                     							</select> </div>
                     						</div>
                     					</div>
                     						<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     							<div class="tabs-block">
                     							<h5>Tujuan</h5>
                     								<div class="input-style">
                     								<select class="input-style selektwo" name="tujuan" id="piltujuan">
                     									 <option value=""></option>
                                       @foreach($dafstat as $area)
                     									<option value="{{ $area->st_name }}-{{ $area->st_code }}">{{ $area->st_name }} - {{ $area->st_code }}</option>
                     									@endforeach
                     								</select> </div>
                     							</div>
                     						</div>
                     							<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                     								<div class="tabs-block">
                     								<h5>Rencana</h5>
                     									<div class="input-style">
                     									<select class="mainselection" id="pilrencana" name="pilrencana">
                     										<option value="O" selected="selected">Sekali Jalan</option>
                     										<option value="R">Pulang Pergi</option>

                     									</select> </div>
                     								</div>
                     							</div>
                     					<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                     						<div class="tabs-block">
                     						<h5>Tanggal Berangkat</h5>
                     							<div class="input-style">
                     							 <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                     								 <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                     							</div>
                     						</div>
                     					</div>
                     					<div id="tny_ret" class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                     						<div class="tabs-block">
                     						<h5>Tanggal Pulang</h5>
                     							<div class="input-style">
                     							 <img src="{{ URL::asset('asettemplate1/img/calendar_icon.png')}}" alt="">
                     								 <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php echo date("Y-m-d");?>" >
                     							</div>
                     						</div>
                     					</div>

                     				</div>
                     					<div class="row">

                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Adults</h5>
                     								 <select class="mainselection" id="adt" name="adt">

                     									 <option value="1">1</option>
                     									 <option value="2">2</option>
                     									 <option value="3">3</option>
                     									 <option value="4">4</option>
                     									 <option value="5">5</option>
                     									 <option value="6">6</option>
                     									 <option value="7">7</option>

                     								 </select>
                     							</div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Kids</h5>
                     										<select class="mainselection"id="chd" name="chd">

                     											<option value="0">0</option>
                     											<option value="1">1</option>
                     											<option value="2">2</option>
                     											<option value="3">3</option>
                     											<option value="4">4</option>
                     											<option value="5">5</option>
                     											<option value="6">6</option>

                     										</select>
                     							</div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                     							<div class="tabs-block">
                     							<h5>Infan</h5>
                     										<select class="mainselection"id="inf" name="inf">

                     											<option value="0">0</option>
                     											<option value="1">1</option>
                     											<option value="2">2</option>
                     											<option value="3">3</option>
                     											<option value="4">4</option>
                     											<option value="5">5</option>
                     											<option value="6">6</option>

                     										</select></div>
                     						</div>
                     						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4" >
                     							<div >
                                 <input type="submit" id="tomcari" onclick="cari()" class="c-button b-30 bg-red-3 hv-red-3-o" value="search">
                                   </div>
                     						</div>


                     	</div>

                   </div>
                 </div>

               </div>
             </div>
           </div>
          </div>
         </div>
        </div>
      </div>
    </div>
  	<div class="list-wrapper bg-grey-2">
  		<div class="container">
        <!-- sisi atas pilihan-->
  			<div class="row">
          	<div class="col-xs-12 col-sm-8 col-md-12">
                  <div class="list-content clearfix" id="dafloading" >

                    <div class="list-item-entry" id="loading_qg">
                        <div class="hotel-item style-10 bg-white">
                          <div class="table-view">
                              <div class="radius-top cell-view">
                                <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KAI.jpg')}}" alt="">
                              </div>
                              <div class="title hotel-middle cell-view">
                                <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                                <h6 class="color-grey-3 list-hidden">one way flights</h6>
                               </div>
                              <div class="title hotel-right clearfix cell-view grid-hidden">
                            </div>
                            </div>
                        </div>
                    </div>




                  </div>
            </div>
        </div>
<!-- baris hasil pilihan -->
  			<div class="row">

  				<div class="col-xs-12 col-sm-8 col-md-12">

              <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Kereta yang dipilih : Padang (PDG) - Jakarta (CGK) | Dewasa: 1 Anak: 0 Bayi: 0</h4>

              </div>
  					<div class="list-content clearfix" >
  						<div class="list-item-entry" id="pilihanpergi">
  					        <div class="hotel-item style-10 bg-white">
  					        	<div class="table-view">
						          	<div class="radius-top cell-view">
                          <h5 id="pilihanpergi_namakereta"style="padding-left:20px;">  DAF TRAN  </h5>
                          <h6 class="color-grey-3 list-hidden">one wayaa flights</h6>
                          <h4 id="hsubclasspergi"><b>
                            SubClass
                            <select   name="pilsubclasspergi" id="pilsubclasspergi">
                                <option value="0" >sad </option>
                                  <option value="0" >sad1 </option>

                            </select>



                          </b></h4>
                            <h4 style="padding-left:20px;"><b id="pilihanpergi_harga">Cheap Flights to Paris</b></h4>
                          	</div>
						          	<div class="title hotel-middle cell-view">
							              <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">Pergi</div>
						          	    			<div class="fi_text color-black" id="pilihanpergi_jampergi">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">Tiba</div>
						          	    			<div class="fi_text color-black" id="pilihanpergi_jamtiba">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>

							           </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	      <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>-->


                        </div>
					            </div>
					        </div>
  						</div>

    					<div class="list-item-entry" id="pilihanpulang" style="padding-bottom:0px;">
  					        <div class="hotel-item style-10 bg-white">
  					        	<div class="table-view">
  						          	<div class="radius-top cell-view">
                            <h5 id="pilihanpulang_namakereta"style="padding-left:20px;">  DAF TRAN  </h5>
    							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
                              <h4 id="hsubclasspulang"><b>
                                SubClass
                                <select   name="pilsubclasspulang" id="pilsubclasspulang">
                                    <option value="0" >sad </option>
                                      <option value="0" >sad1 </option>

                                </select>
                              </b></h4>
                                <h4 style="padding-left:20px;"><b id="pilihanpulang_harga">Cheap Flights to Paris</b></h4>

                          </div>
  						          	<div class="title hotel-middle cell-view">
  							          <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
  						          	    <div class="fi_block grid-hidden row row10">
  						          	    	<div class="flight-icon col-xs-6 col10">
  						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
  						          	    		<div class="fi_content">
  						          	    			<div class="fi_title color-dark-2">Pergi</div>
  						          	    			<div class="fi_text color-black" id="pilihanpulang_jampergi">wed nov 13, 2013 7:50 am</div>
  						          	    		</div>
  						          	    	</div>
  						          	    	<div class="flight-icon col-xs-6 col10">
  						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
  						          	    		<div class="fi_content">
  						          	    			<div class="fi_title color-dark-2">Tiba</div>
  						          	    			<div class="fi_text color-black" id="pilihanpulang_jamtiba">wed nov 13, 2013 7:50 am</div>
  						          	    		</div>
  						          	    	</div>
  						          	    </div>
  							           </div>
  						            <div class="title hotel-right clearfix cell-view grid-hidden">
  					          	      <!----     <div class="hotel-right-text color-dark-2">one way flights</div>
  					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
                                -->

                      <button type="button" class="btn btn-block btn-primary" id="tomgoret">Lanjutkan Transaksi</button>
                          </div>
  					            </div>
  					        </div>
    						</div>



  					</div>


  				</div>
        </div>

<!-- baris sorting -->
                <div class="row" id="barissorting">
                  				<div class="col-xs-12 col-sm-8 col-md-12">
                  <div class="list-header clearfix">
        						<div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
        						  <div style="color:BLACK;">
        							 Harga
        						   </div>
        						</div>
          						<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
          						  <div class="drop"style="color:BLACK;">
          							 <b>Urut</b>
          								<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
          								<span>
          								  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">ASC</a>
          									<a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">DESC</a>
          								</span>
          						   </div>
          						</div>
                      <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
          						  <div style="color:BLACK;">
          							 Jam
          						   </div>
          						</div>
        						<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
        						  <div class="drop"style="color:BLACK;">
        							 <b>Urut</b>
        								<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
        								<span>
        								    <a href="#" onclick="sortAsc('data-waktu')"style="color:BLACK;">ASC</a>
        									<a href="#" onclick="sortDesc('data-waktu')"style="color:BLACK;">DESC</a>
        								</span>
        						   </div>
        						</div>
        					</div>
                </div>
                </div>
    <!-- baris daftar item -->
  			<div class="row">
					<!-- sisi kiri -->
  				<div class="col-xs-12 col-sm-8 col-md-12" id="sisikiri">

              <div class="list-header clearfix" style="background-color: #ff6600;">

                  <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkiri" >Pilih kereta pergi</h4>

              </div>
              <div class="divTable">
              <div class="divTableBody" id="dafpergi">
              <div class="divTableRow">
              <div class="divTableCell">HAHAHA&nbsp;</div>
              <div class="divTableCell">&nbsp;</div>
              <div class="divTableCell">&nbsp;</div>
              <div class="divTableCell">&nbsp;</div>
              <div class="divTableCell">&nbsp;</div>
              <div class="divTableCell"><a href="#ketpilihan"class="c-button b-40 bg-red-3 hv-red-3-o"
               >pilih</a></div>
              </div>


              </div>
              </div>
  					<div class="list-content clearfix">
  						<div class="list-item-entry">
					        <div class="hotel-item style-10 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
						          	</div>
						          	<div class="title hotel-middle cell-view">
							            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
							            <h6 class="color-grey-3 list-hidden">one way flights</h6>
						          	    <h4><b>Cheap Flights to Paris</b></h4>
						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
						          	    <div class="fi_block grid-hidden row row10">
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-black">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    	<div class="flight-icon col-xs-6 col10">
						          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
						          	    		<div class="fi_content">
						          	    			<div class="fi_title color-dark-2">take off</div>
						          	    			<div class="fi_text color-black">wed nov 13, 2013 7:50 am</div>
						          	    		</div>
						          	    	</div>
						          	    </div>
							            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o" id="tomteskir">book now</a>
							            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						            </div>
						            <div class="title hotel-right clearfix cell-view grid-hidden">
					          	        <div class="hotel-right-text color-dark-2">one way flights</div>
					          	        <div class="hotel-right-text color-dark-2">1 stop</div>
						            </div>
					            </div>
					        </div>
  						</div>

  					</div>

  				</div>
					<!-- sisi kanan -->
	  			<div class="col-xs-12 col-sm-8 col-md-12" id="sisikanan">

              <div class="list-header clearfix" style="background-color: #ff6600;">

                   <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Pilih kereta pulang</h4>

              </div>
	  					<div class="list-content clearfix" id="dafpulang">
	  						<div class="list-item-entry">
						        <div class="hotel-item style-10 bg-white">
						        	<div class="table-view">
							          	<div class="radius-top cell-view">
							          	 	<img src="img/tour_list/flight_grid_1.jpg" alt="">
							          	</div>
							          	<div class="title hotel-middle cell-view">
								            <h5>from <strong class="color-red-3">$860</strong> / person</h5>
								            <h6 class="color-grey-3 list-hidden">one way flights</h6>
							          	    <h4><b>Cheap Flights to Paris</b></h4>
							          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
							          	    <div class="fi_block grid-hidden row row10">
							          	    	<div class="flight-icon col-xs-6 col10">
							          	    		<img class="fi_icon" src="img/tour_list/flight_icon_2.png" alt="">
							          	    		<div class="fi_content">
							          	    			<div class="fi_title color-dark-2">take off</div>
							          	    			<div class="fi_text color-black">wed nov 13, 2013 7:50 am</div>
							          	    		</div>
							          	    	</div>
							          	    	<div class="flight-icon col-xs-6 col10">
							          	    		<img class="fi_icon" src="img/tour_list/flight_icon_1.png" alt="">
							          	    		<div class="fi_content">
							          	    			<div class="fi_title color-dark-2">take off</div>
							          	    			<div class="fi_text color-black">wed nov 13, 2013 7:50 am</div>
							          	    		</div>
							          	    	</div>
							          	    </div>
								            <a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
								            <a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
							            </div>
							            <div class="title hotel-right clearfix cell-view grid-hidden">
						          	        <div class="hotel-right-text color-dark-2">one way flights</div>
						          	        <div class="hotel-right-text color-dark-2">1 stop</div>
							            </div>
						            </div>
						        </div>
	  						</div>

	  					</div>

	  				</div>
  			</div>

  			<div class="row" id="divpilihnextorulang">
					<!-- sisi kiritombol -->
  				<div class="col-xs-12 col-sm-8 col-md-6" id="sisikiritombol">

              <div class="list-header clearfix" style="background-color: #ff6600;">

                 <button type="button" class="btn btn-block btn-primary" id="tompilulang">Ubah Pilihan</button>

              </div>

  				</div>
  					<!-- sisi kanantombol -->
    				<div class="col-xs-12 col-sm-8 col-md-6" id="sisikanantombol">

                <div class="list-header clearfix" style="background-color: #ff6600;">

                   <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>

                </div>

    				</div>
          </div>
  		</div>
  	</div>


				@section('akhirbody')
		<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
	  <script type="text/javascript">

    $("#sisikanan").hide();
    $("#barissorting").hide();
    $("#sisikiri").hide();
    <!--
    $('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy'}).val();
   -->

    //variabel

		var hargapulang="0";
		var hargapergi="0";
    var nilpil=0;
    var idpilper="";
    var idpilret="";
		var statasal="BD";
		var kotasal="Bandung";
		var labelasal="";
		var kottujuan="Gambir";
		var stattujuan="GMR";
		var labeltujuan="";
		var pilrencana="O";
		var pilrencana2="O";
		var tglber_d="";
		var tglber_m="";
		var tglber_y="";

		var tglpul_d="";
		var tglpul_m="";
		var tglpul_y="";

		var tglberangkat="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_dep').val(tglberangkat);
    $('#formgo_tgl_deppilihan').val(tglberangkat);
		var tglpulang="<?php echo date("Y-m-d");?>";
    $('#formgo_tgl_ret').val(tglpulang);
    $('#formgo_tgl_retpilihan').val(tglpulang);

        @if($otomatiscari==1)
        $('#formgo_tgl_deppilihan').val("{{$tglberangkat}}");
        $('#formgo_tgl_retpilihan').val("{{$tglpulang}}");
        $('#formgo_trip').val("{{$trip}}");
        $('#formgo_org').val("{{$org}}");
        $('#formgo_des').val("{{$des}}");
        $('#formgo_kotorg').val("{{$kotasal}}");
        $('#formgo_kotdes').val("{{$kottujuan}}");

        @endif
		var jumadt="1";
		var jumchd="0";
		var juminf="0";



		var urljadwal="{{ url('/') }}/jadwalKereta/";
		var urljadwalb=urljadwal;
    var dafIDPergi = [];
    var dafBiayaPergi = [];
    var dafHTMLPergi = [];
    var dafIDPulang = [];
    var dafBiayaPulang = [];
    var dafHTMLPulang = [];


		$('#tomgoret').hide();
		$('#divpilihnextorulang').hide();
		$('#hsubclasspergi').hide();
		$('#hsubclasspulang').hide();
		$('#tomulangipencarian').hide('slow');


    $( "#tompilulang" ).click(function() {
      nilpil=0;
      $('#divpilihnextorulang').hide();
      $('#sisikiri').show('slow');
      $('#barissorting').show('slow');
    });
    $( "#tomgoone" ).click(function() {
    $( "#formgo" ).submit();
    });
    $( "#tomgoret" ).click(function() {
    $( "#formgo" ).submit();
    });
    $( "#tomteskir" ).click(function() {
      $( "#formgo" ).submit();
    });

		$('#tomulangipencarian').on('click', function() {


		$('#formatas').show('slow');

		//$('#labelpilihanpergi').hide('slow');
		$('#tomulangipencarian').hide('slow');

		});
		$('#adt').on('change', function() {
		jumadt=this.value;
    $('#formgo_adt').val(jumadt);

		});
		$('#chd').on('change', function() {
		jumchd=this.value;
    $('#formgo_chd').val(jumchd);

		});
		$('#inf').on('change', function() {
		juminf=this.value;
    $('#formgo_inf').val(juminf);

		});


		$('#labelpilihanpergi').hide();
		$('#pilihanpergi').hide();
		$('#pilihanpulang').hide();
		  $('.selektwo').select2();
		  $('#tny_ret').hide();
			                $('#loading_qg').hide();


		$('#tglberangkat').datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$('#tglpulang').datepicker({ dateFormat: 'dd-mm-yy' }).val();

		$('#tglberangkat').on('change', function() {

		var values=this.value.split('-');
		//alert(this.value);
		tglber_d=values[0];
		tglber_m=values[1];
		tglber_y=values[2];

		tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

    $('#formgo_tgl_deppilihan').val(tglberangkat);

		});

		$('#tglpulang').on('change', function() {

		var values=this.value.split('-');
		//alert(this.value);
		tglpul_d=values[0];
		tglpul_m=values[1];
		tglpul_y=values[2];

		tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
    $('#formgo_tgl_retpilihan').val(tglpulang);

		});

		$('#pilasal').on('change', function() {
		  var values=this.value.split('-');
		  var kotas=values[1];
		  kotasal=values[0];
		  //alert(kotas);
		  statasal=kotas;
		  labelasal=this.value;
      $('#formgo_labelorg').val(labelasal);
      $('#formgo_org').val(statasal);
      $('#formgo_kotorg').val(kotasal);

		});
		$('#piltujuan').on('change', function() {
		  var values=this.value.split('-');
		  var kottuj=values[1];
		  kottujuan=values[0];
		  //alert(kottuj);
		  stattujuan=kottuj;
		  labeltujuan=this.value;
      $('#formgo_labeldes').val(labeltujuan);
      $('#formgo_des').val(kottuj);
      $('#formgo_kotdes').val(kottujuan);

		});
		$('#pilrencana').on('change', function() {
		  //alert( this.value );
		  pilrencana=this.value;
      $('#formgo_trip').val(pilrencana);
		  var tny_ret = document.getElementById("tny_ret").value;

		if(this.value=="O"){
		    $('#tny_ret').hide();
     }else{
		    $('#tny_ret').show();
     }


		});

		function setkolom(){
		labelutama();
		//  $('#labelkolomkiri').html(kotasal+" ("+statasal+") - "+kottujuan+" ("+stattujuan+")");
		if(pilrencana=="R"){
		//  $('#labelkolomkanan').html(kottujuan+" ("+stattujuan+") - "+kotasal+" ("+statasal+")");
		}
		}
		function labelutama(){
		  $('#labelkolomutama').html('Kereta yang dipilih : '+kotasal+" ("+statasal+") - "+kottujuan+" ("+stattujuan+") | Dewasa: "+jumadt+" Anak: "+jumchd+" Bayi: "+juminf);

		}

function convertToRupiah(angka){
var rupiah = '';
var angkarev = angka.toString().split('').reverse().join('');
for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
return rupiah.split('',rupiah.length-1).reverse().join('');
}

$('#pilsubclasspergi').on('change', function() {
  hargapergi=this.value.split(",")[0];
  var totaladt=hargapergi*jumadt;
  var totalchd=hargapergi*jumchd;
  var totalinf=hargapergi*juminf;
  $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
  totalhargapergi=totaladt+totalchd+totalinf;
  $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(totalhargapergi));


});
$('#pilsubclasspulang').on('change', function() {
  hargapulang=this.value.split(",")[0];
  var totaladt=hargapulang*jumadt;
  var totalchd=hargapulang*jumchd;
  var totalinf=hargapulang*juminf;
  $('#formgo_selectedIDret').val(this.value.split(",")[1]);
  totalhargapulang=totaladt+totalchd+totalinf;
  $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(totalhargapulang));


});

		function pilihPergi(bahanid
		  ,jampergiberangkat
		  ,jampergitiba
		  ,harga
		  ,namakereta
		  ,kumpulaniddep
		  ){
        var totaladt=harga*jumadt;
        var totalchd=harga*jumchd;
        var totalinf=harga*juminf;
        harga=totaladt+totalchd+totalinf;

        $('#formgo_selectedIDdep').val(kumpulaniddep);
         $('#pilsubclasspergi').find('option').remove();

    $('#formgo_TrainNoDep').val(bahanid);
    $('#formgo_keretaDep').val(namakereta);
    $('#formgo_hargaDep').val(harga);
    $('#formgo_tgl_dep').val(jampergiberangkat);
    $('#formgo_tgl_dep_tiba').val(jampergitiba);
    /**
   $("#"+idpilper).removeClass("kotakpilihb cell-view");
    $("#"+idpilper).addClass("kotakpilih cell-view");

		$("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
    $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
idpilper=bahanid+"_gbr";
**/

		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		$('#pilihanpergi').show('slow');
		//  alert(jampergitiba);
		        $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
		         $('#pilihanpergi_jampergi').html(jampergiberangkat);
		        $('#pilihanpergi_jamtiba').html(jampergitiba);
           $('#pilihanpergi_namakereta').html(namakereta);
		        $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(harga));

            nilpil+=1;
            $(location).attr('href', '#labelkolomutama');

          //  alert ('tes tes');
              $( "#labelkolomutama" ).focusin();
            if(pilrencana2=="O"){
             $('#sisikiri').hide('slow');
              $('#barissorting').hide('slow');
             mintaformgo();
           }else if(pilrencana2=="R"){
             mintaformret();
           }

		}
		function pilihPulang(bahanid
		  ,jampulangberangkat
		  ,jampulangtiba
		  ,harga
		  ,namakereta
      ,kumpulanidret
		  ){

                var totaladt=harga*jumadt;
                var totalchd=harga*jumchd;
                var totalinf=harga*juminf;
                harga=totaladt+totalchd+totalinf;
         $('#pilsubclasspulang').find('option').remove();

         $('#formgo_TrainNoRet').val(bahanid);
        $('#formgo_keretaRet').val(namakereta);
        $('#formgo_hargaRet').val(harga);
        $('#formgo_tgl_ret').val(jampulangberangkat);
        $('#formgo_tgl_ret_tiba').val(jampulangtiba);


        $('#formgo_selectedIDret').val(kumpulanidret);
         $('#barissorting').hide('slow');
		$('#labelpilihanpergi').show('slow');
  $('#labelkolomutama').show('slow');
		  $('#pilihanpulang').show('slow');
		//  alert(jampergitiba);
		        $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
		        //  alert($('#pilihanpergi_gbr').attr('src'));
		         $('#pilihanpulang_jampergi').html(jampulangberangkat);
		        $('#pilihanpulang_jamtiba').html(jampulangtiba);
            $('#pilihanpulang_namakereta').html('Pulang : '+namakereta);
		        $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(harga));
            nilpil+=1;
            if(pilrencana2=="R"){
              if(nilpil=="2"){
                mintaformgo();

              }
            }
          $(location).attr('href', '#labelkolomutama');

          $("#sisikanan").hide('slow');
    }
    function mintaformgo(){
    if(pilrencana2=="R"){
      $('#divpilihnextorulang').show('slow');
      $('#tomgoret').hide();
    }else if(pilrencana2=="O"){
      $('#tomgoret').hide();
  		$('#divpilihnextorulang').show('slow');
    }
    }
    function mintaformret(){
       $("#sisikiri").hide('slow');
       $("#sisikanan").show('slow');
    if(pilrencana2=="R"){
       $('#divpilihnextorulang').hide();
       $('#tomgoret').show('slow');
    }else if(pilrencana2=="O"){
       $('#tomgoret').hide();
  		 $('#divpilihnextorulang').show('slow');
    }
    }

		function sembunyi(){
		      $('#loadmaskapai').hide('slow');
		}
		function cari(){
    nilpil=0;
    if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

    $("#barissorting").show('slow');
    $("#sisikanan").hide();
    $('#divpilihnextorulang').hide();
    $('#tomgoret').hide();
		//  alert("Memulai pencarian");
		        //  $('#hasilkirim').html("<b>Hello world!</b>");
            if(pilrencana=="O"){
                $("#sisikanan").hide('slow');
                $("#sisikiri").show('slow');
                  //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                  //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
        		  }else{
                $("#sisikiri").show('slow');
                //$("#sisikanan").show('slow');
                  //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                  //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                  //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                  //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
        		  }
		        urljadwalb=urljadwal;
		        urljadwalb+="org/"+statasal;
		        urljadwalb+="/des/"+stattujuan;
		        urljadwalb+="/trip/"+pilrencana;
		        urljadwalb+="/tglberangkat/"+tglberangkat;
		        urljadwalb+="/tglpulang/"+tglpulang;
		        urljadwalb+="/jumadt/"+jumadt;
		        urljadwalb+="/jumchd/"+jumchd;
		        urljadwalb+="/juminf/"+juminf;
		        //akhir urljadwalb+="ac/";
										$('#loading_qg').show('fadeOut');

		                $('#dafpergi').html('');
										$('#dafpulang').html('');

						ambildata_qg();

            pilrencana2=pilrencana;
    $('#formatas').hide('slow');
		       $('#pilihanpergi').hide();
		        $('#pilihanpulang').hide();

          		      $('#labelkolomutama').hide('slow');
		      $('#labelpilihanpergi').hide('slow');
		      $('#tomulangipencarian').show('slow');


		      //  alert(urljadwalb);
		      setkolom();
        }else{
          $('#bahanmodal').dialog({ modal: true });
          // $( "#bahanmodal" ).show();
        //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
        }
		}


		var ajaxku_qg;
		function ambildata_qg(){
		  ajaxku_qg = buatajax();
		  var url=urljadwalb;
		  ajaxku_qg.onreadystatechange=stateChanged_qg;
		  ajaxku_qg.open("GET",url,true);
		  ajaxku_qg.send(null);
		}
		 function stateChanged_qg(){
		   var data;
		    if (ajaxku_qg.readyState==4){
		      data=ajaxku_qg.responseText;
		      if(data.length>0){
		       }else{
		       }
            $('#loading_qg').hide('slow');
 		                tambahData(data);
		     }
		}

		function tambahData(data){
			  //$('#dafpergi').html(data+$('#dafpergi').html());
				//$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
				//$('.bagpulang').remove();

        $(data).each(function(){
          if($(this).attr('isiitem')=="1"){
          if($(this).attr('untuk')=="pergi"){
          var temp_id=$(this).attr('id');
          var biaya=$(this).attr('data-harga');
          var waktu=$(this).attr('data-waktu');

          var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
          //alert(waktu);
          var valueToPush = new Array();
          valueToPush[0] = temp_id;
          valueToPush[1] = biaya;
          valueToPush[2] = waktu;
          valueToPush[3] = isiHTML;
          //dafHTMLPergi.push(valueToPush);
          $('#dafpergi').html(isiHTML+$('#dafpergi').html());


          }else if($(this).attr('untuk')=="pulang"){
          var temp_id=$(this).attr('id');
          var biaya=$(this).attr('data-harga');
          var waktu=$(this).attr('data-waktu');

            var isiHTML='<div class="list-item-entry isiitempulang isiitem" id="'+temp_id+'" data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
            //alert(temp_id);

          var valueToPush = new Array();
          valueToPush[0] = temp_id;
          valueToPush[1] = biaya;
          valueToPush[2] = waktu;
          valueToPush[3] = isiHTML;
          //dafHTMLPulang.push(valueToPush);
          $('#dafpulang').html(isiHTML+$('#dafpulang').html());


        }
      }});


		}

    jQuery.fn.sortElements = (function(){

        var sort = [].sort;

        return function(comparator, getSortable) {

            getSortable = getSortable || function(){return this;};

            var placements = this.map(function(){

                var sortElement = getSortable.call(this),
                    parentNode = sortElement.parentNode,

                    // Since the element itself will change position, we have
                    // to have some way of storing its original position in
                    // the DOM. The easiest way is to have a 'flag' node:
                    nextSibling = parentNode.insertBefore(
                        document.createTextNode(''),
                        sortElement.nextSibling
                    );

                return function() {

                    if (parentNode === this) {
                        throw new Error(
                            "You can't sort elements if any one is a descendant of another."
                        );
                    }

                    // Insert before flag:
                    parentNode.insertBefore(this, nextSibling);
                    // Remove flag:
                    parentNode.removeChild(nextSibling);

                };

            });

            return sort.call(this, comparator).each(function(i){
                placements[i].call(getSortable.call(this));
            });

        };

    })();
    var kolomsort="";
    function sorterAsc(a, b) {
    return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
    };
    function sorterDesc(a, b) {
    return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
    };


    function sortAsc(kolom){
      kolomsort=kolom;
      var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
      console.log(sortedDivs);
      $.each(sortedDivs, function (index, value) {
          $('#dafpergi').append(value);
      });


      var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
      console.log(sortedDivs);
      $.each(sortedDivs, function (index, value) {
          $('#dafpulang').append(value);
      });
    }

    function sortDesc(kolom){
      kolomsort=kolom;
        var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
        console.log(sortedDivs);
        $.each(sortedDivs, function (index, value) {
            $('#dafpergi').append(value);
        });


        var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
        console.log(sortedDivs);
        $.each(sortedDivs, function (index, value) {
            $('#dafpulang').append(value);
        });
    }

		var ajaxku;
		function ambildata(){
		  ajaxku = buatajax();
		  var url="{{ url('/') }}/jadwalPesawat";
		  //url=url+"?q="+nip;
		  //url=url+"&sid="+Math.random();
		  ajaxku.onreadystatechange=stateChanged;
		  ajaxku.open("GET",url,true);
		  ajaxku.send(null);
		}
		function buatajax(){
		  if (window.XMLHttpRequest){
		    return new XMLHttpRequest();
		  }
		  if (window.ActiveXObject){
		     return new ActiveXObject("Microsoft.XMLHTTP");
		   }
		   return null;
		 }
		 function stateChanged(){
		   var data;
		    if (ajaxku.readyState==4){
		      data=ajaxku.responseText;
		      if(data.length>0){
		        //document.getElementById("hasilkirim").html = data;

		                $('#hasilkirim').append(data);
		       }else{
		        // document.getElementById("hasilkirim").html = "";
		              //   $('#hasilkirim').html("");
		       }
		     }
		}

    @if($otomatiscari==1)
    statasal="{{$org}}";
    stattujuan="{{$des}}";
    pilrencana="{{$trip}}";
    tglberangkat="{{$tglberangkat}}";
    tglpulang="{{$tglpulang}}";
    jumadt="{{$jumadt}}";
    jumchd="{{$jumchd}}";
    juminf="{{$juminf}}";
    kotasal="{{$kotasal}}";
    kottujuan="{{$kottujuan}}";
    cari();
    @endif
		</script>
		@endsection

		@endsection
