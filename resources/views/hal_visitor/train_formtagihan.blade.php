@extends('layouts.'.$namatemplate)
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner style-6">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/train">kereta</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">thank you</h2>
  				</div>
			</div>
		</div>
	</div>

</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-8">
				<div class="detail-content-block">
					<h3 class="small-title">Isi Nomor Transaksi</h3>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
										<form role="form" method="get" action="transaksi" enctype = "multipart/form-data">
									 <tr>
											<input name="notrx" id="notrx"  type="text" class="form-control" >
										   </tr>
										 </form>

					      	</tbody>
					    </table>
				    </div>
				</div>




       		</div>
       		<div class="col-xs-12 col-md-4">
       		<div class="right-sidebar">
						<!----
					<div class="sidebar-text-label bg-dr-blue-2 color-white">useful information</div>

					<div class="help-contact bg-grey-2">
						<h4 class="color-dark-2">Need Help?</h4>
						<p class="color-grey-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a class="help-phone color-dark-2 link-dr-blue-2" href="tel:0200059600"><img src="img/detail/phone24-dark-2.png" alt="">020 00 59 600</a>
						<a class="help-mail color-dark-2 link-dr-blue-2" href="mailto:let’s_travel@world.com"><img src="img/detail/letter-dark-2.png" alt="">let’s_travel@world.com</a>
					</div>
				-->
       			</div>
       		</div>
       	</div>
	</div>
</div>

@section('akhirbody')
<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
	<script type="text/javascript">
		$('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
   }).val();
</script>
@endsection

@endsection
