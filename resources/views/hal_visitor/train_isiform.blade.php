<!DOCTYPE html>
@extends('layouts.'.$namatemplate)

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
    <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
 @endsection
@section('kontenweb')

<div class="inner-banner  ">
	<img class="center-image" src="{{ URL::asset('img/inner_banner_train.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">

			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
  				<li><a class="link-blue-2" href="{{ url('/') }}/train">kereta api</a> /</li>
				<li><span class="color-red-3">formulir data diri</span></li>
			</ul>


		</div>
	</div>
</div>



<div class="detail-wrapper">
 <div class="container">
         <div class="row padd-90">
           <div class="col-xs-12 col-md-8">
       <form class="simple-from" method="POST" action="kirimdatadiri" id="formutama">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <input type="hidden" id="totalamount" name="totalamount" value="{{$totalamount}}"  >
         <input type="hidden" id="angkaunik" name="angkaunik" value="{{$angkaunik}}"  >
         <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
         <input type="hidden" id="org" name="org" value="{{$org}}"  >
         <input type="hidden" id="des" name="des" value="{{$des}}" >
         <input type="hidden" id="TrainNoDep" name="TrainNoDep" value="{{$TrainNoDep}}"  >
         <input type="hidden" id="TrainNoRet" name="TrainNoRet" value="{{$TrainNoRet}}" >
         <input type="hidden" id="keretaDep" name="keretaDep" value="{{$keretaDep}}"  >
         <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
         <input type="hidden" id="tgl_dep_tiba" name="tgl_dep_tiba" value="{{$tgl_dep_tiba}}"  >
         <input type="hidden" id="trip" name="trip" value="{{$trip}}"  >
         <input type="hidden" id="keretaRet" name="keretaRet" value="{{$keretaRet}}"  >
         <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
         <input type="hidden" id="tgl_ret_tiba" name="tgl_ret_tiba" value="{{$tgl_ret_tiba}}"  >
         <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
         <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
         <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
         <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
         <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >

         <input type="hidden" id="isiancpname" name="isiancpname"  >
         <input type="hidden" id="isiancptlp" name="isiancptlp">
         <input type="hidden" id="isiancpmail" name="isiancpmail" >
         <input type="hidden" id="dafbangkudep" name="dafbangkudep" value="{{$formgo_dafbangkudep}}" >
         <input type="hidden" id="dafbangkuret" name="dafbangkuret" value="{{$formgo_dafbangkuret}}" >

         <!-- ISIAN DEWASA -->
        <?php $jumadt=0; ?>
       @while ($jumadt<$adt)
        <?php $jumadt++; ?>
       <input type="hidden" id="isiantitadt_{{$jumadt}}" name="isiantitadt_{{$jumadt}}" >
       <input type="hidden" id="isianfnadt_{{$jumadt}}" name="isianfnadt_{{$jumadt}}" >
       <input type="hidden" id="isianlnadt_{{$jumadt}}" name="isianlnadt_{{$jumadt}}" >
       <input type="hidden" id="isianhpadt_{{$jumadt}}" name="isianhpadt_{{$jumadt}}" >
      @endwhile

      <!-- ISIAN ANAK -->
       <?php $jumchd=0; ?>
      @while ($jumchd<$chd)
       <?php $jumchd++; ?>
      <input type="hidden" id="isiantitchd_{{$jumchd}}" name="isiantitchd_{{$jumchd}}"  >
      <input type="hidden" id="isianfnchd_{{$jumchd}}" name="isianfnchd_{{$jumchd}}" >
      <input type="hidden" id="isianlnchd_{{$jumchd}}" name="isianlnchd_{{$jumchd}}" >
      <input type="hidden" id="isianbirthchd_{{$jumchd}}" name="isianbirthchd_{{$jumchd}}" >
      @endwhile

      <!-- ISIAN BAYI -->
       <?php $juminf=0; ?>
      @while ($juminf<$inf)
       <?php $juminf++; ?>
      <input type="hidden" id="isiantitinf_{{$juminf}}" name="isiantitinf_{{$juminf}}" >
      <input type="hidden" id="isianfninf_{{$juminf}}" name="isianfninf_{{$juminf}}" >
      <input type="hidden" id="isianlninf_{{$juminf}}" name="isianlninf_{{$juminf}}" >
      <input type="hidden" id="isianbirthinf_{{$juminf}}" name="isianbirthinf_{{$juminf}}" >

         @endwhile

     <input type="hidden" name="fields[code]" value="56345678safs_">

         <div class="simple-group">
           <h3 class="small-title">Info Kontak</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="cpname" name="cpname" required="" placeholder="Nama">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Telepon</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="cptlp" name="cptlp" required="" placeholder="Nomor Telepon">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">E-mail</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" id="cpmail" name="cpmail" required="" placeholder="Email">
                 </div>
               </div>
             </div>

           </div>
         </div>
         <?php $jumpen=$adt+$chd+$inf;$jumadt=0; ?>
        @while ($jumadt<$adt)
           <?php $jumadt++; ?>
         <div class="simple-group">
           <h3 class="small-title">Data diri dewasa #{{$jumadt}}</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Title</div>
                 <select name="titadt_{{$jumadt}}" id="titadt_{{$jumadt}}"class="mainselection" style="width:100%;">

                   <option value="MR">MR</option>
                   <option value="MS">MS</option>
                   <option value="MRS">MRS</option>

                 </select>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Depan</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="fnadt_{{$jumadt}}" id="fnadt_{{$jumadt}}" required="" placeholder="Nama Depan">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Belakang</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="lnadt_{{$jumadt}}" id="lnadt_{{$jumadt}}" required="" placeholder="Nama Belakang">
                 </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nomor Handphone</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                     <input type="text" name="hpadt_{{$jumadt}}" id="hpadt_{{$jumadt}}" required="" placeholder="Nomor Handphone">
                 </div>
               </div>
             </div>

           </div>

         </div>
         @endwhile
         <?php $jumchd=0; ?>
         @while ($jumchd<$chd)
         <?php $jumchd++; ?>
         <div class="simple-group">
           <h3 class="small-title">Data diri anak #{{$jumadt}}</h3>
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Title</div>
                 <select name="titchd_{{$jumchd}}" id="titchd_{{$jumchd}}"class="mainselection" >

                   <option value="MSTR">MSTR</option>
                   <option value="MISS">MISS</option>

                 </select>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Depan</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="fnchd_{{$jumchd}}" id="fnchd_{{$jumchd}}" required="" placeholder="Nama Depan">
                </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Nama Belakang</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="lnchd_{{$jumchd}}" id="lnchd_{{$jumchd}}" required="" placeholder="Nama Belakang">
               </div>
               </div>
             </div>
             <div class="col-xs-12 col-sm-6">
               <div class="form-block type-2 clearfix">
                 <div class="form-label color-dark-2">Tanggal lahir</div>
                 <div class="input-style-1 b-50 brd-0 type-2 color-3">
                   <input type="text" name="birthchd_{{$jumchd}}" id="birthchd_{{$jumchd}}" required="" placeholder="Tanggal Lahir (YYYY-MM-DD)">
               </div>
               </div>
             </div>

           </div>

         </div>
         @endwhile
         <?php $juminf=0; ?>
         @while ($juminf<$inf)
        <?php $juminf++; ?>
        <div class="simple-group">
          <h3 class="small-title">Data diri bayi #{{$jumadt}}</h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Title</div>
                <select name="titinf_{{$juminf}}" id="titinf_{{$juminf}}"class="mainselection" >

                  <option value="MSTR">MSTR</option>
                  <option value="MISS">MISS</option>

                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Nama Depan</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text" name="fninf_{{$juminf}}" id="fninf_{{$juminf}}" required="" placeholder="Nama Depan">
                 </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Nama Belakang</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text" name="lninf_{{$juminf}}" id="lninf_{{$juminf}}" required="" placeholder="Nama Belakang">
              </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-block type-2 clearfix">
                <div class="form-label color-dark-2">Tanggal lahir</div>
                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                  <input type="text" name="birthinf_{{$juminf}}" id="birthinf_{{$juminf}}" required="" placeholder="Tanggal Lahir (YYYY-MM-DD)">
              </div>
              </div>
            </div>

          </div>

        </div>
         @endwhile
         <input type="submit" id="tombolsubmit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="confirm booking">
       </form>
           </div>
           <div class="col-xs-12 col-md-4">
             <div class="right-sidebar">
               <div class="sidebar-text-label bg-dr-blue-2 color-white">informasi perjalanan</div>
               <div class="help-contact bg-grey-2">
                 <h4 class="color-dark-2">{{$kotorg}} (Stasiun {{$st_nameorg}}) ke {{$kotdes}} (Stasiun {{$st_namedes}})</h4>


               <p class="color-grey-2">{{$keretaDep}} nomor {{$TrainNoDep}}</p>
               <p class="color-grey-2">Berangkat : {{$tgl_dep}}</p>
               <p class="color-grey-2">Tiba : {{$tgl_dep_tiba}}</p>


                @if($trip=="R")
                 <h4 class="color-dark-2">{{$st_citydes}} (Stasiun {{$st_namedes}}) ke {{$st_cityorg}} (Stasiun {{$st_nameorg}})</h4>

               <p class="color-grey-2"> {{$keretaRet}} nomor {{$TrainNoRet}}</p>
               <p class="color-grey-2">Berangkat : {{$tgl_ret}}</p>
               <p class="color-grey-2">Tiba : {{$tgl_ret_tiba}}</p>

                @endif
               </div>



          
             </div>
           </div>
         </div>
 </div>
</div>

<!-- AKHIR FORM -->



  		</div>
  	</div>


				@section('akhirbody')
		<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
		<script type="text/javascript">

    $( "#tombolsubmit" ).click(function() {
    //  alert( "Handler for .click() called." );
      $('#isiancpname').val($('#cpname').val());
      $('#coba').val($('#cptlp').val());
      $('#isiancptlp').val($('#cptlp').val());
      $('#isiancpmail').val($('#cpmail').val());


      <?php $jumadt=0; ?>
     @while ($jumadt<$adt)
     <?php $jumadt++; ?>
     $('#isiantitadt_{{$jumadt}}').val($('#titadt_{{$jumadt}}').val());
     $('#isianfnadt_{{$jumadt}}').val($('#fnadt_{{$jumadt}}').val());
     $('#isianlnadt_{{$jumadt}}').val($('#lnadt_{{$jumadt}}').val());
     $('#isianhpadt_{{$jumadt}}').val($('#hpadt_{{$jumadt}}').val());
     @endwhile


     <?php $jumchd=0; ?>
    @while ($jumchd<$chd)
    <?php $jumchd++; ?>
    $('#isiantitchd_{{$jumchd}}').val($('#titchd_{{$jumchd}}').val());
    $('#isianfnchd_{{$jumchd}}').val($('#fnchd_{{$jumchd}}').val());
    $('#isianlnchd_{{$jumchd}}').val($('#lnchd_{{$jumchd}}').val());
    $('#isianbirthchd_{{$jumchd}}').val($('#birthchd_{{$jumchd}}').val());
    @endwhile

    <?php $juminf=0; ?>
   @while ($juminf<$inf)
   <?php $juminf++; ?>
   $('#isiantitinf_{{$juminf}}').val($('#titinf_{{$juminf}}').val());
   $('#isianfninf_{{$juminf}}').val($('#fninf_{{$juminf}}').val());
   $('#isianlninf_{{$juminf}}').val($('#lninf_{{$juminf}}').val());
   $('#isianbirthinf_{{$juminf}}').val($('#birthinf_{{$juminf}}').val());
   @endwhile

  //$( "#formutama" ).submit();
    });
        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
