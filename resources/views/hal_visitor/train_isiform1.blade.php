<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
    <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
 @endsection
@section('kontenweb')

<div class="inner-banner  ">
	<img class="center-image" src="{{ URL::asset('img/inner_banner_train.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">

			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
  				<li><a class="link-blue-2" href="{{ url('/') }}/train">kereta api</a> /</li>
				<li><span class="color-red-3">formulir data diri</span></li>
			</ul>


		</div>
	</div>
</div>

  	<div class="list-wrapper bg-grey-2">
  		<div class="container">

          			<div class="row">

          				<div class="col-xs-12 col-sm-8 col-md-12">

                      <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">
                        <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Kereta dari : {{$kotorg}} ({{$org}}) - {{$kotdes}} ({{$des}})  | Dewasa: {{$adt}} Anak: {{$chd}} Bayi: {{$inf}}</h4>
                      </div>
                      <div class="list-content clearfix" >
                      <div class="list-item-entry" id="pilihanpergi">
                            <div class="hotel-item style-10 bg-white">
                              <div class="table-view">
                                <div class="radius-top cell-view">
                                <span class="tulisanrute">  {{$keretaDep}} </span>

                                </div>
                                <div class="title hotel-middle cell-view">

                              <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                                    <div class="fi_block grid-hidden row row10">
                                      <div class="flight-icon col-xs-6 col10">
                                        <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                                        <div class="fi_content">
                                          <div class="fi_title color-dark-2">Pergi</div>
                                          <div class="fi_text color-grey" id="pilihanpergi_jampergi"> {{$tgl_dep}}</div>
                                        </div>
                                      </div>
                                      <div class="flight-icon col-xs-6 col10">
                                        <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                                        <div class="fi_content">
                                          <div class="fi_title color-dark-2">Tiba</div>
                                          <div class="fi_text color-grey" id="pilihanpergi_jamtiba"> {{$tgl_dep_tiba}}</div>
                                        </div>
                                      </div>
                                    </div>

                                 </div>
                                <div class="title hotel-right clearfix cell-view grid-hidden">
                                    <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
                                      <div class="hotel-right-text color-dark-2">1 stop</div>-->
                                   </div>
                              </div>
                          </div>
                      </div>

                      </div>
          					<div class="list-content clearfix" >


          					</div>

                @if($trip=="R")
                <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpulang">

                  <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Kereta dari : {{$kotdes}} ({{$des}}) - {{$kotorg}} ({{$org}})  | Dewasa: {{$adt}} Anak: {{$chd}} Bayi: {{$inf}}</h4>

                </div>
                <div class="list-content clearfix" >
                <div class="list-item-entry" id="pilihanpergi">
                      <div class="hotel-item style-10 bg-white">
                        <div class="table-view">
                          <div class="radius-top cell-view">
                          <span class="tulisanrute">  {{$keretaRet}} </span>

                          </div>
                          <div class="title hotel-middle cell-view">

                        <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
                              <div class="fi_block grid-hidden row row10">
                                <div class="flight-icon col-xs-6 col10">
                                  <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
                                  <div class="fi_content">
                                    <div class="fi_title color-dark-2">Pergi</div>
                                    <div class="fi_text color-grey" id="pilihanpergi_jampergi"> {{$tgl_ret}}</div>
                                  </div>
                                </div>
                                <div class="flight-icon col-xs-6 col10">
                                  <img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
                                  <div class="fi_content">
                                    <div class="fi_title color-dark-2">Tiba</div>
                                    <div class="fi_text color-grey" id="pilihanpergi_jamtiba"> {{$tgl_ret_tiba}}</div>
                                  </div>
                                </div>
                              </div>

                           </div>
                          <div class="title hotel-right clearfix cell-view grid-hidden">
                              <!--  <div class="hotel-right-text color-dark-2">one way flights</div>
                                <div class="hotel-right-text color-dark-2">1 stop</div>-->
                             </div>
                        </div>
                    </div>
                </div>

                </div>
                    @endif



                    <div class=" clearfix" style="background-color: #ff6600;"id="labelkotakketeranan">

                      <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labeltotalbiaya"  >Total :  {{rupiah($totalamount+$angkaunik)}}</h4>

                    </div>
                    <div style="padding-top:1%;">

                                              <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                                                <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Formulir Data Kontak</h4>

                                              </div>
                          </div>

          				</div>
                </div>
                <!-- BEKAS ROW -->

                <div class="row" style="padding-top:1%;">
                  <div class="col-xs-12">
                    <form class="contact-form js-contact-form" method="POST" action="haha" >
                      <div class="row">
                        <div class="col-xs-12 col-sm-6">
                          <div class="input-style-1 type-2 color-2">
                              <input type="text" id="cpname" name="cpname" required="" placeholder="Nama">
                          </div>
                        </div>
                          <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                                <input type="text" id="cptlp" name="cptlp" required="" placeholder="Nomor Telepon">
                            </div>
                          </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="input-style-1 type-2 color-2">
                              <input type="text" id="cpmail" name="cpmail" required="" placeholder="Email">
                          </div>
                        </div>
                     </div>
                    </form>
                    <div class="ajax-result">
                              <div class="success"></div>
                              <div class="error"></div>
                            </div>
                            <div class="ajax-loader"></div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-md-12">

                <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                  <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama"  >Formulir Data Penumpang</h4>

                </div>
                    <div class="simple-tab type-2 tab-wrapper kotak">
                      <div class="tab-nav-wrapper-modif">
                        <div  class="nav-tab  clearfix">
                           <?php $jumadt=0; ?>
                          @while ($jumadt<$adt)
                             <?php $jumadt++; ?>
                          <div class="nav-tab-item <?php if($jumadt==1){print("active");} ?>">
                            Dewasa {{$jumadt}}
                          </div>
                          @endwhile
                            <?php $jumchd=0; ?>
                          @while ($jumchd<$chd)
                              <?php $jumchd++; ?>
                          <div class="nav-tab-item">
                            Anak {{$jumchd}}
                          </div>
                          @endwhile
                          <?php $juminf=0; ?>
                          @while ($juminf<$inf)
                             <?php $juminf++; ?>
                          <div class="nav-tab-item">
                            Bayi {{$juminf}}
                          </div>
                          @endwhile
                        </div>
                      </div>
                      <div class="tabs-content tabs-wrap-style-modif clearfix">


                        <!-- Loop form dewasa -->

                        <?php $jumadt=0; ?>
                          @while ($jumadt<$adt)
                             <?php $jumadt++; ?>
                          <div class="tab-info <?php if($jumadt==1){print("active");} ?>">
                             <div class="acc-body">

                                              <div class="row" style="padding-top:1%;">
                                                <div class="col-xs-12">
                                                  <form class="contact-form js-contact-form" method="POST" action="#" >
                                                    <div class="row">
                                                      <div class="col-xs-12 col-sm-6">
                                                        <div class="input-style-1 type-2 color-2" style="padding-top:3%;">
                                                        <select name="titadt_{{$jumadt}}" id="titadt_{{$jumadt}}"class="mainselection" style="width:100%;">

                                                          <option value="MR">MR</option>
                                                          <option value="MS">MS</option>
                                                          <option value="MRS">MRS</option>

                                                        </select>
                                                        </div>
                                                      </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                          <div class="input-style-1 type-2 color-2modif">
                                                              <input type="text" name="fnadt_{{$jumadt}}" id="fnadt_{{$jumadt}}" required="" placeholder="Nama Depan">
                                                          </div>
                                                        </div>
                                                       <div class="col-xs-12 col-sm-6">
                                                         <div class="input-style-1 type-2 color-2modif">
                                                             <input type="text" name="lnadt_{{$jumadt}}" id="lnadt_{{$jumadt}}" required="" placeholder="Nama Belakang">
                                                         </div>
                                                       </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                          <div class="input-style-1 type-2 color-2modif">
                                                              <input type="text" name="hpadt_{{$jumadt}}" id="hpadt_{{$jumadt}}" required="" placeholder="Nomor Handphone">
                                                          </div>
                                                        </div>
                                                     </div>
                                                  </form>
                                                  <div class="ajax-result">
                                                            <div class="success"></div>
                                                            <div class="error"></div>
                                                          </div>
                                                          <div class="ajax-loader"></div>
                                                </div>
                                              </div>

                             </div>
                          </div>

                          @endwhile
                        <!--Loop form anak-->
                        <?php $jumchd=0; ?>
                          @while ($jumchd<$chd)
                             <?php $jumchd++; ?>
                          <div class="tab-info">
                             <div class="acc-body">

                                              <div class="row" style="padding-top:1%;">
                                                <div class="col-xs-12">
                                                  <form class="contact-form js-contact-form" method="POST" action="#">
                                                    <div class="row">
                                                      <div class="col-xs-12 col-sm-6">
                                                        <div class="input-style-1 type-2 color-2" style="padding-top:3%;">
                                                        <select name="titchd_{{$jumchd}}" id="titchd_{{$jumchd}}"class="mainselection" >

                                                          <option value="MSTR">MSTR</option>
                                                          <option value="MISS">MISS</option>

                                                        </select>
                                                        </div>
                                                      </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                          <div class="input-style-1 type-2 color-2modif">
                                                              <input type="text" name="fnchd_{{$jumchd}}" id="fnchd_{{$jumchd}}" required="" placeholder="Nama Depan">
                                                          </div>
                                                        </div>
                                                       <div class="col-xs-12 col-sm-6">
                                                         <div class="input-style-1 type-2 color-2modif">
                                                             <input type="text" name="lnchd_{{$jumchd}}" id="lnchd_{{$jumchd}}" required="" placeholder="Nama Belakang">
                                                         </div>
                                                       </div>
                                                      <div class="col-xs-12 col-sm-6">
                                                        <div class="input-style-1 type-2 color-2modif">
                                                            <input type="text" name="birthchd_{{$jumchd}}" id="birthchd_{{$jumchd}}" required="" placeholder="Tanggal Lahir (YYYY-MM-DD)">
                                                        </div>
                                                      </div>
                                                     </div>
                                                  </form>
                                                  <div class="ajax-result">
                                                            <div class="success"></div>
                                                            <div class="error"></div>
                                                          </div>
                                                          <div class="ajax-loader"></div>
                                                </div>
                                              </div>

                             </div>
                          </div>

                          @endwhile


                          <!--Loop form bayi-->
                          <?php $juminf=0; ?>
                            @while ($juminf<$inf)
                               <?php $juminf++; ?>
                            <div class="tab-info">
                               <div class="acc-body">

                                                <div class="row" style="padding-top:1%;">
                                                  <div class="col-xs-12">
                                                    <form class="contact-form js-contact-form" method="POST" action="#">
                                                      <div class="row">
                                                        <div class="col-xs-12 col-sm-6">
                                                          <div class="input-style-1 type-2 color-2" style="padding-top:3%;">
                                                          <select name="titinf_{{$juminf}}" id="titinf_{{$juminf}}"class="mainselection" >

                                                            <option value="MSTR">MSTR</option>
                                                            <option value="MISS">MISS</option>

                                                          </select>
                                                          </div>
                                                        </div>
                                                          <div class="col-xs-12 col-sm-6">
                                                            <div class="input-style-1 type-2 color-2modif">
                                                                <input type="text" name="fninf_{{$juminf}}" id="fninf_{{$juminf}}" required="" placeholder="Nama Depan">
                                                            </div>
                                                          </div>
                                                         <div class="col-xs-12 col-sm-6">
                                                           <div class="input-style-1 type-2 color-2modif">
                                                               <input type="text" name="lninf_{{$juminf}}" id="lninf_{{$juminf}}" required="" placeholder="Nama Belakang">
                                                           </div>
                                                         </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                          <div class="input-style-1 type-2 color-2modif">
                                                              <input type="text" name="birthinf_{{$juminf}}" id="birthinf_{{$juminf}}" required="" placeholder="Tanggal Lahir (YYYY-MM-DD)">
                                                          </div>
                                                        </div>
                                                       </div>
                                                    </form>
                                                    <div class="ajax-result">
                                                              <div class="success"></div>
                                                              <div class="error"></div>
                                                            </div>
                                                            <div class="ajax-loader"></div>
                                                  </div>
                                                </div>

                               </div>
                            </div>

                            @endwhile


                        </div>
                    </div>
                   </div>
                  </div>
<!-- AKHIR FORM -->

  <form class="contact-form " method="POST" action="kirimdatadiri" id="formutama">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="totalamount" name="totalamount" value="{{$totalamount}}"  >
    <input type="hidden" id="angkaunik" name="angkaunik" value="{{$angkaunik}}"  >
    <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
    <input type="hidden" id="org" name="org" value="{{$org}}"  >
    <input type="hidden" id="des" name="des" value="{{$des}}" >
    <input type="hidden" id="TrainNoDep" name="TrainNoDep" value="{{$TrainNoDep}}"  >
    <input type="hidden" id="TrainNoRet" name="TrainNoRet" value="{{$TrainNoRet}}" >
    <input type="hidden" id="keretaDep" name="keretaDep" value="{{$keretaDep}}"  >
    <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
    <input type="hidden" id="tgl_dep_tiba" name="tgl_dep_tiba" value="{{$tgl_dep_tiba}}"  >
    <input type="hidden" id="trip" name="trip" value="{{$trip}}"  >
    <input type="hidden" id="keretaRet" name="keretaRet" value="{{$keretaRet}}"  >
    <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
    <input type="hidden" id="tgl_ret_tiba" name="tgl_ret_tiba" value="{{$tgl_ret_tiba}}"  >
    <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
    <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
    <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
    <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
    <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >

    <input type="hidden" id="isiancpname" name="isiancpname"  >
    <input type="hidden" id="isiancptlp" name="isiancptlp">
    <input type="hidden" id="isiancpmail" name="isiancpmail" >
    <input type="hidden" id="dafbangkudep" name="dafbangkudep" value="{{$formgo_dafbangkudep}}" >
    <input type="hidden" id="dafbangkuret" name="dafbangkuret" value="{{$formgo_dafbangkuret}}" >

    <!-- ISIAN DEWASA -->
   <?php $jumadt=0; ?>
  @while ($jumadt<$adt)
   <?php $jumadt++; ?>
  <input type="hidden" id="isiantitadt_{{$jumadt}}" name="isiantitadt_{{$jumadt}}" >
  <input type="hidden" id="isianfnadt_{{$jumadt}}" name="isianfnadt_{{$jumadt}}" >
  <input type="hidden" id="isianlnadt_{{$jumadt}}" name="isianlnadt_{{$jumadt}}" >
  <input type="hidden" id="isianhpadt_{{$jumadt}}" name="isianhpadt_{{$jumadt}}" >
 @endwhile

 <!-- ISIAN ANAK -->
  <?php $jumchd=0; ?>
 @while ($jumchd<$chd)
  <?php $jumchd++; ?>
 <input type="hidden" id="isiantitchd_{{$jumchd}}" name="isiantitchd_{{$jumchd}}"  >
 <input type="hidden" id="isianfnchd_{{$jumchd}}" name="isianfnchd_{{$jumchd}}" >
 <input type="hidden" id="isianlnchd_{{$jumchd}}" name="isianlnchd_{{$jumchd}}" >
 <input type="hidden" id="isianbirthchd_{{$jumchd}}" name="isianbirthchd_{{$jumchd}}" >
 @endwhile

 <!-- ISIAN BAYI -->
  <?php $juminf=0; ?>
 @while ($juminf<$inf)
  <?php $juminf++; ?>
 <input type="hidden" id="isiantitinf_{{$juminf}}" name="isiantitinf_{{$juminf}}" >
 <input type="hidden" id="isianfninf_{{$juminf}}" name="isianfninf_{{$juminf}}" >
 <input type="hidden" id="isianlninf_{{$juminf}}" name="isianlninf_{{$juminf}}" >
 <input type="hidden" id="isianbirthinf_{{$juminf}}" name="isianbirthinf_{{$juminf}}" >

    @endwhile

                              <div style="padding-bottom:1%;">
                                  <div class=" clearfix" style="background-color: #ff6600;"id="labelpilihanpergi">

                                                        <button type="button" class="btn btn-block btn-primary" id="tombolsubmit">Submit</button>
                                  </div>
                                </div>

                                 <input type="hidden" name="fields[code]" value="56345678safs_">

                              </form>

  		</div>
  	</div>


				@section('akhirbody')
		<script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js')}}"></script>
		<script src="{{ URL::asset('asettemplate1/js/all.js')}}"></script>
		<script type="text/javascript">

    $( "#tombolsubmit" ).click(function() {
    //  alert( "Handler for .click() called." );
      $('#isiancpname').val($('#cpname').val());
      $('#coba').val($('#cptlp').val());
      $('#isiancptlp').val($('#cptlp').val());
      $('#isiancpmail').val($('#cpmail').val());


      <?php $jumadt=0; ?>
     @while ($jumadt<$adt)
     <?php $jumadt++; ?>
     $('#isiantitadt_{{$jumadt}}').val($('#titadt_{{$jumadt}}').val());
     $('#isianfnadt_{{$jumadt}}').val($('#fnadt_{{$jumadt}}').val());
     $('#isianlnadt_{{$jumadt}}').val($('#lnadt_{{$jumadt}}').val());
     $('#isianhpadt_{{$jumadt}}').val($('#hpadt_{{$jumadt}}').val());
     @endwhile


     <?php $jumchd=0; ?>
    @while ($jumchd<$chd)
    <?php $jumchd++; ?>
    $('#isiantitchd_{{$jumchd}}').val($('#titchd_{{$jumchd}}').val());
    $('#isianfnchd_{{$jumchd}}').val($('#fnchd_{{$jumchd}}').val());
    $('#isianlnchd_{{$jumchd}}').val($('#lnchd_{{$jumchd}}').val());
    $('#isianbirthchd_{{$jumchd}}').val($('#birthchd_{{$jumchd}}').val());
    @endwhile

    <?php $juminf=0; ?>
   @while ($juminf<$inf)
   <?php $juminf++; ?>
   $('#isiantitinf_{{$juminf}}').val($('#titinf_{{$juminf}}').val());
   $('#isianfninf_{{$juminf}}').val($('#fninf_{{$juminf}}').val());
   $('#isianlninf_{{$juminf}}').val($('#lninf_{{$juminf}}').val());
   $('#isianbirthinf_{{$juminf}}').val($('#birthinf_{{$juminf}}').val());
   @endwhile

  $( "#formutama" ).submit();
    });
        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
