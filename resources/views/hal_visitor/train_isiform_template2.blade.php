<!DOCTYPE html>
@extends('layouts.'.$namatemplate)

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Isi data penumpang</h2>
        </div>
        <ul class="breadcrumbs pull-right">
          <li><a href="{{url('/')}}">Home</a>
          </li>
          <li><a href="{{$alamatbalik}}">Cari Tiket Kereta</a>
          </li>
          <li class="active">Isi data penumpang</li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sms-6 col-sm-8 col-md-9">
                <div class="booking-section travelo-box">
                  <div class="tab-content justmobile">
                      <div class="tab-pane fade in active" id="hotel-description">
                          <div class="intro table-wrapper full-width hidden-table-sms">
                      <div class="col-sm-5 col-lg-4 features table-cell">
                          <ul>
                              <li><label>Kereta pergi:</label>{{$kotorg}} ke {{$kotdes}}</li>
                              <li><label>Berangkat:</label>{{$tgl_dep}}</li>
                              <li><label>Sampai:</label>{{$tgl_dep_tiba}}</li>
                              <li><label>Daftar bangku:
                              <?php
                              $dafbangkudep=explode(",",$formgo_dafbangkudep);
                              $a=0;
                              ?>
                              @foreach($dafbangkudep as $d)
                              @if($d!=null)
                              @if($a==0)<?php $a=1;print("</label>".$d."</li>");?>@else
                              <li><label></label>{{$d}}</li>
                              @endif
                               @endif
                              @endforeach
                          </ul>
                      </div>
                      @if($trip=="R")
                      <div class="col-sm-5 col-lg-4 features table-cell">
                        <ul style="padding-left:20px;">
                            <li><label>Kereta pulang:</label>{{$kotdes}} ke {{$kotorg}}</li>
                            <li><label>Berangkat:</label>{{$tgl_ret}}</li>
                            <li><label>Sampai:</label>{{$tgl_ret_tiba}}</li>
                            <li><label>Daftar bangku:
                            <?php
                            $dafbangkuret=explode(",",$formgo_dafbangkuret);
                            $a=0;
                            ?>
                            @foreach($dafbangkuret as $d)
                            @if($d!=null)
                            @if($a==0)<?php $a=1;print("</label>".$d."</li>");?>@else
                            <li><label></label>{{$d}}</li>
                            @endif
                             @endif
                            @endforeach
                        </ul>
                      </div>
                       @endif
                  </div>
              </div>
          </div>
          <form class="simple-from" method="POST" action="kirimdatadiri" id="formutama">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="totalamount" name="totalamount" value="{{Crypt::encrypt($totalamount)}}"  >
            <input type="hidden" id="angkaunik" name="angkaunik" value="{{Crypt::encrypt($angkaunik)}}"  >
            <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
            <input type="hidden" id="org" name="org" value="{{$org}}"  >
            <input type="hidden" id="des" name="des" value="{{$des}}" >
            <input type="hidden" id="TrainNoDep" name="TrainNoDep" value="{{$TrainNoDep}}"  >
            <input type="hidden" id="TrainNoRet" name="TrainNoRet" value="{{$TrainNoRet}}" >
            <input type="hidden" id="keretaDep" name="keretaDep" value="{{$keretaDep}}"  >
            <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
            <input type="hidden" id="tgl_dep_tiba" name="tgl_dep_tiba" value="{{$tgl_dep_tiba}}"  >
            <input type="hidden" id="trip" name="trip" value="{{$trip}}"  >
            <input type="hidden" id="keretaRet" name="keretaRet" value="{{$keretaRet}}"  >
            <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
            <input type="hidden" id="tgl_ret_tiba" name="tgl_ret_tiba" value="{{$tgl_ret_tiba}}"  >
            <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
            <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
            <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
            <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
            <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >
            <input type="hidden" id="dafbangkudep" name="dafbangkudep" value="{{$formgo_dafbangkudep}}" >
            <input type="hidden" id="dafbangkuret" name="dafbangkuret" value="{{$formgo_dafbangkuret}}" >
                  <div class="person-information">
                            <h2>Formulir Kontak</h2>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama</label>
                                    <input type="text" id="isiancpname" name="isiancpname" required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Telepon</label>
                                    <input type="text" id="isiancptlp" name="isiancptlp" required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>email address</label>
                                    <input type="email" id="isiancpmail" name="isiancpmail" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>

                        </div>
                        <hr />
                        <?php $jumpen=$adt+$chd+$inf;$jumadt=0; ?>
                       @while ($jumadt<$adt)
                          <?php $jumadt++; ?>
                        <div class="card-information">
                            <h2>Data diri dewasa #{{$jumadt}}</h2>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Gelar</label>
                                    <div class="selector">
                                        <select class="full-width"name="isiantitadt_{{$jumadt}}" id="isiantitadt_{{$jumadt}}">
                                          <option value="MR">MR</option>
                                          <option value="MS">MS</option>
                                          <option value="MRS">MRS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Depan</label>
                                    <input type="text"name="isianfnadt_{{$jumadt}}" id="isianfnadt_{{$jumadt}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="isianlnadt_{{$jumadt}}" id="isianlnadt_{{$jumadt}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nomor Handphone</label>
                                    <input type="text" name="isianhpadt_{{$jumadt}}" id="isianhpadt_{{$jumadt}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>

                        </div>
                        <hr />
                        @endwhile

                        <?php $jumchd=0; ?>
                        @while ($jumchd<$chd)
                        <?php $jumchd++; ?>
                        <div class="card-information">
                            <h2>Data diri anak #{{$jumchd}}</h2>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Gelar</label>
                                    <div class="selector">
                                        <select class="full-width"name="isiantitchd_{{$jumchd}}" id="isiantitchd_{{$jumchd}}">
                                          <option value="MSTR">MSTR</option>
                                          <option value="MISS">MISS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Depan</label>
                                    <input type="text"name="isianfnchd_{{$jumchd}}" id="isianfnchd_{{$jumchd}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="isianlnchd_{{$jumchd}}" id="isianlnchd_{{$jumchd}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Tanggal lahir</label>
                                    <div class="datepicker-wrapbirth">
                                    <input type="text" name="isianbirthchd_{{$jumchd}}" id="isianbirthchd_{{$jumchd}}" required=""  class="isianlahir input-text full-width" value="" placeholder="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr />
                        @endwhile

                        <?php $juminf=0; ?>
                        @while ($juminf<$inf)
                       <?php $juminf++; ?>
                        <div class="card-information">
                            <h2>Data diri bayi #{{$juminf}}</h2>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Gelar</label>
                                    <div class="selector">
                                        <select class="full-width"name="isiantitinf_{{$juminf}}" id="isiantitinf_{{$juminf}}">
                                          <option value="MSTR">MSTR</option>
                                          <option value="MISS">MISS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Depan</label>
                                    <input type="text"name="isianfninf_{{$juminf}}" id="isianfninf_{{$juminf}}" required=""  class="input-text full-width" value="" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="isianlninf_{{$juminf}}" id="isianlninf_{{$juminf}}"  required="" class="input-text full-width" value="" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Tanggal lahir</label>
                                    <div class="datepicker-wrapbirth">
                                    <input type="text" name="isianbirthinf_{{$juminf}}" id="isianbirthinf_{{$juminf}}" required=""  class="isianlahir input-text full-width" value="" placeholder="dd/mm/yy" />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr />
                        @endwhile

                        <div class="form-group row">
                            <div class="col-sm-6 col-md-5">
                                <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="sidebar col-sms-6 col-sm-4 col-md-3 nomobile">
                <div class="booking-details travelo-box">
                    <h4>Kereta pergi</h4>
                    <article class="flight-booking-details">
                        <figure class="clearfix">
                            <a title="" href="flight-detailed.html" class="middle-block"><img class="middle-item" alt="" src="{{url('/')}}/gambarac/KAI/w/75/h/75"></a>
                            <div class="travel-title">
                                <h5 class="box-title">{{$kotorg}} ke {{$kotdes}}<small> </small></h5>
                            </div>
                        </figure>
                        <div class="details">
                            <div class="constant-column-3 timing clearfix">
                                <div class="check-in">
                                    <label>Berangkat</label>
                                    <span>{{$tgl_dep}}</span>
                                </div>
                                <div class="duration text-center">
                                    <i class="soap-icon-clock"></i>
                                    <span> </span>
                                </div>
                                <div class="check-out">
                                    <label>Sampai</label>
                                    <span>{{$tgl_dep_tiba}}</span>
                                </div>
                            </div>
                        </div>
                    </article>

                    <h4>Daftar bangku</h4>
                    <dl class="other-details">
                      <?php
                      $dafbangkudep=explode(",",$formgo_dafbangkudep);
                      ?>
                      @foreach($dafbangkudep as $d)
                      @if($d!=null)
                      <?php
                      $bangku=explode("-",$d);
                      ?>
                      <span style="float:left">{{$bangku[1].$bangku[2].$bangku[3]}}</span><BR>
                       @endif
                       @endforeach
                    </dl>
                </div>
                @if($trip=="R")
                <div class="booking-details travelo-box">
                    <h4>Kereta pulang</h4>
                    <article class="flight-booking-details">
                        <figure class="clearfix">
                            <a title="" href="flight-detailed.html" class="middle-block"><img class="middle-item" alt="" src="{{url('/')}}/gambarac/KAI/w/75/h/75"></a>
                            <div class="travel-title">
                                <h5 class="box-title">{{$kotdes}} ke {{$kotorg}}<small> </small></h5>
                            </div>
                        </figure>
                        <div class="details">
                            <div class="constant-column-3 timing clearfix">
                                <div class="check-in">
                                    <label>Berangkat</label>
                                    <span>{{$tgl_ret}}</span>
                                </div>
                                <div class="duration text-center">
                                    <i class="soap-icon-clock"></i>
                                    <span> </span>
                                </div>
                                <div class="check-out">
                                    <label>Sampai</label>
                                    <span>{{$tgl_ret_tiba}}</span>
                                </div>
                            </div>
                        </div>
                    </article>

                    <h4>Daftar bangku</h4>
                    <dl class="other-details">
                      <?php
                      $dafbangkuret=explode(",",$formgo_dafbangkuret);
                      ?>
                      @foreach($dafbangkuret as $d)
                      @if($d!=null)
                      <?php
                      $bangku=explode("-",$d);
                      ?>
                      <span style="float:left">{{$bangku[1].$bangku[2].$bangku[3]}}</span><BR>
                       @endif
                       @endforeach
                    </dl>
                </div>
                @endif

                <div class="travelo-box contact-box">
                  <h4>Butuh bantuan?</h4>
                  <p>Costumer Service kami siap melayani</p>
                  <address class="contact-details">
                      <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                      <br>
                      <a class="contact-email" href="#">{{$infowebsite['email']}}</a>
                  </address>
                </div>
            </div>
        </div>
    </div>
</section>

				@section('akhirbody')
   	<script type="text/javascript">

    $( "#dialog-confirm" ).hide();

    $('.isianlahir').datepicker({
      dateFormat: 'dd-mm-yy',
      yearRange: "-5:+0",
      changeYear: true}).val();

          var sudahoke=0;
    $('#formutama').on('submit', function() {
    		dialogalert();
                    return (sudahoke==1);
        });
        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
