@extends('layouts.'.$namatemplate)
@section('kontenweb')
<?php $dialogpolicy=1;?>
@parent


<div class="container">
  <ul class="breadcrumb">
      <li><a href="{{url('/')}}">Home</a>
      </li>
      <li><a href="{{$alamatbalik}}">Cari Tiket Kereta</a>
      </li>
      <li class="active">Isi data penumpang</li>
  </ul>
    <div class="row row-wrap">
        <div class="col-md-8">
          <form class="simple-from" method="POST" action="kirimdatadiri" id="formutama">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="totalamount" name="totalamount" value="{{Crypt::encrypt($totalamount)}}"  >
            <input type="hidden" id="angkaunik" name="angkaunik" value="{{Crypt::encrypt($angkaunik)}}"  >
            <input type="hidden" id="coba" name="coba" value="{{$org}}"  >
            <input type="hidden" id="org" name="org" value="{{$org}}"  >
            <input type="hidden" id="des" name="des" value="{{$des}}" >
            <input type="hidden" id="TrainNoDep" name="TrainNoDep" value="{{$TrainNoDep}}"  >
            <input type="hidden" id="TrainNoRet" name="TrainNoRet" value="{{$TrainNoRet}}" >
            <input type="hidden" id="keretaDep" name="keretaDep" value="{{$keretaDep}}"  >
            <input type="hidden" id="tgl_dep" name="tgl_dep" value="{{$tgl_dep}}"  >
            <input type="hidden" id="tgl_dep_tiba" name="tgl_dep_tiba" value="{{$tgl_dep_tiba}}"  >
            <input type="hidden" id="trip" name="trip" value="{{$trip}}"  >
            <input type="hidden" id="keretaRet" name="keretaRet" value="{{$keretaRet}}"  >
            <input type="hidden" id="tgl_ret" name="tgl_ret" value="{{$tgl_ret}}"  >
            <input type="hidden" id="tgl_ret_tiba" name="tgl_ret_tiba" value="{{$tgl_ret_tiba}}"  >
            <input type="hidden" id="selectedIDret" name="selectedIDret" value="{{$selectedIDret}}"  >
            <input type="hidden" id="adt" name="adt" value="{{$adt}}"  >
            <input type="hidden" id="chd" name="chd" value="{{$chd}}"  >
            <input type="hidden" id="inf" name="inf" value="{{$inf}}"  >
            <input type="hidden" id="selectedIDdep" name="selectedIDdep" value="{{$selectedIDdep}}"  >
            <input type="hidden" id="dafbangkudep" name="dafbangkudep" value="{{$formgo_dafbangkudep}}" >
            <input type="hidden" id="dafbangkuret" name="dafbangkuret" value="{{$formgo_dafbangkuret}}" >

          <?php $jumpen=$adt+$chd+$inf;$jumadt=0; ?>

            @if($adt>0)
            <h3>Penumpang Dewasa</h3>
            <ul class="list booking-item-passengers">
              @while ($jumadt<$adt)
                <?php $jumadt++; ?>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <label>Panggilan</label>
                            <select name="isiantitadt_{{$jumadt}}" id="isiantitadt_{{$jumadt}}" style="width:100%;" class="full-width">
                              <option value="MR">MR</option>
                              <option value="MS">MS</option>
                              <option value="MRS">MRS</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama Depan</label>
                                <input class="form-control"name="isianfnadt_{{$jumadt}}" id="isianfnadt_{{$jumadt}}" required=""  type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama Belakang</label>
                                <input class="form-control" name="isianlnadt_{{$jumadt}}" id="isianlnadt_{{$jumadt}}" required=""  type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nomor handphone</label>
                                <input class="form-control" name="isianhpadt_{{$jumadt}}" id="isianhpadt_{{$jumadt}}" required="" type="text" />
                            </div>
                        </div>
                    </div>

                </li>
              @endwhile

            </ul>
            @endif
            <?php $jumchd=0; ?>
            @if($chd>0)
            <h3>Penumpang Anak</h3>
            <ul class="list booking-item-passengers">
              @while ($jumchd<$chd)
                <?php $jumchd++; ?>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <label>Panggilan</label>
                            <select name="isiantitchd_{{$jumchd}}" id="isiantitchd_{{$jumchd}}" style="width:100%;" class="full-width">
                              <option value="MSTR">MSTR</option>
                              <option value="MISS">MISS</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama depan</label>
                                <input name="isianfnchd_{{$jumchd}}" id="isianfnchd_{{$jumchd}}" required="" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama belakang</label>
                                <input name="isianlnchd_{{$jumchd}}" id="isianlnchd_{{$jumchd}}" required="" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <input  name="isianbirthchd_{{$jumchd}}" id="isianbirthchd_{{$jumchd}}" required=""  class="form-control" type="text" />
                            </div>
                        </div>
                    </div>

                </li>
              @endwhile

            </ul>
            @endif
            <?php $juminf=0; ?>
            @if($inf>0)
            <h3>Penumpang Bayi</h3>
            <ul class="list booking-item-passengers">
              @while ($juminf<$inf)
                <?php $juminf++; ?>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <label>Panggilan</label>
                            <select name="isiantitinf_{{$juminf}}" id="isiantitinf_{{$juminf}}" style="width:100%;" class="full-width">
                              <option value="MSTR">MSTR</option>
                              <option value="MISS">MISS</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama depan</label>
                                <input name="isianfninf_{{$juminf}}" id="isianfninf_{{$juminf}}" required="" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama belakang</label>
                                <input name="isianlninf_{{$juminf}}" id="isianlninf_{{$juminf}}" required="" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <input  name="isianbirthinf_{{$juminf}}" id="isianbirthinf_{{$juminf}}" required=""  class="form-control" type="text" />
                            </div>
                        </div>
                    </div>
                </li>
              @endwhile

            </ul>
            @endif

            <h3>Data Kontak</h3>
            <ul class="list booking-item-passengers">
              <li>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama</label>
                                <input class="form-control" id="isiancpname" name="isiancpname" required="" type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Telepon</label>
                                <input class="form-control" id="isiancptlp" name="isiancptlp" required=""  type="text" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" type="email"  id="isiancpmail" name="isiancpmail" required="" />
                            </div>
                        </div>
                    </div>

                </li>

            </ul>

            <input class="btn btn-primary" type="submit" value="Confirm Booking" />
        </form>

        </div>
        <div class="col-md-4">
          <div class="booking-item-payment">
              <header class="clearfix">
                  <h5 class="mb0">{{$kotorg}} ke {{$kotdes}}</h5>
              </header>
              <ul class="booking-item-payment-details">
                  <li>

                      <div class="booking-item-payment-flight">
                        <p>{{$keretaDep}} {{$TrainNoDep}}</p>
                        <?php
                        $detstationorg=station_detailByCode($org);
                        $detstationdes=station_detailByCode($des);
                        ?>
                        <div class="row">
                              <div class="col-md-9">
                                  <div class="booking-item-flight-details">
                                      <div class="booking-item-departure">
                                          <h5 class="booking-item-destination">{{date("H:i A",strtotime($tgl_dep))}}</h5>
                                          <p class="booking-item-destination">{{date("D, M d",strtotime($tgl_dep))}}</p>
                                          <p class="booking-item-destination">{{$detstationorg['st_name']}}</p>
                                      </div>
                                      <div class="booking-item-arrival">
                                        <h5 class="booking-item-destination">{{date("H:i A",strtotime($tgl_dep_tiba))}}</h5>
                                        <p class="booking-item-destination">{{date("D, M d",strtotime($tgl_dep_tiba))}}</p>
                                        <p class="booking-item-destination">{{$detstationdes['st_name']}}</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <p>Durasi: {{lamajalan($tgl_dep,$tgl_dep_tiba,'%02d h, %02d m')}}</p>
                          <p>Daftar bangku:</p>
                          <?php
                          $dafbangkudep=explode(",",$formgo_dafbangkudep);
                          ?>
                          @foreach($dafbangkudep as $d)
                          @if($d!=null)
                          <?php
                          $bangku=explode("-",$d);
                          ?>
                          <p>- {{$bangku[1].$bangku[2].$bangku[3]}}</p>
                          @endif
                          @endforeach
                      </div>
                  </li>
              </ul>

              @if($trip=="R")
              <header class="clearfix">
                  <h5 class="mb0">{{$kotdes}} ke {{$kotorg}}</h5>
              </header>
              <ul class="booking-item-payment-details">
                  <li>

                      <div class="booking-item-payment-flight">

                        <p>{{$keretaRet}} {{$TrainNoRet}}</p>
                        <div class="row">
                              <div class="col-md-9">
                                  <div class="booking-item-flight-details">
                                      <div class="booking-item-departure">
                                          <h5 class="booking-item-destination">{{date("H:i A",strtotime($tgl_ret))}}</h5>
                                          <p class="booking-item-destination">{{date("D, M d",strtotime($tgl_ret))}}</p>
                                          <p class="booking-item-destination">{{$detstationdes['st_name']}}</p>
                                      </div>
                                      <div class="booking-item-arrival">
                                        <h5 class="booking-item-destination">{{date("H:i A",strtotime($tgl_ret_tiba))}}</h5>
                                        <p class="booking-item-destination">{{date("D, M d",strtotime($tgl_ret_tiba))}}</p>
                                        <p class="booking-item-destination">{{$detstationorg['st_name']}}</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                           <p>Durasi: {{lamajalan($tgl_ret,$tgl_ret_tiba,'%02d h, %02d m')}}</p>
                           <p>Daftar bangku:</p>
                           <?php
                           $dafbangkuret=explode(",",$formgo_dafbangkuret);
                           ?>
                           @foreach($dafbangkuret as $d)
                           @if($d!=null)
                           <?php
                           $bangku=explode("-",$d);
                           ?>
                           <p>- {{$bangku[1].$bangku[2].$bangku[3]}}</p>
                           @endif
                           @endforeach
                      </div>
                  </li>
              </ul>
              @endif

              <p class="booking-item-payment-total">Total biaya: <span>{{rupiah($totalamount)}}</span>
              </p>
          </div>
        </div>
    </div>
    <div class="gap"></div>
</div>



		@section('akhirbody')
    <script type="text/javascript">
$( "#dialog-confirm" ).hide();
    $('.isianlahir').datepicker({
      dateFormat: 'dd-mm-yy',
      yearRange: "-5:+0",
      changeYear: true}).val();
    $( "#tombolsubmit" ).click(function() {
    //  alert( "Handler for .click() called." );
      $('#isiancpname').val($('#cpname').val());
      $('#coba').val($('#cptlp').val());
      $('#isiancptlp').val($('#cptlp').val());
      $('#isiancpmail').val($('#cpmail').val());


  $( "#formutama" ).submit();
    });

   <?php $jumchd=0; ?>
  @while ($jumchd<$chd)
  <?php $jumchd++; ?>
  $("#isianbirthchd_{{$jumchd}}").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });
  @endwhile

  <?php $juminf=0; ?>
  @while ($juminf<$inf)
  <?php $juminf++; ?>
  $("#isianbirthinf_{{$juminf}}").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });
  @endwhile

    var sudahoke=0;
    $('#formutama').on('submit', function() {


          dialogalert();
                return (sudahoke==1);
    });



        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }
		</script>
		@endsection

		@endsection
