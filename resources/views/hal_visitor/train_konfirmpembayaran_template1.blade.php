@extends('layouts.'.$namatemplate)
@section('sebelumtitel')
		<script type="text/javascript" src="{{ URL::asset('asettemplate1/js/jquery.maskMoney.min.js')}}"></script>
		<link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')

<!-- INNER-BANNER -->
<div class="inner-banner ">
	<img class="center-image" src="{{ URL::asset('asettemplate1/img/detail/bg_5.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
		  			<ul class="banner-breadcrumb color-white clearfix">
							<li><a class="link-blue-2" href="{{ url('/') }}/">home</a> /</li>
			  				<li><a class="link-blue-2" href="{{ url('/') }}/train">kereta</a> /</li>
							<li><span class="color-red-3">informasi tagihan</span></li>
		  			</ul>
		  			<h2 class="color-white">Trx no. {{$notrx}}</h2>
  				</div>
			</div>
		</div>
	</div>

</div>

<!-- DETAIL WRAPPER -->
<div class="detail-wrapper">
	<div class="container">
       	<div class="row padd-90">
					<?php $otomatiscari=0;?>
					<div class="col-xs-12 col-sm-4 col-md-3 nomobile">
		        <div class="sidebar style-2 clearfix">
		        <div class="sidebar-block">
		          <h4 class="sidebar-title color-dark-2">search</h4>
							<div class="search-inputs">
		            <div class="form-block clearfix">
		              <h5>Asal</h5><div class="input-style b-50 color-3">
		              <select class="input-style selektwo"placeholder="Check In"  name="asal" id="pilasal">
		                   <option value=""></option>
		                   @foreach($dafstat as $area)
		                  <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $org==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
		                  @endforeach
		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Tujuan</h5>
		                <div class="input-style">
		                <select class="input-style selektwo" name="tujuan" id="piltujuan">
		                   <option value=""></option>
		                   @foreach($dafstat as $area)
		                  <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $des==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
		                  @endforeach
		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Rencana</h5>
		                <div class="input-style">
		                <select class="mainselection" id="pilrencana" name="pilrencana" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">
		                  <option value="O" <?php if($otomatiscari==1 && $trip=="O"){print("selected=\"selected\"");} ?>>Sekali Jalan</option>
		                  <option value="R" <?php if($otomatiscari==1 && $trip=="R"){print("selected=\"selected\"");} ?>>Pulang Pergi</option>

		                </select> </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Tanggal Berangkat</h5>
		                <div class="input-style">
		                  <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglberangkat)){print($tglberangkat);}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div id="tny_ret" class="form-block clearfix">
		              <h5 style="padding-bottom:10px;">Tanggal Pulang</h5>
		                <div class="input-style">
		                  <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglpulang)){print($tglpulang);}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
		                </div>
		            </div>
		            <div class="form-block clearfix">
		              <h5>Dewasa</h5>
		                <div class="input-style">
		                  <select class="mainselection" id="adt" name="adt" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=1; $op<=7;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>

		            <div class="form-block clearfix">
		              <h5>Bayi</h5>
		                <div class="input-style">
		                  <select class="mainselection"id="inf" name="inf" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

		                    <?php for($op=0; $op<=4;$op++){
		                      print("<option value=\"$op\"");
		                      if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
		                      print(">$op</option>");
		                    }
		                    ?>

		                  </select> </div>
		            </div>
		          </div>
		          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
		        </div>


		        </div>
		      </div>
       		<div class="col-xs-12 col-md-9">
						<div class="detail-content-block">
							@if (session()->has('notiftagihan'))
							@if(isset(session('notiftagihan')['created']))
							<div class="detail-content-block">
								<h3 class="small-title">Transaksi berhasil dibuat</h3>
								<div class="confirm-label bg-dr-blue-2 radius-5">
									<img class="confirm-img" src="{{ URL::asset('asettemplate1/img/thx_icon.png')}}" alt="">
									<div class="confirm-title color-white">Terimakasih. Silahkan melakukan pembayaran sesuai dengan jumlah tagihan</div>
									<div class="confirm-text color-white-light">Jangan lupa untuk upload bukti pembayaran</div>
									<a href="#" onclick="window.print();" class="confirm-print c-button b-40 bg-white hv-white-o">print details</a>
								</div>
							</div>
							@endif

							@if(isset(session('notiftagihan')['uploaded']))
							<div class="detail-content-block">
								<h3 class="small-title">Upload bukti pembayaran berhasil</h3>
								<div class="confirm-label bg-dr-blue-2 radius-5">
									<img class="confirm-img" src="{{ URL::asset('asettemplate1/img/thx_icon.png')}}" alt="">
									<div class="confirm-title color-white">Terimakasih. Bukti pembayaran Anda sudah terkirim ke sistem.</div>
									<div class="confirm-text color-white-light">Silahkan tunggu email pemberitahuan dari kami.</div>
									<a href="#" onclick="window.print();"  class="confirm-print c-button b-40 bg-white hv-white-o">print details</a>
								</div>
							</div>
							@endif

							@endif
							<h4 class="small-title">Kereta dari {{$st_cityorg}} (Stasiun {{$st_nameorg}}) ke {{$st_citydes}} (Stasiun {{$st_namedes}})</h4>
							<div class="table-responsive">
									<table class="table style-1 type-2 striped">
											<tbody>
												<tr>
														<td class="table-label color-black">Kereta</td>
														<td class="table-label color-dark-2"><strong>{{$keretaDep}} nomor {{$TrainNoDep}}</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Berangkat</td>
														<td class="table-label color-dark-2"><strong>{{$tgl_dep}}</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Tiba</td>
														<td class="table-label color-dark-2"><strong>{{$tgl_dep_tiba}}</strong></td>
												</tr>
												<tr>
														<td class="table-label color-black">Daftar bangku</td>
														<td class="table-label color-dark-2">
				                      <?php

															$a=0;
				                      ?>
														@foreach($dafseatdep as $trans)
			                      @if($trans!=null)
														<?php if($a==1){print(",");}else{$a=1;}?>
														{{$trans->gerbong.$trans->nomorgerbong.$trans->bangku}}

														 @endif
														 @endforeach</td>
												</tr>

											</tbody>
									</table>
								</div>
						 </div>

						 @if($trip=="R")
						 <div class="detail-content-block">
 							<h4 class="small-title">Kereta dari {{$st_citydes}} (Stasiun {{$st_namedes}}) ke {{$st_cityorg}} (Stasiun {{$st_nameorg}})</h4>
 							<div class="table-responsive">
 									<table class="table style-1 type-2 striped">
 											<tbody>
 												<tr>
 														<td class="table-label color-black">Kereta</td>
 														<td class="table-label color-dark-2"><strong>{{$keretaRet}} nomor {{$TrainNoRet}}</strong></td>
 												</tr>
												<tr>
 														<td class="table-label color-black">Berangkat</td>
 														<td class="table-label color-dark-2"><strong>{{$tgl_ret}}</strong></td>
 												</tr>
												<tr>
 														<td class="table-label color-black">Tiba</td>
 														<td class="table-label color-dark-2"><strong>{{$tgl_ret_tiba}}</strong></td>
 												</tr>
												<tr>
														<td class="table-label color-black">Daftar bangku</td>
														<td class="table-label color-dark-2">
				                      <?php

															$a=0;
				                      ?>
														@foreach($dafseatdep as $trans)
			                      @if($trans!=null)
														<?php if($a==1){print(",");}else{$a=1;}?>
														{{$trans->gerbong.$trans->nomorgerbong.$trans->bangku}}

														 @endif
														 @endforeach</td>
												</tr>
 											</tbody>
 									</table>
 								</div>
 						 </div>
						 @endif

<div >
					@if($adt>0)
					 <?php $nomurut=0; foreach($dafpendewasa as $pen){ $nomurut+=1;?>
				<div class="detail-content-block">
					<h4 class="small-title">Dewasa #{{$nomurut}}</h4>
					<div class="table-responsive">
					    <table class="table style-1 type-2 striped">
					      	<tbody>
						        <tr>
						          	<td class="table-label color-black">Gelar</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->tit}}</strong></td>
						        </tr>
										<tr>
						          	<td class="table-label color-black">Nama</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->fn}} {{$pen->ln}}</strong></td>
						        </tr>
						        <tr>
						          	<td class="table-label color-black">Handphone:</td>
						          	<td class="table-label color-dark-2"><strong>{{$pen->hp}}</strong></td>
						        </tr>

					      	</tbody>
					    </table>
				    </div>
				</div>
				<?php }?>
				@endif

				@if($chd>0)
				 <?php $nomurut=0; foreach($dafpenanak as $dataanak){ $nomurut+=1;?>
			<div class="detail-content-block">
				<h4 class="small-title">Anak #{{$nomurut}}</h4>
				<div class="table-responsive">
						<table class="table style-1 type-2 striped">
								<tbody>
									<tr>
											<td class="table-label color-black">Gelar</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->tit}}</strong></td>
									</tr>
									<tr>
											<td class="table-label color-black">Nama</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->fn}} {{$dataanak->ln}}</strong></td>
									</tr>
									<tr>
											<td class="table-label color-black">Lahir:</td>
											<td class="table-label color-dark-2"><strong>{{$dataanak->birth}}</strong></td>
									</tr>

								</tbody>
						</table>
					</div>
			</div>
			<?php }?>
			@endif

			@if($inf>0)
			 <?php $nomurut=0; foreach($dafpenbayi as $databayi){ $nomurut+=1;?>
		<div class="detail-content-block">
			<h4 class="small-title">Bayi #{{$nomurut}}</h4>
			<div class="table-responsive">
					<table class="table style-1 type-2 striped">
							<tbody>
								<tr>
										<td class="table-label color-black">Gelar</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->tit}}</strong></td>
								</tr>
								<tr>
										<td class="table-label color-black">Nama</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->fn}} {{$databayi->ln}}</strong></td>
								</tr>
								<tr>
										<td class="table-label color-black">Lahir:</td>
										<td class="table-label color-dark-2"><strong>{{$databayi->birth}}</strong></td>
								</tr>

							</tbody>
					</table>
				</div>
		</div>
		<?php }?>
		@endif

				<div class="detail-content-block">
					<h4 class="small-title">Informasi Tagihan</h4>
					<div class="table-responsive">
							<table class="table style-1 type-2 striped">
									<tbody>
										<tr>
												<td class="table-label color-black">No. Transaksi:</td>
												<td class="table-label color-dark-2"><strong>{{$notrx}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$labelbiaya}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Batas pembayaran:</td>
												<td class="table-label color-dark-2"><strong>{{DateToIndo($tgl_bill_exp)}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Status tagihan:</td>
												<td class="table-label color-dark-2"><strong>{{$statustransaksi}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">Tagihan untuk:</td>
												<td class="table-label color-dark-2"><strong>{{$cpname}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">EMAIL:</td>
												<td class="table-label color-dark-2"><strong>{{$cpmail}}</strong></td>
										</tr>
										<tr>
												<td class="table-label color-black">NOMOR TELEPHONE:</td>
												<td class="table-label color-dark-2"><strong>{{$cptlp}}</strong></td>
										</tr>

									</tbody>
							</table>
						</div>
				</div>

</div>

@if(count($dafbuk)>0)
<?php $nombukti=0;?>
Daftar bukti pembayaran
	<div class="custom-panel bg-grey-2 radius-4">
		@foreach($dafbuk as $bukti)
		<?php $nombukti+=1;?>
		Bukti #{{$nombukti}}
		<div class="form-group">
			<table class="table">
					<tbody>
						<tr>
							@if($bukti->namafile!=null)
								<td class="" width="50"><img src="{{ url('/') }}/uploads/images/{{$bukti->namafile}}" alt="Bukti Pembayaran" style="width:200px;height:200px;"></td>
							@endif
								<td class="table-label color-dark-2 nomobile">Tanggal upload : {{$bukti->tanggal}}<BR>Dari bank : {{$bukti->rekasal_bank}}<BR>Atas nama : {{$bukti->rekasal_napem}}<BR>Pembayaran ke bank : {{$bukti->rektuju_bank}}</td>
						</tr>
					</tbody>
			</table>
			<div class="justmobile">
				Tanggal upload : {{$bukti->tanggal}}<BR>Dari bank : {{$bukti->rekasal_bank}}<BR>Atas nama : {{$bukti->rekasal_napem}}<BR>Pembayaran ke bank : {{$bukti->rektuju_bank}}
			</div>

		</div>
		<br>
		@endforeach
	</div>
	@endif
@if($status==0 || $status==1 || $status==3)
@if($status==1 || $status==3)
<span id="tommintaupload" onclick="$('#formuploadbukti').show();$(this).hide();" style="color: #ff6600; cursor:pointer;">Upload ulang bukti pembayaran</span>
@endif
				<div class="detail-content-block" id="formuploadbukti">
					<div class="simple-text">
						<h4>Upload bukti pembayaran</h4>
						<form role="form" method="post" action="kirimbukti/{{$notrx}}" enctype = "multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label for="exampleInputEmail1">Dari Bank</label>
								<input name="bank" required="" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Atas Nama</label>
								<input name="napem" required=""  type="text" class="form-control" >
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Ke Rekening</label>
								<select class="form-control" name="rektuju" id="rektuju">
									 @foreach($dafrek as $rek)
									<option value="{{ $rek->id }}" >{{ $rek->bank }} - {{ $rek->norek }} - {{ $rek->napem }}</option>
									@endforeach
								</select>
							</div>

						<div class="form-group">
							<label for="exampleInputFile">Gambar Bukti Transfer</label>
							<input name="foto" type="file" id="exampleInputFile">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							@if($status==1 || $status==3)
							<button type="button" onclick="$('#formuploadbukti').hide();$('#tommintaupload').show();" class="btn btn-danger">Cancel</button>
							@endif
						</div>
					</form>

					</div>
				</div>
					@endif
					<div class="row" style="margin-top:30px;">
					<a  class="c-button b-40 bg-red-3 hv-red-3-o" href="{{URL('/')}}/train/transaksi/{{$notrx}}/pdf" >Cetak PDF</a>
					</div>
       		</div>

       	</div>
	</div>
</div>

@section('akhirbody')
<script type="text/javascript">

@if($status==1)
$('#formuploadbukti').hide();
@endif
	    <!--
	    $('#tanggalbayar').datepicker({ dateFormat: 'dd-mm-yy',
	   }).val();
	   -->
		 //variabel

		 var hargapulang="0";
		 var hargapergi="0";
		 var nilpil=0;
		 var idpilper="";
		 var idpilret="";
		 var statasal="BD";
		 var kotasal="Bandung";
		 var labelasal="";
		 var kottujuan="Gambir";
		 var stattujuan="GMR";
		 var labeltujuan="";
		 var pilrencana="O";
		 var pilrencana2="O";
		 var tglber_d="";
		 var tglber_m="";
		 var tglber_y="";

		 var tglpul_d="";
		 var tglpul_m="";
		 var tglpul_y="";

		 var tglberangkat="<?php echo date("Y-m-d");?>";
		 $('#formgo_tgl_dep').val(tglberangkat);
		 $('#formgo_tgl_deppilihan').val(tglberangkat);
		 var tglpulang="<?php echo date("Y-m-d");?>";
		 $('#formgo_tgl_ret').val(tglpulang);
		 $('#formgo_tgl_retpilihan').val(tglpulang);



		 var jumadt="1";
		 var jumchd="0";
		 var juminf="0";



		 var urljadwal="{{ url('/') }}/train/otomatiscari/";
		 var urljadwalb=urljadwal;
		 var dafIDPergi = [];
		 var dafBiayaPergi = [];
		 var dafHTMLPergi = [];
		 var dafIDPulang = [];
		 var dafBiayaPulang = [];
		 var dafHTMLPulang = [];


		 $('#tomgoret').hide();


		 $( "#tompilulang" ).click(function() {
			 nilpil=0;
			 $('#divpilihnextorulang').hide();
			 $('#sisikiri').show('slow');
			 $('#barissorting').show('slow');
		 });
		 $( "#tomgoone" ).click(function() {
		 $( "#formgo" ).submit();
		 });
		 $( "#tomgoret" ).click(function() {
		 $( "#formgo" ).submit();
		 });
		 $( "#tomteskir" ).click(function() {
			 $( "#formgo" ).submit();
		 });


		 $('#adt').on('change', function() {
		 jumadt=this.value;
		 $('#formgo_adt').val(jumadt);

		 });
		 $('#chd').on('change', function() {
		 jumchd=this.value;
		 $('#formgo_chd').val(jumchd);

		 });
		 $('#inf').on('change', function() {
		 juminf=this.value;
		 $('#formgo_inf').val(juminf);

		 });


			 $('.selektwo').select2();
			 <?php if(($otomatiscari==1 && $trip=="O")||$otomatiscari==0){?>
			 $('#tny_ret').hide();
			 <?php }?>


			 $("#tglberangkat").datepicker({
	         dateFormat: 'dd-mm-yy',
	         changeMonth: true,
	         changeYear: true,
	         minDate: new Date(),
	         maxDate: '+2y'
	     });

	     $("#tglpulang").datepicker({
	         dateFormat: 'dd-mm-yy',
	         minDate: new Date(),
	         changeMonth: true,
	         changeYear: true
	     });
		 $('#tglberangkat').on('change', function() {

		 var values=this.value.split('-');
		 //alert(this.value);
		 tglber_d=values[0];
		 tglber_m=values[1];
		 tglber_y=values[2];

		 tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

		 $('#formgo_tgl_deppilihan').val(tglberangkat);
		 var selectedDate = new Date(tglberangkat);
	 	var msecsInADay = 86400000;
	 	var endDate = new Date(selectedDate.getTime());
	 					$("#tglpulang").datepicker( "option", "minDate", endDate );
	 					$("#tglpulang").datepicker( "option", "maxDate", '+2y' );
	 							settglpulang($("#tglpulang").val());

		 });
		 function settglpulang(val){

		 			var values=val.split('-');
		 			//alert(this.value);
		 			tglpul_d=values[0];
		 			tglpul_m=values[1];
		 			tglpul_y=values[2];

		 			tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
		 			$('#formgo_tgl_retpilihan').val(tglpulang);
		 }
		 $('#tglpulang').on('change', function() {

			 settglpulang(this.value);

		 });

		 $('#pilasal').on('change', function() {
			 var values=this.value.split('-');
			 var kotas=values[1];
			 kotasal=values[0];
			 //alert(kotas);
			 statasal=kotas;
			 labelasal=this.value;
			 $('#formgo_labelorg').val(labelasal);
			 $('#formgo_org').val(statasal);
			 $('#formgo_kotorg').val(kotasal);

		 });
		 $('#piltujuan').on('change', function() {
			 var values=this.value.split('-');
			 var kottuj=values[1];
			 kottujuan=values[0];
			 //alert(kottuj);
			 stattujuan=kottuj;
			 labeltujuan=this.value;
			 $('#formgo_labeldes').val(labeltujuan);
			 $('#formgo_des').val(kottuj);
			 $('#formgo_kotdes').val(kottujuan);

		 });
		 $('#pilrencana').on('change', function() {
			 //alert( this.value );
			 pilrencana=this.value;
			 $('#formgo_trip').val(pilrencana);
			 var tny_ret = document.getElementById("tny_ret").value;

		 if(this.value=="O"){
				 $('#tny_ret').hide();
			}else{
				 $('#tny_ret').show();
			}


		 });

	 function convertToRupiah(angka){
	 var rupiah = '';
	 var angkarev = angka.toString().split('').reverse().join('');
	 for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	 return rupiah.split('',rupiah.length-1).reverse().join('');
	 }

		 function cari(){
		 nilpil=0;
		 if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){

		 $("#barissorting").show('slow');
		 $("#sisikanan").hide();
		 $('#divpilihnextorulang').hide();
		 $('#tomgoret').hide();
		 //  alert("Memulai pencarian");
						 //  $('#hasilkirim').html("<b>Hello world!</b>");
						 if(pilrencana=="O"){
								 $("#sisikanan").hide('slow');
								 $("#sisikiri").show('slow');
									 //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
									 //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
							 }else{
								 $("#sisikiri").show('slow');
								 //$("#sisikanan").show('slow');
									 //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
									 //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
									 //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
									 //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
							 }
						 urljadwalb=urljadwal;
						 urljadwalb+=""+statasal;
						 urljadwalb+="/"+stattujuan;
						 urljadwalb+="/"+pilrencana;
						 urljadwalb+="/"+tglberangkat;
						 urljadwalb+="/"+tglpulang;
						 urljadwalb+="/"+jumadt;
						 urljadwalb+="/"+jumchd;
						 urljadwalb+="/"+juminf;
					   //alert(urljadwalb);
		 				 $(location).attr('href', urljadwalb)
				 }else{
					 $('#bahanmodal').dialog({ modal: true });
					 // $( "#bahanmodal" ).show();
				 //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
				 }
		 }
</script>
@endsection

@endsection
