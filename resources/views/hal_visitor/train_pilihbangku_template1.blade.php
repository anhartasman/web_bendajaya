<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <style>
      <?php
      $hur['A']=1;
      $hur['B']=2;
      $hur['C']=3;
      $hur['D']=4;
      $hur['E']=5;
      $hurnom[0]="";
      $hurnom[1]="A";
      $hurnom[2]="B";
      $hurnom[3]="C";
      $hurnom[4]="D";
      $hurnom[5]="E";
      $namakelas=array(-1=>"asd",0=>"seat",1=>"seat selectedSeat",2=>"seat");
      ?>
      <?php
      $dafwag=$seatdep['seat_map'];
      ?>
      @if (is_array($dafwag))
      @foreach($dafwag as $wag)
      <?php
      $bahanid="dep".$wag['kode_wagon'].$wag['no_wagon'];
      ?>
      #holder{{$bahanid}}{
      @if($wag['jml_col']==4)
      height:220px;
      @elseif($wag['jml_col']==5)
      height:260px;
      @endif
      width:750px;

      border:0px solid #A4A4A4;
      margin-left:0px;
      }
      #place{{$bahanid}} {
      position:relative;
      margin:7px;
      }
      #place{{$bahanid}} a{
      font-size:0.6em;
      }
      #place{{$bahanid}} li
      {
       list-style: none outside none;
       position: absolute;
      }
      #place{{$bahanid}} li:hover
      {
      background-color:yellow;
      }

      @if($wag['jml_col']==4)#place{{$bahanid}} .row-3, #place{{$bahanid}} .row-4 @endif @if($wag['jml_col']==5)#place{{$bahanid}} .row-4, #place{{$bahanid}} .row-5 @endif{
      margin-top:33px;
      }
      #seatDescription{{$bahanid}} li{
      verticle-align:middle;
      list-style: none outside none;
      padding-left:35px;
      height:35px;
      float:left;
      }
      @endforeach
      @endif

      <?php
      $dafwag=$seatret['seat_map'];
      ?>
      @if (is_array($dafwag))
      @foreach($dafwag as $wag)
      <?php
      $bahanid="ret".$wag['kode_wagon'].$wag['no_wagon'];
      ?>
      #holder{{$bahanid}}{
      @if($wag['jml_col']==4)
      height:220px;
      @elseif($wag['jml_col']==5)
      height:260px;
      @endif
      width:750px;

      border:0px solid #A4A4A4;
      margin-left:0px;
      }
      #place{{$bahanid}} {
      position:relative;
      margin:7px;
      }
      #place{{$bahanid}} a{
      font-size:0.6em;
      }
      #place{{$bahanid}} li
      {
       list-style: none outside none;
       position: absolute;
      }
      #place{{$bahanid}} li:hover
      {
      background-color:yellow;
      }

      @if($wag['jml_col']==4)#place{{$bahanid}} .row-3, #place{{$bahanid}} .row-4 @endif @if($wag['jml_col']==5)#place{{$bahanid}} .row-4, #place{{$bahanid}} .row-5 @endif{
      margin-top:33px;
      }
      #seatDescription{{$bahanid}} li{
      verticle-align:middle;
      list-style: none outside none;
      padding-left:35px;
      height:35px;
      float:left;
      }
      @endforeach
      @endif

      .seat{
      background:url("{{ URL::asset('img/available_seat_img.gif')}}") no-repeat scroll 0 0 transparent;
      height:33px;
      width:33px;
      display:block;
      }
      .selectedSeat
      {
      background-image:url("{{ URL::asset('img/booked_seat_img.gif')}}");
      }
      .selectingSeat
      {
      background-image:url("{{ URL::asset('img/selected_seat_img.gif')}}");
      }
      .bangkuNope
      {
      background-image:url("{{ URL::asset('img/booked_seat_img.gif')}}");
      }
      .bangkuYes
      {
      background-image:url("{{ URL::asset('img/selected_seat_img.gif')}}");
      }
      </style>

@endsection
@section('kontenweb')
<div id="bahanmodal" title="Basic dialog">
  INI ISI MODAL
</div>

<form class="contact-form"id="formgo" action="{{url('/')}}/train/train_isiform" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="{{$kotorg}} ({{$org}})">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="{{$kotorg}}">
<input type="hidden" id="formgo_org" name="formgo_org" value="{{$org}}">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="{{$kotdes}} ({{$des}})">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="{{$kotdes}}">
<input type="hidden" id="formgo_des" name="formgo_des" value="{{$des}}">
<input type="hidden" id="formgo_TrainNoDep" name="formgo_TrainNoDep" value="{{$TrainNoDep}}">
<input type="hidden" id="formgo_TrainNoRet" name="formgo_TrainNoRet" value="{{$TrainNoRet}}">
<input type="hidden" id="formgo_keretaDep" name="formgo_keretaDep" value="{{$keretaDep}}">
<input type="hidden" id="formgo_keretaRet" name="formgo_keretaRet" value="{{$keretaRet}}">
<input type="hidden" id="formgo_hargaDep" name="formgo_hargaDep" value="{{Crypt::encrypt($formgo_hargaDep)}}">
<input type="hidden" id="formgo_hargaRet" name="formgo_hargaRet" value="{{Crypt::encrypt($formgo_hargaRet)}}">
<input type="hidden" id="formgo_trip" name="formgo_trip" value="{{$trip}}">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" value="{{$tgl_dep}}">
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" value="{{$tgl_dep_tiba}}">
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" value="{{$tgl_ret}}">
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" value="{{$tgl_ret_tiba}}">
<input type="hidden" id="formgo_adt" name="formgo_adt" value="{{$adt}}">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="{{$chd}}">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="{{$inf}}">
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" value="{{$formgo_tgl_deppilihan}}" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" value="{{$formgo_tgl_retpilihan}}" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" value="{{$selectedIDdep}}" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" value="{{$selectedIDret}}" >
<input type="hidden" id="formgo_dafbangkudep" name="formgo_dafbangkudep" >
<input type="hidden" id="formgo_dafbangkuret" name="formgo_dafbangkuret" >
</form>

<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('img/inner_banner_train.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{url('/')}}">Home</a> /</li>
				<li><a class="link-blue-2" href="{{$alamatbalik}}">Kereta</a></li>
			</ul>
			<h2 class="color-white">Tiket Kereta</h2>
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar bg-white clearfix">
        <div class="sidebar-block">
          <h4 class="sidebar-title color-dark-2">Kereta</h4>
          <div class="search-inputs">
            <div class="form-block clearfix">
              <h5>Asal</h5><div>
              {{$kotorg}} </div>
            </div>
            <div class="form-block clearfix">
              <h5>Tujuan</h5>
                <div  >
                {{$kotdes}} </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Berangkat</h5>
                <div>
                {{$tgl_dep}}
                </div>
            </div>
            @if($trip=="R")
            <div id="tny_ret" class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Pulang</h5>
              <div>
              {{$tgl_ret}}
              </div>
            </div>
            @endif
            <div class="form-block clearfix">
              <h5>Dewasa</h5>
                <div>
                  {{$adt}}
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Bayi</h5>
              <div>
                {{$adt}}
              </div>
            </div>
          </div>
        </div>


        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-9">

      <!--
        <div class="list-header clearfix" id="barissorting">
          <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
            <div style="color:BLACK;">
             Urut berdasarkan
             </div>
          </div>
            <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
              <div class="drop"style="color:BLACK;">
               <b>-</b>
                <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                <span>
                  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">Harga terkecil</a>
                  <a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">Harga terbesar</a>
                  <a href="#" onclick="sortAsc('data-waktu')"style="color:BLACK;">Jam terdekat</a>
                  <a href="#" onclick="sortDesc('data-waktu')"style="color:BLACK;">Jam terakhir </a>
                </span>
               </div>
            </div>

        </div>
      -->
      <div id="titikup"></div>
        <div  id="sisikiri">
          <div class="list-header clearfix"  >

               <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; " >Pilih tempat duduk keberangkatan</h4>

          </div>
@if($errors->has())
            <div class="list-header clearfix" style="background-color: #ff6600;">
              @foreach ($errors->all() as $error)
              <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" >
              {{ $error }}</h4>

                 @endforeach
               </div>
             @endif
             <?php
             $dafwag=$seatdep['seat_map'];
             ?>
             @foreach($dafwag as $wag)
             <?php
             $bahanid="dep".$wag['kode_wagon'].$wag['no_wagon'];

             $bahseatdep=array();
             $muncul=1;
              foreach ($wag['avl'] as $avl){
               $bahseatdep[$hur[$avl[1]]][$avl[0]]=$avl[3];
               if($muncul==0){
                 if($avl[3]==0){
                   $muncul=1;
                 }
               }
             }
             if($muncul==1){
             ?>
               <div class="clearfix" style="background-color: #ff6600;">

                    <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Gerbong {{$wag['kode_wagon']}} {{$wag['no_wagon']}}</h4>

               </div>
             <div class="clearfix table-responsive" id="dafpulang">
             <div id="holder{{$bahanid}}">
             <ul  id="place{{$bahanid}}">

             <?php for($i=0; $i<=$wag['jml_col']; $i++){
                   for($j=0;$j<=$wag['jml_row']; $j++){
                     $seatNo=($i+$j*$wag['jml_col']+1);
                     if(! isset($bahseatdep[$i][$j])){
                       $bahseatdep[$i][$j]=-1;
                     }
                     if($j!=0 && $i!=0 && $bahseatdep[$i][$j]!=-1){
                       $iddasar="1-".$wag['kode_wagon']."-".$wag['no_wagon']."-".$hurnom[$i].$j;
             ?>
             <li id="{{$iddasar}}" onclick="pilihbangkudep('{{$iddasar}}')" class="{{$namakelas[$bahseatdep[$i][$j]]}} row-{{$i}} col-{{$j}} " style="top:{{($i)*35}}px;left:{{($j)*35}}px">
             <a title="col{{$j}},row{{$i}}">{{$hurnom[$i]}}{{$j}}</a>
             </li>
             <?php }else if($j==0){ ?>
             <li class="row-{{$i}} col-{{$j}} " style="top:{{($i)*35}}px;left:{{($j)*35}}px">
             {{$hurnom[$i]}}
             </li>
             <?php }else if($i==0){ ?>
               <li  style="top:{{($i)*35}}px;left:{{($j)*35}}px">
               {{$j}}
               </li>
             <?php }}} ?>
             </ul>
             </div>

             </div>
             <?php } ?>
             @endforeach
             <div class="list-header clearfix" style="background-color: #ff6600;">

                <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" ></h4>

             </div>



        </div>
        <div  id="sisikanan">
        @if($trip=="R")
          <div class="list-header clearfix"  >

               <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; " >Pilih tempat duduk untuk pulang</h4>

          </div>
             <?php
             $dafwag=$seatret['seat_map'];
             ?>
             @if (is_array($dafwag))
             @foreach($dafwag as $wag)
             <?php
             $bahanid="ret".$wag['kode_wagon'].$wag['no_wagon'];


             $bahseatret=array();
             $muncul=1;
             foreach ($wag['avl'] as $avl){
               $bahseatret[$hur[$avl[1]]][$avl[0]]=$avl[3];
               if($muncul==0){
                 if($avl[3]==0){
                   $muncul=1;
                 }
               }
             }
             if($muncul==1){
             ?>
               <div class="clearfix" style="background-color: #ff6600;">

                    <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Gerbong {{$wag['kode_wagon']}} {{$wag['no_wagon']}}</h4>

               </div>
             <div class="clearfix table-responsive" id="dafpulang">
             <div id="holder{{$bahanid}}">
             <ul  id="place{{$bahanid}}">

             <?php for($i=0; $i<=$wag['jml_col']; $i++){
                   for($j=0;$j<=$wag['jml_row']; $j++){
                     $seatNo=($i+$j*$wag['jml_col']+1);
                     if(! isset($bahseatret[$i][$j])){
                       $bahseatret[$i][$j]=-1;
                     }
                     if($j!=0 && $i!=0 && $bahseatret[$i][$j]!=-1){
                         $iddasar="0-".$wag['kode_wagon']."-".$wag['no_wagon']."-".$hurnom[$i].$j;
             ?>
             <li id="{{$iddasar}}" onclick="pilihbangkuret('{{$iddasar}}')" class="{{$namakelas[$bahseatret[$i][$j]]}} row-{{$i}} col-{{$j}} " style="top:{{($i)*35}}px;left:{{($j)*35}}px">
             <a title="col{{$j}},row{{$i}}">{{$hurnom[$i]}}{{$j}}</a>
             </li>
             <?php }else if($j==0){ ?>
             <li class="row-{{$i}} col-{{$j}} " style="top:{{($i)*35}}px;left:{{($j)*35}}px">
             {{$hurnom[$i]}}
             </li>
             <?php }else if($i==0){ ?>
               <li  style="top:{{($i)*35}}px;left:{{($j)*35}}px">
               {{$j}}
               </li>
             <?php }}} ?>
             </ul>
             </div>

             </div>
             <?php } ?>
             @endforeach
             <div class="list-header clearfix" style="background-color: #ff6600;">

                <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" ></h4>

             </div>
             @endif

        @endif
          </div>
        @if($trip=="O")
        <div class="row" id="divtomlanjutdep">

    				<div class="col-xs-12 col-sm-8 col-md-12" >

                <div class="list-header clearfix" style="background-color: #ff6600;">

                   <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>

                </div>

    				</div>
          </div>
          @elseif($trip=="R")
          <div class="row" id="divtomlanjutret">

      				<div class="col-xs-12 col-sm-8 col-md-6" >

                  <div class="list-header clearfix" style="background-color: #ff6600;">

                     <button type="button" class="btn btn-block btn-primary" id="tomgoback">Ubah bangku</button>

                  </div>

      				</div>
              <div class="col-xs-12 col-sm-8 col-md-6" >

                  <div class="list-header clearfix" style="background-color: #ff6600;">

                     <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>

                  </div>

      				</div>

            </div>
            @endif
            <div class="row" id="divtomnextwagon">

        				<div class="col-xs-12 col-sm-8 col-md-12" >

                    <div class="list-header clearfix" style="background-color: #ff6600;">

                       <button type="button" class="btn btn-block btn-primary" id="tomnextwagon">Pilih tempat duduk pulang</button>

                    </div>

        				</div>
              </div>

          <div id="dialog-confirm" title="Bangku sudah dipilih">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><span id="dialog-confirm_message">Batalkan bangku yang sudah dipilih terlebih dahulu</span></p>
          </div>

      </div>
    </div>
  </div>
</div>


				@section('akhirbody')
        <script type="text/javascript">
        var maxsel={{$adt}};
        var jumseldep=0;
        var jumselret=0;
        var isi="cucu";
        var trip="{{$trip}}";

        var dafbangkudep = [];
        var dafbangkuret = [];
         $(function() {
           $( "#sisikanan" ).hide();
           $( "#divtomlanjutdep" ).hide();
           $( "#divtomlanjutret" ).hide();
           $( "#divtomnextwagon" ).hide();
             $( "#dialog-confirm" ).hide();

           });
           <?php
           $dafwag=$seatdep['seat_map'];
           ?>
           @if (is_array($dafwag))
           @foreach($dafwag as $wag)
           <?php
           $bahanid="dep".$wag['kode_wagon'].$wag['no_wagon'];
           ?>
           var settings{{$bahanid}} = {
                          rows: {{$wag['jml_col']}},
                          cols: {{$wag['jml_row']}},
                          rowCssPrefix: 'row-',
                          colCssPrefix: 'col-',
                          seatWidth: 35,
                          seatHeight: 35,
                          seatCss: 'seat',
                          selectedSeatCss: 'selectedSeat{{$bahanid}}',
                          selectingSeatCss: 'selectingSeat{{$bahanid}}'
                      };

                      var init{{$bahanid}} = function (reservedSeat) {
                   var str = [], seatNo, className;
                   for (i = 0; i < settings{{$bahanid}}.rows; i++) {
                       for (j = 0; j < settings{{$bahanid}}.cols; j++) {
                           seatNo = (i + j * settings{{$bahanid}}.rows + 1);
                           className = settings{{$bahanid}}.seatCss + ' ' + settings{{$bahanid}}.rowCssPrefix + i.toString() + ' ' + settings{{$bahanid}}.colCssPrefix + j.toString();
                           if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                               className += ' ' + settings{{$bahanid}}.selectedSeatCss;
                           }
                           str.push('<li class="' + className + '"' +
                                     'style="top:' + (i * settings{{$bahanid}}.seatHeight).toString() + 'px;left:' + (j * settings{{$bahanid}}.seatWidth).toString() + 'px">' +
                                     '<a title="' + seatNo + '">' + seatNo + '</a>' +
                                     '</li>');
                       }
                   }
                   $('#place{{$bahanid}}').html(str.join(''));
               };
   @endforeach
   @endif
        <?php
        $dafwag=$seatret['seat_map'];
        ?>
        @if (is_array($dafwag))
        @foreach($dafwag as $wag)
        <?php
        $bahanid="ret".$wag['kode_wagon'].$wag['no_wagon'];
        ?>
        var settings{{$bahanid}} = {
                       rows: {{$wag['jml_col']}},
                       cols: {{$wag['jml_row']}},
                       rowCssPrefix: 'row-',
                       colCssPrefix: 'col-',
                       seatWidth: 35,
                       seatHeight: 35,
                       seatCss: 'seat',
                       selectedSeatCss: 'selectedSeat{{$bahanid}}',
                       selectingSeatCss: 'selectingSeat{{$bahanid}}'
                   };

                   var init{{$bahanid}} = function (reservedSeat) {
                var str = [], seatNo, className;
                for (i = 0; i < settings{{$bahanid}}.rows; i++) {
                    for (j = 0; j < settings{{$bahanid}}.cols; j++) {
                        seatNo = (i + j * settings{{$bahanid}}.rows + 1);
                        className = settings{{$bahanid}}.seatCss + ' ' + settings{{$bahanid}}.rowCssPrefix + i.toString() + ' ' + settings{{$bahanid}}.colCssPrefix + j.toString();
                        if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
                            className += ' ' + settings{{$bahanid}}.selectedSeatCss;
                        }
                        str.push('<li class="' + className + '"' +
                                  'style="top:' + (i * settings{{$bahanid}}.seatHeight).toString() + 'px;left:' + (j * settings{{$bahanid}}.seatWidth).toString() + 'px">' +
                                  '<a title="' + seatNo + '">' + seatNo + '</a>' +
                                  '</li>');
                    }
                }
                $('#place{{$bahanid}}').html(str.join(''));
            };
            //case I: Show from starting
            //init{{$bahanid}}();

            //Case II: If already booked
            //var bookedSeats{{$bahanid}} = [5, 10, 25];
            //init{{$bahanid}}(bookedSeats{{$bahanid}});
/**
            $('.' + settings{{$bahanid}}.seatCss).click(function () {
if ($(this).hasClass(settings{{$bahanid}}.selectedSeatCss)){
    alert('Bangku ini tidak tersedia');
}else{
  $(this).toggleClass(settings{{$bahanid}}.selectingSeatCss);

  alert('Bangku sudah dipilih');
}
});
**/
@endforeach
@endif
$( "#tomgoback" ).click(function() {
  $('#titikup')[0].scrollIntoView(true);
  $( "#divtomlanjutret" ).hide('slow');
  $( "#sisikiri" ).show('slow');
  $( "#sisikanan" ).hide('slow');
  if((jumseldep==maxsel)){
  $( "#divtomnextwagon" ).show('slow');
  }
});
$( "#tomnextwagon" ).click(function() {
  $('#titikup')[0].scrollIntoView(true);
  $( "#divtomnextwagon" ).hide('slow');
  $( "#sisikiri" ).hide('slow');
  $( "#sisikanan" ).show('slow');
  if((jumseldep==maxsel) && (jumselret==maxsel)){
    $( "#divtomlanjutret" ).show('slow');
  }
});

$( "#tomgoone" ).click(function() {

$('#formgo_dafbangkudep').val(JSON.stringify(dafbangkudep));
$('#formgo_dafbangkuret').val(JSON.stringify(dafbangkuret));
if(trip=="O"){
if((jumseldep==maxsel)){
$( "#formgo" ).submit();
}
}else if(trip=="R"){
if((jumseldep==maxsel) && (jumselret==maxsel)){
  $( "#formgo" ).submit();
}
}
//alert($('#formgo_dafbangkudep').val());
});
function pilihbangkudep(idnya){
// alert(dafbangkudep);
  if ($('#'+idnya).hasClass("selectedSeat")){
      dialogalert('error','Bangku ini tidak tersedia');
  }else{
    if ($('#'+idnya).hasClass("selectingSeat")){
      var index = $.inArray( idnya, dafbangkudep );
      if (index > -1) {
        dafbangkudep.splice(index, 1);
      }
      jumseldep-=1;
      @if($trip=="O")
      $('#divtomlanjutdep').hide();
      @elseif($trip=="R")
      $('#divtomnextwagon').hide();
      @endif
      $('#'+idnya).toggleClass('selectingSeat');
    }else{
      if(jumseldep<maxsel){
        dafbangkudep.push(idnya);
        jumseldep+=1;
        if(jumseldep==maxsel){
          @if($trip=="O")
          $('#divtomlanjutdep').show();
          @elseif($trip=="R")
          $('#divtomnextwagon').show();
          @endif
        }
        $('#'+idnya).toggleClass('selectingSeat');
        }else{
          dialogalert('error','Batalkan bangku sebelumnya terlebih dahulu');
        }
    }
  }
}
function pilihbangkuret(idnya){
//  alert(dafbangkudep);
  if ($('#'+idnya).hasClass("selectedSeat")){
      dialogalert('error','Bangku ini tidak tersedia');
  }else{
    if ($('#'+idnya).hasClass("selectingSeat")){
      var index = $.inArray( idnya, dafbangkuret );
      if (index > -1) {
        dafbangkuret.splice(index, 1);
      }
      jumselret-=1;
      $('#divtomlanjutret').hide();
      $('#'+idnya).toggleClass('selectingSeat');
    }else{
      if(jumselret<maxsel){
        dafbangkuret.push(idnya);
        jumselret+=1;
        if(jumselret==maxsel){
          $('#divtomlanjutret').show();
        }
        $('#'+idnya).toggleClass('selectingSeat');
      }else{
        dialogalert('error','Batalkan bangku sebelumnya terlebih dahulu');
      }
    }
  }
}

function dialogalert(judul,pesan){
    $("#dialog-confirm_message").html(pesan);
    $( "#dialog-confirm" ).dialog({
      title: judul,
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "OK": function() {
          $( this ).dialog( "close" );
        }
      }
    });
}
$('#btnShow').click(function () {
    var str = [];
    $.each($('#place li.' + settings.selectedSeatCss + ' a, #place li.'+ settings.selectingSeatCss + ' a'), function (index, value) {
        str.push($(this).attr('title'));
    });
    alert(str.join(','));
})

$('#btnShowNew').click(function () {
    var str = [], item;
    $.each($('#place li.' + settings.selectingSeatCss + ' a'), function (index, value) {
        item = $(this).attr('title');
        str.push(item);
    });
    alert(str.join(','));
})

        jQuery.fn.sortElements = (function(){

            var sort = [].sort;

            return function(comparator, getSortable) {

                getSortable = getSortable || function(){return this;};

                var placements = this.map(function(){

                    var sortElement = getSortable.call(this),
                        parentNode = sortElement.parentNode,

                        // Since the element itself will change position, we have
                        // to have some way of storing its original position in
                        // the DOM. The easiest way is to have a 'flag' node:
                        nextSibling = parentNode.insertBefore(
                            document.createTextNode(''),
                            sortElement.nextSibling
                        );

                    return function() {

                        if (parentNode === this) {
                            throw new Error(
                                "You can't sort elements if any one is a descendant of another."
                            );
                        }

                        // Insert before flag:
                        parentNode.insertBefore(this, nextSibling);
                        // Remove flag:
                        parentNode.removeChild(nextSibling);

                    };

                });

                return sort.call(this, comparator).each(function(i){
                    placements[i].call(getSortable.call(this));
                });

            };

        })();
        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });


          var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpulang').append(value);
          });
        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });


            var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpulang').append(value);
            });
        }


        </script>
		@endsection

		@endsection
