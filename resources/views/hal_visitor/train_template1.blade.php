<!DOCTYPE html>
@extends('layouts.'.$namatemplate)
<html>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/flight_list.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2016 06:09:25 GMT -->

@section('sebelumtitel')
      <link href="{{ URL::asset('asettemplate1/css/select2.min.css')}}" rel="stylesheet" />
  		<script src="{{ URL::asset('asettemplate1/js/select2.min.js')}}"></script>
@endsection
@section('kontenweb')

<div id="bahanmodal" title="Error">
  INI ISI MODAL
</div>
<form class="contact-form"id="formgo" action="{{url('/')}}/train/train_pilihbangku" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_TrainNoDep" name="formgo_TrainNoDep">
<input type="hidden" id="formgo_TrainNoRet" name="formgo_TrainNoRet">
<input type="hidden" id="formgo_keretaDep" name="formgo_keretaDep">
<input type="hidden" id="formgo_keretaRet" name="formgo_keretaRet">
<input type="hidden" id="formgo_hargaDep" name="formgo_hargaDep">
<input type="hidden" id="formgo_hargaRet" name="formgo_hargaRet">
<input type="hidden" id="formgo_trip" name="formgo_trip" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>

<div class="inner-banner style-4">
	<img class="center-image" src="{{ URL::asset('img/inner_banner_train.jpg')}}" alt="">
	<div class="vertical-align">
		<div class="container">
			<ul class="banner-breadcrumb color-white clearfix">
				<li><a class="link-blue-2" href="{{url('/')}}">Home</a> /</li>
				<li><span class="color-red-3">Kereta</span></li>
			</ul>
			<h2 class="color-white">Tiket Kereta</h2>
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
  <div class="container">
    <div class="row">
      <div  id="sebelahkiri" class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar bg-white clearfix">
        <div class="sidebar-block">
          <h4 class="sidebar-title color-dark-2">search</h4>
          <div class="search-inputs">
            <div class="form-block clearfix">
              <h5>Asal</h5><div class="input-style b-50 color-3">
              <select class="input-style selektwo"placeholder="Check In"  name="asal" id="pilasal">
                   <option value=""></option>
                   @foreach($dafstat as $area)
                  <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $org==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
                  @endforeach
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Tujuan</h5>
                <div class="input-style">
                <select class="input-style selektwo" name="tujuan" id="piltujuan">
                   <option value=""></option>
                   @foreach($dafstat as $area)
                  <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $des==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
                  @endforeach
                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5>Rencana</h5>
                <div class="input-style">
                <select class="mainselection" id="pilrencana" name="pilrencana" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">
                  <option value="O" <?php if($otomatiscari==1 && $trip=="O"){print("selected=\"selected\"");} ?>>Sekali Jalan</option>
                  <option value="R" <?php if($otomatiscari==1 && $trip=="R"){print("selected=\"selected\"");} ?>>Pulang Pergi</option>

                </select> </div>
            </div>
            <div class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Berangkat</h5>
                <div class="input-style">
                  <input id="tglberangkat" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglberangkat)){print(date("d-m-Y",strtotime($tglberangkat)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div id="tny_ret" class="form-block clearfix">
              <h5 style="padding-bottom:10px;">Tanggal Pulang</h5>
                <div class="input-style">
                  <input id="tglpulang" type="text" placeholder="Dd/Mm/Yy" value="<?php if($otomatiscari==1 && isset($tglpulang)){print(date("d-m-Y",strtotime($tglpulang)));}else{echo date("d-m-Y");} ?>" style="color:BLACK;padding:4px; border: 1px solid #aaa; border-radius: 4px;" >
                </div>
            </div>
            <div class="form-block clearfix">
              <h5>Dewasa</h5>
                <div class="input-style">
                  <select class="mainselection" id="adt" name="adt" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=1; $op<=4;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>

            <div class="form-block clearfix">
              <h5>Bayi</h5>
                <div class="input-style">
                  <select class="mainselection"id="inf" name="inf" style="padding:4px; border: 1px solid #aaa; border-radius: 4px;">

                    <?php for($op=0; $op<=4;$op++){
                      print("<option value=\"$op\"");
                      if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
                      print(">$op</option>");
                    }
                    ?>

                  </select> </div>
            </div>
          </div>
          <input type="submit" onclick="cari()"  class="c-button b-40 bg-red-3 hv-red-3-o" value="search">
        </div>


        </div>
      </div>

      <input style="margin-left:10px;margin-bottom:10px;" type="submit" id="tomback" class="c-button b-40 bg-red-3 hv-red-3-o" value="Cari jadwal lain">

      <div class="col-xs-12 col-sm-8 col-md-9">

              <div class="clearfix">

                <div id="titikup"></div>
        				<div class="col-xs-12 col-sm-8 col-md-12"id="divterpilih">
                  <div class="clearfix" style="background-color: #ff6600;" id="labelpilihanpergi">

                     <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomutama" >
                     Pilih kereta pergi</h4>

                 </div>
        					<div class="list-content clearfix" id="pilihanpergi">
        						<div class="list-item-entry" >
        					        <div class="hotel-item style-10 bg-white">
        					        	<div class="table-view">
      						          	<div class="radius-top cell-view">
      						          	 	<img style="padding-left:20px;width:180px;height:50px;"id="pilihanpergi_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
      						          	</div>
      						          	<div class="title hotel-middle cell-view">
      							            <h5 id="pilihanpergi_namakereta">  DAF TRAN  </h5>
      							            <h6 class="color-grey-3 list-hidden">one wayaa flights</h6>
                                <h4 id="hsubclasspergi"><b>
                                  SubClass
                                  <select   name="pilsubclasspergi" id="pilsubclasspergi">
                                      <option value="0" >sad </option>
                                        <option value="0" >sad1 </option>

                                  </select>



                                </b></h4>
                                  <h4><b id="pilihanpergi_harga">Cheap Flights to Paris</b></h4>
      						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
      						          	    <div class="fi_block grid-hidden row row10">
      						          	    	<div class="flight-icon col-xs-6 col10">
      						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
      						          	    		<div class="fi_content">
      						          	    			<div class="fi_title color-dark-2">Pergi</div>
      						          	    			<div class="fi_text color-black" id="pilihanpergi_jampergi">wed nov 13, 2013 7:50 am</div>
      						          	    		</div>
      						          	    	</div>
      						          	    	<div class="flight-icon col-xs-6 col10">
      						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
      						          	    		<div class="fi_content">
      						          	    			<div class="fi_title color-dark-2">Tiba</div>
      						          	    			<div class="fi_text color-black" id="pilihanpergi_jamtiba">wed nov 13, 2013 7:50 am</div>
      						          	    		</div>
      						          	    	</div>
      						          	    </div>

      							           </div>
                               <div class="title hotel-right clearfix cell-view">
                               <span id="tomubahpilihanpergi" style="cursor:pointer;margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" >Ubah</span>
                               </div>
      					            </div>
      					        </div>
        						</div>
                    <div class="clearfix" style="background-color: #ff6600;" id="labelpilihanpulang">

                       <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkedua" >
                       Pilih kereta pergi</h4>

                    </div>
          					<div class="list-item-entry" id="pilihanpulang">
        					        <div class="hotel-item style-10 bg-white">
        					        	<div class="table-view">
        						          	<div class="radius-top cell-view">
        						          	 	<img style="padding-left:20px;width:180px;height:50px;" id="pilihanpulang_gbr" src="img/tour_list/flight_grid_1.jpg" alt="">
        						          	</div>
        						          	<div class="title hotel-middle cell-view">
        							          <h5 id="pilihanpulang_namakereta">  DAF TRAN  </h5>
        							            <h6 class="color-grey-3 list-hidden">loading..</h6>
                                  <h4 id="hsubclasspulang"><b>
                                    SubClass
                                    <select   name="pilsubclasspulang" id="pilsubclasspulang">
                                        <option value="0" >sad </option>
                                          <option value="0" >sad1 </option>

                                    </select>
                                  </b></h4>
                                    <h4><b id="pilihanpulang_harga">Cheap Flights to Paris</b></h4>
        						          	    <p class="list-hidden">Book now and <span class="color-red-3">save 30%</span></p>
        						          	    <div class="fi_block grid-hidden row row10">
        						          	    	<div class="flight-icon col-xs-6 col10">
        						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_2.png')}}" alt="">
        						          	    		<div class="fi_content">
        						          	    			<div class="fi_title color-dark-2">Pergi</div>
        						          	    			<div class="fi_text color-black" id="pilihanpulang_jampergi">wed nov 13, 2013 7:50 am</div>
        						          	    		</div>
        						          	    	</div>
        						          	    	<div class="flight-icon col-xs-6 col10">
        						          	    		<img class="fi_icon" src="{{ URL::asset('asettemplate1/img/tour_list/flight_icon_1.png')}}" alt="">
        						          	    		<div class="fi_content">
        						          	    			<div class="fi_title color-dark-2">Tiba</div>
        						          	    			<div class="fi_text color-black" id="pilihanpulang_jamtiba">wed nov 13, 2013 7:50 am</div>
        						          	    		</div>
        						          	    	</div>
        						          	    </div>
        							           </div>
                                 <div class="title hotel-right clearfix cell-view">
                                 <span id="tomubahpilihanpulang" style="cursor:pointer;margin-right:10px;" class="tomup c-button b-40 bg-blue hv-blue-o grid-hidden" >Ubah</span>
                                 </div>
        					            </div>
        					        </div>
          						</div>



        					</div>



        				</div>
              </div>
        <div class="list-header clearfix" id="barissorting">
          <div class="drop-wrap drop-wrap-s-4 list-sort"style="min-width:30px;">
            <div style="color:BLACK;">
             Urut berdasarkan
             </div>
          </div>
            <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
              <div class="drop"style="color:BLACK;">
               <b>-</b>
                <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                <span>
                  <a href="#" onclick="sortAsc('data-harga')"style="color:BLACK;">Harga terkecil</a>
                  <a href="#" onclick="sortDesc('data-harga')"style="color:BLACK;">Harga terbesar</a>
                  <a href="#" onclick="sortAsc('data-waktu')"style="color:BLACK;">Jam terdekat</a>
                  <a href="#" onclick="sortDesc('data-waktu')"style="color:BLACK;">Jam terakhir </a>
                </span>
               </div>
            </div>

        </div>
        <div  id="sisikiri">
@if($errors->has())
            <div class="list-header clearfix" style="background-color: #ff6600;"id="divtulisanerror">
              @foreach ($errors->all() as $error)
              <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" >
              {{ $error }}</h4>

          </div>
             @endforeach
             @endif
             <div class="list-header clearfix" style="background-color: #ff6600;">

                <h4  style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkiri" >
                Pilih kereta pergi</h4>

            </div>
            <div class="" id="dafloading" >

              <div class="list-item-entry" id="loading_kai">
                  <div class="hotel-item style-10 bg-white">
                    <div class="table-view">
                        <div class="radius-top cell-view">
                          <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KAI.jpg')}}" alt="">
                        </div>
                        <div class="title hotel-middle cell-view">
                          <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                          <h6 class="color-grey-3 list-hidden">loading..</h6>
                         </div>
                        <div class="title hotel-right clearfix cell-view grid-hidden">
                      </div>
                      </div>
                  </div>
              </div>

            </div>

        <div class="list-content clearfix" id="dafpergi">
        </div>
        </div>
        <div  id="sisikanan">

            <div class="list-header clearfix" style="background-color: #ff6600;">

                 <h4 style="text-align:center; padding-top:1%;padding-bottom:1%; color:#fff;" id="labelkolomkanan" >Pilih penerbangan pulang</h4>

            </div>
        <div class="list-content clearfix" id="dafpulang">
        </div>
        </div>

        <div class="row" id="divpilihnextorulang">
				    <!-- sisi kanantombol -->
    				<div class="col-xs-12 col-sm-8 col-md-12" id="sisikanantombol">

                <div class="list-header clearfix" style="background-color: #ff6600;">

                   <button type="button" class="btn btn-block btn-primary" id="tomgoone">Lanjutkan Transaksi</button>

                </div>

    				</div>
          </div>
      </div>
    </div>
  </div>
</div>


				@section('akhirbody')
        <script type="text/javascript">

        $("#sisikanan").hide();
        $("#barissorting").hide();
        $("#sisikiri").hide();
      $('#labelkolomutama').hide();

        //variabel

        var hargapulang="0";
        var hargapergi="0";
        var nilpil=0;
        var satuaja=0;
        var idpilper="";
        var idpilret="";
        var statasal="BD";
        var kotasal="Bandung";
        var labelasal="";
        var kottujuan="Gambir";
        var stattujuan="GMR";
        var labeltujuan="";
        var pilrencana="O";
        var pilrencana2="O";
        var tglber_d="";
        var tglber_m="";
        var tglber_y="";

        var tglpul_d="";
        var tglpul_m="";
        var tglpul_y="";

        var tglberangkat="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_dep').val(tglberangkat);
        $('#formgo_tgl_deppilihan').val(tglberangkat);
        var tglpulang="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_ret').val(tglpulang);
        $('#formgo_tgl_retpilihan').val(tglpulang);

            @if($otomatiscari==1)
            $('#formgo_tgl_deppilihan').val("{{$tglberangkat}}");
            $('#formgo_tgl_retpilihan').val("{{$tglpulang}}");
            $('#formgo_trip').val("{{$trip}}");
            $('#formgo_org').val("{{$org}}");
            $('#formgo_des').val("{{$des}}");
            $('#formgo_kotorg').val("{{$kotasal}}");
            $('#formgo_labelorg').val("{{$kotasal}} ({{$org}})");
            $('#formgo_kotdes').val("{{$kottujuan}}");
            $('#formgo_labeldes').val("{{$kottujuan}} ({{$des}})");
            $('#formgo_chd').val("{{$jumchd}}");
            $('#formgo_adt').val("{{$jumadt}}");
            $('#formgo_inf').val("{{$juminf}}");
            @endif

        var jumadt="1";
        var jumchd="0";
        var juminf="0";



        var urljadwal="{{ url('/') }}/jadwalKereta/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#tomgoret').hide();
        		$('#tomback').hide();
        $('#divpilihnextorulang').hide();
        $('#hsubclasspergi').hide();
        $('#hsubclasspulang').hide();
        $('#tomulangipencarian').hide('slow');


        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#divpilihnextorulang').hide();
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomteskir" ).click(function() {
          $( "#formgo" ).submit();
        });

        $('#tomulangipencarian').on('click', function() {


        $('#formatas').show('slow');

        $('#tomulangipencarian').hide('slow');

        });
        $('#adt').on('change', function() {
        jumadt=this.value;
        $('#formgo_adt').val(jumadt);

        });
        $('#chd').on('change', function() {
        jumchd=this.value;
        $('#formgo_chd').val(jumchd);

        });
        $('#inf').on('change', function() {
        juminf=this.value;
        $('#formgo_inf').val(juminf);

        });


        $('#labelpilihanpulang').hide();
        $('#pilihanpergi').hide();
        $('#pilihanpulang').hide();
          $('.selektwo').select2();
          <?php if(($otomatiscari==1 && $trip=="O")||$otomatiscari==0){?>
    		  $('#tny_ret').hide();
          <?php }?>
                          $('#loading_kai').hide();

    $("#tglberangkat").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate: new Date(),
        maxDate: '+2y'
    });

    $("#tglpulang").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: new Date(),
        changeMonth: true,
        changeYear: true
    });



        $('#tglberangkat').on('change', function() {

        var values=this.value.split('-');
        //alert(this.value);
        tglber_d=values[0];
        tglber_m=values[1];
        tglber_y=values[2];

        tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

        $('#formgo_tgl_deppilihan').val(tglberangkat);

        var selectedDate = new Date(tglberangkat);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime());
                $("#tglpulang").datepicker( "option", "minDate", endDate );
                $("#tglpulang").datepicker( "option", "maxDate", '+2y' );
                    settglpulang($("#tglpulang").val());

        });

            function settglpulang(val){

              		var values=val.split('-');
              		//alert(this.value);
              		tglpul_d=values[0];
              		tglpul_m=values[1];
              		tglpul_y=values[2];

              		tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
                  $('#formgo_tgl_retpilihan').val(tglpulang);
            }
        $('#tglpulang').on('change', function() {

              settglpulang(this.value);
        });

        $('#pilasal').on('change', function() {
          var values=this.value.split('-');
          var kotas=values[1];
          kotasal=values[0];
          //alert(kotas);
          statasal=kotas;
          labelasal=this.value;
          $('#formgo_labelorg').val(labelasal);
          $('#formgo_org').val(statasal);
          $('#formgo_kotorg').val(kotasal);

        });
        $('#piltujuan').on('change', function() {
          var values=this.value.split('-');
          var kottuj=values[1];
          kottujuan=values[0];
          //alert(kottuj);
          stattujuan=kottuj;
          labeltujuan=this.value;
          $('#formgo_labeldes').val(labeltujuan);
          $('#formgo_des').val(kottuj);
          $('#formgo_kotdes').val(kottujuan);

        });
        $('#pilrencana').on('change', function() {
          //alert( this.value );
          pilrencana=this.value;
          $('#formgo_trip').val(pilrencana);
          var tny_ret = document.getElementById("tny_ret").value;

        if(this.value=="O"){
            $('#tny_ret').hide();
         }else{
            $('#tny_ret').show();
         }


        });

        function setkolom(){
          $('#labelkolomutama').html(kotasal+" ("+statasal+") ke "+kottujuan+" ("+stattujuan+") | Dewasa: "+jumadt+" Bayi: "+juminf);
        if(pilrencana=="R"){
          $('#labelkolomkedua').html(kottujuan+" ("+stattujuan+") ke "+kotasal+" ("+statasal+") | Dewasa: "+jumadt+" Bayi: "+juminf);
        }
        }
        function labelutama(){

        }

      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

      $('#pilsubclasspergi').on('change', function() {
      hargapergi=this.value.split(",")[0];
      var totaladt=hargapergi*jumadt;
      var totalchd=hargapergi*jumchd;
      var totalinf=hargapergi*juminf;
      $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
      totalhargapergi=totaladt+totalchd+totalinf;
      $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(totalhargapergi));


      });
      $('#pilsubclasspulang').on('change', function() {
      hargapulang=this.value.split(",")[0];
      var totaladt=hargapulang*jumadt;
      var totalchd=hargapulang*jumchd;
      var totalinf=hargapulang*juminf;
      $('#formgo_selectedIDret').val(this.value.split(",")[1]);
      totalhargapulang=totaladt+totalchd+totalinf;
      $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(totalhargapulang));


      });

        function pilihPergi(bahanid
          ,jampergiberangkat
          ,jampergitiba
          ,harga
          ,namakereta
          ,kumpulaniddep
          ,hasildiskon
          ,kepodep
          ){

            $('#formgo_selectedIDdep').val(kumpulaniddep);
             $('#pilsubclasspergi').find('option').remove();

        $('#formgo_TrainNoDep').val(bahanid);
        $('#formgo_keretaDep').val(namakereta);
        $('#formgo_hargaDep').val(kepodep);
        $('#formgo_tgl_dep').val(jampergiberangkat);
        $('#formgo_tgl_dep_tiba').val(jampergitiba);
        /**
       $("#"+idpilper).removeClass("kotakpilihb cell-view");
        $("#"+idpilper).addClass("kotakpilih cell-view");

        $("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
      idpilper=bahanid+"_gbr";
      **/

      $('#labelkolomutama').show('slow');
        $('#pilihanpergi').show('slow');
        $('#divterpilih').show('slow');
        $('#divtulisanerror').hide();
        //  alert(jampergitiba);
                $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
                //  alert($('#pilihanpergi_gbr').attr('src'));
                 $('#pilihanpergi_jampergi').html(jampergiberangkat);
                $('#pilihanpergi_jamtiba').html(jampergitiba);
               $('#pilihanpergi_namakereta').html(namakereta+' '+bahanid);
                $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(hasildiskon));

                nilpil+=1;
                $(location).attr('href', '#labelkolomutama');

              //  alert ('tes tes');
                  $( "#labelkolomutama" ).focusin();
                if(pilrencana2=="O" || (pilrencana2=="R" && satuaja==1)){
                 $('#sisikiri').hide('slow');
                  $('#barissorting').hide('slow');
                 mintaformgo();
               }else if(pilrencana2=="R" && satuaja==0){
                 mintaformret();
               }
               satuaja=0;

        }
        function pilihPulang(bahanid
          ,jampulangberangkat
          ,jampulangtiba
          ,harga
          ,namakereta
          ,kumpulanidret
          ,hasildiskon
          ,keporet
          ){


             $('#pilsubclasspulang').find('option').remove();

             $('#formgo_TrainNoRet').val(bahanid);
            $('#formgo_keretaRet').val(namakereta);
            $('#formgo_hargaRet').val(keporet);
            $('#formgo_tgl_ret').val(jampulangberangkat);
            $('#formgo_tgl_ret_tiba').val(jampulangtiba);


            $('#labelpilihanpulang').show('slow');
            $('#formgo_selectedIDret').val(kumpulanidret);
             $('#barissorting').hide('slow');
      $('#labelkolomutama').show('slow');
          $('#pilihanpulang').show('slow');
          $('#divterpilih').show('slow');
          //  alert(jampergitiba);
                $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
                //  alert($('#pilihanpergi_gbr').attr('src'));
                 $('#pilihanpulang_jampergi').html(jampulangberangkat);
                $('#pilihanpulang_jamtiba').html(jampulangtiba);
                $('#pilihanpulang_namakereta').html(namakereta+' '+bahanid);
                $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(hasildiskon));

                    mintaformgo();
                    satuaja=0;
              $(location).attr('href', '#labelkolomutama');

              $("#sisikanan").hide('slow');
        }
        function mintaformgo(){
          $('#tomgoret').hide();
          $('#divpilihnextorulang').show('slow');

        }
        function mintaformret(){
           $("#sisikiri").hide('slow');
           $("#sisikanan").show('slow');
           $('#divpilihnextorulang').hide();
           $('#tomgoret').show('slow');

        }

        $('#tomubahpilihanpergi').on('click', function() {
        satuaja=1;
        $('#divterpilih').hide('slow');
        $('#sisikiri').show('slow');
        $('#barissorting').show('slow');
        });
        $('#tomubahpilihanpulang').on('click', function() {
        $('#sisikanan').show('slow');
        $('#divterpilih').hide('slow');
        $('#barissorting').show('slow');
        });
        $('#tomback').on('click', function() {
        $('#tomback').hide('slow');
        $('#sebelahkiri').show('slow');
        });

        $('.tomup').on('click', function() {
          $('#titikup')[0].scrollIntoView(true);
        });
        function cari(){
        nilpil=0;
        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=4){
          var isMobile = window.matchMedia("only screen and (max-width: 760px)");

            if (isMobile.matches) {
              $("#tomback").show('slow');
              $("#sebelahkiri").hide('slow');
              //$('#titikup')[0].scrollIntoView(true);
            }

                $('#labelkolomkedua').hide();
                      $('#labelpilihanpulang').hide('slow');
                    $("#barissorting").show('slow');
                    $("#sisikanan").hide();
                    $('#divpilihnextorulang').hide();
                    $('#tomgoret').hide();


        //  alert("Memulai pencarian");
                //  $('#hasilkirim').html("<b>Hello world!</b>");
                if(pilrencana=="O"){
                    $("#sisikanan").hide('slow');
                    $("#sisikiri").show('slow');
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
                  }else{
                    $("#sisikiri").show('slow');
                        $('#labelkolomkedua').show();
                    //$("#sisikanan").show('slow');
                      //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
                  }
                urljadwalb=urljadwal;
                urljadwalb+="org/"+statasal;
                urljadwalb+="/des/"+stattujuan;
                urljadwalb+="/trip/"+pilrencana;
                urljadwalb+="/tglberangkat/"+tglberangkat;
                urljadwalb+="/tglpulang/"+tglpulang;
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                urljadwalb+="/juminf/"+juminf;
                //akhir urljadwalb+="ac/";
                        $('#loading_kai').show('fadeOut');

                        $('#dafpergi').html('');
                        $('#dafpulang').html('');

                ambildata_qg();

                pilrencana2=pilrencana;
        $('#formatas').hide('slow');
               $('#pilihanpergi').hide();
                $('#pilihanpulang').hide();

                        $('#labelkolomutama').hide('slow');

              $('#tomulangipencarian').show('slow');


              //  alert(urljadwalb);
              setkolom();
            }else{
              $('#bahanmodal').dialog({ modal: true });
              // $( "#bahanmodal" ).show();
            //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
            }
        }


        var ajaxku_qg;
        function ambildata_qg(){
          ajaxku_qg = buatajax();
          var url=urljadwalb;
          ajaxku_qg.onreadystatechange=stateChanged_qg;
          ajaxku_qg.open("GET",url,true);
          ajaxku_qg.send(null);
        }
         function stateChanged_qg(){
           var data;
            if (ajaxku_qg.readyState==4){
              data=ajaxku_qg.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_kai').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){
            //$('#dafpergi').html(data+$('#dafpergi').html());
            //$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
            //$('.bagpulang').remove();

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              if($(this).attr('untuk')=="pergi"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var waktu=$(this).attr('data-waktu');

              var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              var valueToPush = new Array();
              valueToPush[0] = temp_id;
              valueToPush[1] = biaya;
              valueToPush[2] = waktu;
              valueToPush[3] = isiHTML;
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());


              }else if($(this).attr('untuk')=="pulang"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var waktu=$(this).attr('data-waktu');

                var isiHTML='<div class="list-item-entry isiitempulang isiitem" id="'+temp_id+'" data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
                //alert(temp_id);

              var valueToPush = new Array();
              valueToPush[0] = temp_id;
              valueToPush[1] = biaya;
              valueToPush[2] = waktu;
              valueToPush[3] = isiHTML;
              //dafHTMLPulang.push(valueToPush);
              $('#dafpulang').html(isiHTML+$('#dafpulang').html());


            }
          }});


        }

        jQuery.fn.sortElements = (function(){

            var sort = [].sort;

            return function(comparator, getSortable) {

                getSortable = getSortable || function(){return this;};

                var placements = this.map(function(){

                    var sortElement = getSortable.call(this),
                        parentNode = sortElement.parentNode,

                        // Since the element itself will change position, we have
                        // to have some way of storing its original position in
                        // the DOM. The easiest way is to have a 'flag' node:
                        nextSibling = parentNode.insertBefore(
                            document.createTextNode(''),
                            sortElement.nextSibling
                        );

                    return function() {

                        if (parentNode === this) {
                            throw new Error(
                                "You can't sort elements if any one is a descendant of another."
                            );
                        }

                        // Insert before flag:
                        parentNode.insertBefore(this, nextSibling);
                        // Remove flag:
                        parentNode.removeChild(nextSibling);

                    };

                });

                return sort.call(this, comparator).each(function(i){
                    placements[i].call(getSortable.call(this));
                });

            };

        })();
        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });


          var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpulang').append(value);
          });
        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });


            var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpulang').append(value);
            });
        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        statasal="{{$org}}";
        stattujuan="{{$des}}";
        pilrencana="{{$trip}}";
        tglberangkat="{{$tglberangkat}}";
        tglpulang="{{$tglpulang}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        juminf="{{$juminf}}";
        kotasal="{{$kotasal}}";
        kottujuan="{{$kottujuan}}";
        cari();
        @endif
        </script>
		@endsection

		@endsection
