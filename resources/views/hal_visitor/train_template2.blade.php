<!DOCTYPE html>
@extends('layouts.'.$namatemplate)

@section('kontenweb')

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Cari Tiket Kereta</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li class="active">Kereta Api</li>
        </ul>
    </div>
</div>

<form class="contact-form"id="formgo" action="{{url('/')}}/train/train_pilihbangku" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<input type="hidden" id="formgo_labelorg" name="formgo_labelorg" value="Jakarta (CGK)">
<input type="hidden" id="formgo_kotorg" name="formgo_kotorg" value="Jakarta">
<input type="hidden" id="formgo_org" name="formgo_org" value="CGK">
<input type="hidden" id="formgo_labeldes" name="formgo_labeldes" value="Manokwari (MKW)">
<input type="hidden" id="formgo_kotdes" name="formgo_kotdes" value="Manokwari">
<input type="hidden" id="formgo_des" name="formgo_des" value="MKW">
<input type="hidden" id="formgo_TrainNoDep" name="formgo_TrainNoDep">
<input type="hidden" id="formgo_TrainNoRet" name="formgo_TrainNoRet">
<input type="hidden" id="formgo_keretaDep" name="formgo_keretaDep">
<input type="hidden" id="formgo_keretaRet" name="formgo_keretaRet">
<input type="hidden" id="formgo_hargaDep" name="formgo_hargaDep">
<input type="hidden" id="formgo_hargaRet" name="formgo_hargaRet">
<input type="hidden" id="formgo_trip" name="formgo_trip" value="O">
<input type="hidden" id="formgo_tgl_dep" name="formgo_tgl_dep" >
<input type="hidden" id="formgo_tgl_dep_tiba" name="formgo_tgl_dep_tiba" >
<input type="hidden" id="formgo_tgl_ret" name="formgo_tgl_ret" >
<input type="hidden" id="formgo_tgl_ret_tiba" name="formgo_tgl_ret_tiba" >
<input type="hidden" id="formgo_adt" name="formgo_adt" value="1">
<input type="hidden" id="formgo_chd" name="formgo_chd" value="0">
<input type="hidden" id="formgo_inf" name="formgo_inf" value="0">
<input type="hidden" id="formgo_tgl_deppilihan" name="formgo_tgl_deppilihan" >
<input type="hidden" id="formgo_tgl_retpilihan" name="formgo_tgl_retpilihan" >
<input type="hidden" id="formgo_selectedIDdep" name="formgo_selectedIDdep" >
<input type="hidden" id="formgo_selectedIDret" name="formgo_selectedIDret" >

</form>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="row">
                <div class="col-sm-4 col-md-3"  id="sebelahkiri" >
                   <div class="toggle-container filters-container">
                        <div class="panel style1 arrow-right">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" href="#accomodation-type-filter" class="" aria-expanded="true">Search</a>
                          </h4>
                          <div id="modify-search-panel" class="panel-collapse collapse in" aria-expanded="true">

                                <div class="panel-content">
                                        <div class="form-group">
                                            <label>Asal</label>
                                                <select style="width:100%" class="selektwo full-width" name="pilasal" id="pilasal">
                                                  <option value=""></option>
                                                  @foreach($dafstat as $area)
                                                 <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $org==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
                                                 @endforeach
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Tujuan</label>
                                            <select style="width:100%"  class="full-width selektwo" name="tujuan" id="piltujuan">
                                               <option value=""></option>
                                               @foreach($dafstat as $area)
                                              <option value="{{ $area->st_name }}-{{ $area->st_code }}" <?php if($otomatiscari==1 && $des==$area->st_code){print("selected=\"selected\"");} ?>>{{ $area->st_name }} - {{ $area->st_code }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Rencana</label>
                                            <div class="selector">
                                              <select class="full-width" id="pilrencana" name="pilrencana" style="">
                                                <option value="O" <?php if($otomatiscari==1 && $trip=="O"){print("selected=\"selected\"");} ?>>Sekali Jalan</option>
                                                <option value="R" <?php if($otomatiscari==1 && $trip=="R"){print("selected=\"selected\"");} ?>>Pulang Pergi</option>

                                              </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Berangkat</label>
                                            <div class="datepicker-wrap">
                                                <input id="tglberangkat" onchange="settglberangkat(this.value);" name="tglberangkat"  type="text" class="input-text full-width" placeholder="dd/mm/yy"   value="<?php if($otomatiscari==1 && isset($tglberangkat)){print(date("d-m-Y",strtotime($tglberangkat)));}else{echo date("d-m-Y");} ?>" />
                                            </div>
                                        </div>
                                        <div id="tny_ret" class="form-group">
                                            <label>Tanggal Pulang</label>
                                            <div class="datepicker-wrap">
                                                <input id="tglpulang" onchange="settglpulang(this.value);" name="tglpulang"  type="text" class="input-text full-width" placeholder="dd/mm/yy" value="<?php if($otomatiscari==1 && isset($tglpulang)){print(date("d-m-Y",strtotime($tglpulang)));}else{echo date("d-m-Y");} ?>" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                          <label>Dewasa</label>
                                          <div class="selector">
                                              <select class="full-width" id="adt"  name="adt" >
                                                <?php for($op=1; $op<=7;$op++){
                                                  print("<option value=\"$op\"");
                                                  if($otomatiscari==1 && $op==$jumadt){print("selected=\"selected\"");}
                                                  print(">$op</option>");
                                                }
                                                ?>
                                              </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label>Bayi</label>
                                          <div class="selector">
                                              <select class="full-width" id="inf" name="inf"  >

                                                  <?php for($op=0; $op<=6;$op++){
                                                    print("<option value=\"$op\"");
                                                    if($otomatiscari==1 && $op==$juminf){print("selected=\"selected\"");}
                                                    print(">$op</option>");
                                                  }
                                                  ?>

                                              </select>
                                          </div>
                                        </div>
                                        <br />
                                        <button onclick="cari()" class="btn-medium icon-check uppercase full-width">search again</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                  <button style="margin-left:auto;margin-right:auto;margin-bottom:20px;" class="tomup full-width btn-small" id="tomback">Cari jadwal lain</button>
                  <div id="diverror">
                  @if($errors->has())
                  @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong>{{ $error }}
                  </div>
                  @endforeach
                  @endif
                  </div>
                    <div  id="barissorting"  class="sort-by-section clearfix"style="margin-bottom:20px;padding-bottom:20px;">

                        <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
                        <h4 class="sort-by-title block-sm">Sort results by:</h4>
                        <select id="chd" style="margin-top:10px;" >
                            <option value="0" onclick="sortAsc('data-harga')">Harga Terendah</option>
                            <option value="1" onclick="sortDesc('data-harga')">Harga Tertinggi</option>
                            <option value="1" onclick="sortAsc('data-bintang')">Bintang terendah</option>
                            <option value="1" onclick="sortDesc('data-bintang')">Bintang tertinggi </option>
                        </select>
                      </div>

                    </div>
                    <div class="flight-list listing-style3 flight">
                      <div class="list-item-entry" id="loading_kai">
                          <div class="hotel-item style-10 bg-white">
                            <div class="table-view">
                                <div class="radius-top cell-view">
                                  <img style="padding-left:10px;width:80px;height:30px;" src="{{ URL::asset('img/ac/Airline-KAI.jpg')}}" alt="">
                                </div>
                                <div class="title hotel-middle cell-view">
                                  <img src="{{ URL::asset('asettemplate1/img/imgload.gif')}}" alt="">
                                 </div>
                                <div class="title hotel-right clearfix cell-view grid-hidden">
                              </div>
                              </div>
                          </div>
                      </div>
                        <div id="titikup"></div>
                				<div  id="divterpilih">
                      <div class="page-title-container" id="labelpilihanpergi">
                          <div class="page-title pull-left">
                            <h2 class="entry-title" style="padding-left:10px;" id="labelkolomutama" >Pilihan Penerbangan Berangkat</h2>
                          </div>
                      </div>
                      <div class="flight-list listing-style3 flight" id="pilihanpergi">
                        <article class="box">
                            <figure class="col-xs-3 col-sm-2">
                                <span><img alt=""id="pilihanpergi_gbr" src="http://placehold.it/270x160"></span>
                            </figure>
                            <div class="details col-xs-9 col-sm-10">
                                <div class="details-wrapper">
                                    <div class="first-row">
                                        <div>
                                            <h4 class="box-title" id="pilihanpergi_namakereta">Indianapolis to Paris<small>Oneway flight</small></h4>
                                            <div class="amenities">
                                                <i class="soap-icon-wifi circle"></i>
                                                <i class="soap-icon-entertainment circle"></i>
                                                <i class="soap-icon-fork circle"></i>
                                                <i class="soap-icon-suitcase circle"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <span class="price"id="pilihanpergi_harga"><small>AVG/PERSON</small>$620</span>
                                        </div>
                                    </div>
                                    <div class="second-row">
                                        <div class="time">
                                            <div class="take-off col-sm-4">
                                                <div>
                                                    <span class="skin-color">Berangkat</span><br /><span  id="pilihanpergi_jampergi">Wed Nov 13, 2013 7:50 Am</span>
                                                </div>
                                            </div>
                                            <div class="landing col-sm-4">
                                                <div>
                                                    <span class="skin-color">Pulang</span><br /><span  id="pilihanpergi_jamtiba">Wed Nov 13, 2013 9:20 am</span>
                                                </div>
                                            </div>
                                            <div class="total-time col-sm-4">
                                              <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                <div>
                                                    <span class="skin-color">Lama perjalanan</span><br /><span  id="pilihanpergi_totalwaktu">13 Hour, 40 minutes</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div  >
                                        <button class="tomup full-width btn-small" id="tomubahpilihanpergi">Ubah</button>

                                      </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        </div>
                        <div class="page-title-container" id="labelpilihanpulang">
                            <div class="page-title pull-left">
                              <h2 class="entry-title" style="padding-left:10px;" id="labelkolomkedua" >Kereta pulang</h2>
                            </div>
                        </div>


                        <div class="flight-list listing-style3 flight" id="pilihanpulang">
                            <article class="box">
                                <figure class="col-xs-3 col-sm-2">
                                    <span><img alt="" id="pilihanpulang_gbr"  src="http://placehold.it/270x160"></span>
                                </figure>
                                <div class="details col-xs-9 col-sm-10">
                                    <div class="details-wrapper">
                                        <div class="first-row">
                                            <div>
                                                <h4 class="box-title" id="pilihanpulang_namakereta">Indianapolis to Paris<small>Oneway flight</small></h4>

                                                <div class="amenities">
                                                    <i class="soap-icon-wifi circle"></i>
                                                    <i class="soap-icon-entertainment circle"></i>
                                                    <i class="soap-icon-fork circle"></i>
                                                    <i class="soap-icon-suitcase circle"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <span class="price" id="pilihanpulang_harga"> $620</span>
                                            </div>
                                        </div>
                                        <div class="second-row">
                                            <div class="time">
                                                <div class="take-off col-sm-4">
                                                    <div>
                                                        <span class="skin-color">Pergi</span><br /> <span  id="pilihanpulang_jampergi">Wed Nov 13, 2013 7:50 Am</span>
                                                    </div>
                                                </div>
                                                <div class="landing col-sm-4">
                                                    <div>
                                                        <span class="skin-color">Sampai</span><br /> <span  id="pilihanpulang_jamtiba">Wed Nov 13, 2013 9:20 am</span>
                                                    </div>
                                                </div>
                                                <div class="total-time col-sm-4">
                                                    <div class="icon"><i class="soap-icon-clock yellow-color"></i></div>
                                                    <div>
                                                        <span class="skin-color">Lama perjalanan</span><br /> <span  id="pilihanpulang_totalwaktu">Wed Nov 13, 2013 9:20 am</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button  class="tomup full-width btn-small" id="tomubahpilihanpulang">Ubah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                          </div>
                        </div>

                      <div  id="sisikiri">

                      <div class="page-title-container">
                          <div class="page-title pull-left">
                            <h2 class="entry-title" style="padding-left:10px;">Pilih Kereta Berangkat</h2>
                          </div>
                      </div>
                      <div  id="dafpergi">
                      </div>
                      </div>
                      <div  id="sisikanan">
                        <div class="page-title-container">
                            <div class="page-title pull-left">
                              <h2 class="entry-title" style="padding-left:10px;">Pilih Kereta Pulang</h2>
                            </div>
                        </div>
                      <div  id="dafpulang">
                      </div>
                      </div>
                      <div  id="divpilihnextorulang">
                            <button  class="full-width btn-large" id="tomgoone" style="margin-top:10px;">Lanjutkan Transaksi</button>


                      </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


				@section('akhirbody')
        <script type="text/javascript">

        $("#sisikanan").hide();
        $("#barissorting").hide();
        $("#sisikiri").hide();
        //variabel

        var hargapulang="0";
        var hargapergi="0";
        var nilpil=0;
        var idpilper="";
        var satuaja=0;
        var idpilret="";
        var statasal="BD";
        var kotasal="Bandung";
        var labelasal="";
        var kottujuan="Gambir";
        var stattujuan="GMR";
        var labeltujuan="";
        var pilrencana="O";
        var pilrencana2="O";
        var tglber_d="";
        var tglber_m="";
        var tglber_y="";

        var tglpul_d="";
        var tglpul_m="";
        var tglpul_y="";

        var tglberangkat="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_dep').val(tglberangkat);
        $('#formgo_tgl_deppilihan').val(tglberangkat);
        var tglpulang="<?php echo date("Y-m-d");?>";
        $('#formgo_tgl_ret').val(tglpulang);
        $('#formgo_tgl_retpilihan').val(tglpulang);

            @if($otomatiscari==1)
            $('#formgo_tgl_deppilihan').val("{{$tglberangkat}}");
            $('#formgo_tgl_retpilihan').val("{{$tglpulang}}");
            $('#formgo_trip').val("{{$trip}}");
            $('#formgo_org').val("{{$org}}");
            $('#formgo_des').val("{{$des}}");
            $('#formgo_kotorg').val("{{$kotasal}}");
            $('#formgo_labelorg').val("{{$kotasal}} ({{$org}})");
            $('#formgo_kotdes').val("{{$kottujuan}}");
            $('#formgo_labeldes').val("{{$kottujuan}} ({{$des}})");
            $('#formgo_chd').val("{{$jumchd}}");
            $('#formgo_adt').val("{{$jumadt}}");
            $('#formgo_inf').val("{{$juminf}}");
            @endif

        var jumadt="1";
        var jumchd="0";
        var juminf="0";



        var urljadwal="{{ url('/') }}/jadwalKereta/";
        var urljadwalb=urljadwal;
        var dafIDPergi = [];
        var dafBiayaPergi = [];
        var dafHTMLPergi = [];
        var dafIDPulang = [];
        var dafBiayaPulang = [];
        var dafHTMLPulang = [];


        $('#labelpilihanpergi').hide();
        $('#tomback').hide();
        $('#labelpilihanpulang').hide();
        $('#tomgoret').hide();
        $('#divpilihnextorulang').hide();
        $('#hsubclasspergi').hide();
        $('#hsubclasspulang').hide();
        $('#tomulangipencarian').hide('slow');


        $( "#tompilulang" ).click(function() {
          nilpil=0;
          $('#divpilihnextorulang').hide();
          $('#sisikiri').show('slow');
          $('#barissorting').show('slow');
        });
        $( "#tomgoone" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomgoret" ).click(function() {
        $( "#formgo" ).submit();
        });
        $( "#tomteskir" ).click(function() {
          $( "#formgo" ).submit();
        });

        $('#tomulangipencarian').on('click', function() {


        $('#formatas').show('slow');

        $('#tomulangipencarian').hide('slow');

        });
        $('#adt').on('change', function() {
        jumadt=this.value;
        $('#formgo_adt').val(jumadt);

        });
        $('#chd').on('change', function() {
        jumchd=this.value;
        $('#formgo_chd').val(jumchd);

        });
        $('#inf').on('change', function() {
        juminf=this.value;
        $('#formgo_inf').val(juminf);

        });


        $('#pilihanpergi').hide();
        $('#pilihanpulang').hide();
          $('.selektwo').select2();
          <?php if(($otomatiscari==1 && $trip=="O")||$otomatiscari==0){?>
    		  $('#tny_ret').hide();
          <?php }?>
                          $('#loading_kai').hide();


                          $("#tglberangkat").datepicker({
                              dateFormat: 'dd-mm-yy',
                              changeMonth: true,
                              changeYear: true,
                              minDate: new Date(),
                              maxDate: '+2y'
                          });

                          $("#tglpulang").datepicker({
                              dateFormat: 'dd-mm-yy',
                              minDate: new Date(),
                              changeMonth: true,
                              changeYear: true
                          });

        function settglberangkat(val) {

        var values=val.split('-');
        //alert(this.value);
        tglber_d=values[0];
        tglber_m=values[1];
        tglber_y=values[2];

        tglberangkat=tglber_y+"-"+tglber_m+"-"+tglber_d;

        $('#tglberangkat').val(tglber_d+"-"+tglber_m+"-"+tglber_y);
         $('#formgo_tgl_deppilihan').val(tglberangkat);

         var selectedDate = new Date(tglberangkat);
         var msecsInADay = 86400000;
         var endDate = new Date(selectedDate.getTime());
                 $("#tglpulang").datepicker( "option", "minDate", endDate );
                 $("#tglpulang").datepicker( "option", "maxDate", '+2y' );
                     setulangtglpulang($("#tglpulang").val());

        }
        function setulangtglpulang(val){

              var values=val.split('-');
              //alert(this.value);
              tglpul_d=values[0];
              tglpul_m=values[1];
              tglpul_y=values[2];

              tglpulang=tglpul_y+"-"+tglpul_m+"-"+tglpul_d;
              $('#formgo_tgl_retpilihan').val(tglpulang);
              $('#tglpulang').val(tglpul_d+"-"+tglpul_m+"-"+tglpul_y);
        }
        function settglpulang(val){

                        setulangtglpulang(val);
        }

        $('#pilasal').on('change', function() {
          var values=this.value.split('-');
          var kotas=values[1];
          kotasal=values[0];
          //alert(kotas);
          statasal=kotas;
          labelasal=this.value;
          $('#formgo_labelorg').val(labelasal);
          $('#formgo_org').val(statasal);
          $('#formgo_kotorg').val(kotasal);

        });
        $('#piltujuan').on('change', function() {
          var values=this.value.split('-');
          var kottuj=values[1];
          kottujuan=values[0];
          //alert(kottuj);
          stattujuan=kottuj;
          labeltujuan=this.value;
          $('#formgo_labeldes').val(labeltujuan);
          $('#formgo_des').val(kottuj);
          $('#formgo_kotdes').val(kottujuan);

        });
        $('#pilrencana').on('change', function() {
          //alert( this.value );
          pilrencana=this.value;
          $('#formgo_trip').val(pilrencana);
          var tny_ret = document.getElementById("tny_ret").value;

        if(this.value=="O"){
            $('#tny_ret').hide();
         }else{
            $('#tny_ret').show();
         }


        });

        function setkolom(){
        $('#labelkolomutama').html(kotasal+" ("+statasal+") ke "+kottujuan+" ("+stattujuan+")");
        if(pilrencana=="R"){
        $('#labelkolomkedua').html(kottujuan+" ("+stattujuan+") ke "+kotasal+" ("+statasal+")");
        }
        }

      function convertToRupiah(angka){
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return rupiah.split('',rupiah.length-1).reverse().join('');
      }

      $('#pilsubclasspergi').on('change', function() {
      hargapergi=this.value.split(",")[0];
      var totaladt=hargapergi*jumadt;
      var totalchd=hargapergi*jumchd;
      var totalinf=hargapergi*juminf;
      $('#formgo_selectedIDdep').val(this.value.split(",")[1]);
      totalhargapergi=totaladt+totalchd+totalinf;
      $('#pilihanpergi_harga').html("Total : IDR "+convertToRupiah(totalhargapergi));


      });
      $('#pilsubclasspulang').on('change', function() {
      hargapulang=this.value.split(",")[0];
      var totaladt=hargapulang*jumadt;
      var totalchd=hargapulang*jumchd;
      var totalinf=hargapulang*juminf;
      $('#formgo_selectedIDret').val(this.value.split(",")[1]);
      totalhargapulang=totaladt+totalchd+totalinf;
      $('#pilihanpulang_harga').html("Total : IDR "+convertToRupiah(totalhargapulang));


      });

        function pilihPergi(bahanid
          ,jampergiberangkat
          ,jampergitiba
          ,harga
          ,namakereta
          ,kumpulaniddep
          ,hasildiskon
          ,kepodep
          ){
            var totaladt=harga*jumadt;
            var totalchd=harga*jumchd;
            var totalinf=harga*juminf;
            //harga=totaladt+totalchd+totalinf;

            $('#labelpilihanpergi').show('slow');
            $('#formgo_selectedIDdep').val(kumpulaniddep);
             $('#pilsubclasspergi').find('option').remove();

        $('#formgo_TrainNoDep').val(bahanid);
        $('#formgo_keretaDep').val(namakereta);
        $('#formgo_hargaDep').val(kepodep);
        $('#formgo_tgl_dep').val(jampergiberangkat);
        $('#formgo_tgl_dep_tiba').val(jampergitiba);
        /**
       $("#"+idpilper).removeClass("kotakpilihb cell-view");
        $("#"+idpilper).addClass("kotakpilih cell-view");

        $("#"+bahanid+"_gbr").removeClass("kotakpilih cell-view");
        $("#"+bahanid+"_gbr").addClass("kotakpilihb cell-view");
      idpilper=bahanid+"_gbr";
      **/

      $('#labelkolomutama').show('slow');
        $('#pilihanpergi').show('slow');
        $('#divterpilih').show('slow');
        //  alert(jampergitiba);
                $('#pilihanpergi_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
                //  alert($('#pilihanpergi_gbr').attr('src'));
                 $('#pilihanpergi_jampergi').html(jampergiberangkat);
                $('#pilihanpergi_jamtiba').html(jampergitiba);
               $('#pilihanpergi_namakereta').html(namakereta+' '+bahanid);
                $('#pilihanpergi_harga').html("Rp "+convertToRupiah(hasildiskon));

                nilpil+=1;
                $(location).attr('href', '#labelkolomutama');

                               $('#pilihanpergi_totalwaktu').html($('#'+bahanid+'_totalwaktu').html());

              //  alert ('tes tes');
                  $( "#labelkolomutama" ).focusin();
                  if(pilrencana2=="O" || (pilrencana2=="R" && satuaja==1)){
                   $('#sisikiri').hide('slow');
                    $('#barissorting').hide('slow');
                   mintaformgo();
                 }else if(pilrencana2=="R" && satuaja==0){
                   mintaformret();
                 }
                 satuaja=0;

        }
        function pilihPulang(bahanid
          ,jampulangberangkat
          ,jampulangtiba
          ,harga
          ,namakereta
          ,kumpulanidret
          ,hasildiskon
          ,keporet
          ){

                    var totaladt=harga*jumadt;
                    var totalchd=harga*jumchd;
                    var totalinf=harga*juminf;
                //    harga=totaladt+totalchd+totalinf;
             $('#pilsubclasspulang').find('option').remove();

            $('#labelpilihanpulang').show('slow');
             $('#formgo_TrainNoRet').val(bahanid);
            $('#formgo_keretaRet').val(namakereta);
            $('#formgo_hargaRet').val(keporet);
            $('#formgo_tgl_ret').val(jampulangberangkat);
            $('#formgo_tgl_ret_tiba').val(jampulangtiba);


            $('#formgo_selectedIDret').val(kumpulanidret);
             $('#barissorting').hide('slow');
             $('#divterpilih').show('slow');
      $('#labelkolomutama').show('slow');
          $('#pilihanpulang').show('slow');
        //  alert(jampergitiba);
                $('#pilihanpulang_gbr').attr('src',$('#'+bahanid+'_img').attr('src'));
                //  alert($('#pilihanpergi_gbr').attr('src'));
                 $('#pilihanpulang_jampergi').html(jampulangberangkat);
                $('#pilihanpulang_jamtiba').html(jampulangtiba);
                $('#pilihanpulang_namakereta').html(namakereta+' '+bahanid);
                $('#pilihanpulang_harga').html("Rp "+convertToRupiah(hasildiskon));
                mintaformgo();
                satuaja=0;
                $('#pilihanpulang_totalwaktu').html($('#'+bahanid+'_totalwaktu').html());

              $(location).attr('href', '#labelkolomutama');

              $("#sisikanan").hide('slow');
        }
        $('#tomubahpilihanpergi').on('click', function() {
        satuaja=1;
        $('#divterpilih').hide('slow');
        $('#sisikiri').show('slow');
        $('#barissorting').show('slow');
        });
        $('#tomubahpilihanpulang').on('click', function() {
        $('#sisikanan').show('slow');
        $('#divterpilih').hide('slow');
        $('#barissorting').show('slow');
        });
        $('#tomback').on('click', function() {
        $('#tomback').hide('slow');
        $('#sebelahkiri').show('slow');
        });
        function mintaformgo(){
        if(pilrencana2=="R"){
          $('#divpilihnextorulang').show('slow');
          $('#tomgoret').hide();
        }else if(pilrencana2=="O"){
          $('#tomgoret').hide();
          $('#divpilihnextorulang').show('slow');
        }
        }
        function mintaformret(){
           $("#sisikiri").hide('slow');
           $("#sisikanan").show('slow');
        if(pilrencana2=="R"){
           $('#divpilihnextorulang').hide();
           $('#tomgoret').show('slow');
        }else if(pilrencana2=="O"){
           $('#tomgoret').hide();
           $('#divpilihnextorulang').show('slow');
        }
        }

        function sembunyi(){
              $('#loadmaskapai').hide('slow');
        }
        function cari(){
        nilpil=0;
        if((Number(jumadt)+Number(jumchd)+Number(juminf))<=7){
          if (isMobile.matches) {
            $("#tomback").show('slow');
            $("#sebelahkiri").hide('slow');
            //$('#titikup')[0].scrollIntoView(true);
          }
        $('#labelpilihanpergi').hide();
        $('#labelpilihanpulang').hide();
        $("#barissorting").show('slow');
        $("#sisikanan").hide();
        $('#divpilihnextorulang').hide();
        $('#tomgoret').hide();
        //  alert("Memulai pencarian");
                //  $('#hasilkirim').html("<b>Hello world!</b>");
                if(pilrencana=="O"){
                    $("#sisikanan").hide('slow');
                    $("#sisikiri").show('slow');
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-12");
                  }else{
                    $("#sisikiri").show('slow');
                    //$("#sisikanan").show('slow');
                      //  $("#sisikanan").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikanan").addClass("col-xs-12 col-sm-8 col-md-6");
                      //  $("#sisikiri").removeClass("col-xs-12 col-sm-8 col-md-12");
                      //  $("#sisikiri").addClass("col-xs-12 col-sm-8 col-md-6");
                  }
                urljadwalb=urljadwal;
                urljadwalb+="org/"+statasal;
                urljadwalb+="/des/"+stattujuan;
                urljadwalb+="/trip/"+pilrencana;
                urljadwalb+="/tglberangkat/"+tglberangkat;
                urljadwalb+="/tglpulang/"+tglpulang;
                urljadwalb+="/jumadt/"+jumadt;
                urljadwalb+="/jumchd/"+jumchd;
                urljadwalb+="/juminf/"+juminf;
                //akhir urljadwalb+="ac/";
                        $('#loading_kai').show('fadeOut');

                        $('#dafpergi').html('');
                        $('#dafpulang').html('');

                ambildata_qg();

                pilrencana2=pilrencana;
        $('#formatas').hide('slow');
               $('#pilihanpergi').hide();
                $('#pilihanpulang').hide();

                        $('#labelkolomutama').hide('slow');
              $('#tomulangipencarian').show('slow');


              //  alert(urljadwalb);
              setkolom();
            }else{
              $('#bahanmodal').dialog({ modal: true });
              // $( "#bahanmodal" ).show();
            //  alert ('JUMLAH PENUMPANG TIDAK BOLEH LEBIH DARI TUJUH ');
            }
        }


        var ajaxku_qg;
        function ambildata_qg(){
          ajaxku_qg = buatajax();
          var url=urljadwalb;
          ajaxku_qg.onreadystatechange=stateChanged_qg;
          ajaxku_qg.open("GET",url,true);
          ajaxku_qg.send(null);
        }
         function stateChanged_qg(){
           var data;
            if (ajaxku_qg.readyState==4){
              data=ajaxku_qg.responseText;
              if(data.length>0){
               }else{
               }
                $('#loading_kai').hide('slow');
                        tambahData(data);
             }
        }

        function tambahData(data){
            //$('#dafpergi').html(data+$('#dafpergi').html());
            //$('#dafpulang').html($('.bagpulang').html()+$('#dafpulang').html());
            //$('.bagpulang').remove();

            $(data).each(function(){
              if($(this).attr('isiitem')=="1"){
              if($(this).attr('untuk')=="pergi"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var waktu=$(this).attr('data-waktu');

              var isiHTML='<div class="list-item-entry isiitempergi isiitem" id="'+temp_id+'"  data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
              //alert(waktu);
              var valueToPush = new Array();
              valueToPush[0] = temp_id;
              valueToPush[1] = biaya;
              valueToPush[2] = waktu;
              valueToPush[3] = isiHTML;
              //dafHTMLPergi.push(valueToPush);
              $('#dafpergi').html(isiHTML+$('#dafpergi').html());


              }else if($(this).attr('untuk')=="pulang"){
              var temp_id=$(this).attr('id');
              var biaya=$(this).attr('data-harga');
              var waktu=$(this).attr('data-waktu');

                var isiHTML='<div class="list-item-entry isiitempulang isiitem" id="'+temp_id+'" data-waktu="'+waktu+'" data-harga="'+biaya+'">'+$(this).html()+'</div>';
                //alert(temp_id);

              var valueToPush = new Array();
              valueToPush[0] = temp_id;
              valueToPush[1] = biaya;
              valueToPush[2] = waktu;
              valueToPush[3] = isiHTML;
              //dafHTMLPulang.push(valueToPush);
              $('#dafpulang').html(isiHTML+$('#dafpulang').html());


            }
          }});


        }

        jQuery.fn.sortElements = (function(){

            var sort = [].sort;

            return function(comparator, getSortable) {

                getSortable = getSortable || function(){return this;};

                var placements = this.map(function(){

                    var sortElement = getSortable.call(this),
                        parentNode = sortElement.parentNode,

                        // Since the element itself will change position, we have
                        // to have some way of storing its original position in
                        // the DOM. The easiest way is to have a 'flag' node:
                        nextSibling = parentNode.insertBefore(
                            document.createTextNode(''),
                            sortElement.nextSibling
                        );

                    return function() {

                        if (parentNode === this) {
                            throw new Error(
                                "You can't sort elements if any one is a descendant of another."
                            );
                        }

                        // Insert before flag:
                        parentNode.insertBefore(this, nextSibling);
                        // Remove flag:
                        parentNode.removeChild(nextSibling);

                    };

                });

                return sort.call(this, comparator).each(function(i){
                    placements[i].call(getSortable.call(this));
                });

            };

        })();
        var kolomsort="";
        function sorterAsc(a, b) {
        return Number(a.getAttribute(kolomsort)) > Number(b.getAttribute(kolomsort));
        };
        function sorterDesc(a, b) {
        return Number(a.getAttribute(kolomsort)) < Number(b.getAttribute(kolomsort));
        };


        function sortAsc(kolom){
          kolomsort=kolom;
          var sortedDivs = $(".isiitempergi").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpergi').append(value);
          });


          var sortedDivs = $(".isiitempulang").toArray().sort(sorterAsc);
          console.log(sortedDivs);
          $.each(sortedDivs, function (index, value) {
              $('#dafpulang').append(value);
          });
        }

        function sortDesc(kolom){
          kolomsort=kolom;
            var sortedDivs = $(".isiitempergi").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpergi').append(value);
            });


            var sortedDivs = $(".isiitempulang").toArray().sort(sorterDesc);
            console.log(sortedDivs);
            $.each(sortedDivs, function (index, value) {
                $('#dafpulang').append(value);
            });
        }

        var ajaxku;
        function ambildata(){
          ajaxku = buatajax();
          var url="{{ url('/') }}/jadwalPesawat";
          //url=url+"?q="+nip;
          //url=url+"&sid="+Math.random();
          ajaxku.onreadystatechange=stateChanged;
          ajaxku.open("GET",url,true);
          ajaxku.send(null);
        }
        function buatajax(){
          if (window.XMLHttpRequest){
            return new XMLHttpRequest();
          }
          if (window.ActiveXObject){
             return new ActiveXObject("Microsoft.XMLHTTP");
           }
           return null;
         }
         function stateChanged(){
           var data;
            if (ajaxku.readyState==4){
              data=ajaxku.responseText;
              if(data.length>0){
                //document.getElementById("hasilkirim").html = data;

                        $('#hasilkirim').append(data);
               }else{
                // document.getElementById("hasilkirim").html = "";
                      //   $('#hasilkirim').html("");
               }
             }
        }

        @if($otomatiscari==1)
        statasal="{{$org}}";
        stattujuan="{{$des}}";
        pilrencana="{{$trip}}";
        tglberangkat="{{$tglberangkat}}";
        tglpulang="{{$tglpulang}}";
        jumadt="{{$jumadt}}";
        jumchd="{{$jumchd}}";
        juminf="{{$juminf}}";
        kotasal="{{$kotasal}}";
        kottujuan="{{$kottujuan}}";
        cari();
        @endif
        </script>
		@endsection

		@endsection
