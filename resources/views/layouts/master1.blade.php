<html>
    <head>
      <meta charset="utf-8">
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta name="format-detection" content="telephone=no" />

      <link rel="shortcut icon" href="favicon.ico"/>
      <link href="{{ URL::asset('asettemplate1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ URL::asset('asettemplate1/css/jquery-ui.structure.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ URL::asset('asettemplate1/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="../../../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
      <link href="{{ URL::asset('asettemplate1/css/style.css') }}" rel="stylesheet" type="text/css"/>
      <script type="application/javascript" src="{{ URL::asset('asettemplate1/js/jquery-2.2.2.min.js')}}"></script>

       @yield('sebelumtitel')
      <title>ATA Travel</title>
    </head>
    <body data-color="theme-1">
            @section('sidebargaya')
      <div class="style-page">
             <div class="wrappers">
              <div class="conf-button">
               <span class="fa fa-cog"></span>
                <h6>Theme options</h6>
              </div>
              <a href="index-2.html" class="site-logo"><img src="{{ URL::asset('asettemplate1/img/theme-1/logo_dark.png') }}" alt=""></a>
           <div class="color-block">
             <h5>change color</h5>
              <div class="entry bg-1" data-color="theme-1"></div>
            <div class="entry bg-2" data-color="theme-2"></div>
            <div class="entry bg-3" data-color="theme-3"></div>
            <div class="entry bg-4" data-color="theme-4"></div>
            <div class="entry bg-5" data-color="theme-5"></div>
            <div class="entry bg-6" data-color="theme-6"></div>
            <div class="entry bg-7" data-color="theme-7"></div>
            <div class="entry bg-8" data-color="theme-8"></div>
            <div class="entry bg-9" data-color="theme-9"></div>
           </div>
           <div class="color-block">
             <h5>Layout style</h5>
               <div class="check-option active"><span class="boxed">Boxed</span></div>
             <div class="check-option"><span class="noboxed">Wide</span></div>
           </div>
           <div class="color-block">
             <h5>Elements style</h5>
             <div class="check-option active"><span class="rounded">Rounded</span></div>
             <div class="check-option"><span class="norounded">Not rounded</span></div>
           </div>
           <div class="color-block">
              <h5>Header style</h5>
              <div class="header-style">
               <a href="menu_style_1.html" class="active">
                 <img src="{{ URL::asset('asettemplate1/img/landing/header_1.png') }}" alt="">
               </a>
             <a href="menu_style_2.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_2.png') }}" alt="">
             </a>
             <a href="menu_style_3.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_3.png') }}" alt="">
             </a>
             <a href="menu_style_4.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_4.png') }}" alt="">
             </a>
             <a href="menu_style_5.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_5.png') }}" alt="">
             </a>
             <a href="menu_style_6.html">
                <img src="{{ URL::asset('asettemplate1/img/landing/header_6.png') }}" alt="">
             </a>
            </div>
           </div>
           <div class="color-block">
              <h5>Footer style</h5>
                <div class="header-style">
                <a href="footer_style_1.html" class="active">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_1.png') }}" alt="">
                </a>
                <a href="footer_style_2.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_2.png') }}" alt="">
                </a>
                <a href="footer_style_3.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_3.png') }}" alt="">
                </a>
                <a href="footer_style_4.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_4.png') }}" alt="">
                </a>
                <a href="footer_style_5.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_5.png') }}" alt="">
                </a>
            </div>
           </div>
         </div>
         </div>
     @show
     <div class="loading">
   	<div class="loading-center">
   		<div class="loading-center-absolute">
   			<div class="object object_four"></div>
   			<div class="object object_three"></div>
   			<div class="object object_two"></div>
   			<div class="object object_one"></div>
   		</div>
   	</div>
     </div>

   @section('header')
       <header class="color-1 hovered menu-3">
        <div class="container">
        	  <div class="row">
        	  	 <div class="col-md-12">
       	  	    <div class="nav">
        	  	    <a href="{{ url('/') }}" class="logo">
        	  	    	<img src="{{ URL::asset('uploads/images/logoweb.png') }}" alt="lets travel">
        	  	    </a>
        	  	    <div class="nav-menu-icon">
     		      <a href="#"><i></i></a>
     		    </div>
        	  	 	<nav class="menu">
     			  	<ul>
     					<li class="{{ Request::segment(1) === null ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>

              <li class="{{ Request::segment(1) === 'flight' ? 'active' : null }}"><a href="{{ url('/') }}/flight">Penerbangan</a></li>
     					<li class="type-1 {{ Request::segment(1) === 'about' ? 'active' : null }}"><a href="#">About<span class="fa fa-angle-down"></span></a>
     						<ul class="dropmenu">
     						<li><a href="{{ url('/') }}/about/about">Tentang Kami</a></li>
     						<li><a href="{{ url('/') }}/about/galeri">Galeri</a></li>
     						<li><a href="{{ url('/') }}/about/blog">Blog</a></li>
     						<li><a href="{{ url('/') }}/about/faq">Tanya Jawab</a></li>
     						<li><a href="{{ url('/') }}/about/policies">Kebijakan</a></li>
     						<li><a href="{{ url('/') }}/about/kontak">Kontak</a></li>
     						</ul>
     					</li>


     			  	</ul>
     		   </nav>
     		   </div>
        	  	 </div>
        	  </div>
        </div>
       </header>
   @show

 @yield('kontenweb')

        @section('footer')

        @foreach($datakontak as $dakon)
        <?php

        if($dakon->label=="handphone"){
          $handphone=$dakon->isi;
        }else if($dakon->label=="alamat"){
          $alamat=$dakon->isi;
        }else if($dakon->label=="email"){
          $email=$dakon->isi;
        }else if($dakon->label=="googlemap"){
          $googlemap=$dakon->isi;
        }else if($dakon->label=="deskripsi"){
          $deskripsi=$dakon->isi;
        }

         ?>
        @endforeach
            <footer class="bg-dark type-2">
            	<div class="container">
            		<div class="row">
            			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            				<div class="footer-block">
            					<img src="{{ URL::asset('uploads/images/logoweb.png') }}" alt="" class="logo-footer">
            					<div class="f_text color-grey-7">{{$deskripsi}}</div>
            					<div class="footer-share">
            						<a href="#"><span class="fa fa-facebook"></span></a>
            						<a href="#"><span class="fa fa-twitter"></span></a>
            						<a href="#"><span class="fa fa-google-plus"></span></a>
            						<a href="#"><span class="fa fa-pinterest"></span></a>
            					</div>
            				</div>
            			</div>
            			<div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
        				   <div class="footer-block">
        						<h6>Travel News</h6>
        						<div class="f_news clearfix">
        							<a class="f_news-img black-hover" href="#">
        								<img class="img-responsive" src="img/home_6/news_1.jpg" alt="">
        								<div class="tour-layer delay-1"></div>
        							</a>
        							<div class="f_news-content">
        								<a class="f_news-tilte color-white link-red" href="#">amazing place</a>
        								<span class="date-f">Mar 18, 2015</span>
        								<a href="#" class="r-more">read more</a>
        							</div>
        						</div>
        						<div class="f_news clearfix">
        							<a class="f_news-img black-hover" href="#">
        								<img class="img-responsive" src="img/home_6/news_2.jpg" alt="">
        								<div class="tour-layer delay-1"></div>
        							</a>
        							<div class="f_news-content">
        								<a class="f_news-tilte color-white link-red" href="#">amazing place</a>
        								<span class="date-f">Mar 18, 2015</span>
        								<a href="#" class="r-more">read more</a>
        							</div>
        						</div>
        						<div class="f_news clearfix">
        							<a class="f_news-img black-hover" href="#">
        								<img class="img-responsive" src="img/home_6/news_1.jpg" alt="">
        								<div class="tour-layer delay-1"></div>
        							</a>
        							<div class="f_news-content">
        								<a class="f_news-tilte color-white link-red" href="#">amazing place</a>
        								<span class="date-f">Mar 18, 2015</span>
        								<a href="#" class="r-more">read more</a>
        							</div>
        						</div>
        				   </div>
        				</div>
            			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            			   <div class="footer-block">
                             <h6>Tags:</h6>
            			      <a href="#" class="tags-b">flights</a>
            			      <a href="#" class="tags-b">traveling</a>
            			      <a href="#" class="tags-b">sale</a>
            			      <a href="#" class="tags-b">cruises</a>
            			      <a href="#" class="tags-b">cars</a>
            			      <a href="#" class="tags-b">hotels</a>
            			      <a href="#" class="tags-b">tours</a>
            			      <a href="#" class="tags-b">booking</a>
            			      <a href="#" class="tags-b">countries</a>
        				   </div>
        				</div>
        				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="footer-block">
                             <h6>Contact Info</h6>

                   									 	   @foreach($datakontak as $dakon)
                   											 <?php
                   											 if($dakon->label=="handphone"){
                   												 $handphone=$dakon->isi;
                   											 }else if($dakon->label=="alamat"){
                   												 $alamat=$dakon->isi;
                   											 }if($dakon->label=="email"){
                   												 $email=$dakon->isi;
                   											 }if($dakon->label=="googlemap"){
                   												 $googlemap=$dakon->isi;
                   											 }if($dakon->label=="deskripsi"){
                   												 $deskripsi=$dakon->isi;
                   											 }

                   											  ?>
                   								 			@endforeach
                               <div class="contact-info">
                    	<div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="tel:{{$handphone}}">{{$handphone}}</a></div>
        						<div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="mailto:{{$email}}">{{$email}}</a></div>

        					</div>
        				   </div>
        				</div>
            		</div>
            	</div>
            	<div class="footer-link bg-black">
            	  <div class="container">
            	  	<div class="row">
            	  		<div class="col-md-12">
            	  		    <div class="copyright">
        						<span>&copy; 2015 All rights reserved. LET'STRAVEL</span>
        					</div>
            	  			<ul>
        						<li><a class="link-aqua" href="#">Privacy Policy </a></li>
        						<li><a class="link-aqua" href="#">About Us</a></li>
        						<li><a class="link-aqua" href="#">Support </a></li>
        						<li><a class="link-aqua" href="#">FAQ</a></li>
        						<li><a class="link-aqua" href="#">Blog</a></li>
        						<li><a class="link-aqua" href="#"> Forum</a></li>
        					</ul>
            	  		</div>
            	  	</div>
            	  </div>
            	</div>
            </footer>
@show
       <script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery-ui.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js') }}"></script>
         <script src="{{ URL::asset('asettemplate1/js/all.js') }}"></script>
         @yield('akhirbody')
    </body>
</html>
