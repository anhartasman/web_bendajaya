<!DOCTYPE html>
<html>

<!-- Mirrored from clear.lcrm.in/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Apr 2017 16:00:28 GMT -->
<head>
    <meta charset="UTF-8">
    <title>Clear Admin Template | Clear Admin Template </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon.ico') }}"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/app.css') }}"/>
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" href="{{ URL::asset('vendors/swiper/css/swiper.min.css') }}">
    <link href="{{ URL::asset('vendors/nvd3/css/nv.d3.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('vendors/lcswitch/css/lc_switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendors/simple-line-icons/css/simple-line-icons.css') }}">

    <link href="{{ URL::asset('css/custom_css/dashboard1.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/custom_css/dashboard1_timeline.css') }}" rel="stylesheet"/>
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    <!--end of page level css-->
    @yield('didalamhead')
</head>
<body class="skin-default">
<div class="preloader">
    <div class="loader_img"><img src="{{ URL::asset('img/loader.gif') }}" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="{{url('/admin')}}" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the marginin -->
            <img src="{{ URL::asset('img/logo.png') }}" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw ti-menu"></i>
            </a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
              <!-- hapus
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw ti-email black"></i>
                        <span class="label label-success">2</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages table-striped">
                        <li class="dropdown-title">New Messages</li>
                        <li>
                            <a href="#" class="message striped-col">
                                <img class="message-image img-circle" src="img/authors/avatar7.jpg" alt="avatar-image">

                                <div class="message-body"><strong>Ernest Kerry</strong>
                                    <br>
                                    Can we Meet?
                                    <br>
                                    <small>Just Now</small>
                                    <span class="label label-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="message">
                                <img class="message-image img-circle" src="img/authors/avatar6.jpg" alt="avatar-image">

                                <div class="message-body"><strong>John</strong>
                                    <br>
                                    Dont forgot to call...
                                    <br>
                                    <small>5 minutes ago</small>
                                    <span class="label label-success label-mini msg-lable">New</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="message striped-col">
                                <img class="message-image img-circle" src="img/authors/avatar5.jpg" alt="avatar-image">

                                <div class="message-body">
                                    <strong>Wilton Zeph</strong>
                                    <br>
                                    If there is anything else &hellip;
                                    <br>
                                    <small>14/10/2014 1:31 pm</small>
                                </div>

                            </a>
                        </li>
                        <li>
                            <a href="#" class="message">
                                <img class="message-image img-circle" src="img/authors/avatar1.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Jenny Kerry</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="message striped-col">
                                <img class="message-image img-circle" src="img/authors/avatar.jpg" alt="avatar-image">
                                <div class="message-body">
                                    <strong>Tony</strong>
                                    <br>
                                    Let me know when you free
                                    <br>
                                    <small>5 days ago</small>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer"><a href="#"> View All messages</a></li>
                    </ul>

                </li>
                <!--rightside toggle-->
                <li>
                    <a href="#" class="dropdown-toggle toggle-right" data-toggle="dropdown">
                        <i class="fa fa-fw ti-view-list black"></i>
                        <span class="label label-danger">9</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle padding-user" data-toggle="dropdown">
                        <img src="{{ url('/') }}/uploads/images/{{$tb_user->filefoto}}" width="35" class="img-circle img-responsive pull-left"
                             height="35" alt="User Image">
                        <div class="riot">
                            <div>
                                {{$tb_user->name}}
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ url('/') }}/uploads/images/{{$tb_user->filefoto}}" class="img-circle" alt="User Image">
                            <p> {{$tb_user->name}}</p>
                        </li>
                        <!-- Menu Body -->
                        <li class="p-t-3"><a href="#"> <i class="fa fa-fw ti-user"></i> My Profile </a>
                        </li>
                        <li role="presentation"></li>
                        <li><a href="{{ url('/admin/akun/pengaturan') }}"> <i class="fa fa-fw ti-settings"></i> Account Settings </a></li>
                        <li role="presentation" class="divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#">
                                    <i class="fa fa-fw ti-lock"></i>
                                    Lock
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="{{url('/admin/logout')}}">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="#">
                            <img src="{{ url('/') }}/uploads/images/{{$tb_user->filefoto}}" class="img-circle" alt="User Image"></a>
                        <div class="content-profile">
                            <h4 class="media-heading">{{$tb_user->name}}</h4>
                            <!-- hapus
                            <ul class="icon-list">
                                <li>
                                    <a href="users.html">
                                        <i class="fa fa-fw ti-user"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="lockscreen.html">
                                        <i class="fa fa-fw ti-lock"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="edit_user.html">
                                        <i class="fa fa-fw ti-settings"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="login.html">
                                        <i class="fa fa-fw ti-shift-right"></i>
                                    </a>
                                </li>
                            </ul>
                          -->
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li class="{{ Request::segment(2) === null ? 'active' : null }}" id="active">
                        <a href="{{url('/admin')}}">
                            <i class="menu-icon ti-desktop"></i>
                            <span class="mm-text ">Dashboard </span>
                        </a>
                    </li>
                    <?php
                    $arsidemenu=array();

                    switch(getUserInfo('user_pangkat')){
                   case 1: // buyer
                   $arsidemenu=array("gruplawyer"
                   ,"/admin/lawyers/logtelepon"
                   ,"grupcallcenter"
                   ,"/admin/callcenter/dokumentasi"
                   ,"/admin/callcenter/orderaktif"
                   ,"/admin/callcenter/arsiporder");
                   break;
                   case 2: // driver
                   $arsidemenu=array("gruplawyer"
                   ,"/admin/lawyers/logtelepon"
                   ,"/admin/lawyers/billingfee"
                   ,"grupkeuangan"
                   ,"/admin/keuangan/pengajuantopup"
                   ,"/admin/keuangan/arsiptopup"
                   ,"/admin/keuangan/saldodeposit"
                   ,"/admin/keuangan/transferfee"
                   ,"/admin/keuangan/arsiptransferfee"
                   ,"/admin/keuangan/transferfeeklien"
                   ,"/admin/keuangan/arsiptransferfeeklien"
                 );
                   break;
                   case 3: // seller
                   $arsidemenu=array(
                   "grupdagang"
                   ,"/admin/product_category/list"
                   ,"/admin/product/list"
                   );
                   break;
                   case 4: // admin
                   $arsidemenu=array("grupuser"
                   ,"/admin/users/manage"
                   ,"grupxxxkontenapps"
                   ,"/admin/kontenapps/berita"
                   ,"grupxxxlawyer"
                   ,"/admin/lawyers/list"
                   ,"/admin/lawyers/rating"
                   ,"/admin/lawyers/billingfee"
                   ,"/admin/lawyers/logtelepon"
                   ,"grupkeuangan"
                   ,"/admin/keuangan/pengajuantopup"
                   ,"/admin/keuangan/arsiptopup"
                   ,"/admin/keuangan/saldodeposit"
                   ,"/admin/keuangan/transaksijualbeli"
                   ,"grupxxxcallcenter"
                   ,"/admin/callcenter/dokumentasi"
                   ,"/admin/callcenter/arsiporder"
                   ,"grupdagang"
                   ,"/admin/product_category/list"
                   ,"/admin/transport_service/list"
                   ,"gruplaporan"
                   ,"/admin/laporan/complain"
                   );
                   break;
                    } ?>
                    @if(in_array("grupuser", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'users' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-user"></i>
                            <span>
                                    User
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        @if(in_array("/admin/users/manage", $arsidemenu))
                        <ul class="sub-menu {{ Request::segment(3) === 'manage' ? 'active' : null }}">
                            <li>
                                <a href="{{url('/admin/users/manage')}}">
                                    <i class="icon-organization icons"></i> User
                                </a>
                            </li>


                        </ul>
                        @endif
                    </li>
                    @endif
                    @if(in_array("grupkontenapps", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'kontenapps' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-list"></i>
                            <span>
                                    Konten apps
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                        @if(in_array("/admin/kontenapps/berita", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/kontenapps/berita')}}">
                                    <i class="fa fa-users"></i> Berita
                                </a>
                            </li>
                          @endif

                        </ul>
                    </li>
                    @endif
                    @if(in_array("gruplawyer", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'lawyers' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-list"></i>
                            <span>
                                    Lawyer
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                        @if(in_array("/admin/lawyers/list", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/lawyers/list')}}">
                                    <i class="fa fa-users"></i> Manage
                                </a>
                            </li>
                          @endif
                            <li>
                            @if(in_array("/admin/lawyers/rating", $arsidemenu))
                                <a href="{{url('/admin/lawyers/rating')}}">
                                    <i class="fa fa-book "></i> Laporan Kinerja
                                </a>
                            </li>
                          @endif
                          @if(in_array("/admin/lawyers/billingfee", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/lawyers/billingfee')}}">
                                    <i class="fa fa-money "></i> Billing fee
                                </a>
                            </li>
                          @endif
                          @if(in_array("/admin/lawyers/logtelepon", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/lawyers/logtelepon')}}">
                                    <i class="icon-phone icons"></i> Log telepon
                                </a>
                            </li>
                          @endif

                        </ul>
                    </li>
                    @endif
                    @if(in_array("grupkeuangan", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'keuangan' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-list"></i>
                            <span>
                                    Keuangan
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                        @if(in_array("/admin/keuangan/transaksijualbeli", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/transaksijualbeli')}}">
                                    <i class="fa fa-money "></i> Transaksi jual beli
                                </a>
                            </li>
                            @endif
                        @if(in_array("/admin/keuangan/pengajuantopup", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/pengajuantopup')}}">
                                    <i class="fa fa-money "></i> Pengajuan top up
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/keuangan/arsiptopup", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/arsiptopup')}}">
                                    <i class="icon-briefcase icons "></i> Arsip top up
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/keuangan/saldodeposit", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/saldodeposit')}}">
                                    <i class="fa fa-bank "></i> Saldo deposit
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/keuangan/transferfeeklien", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/transferfeeklien')}}">
                                    <i class="fa fa-money "></i> Transfer komisi klien
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/keuangan/arsiptransferfeeklien", $arsidemenu))
                              <li>
                                  <a href="{{url('/admin/keuangan/arsiptransferfeeklien')}}">
                                      <i class="icon-briefcase icons "></i> Arsip Transfer komisi klien
                                  </a>
                              </li>
                            @endif
                            @if(in_array("/admin/keuangan/transferfee", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/keuangan/transferfee')}}">
                                    <i class="fa fa-money "></i> Transfer fee
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/keuangan/arsiptransferfee", $arsidemenu))
                              <li>
                                  <a href="{{url('/admin/keuangan/arsiptransferfee')}}">
                                      <i class="icon-briefcase icons "></i> Arsip Transfer Fee
                                  </a>
                              </li>
                            @endif

                        </ul>
                    </li>
                    @endif
                    @if(in_array("grupcallcenter", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'callcenter' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-list"></i>
                            <span>
                                    Call center
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                          @if(in_array("/admin/callcenter/dokumentasi", $arsidemenu))
                          <li>
                                <a href="{{url('/admin/callcenter/dokumentasi')}}">
                                    <i class="icon-book-open icons "></i> Dokumentasi aktif
                                </a>
                            </li>
                            @endif
                            @if(in_array("/admin/callcenter/orderaktif", $arsidemenu))
                              <li>
                                <a href="{{url('/admin/callcenter/orderaktif')}}">
                                    <i class="icon-book-open icons "></i> Order aktif
                                </a>
                            </li>
                          @endif
                          @if(in_array("/admin/callcenter/arsiporder", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/callcenter/arsiporder')}}">
                                    <i class="icon-briefcase icons"></i> Arsip order
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if(in_array("gruplaporan", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'laporan' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-list"></i>
                            <span>
                                    Laporan
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu {{ Request::segment(2) === 'logpan' ? 'active' : null }}">
                          @if(in_array("/admin/laporan/complain", $arsidemenu))
                            <li>
                                <a href="{{url('/admin/laporan/complain')}}">
                                    <i class="icon-call-in icons"></i> Complain
                                </a>
                            </li>
                          @endif

                        </ul>
                    </li>
                    @endif
                    @if(in_array("grupdagang", $arsidemenu))
                    <li class="menu-dropdown {{ Request::segment(2) === 'product_category' ? 'active' : null }}">
                        <a href="#">
                            <i class="menu-icon fa fa-user"></i>
                            <span>
                                    Toko Online
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        @if(in_array("/admin/product_category/list", $arsidemenu))
                        <ul class="sub-menu {{ Request::segment(3) === 'list' ? 'active' : null }}">
                            <li>
                                <a href="{{url('/admin/product_category/list')}}">
                                    <i class="icon-organization icons"></i> Kategori Produk
                                </a>
                            </li>


                        </ul>
                        @endif
                        @if(in_array("/admin/product/list", $arsidemenu))
                        <ul class="sub-menu {{ Request::segment(3) === 'list' ? 'active' : null }}">
                            <li>
                                <a href="{{url('/admin/product/list')}}">
                                    <i class="icon-organization icons"></i> Produk
                                </a>
                            </li>


                        </ul>
                        @endif
                        @if(in_array("/admin/transport_service/list", $arsidemenu))
                        <ul class="sub-menu {{ Request::segment(3) === 'list' ? 'active' : null }}">
                            <li>
                                <a href="{{url('/admin/transport_service/list')}}">
                                    <i class="icon-organization icons"></i> Transport Provider
                                </a>
                            </li>


                        </ul>
                        @endif
                    </li>
                    @endif

                </ul>
                <!-- / .navigation --> </div>
            <!-- menu --> </section>
        <!-- /.sidebar --> </aside>
    <aside class="right-side">

@yield('statheader')
@if(isset($dafmenu))
      <section class="content-header">
          <h1>
              {{$namamenu}}
          </h1>
          <ol class="breadcrumb">
            <li>
                <a href="{{url('/admin')}}">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            @foreach($dafmenu as $key=>$value)
            @if($key=="null")
            <li class="active">
                {{$value}}
            </li>
            @else
              <li>
                  <a href="{{$key}}"> {{$value}}</a>
              </li>
            @endif
              @endforeach

          </ol>
      </section>
      @endif
@yield('kontenweb')

        <!-- /.content --> </aside>
    <!-- /.right-side --> </div>
<!-- ./wrapper -->
<!-- global js -->
<div id="qn"></div>
<script src="{{ URL::asset('ajax/libs/jquery/1.12.4/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap@3.3.7,bootstrap.switch@3.3.2,jquery.nicescroll@3.6.0.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}" type="text/javascript"></script>
<!-- end of global js -->

<!-- begining of page level js -->
<!--Sparkline Chart-->
<script type="text/javascript" src="{{ URL::asset('js/custom_js/sparkline/jquery.flot.spline.js') }}"></script>
<!-- flip --->
<script type="text/javascript" src="{{ URL::asset('vendors/flip/js/jquery.flip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/lcswitch/js/lc_switch.min.js') }}"></script>
<!--flot chart-->
<script type="text/javascript" src="{{ URL::asset('vendors/flotchart/js/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/flotchart/js/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/flotchart/js/jquery.flot.stack.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/flotspline/js/jquery.flot.spline.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/flot.tooltip/js/jquery.flot.tooltip.js') }}"></script>
<!--swiper-->
<script type="text/javascript" src="{{ URL::asset('vendors/swiper/js/swiper.min.js') }}"></script>
<!--chartjs-->
<script src="{{ URL::asset('vendors/chartjs/js/Chart.js') }}"></script>
<!--nvd3 chart-->
<script type="text/javascript" src="{{ URL::asset('js/nvd3/d3.v3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/nvd3/js/nv.d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('vendors/advanced_newsTicker/js/newsTicker.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-ui.js') }}"></script>
<!-- end of page level js -->
<script>
$(".rupiahjs").on('keyup', function(e)
	{
		$(this).val(formatRupiah(this.value, 'Rp '));
	});
function formatRupiah(angka, prefix)
{
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split	= number_string.split(','),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}
</script>
@yield('bagianfooter')
</body>
</html>
