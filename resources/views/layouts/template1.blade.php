<html>
    <head>
      <meta charset="utf-8">
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta name="format-detection" content="telephone=no" />

      <link rel="shortcut icon" href="favicon.ico"/>
      <link href="{{ URL::asset('asettemplate1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ URL::asset('asettemplate1/css/jquery-ui.structure.min.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ URL::asset('asettemplate1/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="../../../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
      <link href="{{ URL::asset('asettemplate1/css/style.css') }}" rel="stylesheet" type="text/css"/>
      <script type="application/javascript" src="{{ URL::asset('asettemplate1/js/jquery-2.2.2.min.js')}}"></script>
      <!-- jquery Dialog -->
  		<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.maskMoney.min.js')}}"></script>
     <script src="{{ URL::asset('assets/js/jquery-ui.min.js')}}"></script>
     <link rel="stylesheet" href="{{ URL::asset('asettemplate1/css/jquery-ui.css')}}">
     <link href="{{ URL::asset('assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
     <script src="{{ URL::asset('assets/js/bootstrap-toggle.min.js')}}"></script>
       @yield('sebelumtitel')
      <title>{{$infowebsite['judulweb']}} </title>
    </head>
    <body data-color="{{$settingan_member->style_color}}" class="{{$settingan_member->style_element}}">
            @section('sidebargaya')
<!--
      <div class="style-page">
             <div class="wrappers">
              <div class="conf-button">
               <span class="fa fa-cog"></span>
                <h6>Theme options</h6>
              </div>
              <a href="index-2.html" class="site-logo"><img src="{{ URL::asset('asettemplate1/img/theme-1/logo_dark.png') }}" alt=""></a>
           <div class="color-block">
             <h5>change color</h5>
              <div class="entry bg-1" data-color="theme-1"></div>
            <div class="entry bg-2" data-color="theme-2"></div>
            <div class="entry bg-3" data-color="theme-3"></div>
            <div class="entry bg-4" data-color="theme-4"></div>
            <div class="entry bg-5" data-color="theme-5"></div>
            <div class="entry bg-6" data-color="theme-6"></div>
            <div class="entry bg-7" data-color="theme-7"></div>
            <div class="entry bg-8" data-color="theme-8"></div>
            <div class="entry bg-9" data-color="theme-9"></div>
           </div>
           <div class="color-block">
             <h5>Layout style</h5>
               <div class="check-option active"><span class="boxed">Boxed</span></div>
             <div class="check-option"><span class="noboxed">Wide</span></div>
           </div>
           <div class="color-block">
             <h5>Elements style</h5>
             <div class="check-option active"><span class="rounded">Rounded</span></div>
             <div class="check-option"><span class="norounded">Not rounded</span></div>
           </div>
           <div class="color-block">
              <h5>Header style</h5>
              <div class="header-style">
               <a href="menu_style_1.html" class="active">
                 <img src="{{ URL::asset('asettemplate1/img/landing/header_1.png') }}" alt="">
               </a>
             <a href="menu_style_2.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_2.png') }}" alt="">
             </a>
             <a href="menu_style_3.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_3.png') }}" alt="">
             </a>
             <a href="menu_style_4.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_4.png') }}" alt="">
             </a>
             <a href="menu_style_5.html">
               <img src="{{ URL::asset('asettemplate1/img/landing/header_5.png') }}" alt="">
             </a>
             <a href="menu_style_6.html">
                <img src="{{ URL::asset('asettemplate1/img/landing/header_6.png') }}" alt="">
             </a>
            </div>
           </div>
           <div class="color-block">
              <h5>Footer style</h5>
                <div class="header-style">
                <a href="footer_style_1.html" class="active">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_1.png') }}" alt="">
                </a>
                <a href="footer_style_2.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_2.png') }}" alt="">
                </a>
                <a href="footer_style_3.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_3.png') }}" alt="">
                </a>
                <a href="footer_style_4.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_4.png') }}" alt="">
                </a>
                <a href="footer_style_5.html">
                 <img src="{{ URL::asset('asettemplate1/img/landing/footer_5.png') }}" alt="">
                </a>
            </div>
           </div>
         </div>
         </div>
-->
     @show
     <div class="loading">
   	<div class="loading-center">
   		<div class="loading-center-absolute">
   			<div class="object object_four"></div>
   			<div class="object object_three"></div>
   			<div class="object object_two"></div>
   			<div class="object object_one"></div>
   		</div>
   	</div>
     </div>
   @section('header')
       <header class="color-1 hovered menu-3">
        <div class="container">
        	  <div class="row">
        	  	 <div class="col-md-12">
       	  	    <div class="nav">
        	  	    <a href="{{ url('/') }}" class="logo">
        	  	    	<img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" width="212" height="36" alt="lets travel">
        	  	    </a>
        	  	    <div class="nav-menu-icon">
     		      <a href="#"><i></i></a>
     		    </div>
        	  	 	<nav class="menu">
     			  	<ul>
     					<li class="{{ Request::segment(1) === null ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>

              <li class="{{ Request::segment(1) === 'flight' ? 'active' : null }}"><a href="{{ url('/') }}/flight">Penerbangan</a></li>
              <li class="{{ Request::segment(1) === 'train' ? 'active' : null }}"><a href="{{ url('/') }}/train">Kereta Api</a></li>
              <li class="{{ $menuheader === 'hotel' ? 'active' : null }}"><a href="{{ url('/') }}/hotel">Hotel</a></li>
              <li class="{{ $menuheader === 'cekpesanan' ? 'active' : null }}"><a href="{{ url('/') }}/cekpesanan">Cek Pesanan</a></li>
     					<li class="type-1 {{ $menuheader === 'about' ? 'active' : null }}"><a href="#">Tentang<span class="fa fa-angle-down"></span></a>
     						<ul class="dropmenu">
     						<li><a href="{{ url('/') }}/about">Tentang Kami</a></li>
     						<li><a href="{{ url('/') }}/galeri">Galeri</a></li>
     						<li><a href="{{ url('/') }}/blog">Blog</a></li>
     						<li><a href="{{ url('/') }}/faq">Tanya Jawab</a></li>
     						<li><a href="{{ url('/') }}/policies">Kebijakan</a></li>
     						<li><a href="{{ url('/') }}/kontak">Kontak</a></li>
     						</ul>
     					</li>


     			  	</ul>
     		   </nav>
     		   </div>
        	  	 </div>
        	  </div>
        </div>
       </header>
   @show
 @section('kontenweb')
 @if(isset($dialogpolicy))
 <div id="dialog-confirm" title="Syarat & Ketentuan Transfer">
   <p> <span id="dialog-confirm_message">
     Dengan memilih pembayaran melalui Transfer, Anda menyetujui bahwa:
     <BR><BR>1) Jumlah transfer harus sesuai harga total (hingga 3 angka terakhir) dan dilakukan sebelum batas waktu pembayaran berakhir.
     <BR><BR>2) Transfer antarbank (transfer antar dua rekening dari bank berbeda) wajib menggunakan metode pembayaran ATM.
     <BR><BR>3) Tidak disarankan melakukan transfer dengan LLG/Kliring/SKNBI. Transfer via LLG/Kliring/SKNBI membutuhkan waktu verifikasi lebih lama sehingga e-tiket/voucher hotel mungkin gagal diterbitkan.</span></p>
 </div>
 @endif
 @show

        @section('footer')

            <footer class="bg-dark type-2">
            	<div class="container">
            		<div class="row">
            			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            				<div class="footer-block">
            					<img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" width="212" height="36"  alt="" class="logo-footer">
            					<div class="f_text color-grey-7">{{$infowebsite['deskripsi']}}</div>
            					<div class="footer-share">
            						<a href="{{$infowebsite['facebook']}}"><span class="fa fa-facebook"></span></a>
            						<a href="{{$infowebsite['twitter']}}"><span class="fa fa-twitter"></span></a>
            						<a href="{{$infowebsite['googleplus']}}"><span class="fa fa-google-plus"></span></a>
            					</div>
            				</div>
            			</div>
            			<div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
        				   <div class="footer-block">
        						<h6>Berita</h6>
                   @foreach($articles as $artikel)
                   <?php
       						$linkartikel=url('/')."/blog/".date('Y',strtotime($artikel->tanggal))."/".date('m',strtotime($artikel->tanggal))."/".str_replace(" ","-",$artikel->judul).".html";
       						 ?>
        						<div class="clearfix">
        							<div class="f_news-content">
        								<a class="f_news-tilte color-white link-red" href="{{$linkartikel}}">{{$artikel->judul}}</a>
        								<span class="date-f">{{$artikel->tanggal}}</span>
        								<a href="{{$linkartikel}}" class="r-more">read more</a>
        							</div>
        						</div>
                    @endforeach

        				   </div>
        				</div>
            			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            			   <div class="footer-block">
                             <h6>Kategori:</h6>
                            @foreach($kategorisblog as $cat)
            			      <a href="{{url('/')."/blog/kategori/$cat->category"}}" class="tags-b">{{$cat->category}}</a>
                        @endforeach

        				   </div>
        				</div>
        				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="footer-block">
                             <h6>Info Kontak</h6>

                               <div class="contact-info">
                    	<div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="tel:{{$infowebsite['handphone']}}">{{$infowebsite['handphone']}}</a></div>
        						<div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="mailto:{{$infowebsite['email']}}">{{$infowebsite['email']}}</a></div>

        					</div>
        				   </div>
        				</div>
            		</div>
            	</div>
            	<div class="footer-link bg-black">
            	  <div class="container">
            	  	<div class="row">
            	  		<div class="col-md-12">
            	  		    <div class="copyright">
        						<span> <!----&copy;2015 All rights reserved. LET'STRAVEL--></span>
        					</div>
            	  			<ul>

        						<li><a class="link-aqua" href="{{ url('/') }}/about">Tentang kami</a></li>
        						<li><a class="link-aqua" href="{{ url('/') }}/kontak">Bantuan</a></li>
        						<li><a class="link-aqua" href="{{ url('/') }}/faq">Tanya jawab</a></li>
        						<li><a class="link-aqua" href="{{ url('/') }}/blog">Blog</a></li>

        					</ul>
            	  		</div>
            	  	</div>
            	  </div>
            	</div>
            </footer>
@show
       <script src="{{ URL::asset('asettemplate1/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/idangerous.swiper.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.viewportchecker.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ URL::asset('asettemplate1/js/jquery.mousewheel.min.js') }}"></script>
       <script>
       var isMobile = window.matchMedia("only screen and (max-width: 760px)");

         $( document ).ready(function() {

	   $('.container').addClass('{{$settingan_member->style_layout}}');
});


@if(isset($dialogpolicy))
function dialogalert(){
$( "#dialog-confirm" ).dialog({
  resizable: false,
  height: "auto",
  width: 400,
  modal: true,
	buttons: {
    "Ok": {
            click: function () {
                $(this).dialog("close");
                sudahoke=1;
                  $( "#formutama" ).submit();
            },
            text: 'Lanjut',
            class: 'oranye'
        },
					 "Cancel": {
									 click: function () {
											 $(this).dialog("close");
									 },
									 text: 'Cancel'
							 }
						 }
});
$('.ui-dialog-buttonset').css('float','none');
$('.ui-dialog-buttonset>button:last-child').css('float','right');
$('.ui-dialog-buttonset>button').css('background','#BF2727');
$('.ui-dialog-buttonset>button.oranye').css('background','#ff6600');
$('.ui-dialog-buttonset>button').css('color','#fff');
$('.ui-dialog-buttonset>button').css('height','34');
}
@endif
</script>
         @yield('akhirbody')<script src="{{ URL::asset('asettemplate1/js/all.js') }}"></script>

    </body>
</html>
