<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
		       @yield('sebelumtitel')
    <title>{{$infowebsite['judulweb']}}</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo | Responsive HTML5 Travel Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/animate.min.css') }}">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('asettemplate2/components/revolution_slider/css/settings.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('asettemplate2/components/revolution_slider/css/style.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('asettemplate2/components/jquery.bxslider/jquery.bxslider.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('asettemplate2/components/flexslider/flexslider.css') }}" media="screen" />

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ URL::asset('asettemplate2/css/style.css') }}">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/custom.css') }}">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/updates.css') }}">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/updates.css') }}">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate2/css/responsive.css') }}">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <!-- Javascript Page Loader -->
		<script type="application/javascript" src="{{ URL::asset('assets/js/jquery-2.2.2.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/paceaa.min.js') }}" data-pace-options='{ "ajax": false }'></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/page-loadingaa.js') }}"></script>
		<link href="{{ URL::asset('asettemplate2/css/select2.min.css')}}" rel="stylesheet" />
		<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.maskMoney.min.js')}}"></script>
<script src="{{ URL::asset('asettemplate2/js/select2.min.js')}}"></script>
  <!-- jquery Dialog -->
<link href="{{ URL::asset('assets/css/jquery-ui.css')}}" rel="stylesheet" type="text/css"/>
 <script src="{{ URL::asset('assets/js/jquery-ui.min.js')}}"></script>
 <link href="{{ URL::asset('assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
 <script src="{{ URL::asset('assets/js/bootstrap-toggle.min.js')}}"></script>

 @if(isset($front))
 <style>
 #main {
   margin-bottom: 340px; }
	</style>
@endif
</head>
<body>
    <div id="page-wrapper">
			@section('header')
        <header id="header" class="navbar-static-top">


            <div class="main-header">

                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logoa navbar-brand">
                        <a href="{{url('/')}}" title="{{$infowebsite['judulweb']}} - home">
                            <img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>

                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
													<li class="{{ Request::segment(1) === null ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>

													<li class="{{ Request::segment(1) === 'flight' ? 'active' : null }}"><a href="{{ url('/') }}/flight">Penerbangan</a></li>
													<li class="{{ Request::segment(1) === 'train' ? 'active' : null }}"><a href="{{ url('/') }}/train">Kereta Api</a></li>
													<li class="{{ $menuheader === 'hotel' ? 'active' : null }}"><a href="{{ url('/') }}/hotel">Hotel</a></li>
													<li class="{{ $menuheader === 'cekpesanan' ? 'active' : null }}"><a href="{{ url('/') }}/cekpesanan">Cek Pesanan</a></li>
													<li class="type-1 {{ $menuheader === 'about' ? 'active' : null }}">
														<a href="#">Tentang </a>
														<ul >
														<li><a href="{{ url('/') }}/about">Tentang Kami</a></li>
														<li><a href="{{ url('/') }}/galeri">Galeri</a></li>
														<li><a href="{{ url('/') }}/blog">Blog</a></li>
														<li><a href="{{ url('/') }}/faq">Tanya Jawab</a></li>
														<li><a href="{{ url('/') }}/policies">Kebijakan</a></li>
														<li><a href="{{ url('/') }}/kontak">Kontak</a></li>
														</ul>
													</li>


                        </ul>
                    </nav>
                </div>

                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
											<li class="{{ Request::segment(1) === null ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>

											<li class="{{ Request::segment(1) === 'flight' ? 'active' : null }}"><a href="{{ url('/') }}/flight">Penerbangan</a></li>
											<li class="{{ Request::segment(1) === 'train' ? 'active' : null }}"><a href="{{ url('/') }}/train">Kereta Api</a></li>
											<li class="{{ Request::segment(1) === 'hotel' ? 'active' : null }}"><a href="{{ url('/') }}/hotel">Hotel</a></li>
											<li class="{{ $menuheader === 'cekpesanan' ? 'active' : null }}"><a href="{{ url('/') }}/cekpesanan">Cek Pesanan</a></li>
                        <li class="menu-item-has-children">
                            <a >Tentang</a>
                            <ul>
															<li><a href="{{ url('/') }}/about">Tentang Kami</a></li>
															<li><a href="{{ url('/') }}/galeri">Galeri</a></li>
															<li><a href="{{ url('/') }}/blog">Blog</a></li>
															<li><a href="{{ url('/') }}/faq">Tanya Jawab</a></li>
															<li><a href="{{ url('/') }}/policies">Kebijakan</a></li>
															<li><a href="{{ url('/') }}/kontak">Kontak</a></li>
                            </ul>
                        </li>

                    </ul>



                </nav>
            </div>
            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
				@show
				@yield('kontenweb')
				@if(isset($dialogpolicy))
			  <div id="dialog-confirm" title="Syarat & Ketentuan Transfer">
			    <p> <span id="dialog-confirm_message">
			      Dengan memilih pembayaran melalui Transfer, Anda menyetujui bahwa:
			      <BR><BR>1) Jumlah transfer harus sesuai harga total (hingga 3 angka terakhir) dan dilakukan sebelum batas waktu pembayaran berakhir.
			      <BR><BR>2) Transfer antarbank (transfer antar dua rekening dari bank berbeda) wajib menggunakan metode pembayaran ATM.
			      <BR><BR>3) Tidak disarankan melakukan transfer dengan LLG/Kliring/SKNBI. Transfer via LLG/Kliring/SKNBI membutuhkan waktu verifikasi lebih lama sehingga e-tiket/voucher hotel mungkin gagal diterbitkan.</span></p>
			  </div>
			  @endif
				@section('footer')
				<footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>Tags</h2>
                            <ul class="discover triangle hover row">
															@foreach($kategorisblog as $cat)
                                <li class="col-xs-6"><a href="{{url('/')."/blog/kategori/$cat->category"}}">{{$cat->category}}</a></li>
                              @endforeach
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Travel News</h2>
                            <ul class="travel-news">
				                   @foreach($articles as $artikel)
				                   <?php
				       						$linkartikel=url('/')."/blog/".date('Y',strtotime($artikel->tanggal))."/".date('m',strtotime($artikel->tanggal))."/".str_replace(" ","-",$artikel->judul).".html";
				       						 ?>
                                <li>
                                    <div class="thumb">
                                        <a href="{{$linkartikel}}">
                                            <img src="http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="{{$linkartikel}}">{{$artikel->judul}}</a></h5>
																				<?php $content = html_cut($artikel->isi, 60); ?>
                                        <p><?php echo $content;?></p>
                                        <span class="date">{{$artikel->tanggal}}</span>
                                    </div>
                                </li>
						                    @endforeach

                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>
                            <br />
                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email" />
                            </div>
                            <br />
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>About</h2>
                            <p>{{$infowebsite['deskripsi']}}</p>
                            <br />
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> {{$infowebsite['handphone']}}</span>
                                <br />
                                <a href="#" class="contact-email">{{$infowebsite['email']}}</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="{{$infowebsite['twitter']}}" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="{{$infowebsite['googleplus']}}" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="{{$infowebsite['facebook']}}" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="{{$infowebsite['linkedin']}}" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    <div class="logoa pull-left">
                        <a href="{{url('/')}}" title="Travelo - home">
                            <img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="{{$infowebsite['judulweb']}} - home" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2014 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
				@show
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery-ui.1.10.4.min.js') }}"></script>

    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/bootstrap.min.js') }}"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/components/flexslider/jquery.flexslider.js') }}"></script>

    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery.stellar.min.js') }}"></script>

    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/jquery.stellar.min.js') }}"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/theme-scripts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('asettemplate2/js/scripts.js') }}"></script>

    <script type="text/javascript">
		var isMobile = window.matchMedia("only screen and (max-width: 760px)");

		tjq(".flexslider").flexslider({
				animation: "fade",
				controlNav: false,
				animationLoop: true,
				directionNav: false,
				slideshow: true,
				slideshowSpeed: 5000
		});


		@if(isset($dialogpolicy))
		function dialogalert(){
		$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height: "auto",
		  width: 400,
		  modal: true,
			buttons: {
		    "Ok": {
		            click: function () {
		                $(this).dialog("close");
		                sudahoke=1;
		                  $( "#formutama" ).submit();
		            },
		            text: 'Lanjut',
		            class: 'oranye'
		        },
							 "Cancel": {
											 click: function () {
													 $(this).dialog("close");
											 },
											 text: 'Cancel'
									 }
								 }
		});
		$('.ui-dialog-buttonset').css('float','none');
		$('.ui-dialog-buttonset>button:last-child').css('float','right');
		$('.ui-dialog-buttonset>button').css('background','#BF2727');
		$('.ui-dialog-buttonset>button.oranye').css('background','#ff6600');
		$('.ui-dialog-buttonset>button').css('color','#fff');
		$('.ui-dialog-buttonset>button').css('height','34');
		}
		@endif
    </script>

						 @yield('akhirbody')
</body>
</html>
