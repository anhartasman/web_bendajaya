<!DOCTYPE HTML>
<html>

<head>
	 @yield('sebelumtitel')
    <title>{{$infowebsite['judulweb']}}</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="{{ URL::asset('asettemplate3/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('asettemplate3/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('asettemplate3/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('asettemplate3/css/styles.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('asettemplate3/css/mystyles.css')}}">
    <script src="{{ URL::asset('asettemplate3/js/modernizr.js')}}"></script>

		<script type="application/javascript" src="{{ URL::asset('assets/js/jquery-2.2.2.min.js')}}"></script>
			<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.maskMoney.min.js')}}"></script>
	<!-- jquery Dialog -->
	<link href="{{ URL::asset('assets/css/jquery-ui.css')}}" rel="stylesheet" type="text/css"/>
	 <script src="{{ URL::asset('assets/js/jquery-ui.min.js')}}"></script>
	 <link href="{{ URL::asset('assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
	 <script src="{{ URL::asset('assets/js/bootstrap-toggle.min.js')}}"></script>


</head>

<body>

    <!-- FACEBOOK WIDGET
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
		 -->
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a class="logo" href="{{url('/')}}">
                                <img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="Image Alternative text" title="Image Title" />
                            </a>
                        </div>
												<!--
                        <div class="col-md-3 col-md-offset-2">
                            <form class="main-header-search">
                                <div class="form-group form-group-icon-left">
                                    <i class="fa fa-search input-icon"></i>
                                    <input type="text" class="form-control">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <div class="top-user-area clearfix">
                                <ul class="top-user-area-list list list-horizontal list-border">
                                    <li class="top-user-area-avatar">
                                        <a href="user-profile.html">
                                            <img class="origin round" src="img/40x40.png" alt="Image Alternative text" title="AMaze" />Hi, John</a>
                                    </li>
                                    <li><a href="#">Sign Out</a>
                                    </li>
                                    <li class="nav-drop"><a href="#">USD $<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a>
                                        <ul class="list nav-drop-menu">
                                            <li><a href="#">EUR<span class="right">€</span></a>
                                            </li>
                                            <li><a href="#">GBP<span class="right">£</span></a>
                                            </li>
                                            <li><a href="#">JPY<span class="right">円</span></a>
                                            </li>
                                            <li><a href="#">CAD<span class="right">$</span></a>
                                            </li>
                                            <li><a href="#">AUD<span class="right">A$</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="top-user-area-lang nav-drop">
                                        <a href="#">
                                            <img src="img/flags/32/uk.png" alt="Image Alternative text" title="Image Title" />ENG<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i>
                                        </a>
                                        <ul class="list nav-drop-menu">
                                            <li>
                                                <a title="German" href="#">
                                                    <img src="img/flags/32/de.png" alt="Image Alternative text" title="Image Title" /><span class="right">GER</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Japanise" href="#">
                                                    <img src="img/flags/32/jp.png" alt="Image Alternative text" title="Image Title" /><span class="right">JAP</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Italian" href="#">
                                                    <img src="img/flags/32/it.png" alt="Image Alternative text" title="Image Title" /><span class="right">ITA</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="French" href="#">
                                                    <img src="img/flags/32/fr.png" alt="Image Alternative text" title="Image Title" /><span class="right">FRE</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Russian" href="#">
                                                    <img src="img/flags/32/ru.png" alt="Image Alternative text" title="Image Title" /><span class="right">RUS</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Korean" href="#">
                                                    <img src="img/flags/32/kr.png" alt="Image Alternative text" title="Image Title" /><span class="right">KOR</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
											-->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="nav">
                    <ul class="slimmenu" id="slimmenu">

											<li class="{{ Request::segment(1) === null ? 'active' : null }}"><a href="{{ url('/') }}">Home</a></li>

											<li class="{{ Request::segment(1) === 'flight' ? 'active' : null }}"><a href="{{ url('/') }}/flight">Penerbangan</a></li>
											<li class="{{ Request::segment(1) === 'train' ? 'active' : null }}"><a href="{{ url('/') }}/train">Kereta Api</a></li>
											<li class="{{ $menuheader === 'hotel' ? 'active' : null }}"><a href="{{ url('/') }}/hotel">Hotel</a></li>
											<li class="{{ $menuheader === 'cekpesanan' ? 'active' : null }}"><a href="{{ url('/') }}/cekpesanan">Cek Pesanan</a></li>
											<li class="type-1 {{ $menuheader === 'about' ? 'active' : null }}">
												<a href="#">Tentang </a>
												<ul >
												<li><a href="{{ url('/') }}/about">Tentang Kami</a></li>
												<li><a href="{{ url('/') }}/galeri">Galeri</a></li>
												<li><a href="{{ url('/') }}/blog">Blog</a></li>
												<li><a href="{{ url('/') }}/faq">Tanya Jawab</a></li>
												<li><a href="{{ url('/') }}/policies">Kebijakan</a></li>
												<li><a href="{{ url('/') }}/kontak">Kontak</a></li>
												</ul>
											</li>

										</ul>
                </div>
            </div>
        </header>

      @yield('kontenweb')
			@if(isset($dialogpolicy))
			<div id="dialog-confirm" title="Syarat & Ketentuan Transfer">
				<p> <span id="dialog-confirm_message">
					Dengan memilih pembayaran melalui Transfer, Anda menyetujui bahwa:
					<BR><BR>1) Jumlah transfer harus sesuai harga total (hingga 3 angka terakhir) dan dilakukan sebelum batas waktu pembayaran berakhir.
					<BR><BR>2) Transfer antarbank (transfer antar dua rekening dari bank berbeda) wajib menggunakan metode pembayaran ATM.
					<BR><BR>3) Tidak disarankan melakukan transfer dengan LLG/Kliring/SKNBI. Transfer via LLG/Kliring/SKNBI membutuhkan waktu verifikasi lebih lama sehingga e-tiket/voucher hotel mungkin gagal diterbitkan.</span></p>
			</div>
			@endif


        <footer id="main-footer">
            <div class="container">
                <div class="row row-wrap">
                    <div class="col-md-3">
                        <a class="logo" href="{{url('/')}}">
                            <img src="{{ URL::asset('uploads/images/logoweb'.$settingan_member->mmid.'.png') }}" alt="Image Alternative text" title="Image Title" />
                        </a>
                        <p class="mb20">{{$infowebsite['deskripsi']}}</p>
                        <ul class="list list-horizontal list-space">
                            <li>
                                <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="{{$infowebsite['facebook']}}"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="{{$infowebsite['twitter']}}"></a>
                            </li>
                            <li>
                                <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="{{$infowebsite['googleplus']}}"></a>
                            </li>
                            <li>
                                <a class="fa fa-linkedin box-icon-normal round animate-icon-bottom-to-top" href="{{$infowebsite['linkedin']}}"></a>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-3">
                        <h4>Newsletter</h4>
                        <form>
                            <label>Enter your E-mail Address</label>
                            <input type="text" class="form-control">
                            <p class="mt5"><small>*We Never Send Spam</small>
                            </p>
                            <input type="submit" class="btn btn-primary" value="Subscribe">
                        </form>
                    </div>
                    <div class="col-md-2">
                        <ul class="list list-footer">
                            <li><a href="{{url('/about')}}">Tentang Kami</a>
                            </li>
                            <li><a href="{{url('/faq')}}">Tanya jawab</a>
                            </li>
                            <li><a href="{{url('/policies')}}">Kebijakan</a>
                            </li>
                            <li><a href="{{url('/kontak')}}">Kontak Kami</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4>Punya Pertanyaan?</h4>
                        <h4 class="text-color">{{$infowebsite['handphone']}}</h4>
                        <h4><a href="#" class="text-color">{{$infowebsite['email']}}</a></h4>

                    </div>

                </div>
            </div>
        </footer>

        <script src="{{ URL::asset('asettemplate3/js/bootstrap.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/slimmenu.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/bootstrap-datepicker.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/bootstrap-timepicker.js')}}"></script>

        <script src="{{ URL::asset('asettemplate3/js/dropit.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/ionrangeslider.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/icheck.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/fotorama.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/typeahead.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/card-payment.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/magnific.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/owl-carousel.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/fitvids.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/tweet.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/countdown.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/gridrotator.js')}}"></script>
        <script src="{{ URL::asset('asettemplate3/js/custom.js')}}"></script>
				<link href="{{ URL::asset('asettemplate3/css/select2.min.css')}}" rel="stylesheet" />
				<script src="{{ URL::asset('asettemplate3/js/select2.min.js')}}"></script>
				<script type="text/javascript">
				var isMobile = window.matchMedia("only screen and (max-width: 760px)");
				@if(isset($dialogpolicy))
				function dialogalert(){
				$( "#dialog-confirm" ).dialog({
				  resizable: false,
				  height: "auto",
				  width: 400,
				  modal: true,
					buttons: {
				    "Ok": {
				            click: function () {
				                $(this).dialog("close");
				                sudahoke=1;
				                  $( "#formutama" ).submit();
				            },
				            text: 'Lanjut',
				            class: 'oranye'
				        },
									 "Cancel": {
													 click: function () {
															 $(this).dialog("close");
													 },
													 text: 'Cancel'
											 }
										 }
				});
				$('.ui-dialog-buttonset').css('float','none');
				$('.ui-dialog-buttonset>button:last-child').css('float','right');
				$('.ui-dialog-buttonset>button').css('background','#BF2727');
				$('.ui-dialog-buttonset>button.oranye').css('background','#ff6600');
				$('.ui-dialog-buttonset>button').css('color','#fff');
				$('.ui-dialog-buttonset>button').css('height','34');
				}
				@endif
				</script>

		</div>

								 @yield('akhirbody')
</body>

</html>
