-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  `salt` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_admin` (`id`, `nama`, `username`, `password`, `salt`, `level`, `last_login`, `update_time`) VALUES
(1,	'Ujang',	'ujang',	'1c87e047981901fcd2ed04f528fe62cf188dbf3525074bcea4ca0f318c78304ff6ab1a8d6463c807181948fa3b492b797b52f07bc5aa98af80333d850ab3902c',	'garammanis',	5,	NULL,	NULL),
(2,	'Udin',	'udin',	'9237a2f7e6c70c987620ebefd1f695b5b8eb37654729ca7f8dd438b8b9e94819f561ce5af1caf1b77d195719d3d40008af364d6e30cea36d1ab7aad34c6382bf',	'garammanis827',	5,	'0000-00-00 00:00:00',	NULL);

DROP TABLE IF EXISTS `tb_call_history`;
CREATE TABLE `tb_call_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `seconds` int(11) NOT NULL,
  `bill` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_call_history` (`id`, `idk`, `idl`, `start`, `seconds`, `bill`) VALUES
(1,	2,	5,	'2017-02-23 19:33:54',	15,	1125),
(2,	2,	5,	'2017-02-24 15:01:06',	15,	1125),
(3,	2,	5,	'2017-02-24 15:02:49',	10,	750),
(4,	2,	5,	'2017-02-24 18:22:57',	10,	750),
(5,	2,	4,	'2017-02-25 20:41:41',	10,	1010),
(6,	2,	5,	'2017-03-01 15:59:15',	5,	375),
(7,	2,	5,	'2017-03-01 16:00:38',	15,	1125),
(8,	2,	5,	'2017-03-01 18:10:35',	5,	375),
(9,	2,	5,	'2017-03-02 06:52:45',	5,	375),
(10,	2,	5,	'2017-03-02 09:45:36',	5,	375),
(11,	2,	5,	'2017-03-02 09:51:55',	5,	375),
(12,	2,	5,	'2017-03-02 10:59:40',	40,	3000),
(13,	2,	5,	'2017-03-02 11:00:53',	10,	750),
(14,	2,	5,	'2017-03-02 11:02:23',	15,	1125),
(15,	2,	5,	'2017-03-02 11:17:57',	5,	375),
(16,	2,	5,	'2017-03-02 11:43:17',	10,	750),
(17,	2,	5,	'2017-03-02 11:48:14',	5,	375),
(18,	2,	5,	'2017-03-02 15:58:35',	5,	375),
(19,	2,	5,	'2017-03-02 16:01:49',	5,	375),
(20,	2,	5,	'2017-03-03 04:06:21',	10,	750),
(21,	2,	5,	'2017-03-03 04:07:21',	5,	375),
(22,	2,	5,	'2017-03-07 09:12:21',	10,	750),
(23,	2,	5,	'2017-03-07 09:15:40',	5,	375),
(24,	2,	5,	'2017-03-07 09:30:37',	10,	750),
(25,	2,	5,	'2017-03-10 13:58:11',	15,	1125),
(26,	2,	5,	'2017-03-10 14:06:36',	15,	1125),
(27,	2,	5,	'2017-03-14 10:01:17',	5,	375),
(28,	1,	4,	'2017-03-19 14:05:09',	0,	0),
(29,	2,	5,	'2017-04-05 05:59:31',	0,	0);

DROP TABLE IF EXISTS `tb_filepesan`;
CREATE TABLE `tb_filepesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idp` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ukuran` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_filepesan` (`id`, `idp`, `nama`, `ukuran`) VALUES
(1,	3,	'3_Slip Gaji Anhar Tasman Des 16.pdf.pdf',	'0'),
(2,	4,	'4_manual-faq.txt.txt',	'0'),
(3,	5,	'5_en_story.txt.txt',	'0'),
(4,	6,	'6_en.txt.txt',	'0'),
(5,	7,	'7_en.txt.txt',	'0'),
(6,	7,	'7_IMG_20161002_111303.jpg.jpg',	'0'),
(7,	8,	'8_en.txt.txt',	'0'),
(8,	8,	'8_IMG_20161002_111303.jpg.jpg',	'0'),
(9,	10,	'10_bbm_tone.wav.wav',	'0'),
(10,	11,	'11_IMG_20170220_102506.jpg.jpg',	'0'),
(11,	12,	'12_IMG_20170220_102506.jpg.jpg',	'0'),
(12,	12,	'12_facebook_ringtone_pop.m4a.m4a',	'0'),
(13,	13,	'13_IMG-20170217-WA0005.jpeg.jpeg',	'0'),
(14,	14,	'14_IMG-20170217-WA0005.jpeg.jpeg',	'0'),
(15,	14,	'14_IMG_20170130_174820.jpg.jpg',	'0'),
(16,	15,	'15_IMG_20170220_043758.jpg.jpg',	'0'),
(17,	15,	'15_IMG_20170220_102506.jpg.jpg',	'0'),
(18,	16,	'16_IMG_20170220_102506.jpg.jpg',	'0'),
(19,	16,	'16_IMG_20170220_043758.jpg.jpg',	'0'),
(20,	17,	'17_IMG_20170220_102506.jpg.jpg',	'0'),
(21,	18,	'18_IMG_20170220_043758.jpg.jpg',	'0'),
(22,	18,	'18_1487590161949.jpg.jpg',	'0'),
(23,	19,	'19_IMG_20170215_160920_386.jpg.jpg',	'0'),
(24,	19,	'19_13x19cm_nulisbukutemplate.doc.doc.doc',	'0'),
(25,	20,	'20_Wait-for-it.mp4.mp4',	'0'),
(26,	21,	'21_VID_85290914_110147_282.mp4.mp4',	'0'),
(27,	21,	'21_Its-a-trap-to-grow-up.jpg.jpg',	'0'),
(28,	23,	'23_IMG-20170220-WA0011.jpeg',	''),
(29,	24,	'24_IMG-20170220-WA0011.jpeg',	''),
(30,	25,	'25_IMG-20170220-WA0011.jpeg',	''),
(31,	26,	'26_IMG-20170220-WA0011.jpeg',	''),
(32,	27,	'27_IMG-20170220-WA0011.jpeg',	'736 Kb'),
(33,	28,	'28_IMG-20170220-WA0011.jpeg',	'736 Kb'),
(34,	28,	'28_en_story.txt',	'89 Kb'),
(35,	28,	'28_Slip Gaji Anhar Tasman Des 16.pdf',	'39 Kb'),
(36,	29,	'29_Masya Allah dan Subhanallah.jpg',	'53 Kb'),
(37,	30,	'30_Masya Allah dan Subhanallah.jpg',	'53 Kb'),
(38,	31,	'31_1487725661534.jpg',	'36 Kb'),
(39,	32,	'32_1487744087377.jpg',	'57 Kb'),
(40,	33,	'33_1487744964351.jpg',	'64 Kb'),
(41,	34,	'34_1487745389276.jpg',	'65 Kb'),
(42,	35,	'35_1487745878132.jpg',	'53 Kb'),
(43,	36,	'36_IMG-20170222-WA0003.jpg',	'81 Kb'),
(44,	37,	'37_1487747776265.jpg',	'59 Kb'),
(45,	38,	'38_1487748035968.jpg',	'61 Kb'),
(46,	38,	'38_1487748047170.jpg',	'49 Kb'),
(47,	39,	'39_1487750500119.jpg',	'32 Kb'),
(48,	40,	'40_1487751030411.jpg',	'48 Kb'),
(49,	41,	'41_1488258449285.jpg',	'36 Kb'),
(50,	43,	'43_1489590158065.jpg',	'53 Kb');

DROP TABLE IF EXISTS `tb_infosistem`;
CREATE TABLE `tb_infosistem` (
  `kolom` varchar(20) NOT NULL,
  `isi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_infosistem` (`kolom`, `isi`) VALUES
('avidhape',	2);

DROP TABLE IF EXISTS `tb_jem_lawyer`;
CREATE TABLE `tb_jem_lawyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idl` int(11) DEFAULT NULL,
  `idj` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_jem_lawyer` (`id`, `idl`, `idj`) VALUES
(3,	4,	2),
(4,	4,	4),
(5,	5,	3),
(6,	5,	1),
(7,	4,	1),
(8,	8,	1),
(9,	8,	3),
(10,	8,	4),
(11,	7,	1),
(12,	7,	3),
(13,	9,	1),
(14,	9,	2),
(15,	10,	1),
(16,	10,	4);

DROP TABLE IF EXISTS `tb_jenishukum`;
CREATE TABLE `tb_jenishukum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `materil` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_jenishukum` (`id`, `jenis`, `materil`) VALUES
(1,	'Pidana',	1),
(2,	'Perdata',	1),
(3,	'Investasi',	1),
(4,	'Perusahaan',	1);

DROP TABLE IF EXISTS `tb_klien`;
CREATE TABLE `tb_klien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idu` int(11) NOT NULL,
  `online` int(1) DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  `saldo` int(11) DEFAULT '0',
  `token` text,
  `idch` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_klien` (`id`, `idu`, `online`, `update_time`, `saldo`, `token`, `idch`) VALUES
(1,	1,	0,	NULL,	4763150,	NULL,	NULL),
(2,	2,	0,	NULL,	4894970,	'ej2M7um8Cgk:APA91bGQuXNyuxmrdsr75YXiEmM4MGAB-bL5jGlvvNkvKkf8A1ksjLwfVHhCE_NktOu9j_6J_08HDi0yrOy-hfYOt3ORMGQhnQFv-EdAmFQtz9b7jGPkEQpXS8v3jnNASPChTsesYDbd',	NULL);

DROP TABLE IF EXISTS `tb_lawyer`;
CREATE TABLE `tb_lawyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idu` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `online` int(1) DEFAULT '0',
  `onlinesejak` datetime DEFAULT NULL,
  `permenit` int(11) DEFAULT NULL,
  `filefoto` varchar(50) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `token` text,
  `latitude` varchar(15) DEFAULT '0',
  `longitude` varchar(15) DEFAULT '0',
  `firmlatitude` varchar(15) DEFAULT '0',
  `firmlongitude` varchar(15) DEFAULT '0',
  `tampilkanposisi` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_lawyer` (`id`, `idu`, `nama`, `email`, `online`, `onlinesejak`, `permenit`, `filefoto`, `update_time`, `username`, `password`, `saldo`, `token`, `latitude`, `longitude`, `firmlatitude`, `firmlongitude`, `tampilkanposisi`) VALUES
(4,	3,	'Ucok Salibatae',	'ucok_salibatea@yahoo.com',	1,	NULL,	6010,	'fotolawyer4.jpg',	'2017-03-31 21:31:30',	'la_ucok',	'ucok123',	138370,	'0',	'-6.183565',	'106.767870',	'-6.184706',	'106.768632',	2),
(5,	4,	'Otong',	'otong@yahoo.com',	1,	'2017-04-03 12:47:47',	4500,	'fotolawyer5.jpg',	'2016-12-22 17:41:21',	'la_otong',	'otong123',	761250,	'0',	'-6.18401142',	'106.76744529',	'0',	'0',	1),
(7,	5,	'Taji M. Sianturi',	'taji@yahoo.com',	1,	NULL,	5500,	'fotolawyer7.png',	'2016-12-23 09:30:14',	'la_todung',	'todung123',	279220,	NULL,	'0',	'0',	'0',	'0',	0),
(8,	6,	'Todung Mulya Lubis',	'todung@yahoo.com',	0,	NULL,	7800,	'fotolawyer8.png',	NULL,	'la_todung',	'todung123',	5850,	NULL,	'0',	'0',	'0',	'0',	0),
(9,	7,	'Juniver Girsang',	'juniver@yahoo.com',	1,	NULL,	6500,	'fotolawyer9.png',	NULL,	'la_juniver',	'juniver123',	15805,	NULL,	'0',	'0',	'0',	'0',	0),
(10,	8,	'Elza Syarief',	'elza@yahoo.com',	0,	NULL,	6500,	'fotolawyer10.png',	NULL,	'la_elza',	'elza123',	1534720,	NULL,	'0',	'0',	'0',	'0',	0);

DROP TABLE IF EXISTS `tb_pesan`;
CREATE TABLE `tb_pesan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `idk` int(11) NOT NULL,
  `idl` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL,
  `tanggalupdate` datetime NOT NULL,
  `main` int(1) NOT NULL,
  `idbm` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_pesan` (`id`, `status`, `idk`, `idl`, `judul`, `pesan`, `tanggal`, `tanggalupdate`, `main`, `idbm`) VALUES
('1',	'0',	'2',	'5',	'zf',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('2',	'0',	'2',	'5',	'',	'tes tes tes',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('3',	'0',	'2',	'5',	'',	'tes tes tesxt Dy xgxgzgzg',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('4',	'0',	'2',	'5',	'',	'ycycyd',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('5',	'0',	'2',	'5',	'',	'xgxtxtzr',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('6',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('7',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('8',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('9',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('10',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('11',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('12',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('13',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('14',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('15',	'0',	'2',	'5',	'',	'CJ dhhh',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('16',	'0',	'2',	'5',	'',	'sxjHH hhchahgdycy',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('17',	'0',	'2',	'5',	'',	'hHahaha',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('18',	'0',	'2',	'5',	'',	'hahahahs',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('19',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('20',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('21',	'0',	'2',	'5',	'',	'',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('22',	'0',	'2',	'5',	'',	'gsgvxh',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('23',	'0',	'2',	'5',	'',	'ffc',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('24',	'0',	'2',	'5',	'',	'ffc',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('25',	'0',	'2',	'5',	'',	'SG sgsg',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('26',	'0',	'2',	'5',	'',	'SG sgsg',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0',	'0'),
('27',	'0',	'2',	'5',	'fhxhhxh',	'SG sgsg',	'2017-02-21 20:21:28',	'0000-00-00 00:00:00',	'0',	'0'),
('28',	'0',	'2',	'5',	'fhxhhxh',	'SG sgsg',	'2017-02-21 20:22:14',	'0000-00-00 00:00:00',	'0',	'0'),
('29',	'0',	'1',	'4',	'',	'',	'2017-02-22 07:33:26',	'0000-00-00 00:00:00',	'0',	'0'),
('30',	'0',	'1',	'4',	'tes pesan',	'apakah bisa masuk?',	'2017-02-22 07:35:40',	'0000-00-00 00:00:00',	'0',	'0'),
('31',	'0',	'1',	'4',	'saya ditilang',	'polisinya minta 100ribu? dikasih ga?',	'2017-02-22 08:03:22',	'0000-00-00 00:00:00',	'0',	'0'),
('32',	'0',	'1',	'5',	'tes siang',	'lagi coba lagi',	'2017-02-22 13:14:53',	'0000-00-00 00:00:00',	'0',	'0'),
('33',	'0',	'1',	'4',	'tes meeting',	'tes lagi meeting',	'2017-02-22 13:29:30',	'0000-00-00 00:00:00',	'0',	'0'),
('34',	'0',	'1',	'4',	'tes air',	'kirim air lagi',	'2017-02-22 13:36:36',	'0000-00-00 00:00:00',	'0',	'0'),
('35',	'0',	'1',	'4',	'tes makan',	'makan nasi enak',	'2017-02-22 13:44:42',	'0000-00-00 00:00:00',	'0',	'0'),
('36',	'0',	'2',	'5',	'CC',	'XF XF xt',	'2017-02-22 14:05:00',	'0000-00-00 00:00:00',	'0',	'0'),
('37',	'0',	'1',	'4',	'tes gelas',	'kirim gelas',	'2017-02-22 14:16:20',	'0000-00-00 00:00:00',	'0',	'0'),
('38',	'0',	'1',	'4',	'saya kena tilang',	'polisinya minta uang.tolong dong',	'2017-02-22 14:20:56',	'0000-00-00 00:00:00',	'0',	'0'),
('39',	'0',	'2',	'5',	'tes',	'teeees',	'2017-02-22 15:02:30',	'0000-00-00 00:00:00',	'0',	'0'),
('40',	'0',	'1',	'4',	'tes sepatu',	'sepatu',	'2017-02-22 15:10:35',	'0000-00-00 00:00:00',	'0',	'0'),
('41',	'0',	'1',	'4',	'gdhdhd',	'chdhdhd',	'2017-02-28 12:07:36',	'0000-00-00 00:00:00',	'0',	'0'),
('42',	'0',	'2',	'0',	'jJjidudud',	'jajajsjzjsjzizi',	'2017-03-01 14:57:24',	'0000-00-00 00:00:00',	'0',	'0'),
('43',	'0',	'1',	'0',	'hfjdhd',	'bdhdh',	'2017-03-15 22:02:24',	'0000-00-00 00:00:00',	'0',	'0');

DROP TABLE IF EXISTS `tb_sessid`;
CREATE TABLE `tb_sessid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessid` text,
  `tabel` varchar(10) DEFAULT NULL,
  `idt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_sessid` (`id`, `sessid`, `tabel`, `idt`) VALUES
('132',	'8c4d27abfa2f8e30c456fecb4e5bd2ccba3541a5298a9c4808e3ad7f9265f2afb85140cc9af17af350fa50fb2e7eebcf8e07831b15793f291f17b8263637c3fc',	'klien',	'5'),
('147',	'296194564d2d120dab18c13ea78b254b447650a83137a220576476c420d3310e92c52864a49c6c62c735d2bbfc096d9381181ae14cd3e2db1225614617cff6da',	'lawyer',	'4'),
('153',	'8188a2ebcfbb441245eac9310077ddd9fd89d402b1f4f3e8d9d9e430ef94342cfe8d6a39be2aa89873a35003e7e34970841e297015a7f627aefa62625db6ea0c',	'lawyer',	'5'),
('154',	'1892ca101eca90d7602c9d4e729198d57671b24b561a29ddea2fcf69a11c4ae918bdd01e009b2d8ea2a95d9efe72e7149c14bc06657da37567450d5ffdb4e72a',	'klien',	'1'),
('164',	'c0abe76495480e57d25c62fe0ce1f6995eee81a9a248d0e20f592b5a11f63ff454b76cdbe54ed598c7f5e6bb1154440244be369c894b7970a60f37b16942651a',	'klien',	'15'),
('172',	'ae8880146a80a7b8332e2bfbfc5918c57995dbc95c926ff49b2d94eb1d5dee851522d0dbeb0394263e9e84dbdc83c9910b6d3a48a016cf4895c95970b61c4ceb',	'klien',	'14'),
('174',	'13e8592ff44b3835e9259e7ddace91a1011d81bf9d6f4f25c019d3b625a60cfb2971c94f81ce31617de5e2a8f099036f2471ef330ae77b1a1c54865d4691f496',	'klien',	'2');

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '0',
  `filefoto` varchar(30) NOT NULL DEFAULT 'fotodefault.png',
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `salt` varchar(20) NOT NULL,
  `sessid` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tb_user` (`id`, `level`, `nama`, `email`, `phone`, `filefoto`, `username`, `password`, `salt`, `sessid`) VALUES
('1',	'1',	'Cecep',	'cecepsuhaja@yahoo.com',	'',	'fotodefault.png',	'cecep',	'fd998c35e8595229e2e798bf6059467ab3c281a3b022289db760317a0fe1b50b423701a880576927d0029fcc0524af416e6627494c324333077e89945bb16b03',	'garammanis373',	''),
('2',	'1',	'Tomas',	'tomas@yahoo.com',	'',	'fotodefault.png',	'tomas',	'715a1a709e287716ee7626b5a37bfd05dcbe7eeee20178b2f42709f731de8df7e68877f8f31025c396929979c18f5b42401e74f9fb1877353c5a74ef8fd68100',	'garammanis291',	'e5441e08ef07f08aa7d2da4df49f120f38bab4c78ffb706e214a1a54f36d506f4371fce495d015bf0ad67403ddfbaac1b37861ac372ab27d94c466293c1893bd'),
('3',	'2',	'Ucok Salibatae',	'ucok_salibatea@yahoo.com',	'0',	'fotolawyer4.jpg',	'la_ucok',	'd29b199143d89ea2b0c5d7b336b2aa29400eac9fe3ff04e475503b53f27875235abcdfed04ae42de98809b428e17fdba5a0243a8410d59d988f8cc6348ceca5d',	'garammanis542',	''),
('4',	'2',	'Otong',	'otong@yahoo.com',	'6287886820318',	'fotolawyer4.jpg',	'la_otong',	'606a2da87ddf10e9b0548fa3ee16e97bc94b81aed529e2602572830152e461f13326ef00d486eddd5b37836167abe07fbd0049cb1cd6308beb5b301970b914c5',	'garammanis342',	''),
('5',	'2',	'Taji M. Sianturi',	'taji@yahoo.com',	'0',	'fotolawyer7.png',	'la_taji',	'e77556639111db3158bb6c43af3bcb921a687a649cb2618b3717def65bb3bca0939c9024291307c0404005ea26caea23f185d61103f69308b84b029fc535c888',	'garammanis987',	''),
('6',	'2',	'Todung Mulya Lubis',	'todung@yahoo.com',	'0',	'fotolawyer8.png',	'la_todung',	'3055bbba87b3092bf7e617be9c8876f3e280b0e87ba17378b8d94f833740f7fbaba1bc0ece160e4aeb6cf6df71561342c35af65372df982eb6604c525c92ebf9',	'garammanis360',	''),
('7',	'2',	'Juniver Girsang',	'juniver@yahoo.com',	'0',	'fotolawyer9.png',	'la_juniver',	'39c23206e06999a08a1bec38a55f85788940c9b91b781d4991ca224e67a2c5a6aadfa757afc404b8c09fed435166658e36ba0a62d3e16f959e8402430cc2d1b1',	'garammanis685',	''),
('8',	'2',	'Elza Syarief',	'elza@yahoo.com',	'0',	'fotolawyer10.png',	'la_elza',	'b686b2670304470fcbcd20475cf43547b489303d275f95d69c828c335bb91d1cc6ecc14d33ecf95baf6ee1db4bf574de21816f3c4a5a881068be8f75137465b3',	'garammanis637',	'');

-- 2017-04-13 22:28:24
